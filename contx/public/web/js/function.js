
function readURL(input) {
    if (input.files && input.files[0]) {

        var reader = new FileReader();

        reader.onload = function (e) {
          // alert(e.target.result);
            $('.company-logo').attr('src', e.target.result);
            $(".inputFilesActions .uploadImg span").text("Change Image");
            $(".remove-logo").show();
        };
        reader.readAsDataURL(input.files[0]);
    }
}
$(document).ready(function(){
	$.validator.addMethod('validUrl', function(value, element) {
        var url = $.validator.methods.url.bind(this);
        return url(value, element) || url('http://' + value, element);
    }, 'Please enter a valid URL');

	$(".multiSelect").treeMultiselect({startCollapsed: true});
	$(".chosen-select").chosen();

  //validate founders
  $("#new-company").validate({
        rules: {
            creator_name: {
		      	required: true
		    },
		    creator_email: {
		      	required: true,
		      	email: true
		    },
		    name: {
		      	required: true
		    },
		    // founder_name: {
		    //   	required: true
		    // },
		    description: {
		      	required: true
		    },
		    website: {
		      	required: true,
		      	validUrl: true
		    },
		    facebook: {
		      	validUrl: true
		    },
		    twitter: {
		      	validUrl: true
		    },
		    linkedin: {
		      	validUrl: true
		    },
		    instagram: {
		      	validUrl: true
		    }
        },
        messages :{
	        creator_name: {
		      	required: 'Enter your full name'
		    },
		    creator_email: {
		      	required: 'Enter your email'
		    },
		    name: {
		      	required: 'Enter company name'
		    },
		    // founder_name: {
		    //   	required: 'Enter founder name'
		    // },
		    description: {
		      	required: 'Enter description'
		    },
		    website: {
		      	required: 'Enter website'
		    }
	    },
	    submitHandler: function() {
        // alert($("#founder-arr").val());
        if($("#founder-arr").val()==''){

          $("#error-founder").text("Choose Founder").show();
          return false;
        }
	    	if($("#industries-arr").val().length==0){
	    		$("#error-industry").text("Choose Industries").show();
	    		return false;
	    	}
        $("#error-founder").hide();
	    	$("#error-industry").hide();
	    	$(".loader-layer").show();
			setTimeout(function(){
		    	$.ajax(
		        {
		            url: "/company/new",
		            type: "post",
		            datatype: "json",
		            data: new FormData($('#new-company')[0]),
		            contentType: false,
	    			processData: false
		        }).done(function(data){
		        	$(".loader-layer").hide();
		        	if(data.success == 1){
                        swal({
                          title: "Added",
                          text: "Company details has been added successfully!",
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "ok!",
                          closeOnConfirm: true
                        },
                        function(){
                            $('#createComp').modal("toggle");
                            window.location.reload();
                        });
                         window.location.href = "/companies";


                    }else{
                        swal("Error", data.msg, "error");
                    }
		        }).fail(function(jqXHR, ajaxOptions, thrownError){
		            $(".loader-layer").hide();
		            swal("Error", jqXHR.responseJSON.msg, "error");
		            // swal("Error", 'A company has already been registered with this name', "error");
		        });
			},1000);
		}
    });

	//validate new company
	$("#new-founder").validate({
        rules: {
            founder_name: {
		      	required: true
		    },
		    founder_email: {
		      	required: true,
		      	email: true
		    },
		    facebook: {
		      	validUrl: true
		    },
		    twitter: {
		      	validUrl: true
		    },
		    linkedin: {
		      	validUrl: true
		    },
		    instagram: {
		      	validUrl: true
		    }
        },
        messages :{
	        founder_name: {
		      	required: 'Enter Founder full name'
		    },
		    founder_email: {
		      	required: 'Enter Founder email'
		    },
	    },
	    submitHandler: function() {


	    	$(".loader-layer").show();
			setTimeout(function(){
		    	$.ajax(
		        {
		            url: "/founder/new",
		            type: "post",
		            datatype: "json",
		            data: new FormData($('#new-founder')[0]),
		            contentType: false,
	    			processData: false
		        }).done(function(data){
		        	$(".loader-layer").hide();
		        	if(data.success == 1){
                        swal({
                          title: "Added",
                          text: "Founder details has been added successfully!",
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "ok!",
                          closeOnConfirm: true
                        },
                        function(){

                          // $("#founder_name").val(data.foundername);
                          $( "#founder_name" ).val( function( index, val ) {
                            return val + data.foundername;
                          });

                          $("#createFounder").modal("toggle");
                          $(".body").addClass("modal-close");
                          $(".body").css("padding-right", "17px");
                          $("#createComp").css("display", "block");
                          $("#createComp").addClass('show');
                          $('#chosen-choices').append('<li class="search-choice"><span>'+ data.foundername +'</span><a class="search-choice-close" data-option-array-index="2"></a></li><li class="search-field"></li>');
                          var rand=$("#rand").val();

                          $(".allfounders").append('<option value='+data.founderid+' selected>'+ data.foundername +'</option>')
             //              $.ajax({
             //               url: "/founder/display",
             //               type: "get",
             //               datatype: "json",
             //                data:{
             //                  rand:rand
             //                },
             //                header:{
             //                  'X-CSRF-TOKEN': '{{ csrf_token() }}'
             //                },
             //
             //              success: function (data) {
             //
             //
             //                alert(data.founder.length);
             //
             //     var s = '';
             //     for (var i = 0; i < data.founder.length; i++) {
             //       s+='<li class="search-choice"><span>Accounting</span><a class="search-choice-close" data-option-array-index="2"></a></li><li class="search-field"></li>'
             //         // s += '<option value="' + data[i].founder[0].founder_id + '">' +data[i].founder[0].founder_id + '</option>';
             //     }
             //     $("#founderss").html(s);
             // }
             //              });

                        });
                    }else{
                        swal("Error", data.msg, "error");
                    }
		        }).fail(function(jqXHR, ajaxOptions, thrownError){
		            $(".loader-layer").hide();
		            //swal("Error", jqXHR.responseJSON.msg, "error");
		            swal("Error", 'A Founder already registered with this email', "error");
		        });
			},1000);
		}
    });

	$(".remove-logo").click(function(){

		$('.company-logo').attr('src', '#');
		$('#company-logo-input').val('');
		$(".inputFilesActions .uploadImg span").text("Upload Image");
    $(".remove-logo").hide();
	});

	$("#createCompBtn").click(function(){
		$("#new-company").trigger("reset");
		$("#new-company label.error").hide();
		$("#createComp").modal("toggle");
	});

  $("#submitRequestBtn").click(function(){

		$("#new-request").trigger("reset");
		$("#new-request label.error").hide();
		$("#submitRequest").modal("toggle");
	});

	//validate new investor
	$("#new-investor1").validate({
        rules: {
            investor_name: {
		      	required: true
		    },
		    investor_email: {
		      	required: true,
		      	email: true
		    },
		    description: {
		      	required: true
		    },
		    website: {
		      	required: true,
		      	validUrl: true
		    },
		    facebook: {
		      	validUrl: true
		    },
		    twitter: {
		      	validUrl: true
		    },
		    linkedin: {
		      	validUrl: true
		    },
		    instagram: {
		      	validUrl: true
		    }
        },
        messages :{
	        investor_name: {
		      	required: 'Enter your full name'
		    },
		    investor_email: {
		      	required: 'Enter your email'
		    },
		    description: {
		      	required: 'Enter description'
		    },
		    website: {
		      	required: 'Enter website'
		    }
	    },
	    submitHandler: function() {
	    	console.log('asdsaddasd');



	    	$(".loader-layer").show();
			setTimeout(function(){
        console.log('asdsaddasd');
		    	$.ajax(
		        {

		            url: "/investor/new",
		            type: "post",
		            datatype: "json",
		            data: new FormData($('#new-investor1')[0]),
		            contentType: false,
	    			processData: false
		        }).done(function(data){
		        	$(".loader-layer").hide();
		        	if(data.success == 1){
                        swal({
                          title: "Added",
                          text: "Investor details has been added successfully!",
                          type: "success",
                          showCancelButton: false,
                          confirmButtonColor: "#DD6B55",
                          confirmButtonText: "ok!",
                          closeOnConfirm: true
                        },
                        function(){
                            window.location.reload();
                        }
                      );
                      window.location.href = "/investors";
                    }else{
                        swal("Error", data.msg, "error");
                    }
		        }).fail(function(jqXHR, ajaxOptions, thrownError){
		            $(".loader-layer").hide();
		            //swal("Error", jqXHR.responseJSON.msg, "error");
		            swal("Error", 'A Investor has already been registered with this email', "error");
		        });
			},1000);
		}
    });

	/*$("#remove-logo").click(function(){
		$('#company-logo').attr('src', '#');
		$('#company-logo-input').val('');
		$(".inputFilesActions .uploadImg span").text("Upload Image");
	});

	$("#createCompBtn").click(function(){
		$("#new-company").trigger("reset");
		$("#new-company label.error").hide();
		$("#createComp").modal("toggle");
	});*/
});

function checkinvestor(){

  var email=$("#investor_email").val();

  $.ajax({
    type:'get',
    url:"/checkemail/"+email,
    datatype:'JSON',
    data:{
    },
    header:{
      'X-CSRF-TOKEN': '{{ csrf_token() }}'
    },
    success:function(data){
      if(data.desc!=''){
        $("#desc").val(data.desc);
      }
      if(data.name!=''){
        $("#name").val(data.name);
      }
      if(data.facebook!=''){
        $("#facebook").val(data.facebook);
      }
      if(data.instagram!=''){
        $("#instagram").val(data.instagram);
      }
      if(data.twitter!=''){
        $("#twitter").val(data.twitter);
      }
      if(data.linkedin!=''){
        $("#linked").val(data.linkedin);
      }
      if(data.website!=''){
        $("#website").val(data.website);
      }
    }
  });


}
function opencompany(){

  $("#createFounder").modal("toggle");

  // $(".body").addClass("modal-open");
  // $(".body").css("padding-right", "17px");
  // $("#createComp").css("display", "block");
  // $("#createComp").addClass('show');
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator, DB;

use App\Models\Country;
use App\Models\Industry;

class HomeCont extends Controller
{
/**
 * @OA\Info(
 *   title="My first API",
 *   version="1.0.0",
 *   @OA\Contact(
 *     email="rohitdalal@code-brew.com"
 *   )
 * )
 */


 /**
 * @OA\Get(
 *     path="/api/data",
 *     tags={"App Data"},
 *     description="App Data",
 *     @OA\Response(response="200", description="Success"),
 *     @OA\Response(response="400", description="Validation Error"),
 *     @OA\Response(response=500, description="Api Error"),
 *     @OA\Response(response=401, description="Unauthorized")
 * )
 */ 
public static function get_data(Request $request)
	{
	try{

        $countries = Country::select('country_id', 'code', 'name')->get();

        $industries = Industry::select("industry_id", "name")->where('is_blocked', false)->orderBy('name', "ASC")->get();


	    return response(['success'=>1, 'statuscode'=>200, 'msg' => 'App data', 'result' => [ 'countries' => $countries, 'industries' => $industries ] ],200);

	    }
	catch(\Exception $e)
	    {
	        return response(['success'=>0, 'statuscode'=>500, 'msg' => $e->getMessage()],500);
	    }
	}



}

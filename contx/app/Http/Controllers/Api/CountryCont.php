<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator, DB;

use App\Models\Country;

class CountryCont extends Controller
{

/**
 * @OA\Get(
 *      path="/api/countries/list",
 *      tags={"Countries"},
 *      summary="Countries Listing",
 *      description="Returns countries list",
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 * )
 * 
 **/
 public static function get_countries(Request $request)
	{
	try{
        $countries = Country::orderBy('name')->get();

	    return response(['success'=>1, 'statuscode'=>200, 'msg' => 'Country data', 'result' => [ 'countries' => $countries ] ],200);

	    }
	catch(\Exception $e)
	    {
	        return response(['success'=>0, 'statuscode'=>500, 'msg' => $e->getMessage()],500);
	    }
	}
    
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator, DB;

use App\Models\Startup;
use Carbon\Carbon;

class StartupCont extends Controller
{

/**
 * @OA\Post(
 *      path="/api/startups/list",
 *      tags={"Startups"},
 *      summary="Startups Listing",
 *      description="Returns startups list",
 *     @OA\Parameter(
 *         description="Page Number",
 *         in="query",
 *         name="page",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Filter by city name",
 *         in="query",
 *         name="filter_city",
 *         required=false,
 *         @OA\Schema(
 *             type="string"
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Filter by Countries Id",
 *         in="query",
 *         name="filter_countires[]",
 *         required=false,
 *         @OA\Schema(
 *             type="array",
 *             @OA\Items(type="number"),
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Filter by Industries Id",
 *         in="query",
 *         name="filter_industries[]",
 *         required=false,
 *         @OA\Schema(
 *             type="array",
 *             @OA\Items(type="number"),
 *         ),
 *         style="form"
 *     ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 * )
 * 
 **/
 public static function get_startups(Request $request)
	{
	try{
        $data = $request->all();

        $validation = Validator::make($data,[
            'page' => 'required'
                ]);
        if($validation->fails())
            return response(array('success'=>0, 'statuscode'=>400, 'msg'=>$validation->getMessageBag()->first()),400);

        $data['take'] = 20;

        $data['skip'] = $data['page']*$data['take'];

        $startups = Startup::startups_list($data);

	    return response(['success'=>1, 'statuscode'=>200, 'msg' => 'Startup data', 'result' => [ 'startups' => $startups ] ],200);

	    }
	catch(\Exception $e)
	    {
	        return response(['success'=>0, 'statuscode'=>500, 'msg' => $e->getMessage()],500);
	    }
	}

/**
 * @OA\Post(
 *      path="/api/startups/add",
 *      tags={"Startups"},
 *      summary="Startup Add New",
 *      description="Returns startup",
 *     @OA\Parameter(
 *         description="Country",
 *         in="query",
 *         name="country_id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="City",
 *         in="query",
 *         name="city_id",
 *         required=false,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Industry",
 *         in="query",
 *         name="industry_id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Name",
 *         in="query",
 *         name="name",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Description",
 *         in="query",
 *         name="description",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Founded In (ex 2013)",
 *         in="query",
 *         name="founded",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Website",
 *         in="query",
 *         name="website",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Facebook",
 *         in="query",
 *         name="facebook",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Instagram",
 *         in="query",
 *         name="instagram",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="Twitter",
 *         in="query",
 *         name="twitter",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *     @OA\Parameter(
 *         description="LinkedIn",
 *         in="query",
 *         name="linkedin",
 *         required=true,
 *         @OA\Schema(
 *             type="string",
 *         ),
 *         style="form"
 *     ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 * )
 * 
 **/
    public static function add_startup(Request $request)
    {
    try{

        $data = $request->all();

        $validation = Validator::make($data,[
            'country_id' => 'required|exists:countries,country_id',
            'city_id'    => 'exists:cities,city_id',
            'industry_id'=> 'required:exists:industries,id',
            'name'       => 'required',
            'description'=> 'required',
            'founded'    => 'required',
            'website'    => 'required|active_url',
            'facebook'   => '',
            'instagram'  => '',
            'twitter'    => '',
            'linkedin'   => ''
                ]);
        if($validation->fails())
            return response(array('success'=>0, 'statuscode'=>400, 'msg'=>$validation->getMessageBag()->first()),400);
        $data['logo']           = '';
        $data['valuation']      = '';
        $data['created_by']     = 'Api';
        $data['ipo_symbol']     = '';
        $data['created_from']   = 'Api';
        $data['city_id']        = isset($data['city_id']) ? $data['city_id'] : 0;

        $startup = Startup::create($data);

        if($startup){
            // link the industry
            DB::table('startup_industries')->insert([
                    'startup_id'    => $startup->startup_id,
                    'industry_id'   => $data['industry_id'],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now()
                ]);
            // link the country
            DB::table('startup_countries')->insert([
                    'startup_id'    => $startup->startup_id,
                    'country_id'    => $data['country_id'],
                    'created_at'    => Carbon::now(),
                    'updated_at'    => Carbon::now()
                ]);
        }

        return response(['success'=>1, 'statuscode'=>200, 'msg' => 'App data', 'result' => ['startup'=>$startup] ],200);

        }
    catch(\Exception $e)
        {
            return response(['success'=>0, 'statuscode'=>500, 'msg' => $e->getMessage()],500);
        }
    }
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator, DB;

use App\Models\Industry;

class IndustryCont extends Controller
{

/**
 * @OA\Get(
 *      path="/api/industries/list",
 *      tags={"Industries"},
 *      summary="Industries Listing",
 *      description="Returns industries list",
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 * )
 * 
 **/
 public static function get_industries(Request $request)
	{
	try{
        $industries = Industry::orderBy('name')->select('industry_id','name')->get();

	    return response(['success'=>1, 'statuscode'=>200, 'msg' => 'Country data', 'result' => [ 'industries' => $industries ] ],200);

	    }
	catch(\Exception $e)
	    {
	        return response(['success'=>0, 'statuscode'=>500, 'msg' => $e->getMessage()],500);
	    }
	}
    
}

<?php

namespace App\Http\Controllers\Api;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Validator, DB;

use App\Models\City;

class CityCont extends Controller
{

/**
 * @OA\Post(
 *      path="/api/cities/list",
 *      tags={"Cities"},
 *      summary="Cities Listing",
 *      description="Returns cities list",
 *     @OA\Parameter(
 *         description="Country",
 *         in="query",
 *         name="country_id",
 *         required=true,
 *         @OA\Schema(
 *             type="integer",
 *             format="int64",
 *         ),
 *         style="form"
 *     ),
 *      @OA\Response(
 *          response=200,
 *          description="successful operation"
 *       ),
 *      @OA\Response(response=400, description="Bad request"),
 *      @OA\Response(response=404, description="Resource Not Found"),
 * )
 * 
 **/
 public static function get_cities(Request $request)
	{
	try{

		$data = $request->all();

        $validation = Validator::make($data,[
            'country_id' => 'required'
                ]);
        if($validation->fails())
            return response(array('success'=>0, 'statuscode'=>400, 'msg'=>$validation->getMessageBag()->first()),400);
        $country_id = $data['country_id'];
        $cities = City::where('country_id',$country_id)->orderBy('name')->get();

	    return response(['success'=>1, 'statuscode'=>200, 'msg' => 'Country data', 'result' => [ 'cities' => $cities ] ],200);

	    }
	catch(\Exception $e)
	    {
	        return response(['success'=>0, 'statuscode'=>500, 'msg' => $e->getMessage()],500);
	    }
	}
    
}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Models\Startup;
use App\Models\Investor;
use App\Models\Founder;
use App\Models\Industry;
use App\Models\Country;
use App\Models\City;
class SitemapController extends Controller
{
    public function index(){

    	$startups = Startup::all()->first();
	    $investors = Investor::all()->first();
	    $founders = Founder::all()->first();

	    return response()->view('sitemap.index', [
	        'startups' 	=> $startups,
	        'investors' => $investors,
	        'founders'  => $founders,
	    ])->header('Content-Type', 'text/xml');

    }

    public function startups(){

        $startups = Startup::latest()->get();
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );
        $countrys=Country::whereIn('name',$latin_countries)->get();
        $citys=City::all();
        $industries=Industry::where('used','1')->get();
        return response()->view('sitemap.startups', [
            'startups' => $startups,
            'countrys'=>$countrys,
            'citys'=>$citys,
            'industries'=>$industries,
        ])->header('Content-Type', 'text/xml');

    }

    public function companies(){

        $startups = Startup::latest()->get();
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );
        $countrys=Country::whereIn('name',$latin_countries)->get();
        // $citys = City::whereIn('country_id',$countrys)->where('is_blocked',0)->orderBy('name')->get();
        $citys=City::all();
        $industries=Industry::where('used','1')->get();

        return response()->view('sitemap.all', [
            'startups' => $startups,
            'countrys'=>$countrys,
            'citys'=>$citys,
            'industries'=>$industries,
        ])->header('Content-Type', 'text/xml');

    }

    public function investors(){

        $investors = Investor::latest()->get();
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );
        $countrys=Country::whereIn('name',$latin_countries)->get();
        $citys=City::all();
        $industries=Industry::where('used','1')->get();
        return response()->view('sitemap.investors', [
            'investors' => $investors,
            'countrys'=>$countrys,
            'citys'=>$citys,
            'industries'=>$industries,
        ])->header('Content-Type', 'text/xml');

    }

    public function founders(){

        $founders = Founder::latest()->get();
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );
        $countrys=Country::whereIn('name',$latin_countries)->get();
        $citys=City::all();
        $industries=Industry::where('used','1')->get();
        return response()->view('sitemap.founders', [
            'founders' => $founders,
            'countrys'=>$countrys,
            'citys'=>$citys,
            'industries'=>$industries,
        ])->header('Content-Type', 'text/xml');

    }

    public function industries(){

        $industries = Industry::latest()->get();

        return response()->view('sitemap.industries', [
            'industries' => $industries,
        ])->header('Content-Type', 'text/xml');

    }

}

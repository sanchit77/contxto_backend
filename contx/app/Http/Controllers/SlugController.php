<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Startup;
use App\Models\Investor;
use App\Models\Founder;

class SlugController extends Controller
{
    public function createStartupSlug(Request $request){
    	// generate the slug for the startups //
    	$startups = Startup::all();

    	foreach($startups as $startup) { 
    		$slug = str_slug($startup->name,'-');
    		$exists = Startup::where('slug',$slug)->where('startup_id','<>',$startup->startup_id)->count();
    		if($exists)
    			$slug.= '-'.$startup->startup_id;
    		Startup::where('startup_id',$startup->startup_id)->update([
    				'slug' => $slug
    			]);
    	}
    }

    public function createInvestorSlug(Request $request){
    	// generate the slug for the investor //
    	$investors = Investor::all();

    	foreach($investors as $investor) { 
    		$slug = str_slug($investor->name,'-');
    		$exists = Investor::where('slug',$slug)->where('investor_id','<>',$investor->investor_id)->count();
    		if($exists)
    			$slug.= '-'.$investor->investor_id;
    		Investor::where('investor_id',$investor->investor_id)->update([
    				'slug' => $slug
    			]);
    	}
    }

    public function createFounderSlug(Request $request){
    	// generate the slug for the founder //
    	$founders = Founder::all();

    	foreach($founders as $founder) { 
    		$slug = str_slug($founder->name,'-');
    		$exists = Founder::where('slug',$slug)->where('founder_id','<>',$founder->founder_id)->count();
    		if($exists)
    			$slug.= '-'.$founder->founder_id;
    		Founder::where('founder_id',$founder->founder_id)->update([
    				'slug' => $slug
    			]);
    	}
    }

    public static function generateAllPending(){
    	// generate for all the pending startups//

    	$startups = Startup::whereNull('slug')->get();
    	foreach($startups as $startup) { 
    		$slug = str_slug($startup->name,'-');
    		$exists = Startup::where('slug',$slug)->where('startup_id','<>',$startup->startup_id)->count();
    		if($exists)
    			$slug.= '-'.$startup->startup_id;
    		Startup::where('startup_id',$startup->startup_id)->update([
    				'slug' => $slug
    			]);
    	}

    	// generate for pending founders //

    	$founders = Founder::whereNull('slug')->get();

    	foreach($founders as $founder) { 
    		$slug = str_slug($founder->name,'-');
    		$exists = Founder::where('slug',$slug)->where('founder_id','<>',$founder->founder_id)->count();
    		if($exists)
    			$slug.= '-'.$founder->founder_id;
    		Founder::where('founder_id',$founder->founder_id)->update([
    				'slug' => $slug
    			]);
    	}


    	// generate for pending investors//


    	$investors = Investor::whereNull('slug')->get();

    	foreach($investors as $investor) { 
    		$slug = str_slug($investor->name,'-');
    		$exists = Investor::where('slug',$slug)->where('investor_id','<>',$investor->investor_id)->count();
    		if($exists)
    			$slug.= '-'.$investor->investor_id;
    		Investor::where('investor_id',$investor->investor_id)->update([
    				'slug' => $slug
    			]);
    	}

    }
}

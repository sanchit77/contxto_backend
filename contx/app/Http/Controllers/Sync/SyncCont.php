<?php

namespace App\Http\Controllers\Sync;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Jobs\SyncCatWeb1;
use App\Jobs\SyncStartupWeb1;
use App\Jobs\SyncStartupWeb2;

use App\Models\Industry;

use Sunra\PhpSimple\HtmlDomParser;

use Buzz\Browser;
use Buzz\Client\FileGetContents;

class SyncCont extends Controller
{

///////////     Sync Website First  /////////////////////////
public static function sync1()
{
try{
    $data = [];

    define('MAX_FILE_SIZE', 1200000); // or larger if needed, default is 600000

    $data['source'] = 'https://lavca.org/vc/startup-directory';
    //$data['source'] = 'http://contxto.netsolutionindia.com/web1';

    // Create DOM from URL or file
    $html = HtmlDomParser::file_get_html($data['source'], false, null, 0);

    $data['categories'] = array();
        if(!empty($html)) 
        {
            foreach($html->find("#directoryFilterOptions2") as $divClass) {
                foreach($divClass->find("li") as $title ) {
                    $data['categories'][] = $title->plaintext;
                }

            }
        }

    $data['categories'] = array_diff($data['categories'], array('All'));

    SyncCatWeb1::dispatch($data['categories'], 'Web1');

    $data['startups'] = [];
    if(!empty($html)) 
    {
        foreach($html->find("#directory-list") as $startUps) {// Startups

            $item = [];
            foreach($startUps->find('li') as $startup_div) {/// Startup Li Loop

            $tname_tag = $startup_div->find('div h1 a')[0]->attr;// Name Website

            /////////// Description     ////////////////
            $tdesc_tag = $startup_div->find('div[class=listorg_description] p');
            if(!empty($tdesc_tag))
                $tdesc = strip_tags($tdesc_tag[0]->innertext);
            else
                $tdesc = '';
            /////////// Description     ////////////////

            ///////////     Foundation  ////////////////
            $tfoundation = $startup_div->find('p[class=listorg_founded]');
            if(!empty($tfoundation))
                {
                    $tfoundation = trim(str_replace("Founded: ", "", $tfoundation[0]->plaintext));
                    //$tfounded = $tfoundation[0]->innerText;
                }
            else
                $tfoundation = '';
            //////////     Foundation  ////////////////

            //////////     Country  ///////////////////
            $tcountry = $startup_div->find('p[class=listorg_country_hq]');
            if(!empty($tcountry))
                {
                $tcountry = trim(str_replace("HQ: ", "", $tcountry[0]->plaintext));
                //$tcountry = preg_replace("/\([^)]+\)/","",$tcountry);
                $tcountry = preg_replace("/\(([^()]*+|(?R))*\)/","", $tcountry);
                }
            else
                $tcountry = '';
            //////////     Country  ///////////////////

            //////////     Target Countries  //////////
            $tcountries = $startup_div->find('p[class=listorg_locations]');
            if(!empty($tcountries))
                {
                $tcountries = trim(str_replace("Locations: ", "", $tcountries[0]->plaintext));

                //$tcountries = preg_replace("/\([^)]+\)/","",$tcountries);
                $tcountries = preg_replace("/\(([^()]*+|(?R))*\)/","", $tcountries);
                $tcountries = explode(", ", $tcountries);
                }
            else
                $tcountries = [];
            //////////     Target Countries  //////////

            //////////     Target Industries  /////////
            $tindustries = $startup_div->find('p[class=listorg_portfolio]');
            if(!empty($tindustries))
                {
                    $tindustries = trim(str_replace("Sectors: ", "", $tindustries[0]->plaintext));

                    $tindustries = explode(", ", $tindustries);
                }
            else
                $tindustries = [];
            //////////     Target Industries  /////////

            //////////     Target Founders  /////////
            $tfounders = $startup_div->find('p[class=listorg_portfolio]')[0]->next_sibling();

            $founder_text_check = trim(html_entity_decode(strip_tags($tfounders->innertext)));
            $temp1 =[];
            if($founder_text_check == "Executive Team:")
                {//////////     Founder Found   /////////////////////

                    $tfounders = $tfounders->next_sibling();  

                    $complete_text = $tfounders->innertext;
            
                    $temp_count = COUNT($tfounders->find("a"));
                    foreach($tfounders->find("a") as $key => $founder) {///////    For Each Founder
        
                    $from = html_entity_decode($founder->outertext).':';

                    if($key == $temp_count-1)
                        $to = '</p>';
                    else
                        $to = '<a href=';

    $sub = substr($complete_text, strpos($complete_text,$from)+strlen($from),strlen($complete_text));
    $roles = trim(html_entity_decode(substr($sub,0,strpos($sub,$to))));
  
    if($roles == "")
        {//Last Founder Role
            $roles = html_entity_decode(substr($complete_text, strpos($complete_text, $sub)));

            $roles = trim($roles);

            $roles = explode("</a>:",$roles);
            $temp_count = COUNT($roles);

            $roles = trim($roles[$temp_count-1]);

        }//Last Founder Role

        // if(html_entity_decode($tname_tag['title']) == 'Aquarela')
        // {
        //     dd($complete_text, strpos($complete_text,$from), strlen($from), strlen($complete_text), $sub, $roles, $temp1);
        // }   

        $roles = strip_tags($roles);
        $roles = str_replace("a>:", "",$roles);
        $roles = str_replace("br>", "",$roles);

                        $temp1[] = [
                            'name' => ucwords(trim($founder->innertext)),
                            'roles' => trim(rtrim($roles, ',')),
                            'link' => isset($founder->attr['href']) ? $founder->attr['href'] : ''
                        ];

                    }///////    For Each Founder

                }//////////     Founder Found   /////////////////////

            //////////     Target Founders  /////////

            //////////     Investors  /////////
            if(COUNT($temp1) > 0)
                {
                    $tinvestors = $tfounders->next_sibling()->next_sibling()->next_sibling();
                }
            else
                {
                    $tinvestors = $tfounders->next_sibling();
                }

            $temp2 =[];
            foreach($tinvestors->find("a") as $tinvestor) {//Each Investors

                $temp2[] = [
                    'name' => trim($tinvestor->innertext),
                    'link' => trim($tinvestor->attr['href']),
                ];
            }//Each Investors

            //////////     Investors  /////////

            //////////      Twitter     ///////
            $twitter = '';

            $ttwitter = $startup_div->find('div[class=lavcabutton lavcabutton-secondary] h3 a[title=Twitter]');
            if(!empty($ttwitter))
                {
                    $ttwitter = $ttwitter[0];
                    $twitter = $ttwitter->attr['href'];
                }
            
            //////////      Twitter     ///////
            
            $data['startups'][] = [
                    'name' => html_entity_decode($tname_tag['title']),
                    'website' => $tname_tag['href'],
                    'description' => $tdesc,
                    'founded' => $tfoundation,
                    'country' => $tcountry,
                    'target_countries' => $tcountries,
                    'industries' => $tindustries,
                    'investors' => $temp2,
                    'founders' => $temp1,
                    'twitter' => $twitter                    
                ];
            }/// Startup Li Loop

        }// Startups

    }

    SyncStartupWeb1::dispatch($data['startups'], 'Web1');

    echo "Command executed successfully";

    return \Response::json(["success"=>1, 'data' => $data], 200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage(), 'data' => $data ),200);
    }
}

//////////////////          Sync Website 2  ////////////////////////
public static function sync2($id) {
try{
    $data = [];

    define('MAX_FILE_SIZE', 1200000); // or larger if needed, default is 600000

    if($id == 2)
        {
            $data['source'] = 'https://mexico.startups-list.com/';
        }
    elseif($id == 3)
        {
            $data['source'] = 'https://santiago.startups-list.com/';
        }
    elseif($id == 4)
        {
            $data['source'] = 'https://sao-paulo.startups-list.com/';
        }
    else
        {
            $data['source'] = 'https://buenos-aires.startups-list.com/';
        }
    //$data['source'] = 'http://contxto.netsolutionindia.com/web2';
    //https://pastebin.com/5PstJyxA

    $context = stream_context_create(array('http' => array('header' => 'User-Agent: Mozilla compatible')));
    $response = HtmlDomParser::file_get_html($data['source'], false, $context, 0);
    $html = HtmlDomParser::str_get_html($response);

    $data['categories'] = array();
    $data['categories_dval'] = [];

    foreach($html->find("nav[class=categories] a") as $key => $divClass) {

        if(!array_key_exists("data-id",$divClass->attr))
            continue;

        $name = html_entity_decode(strip_tags(trim($divClass->innertext)));
        $did = html_entity_decode(strip_tags(trim($divClass->attr['data-id'])));

        $data['categories'][] = $name;

        $data['categories_dval'][$did] = $name;
    
    }

        $data['categories'] = array_unique($data['categories']);
        SyncCatWeb1::dispatch($data['categories'], 'Web2');
    ////////////////    Categories  /////////////////////////////

    $data['startups'] = [];
    foreach($html->find("div[id=wrap] div[class=startup]") as $key => $startupDivs) {

        $name = html_entity_decode(strip_tags(trim($startupDivs->attr['data-name'])));
        $website = html_entity_decode(strip_tags(trim($startupDivs->attr['data-href'])));

        ///////////     Categories  ////////////////
        $categories = [];
        $atts = $startupDivs->attr['class'];//Atts
        $cats = explode(" ", trim(str_replace("card startup","",$atts)));

        foreach($cats as $cat)
            {
                if(!array_key_exists($cat, $data['categories_dval']))
                    continue;

                $categories[] = $data['categories_dval'][$cat];
                
            }
        ///////////     Categories  ////////////////

        /////////       Image   ////////////////////
        $imageTag = $startupDivs->find("a[class=main_link] img")[0];
        $image = $imageTag->attr['src'];

        if($image == "/img/spacer.gif")
            $image = $imageTag->attr['data-src'];
        /////////       Image   ////////////////////

        /////////       Description     ////////////
        $descTag = $startupDivs->find("p")[0];
        $description = trim(html_entity_decode(strip_tags($descTag->innertext)));
        /////////       Description     ////////////

        /////////       Social Links    ////////////
        //$imageTag = $startupDivs->find("a[class=social]")[0];
        $twitter = '';
        $facebook = '';
        $linkedin = '';
        $instagram = '';
        foreach($startupDivs->find("div[class=social] a") as $key1 => $socialLinks) {

            $favicon = trim($socialLinks->innertext);

            if($favicon == '<i class="fa fa-globe"></i>')
                $website = $socialLinks->attr['href'];
            elseif($favicon == '<i class="fa fa-twitter"></i>')
                $twitter = $socialLinks->attr['href'];
            elseif($favicon == '<i class="fa fa-facebook-square"></i>')
                $facebook = $socialLinks->attr['href'];

        }
    /////////       Social Links    ////////////

        $data['startups'][] = [
                'name' => $name,
                'image' => $image,
                'description' => $description,
                'industries' => $categories,
                'website' => $website,
                'twitter' => $twitter,
                'facebook' => $facebook,
                'linkedin' => $linkedin,
                'instagram' => $instagram
            ];

    }

    if($data['source'] == 'https://mexico.startups-list.com/')
        $source = "Web2";
    elseif($data['source'] == 'https://santiago.startups-list.com/')
        $source = "Web3";
    elseif($data['source'] == 'https://sao-paulo.startups-list.com/')
        $source = "Web4";
    else
        $source = "Web5";//https://buenos-aires.startups-list.com/

    SyncStartupWeb2::dispatch($data['startups'], $source);

    unset($data['categories_dval']);

    echo "Command Web{$id} sync executed successfully";

    return \Response::json(["success"=>1, 'data' => $data], 200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage(), 'data' => $data ),200);
    }
}

////////////////////        Angel Web       .////////////////////////////
public static function webAngel(Request $request)
{
    //$
}

////////////////////        Sync Angel      /////////////////////////////
public static function sync_angel(Request $request)
{
//try{

    $data = $request->all();
    $url = "https://angel.co/mexico?page=1";


    $client = new \GuzzleHttp\Client();
    $response = $client->request('GET', $url, [
        'headers' => [
            'X-Requested-With' => 'XMLHttpRequest',
            'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36',
            'allow_redirects' => false
            // 'User-Agent' => 'testing/1.0',
            // 'Accept'     => 'application/json',
            // 'X-Foo'      => ['Bar', 'Baz']
        ]
    ]);

    // "X-Requested-With: XMLHttpRequest",
    // "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36"

    print_r($response);


    exit;


    $ch = curl_init($url);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
    curl_setopt($ch, CURLOPT_HEADER, 0);
    curl_setopt($ch, CURLOPT_TIMEOUT, 15);
    curl_setopt($ch,CURLOPT_USERAGENT, 'Mozilla/5.0 (Windows NT 6.1; WOW64) AppleWebKit/537.17 (KHTML, like Gecko) Chrome/24.0.1312.52 Safari/537.17)');
    curl_setopt($ch, CURLOPT_FRESH_CONNECT, TRUE);
    $curl_scraped_page = curl_exec($ch);

dd($curl_scraped_page);

    exit;
    $options = array( 
	    CURLOPT_RETURNTRANSFER => true, // to return web page
        CURLOPT_HEADER         => true, // to return headers in addition to content
        CURLOPT_FOLLOWLOCATION => true, // to follow redirects
        //CURLOPT_ENCODING       => "",   // to handle all encodings
        //CURLOPT_AUTOREFERER    => true, // to set referer on redirect
        CURLOPT_CONNECTTIMEOUT => 120,  // set a timeout on connect
        CURLOPT_TIMEOUT        => 120,  // set a timeout on response
        //CURLOPT_MAXREDIRS      => 10,   // to stop after 10 redirects
        //CURLINFO_HEADER_OUT    => true, // no header out
        //CURLOPT_SSL_VERIFYPEER => false,// to disable SSL Cert checks
        CURLOPT_HTTP_VERSION   => CURL_HTTP_VERSION_1_1,
        );
    $handle = curl_init( $url );
 
    curl_setopt($handle, CURLOPT_HTTPHEADER, array(
        "X-Requested-With: XMLHttpRequest",
        "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36"
        )
    );
 
    curl_setopt_array( $handle, $options );
 
    // additional for storing cookie 
        $tmpfname = storage_path().'/logs/cookie.txt';
        //dirname(__FILE__).'/cookie.txt';
        //dd($tmpfname, storage_path().'/logs/cookie.txt');

        curl_setopt($handle, CURLOPT_COOKIEJAR, $tmpfname);
        curl_setopt($handle, CURLOPT_COOKIEFILE, $tmpfname);

        $raw_content = curl_exec( $handle );
        $err = curl_errno( $handle );
        $errmsg = curl_error( $handle );
        $header = curl_getinfo( $handle ); 
        curl_close( $handle );
 
//dd($handle, $raw_content, $err, $errmsg);

        $header_content = substr($raw_content, 0, $header['header_size']);
        $body_content = trim(str_replace($header_content, '', $raw_content));
    
    // let's extract cookie from raw content for the viewing purpose         
        $cookiepattern = "#Set-Cookie:\\s+(?<cookie>[^=]+=[^;]+)#m"; 
        preg_match_all($cookiepattern, $header_content, $matches); 
        $cookiesOut = implode("; ", $matches['cookie']);

        $header['errno']   = $err;
        $header['errmsg']  = $errmsg;
        $header['headers']  = $header_content;
        $header['content'] = $body_content;
        $header['cookies'] = $cookiesOut;
    return $header;




    exit;

    $headers = [
        'Accept: */*',
        'Content-Type: application/x-www-form-urlencoded',
        'Custom-Header: custom-value',
        'Custom-Header-Two: custom-value-2'
    ];
    
    // open connection
    $ch = curl_init();
    
    // set curl options
    $options = [
        CURLOPT_URL => $url,
        //CURLOPT_POST => count($data),
        //CURLOPT_POSTFIELDS => http_build_query($data),
        CURLOPT_HTTPHEADER => $headers,
        CURLOPT_RETURNTRANSFER => true,
    ];
    curl_setopt_array($ch, $options);
    
    // execute
    $result = curl_exec($ch);
    
    // close connection
    curl_close($ch);




    exit;

    $useragent = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/67.0.3396.99 Safari/537.36';

    $headerOptions = array(
        ':authority: flight-pricing.maxmilhas.com.br',
        ':method: OPTIONS',
        ':path: /search?time='.$epochtime,
        ':scheme: https',
        'accept: */*',
        'accept-encoding: gzip, deflate, br',
        'accept-language: pt-BR,pt;q=0.9,en-US;q=0.8,en;q=0.7',
        'access-control-request-headers: authorization,content-type',
        'access-control-request-method: POST',
        'origin: https://www.maxmilhas.com.br',
        'user-agent: '.$useragent
    );
    
    $url = 'https://flight-pricing.maxmilhas.com.br/search?time='.$epochtime;
    
    $curl = curl_init();
        curl_setopt($curl, CURLOPT_USERAGENT, $useragent );
        curl_setopt($curl, CURLOPT_AUTOREFERER, true);
        curl_setopt($curl, CURLINFO_HEADER_OUT, true);
        curl_setopt($curl, CURLOPT_COOKIEJAR, "C:/wamp64/www/maxmilhas/tmp/cookieoptions.txt");
        curl_setopt_array($curl, array(
              CURLOPT_URL => $url,
              CURLOPT_RETURNTRANSFER => true,
              CURLOPT_ENCODING => "",
              CURLOPT_MAXREDIRS => 10,
              CURLOPT_TIMEOUT => 30,
              CURLOPT_CUSTOMREQUEST => "OPTIONS",
              CURLOPT_HTTPHEADER => $headerOptions  
        ));
    
        $response = curl_exec($curl);
        $err = curl_errno($curl);
        file_put_contents('log/headerout/options.txt',curl_getinfo($curl, CURLINFO_HEADER_OUT ));
    
    curl_close($curl);
    
    if ($err) {
        echo "cURL -OPTIONS Request- Error #:" . $err;
    }
    
    file_put_contents('log/options.txt',$response);

















    $response = \Httpful\Request::get($url)
    //->xExampleHeader("My Value")                // Add in a custom header X-Example-Header
    //->withXAnotherHeader("Another Value")       // Sugar: You can also prefix the method with "with"
    //->addHeader('X-Or-This', 'Header Value')    // Or use the addHeader method
    ->addHeaders(array(
        'CURLOPT_USERAGENT' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36',              // Or add multiple headers at once
        'X-Requested-With' => 'XMLHttpRequest',              // in the form of an assoc array

        'User-Agent' => 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36',

        'Accept' => 'XMLHttpRequest'
    ))
    ->send();

    print_r($response);
    //exit;
    dd($response);




    $cu = curl_init();
    curl_setopt($cu, CURLOPT_URL, $url);
    curl_setopt($cu, CURLOPT_SSL_VERIFYPEER, false);
    curl_setopt($cu, CURLOPT_SSL_VERIFYHOST, false);
    curl_setopt ($cu, CURLOPT_USERAGENT, "Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36");
    //curl_setopt($cu, CURLOPT_REFERER, "http://www.tjreb.com");
    //curl_setopt($cu, CURLOPT_HEADER, true);
//curl_setopt($cu, CURLOPT_FOLLOWLOCATION, false);
curl_setopt($cu, CURLOPT_RETURNTRANSFER, TRUE);
    $co = curl_exec($cu) ;


print_r($co);
exit;

    //     $browser = $_SERVER['HTTP_USER_AGENT'];
    //     $ch = curl_init();
        
    //     $header[0] = "Accept: text/xml,application/xml,application/xhtml+xml,";
    //     $header[0] .= "text/html;q=0.9,text/plain;q=0.8,image/png,*/*;q=0.5";
    //     $header[] = "Cache-Control: max-age=0";
    //     $header[] = "Connection: keep-alive";
    //     $header[] = "Keep-Alive: 300";
    //     $header[] = "Accept-Charset: ISO-8859-1,utf-8;q=0.7,*;q=0.7";
    //     $header[] = "Accept-Language: en-us,en;q=0.5";
    //     $header[] = "Pragma: "; // browsers keep this blank.
        
    //     curl_setopt($ch, CURLOPT_URL, $url);
    //     curl_setopt($ch, CURLOPT_USERAGENT, $browser);
    //     curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    //     curl_setopt($ch, CURLOPT_REFERER, $ref);
    //     curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
    //     curl_setopt($ch, CURLOPT_AUTOREFERER, false);
    //     $html = curl_exec($ch);
    //     curl_close ($ch);
    //     return $html;
    // dd(1);

    $ch = curl_init();
    //$url = urlencode("https://angel.co/mexico?page=10");

    $header=array(
        'User-Agent: Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.9.2.12) Gecko/20101026 Firefox/3.6.12',
        'X-Requested-With: XMLHttpRequest'
    );
    
    curl_setopt($ch,CURLOPT_URL,$url);
    // curl_setopt($ch,CURLOPT_RETURNTRANSFER,true);
    // curl_setopt($ch,CURLOPT_COOKIEFILE,'cookies.txt');
    // curl_setopt($ch,CURLOPT_COOKIEJAR,'cookies.txt');
    curl_setopt($ch,CURLOPT_HTTPHEADER,$header);
    
    $result=curl_exec($ch);
    $err = curl_error($ch);
    
    curl_close($ch);

    print_r("dadawdawd awdaw dwdawdawd");
    var_dump($result, $err);
    exit;


    ////////////        Curl Request    //////////////////
    $curl = new \Curl\Curl();
    $curl->setUserAgent('Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36');
    //$curl->setReferrer('');
    $curl->setHeader('X-Requested-With', 'XMLHttpRequest');
    //$curl->setCookie('key', 'value');
    $curl->get('https://angel.co/mexico?page=1');
    
    if ($curl->error) {
        echo 'Error ';
        echo $curl->error_code;

        echo $curl->error;

        var_dump($curl->request_headers);
        var_dump($curl->response_headers);

    }
    else {
        echo $curl->response;
    }


    exit;

    // $curl = curl_init();

    // curl_setopt_array($curl, array(
    //   CURLOPT_URL => "https://angel.co/mexico?page=1",
    //   CURLOPT_RETURNTRANSFER => true,
    //   CURLOPT_ENCODING => "",
    //   CURLOPT_MAXREDIRS => 10,
    //   CURLOPT_TIMEOUT => 30,
    //   CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
    //   CURLOPT_CUSTOMREQUEST => "GET",
    //   CURLOPT_HTTPHEADER => array(
    //     "Cache-Control: no-cache",
    //     "User-Agent: Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/50.0.2661.75 Safari/537.36",
    //     "X-Requested-With: XMLHttpRequest"
    //   ),
    // ));
    // $response = curl_exec($curl);
    // $err = curl_error($curl);
    
    // curl_close($curl);
    
    // if ($err) {
    //   echo "cURL Error #:" . $err;
    //   exit;
    // }
    ////////////        Curl Request    //////////////////

    if(!isset($response->html))
        {
            echo "HTML NOT FOUND";
            echo $response;
            exit;
        }


    $html = HtmlDomParser::str_get_html($response->html);

    //$data['html'] = $html->innertext;
    //$data['companies_html'] = [];
    $data['startups'] = [];

    foreach($html->find("div[class='dts27 frw44 _a _jm']") as $key => $divClass) {

        //$data['companies_html'][] = $divClass->innertext;

        ///////////////////         Image ////////////////////
        $img = $divClass->find("img[class=angel_image]")[0];
        $img_atts = html_entity_decode($img->attr['src']);//Image
        ///////////////////         Image ////////////////////

        ///////////////////     Name, Link, Id ////////////////
        $name_anchor = $divClass->find("div[class=name] a")[0];

        $temp_link = html_entity_decode($name_anchor->attr['href']);// Link
        $temp_id = html_entity_decode($name_anchor->attr['data-id']);// Id

        $name = html_entity_decode(strip_tags($name_anchor->innertext));// Name
        ///////////////////     Name, Link, Id ////////////////

        ///////////////////     Description     ///////////////
        $temp_dec = $divClass->find("div[class=blurb]")[0];

        $desc = html_entity_decode(strip_tags($temp_dec->innertext));
        $desc = str_replace("\"", "", $desc);

        ////////////        City Category   ///////////////////
        $temp_city = $divClass->find("div[class=tags]")[0];

        $city = "";
        $category = "";
        foreach($html->find("div[class=tags] a") as $key1 => $tag) {

            if($key1 == 0)
                $city = $tag->innertext;
            else
                $category = $tag->innertext;
        }

        //dd($temp_city->innertext);
        

        //dd($divClass->innertext);

        // if(!array_key_exists("data-id",$divClass->attr))
        //     continue;

        // $name = html_entity_decode(strip_tags(trim($divClass->innertext)));
        // $did = html_entity_decode(strip_tags(trim($divClass->attr['data-id'])));

        // $data['categories'][] = $name;

        // $data['categories_dval'][$did] = $name;

        $data['startups'][] = [
            'angel_id' => $temp_id,
            'name' => $name,
            'link' => $temp_link,
            'image' => $img_atts,
            'desc' => $desc,
            'city' => $city,
            'category' => $category
        ];
    
    }



    return \Response::json(["success"=>1, 'data' => $data], 200);
    

//     }
// catch(\Exception $e)
//     {
//         return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage(), 'data' => $data ),200);
//     }
}

////////////////////

}

<?php

namespace App\Http\Controllers\Sync;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Http\Controllers\CommonController;

use App\Jobs\StartupWeb1;// Insert Update Startup Queue Web1
use App\Jobs\StartupWeb2;// Insert Update Startup Queue Web2, 3, 4, 5

use App\Models\Industry;
use App\Models\Investor;
use App\Models\Country;
use App\Models\City;
use App\Models\Startup;
use App\Models\Founder;

use App\Models\StartupIndustry;
use App\Models\StartupInvestor;
use App\Models\StartupCountry;
use App\Models\StartupFounder;

class SyncDbCont extends Controller
{
///////////////     Update Industries    ////////////////////////
public static function update_industries($industries, $source)
{
try{

    $insert_array = [];
    $temp_len =COUNT($industries);

    foreach($industries as $key => $industry) {

        $industry = Industry::where('name', $industries[$key])->first();
        if(!$industry)
            {
                $insert_array[] = [
                    'name' => $industries[$key],
                    'source' => $source
                ];
            }

    }
    //\Log::info($insert_array);

    if(COUNT($insert_array))
        Industry::insert($insert_array);

    return 'Industries updated successfully '.$source;

    }
catch(\Exception $e)
    {
        return $e;
        //return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

////////////////        Sync Startups Web1      ////////////////////////
public static function update_startups($startups, $source)
{
try{
    
    $temp_len =COUNT($startups);

    $seconds = 1;
    foreach($startups as $key => $startup) {

        StartupWeb1::dispatch($startup)
        ->delay(now()->addSeconds($seconds));

        $seconds = $seconds+1;
        if($seconds > 99)
            $seconds = 1;
    }

    return 'Web1 Startup update job dispatched successfully';

    }
catch(\Exception $e)
    {
        return $e;
        //return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

//////////////      CRUD Startup Web1   ////////////////////////////
public static function startup_web1_crud($startup)
{
try{
    $data = [];

    //\Log::info(json_encode($startup));

    $global_countries = SyncDbCont::countries_data();

    $startup['country_id'] = isset($global_countries[$startup['country']]) ? $global_countries[$startup['country']] : 0;//Country Id

	$startup['created_by'] = "Cron";
	$startup['created_from'] = "Web1";

    $old_startup = Startup::where('name',$startup['name'])->first();
    if(!$old_startup)
        $old_startup = Startup::create_startup($startup);
    else
        {
            return 'Startup Web1 CRUD success';
            $startup['startup_id'] = $old_startup->startup_id;
            Startup::update_startup($startup, $old_startup);
        }

    $data['startup_id'] = $old_startup->startup_id;

    /////////////       Target Countries    ////////////////////////////
    $insert_target_country = [];

    foreach($startup['target_countries'] as $key => $target_country)
        {
        $temp_country_id = isset($global_countries[$target_country]) ? $global_countries[$target_country] : 0;

        if($temp_country_id == 0)
            continue;

        $check_target = StartupCountry::where('startup_id', $data['startup_id'])->where('country_id', $temp_country_id)->first();
        if(!$check_target)
            $insert_target_country[] = [
                'startup_id' => $data['startup_id'],
                'country_id' => $temp_country_id
            ];

        }

    if(COUNT($insert_target_country) > 0)
        StartupCountry::insert($insert_target_country);
    /////////////       Target Countries    ////////////////////////////

    /////////////       Industries    //////////////////////////////////
    $insert_industries = [];
    
    $industries_data = SyncDbCont::industries_data();

    foreach($startup['industries'] as $key => $startup_industry)
    {

    $industry_id = isset($industries_data[$startup_industry]) ? $industries_data[$startup_industry] : 0;

    if($industry_id == 0)
        $industry_id = Industry::create($startup_industry, 'Web1');

    $check_industry = StartupIndustry::where('startup_id', $data['startup_id'])->where('industry_id', $industry_id)->first();
    if(!$check_industry)
        $insert_industries[] = [
            'startup_id' => $data['startup_id'],
            'industry_id' => $industry_id,
            'source' => 'Web1'
        ];

    }

    if(COUNT($insert_industries) > 0)
        StartupIndustry::insert($insert_industries);
    /////////////       Industries    //////////////////////////////////

    /////////////       Investors    ///////////////////////////////////
    $insert_investors = [];

    foreach($startup['investors'] as $key => $startup_investor)
    {
    
    $investor = Investor::where('name', $startup_investor['name'])->first();
    if(!$investor)
        $investor = Investor::create($startup_investor['name'], 0, 'Cron');

    $check_investor = StartupInvestor::where('startup_id', $data['startup_id'])->where('investor_id', $investor->investor_id)->first();
    if(!$check_investor)
        $insert_investors[] = [
            'startup_id' => $data['startup_id'],
            'investor_id' => $investor->investor_id,
            'source' => 'Web1',
            'investor_link' => $startup_investor['link']
        ];
    else
        {
            StartupInvestor::where('startup_investor_id',$check_investor->startup_investor_id)->update([
                'investor_link' => $startup_investor['link']
            ]);
        }

    }
    if(COUNT($insert_investors) > 0)
        StartupInvestor::insert($insert_investors);

    /////////////       Investors    ///////////////////////////////////

    /////////////       Founders    ////////////////////////////////////
    $insert_founders = [];

    foreach($startup['founders'] as $key => $startup_founder)
        {

        $founder = Founder::where('name', $startup_founder['name'])->first();
        if(!$founder)
            $founder = Founder::create($startup_founder['name'], 0, 'Cron');

        $check_founder = StartupFounder::where('startup_id', $data['startup_id'])->where('founder_id', $founder->founder_id)->first();
        if(!$check_founder)
            $insert_founders[] = [
                'startup_id' => $data['startup_id'],
                'founder_id' => $founder->founder_id,
                'source' => 'Web1',
                'roles' => $startup_founder['roles'],
                'founder_link' => $startup_founder['link']
            ];
        else
            {
                StartupFounder::where('startup_founder_id', $check_founder->startup_founder_id)->update([
                    'roles' => $startup_founder['roles'],
                    'founder_link' => $startup_founder['link']
                ]);
            }

        }

    if(COUNT($insert_founders) > 0)
        StartupFounder::insert($insert_founders);
    /////////////       Founders    ////////////////////////////////////

        return 'Startup Web1 CRUD success';
    }
catch(\Exception $e)
    {
        return $e;
        //return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

//////////////      Countries With Name and ids     ////////////////////
public static function countries_data()
{

    $countries = Country::select('country_id', 'name')->get();

    $temp_countries = [];
    $temp_count = COUNT($countries);

    for($i0=0;$i0<$temp_count;$i0++)
        {
            $temp_countries[$countries[$i0]->name] = $countries[$i0]->country_id;
        }

    return $temp_countries;

}

//////////////      Investors With Name and ids     ////////////////////
public static function investors_data()
{

    $investors = Investor::select('investor_id', 'name')->get();

    $temp_investors = [];
    $temp_count = COUNT($investors);

    for($i0=0;$i0<$temp_count;$i0++)
        {
            $temp_investors[$investors[$i0]->name] = $investors[$i0]->investor_id;
        }

    return $temp_investors;

}

//////////////      Industries WIth Name and Ids    ////////////////////
public static function industries_data()
{
    $industries = Industry::select('industry_id', 'name')->get();

    $temp_industries = [];
    $temp_count = COUNT($industries);

    for($i0=0;$i0<$temp_count;$i0++)
        {
            $temp_industries[$industries[$i0]->name] = $industries[$i0]->industry_id;
        }

    return $temp_industries;

}

////////////////        Sync Startups Web2      ////////////////////////
public static function update_startups_web2($startups, $source)
{
try{
    
    $temp_len =COUNT($startups);

    $seconds = 1;
    foreach($startups as $key => $startup) {

        StartupWeb2::dispatch($startup, $source)
        ->delay(now()->addSeconds($seconds));

        $seconds = $seconds+1;
        if($seconds > 99)
            $seconds = 1;
    }

    return $source.' Startup update job dispatched successfully';

    }
catch(\Exception $e)
    {
        return $e;
        //return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}
//////////////      CRUD Startup Web2   ////////////////////////////
public static function startup_web2_crud($startup, $source)
{
try{
    $data = [];

    //////////      Country City    ///////////////////////
    if($source == "Web2")
        {
            $startup['country'] = "Mexico";
        }
    elseif($source == "Web3")
        {
            $startup['country'] = "Chile";
            $startup['city'] = "Santiago";
        }
    elseif($source == "Web4")
        {
            $startup['country'] = "Brazil";
            $startup['city'] = "São Paulo";
        }
    else
        {
            $startup['country'] = "Argentina";
            $startup['city'] = "Buenos Aires";
        }
    //////////      Country City    ///////////////////////

    $global_countries = SyncDbCont::countries_data();

    $startup['created_by'] = "Cron";
    $startup['created_from'] = $source;
    
    $startup['country_id'] = isset($global_countries[$startup['country']]) ? $global_countries[$startup['country']] : 0;//Country Id

    /////////////       City    ///////////////////////////////
    if(isset($startup['city']) && $startup['city'] != '')
        {
            $cityCheck = City::where('name', $startup['city'])->where('country_id', $startup['country_id'])->first();
            if($cityCheck)
                $startup['city_id'] = $cityCheck->city_id;
            else
                $startup['city_id'] = City::create($startup['city'], $startup['country_id']);
        }
    else
        {
            $startup['city_id'] = 0;
        }
    
    $startup['logo'] = CommonController::save_image_from_url($startup['image']);
    //$startup['logo'] = ($startup['logo'] == '') ? '' : env('IMAGE_URL').$startup['logo'];

    $old_startup = Startup::where('name',$startup['name'])->first();
    if(!$old_startup)
        $old_startup = Startup::create_startup($startup);
    else
        {
            return 'Startup Web2 CRUD success';

			if(($startup['logo'] != '') && $old_startup->logo != "" && file_exists('Uploads/'.$old_startup->logo) )
				$con = unlink('Uploads/'.$old_startup->logo);

            $startup['startup_id'] = $old_startup->startup_id;
            
            $startup['logo'] = ($startup['logo'] != '') ? $startup['logo'] : $old_startup->logo;

            $startup['city_id'] = ($startup['city_id'] != 0) ? $startup['city_id'] : $old_startup->city_id;

            Startup::update_startup($startup, $old_startup);
        }
    
    $data['startup_id'] = $old_startup->startup_id;

    /////////////       Industries    //////////////////////////////////
    $insert_industries = [];
    
    $industries_data = SyncDbCont::industries_data();

    foreach($startup['industries'] as $key => $startup_industry)
    {

    $industry_id = isset($industries_data[$startup_industry]) ? $industries_data[$startup_industry] : 0;

    if($industry_id == 0)
        $industry_id = Industry::create($startup_industry, $source);

    $check_industry = StartupIndustry::where('startup_id', $data['startup_id'])->where('industry_id', $industry_id)->first();
    if(!$check_industry)
        $insert_industries[] = [
            'startup_id' => $data['startup_id'],
            'industry_id' => $industry_id,
            'source' => $source
        ];

    }

    if(COUNT($insert_industries) > 0)
        StartupIndustry::insert($insert_industries);
    /////////////       Industries    //////////////////////////////////

    return "Startup ".$source." CRUD success";

    }
catch(\Exception $e)
    {
        return $e;
        //return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

//////////////      Test Api    ///////////////////////
public function test(Request $request)
{
try{

    $data = $request->all();

    $source = "Web1";
    $startup = json_decode('{
        "name": "Properati",
        "image": "https://d1qb2nb5cznatu.cloudfront.net/startups/i/144469-cee7bc432edb02b87508b9120b6fba86-thumb_jpg.jpg?buster=1354803475",
        "description": "The Real Estate Market Place for Latam  Properati.com is a real estate market place for Latam that provides insightful data to help users take better decisions.",
        "industries": [
            "Real Estate"
        ],
        "website": "http://www.properati.com",
        "twitter": "http://twitter.com/properati",
        "facebook": "",
        "linkedin": "",
        "instagram": ""
    }', true);

    //https://mexico.startups-list.com/         //https://pastebin.com/5PstJyxA
    //https://santiago.startups-list.com/       //https://pastebin.com/ZaD3137e
    //https://sao-paulo.startups-list.com/      //https://pastebin.com/sh7xPbdM
    //https://buenos-aires.startups-list.com/   //https://pastebin.com/LN5RYfJu


    return \Response::json([ 'old_startup' => $old_startup, 'data'=>$data, 'startup' => $startup, 'source' => $source ], 200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}
}

<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use App\Models\Founder;
use App\Models\StartupFounder;
class FounderCont extends Controller
{
///////////     Index       ////////////////////////
public static function index(Request $request) {
    return \View::make('Admin.Founders.index');
}

///////////     Ajax Data   ////////////////////////
public static function data_ajax(Request $request) {
try{
    $data = $request->all();

    if(!isset($data['start']))
        $data['start'] = 0;

    if(!isset($data['length']))
        $data['length'] = 10;

    $recordsTotal = Founder::count();

    $founders = Founder::ajax_data($data);
    // return response()->json(array('part'=>$founders));
    $founders = json_decode(json_encode($founders), true);
     // return response()->json(array('part'=>$founders));
    $json_data  = array(
        //'data_received' => $data,
        "draw"=> intval($data['draw']),
        "recordsTotal" => $recordsTotal,
        "recordsFiltered" => $recordsTotal,
        "founders" => $founders,
    );

    return \Response::json($json_data, 200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

//////////////      Ajax Update Name    /////////////////////
public static function ajax_update(Request $request) {
try{

    $data = $request->all();

    $rules = array(
        'fBtnId' => 'required',
        'founder_id' => 'required|exists:founders,founder_id',
        'name' => 'required|unique:founders,name,'.$data['founder_id'].',founder_id'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    Founder::where('founder_id', $data['founder_id'])->update([
        'founder_id' => $data['founder_id'],
        'name' => $data['name']
    ]);

    $data['msg'] = 'Founder updated successfully';

    $new_html = '<button class="btn btn-default" id="'.$data['fBtnId'].'" onClick ="update_name(this.id)" title="'.$data['name'].'">'.$data['name'].'</button>';//0

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'], 'new_html' => $new_html ),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

//////////////      Startups Listing   //////////////////
public static function startups(Request $request)
    {
    try{

        $data = $request->all();

        $rules = array(
            'founder_id' => 'required|exists:founders,founder_id',
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

        $founder = Founder::where('founder_id', $data['founder_id'])->first();

        return \View::make('Admin.Founders.startupIndex', compact('founder'));

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

    public function delete_ajax(Request $request){
      try{
        $id=$request->founder_id;

        $rules=array(
          'founder_id'=>'required|exists:founders,founder_id',
        );
        $validator=\Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>'0','status_code'=>400,'msg'=>$validator->getMessageBag()->first()),200);

        $founder=Founder::where('founder_id',@$id)->delete();
        $founder=StartupFounder::where('founder_id',@$id)->delete();
        return \Response::json(array('success'=>'1','status_code'=>200,'msg'=>'Deleted'),200);
      }
      catch(\Exception $e){
            return \Response::json(array('success'=>'0','status_code'=>500,'msg'=>$e->getMessage()),200);
      }
      }


//////////////

}

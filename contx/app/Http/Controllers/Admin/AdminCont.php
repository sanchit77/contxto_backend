<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Startup;
use Illuminate\Http\Response;
use Carbon\Carbon;
use Validator;
use DB;
use Mail;
use Illuminate\Support\Facades\Input;
use View;
use Redirect;
use Illuminate\Cookie\CookieJar;
use Request as ORequest;
use App\Models\Industry;
use App\Models\Investor;
use App\Models\Founder;


use App\Models\Admin;

use App\Http\Controllers\CommonController;

class AdminCont extends Controller
{
//////////////		Admin Login Get 	///////////////
public static function admin_login_get()
	{
		return View::make('Admin.Other.login');
	}

    //////////////		Admin Login Post 	///////////////
public static function admin_login_post(Request $request)
	{
	try{

		$rules = array(
			'email'=>'required|email|exists:admins,email',
			'password'=>'required',
			'timezone'=>'required|timezone');

		$validator = Validator::make($request->all(),$rules);
		if($validator->fails())
			return Redirect::back()->witherrors($validator->getMessageBag()->first())->withInput();

		$admin = Admin::where('email', $request['email'])->first();
		// if(!\Hash::Check($request['password'], $admin->password))
		// 	return Redirect::back()->witherrors('Password is Incorrect')->withInput();

		$time = new \DateTime('now', new \DateTimeZone($request['timezone']));
		$request['timezonez'] = $time->format('P');

		$request['admin_id'] = $admin->admin_id;
		$request['ip_address'] = $request->ip();

		\Cookie::queue('adminAccessToken',  str_random(100) , 60);
		\Cookie::queue('admintz', $request['timezonez'] , 60);
		\Cookie::queue('admintId', $admin->admin_id , 60);

		Admin::super_admin_update($request->all(),$admin);

		return Redirect::route('admin_dashboard')->with('successMsg','Logged In Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////			Admin Dash Board 	///////////////////////
public static function admin_dashboard(Request $request)
    {
        $admin = \App('admin');

        $data = [];

        $data['starting_dt'] = Carbon::now($admin->timezonez)->subMonths(2)->format('Y-m-d');
        $data['ending_dt'] = Carbon::now($admin->timezonez)->addday(1)->format('Y-m-d');
        $startups = Startup::where('is_public',1)->count('startup_id');
				$startups = Startup::where('is_public',1)->count('startup_id');
				$investors=Investor::count('investor_id');
				$founders=founder::count('founder_id');
				$industry=Industry::count('industry_id');
        return View::make('Admin.dashboard',compact('data','startups','investors','founders','industry'));
    }

//////////////		Logout 			//////////////////
public static function admin_logout()
	{
	try{
			$admin = \App('admin');

			Admin::where('admin_id',$admin->admin_id)->update([
				'updated_at' => new \DateTime
			]);

			\Cookie::queue('adminAccessToken', 0 , 0);
			\Cookie::queue('admintz', 0 , 0);

			return Redirect::route('admin_login_get')->with('status', 'Logged Out Successfully');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////////		Admin Profile Update Get /////////////////
public static function aprofile_update_get()
	{
	try{
			return View::make('Admin.Profile.profileUpdate');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////		Admin Profile Update Post /////////////////////
public static function aprofile_update_post(Request $request)
	{
	try{
			$data = Input::all();
			$rules = array(
				'name'=>'required',
				'job'=>'required',
				'profile_pic'=>'image',
			);

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$admin = \App('admin');

			if(!empty($request['profile_pic']))
				{
					$request['profile_pic_name'] = CommonController::image_uploader(Input::file('profile_pic'));
					if(empty($request['profile_pic_name']))
						return Redirect::back()->withErrors('Sorry, profile pic not uploaded. Please try again');
				}
			else
				$request['profile_pic_name'] = $admin->profile_pic;

			Admin::where('admin_id',$admin->admin_id)->update(array(
				'name' => $request['name'],
				'job' => $request['job'],
				'profile_pic' => $request['profile_pic_name'],
				'updated_at' => new \DateTime
			));

			return Redirect::back()->with('status','Profile Updated Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

/////////////		Admin Password Update Get  /////////////////////
public static function apassword_update_get()
	{
	try{
			return View::make('Admin.Profile.passwordUpdate');
		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}

//////////////		Admin Password Update post  ////////////////////
public static function apassword_update_post(Request $request)
	{
	try{
			$rules = array('old_password'=>'required','password'=>'required|confirmed');

			$validator = Validator::make($request->all(),$rules);
			if($validator->fails())
				return Redirect::back()->withErrors($validator->getMessageBag()->first());

			$admin = \App('admin');

			if(!\Hash::check($request['old_password'],$admin->admin['password']))
				return Redirect::back()->withErrors('Please Enter the Correct Old Password');
			if(\Hash::check($request['password'],$admin->admin['password']))
				return Redirect::back()->withErrors('Please Enter different password than the current one');

			Admin::where('admin_id',$admin->admin_id)->update(array(
				'password'=>\Hash::make($request['password']),
				'updated_at'=>new \DateTime
			));

			return Redirect::Back()->with('status','Password Updated Successfully');

		}
	catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
	}


}

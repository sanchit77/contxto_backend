<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use App\Models\Investor;
use App\Models\InvestorTemp;
use App\Models\StartupInvestor;
use App\Models\Stage;
use App\Models\Round;
use Illuminate\Support\Facades\Input;
class StageCont extends Controller
{
public function index(){
  $stages=Round::all();
  return \View::make('Admin.Stages.index',compact('stages'));
}

public function add(){
  return \View::make('Admin.Stages.add');
}

public function save(Request $request){
  // dd($request->name);
  $rounds=new Round;
  $rounds->name=@$request->name;
  $rounds->save();
  return \Redirect::route('admin.stage.index');
}

public function StageEdit($id){
  $rounds=Round::where('round_id',$id)->first();
  return \View::make('Admin.Stages.edit',compact('rounds'));
}

public function StageUpdate(Request $request){
  $id=$request->stage_id;
  $update=\DB::table('rounds')
  ->where('round_id',$id)
  ->update(array('name'=>$request->name));
  return \Redirect::route('admin.stage.index');
}

public function stageDelete($id){
  $rounds=Round::where('round_id',$id)->delete();
  return back();
}
}

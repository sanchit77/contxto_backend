<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\StartupIndustry;
use App\Models\Industry;

class IndustryCont extends Controller
{
///////////     Index       ////////////////////////
public static function index(Request $request) {
    return \View::make('Admin.Industries.index');
}

///////////     Ajax Data   ////////////////////////
public static function data_ajax(Request $request) {
    try{
        $data = $request->all();

        if(!isset($data['start']))
            $data['start'] = 0;

        if(!isset($data['length']))
            $data['length'] = 10;

        $recordsTotal = Industry::count();

        $industries = Industry::ajax_data($data);

        $industries = json_decode(json_encode($industries), true);

        $json_data  = array(
            //'data_received' => $data,
            "draw"=> intval($data['draw']),
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsTotal,
            "industries" => $industries,
        );

        return \Response::json($json_data, 200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

//////////////      Ajax Update Name    /////////////////////
public static function ajax_update(Request $request) {
    try{

        $data = $request->all();

        $rules = array(
            'fBtnId' => 'required',
            'industry_id' => 'required|exists:industries,industry_id',
            'name' => 'required|unique:industries,name,'.$data['industry_id'].',industry_id'
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

        Industry::where('industry_id', $data['industry_id'])->update([
            'industry_id' => $data['industry_id'],
            'name' => $data['name']
        ]);

        $data['msg'] = 'Industry updated successfully';

        $new_html = '<button class="btn btn-default" id="'.$data['fBtnId'].'" onClick ="update_name(this.id)" title="'.$data['name'].'">'.$data['name'].'</button>';//0

        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'], 'new_html' => $new_html ),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

//////////////      Industry Startups Listing   //////////////////
public static function startups(Request $request)
    {
    try{

        $data = $request->all();

        $rules = array(
            'industry_id' => 'required|exists:industries,industry_id'
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

        $industry = Industry::where('industry_id', $data['industry_id'])->first();

        return \View::make('Admin.Industries.startupIndex', compact('industry'));

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

    public function delete_ajax(Request $request){
      try{
        $id=$request->industry_id;
        $rules=array(
               'industry_id'=>'required|exists:industries,industry_id',
        );
        $validator=\Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0 ,'status_code'=>400, 'msg'=>$validator->getMessageBag()->first()),200);
        $industry=Industry::where('industry_id',@$id)->delete();
        $startupindustry=StartupIndustry::where('industry_id',@$id)->delete();
         return \Response::json(array('success'=>1, 'status_code'=>200, 'msg'=>'Deleted'),200);
      }catch(\Exception $e){
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
      }
    }

//////////////////

}

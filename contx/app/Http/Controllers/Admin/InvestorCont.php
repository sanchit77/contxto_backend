<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Http\Controllers\CommonController;
use App\Models\Investor;
use App\Models\InvestorTemp;
use App\Models\StartupInvestor;
use Illuminate\Support\Facades\Input;
class InvestorCont extends Controller
{
///////////     Index       ////////////////////////
public static function index(Request $request) {
    return \View::make('Admin.Investors.index');
}

public static function review(Request $request){
  return \View::make('Admin.Investors.review');
}

///////////     Ajax Data   ////////////////////////
public static function data_ajax(Request $request) {
    try{
        $data = $request->all();

        if(!isset($data['start']))
            $data['start'] = 0;

        if(!isset($data['length']))
            $data['length'] = 10;

        $recordsTotal = Investor::count();

        $investors = Investor::ajax_data($data);

        $json_data  = array(
            'data_received' => $data,
            "draw"=> intval($data['draw']),
            "recordsTotal" => $recordsTotal,
            "recordsFiltered" => $recordsTotal,
            "investors" => $investors,
        );

        return \Response::json($json_data, 200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }


    public static function investors_data_review(Request $request) {

        try{
            $data = $request->all();

            if(!isset($data['start']))
                $data['start'] = 0;

            if(!isset($data['length']))
                $data['length'] = 10;

            $recordsTotal = InvestorTemp::count();


            $investors = InvestorTemp::ajax_data($data);
            // return response()->json(array('part'=>$investors));
            $json_data  = array(
                'data_received' => $data,
                "draw"=> intval($data['draw']),
                "recordsTotal" => $recordsTotal,
                "recordsFiltered" => $recordsTotal,
                "investors" => $investors,
            );

            return \Response::json($json_data, 200);

            }
        catch(\Exception $e)
            {
                return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
            }
        }

//////////////      Ajax Update Name    /////////////////////
public static function ajax_update(Request $request) {
    try{

        $data = $request->all();

        $rules = array(
            'fBtnId' => 'required',
            'investor_id' => 'required|exists:investors,investor_id',
            'name' => 'required|unique:investors,name,'.$data['investor_id'].',investor_id'
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

        Investor::where('investor_id', $data['investor_id'])->update([
            'investor_id' => $data['investor_id'],
            'name' => $data['name']
        ]);

        $data['msg'] = 'Investor updated successfully';

        $new_html = '<button class="btn btn-default" id="'.$data['fBtnId'].'" onClick ="update_name(this.id)" title="'.$data['name'].'">'.$data['name'].'</button>';//0

        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'], 'new_html' => $new_html ),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

//////////////      Startups Listing   //////////////////
public static function startups(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'investor_id' => 'required|exists:investors,investor_id',
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    $investor = Investor::where('investor_id', $data['investor_id'])->first();

    return \View::make('Admin.Investors.startupIndex', compact('investor'));

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

public function delete_ajax(Request $request){
  try{
    $id=$request->investor_id;
    $rules=array(
           'investor_id'=>'required|exists:investors,investor_id',
    );
    $validator=\Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0 ,'status_code'=>400, 'msg'=>$validator->getMessageBag()->first()),200);
    $investor=Investor::where('investor_id',@$id)->delete();
    $startupinvestors=StartupInvestor::where('investor_id',@$id)->delete();
     return \Response::json(array('success'=>1, 'status_code'=>200, 'msg'=>'Deleted'),200);
  }catch(\Exception $e){
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
  }
}

public function invajax_update_logo(Request $request)
  {
  try{

      $data = $request->all();

      $rules = array(
          'startup_id' => 'required|exists:investors,investor_id',
          'logo' => 'required|image',
          'col_name' => 'required'
      );

      $validator = \Validator::make($request->all(),$rules);
      if($validator->fails())
          return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

      $data['logo_name'] = CommonController::image_uploader(Input::file('logo'));
      if(empty($data['logo_name']))
          return back()->withErrors('Sorry, image not uploaded. Please try again');
      $data['logo_name'] = env('IMAGE_URL').$data['logo_name'];

      Investor::where('investor_id', $data['startup_id'])->update([
          'image' => $data['logo_name']
      ]);

      $startup = Investor::where('investor_id', $data['startup_id'])->select("investors.*")
      ->first();
       $url=\Request::root();
      // $new_html = $this->generate_logo_td_html($startup->image, $startup->name, $data['col_name']);//13
      $image_td = '<div id="'.$data['col_name'].'3">';

      if($startup->image != '')
          $image_td = $image_td.'<a href="'.$url.'/Uploads/'.$startup->image.'" title="'.$startup->name.'" class="lightBoxGallery" data-gallery=""> <img src="'.$url.'/Uploads/'.$startup->image.'"  class="img-circle"> </a> <br>';

      $image_td = $image_td.'<br><button type="button" class="btn btn-default" id="'.$data['col_name'].'3a" onClick="update_logo_func(this.id, this.parentNode.id)"><i class="fa fa-edit"></i></button>';

      $image_td = $image_td.'</div>';

      $new_html= $image_td;

      return \Response::json(array('success'=>1, 'status_code'=>200, 'msg'=> "Logo added successfully", 'new_html' => $new_html),200);

      }
  catch(\Exception $e)
      {
          return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
      }
  }
}

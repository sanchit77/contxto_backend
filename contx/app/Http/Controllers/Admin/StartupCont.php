<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

use Illuminate\Support\Facades\Input;

//use Db;
use App\Models\Country;
use App\Models\City;
use App\Models\Industry;
use App\Models\Startup;
use App\Models\StartupTemp;
use App\Models\StartupCountriesTemp;
use App\Models\StartupIndustriesTemp;
use App\Models\FounderTemp;
use App\Models\StartupFoundersTemp;
use App\Models\Founder;
use App\Models\Round;
use App\Models\StartupIndustry;
use App\Models\StartupFounder;
use App\Models\StartupCountry;
use App\Models\StartupInvestor;
use App\Models\StartupRound;
use App\Models\StartupNews;

use App\Models\Investor;
use App\Models\InvestorType;
use URL;
use App\Http\Controllers\CommonController;

class StartupCont extends Controller
{
    public static $startup_columns = [
        '0' => 'startup_id',
        '1' => 'name',
        '2' => 'valuation',
        '4' => 'founder',
        '5' => 'investor',
        '6' => 'round',
        '8' => 'description',
        '10' => 'founded',
        '14' => 'website',
        '15' => 'facebook',
        '16' => 'twitter',
        '17' => 'linkedin',
        '18' => 'instagram',
        '19' => 'ipo_symbol',
        '20' => 'is_public'
    ];


///////////     Countries       ////////////////////////
public static function countries(Request $request)
    {
            $data = $request->all();

            if(!isset($data['oldCountryId']))
                $data['oldCountryId'] = 0;

            if(!isset($data['oldCityId']))
                $data['oldCityId'] = 0;

            $countries = Country::select('country_id as id', 'name as text',
                \DB::RAW("(CASE WHEN country_id = ".$data['oldCountryId']." THEN true ELSE false END) as selected")
            )->get();

            $cities = City::where('country_id', $data['oldCountryId'])->select("city_id as id", "name as text",
            \DB::RAW("(CASE WHEN city_id = ".$data['oldCityId']." THEN true ELSE false END) as selected")
            )->get();

            return \Response::json([ 'countries' => $countries, 'cities' => $cities ], 200);

    }

//////////////          Cities Listings     /////////////////////
public static function cities(Request $request)
    {
        $data = $request->all();

        if(!isset($data['newCountryId']))
            $data['newCountryId'] = 0;

        if(!isset($data['oldCityId']))
            $data['oldCityId'] = 0;

        $cities = City::where('country_id', $data['newCountryId'])->select("city_id as id", "name as text",
            \DB::RAW("(CASE WHEN city_id = ".$data['oldCityId']." THEN true ELSE false END) as selected")
            )->get();

        return \Response::json([ 'cities' => $cities ], 200);

    }

//////////////      Industries Listings     ///////////////////////////////
public static function industries(Request $request)
{
    $data = $request->all();

    if(!$data['startup_id'])
        {
            $data['startup_id'] = 0;
        }

    $industries = Industry::select('industries.industry_id as id', 'name as text',
        \DB::RAW("(CASE WHEN startup_industries.industry_id IS NOT NULL THEN true ELSE false END) as selected")
    )
    ->leftjoin('startup_industries', function($q) use ($data){

        $q->on('startup_industries.industry_id', 'industries.industry_id')->where('startup_industries.startup_id', $data['startup_id']);

    })
    ->get();

    return \Response::json([ 'industries' => $industries ], 200);

}

//////////////      Investorys & Type Ajax  ///////////////////////////////
public static function investors_ajax(Request $request)
{
    // if(!isset($request['startup_id']))
    //     $request['startup_id'] = 0;

    if(!isset($request['startup_investor_id']))
        $request['startup_investor_id'] = 0;

    $startup_inverstor = StartupInvestor::where('startup_investor_id', $request['startup_investor_id'])
    ->with([
        'investor'
    ])
    ->first();
    // return response()->json(array('part'=>$startup_inverstor));
    if($startup_inverstor)
        {
            $request['investor_id'] = $startup_inverstor->investor_id;

            if($startup_inverstor['investor'])
                $request['investor_type_id'] = $startup_inverstor['investor']->investor_type_id;
            else
                $request['investor_type_id'] = 0;
        }
    else
        {

            $request['investor_id'] = 0;
            $request['investor_type_id'] = 0;
        }

    $investors = Investor::select("investor_id as id", "name as text",
        \DB::RAW("(CASE WHEN investor_id=".$request['investor_id']." THEN true ELSE false END) as selected")
    )->get();
    // return response()->json(array('part'=>$investors));
    $investor_types = InvestorType::select("investor_type_id as id", "name as text",
        \DB::RAW("(CASE WHEN investor_type_id=".$request['investor_type_id']." THEN true ELSE false END) as selected")
    )->get();

    return \Response::json([ 'investors' => $investors, 'investor_types' => $investor_types ], 200);

}

//////////////          Target Countries Listings     /////////////////////
public static function target_countries(Request $request)
    {

        $data = $request->all();

        $target_countries = Country::leftjoin('startup_countries', function($q) use ($data) {

            $q->on('startup_countries.country_id', 'countries.country_id')->where('startup_countries.startup_id', $data['startup_id']);

        })
        ->select('countries.country_id as id', 'name as text',
            \DB::RAW("(CASE WHEN startup_countries.startup_country_id is NOT NULL THEN true ELSE false END) as selected")
        )->get();

        return \Response::json(['data' => $data, 'target_countries' => $target_countries], 200);

    }

///////////     Index       ////////////////////////
public static function index(Request $request)
    {
    try{
            return \View::make('Admin.Startups.index');
        }
    catch(\Exception $e)
		{
			return Redirect::back()->witherrors($e->getMessage())->withInput();
		}
    }

    /////////////
public function startups_data1(Request $request)
    {
//    try{

        $data = $request->all();
        if(!isset($data['start']))
            $data['start'] = 0;

        if(!isset($data['length']))
            $data['length'] = 10;

        $data['filter'] = Startup::make_filter_query($data['columns'], $data['order']);
        //dd($data['filter']);

        /////////////   Count   ///////////////////
        $recordsTotal = Startup::admin_dynamic_searching_count($data);
        $recordsTotal = COUNT($recordsTotal);
        /////////////   Count   ///////////////////

        $startups = Startup::admin_dynamic_searching($data);

        $startups = json_decode(json_encode($startups), true);

        $con = [];
        $startups_len = COUNT($startups);

    for($i=0;$i<$startups_len;$i++)
        {

            $temp_id = $startups[$i]['startup_id'];//0
            $temp_name = $startups[$i]['name'];//1
            $temp_valuation = $startups[$i]['valuation'];//2
            $temp_logo = $startups[$i]['logo'];//3
            $temp_founders = $startups[$i]['founders'];//4
            $temp_investors = $startups[$i]['investors'];//5

            $temp_startup_rounds = $startups[$i]['startup_rounds'];//6
// return response()->json(array('part'=>$temp_startup_rounds));
            $temp_startup_countries = $startups[$i]['startup_countries'];//7
            $temp_description = $startups[$i]['description'];//8
            $temp_founded = $startups[$i]['founded'];//9
            $temp_country = $startups[$i]['country'];//10
            $temp_city = $startups[$i]['city'];//11

            $temp_industries = $startups[$i]['startup_industries'];//12

            $temp_startup_news_count = $startups[$i]['startup_news_count'];//13

            $temp_website = $startups[$i]['website'];//14
            $temp_facebook = $startups[$i]['facebook'];//15
            $temp_twitter = $startups[$i]['twitter'];//16
            $temp_linkedin = $startups[$i]['linkedin'];//17
            $temp_instagram = $startups[$i]['instagram'];//18
            $temp_ipo_symbol = $startups[$i]['ipo_symbol'];//19
            $temp_is_public = $startups[$i]['is_public'];//20

            $col_name = "table_".$temp_id."_".$i."_";// table_id_rowno_colno

            $var_on_blur_string = ' onblur="inputLostFocus(this.value, this.id, this.getAttribute(\'data-oval\') )" ';
            $var_on_keypress_string = ' onkeydown="inputKeyDown(this.value, this.id, this.getAttribute(\'data-oval\'), event)" ';

            $name = '<input maxlength="255" type="text" data-oval="'.$temp_name.'" class="form-control input-md uif-hidden" name='.$col_name.'1 id='.$col_name.'1 value="'.$temp_name.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'1s >'.$temp_name.'</button>';

            $valuation = '<input maxlength="100" type="text" data-oval="'.$temp_valuation.'" class="form-control input-md uif-hidden" name='.$col_name.'2 id='.$col_name.'2 value="'.$temp_valuation.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'2s >'.$temp_valuation.'</button>';

            $logo = $this->generate_logo_td_html($temp_logo, $temp_name, $col_name);

            /////////// Founders    /////////////////
            // $founders = StartupCont::generate_has_many_rel_td($temp_founders, $col_name.'4_', 4, 'startup_founder_id', 'name');
            $founders = $this->generate_founder_td_html($temp_founders, $col_name);//4
            //dd($founders, $temp_founders);
            /////////// Founders    /////////////////

            /////////// Investors    /////////
            $investors = $this->generate_investor_td_html($temp_investors, $col_name);//5
            // $investors = StartupCont::generate_has_many_rel_td($temp_investors, $col_name.'5_', 5, 'startup_investor_id', 'name');
            /////////// Investors    /////////

            /////////// Investment Rounds   ////////
            $startup_rounds = $this->generate_rounds_td_html($temp_startup_rounds, $col_name);//6
            /////////// Investment Rounds   ////////

            ////////////////////////////////////////////////
            ///////////     Startup Invest Countries    ////////////
            $target_countries = '<div id="'.$col_name.'7" onclick="target_countries_clicked(this.id)">';//7

            $temp_count = COUNT($temp_startup_countries);

            for($i2=0;$i2<$temp_count;$i2++)
                {
                    $target_countries = $target_countries.' <button class="btn btn-default">#'.$temp_startup_countries[$i2]['name'].'</button>';
                }

            if($temp_count == 0)
            $target_countries = $target_countries.'<button class="btn btn-primary"><i class="fa fa-plus"></i></button>';

            $target_countries = $target_countries.'</div>';
            ///////////     Startup Invest Countries    ////////////
            /////////////////////////////////////////////////

            $description = '<input type="text" data-oval="'.$temp_description.'" class="form-control input-md uif-hidden" name='.$col_name.'8 id='.$col_name.'8 value="'.$temp_description.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'8s >'.$temp_description.'</button>';

            $founded = '<a id="'.$col_name.'10" href="#modal-founded" role="button" class="btn" data-toggle="modal" onClick="founded_click(this.id, this.text)">'.$temp_founded.'</a>';//10
            // $founded = "<div id='".$col_name."9' onClick='founded_click(this.id, \"".$temp_founded."\" )'>".$temp_founded."</div>";//10

            //////////  State And Country   /////////////////////
            $country = '<button class="btn btn-default" id='.$col_name.'10 onClick="initialize_country_city_data(this.id, '.$startups[$i]['country_id'].', '.$startups[$i]['city_id'].')">'.$temp_country.'</button>';//10

            $city = '<button class="btn btn-default" id="'.$col_name.'11" onClick="initialize_country_city_data(\''.$col_name.'10\', '.$startups[$i]['country_id'].', '.$startups[$i]['city_id'].')">'.$temp_city.'</button>';//11
            //////////  State ANd Country   /////////////////////

            $industry = $this->generate_industries_td_html($temp_industries, $col_name);//12
            //$industry = '<button class="btn btn-default" id='.$col_name.'12 onClick ="initialize_industy_data(this.id)">'.$temp_industry.'</button>';//12

            $startup_news_count = $this->generate_news_td_html($temp_startup_news_count, $col_name);//13

            $website = '<input maxlength="100" type="text" data-oval="'.$temp_website.'" class="form-control input-md uif-hidden" name='.$col_name.'14 id='.$col_name.'14 value="'.$temp_website.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'14s >'.$temp_website.'</button>';

            $facebook = '<input maxlength="100" type="text" data-oval="'.$temp_facebook.'" class="form-control input-md uif-hidden" name='.$col_name.'15 id='.$col_name.'15 value="'.$temp_facebook.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'15s >'.$temp_facebook.'</button>';

            $twitter = '<input maxlength="100" type="text" data-oval="'.$temp_twitter.'" class="form-control input-md uif-hidden" name='.$col_name.'16 id='.$col_name.'16 value="'.$temp_twitter.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'16s >'.$temp_twitter.'</button>';

            $linkedin = '<input maxlength="100" type="text" data-oval="'.$temp_linkedin.'" class="form-control input-md uif-hidden" name='.$col_name.'17 id='.$col_name.'17 value="'.$temp_linkedin.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'17s >'.$temp_linkedin.'</button>';

            $instagram = '<input maxlength="100" type="text" data-oval="'.$temp_instagram.'" class="form-control input-md uif-hidden" name='.$col_name.'18 id='.$col_name.'18 value="'.$temp_instagram.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'18s >'.$temp_instagram.'</button>';

            $ipo_symbol = '<input maxlength="100" type="text" data-oval="'.$temp_ipo_symbol.'" class="form-control input-md uif-hidden" name='.$col_name.'19 id='.$col_name.'19 value="'.$temp_ipo_symbol.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'19s >'.$temp_ipo_symbol.'</button>';


            if($temp_is_public == "Public")
                $is_public = '<select data-oval="1" class="form-control" id="'.$col_name.'20" onChange="inputLostFocus(this.value, this.id, this.getAttribute(\'data-oval\'))"><option value="1" selected="true">Public</option><option value="0">Private</option></select>';
            else
                $is_public = '<select data-oval="0" class="form-control" id="'.$col_name.'20" onChange="inputLostFocus(this.value, this.id, this.getAttribute(\'data-oval\'))"><option value="1">Public</option><option value="0" selected="true">Private</option></select>';

            unset($startups[$i]['startup_industries']);
            unset($startups[$i]['startup_countries']);

            $startups[$i]['name'] = $name;//1
            $startups[$i]['valuation'] = $valuation;//2
            $startups[$i]['logo'] = $logo;//3
            $startups[$i]['founders'] = $founders;//4
            $startups[$i]['investors'] = $investors;//5
            $startups[$i]['startup_rounds'] = $startup_rounds;//6

            $startups[$i]['target_countries'] = $target_countries;//8
            $startups[$i]['description'] = $description;//8
            $startups[$i]['founded'] = $founded;//9
            $startups[$i]['country'] = $country;//10
            $startups[$i]['city'] = $city;//11
            $startups[$i]['industry'] = $industry;//12
            $startups[$i]['startup_news_count'] = $startup_news_count;//13
            $startups[$i]['website'] = $website;//14
            $startups[$i]['facebook'] = $facebook;//15
            $startups[$i]['twitter'] = $twitter;//16
            $startups[$i]['linkedin'] = $linkedin;//17
            $startups[$i]['instagram'] = $instagram;//18
            $startups[$i]['ipo_symbol'] = $ipo_symbol;//19
            $startups[$i]['is_public'] = $is_public;//20

        }

            $json_data  = array(
                'data_received' => $data,

                "draw"=> intval($request['draw']),
                "recordsTotal" => count($startups),
                "recordsFiltered" => intval($recordsTotal),

                "startups" => $startups,
            );

            return \Response::json($json_data, 200);

        //     }
        // catch(\Exception $e)
        //     {
        //         return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage(), 'data'=> $data),200);
        //     }
        }

///////////     review       ////////////////////////
public static function review(Request $request)
    {
    try{
            return \View::make('Admin.Startups.review');
        }
    catch(\Exception $e)
        {
            return Redirect::back()->witherrors($e->getMessage())->withInput();
        }
    }

/////////////
public function startups_data_review(Request $request)
    {
//    try{

        $data = $request->all();

        if(!isset($data['start']))
            $data['start'] = 0;

        if(!isset($data['length']))
            $data['length'] = 10;

        $data['filter'] = StartupTemp::make_filter_query($data['columns'], $data['order']);

        /////////////   Count   ///////////////////
        $recordsTotal = StartupTemp::admin_dynamic_searching_count($data);
        $recordsTotal = COUNT($recordsTotal);
        /////////////   Count   ///////////////////

        $startups = StartupTemp::admin_dynamic_searching($data);
        $startups = json_decode(json_encode($startups), true);

        $con = [];
        $startups_len = COUNT($startups);

        for($i=0;$i<$startups_len;$i++)
        {

            $temp_id = $startups[$i]['startup_id'];//0
            $temp_name = $startups[$i]['name'];//1
            $temp_logo = $startups[$i]['logo'];//3
            $temp_founders = $startups[$i]['founders_temp'];//4
            $temp_startup_countries = $startups[$i]['startup_countries_temp'];//7
            $temp_description = $startups[$i]['description'];//8
            $temp_founded = $startups[$i]['founded'];//9
            $temp_country = $startups[$i]['country'];//10
            $temp_city = $startups[$i]['city'];//11
            $temp_industries = $startups[$i]['startup_industries_temp'];//12
            $temp_website = $startups[$i]['website'];//14
            $temp_facebook = $startups[$i]['facebook'];//15
            $temp_twitter = $startups[$i]['twitter'];//16
            $temp_linkedin = $startups[$i]['linkedin'];//17
            $temp_instagram = $startups[$i]['instagram'];//18
            $temp_ipo_symbol = $startups[$i]['ipo_symbol'];//19
            $temp_is_public = $startups[$i]['is_public'];//20

            $col_name = "table_".$temp_id."_".$i."_";// table_id_rowno_colno

            $var_on_blur_string = ' onblur="inputLostFocus(this.value, this.id, this.getAttribute(\'data-oval\') )" ';
            $var_on_keypress_string = ' onkeydown="inputKeyDown(this.value, this.id, this.getAttribute(\'data-oval\'), event)" ';

            $name = '<input maxlength="255" type="text" data-oval="'.$temp_name.'" class="form-control input-md uif-hidden" name='.$col_name.'1 id='.$col_name.'1 value="'.$temp_name.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'1s >'.$temp_name.'</button>';

            $logo = $this->generate_logo_td_html_temp($temp_logo, $temp_name, $col_name);

            /////////// Founders    /////////////////
            // $founders = StartupCont::generate_has_many_rel_td($temp_founders, $col_name.'4_', 4, 'startup_founder_id', 'name');
            $founders = $this->generate_founder_td_html_temp($temp_founders, $col_name);//4
            //dd($founders, $temp_founders);
            /////////// Founders    /////////////////


            ////////////////////////////////////////////////
            ///////////     Startup Invest Countries    ////////////
            $target_countries = '<div id="'.$col_name.'7" onclick="target_countries_clicked(this.id)">';//7

            $temp_count = COUNT($temp_startup_countries);

            for($i2=0;$i2<$temp_count;$i2++)
                {
                    $target_countries = $target_countries.' <button class="btn btn-default">#'.$temp_startup_countries[$i2]['name'].'</button>';
                }

            if($temp_count == 0)
            $target_countries = $target_countries.'<button class="btn btn-primary"><i class="fa fa-plus"></i></button>';

            $target_countries = $target_countries.'</div>';
            ///////////     Startup Invest Countries    ////////////
            /////////////////////////////////////////////////

            $description = '<input type="text" data-oval="'.$temp_description.'" class="form-control input-md uif-hidden" name='.$col_name.'8 id='.$col_name.'8 value="'.$temp_description.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'8s >'.$temp_description.'</button>';

            $founded = '<a id="'.$col_name.'10" href="#modal-founded" role="button" class="btn" data-toggle="modal" onClick="founded_click(this.id, this.text)">'.$temp_founded.'</a>';//10
            // $founded = "<div id='".$col_name."9' onClick='founded_click(this.id, \"".$temp_founded."\" )'>".$temp_founded."</div>";//10

            //////////  State And Country   /////////////////////
            $country = '<button class="btn btn-default" id='.$col_name.'10 onClick="initialize_country_city_data(this.id, '.$startups[$i]['country_id'].', '.$startups[$i]['city_id'].')">'.$temp_country.'</button>';//10

            $city = '<button class="btn btn-default" id="'.$col_name.'11" onClick="initialize_country_city_data(\''.$col_name.'10\', '.$startups[$i]['country_id'].', '.$startups[$i]['city_id'].')">'.$temp_city.'</button>';//11
            //////////  State ANd Country   /////////////////////

            $industry = $this->generate_industries_td_html($temp_industries, $col_name);//12
            //$industry = '<button class="btn btn-default" id='.$col_name.'12 onClick ="initialize_industy_data(this.id)">'.$temp_industry.'</button>';//12

            $website = '<input maxlength="100" type="text" data-oval="'.$temp_website.'" class="form-control input-md uif-hidden" name='.$col_name.'14 id='.$col_name.'14 value="'.$temp_website.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'14s >'.$temp_website.'</button>';

            $facebook = '<input maxlength="100" type="text" data-oval="'.$temp_facebook.'" class="form-control input-md uif-hidden" name='.$col_name.'15 id='.$col_name.'15 value="'.$temp_facebook.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'15s >'.$temp_facebook.'</button>';

            $twitter = '<input maxlength="100" type="text" data-oval="'.$temp_twitter.'" class="form-control input-md uif-hidden" name='.$col_name.'16 id='.$col_name.'16 value="'.$temp_twitter.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'16s >'.$temp_twitter.'</button>';

            $linkedin = '<input maxlength="100" type="text" data-oval="'.$temp_linkedin.'" class="form-control input-md uif-hidden" name='.$col_name.'17 id='.$col_name.'17 value="'.$temp_linkedin.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'17s >'.$temp_linkedin.'</button>';

            $instagram = '<input maxlength="100" type="text" data-oval="'.$temp_instagram.'" class="form-control input-md uif-hidden" name='.$col_name.'18 id='.$col_name.'18 value="'.$temp_instagram.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'18s >'.$temp_instagram.'</button>';

            $ipo_symbol = '<input maxlength="100" type="text" data-oval="'.$temp_ipo_symbol.'" class="form-control input-md uif-hidden" name='.$col_name.'19 id='.$col_name.'19 value="'.$temp_ipo_symbol.'" '.$var_on_blur_string.' '.$var_on_keypress_string.' /><button type="button" class="sneaky" id='.$col_name.'19s >'.$temp_ipo_symbol.'</button>';

            if($temp_is_public == "Public")
                $is_public = "Public";
            else
                $is_public = "Private";
            $aprrove_btn = "<button data-id='".$temp_id."' type='button' onClick='return approveStartup(this);'>Approve</button>";

            unset($startups[$i]['startup_industries']);
            unset($startups[$i]['startup_countries']);

            $startups[$i]['name'] = $name;//1
            $startups[$i]['logo'] = $logo;//3
            $startups[$i]['founders'] = $founders;//4
            $startups[$i]['target_countries'] = $target_countries;//8
            $startups[$i]['description'] = $description;//8
            $startups[$i]['founded'] = $founded;//9
            $startups[$i]['country'] = $country;//10
            $startups[$i]['city'] = $city;//11
            $startups[$i]['industry'] = $industry;//12
            $startups[$i]['website'] = $website;//14
            $startups[$i]['facebook'] = $facebook;//15
            $startups[$i]['twitter'] = $twitter;//16
            $startups[$i]['linkedin'] = $linkedin;//17
            $startups[$i]['instagram'] = $instagram;//18
            $startups[$i]['ipo_symbol'] = $ipo_symbol;//19
            $startups[$i]['is_public'] = $is_public;//20
            $startups[$i]['aprrove_btn'] = $aprrove_btn;//21
        }

            $json_data  = array(
                'data_received' => $data,

                "draw"=> intval($request['draw']),
                "recordsTotal" => count($startups),
                "recordsFiltered" => intval($recordsTotal),

                "startups" => $startups,
            );

            return \Response::json($json_data, 200);

        //     }
        // catch(\Exception $e)
        //     {
        //         return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage(), 'data'=> $data),200);
        //     }
        }

    //////////// Ajax approve startup ///////
    public function ajax_approve_startup(Request $request){
        $data = $request->all();

        if(!isset($data['startup_id']))
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> "Input id not provided", 'data' => $data), 200);

      //  move startup to main table
        $startup = StartupTemp::find($data['startup_id']);
        $startup_clone = $startup->replicate();
        unset($startup_clone['created_at'],$startup_clone['updated_at']);
        $startup_clone['city_id']=0;
        $startup_new = json_decode($startup_clone, true);
        $start=Startup::create($startup_new);


        //move startup countries to main table
        $startup_countries = StartupCountriesTemp::where('startup_id',$data['startup_id'])->first();
        $startup_countries_clone = $startup_countries->replicate();
        unset($startup_countries_clone['created_at'],$startup_countries_clone['updated_at']);
        $startup_countries_new = json_decode($startup_countries_clone, true);

        $country=StartupCountry::create($startup_countries_new);
        $countryid=$country->startup_country_id;
        $update_country=\DB::table('startup_countries')
        ->where('startup_country_id',$countryid)
        ->update(array('startup_id'=>$start->startup_id));


        //move startup industries to main table
        $startup_industries = StartupIndustriesTemp::where('startup_id',$data['startup_id'])->get();
        foreach ($startup_industries as $key => $value) {
            $startup_industries_clone = $value->replicate();
            unset($startup_industries_clone['created_at'],$startup_industries_clone['updated_at']);
            $startup_industries_new = json_decode($startup_industries_clone, true);
            $industry=StartupIndustry::create($startup_industries_new);
            $industryid=$industry->startup_industry_id;
            $update_industry=\DB::table('startup_industries')
            ->where('startup_industry_id',$industryid)
            ->update(array('startup_id'=>$start->startup_id));
        }
        // return response()->json(array('part'=>1));
        //
        //move startup founders & founders to main table
        $startup_founders = StartupFoundersTemp::where('startup_id',$data['startup_id'])->get();
        foreach($startup_founders as $startup_founder){
        $founders = FounderTemp::where('founder_id',$startup_founder->founder_id)->get();
        foreach($founders as  $key => $value){
        $founders_clone = $value->replicate();
        unset($founders_clone['created_at'],$founders_clone['updated_at']);
        $founders_clone['is_blocked'] =0;
        $founders_new = json_decode($founders_clone, true);
        $founder =  Founder::create($founders_new);

    }

       // $founders->delete();
      // print $founder->founder_id;
        $startup_founders_clone = $startup_founder->replicate();
        unset($startup_founders_clone['created_at'],$startup_founders_clone['updated_at']);
        $startup_founders_new = json_decode($startup_founders_clone, true);

        $startfounder=StartupFounder::create($startup_founders_new);

        $startfounderid=$startfounder->startup_founder_id;

        $update_startfounder=\DB::table('startup_founders')
        ->where('startup_founder_id',$startfounderid)
        ->update(array('startup_id'=>@$start->startup_id,'founder_id'=>@$founder->founder_id));
    }


        //move founders to main table



        //move startup founders to main table


        // //delete temp data
        $startup_industries = StartupIndustriesTemp::where('startup_id',$data['startup_id'])->delete();
        $startup_countries->delete();

        $startup_founderss = StartupFoundersTemp::where('startup_id',$data['startup_id'])->get();
        foreach($startup_founderss as $startup_founders){
        $founders = FounderTemp::where('founder_id',$startup_founders->founder_id)->delete();
        }
        $startup_founders = StartupFoundersTemp::where('startup_id',$data['startup_id'])->delete();
        $startup->delete();
        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg'=> "Approved"), 200);
    }

//////////////////   Ajax Update Text   /////////////////////////
public function ajax_update_text(Request $request)
    {
    try{

        $data = $request->all();

        if(!isset($data['inputId']))
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> "Input id not provided", 'data' => $data), 200);

        $data['exploded'] = explode('_', $data['inputId']);
        if(COUNT($data['exploded']) != 4)
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> "Invalid Input id provided", 'data' => $data), 200);

        $data['startup_id'] = (int) $data['exploded'][1];
        $data['row_id'] = (int) $data['exploded'][2];
        $data['column_id'] = (int) $data['exploded'][3];

        $data['column_name'] = static::$startup_columns[$data['column_id']];
        if($data['column_name']=='name'){
            $slug = str_replace(" ","-",$data['newVal']);
            $startup = Startup::where('slug',$slug)->where('startup_id','!=',$data['startup_id'])->count();
            if($startup>0){
                return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> "Company already exist with this name"), 200);
            }
            $data['updated'] = Startup::where('startup_id', $data['startup_id'])->update([
                $data['column_name'] => $data['newVal'],
                'slug' => $slug
            ]);
        }else{
            $data['updated'] = Startup::where('startup_id', $data['startup_id'])->update([
                $data['column_name'] => $data['newVal']
            ]);
        }

        $data['msg'] = ucwords(str_replace('_', ' ', $data['column_name'])).' updated successfully';

        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'], 'data' => $data ),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage() ),200);
        }
    }

////////////        Ajax Add Many Relation  ///////////////////
public function ajax_add_has_many_relation(Request $request)
    {
    try{
        $data = $request->all();

        $rules = array(
            'startup_id' => 'required|exists:startups,startup_id',
            'column_id' => 'required',
            'inputValue' => 'required',
            'row_id' => 'required'
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

        $data['column_name'] = static::$startup_columns[$data['column_id']];

        if($data['column_id'] == 4)
            {///////////////    Founder     /////////////////

            $founder_id = Founder::add_new($data['inputValue']);
            $startup_founder_id = StartupFounder::add_new($data['startup_id'], $founder_id);

            $new_startup = Startup::where('startup_id',$data['startup_id'])
            ->with([
                'founders'
            ])
            ->first()
            ->toArray();

            $col_name = "table_".$data['startup_id']."_".$data['row_id']."_".$data['column_id'].'_';// table_id_rowno_colno_

            $new_html = StartupCont::generate_has_many_rel_td($new_startup['founders'], $col_name, $data['column_id'], 'startup_founder_id', 'name');

            }///////////////    Founder     /////////////////
        elseif($data['column_id'] == 5)
            {///////////////    Investor     ////////////////

                $investor_id = Investor::add_new($data['inputValue']);
                $startup_investor_id = StartupInvestor::add_new($data['startup_id'], $investor_id);

                $new_startup = Startup::where('startup_id',$data['startup_id'])
                ->with([
                    'investors'
                ])
                ->first()
                ->toArray();

                $col_name = "table_".$data['startup_id']."_".$data['row_id']."_".$data['column_id'].'_';// table_id_rowno_colno_

                $new_html = StartupCont::generate_has_many_rel_td($new_startup['investors'], $col_name, $data['column_id'], 'startup_investor_id', 'name');

            }///////////////    Investor     ////////////////

        $data['msg'] = ucwords($data['column_name']).' added successfully';

        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'], 'new_html' => $new_html ),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

/////////   Update Has many Relation    ///////////////////////
public static function ajax_update_has_many_relation(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'row_id' => 'required',
        'column_id' => 'required',
        'db_row_id' => 'required',
        'newVal' => 'required',
        'dynamic_db_id' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return Redirect::back()->withErrors($validator->getMessageBag()->first())->withInput($request->all());

    $data['column_name'] = static::$startup_columns[$data['column_id']];

    if($data['column_id'] == 4)
        {///////////////    Founder     /////////////////

            Founder::where('founder_id',$data['dynamic_db_id'])
            ->update([
                'name' => $data['newVal']
            ]);

        }///////////////    Founder     /////////////////
    else if($data['column_id'] == 5)
        {///////////////    Investor     ////////////////

            Investor::where('investor_id',$data['dynamic_db_id'])
            ->update([
                'name' => $data['newVal']
            ]);

        }///////////////    Investor     ////////////////

    $data['msg'] = ucwords($data['column_name']).' updated successfully';

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'] ),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

////////////////        Ajax Update Target Countries    //////////////////
public static function ajax_utarget_countries(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'new_ids' => 'required|array'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    StartupCountry::where('startup_id',$data['startup_id'])->delete();

    $con = [];

    $len = COUNT($data['new_ids']);
    for($i=0;$i<$len;$i++)
        {
            $con[] = [
                'startup_id' => $data['startup_id'],
                'country_id' => $data['new_ids'][$i]
            ];

        }
    if(COUNT($con) > 0)
        StartupCountry::insert($con);

    $countries = Country::whereIn('country_id', $data['new_ids'])->get();

    $new_html = '';

    $temp_count = COUNT($countries);

    for($i=0;$i<$temp_count;$i++)
        {
            $new_html = $new_html.' <button class="btn btn-default">#'.$countries[$i]['name'].'</button>';
        }

    if($temp_count == 0)
        $new_html = $new_html.'<button class="btn btn-primary"><i class="fa fa-plus"></i></button>';

    $new_html = $new_html.'</div>';

    return \Response::json(["success"=>1, "status_code"=> 200, "msg" => "Target Countries updated successfully", 'new_html' => $new_html ],200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

//////////////      Ajax Update Location    //////////////////////////////
public static function ajax_update_loc(Request $request)
    {
    try{
        $data = $request->all();

        $rules = array(
            'startup_id' => 'required|exists:startups,startup_id',
            'country_id' => 'required|exists:countries,country_id',
            'city_val' => 'required'
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

        $city = City::where('country_id', $data['country_id'])->where('name', $data['city_val'])->first();
        if(!$city)
            {
                $data['city_id'] = City::insertGetId([
                    'country_id' => $data['country_id'],
                    'name' => $data['city_val']
                ]);
            }
        else
            {
                $data['city_id'] = $city->city_id;
            }

        Startup::where('startup_id', $data['startup_id'])->update([
            'country_id' => $data['country_id'],
            'city_id' => $data['city_id']
        ]);

        $data['msg'] = 'Location updated successfully';

        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $data['msg'] ),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

///////////////     Update Industry         //////////////////////
public function ajax_update_industry(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'col_name' => 'required',
        'industries' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    $i = 0;
    $len = COUNT($data['industries']);
    $con = [];

    StartupIndustry::where('startup_id', $data['startup_id'])->delete();

    for($i=0;$i<$len;$i++)
    {

        if(!is_numeric($data['industries'][$i]['id']))
            {/////////////      Industry Already Present    //////////

                $check = Industry::where('name',$data['industries'][$i]['text'])
                ->first();
                if($check)
                    {
                        $data['industries'][$i]['id'] = $check->industry_id;
                    }
                else
                    {
                        $data['industries'][$i]['id'] = Industry::create(
                            $data['industries'][$i]['text'],
                            'Admin'
                        );
                    }

            }/////////////      Industry Already Present    //////////

        $con[] = [
            'industry_id' => $data['industries'][$i]['id'],
            'startup_id' => $data['startup_id']
        ];

    }

    if(COUNT($con))
        StartupIndustry::insert($con);

    $startup = Startup::where('startup_id', $data['startup_id'])
        ->with([
            'startup_industries'
        ])
    ->first();

    $new_html = $this->generate_industries_td_html($startup->startup_industries, $data['col_name']);

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => "Industry updated successfully", 'new_html' => $new_html ),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

/////////////// Ajax Create Investor & Type     ////////////////////
public function ajax_create_investor(Request $request)
{
    try{

        $data = $request->all();

        $rules = array(
            'startup_id' => 'required|exists:startups,startup_id',
            //'startup_investor_id' => 'required|exists:startup_investors,startup_investor_id',
            // 'investor_val' => 'required',
            // 'investor_id' => 'required',
            'investor_type_val' => 'required',
            'investor_type_id' => 'required'
        );

        $validator = \Validator::make($request->all(),$rules);
        if($validator->fails())
            return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

         // return response()->json(array('part'=>$data['investor_select']));
    //////////////////////  Type
        if(!is_numeric($data['investor_type_id']))
            {/////////  New Investor Type   ////////////////

                $data['investor_type_id'] = InvestorType::insertGetId([
                    'name' => $data['investor_type_val']
                ]);

            }/////////  New Investor Type   ////////////////
        else
            {/////////  New Investor Type   ////////////////

                  InvestorType::where('investor_type_id', $data['investor_type_id'])->update([
                      'name' => $data['investor_type_val']
                  ]);

            }/////////  New Investor Type   ////////////////
    //////////////////////  Type

        // if(!is_numeric($data['investor_id']))
        //     {/////////  New Investor    ////////////////
        //
        //         $data['investor_id'] = Investor::insertGetId([
        //             'investor_type_id' => $data['investor_type_id'],
        //             'name' => $data['investor_val']
        //         ]);
        //
        //     }/////////  New Investor    ////////////////
        // else
        //     {/////////  Old Investor    ////////////////
        //
        //         Investor::where('investor_id', $data['investor_id'])
        //         ->update([
        //             'investor_type_id' => $data['investor_type_id'],
        //             'name' => $data['investor_val']
        //         ]);
        //
        //     }/////////  Old Investor    ////////////////
        foreach($data['investor_select'] as $invest_id){
        StartupInvestor::create([
            'startup_id' => $data['startup_id'],
            'investor_id' => $invest_id
        ]);
      }

        $startup = Startup::where('startup_id', $data['startup_id'])
            ->with([
                'investors'
            ])
            ->first();

        $new_html = $this->generate_investor_td_html($startup->investors, $request['col_name']);

        return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => "Startup investor added successfully", 'new_html' => $new_html ),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
}

/////////////// Ajax Update Investors & Type    ////////////////////
public function ajax_update_investor(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'startup_investor_id' => 'required|exists:startup_investors,startup_investor_id',
        'investor_val' => 'required',
        'investor_id' => 'required',
        'investor_type_val' => 'required',
        'investor_type_id' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

//////////////////////  Type
    if(!is_numeric($data['investor_type_id']))
        {/////////  New Investor Type   ////////////////

            $data['investor_type_id'] = InvestorType::insertGetId([
                'name' => $data['investor_type_val']
            ]);

        }/////////  New Investor Type   ////////////////
    else
        {/////////  New Investor Type   ////////////////

            InvestorType::where('investor_type_id', $data['investor_type_id'])->update([
                'name' => $data['investor_type_val']
            ]);

        }/////////  New Investor Type   ////////////////
//////////////////////  Type

    if(!is_numeric($data['investor_id']))
        {/////////  New Investor    ////////////////

            $data['investor_id'] = Investor::insertGetId([
                'investor_type_id' => $data['investor_type_id'],
                'name' => $data['investor_val']
            ]);

        }/////////  New Investor    ////////////////
    else
        {/////////  Old Investor    ////////////////

            Investor::where('investor_id', $data['investor_id'])
            ->update([
                'investor_type_id' => $data['investor_type_id'],
                'name' => $data['investor_val']
            ]);

        }/////////  Old Investor    ////////////////

    StartupInvestor::where('startup_investor_id', $data['startup_investor_id'])->update([
        'investor_id' => $data['investor_id']
    ]);


    $startup = Startup::where('startup_id', $data['startup_id'])
        ->with([
            'investors'
        ])
        ->first();

    $new_html = $this->generate_investor_td_html($startup->investors, $request['col_name']);

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => "Startup investor updated successfully", 'new_html' => $new_html ),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

/////////////       Ajax Data Investment Round ////////////////////
public static function ajax_investor_round_data(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'startup_round_id' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    if($data['startup_round_id'] == 0)
        {////////////       New     ////////////////
            $data['round_id'] = 0;
            $data['investor_id'] = 0;

            $startup_round = null;
        }////////////       New     ////////////////
    else
        {////////////       Old     ////////////////
            $startup_round = StartupRound::where('startup_round_id', $data['startup_round_id'])->first();
            if(!$startup_round)
                return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> 'Startup round not found'),200);

            $data['round_id'] = $startup_round->round_id;
            $data['investor_ids'] = $startup_round->investors_name;
            $data['investor_id'] = $startup_round->investor_id;
            $invest=explode(",",$data['investor_ids']);
//1,2
        }
        // return response()->json(array('part'=>$invest));
    // foreach($invest as $inves){
      // return response()->json(array('part'=>$inves));
    $investors = Investor::select("investor_id as id", "name as text",
        \DB::Raw("(CASE WHEN investor_id IN (".$data['investor_id'].") THEN true ELSE false END) as selected")
    )->get();
  // }


    $rounds = Round::select("round_id as id", "name as text",
        \DB::RAW("(CASE WHEN round_id=".$data['round_id']." THEN true ELSE false END) as selected")
    )->get();

    return \Response::json([ 'startup_round' => $startup_round, 'investors' => $investors, 'rounds' => $rounds ], 200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

/////////       Add Round Data  ///////////////////////////////
public function ajax_cinvestor_round_data(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'investor_val' => 'required',
        'investor_id' => 'required',
        'round_val' => 'required',
        'round_id' => 'required',
        'col_name' => 'required',
        'amount' => 'required',
        'dt' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    if(!is_numeric($data['round_id']))
        {/////////  New Round    ////////////////
            $data['round_id'] = Round::insertGetId([
                'name' => $data['round_val']
            ]);
        }/////////  New Round    ////////////////
    else
        {/////////  Old Round    ////////////////
            Round::where('round_id', $data['round_id'])
            ->update([
                'name' => $data['round_val']
            ]);
            // return response()->json(array('part'=>$data['investor_select1']));
        }/////////  Old Round    ////////////////
        $startups=Startup::where('startup_id',$data['startup_id'])->first();
        $county=Country::where('country_id',@$startups->country_id)->first();
        $statrtupindus=StartupIndustry::where('startup_id',$data['startup_id'])->first();

        $industry_name=Industry::where('industry_id',@$statrtupindus->industry_id)->first();
        $rounds_name=Round::where('round_id',$data['round_id'])->first();
        $invest=implode(",",$data['investor_select1']);

// foreach($data['investor_select1'] as $investor){
        StartupRound::insert([
            'startup_id' => $data['startup_id'],
            'round_id' => $data['round_id'],
            'round_name' => @$rounds_name->name,
            'investors_name' => @$invest,
            'investor_id' => $data['investor_id'],
            'amount' => $data['amount'],
            'dt' => $data['dt'],
            'country_id' => @$startups->country_id,
            'country_name' => @$county->name,
            'industry_id' => @$statrtupindus->industry_id,
            'industry_name' => @$industry_name->name,
        ]);
      // }

    $startup = Startup::where('startup_id', $data['startup_id'])
        ->with([
            'startup_rounds'
        ])
        ->first();

    $new_html = $this->generate_rounds_td_html($startup->startup_rounds, $data['col_name']);//6

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => "Startup investment round added successfully", 'new_html' => $new_html ),200);


    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

/////////       Update Round Data   ///////////////////////////
public function ajax_uinvestor_round_data(Request $request)
{
try{
    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'startup_round_id' => 'required|exists:startup_rounds,startup_round_id',
        'investor_val' => 'required',
        'investor_id' => 'required',
        'round_val' => 'required',
        'round_id' => 'required',
        'col_name' => 'required',
        'amount' => 'required',
        'dt' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    if(!is_numeric($data['round_id']))
        {/////////  New Round    ////////////////

            $data['round_id'] = Round::insertGetId([
                'name' => $data['round_val']
            ]);

        }/////////  New Round    ////////////////
    else
        {/////////  Old Round    ////////////////

            Round::where('round_id', $data['round_id'])
            ->update([
                'name' => $data['round_val']
            ]);

        }/////////  Old Round    ////////////////
        $invest=implode(",",$data['investor_select1']);

// foreach($data['investor_select1'] as $investor){


        StartupRound::where('startup_round_id', $data['startup_round_id'])->update([
            'round_id' => $data['round_id'],
            'investors_name' => @$invest,
            'investor_id' => $data['investor_id'],
            'amount' => $data['amount'],
            'dt' => $data['dt']
        ]);

    $startup = Startup::where('startup_id', $data['startup_id'])
        ->with([
            'startup_rounds'
        ])
        ->first();

    $new_html = $this->generate_rounds_td_html($startup->startup_rounds, $data['col_name']);//6

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => "Startup investment round updated successfully", 'new_html' => $new_html ),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

////////////////    Update Logo         /////////////////////////////
public function ajax_update_logo(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'logo' => 'required|image',
        'col_name' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    $data['logo_name'] = CommonController::image_uploader(Input::file('logo'));
    if(empty($data['logo_name']))
        return back()->withErrors('Sorry, image not uploaded. Please try again');
    $data['logo_name'] = env('IMAGE_URL').$data['logo_name'];

    Startup::where('startup_id', $data['startup_id'])->update([
        'logo' => $data['logo_name']
    ]);

    $startup = Startup::where('startup_id', $data['startup_id'])->select("startups.*")
    ->first();

    $new_html = $this->generate_logo_td_html($startup->logo, $startup->name, $data['col_name']);//13

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg'=> "Logo added successfully", 'new_html' => $new_html),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

////////////////        Founder Get Data    /////////////////////////
public static function ajax_founder_data(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'startup_founder_id' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    if($data['startup_founder_id'] == 0)
        {
            $founders = Founder::select("founders.founder_id as id", "name as text")
            ->leftJoin('startup_founders', function($q) use ($data) {
                $q->on('startup_founders.founder_id', 'founders.founder_id');
                $q->where('startup_founders.startup_id', $data['startup_id']);
            })
            ->where('startup_founders.startup_founder_id', null)
            ->get();
        }
    else
        {

            $startup_founder = StartupFounder::where('startup_founder_id', $data['startup_founder_id'])->first();

            $data['founder_id'] = $startup_founder->founder_id;

            $founders = Founder::select("founder_id as id", "name as text",
                \DB::RAW("(CASE WHEN founder_id=".$data['founder_id']." THEN true ELSE false END) as selected")
            )->get();

        }


    return \Response::json(["success"=>1, 'founders' => $founders ], 200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

/////////////////       Founder Update      /////////////////////////
public function ajax_founder_update(Request $request)
{
try{

    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'startup_founder_id' => 'required',
        'founder_val' => 'required',
        'founder_id' => 'required',
        'col_name' => 'required',
        'roles' => 'required',
        //'profile_url' => 'sometimes|url'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);


    if(!is_numeric($data['founder_id']))
        {/////////  New Founder   ////////////////

            $check_founder = Founder::where('name', $data['founder_val'])->first();
            if($check_founder)
                {
                    $data['founder_id'] = $check_founder->founder_id;
                }
            else
                {
                    $data['founder_id'] = Founder::add_new($data['founder_val']);
                }

        }/////////  New Founder   ////////////////
    if($data['startup_founder_id'] != 0)
        $startup_founder = StartupFounder::where('startup_founder_id', $data['startup_founder_id'])->delete();

    $check_startup_founder = StartupFounder::where('founder_id', $data['founder_id'])->where('startup_id', $data['startup_id'])->first();
    if($check_startup_founder)
        {
            StartupFounder::where('startup_founder_id', $check_startup_founder->startup_founder_id)->update([
                'roles' => $data['roles'],
                'founder_link' => $data['founder_link']
            ]);
        }
    else
        {
            $data['admin']='Admin';
            // return response()->json(array('part'=>$data['startup_id']));
            StartupFounder::create([
            'startup_id'=>$data['startup_id'],
            'founder_id'=> $data['founder_id'],
            'source'=> $data['admin'],
            'roles'=>$data['roles'],
            'founder_link'=>$data['founder_link']
          ]);

        }

    $startup = Startup::where('startup_id', $data['startup_id'])
    ->with([
        'founders'
    ])
    ->first();

    $new_html = $this->generate_founder_td_html($startup->founders, $request['col_name']);

    if($data['ajax_type'] == 'Add')
        $msg = 'Founder added successfully';
    else
        $msg = 'Founder updated successfully';

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg' => $msg, 'new_html' => $new_html ),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

////////////////        Create News         /////////////////////////
public function ajax_cnews(Request $request)
{
try{
    $data = $request->all();

    $rules = array(
        'startup_id' => 'required|exists:startups,startup_id',
        'url' => 'required',
        'image' => 'required|image',
        'description' => 'required',
        'source' => 'required',
        'col_name' => 'required'
    );

    $validator = \Validator::make($request->all(),$rules);
    if($validator->fails())
        return \Response::json(array('success'=>0, 'status_code'=>400, 'msg'=> $validator->getMessageBag()->first()),200);

    $data['image_name'] = CommonController::image_uploader(Input::file('image'));
    if(empty($data['image_name']))
            return Redirect::back()->withErrors('Sorry, image not uploaded. Please try again');

    $data['image_name'] = env('IMAGE_URL').$data['image_name'];

    StartupNews::insert([
		'startup_id' => $data['startup_id'],
		'url' => $data['url'],
		'image' => $data['image_name'],
		'description' => $data['description'],
		'source' => $data['source']
    ]);
//$startups[$i]['startup_news_count']
    $startup = Startup::where('startup_id', $data['startup_id'])->select("startups.*")
    ->withCount('startup_news')
    ->first();

    $new_html = $this->generate_news_td_html($startup->startup_news_count, $data['col_name']);//13

    return \Response::json(array('success'=>1, 'status_code'=>200, 'msg'=> "News added successfully", 'new_html' => $new_html),200);

    }
catch(\Exception $e)
    {
        return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
    }
}

/////////   Generate has Many TD HTML   ///////////////////////
public static function generate_has_many_rel_td($relations, $col_name, $column, $key, $value)
    {

        $html = '<div id="'.$col_name.'div">';
        $ccount = COUNT($relations);

        if($ccount > 0)
            {
            for($i1=0;$i1<$ccount;$i1++)
                {

    $temp_col_name = $col_name.$relations[$i1][$key];

    $html = $html.'<div class="input-group"><input type="text" class="form-control" id="'.$temp_col_name.'" value="'.$relations[$i1]['name'].'"> <span class="input-group-btn"><button type="button" id="'.$temp_col_name.'s" class="btn btn-default" onclick="update_many_relation_btn_click(this.id, '.$relations[$i1]['dynamic_db_id'].' )" ><i class="fa fa-edit"></i></button></span></div>';
                }
            }

        $html = $html.'<button type="button" class="btn btn-primary" id="'.$col_name.'0a" onclick="add_many_relation_btn_click(this.id, this.parentNode.id)"><i class="fa fa-plus"></i></button>';

        $html = $html.'</div>';
        /////////// Founders    /////////////////

        return $html;

    }
/////////


///////////     Test Function   ///////////////////////////////
public function test(Request $request)
    {
  try{
        $data = $request->all();

        // $founders = Founder::ajax_data($data);
        $investors = Investor::ajax_data($data);

        $result = [
            'data' => $data,
            //'founders' => $founders
            'investors' => $investors
        ];

        return \Response::json(array('success'=>1, 'status_code'=>200, 'result' => $result),200);

        }
    catch(\Exception $e)
        {
            return \Response::json(array('success'=>0, 'status_code'=>500, 'msg'=>$e->getMessage()),200);
        }
    }

///////////     Create Search Where     ////////////////////
public function create_search_where($data)
    {

        $data_len = COUNT($data['columns']);

        $where_raw_string = '(startup_id != 0)';
        for($i=0;$i<$data_len;$i++)
            {////////////       Columns Loop    ///////////////////
                $search = null;

                if(!in_array($i,[0, 1, 2, 7, 13, 14, 15, 16, 17, 18]))
                    {
                        continue;
                    }

                $key = $data['columns'][$i]['data'];
                $search = $data['columns'][$i]['search']['value'];

                //dd($key, $search, $data_len[$i]);
                if($search == null)
                    continue;

                $where_raw_string = $where_raw_string.' AND ("'.$key.'" LIKE "%'.$search.'%")';


            }////////////       Columns Loop    ///////////////////

        return [
            'where_raw_string' => $where_raw_string
        ];

    }

///////////////     Create Investor Td HTML     /////////////////////////
public static function generate_investor_td_html($temp_investors, $col_name)
{

    $temp_count = COUNT($temp_investors);
    $investors = '<div id="'.$col_name.'5">';

    for($in1=0;$in1<$temp_count;$in1++)
        {
            $sinverstor_id = $temp_investors[$in1]['startup_investor_id'];

            $btn_text = $temp_investors[$in1]['investory_name'].' ( #'.$temp_investors[$in1]['investory_type_name'].' )';

            $investors = $investors.' <button onClick="investorUpdateClick(this.id, this.parentNode.id)" id="'.$col_name.'5_'.$sinverstor_id.'" class="btn btn-default" data-sinverstor_id="'.$sinverstor_id.'" >'.$btn_text.'</button>';
        }

        $investors = $investors.'<br><button type="button" class="btn btn-primary" id="'.$col_name.'5a" onclick="add_investor_click(this.id, this.parentNode.id)"><i class="fa fa-plus"></i></button>';

    $investors = $investors.'</div>';

    return $investors;
}

///////////     Index       ////////////////////////
public static function generate_rounds_td_html($temp_rounds, $col_name)
    {
        $temp_count = COUNT($temp_rounds);

        $rounds = '<div id="'.$col_name.'6">';

        $hr = '<hr style="margin-top:4px !important;margin-bottom:4px !important;">';

        $divCss= 'style="border-radius: 15px; border: 2px solid;>"';
        $url=\Request::root();

        for($ir1=0;$ir1<$temp_count;$ir1++)
            {

                $sstartup_round_id = $temp_rounds[$ir1]['startup_round_id'];
                $arri=[];
                $arri1=[];
                $sstartup_investor = $temp_rounds[$ir1]['investors_name'];
                $arr = explode(",", $sstartup_investor);
                $invest_name=Investor::whereIn('investor_id',$arr)->get();
                foreach($invest_name as $inv){
                  $arri[]="<a href='$url/investor/$inv->slug' id='links'>".$inv->name."</a>";
                  $arri1[]=$inv->name;
                }

                $space_separated = implode(", ", $arri);
                $space_separated1 = implode(",", $arri1);


                $update=\DB::table('startup_rounds')
                ->where('startup_round_id',@$sstartup_round_id)
                ->update(array('investnames'=>@$space_separated1,'investnameslinks'=>@$space_separated));


                $rounds = $rounds.' <div id="'.$col_name.'6_'.$sstartup_round_id.'" onClick="roundUpdateClick(this.id, this.parentNode.id)" '.$divCss.' >';

            //     <td>
            //     {{ $driver->name }} ({{ $driver->phone_number }})
            //     <hr style="margin-top:4px !important;margin-bottom:4px !important;">
            //     <b>OTP -</b> {{ $driver->otp }}
            //     @if($driver->organisation != null)
            //     <hr style="margin-top:4px !important;margin-bottom:4px !important;">
            //         <b>Company - </b> {{$driver->organisation['name']}}
            //     @endif
            // </td>

    // $li1 = 'Investor - <b>'.$temp_rounds[$ir1]['investor_name'].'</b>'.$hr;
    $li1 = 'Investor - <b>'.$space_separated1.'</b>'.$hr;
    $li2 = 'Amount - <b>'.$temp_rounds[$ir1]['amount'].'</b>'.$hr;
    $li3 = 'Date - <b>'.$temp_rounds[$ir1]['dt'].'</b>'.$hr;
    $li4 = 'Round - <b>'.$temp_rounds[$ir1]['round_name'].'</b>';

                $rounds = $rounds.$li1.$li2.$li3.$li4.'</div>';

                // $btn_text = $temp_rounds[$ir1]['investor_name'].' '.$temp_rounds[$ir1]['amount'].' '.$temp_rounds[$ir1]['round_name'];

                // $rounds = $rounds.'<button onClick="roundUpdateClick(this.id, this.parentNode.id)" id="'.$col_name.'6_'.$sstartup_round_id.'" class="btn btn-default" data-sstartup_round_id="'.$sstartup_round_id.'" >'.$btn_text.'</button>';

            }

        $rounds = $rounds.'<br><button type="button" class="btn btn-primary" id="'.$col_name.'6a" onclick="add_round_click(this.id, this.parentNode.id)"><i class="fa fa-plus"></i></button>';

        $rounds = $rounds.'</div>';

        return $rounds;

    }
/////////////////////       Generate New TD     /////////////////////
public static function generate_news_td_html($temp_count, $col_name)
    {

        $news = '<div id="'.$col_name.'13">';

        $news = $news.'<a onClick="view_news(this.id, this.parentNode.id)"> View List ('.$temp_count.')</a>';
        //$li1 = 'View List - <b>'.$temp_count.'</b>';

        $news = $news.'<br><br><button type="button" class="btn btn-primary" id="'.$col_name.'13a" onclick="add_news_click(this.id, this.parentNode.id)"><i class="fa fa-plus"></i></button>';

        $news = $news.'</div>';

        return $news;

    }

/////////////////////       Generate Logo Td    //////////////////
public static function generate_logo_td_html($image, $startup_name, $col_name)
    {
        $url=\Request::root();
        $image_td = '<div id="'.$col_name.'3">';

        if($image != '')
            $image_td = $image_td.'<a href="'.$url.'/Uploads/'.$image.'" title="'.$startup_name.'" class="lightBoxGallery" data-gallery=""> <img src="'.$url.'/Uploads/'.$image.'"  class="img-circle"> </a> <br>';

        $image_td = $image_td.'<br><button type="button" class="btn btn-default" id="'.$col_name.'3a" onClick="update_logo_func(this.id, this.parentNode.id)"><i class="fa fa-edit"></i></button>';

        $image_td = $image_td.'</div>';

        return $image_td;
    }

    /////////////////////   Generate Logo Td   temp //////////////////
    public static function generate_logo_td_html_temp($image, $startup_name, $col_name)
        {

            $image_td = '<div id="'.$col_name.'3">';

            if($image != '')
                $image_td = $image_td.'<a href="'.env('IMAGE_URL').$image.'" title="'.$startup_name.'" class="lightBoxGallery" data-gallery=""> <img src="'.env('IMAGE_URL').$image.'"  class="img-circle"> </a> <br>';
            $image_td = $image_td.'</div>';

            return $image_td;
        }

    /////////////////       Generate Founder TD     ////////////////////
    public static function generate_founder_td_html($temp_founders, $col_name)
    {

        $founders = '<div id="'.$col_name.'4">';

        $temp_count = COUNT($temp_founders);
        // $founders=$temp_count;
        for($i4=0;$i4<$temp_count;$i4++)
            {
        $temp_id = $col_name.$temp_founders[$i4]['founder_id'].'_'.$temp_founders[$i4]['startup_founder_id'];

        $temp_name = $temp_founders[$i4]['name'];
        $temp_roles = $temp_founders[$i4]['roles'];
        $temp_founder_link = $temp_founders[$i4]['founder_link'];

        $founders = $founders.'<a title="'.$temp_name.'" data-name="'.$temp_name.'" data-roles="'.$temp_roles.'" data-founder_link="'.$temp_founder_link.'" class="btn btn-default" id="'.$temp_id.'" onClick="founder_update(this.id, this.parentNode.id)">'.$temp_name.' ('.$temp_roles.')</a><br>';

            }

        $founders = $founders.'<br><button type="button" class="btn btn-primary" id="'.$col_name.'4a" onClick="add_founder_click(this.id, this.parentNode.id)"><i class="fa fa-plus"></i></button>';

        $founders = $founders.'</div>';

        return $founders;

    }

    /////////////////       Generate Founder TD     ////////////////////
    public static function generate_founder_td_html_temp($temp_founders, $col_name)
    {

        $founders = '<div id="'.$col_name.'4">';

        $temp_count = COUNT($temp_founders);

        for($i4=0;$i4<$temp_count;$i4++)
            {
        $temp_id = $col_name.$temp_founders[$i4]['founder_id'].'_'.$temp_founders[$i4]['startup_founder_id'];

        $temp_name = $temp_founders[$i4]['name'];
        $temp_roles = $temp_founders[$i4]['roles'];
        $temp_founder_link = $temp_founders[$i4]['founder_link'];

        $founders = $founders.'<a title="'.$temp_name.'" data-name="'.$temp_name.'" data-roles="'.$temp_roles.'" data-founder_link="'.$temp_founder_link.'" class="btn btn-default" id="'.$temp_id.'" onClick="founder_update(this.id, this.parentNode.id)">'.$temp_name.' ('.$temp_roles.')</a><br>';

            }

        $founders = $founders.'</div>';

        return $founders;

    }

    ////////////////        Industries TD   ////////////////////////////
    public static function generate_industries_td_html($temp_industries, $col_name)
    {//12

        $industries = '<div id="'.$col_name.'12p">';

        $temp_count = COUNT($temp_industries);

        for($i4=0;$i4<$temp_count;$i4++)
            {
        $temp_id = $col_name.$temp_industries[$i4]['industry_id'];

        $industries = $industries.'<a class="btn btn-default" id="'.$temp_id.'" onClick="founder_industries(this.id, this.parentNode.id)">'.$temp_industries[$i4]['name'].'</a> ';

            }

        $industries = $industries.'</div>';

        return $industries;

    }


}

<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
///////////			Image Uploader 		//////////////////////
public static function image_uploader($image)
	{
	try{

	  	$extention = $image->GetClientOriginalExtension();
	    $filename = substr(sha1(time().time()), 0, 40) . str_random(25).".{$extention}";

		// $s3 = \AWS::createClient('s3');

			// $upload_success = $s3->putObject(array(
			// 		'ACL' => 'public-read',
			// 	    'Bucket'     => env('AWS_BUCKET'),
			// 		'Key'    => 'Uploads/'.$filename,
			// 		'Body'   => fopen($image->getPathname(), 'r'),
			// 		'ContentType' => 'image/'.$extention,
			// ));

		$upload_success = $image->move(public_path() . '/Uploads', $filename);

		return $upload_success ? $filename : '';

		}
	catch(\Exception $e)
		{
			return '';
		}
	}

///////////////		Save Image From URL 	//////////////////////
public static function save_image_from_url($url)
{

	$filename = substr(sha1(time().time()), 0, 25) . str_random(35).".jpeg";

	$ch = curl_init();
	curl_setopt($ch, CURLOPT_URL,$url);
	curl_setopt($ch, CURLOPT_TIMEOUT, 30);
	curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
	curl_setopt($ch, CURLOPT_HTTPAUTH, CURLAUTH_ANY);
			
	$response = curl_exec ($ch);
	$status_code = curl_getinfo($ch, CURLINFO_HTTP_CODE);   //get status code

	curl_close($ch);

	//$upload_success = file_put_contents('Uploads/'.$filename, $response);
	//$upload_success = $image->move(public_path() . '/Uploads', $filename);
	$upload_success = file_put_contents(public_path().'/Uploads/'.$filename, $response);

	return $upload_success ? $filename : '';

}

}

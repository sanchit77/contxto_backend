<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Country;
use App\Models\Industry;
use App\Models\Investor;
use App\Models\InvestorTemp;
use App\Models\News;
use App\Models\Founder;
use App\Models\Startup;
use App\Models\StartupTemp;
use App\Models\FounderTemp;
use App\Models\City;
use App\Models\StartupInvestor;
use App\Models\StartupInvestorTemp;
use App\Models\StartupRound;
use App\Models\StartupNews;
use App\Models\StartupIndustry;
use Mail;
use DB;
use Carbon\Carbon;
use Validator;
use Redirect;
use View;
use App\Jobs\NewCompanyRequest;
use GuzzleHttp\Client;

class WebController extends Controller
{
	/*
		home with the filters
		default startups
		filters : country[t] city[t] industry[t] valuation size
	*/
//,$keyword = null
    public function index(Request $request,$country=null,$city=null,$industry=null,$keyword=null){
      // return response()->json(array('part'=>$request->input('country_id')));
      $rand=rand();
    	$page   = $request->input('page',0);
        $news_page = $request->input('news_page',0);
        $inv_page  = $request->input('inv_page',0);
        $fnd_page  = $request->input('fnd_page',0);
    	$limit  = $request->input('limit',10);
        if ($request->ajax()) {
            $keyword= $request->input('keyword') ?? '';
        }else{
            $keyword= $keyword ?? '';
        }
    	$cities = array();

      if($request->input('industries')){
    	$industries_str = $request->input('industries') ?? '';
    }
    else if(@$industry){

      $induss = Industry::select('industry_id','name')->where('slug',@$industry)->first();
      $industries_str = @$induss->industry_id;
    }
    else{
      	$industries_str = $request->input('industries') ?? '';
    }

    	// $industries_str = $request->input('industries') ?? '';

    // $industries_str = $request->input('industries') ?? '';
        $industries_arr = [];
        if($industries_str!=''){
            $industries_arr = explode(',', $industries_str);
        }

        $valuations = array(); // we dont have any value for this in DB when it will updated ?
    	$sizes      = array();
    if($request->input('city_id')){
    	$city_id = $request->input('city_id') ?? 0;
    }
    else if(@$city){
      $citys = City::select('city_id','name')->where('slug',@$city)->first();
      $city_id = @$citys->city_id;
    }
    else{
      	$city_id = $request->input('city_id') ?? 0;
    }

    	// $city_id = $request->input('city_id') ?? 0;
      // return response()->json(array('part'=>$request->input('country_id')));
      if($request->input('country_id')){


        $country_id = $request->input('country_id') ?? 0;
      }
      else if(@$country){
        	$county = Country::select('country_id','name')->where('slug',@$country)->first();
          $country_id = @$county->country_id;
      }
      else if($request->input('country_id')==0){
          $country_id='';
      }
      else{
        $country_id = $request->input('country_id') ?? 0;
      }
// return response()->json(array('part'=>$request->input('country_id')));
  // $country_id = $request->input('country_id') ?? 0;
      // $country_id = $request->input('country_id') ?? 0;

        //$country_name  = '';
        $city_name     = '';
        $industry_name = '';
        $investor_name = '';
    	$industry_id= $request->input('industry_id',null);
        $investor_id= $request->input('investor_id',null);
        /*if($country_id){
            $selected_country = Country::where('country_id',$country_id)->first();
            $country_name = $selected_country->name;
        }*/

        /*if($city_id){
            $selected_city = City::where('city_id',$city_id)->first();
            $city_name     = $selected_city->name;
        }*/

        if($industry_id){
            $selected_industry = Industry::where('industry_id',$industry_id)->first();
            $industry_name = $selected_industry->name;


        }

        if($investor_id){
            $selected_investor = Investor::where('investor_id',$investor_id)->first();
            $investor_name = $selected_investor->name;

        }

        // only latin american //
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                // 'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                // 'Saint-Barthelemy',
                // 'Saint-Martin'
            );

    	$countries = Country::select('country_id','name')
                ->withCount([
                    'startups' => function($q){
                        $q->where('is_public',1);
                    }
                ])
                ->whereIn('name',$latin_countries)
                ->orderBy('name')
                ->get();
                // $selectcountries = Country::select('country_id','name')
                //           ->withCount([
                //               'startups' => function($q){
                //                   $q->where('is_public',1);
                //               }
                //           ])
                //           ->whereIn('name',$latin_countries)
                //           ->orderBy('name')
                //           ->get();
    	if($country_id){
    		$cities = City::where('country_id',$country_id)->select('city_id','name')->where('is_blocked',0)->where('slug','!=','')->orderBy('name')->get();
    	}


    	// return data if any filter set //
        $q = Startup::with(['country','city','founders','investors','startup_industries']);
        if($keyword){
            $q->where('name','like','%'.$keyword.'%')->orWhere('slug','like','%'.$keyword.'%');
        }

        if($country_id){
          // dd($country_id);
            $q->where('country_id',$country_id);
            /*$q->whereHas('startup_industries',function($qq) use($country_id){
                $qq->where('country_id',$country_id);
            });*/
            $countrie=Country::where('country_id',$country_id)->first();
            $country_slug=@$countrie->slug;
            $country_name=@$countrie->name;
            if($city_id){
                $cit=City::where('city_id',$city_id)->where('country_id',@$country_id)->first();
                $city_id=@$cit->city_id;
            }
        }

        //if($industry_id){
        if(count($industries_arr)>0){
            $q->whereHas('startup_industries',function($qq)use($industries_arr){
                $qq->whereIn('industry_id',$industries_arr);
            });
            $indus=Industry::where('industry_id',$industries_arr)->first();
            $indus_name=@$indus->name;
            $indus_slug=@$indus->slug;
        }

        if($investor_id){
            $q->whereHas('investors',function($qq) use($investor_id){
                $qq->where('investor_id',$investor_id);
            });
        }

        $cits = City::where('country_id',$country_id)->first();

        if(@$cits){

        if($city_id){
          // return response()->json(array('part'=>$city_id));
            $q->where('city_id',$city_id)->where('country_id',@$country_id);
            $citie=City::where('city_id',$city_id)->first();
            $city_name=@$citie->name;

            if($city_id==0){
              $city_slug="";
            }
            else{
            $city_slug=@$citie->slug;
          }
        }
      }
      else{

        $city_slug="";
      }

    	$startups = $q->where('is_public',1)->orderBy('name','asc')->paginate(20);


      // dd($startups);
        //dd($startups);
        //dd($startups);

        // get the recent fundings top 10//

        //$fundings = StartupInvestor::with(['startup.country','startup.city','investor'])->orderBy('created_at','DESC')->limit(10)->get();

        /*function nice_number($n) {
                // first strip any formatting;
                $n = (0+str_replace(",", "", $n));

                // is this a number?
                if (!is_numeric($n)) return false;

                // now filter it;
                if ($n > 1000000000000) return round(($n/1000000000000), 2).'T';
                elseif ($n > 1000000000) return round(($n/1000000000), 2).'B';
                elseif ($n > 1000000) return round(($n/1000000), 2).'M';
                elseif ($n > 1000) return round(($n/1000), 2).'K';

                return number_format($n);
            }

            $num = nice_number('20001111'); //14.12 million
            dd($num);*/

        $fundings = StartupRound::select(
                '*',
                DB::RAW('(CASE
                            WHEN amount > 1000000000000 THEN CONCAT(round((amount/1000000000000),1),"T")
                            WHEN amount > 1000000000 THEN CONCAT(round((amount/1000000000),1),"B")
                            WHEN amount > 1000000 THEN CONCAT(round((amount/1000000),1),"M")
                            WHEN amount > 1000 THEN CONCAT(round((amount/1000), 1),"K")
                            ELSE amount
                        END) as amount')
            )->with([
            'startup'
            /*'round',
            'investor',
            'startup.country',
            'startup.city'*/
            ])->orderBy('dt','DESC')->limit(10)->get();


       $title='Company Listing | Contxto';
       $description='Contxto is the leading destination for company insights from early-stage startups. Get insights into your competition. Uncover startup trends, get company funding data. Find new prospects, beat competitors and quotas.';
       $url=$request->url();
       $image_link='https://www.contxto.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-04-at-1.53.19-PM.jpeg';
       $imagename='Contxto Logo';
        // for create form //
        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }

        $industries = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();

        $founders       = Founder::select('founder_id','name')->orderBy('name')->get();
        $investors      = Investor::select('investor_id','name')->orderBy('name')->get();

        $industries_tags= Industry::pluck('name');

        $country=Country::select('name')->where('slug',@$country)->first();
        $industry=Industry::select('name')->where('slug',@$industry)->first();

        $city=City::select('name')->where('slug',@$city)->first();

        /*
            Get the lastest added news
        */
        $news = StartupNews::with(['startup'])->orderBy('created_at','DESC')->where('is_blocked','0')->paginate(3,['*'],'news_page');
        $news->setPageName('news_page');

        $founders_data = Founder::orderBy('name')->paginate(12,['*'],'fnd_page');
        $founders_data->setPageName('fnd_page');

        $investors_data = Investor::orderBy('name')->paginate(12,['*'],'inv_page');
        $investors_data->setPageName('inv_page');

        $foundComp = $startups->total().' '.($startups->total()>1 ? ' Companies' : ' Company').' found';
        if ($request->ajax()) {
            $view = View::make('elements.company-list', compact('startups','keyword','foundComp','country_id','city_id','industries_str'));
            $html = $view->render();

            //return view('elements.company-list', compact('startups','keyword','foundComp'))->with(['foundComp']);
            //$html = view('elements.company-list', compact('startups','keyword','foundComp'));
            return response()->json(array('success'=>1,'html'=>$html,'keyword'=>$keyword,'cityname'=>@$city_name,'cityslug'=>@$city_slug,'countryname'=>@$country_name,'countryslug'=>@$country_slug,'indusname'=>@$indus_name,'indusslug'=>@$indus_slug,'foundComp'=>$foundComp,'title'=>$title,'description'=>$description,
            'url'=>$url,'image_link'=>$image_link,'imagename'=>$imagename,'city_id'=>$city_id), 200);
        }
        // dd($countriess);
        // $ind_name = Industry::where('slug',@$industry)->first();
        // if($ind_name){
        // @$ind_name1 = @$ind_name->industry_id;
        // }
        // else{
        // @$ind_name1='';
        // }

    	//return view('index')->with(compact('countries','cities','industries','sizes','valuations','country_id','city_id','industry_id','valuation_id','size_id','startups','keyword','country_name','city_name','industry_name','investor_name','fundings','founders','investors','founded_years','industries_tags','news','investors_data','founders_data'));
        return view('index')->with(compact('city','industry','country','title','description','url','image_link','imagename','rand','countries','cities','startups','keyword','foundComp','fundings','country_id','city_id','industries','industries_str','founded_years','founders'));
    }



    public function getIndustry(Request $request,$name=null){


    	$page   = $request->input('page',0);
        $news_page = $request->input('news_page',0);
        $inv_page  = $request->input('inv_page',0);
        $fnd_page  = $request->input('fnd_page',0);
    	$limit  = $request->input('limit',10);
        if ($request->ajax()) {
            $keyword= $request->input('keyword') ?? '';
        }else{
            $keyword= $keyword ?? '';
        }
    	$cities = array();
    	$industries_str = $request->input('industries') ?? '';
        $industries_arr = [];
        if($industries_str!=''){
            $industries_arr = explode(',', $industries_str);
        }

        $valuations = array(); // we dont have any value for this in DB when it will updated ?
    	$sizes      = array();
    	$city_id = $request->input('city_id') ?? 0;
    	$country_id = $request->input('country_id') ?? 0;
        //$country_name  = '';
        $city_name     = '';
        $industry_name = '';
        $investor_name = '';
    	$industry_id= $request->input('industry_id',null);
        $investor_id= $request->input('investor_id',null);

        /*if($country_id){
            $selected_country = Country::where('country_id',$country_id)->first();
            $country_name = $selected_country->name;
        }*/

        /*if($city_id){
            $selected_city = City::where('city_id',$city_id)->first();
            $city_name     = $selected_city->name;
        }*/

        if($industry_id){
            $selected_industry = Industry::where('industry_id',$industry_id)->first();
            $industry_name = $selected_industry->name;
        }

        if($investor_id){
            $selected_investor = Investor::where('investor_id',$investor_id)->first();
            $investor_name = $selected_investor->name;
        }

        // only latin american //
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );

    	$countries = Country::select('country_id','name')
                ->withCount([
                    'startups' => function($q){
                        $q->where('is_public',1);
                    }
                ])
                ->whereIn('name',$latin_countries)
                ->orderBy('name')
                ->get();
    	if($country_id){
    		$cities = City::where('country_id',$country_id)->select('city_id','name')->where('is_blocked',0)->orderBy('name')->get();
    	}


    	// return data if any filter set //
        $q = Startup::with(['country','city','founders','investors','startup_industries']);
        if($keyword){
            $q->where('name','like','%'.$keyword.'%');
        }
        if($name){
          $industry=Industry::where('slug',@$name)->first();
          $q->whereHas('startup_industries',function($qq)use($industry){
              $qq->whereIn('industry_id',$industry);
                });

        }
        if($country_id){
            $q->where('country_id',$country_id);
            /*$q->whereHas('startup_industries',function($qq) use($country_id){
                $qq->where('country_id',$country_id);
            });*/
        }

        //if($industry_id){
        if(count($industries_arr)>0){
            $q->whereHas('startup_industries',function($qq)use($industries_arr){
                $qq->whereIn('industry_id',$industries_arr);
            });
        }

        if($investor_id){
            $q->whereHas('investors',function($qq) use($investor_id){
                $qq->where('investor_id',$investor_id);
            });
        }

        if($city_id){
            $q->where('city_id',$city_id);
        }

    	$startups = $q->where('is_public',1)->orderBy('name','asc')->paginate(5);
        //dd($startups);
        //dd($startups);

        // get the recent fundings top 10//

        //$fundings = StartupInvestor::with(['startup.country','startup.city','investor'])->orderBy('created_at','DESC')->limit(10)->get();

        /*function nice_number($n) {
                // first strip any formatting;
                $n = (0+str_replace(",", "", $n));

                // is this a number?
                if (!is_numeric($n)) return false;

                // now filter it;
                if ($n > 1000000000000) return round(($n/1000000000000), 2).'T';
                elseif ($n > 1000000000) return round(($n/1000000000), 2).'B';
                elseif ($n > 1000000) return round(($n/1000000), 2).'M';
                elseif ($n > 1000) return round(($n/1000), 2).'K';

                return number_format($n);
            }

            $num = nice_number('20001111'); //14.12 million
            dd($num);*/

        $fundings = StartupRound::select(
                '*',
                DB::RAW('(CASE
                            WHEN amount > 1000000000000 THEN CONCAT(round((amount/1000000000000),1),"T")
                            WHEN amount > 1000000000 THEN CONCAT(round((amount/1000000000),1),"B")
                            WHEN amount > 1000000 THEN CONCAT(round((amount/1000000),1),"M")
                            WHEN amount > 1000 THEN CONCAT(round((amount/1000), 1),"K")
                            ELSE amount
                        END) as amount')
            )->with([
            'startup'
            /*'round',
            'investor',
            'startup.country',
            'startup.city'*/
            ])->orderBy('dt','DESC')->limit(10)->get();



        // for create form //
        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }
        $industries = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();
        $founders       = Founder::select('founder_id','name')->orderBy('name')->get();
        $investors      = Investor::select('investor_id','name')->orderBy('name')->get();

        $industries_tags= Industry::pluck('name');

        /*
            Get the lastest added news
        */
        $news = StartupNews::with(['startup'])->orderBy('created_at','DESC')->where('is_blocked','0')->paginate(3,['*'],'news_page');
        $news->setPageName('news_page');

        $founders_data = Founder::orderBy('name')->paginate(12,['*'],'fnd_page');
        $founders_data->setPageName('fnd_page');

        $investors_data = Investor::orderBy('name')->paginate(12,['*'],'inv_page');
        $investors_data->setPageName('inv_page');

        $foundComp = $startups->total().' '.($startups->total()>1 ? ' Companies' : ' Company').' found';
        if ($request->ajax()) {
            $view = View::make('elements.company-list', compact('startups','keyword','foundComp','country_id','city_id','industries_str'));
            $html = $view->render();

            //return view('elements.company-list', compact('startups','keyword','foundComp'))->with(['foundComp']);
            //$html = view('elements.company-list', compact('startups','keyword','foundComp'));
            return response()->json(array('success'=>0,'html'=>$html,'foundComp'=>$foundComp), 200);
        }



    	//return view('index')->with(compact('countries','cities','industries','sizes','valuations','country_id','city_id','industry_id','valuation_id','size_id','startups','keyword','country_name','city_name','industry_name','investor_name','fundings','founders','investors','founded_years','industries_tags','news','investors_data','founders_data'));
        return view('index')->with(compact('countries','cities','startups','keyword','foundComp','fundings','country_id','city_id','industries','industries_str','founded_years','founders'));
    }

    /*
        investors
    */
    public function investors(Request $request,$country=null,$city=null,$industry=null,$keyword = null){

        if ($request->ajax()) {
            $keyword= $request->input('keyword') ?? '';
        }else{
            $keyword= $keyword ?? '';
        }
        	$cities = array();
        // $country_id = $request->input('country_id') ?? 0;
        if($request->input('industries')){
        $industries_str = $request->input('industries') ?? '';
        }
        else if(@$industry){
          $induss = Industry::select('industry_id','name')->where('slug',@$industry)->first();
          $industries_str = @$induss->industry_id;
        }
        else{
          $industries_str = $request->input('industries') ?? '';
        }
        $industries_arr = [];
        if($industries_str!=''){
            $industries_arr = explode(',', $industries_str);
        }

        $city_id = $request->input('city_id') ?? 0;
        if($request->input('country_id')){
        $country_id = $request->input('country_id') ?? 0;
        }
        else if(@$country){
          $county = Country::select('country_id','name')->where('slug',@$country)->first();
          $country_id = @$county->country_id;
        }
        else{
          $country_id = $request->input('country_id') ?? 0;
        }
        $sort = @$request->input('sort') ?? 0;

        // only latin american //
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );

            $countries = Country::select('country_id','name')
            ->withCount([
                'investors' => function($q){
                    $q->where('is_blocked',0);
                }
            ])
                      ->whereIn('name',$latin_countries)
                      ->orderBy('name')
                      ->get();



        // return data if any filter set //
        $q = Investor::with(['country','city','founders','investors','startup_industries']);

        $q = Investor::withCount([
            'startup_inverstors'
        ]);

        if($country_id){
      		$cities = City::where('country_id',$country_id)->select('city_id','name')->where('is_blocked',0)->orderBy('name')->get();
      	}
        if($keyword){
            $q->where('name','like','%'.$keyword.'%');
        }

        if($country_id){
             $q->where('country_id',$country_id);
             $countri=Country::where('country_id',$country_id)->first();

             $country_slug=@$countri->slug;
             $country_name=@$countri->name;
         }

         if(count($industries_arr)>0){
             $q->whereHas('startup_inverstors',function($qq)use($industries_arr){
                $qq->whereIn('investor_id',$industries_arr);
             });
             $indus=Industry::where('industry_id',$industries_arr)->first();
             $indus_name=@$indus->name;
             $indus_slug=@$indus->slug;
      }

      if($city_id){
          $q->where('city_id',$city_id);
          $cities=City::where('city_id',$city_id)->first();
          $city_name=@$cities->name;
          $city_slug=@$cities->slug;
      }
        if($sort==1){
          $investors = $q->orderBy('startup_inverstors_count','asc')->paginate(10);
        }
        else if($sort==2){
          $investors = $q->orderBy('startup_inverstors_count','desc')->paginate(10);
        }
        else{

          $investors = $q->orderBy('name','asc')->paginate(5);
        }

        //dd($investors);

        $fundings = StartupRound::select(
                '*',
                DB::RAW('(CASE
                            WHEN amount > 1000000000000 THEN CONCAT(round((amount/1000000000000),1),"T")
                            WHEN amount > 1000000000 THEN CONCAT(round((amount/1000000000),1),"B")
                            WHEN amount > 1000000 THEN CONCAT(round((amount/1000000),1),"M")
                            WHEN amount > 1000 THEN CONCAT(round((amount/1000), 1),"K")
                            ELSE amount
                        END) as amount')
            )->with([
            'startup'
            ])->orderBy('dt','DESC')->limit(10)->get();

            $country=Country::select('name')->where('slug',@$country)->first();
            $industry=Industry::select('name')->where('slug',@$industry)->first();

        $industries = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();

        $foundInvestors = $investors->total().' '.($investors->total()>1 ? ' Investors' : ' Investor').' found';

        if ($request->ajax()) {

            $view = View::make('elements.investors-list', compact('investors','keyword','foundInvestors','country_id','city_id','industries_str'));
            $html = $view->render();
            return response()->json(array('success'=>0,'html'=>$html,'keyword'=>$keyword,'cityname'=>@$city_name,'cityslug'=>@$city_slug,'countryname'=>@$country_name,'countryslug'=>@$country_slug,'indusname'=>@$indus_name,'indusslug'=>@$indus_slug,'foundInvestors'=>$foundInvestors), 200);
        }

        $title='Investors Listing | Contxto';
        $description='Contxto is the leading destination for company insights from early-stage startups. Get insights into your competition. Uncover startup trends, get company funding data. Find new prospects, beat competitors and quotas.';
        $url=$request->url();
        $image_link='https://www.contxto.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-04-at-1.53.19-PM.jpeg';
        $imagename='Contxto Logo';

        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }
        return view('investors')->with(compact('country','industry','title','description','url','image_link','imagename','cities','countries','investors','keyword','foundInvestors','fundings','country_id','city_id','industries','industries_str','founded_years'));
    }

    /*
        founders
    */
    public function founders(Request $request,$country=null,$city=null,$industry=null,$keyword = null){
        if ($request->ajax()) {
            $keyword= $request->input('keyword') ?? '';
        }else{
            $keyword= $keyword ?? '';
        }
        $cities = array();
        if($request->input('industries')){
            $industries_str = $request->input('industries') ?? '';
        }
        else if(@$industry){
          $induss = Industry::select('industry_id','name')->where('slug',@$industry)->first();
          $industries_str = @$induss->industry_id;
        }
        else{
            $industries_str = $request->input('industries') ?? '';
        }

        $industries_arr = [];
        if($industries_str!=''){
            $industries_arr = explode(',', $industries_str);
        }

        $city_id = $request->input('city_id') ?? 0;
        if($request->input('country_id')){
        $country_id = $request->input('country_id') ?? 0;
        }
        else if(@$country){
          $county = Country::select('country_id','name')->where('slug',@$country)->first();
          $country_id = @$county->country_id;
        }
        else{
            $country_id = $request->input('country_id') ?? 0;
        }
        $sort=$request->input('sort') ?? 0;
        // only latin american //
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );
            $countries = Country::select('country_id','name')
            ->withCount([
                'founders' => function($q){
                    $q->where('is_blocked',0);
                }
            ])
                      ->whereIn('name',$latin_countries)
                      ->orderBy('name')
                      ->get();
        // $countries = Country::select('country_id','name')->whereIn('name',$latin_countries)->orderBy('name')->get();

        // return data if any filter set //

        $q = Founder::with(['country']);

        $q = Founder::withCount([
            'startup_founders'
        ]);

        if($keyword){
            $q->where('name','like','%'.$keyword.'%');
        }

        if($country_id){
            $q->where('country_id',$country_id);
            $countri=Country::where('country_id',$country_id)->first();
            $country_slug=@$countri->slug;
            $country_name=@$countri->name;
        }

        if(count($industries_arr)>0){

            $q->whereHas('startup_founders',function($qq)use($industries_arr){
                $qq->whereIn('founder_id',$industries_arr);
            });
            $indus=Industry::where('industry_id',$industries_arr)->first();
            $indus_name=@$indus->name;
            $indus_slug=@$indus->slug;
        }
        if($sort==1){
          $founders = $q->orderBy('startup_founders_count','asc')->paginate(10);
        }
        else if($sort==2){
          $founders = $q->orderBy('startup_founders_count','desc')->paginate(10);
        }
        else{

          $founders = $q->orderBy('name','asc')->paginate(5);
        }
        // $founders = $q->orderBy('name','asc')->paginate(5);
        //dd($founders);
  // dd($founders);
        $fundings = StartupRound::select(
                '*',
                DB::RAW('(CASE
                            WHEN amount > 1000000000000 THEN CONCAT(round((amount/1000000000000),1),"T")
                            WHEN amount > 1000000000 THEN CONCAT(round((amount/1000000000),1),"B")
                            WHEN amount > 1000000 THEN CONCAT(round((amount/1000000),1),"M")
                            WHEN amount > 1000 THEN CONCAT(round((amount/1000), 1),"K")
                            ELSE amount
                        END) as amount')
            )->with([
                'startup',
                'round'
            ])->orderBy('dt','DESC')->limit(10)->get();

            $country=Country::select('name')->where('slug',@$country)->first();
            $industry=Industry::select('name')->where('slug',@$industry)->first();

        $industries = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();

        $foundFounders = $founders->total().' '.($founders->total()>1 ? ' Founders' : ' Founder').' found';
        if ($request->ajax()) {
            $view = View::make('elements.founders-list', compact('founders','keyword','foundFounders','country_id','city_id','industries_str'));
            $html = $view->render();
            return response()->json(array('success'=>0,'html'=>$html,'keyword'=>$keyword,'countryname'=>@$country_name,'countryslug'=>@$country_slug,'indusname'=>@$indus_name,'indusslug'=>@$indus_slug,'foundFounders'=>$foundFounders), 200);
        }


        $title='Founders Listing | Contxto';
        $description='Contxto is the leading destination for company insights from early-stage startups. Get insights into your competition. Uncover startup trends, get company funding data. Find new prospects, beat competitors and quotas.';
        $url=$request->url();
        $image_link='https://www.contxto.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-04-at-1.53.19-PM.jpeg';
        $imagename='Contxto Logo';

        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }

        return view('founders')->with(compact('country','industry','title','description','url','image_link','imagename','countries','cities','founders','keyword','foundFounders','fundings','country_id','city_id','industries','industries_str','founded_years'));
    }


    /*
        Get the company by name and show the details
    */
    public function getCompanyDetails(Request $request,$company_name){

        $company = Startup::with([
                        'country',
                        'city',
                        'founders',
                        'investors',
                        'startup_industries',
                        'startup_founders',
                        'startup_rounds_cal' => function($query){
                            $query->select(
                                '*',
                                DB::RAW('(CASE
                                    WHEN amount > 1000000000000 THEN CONCAT(round((amount/1000000000000),1),"T")
                                    WHEN amount > 1000000000 THEN CONCAT(round((amount/1000000000),1),"B")
                                    WHEN amount > 1000000 THEN CONCAT(round((amount/1000000),1)," M")
                                    WHEN amount > 1000 THEN CONCAT(round((amount/1000), 1),"K")
                                    ELSE amount
                                END) as amount')
                            )
                            ->with([
                                'investor',
                                'round'
                            ]);
                        },
                        'startup_news' => function($query){
                            $query->take(3)->orderBy('startup_news_id','desc');
                        },
                        'startup_rounds_sum' => function($query){
                            $query->with([
                                'round'
                            ])
                            ->whereHas('round',function($query){
                                $query->where('is_blocked',0);
                            });
                        }
                    ])
                    ->withCount([
                        'startup_news'
                    ])
                    ->where('slug',$company_name)
                    ->orWhere('name',$company_name)
                    ->first();

                    if($company){
        if($company->startup_rounds_sum){
            $amount_sum = $company->startup_rounds_sum->pluck('amount')->sum();
        }else{
            $amount_sum = 0;
        }

        $investors = Investor::join('startup_investors','startup_investors.investor_id','=','investors.investor_id')->select('investors.investor_id','investors.name')->where('startup_investors.startup_id',$company->startup_id)->orderBy('name')->get();
        //dd($investors);

        // similar companies [same type industries]//
        $industries_array = DB::table('startup_industries')->where('startup_id',$company->startup_id)->pluck('industry_id')->all();

        $similar_companies= Startup::whereIn('startup_id',function($q) use($industries_array){
            $q->select('startup_id')->from('startup_industries')->whereIn('industry_id',$industries_array);
        })->where('startup_id','<>',$company->startup_id)->take(6)->get();

        //dd($similar_companies);

        // for create form //
        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }
        $industries     = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();
        $founders       = Founder::select('founder_id','name')->orderBy('name')->get();
        $industries_tags= Industry::pluck('name');

        $countries = Country::select('country_id','name')->orderBy('name')->get();
        $title = $company->name.' | Contxto';
        $description = $company->description;
        if($company->startup_news->last()){
            $last_news_id=$company->startup_news->last()->startup_news_id;
        }else{
            $last_news_id=0;
        }

        // only latin american //
        $latin_countries = array(
                'Belize',
                'Costa Rica',
                'El Salvador',
                'Guatemala',
                'Honduras',
                'Mexico',
                'Nicaragau',
                'Panama',
                'Argentina',
                'Bolivia',
                'Brazil',
                'Chile',
                'Colombia',
                'Ecuador',
                'French Guiana',
                'Guyana',
                'Paraguay',
                'Peru',
                'Suriname',
                'Uruguay',
                'Venezuela',
                'Cuba',
                'Dominican Republic',
                'Haiti',
                'Guadeloupe',
                'Martinique',
                'Puerto Rico',
                'Saint-Barthelemy',
                'Saint-Martin'
            );

        $countries = Country::select('country_id','name')->whereIn('name',$latin_countries)->orderBy('name')->get();

        //NEWS API URL
        $url = 'https://contxto.com/wp-json/wp/v2/posts?search='.$company->name;
        //create a new cURL resource
        $ch = curl_init($url);
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute the POST request
        $result = curl_exec($ch);
        //close cURL resource
        curl_close($ch);
        $all_news = json_decode($result);

        // dd($company->startup_rounds_cal[0]->investors_name);
        foreach($company->startup_rounds_cal as $comp){


          // dd($comp->investors_name);
          $arr = explode(",", $comp->investors_name);

          $invest_name=Investor::whereIn('investor_id',$arr)->get();


          //  dd($invest_name[0]->Investor);
          foreach($invest_name as $arr){
            $arri=$arr->name;
          }
          //
           // print_r($arri);
          // return response()->json(array('part'=>$arri));

        }

        //return view('company-details')->with(compact('company','similar_companies','founded_years','industries','founders','investors','countries','industries_tags','title','description'));
        return view('company-details')->with(compact('invest_name','arri','title','company','similar_companies','last_news_id','amount_sum','founded_years','countries','industries','all_news'));
      }
      else{
        return redirect('errors/404');
      }
    }



    /*
        Get cities
    */
    public function getCities(Request $request){
        $country_id = $request->input('country_id') ?? 0;
        $cities = City::where('country_id',$country_id)->select('city_id','name')->where('is_blocked',0)->orderBy('name')->get();
        /*$view = View::make('elements.cities', compact('cities'));
        $html = $view->render();*/
        return view('elements.cities', compact('cities'));
    }

    /*
        Get the company news
    */
    public function getCompanyNews(Request $request){
        $input = $request->all();
        //validation rules
        $rules = array(
                    'startup_id' => 'required|exists:startups,startup_id',
                    'last_news_id' => 'required|exists:startup_news,startup_news_id'
                );
        //validate input
        $validation = Validator::make($input,$rules);
        if($validation->fails()){
            return response()->json(array('success'=>0,'statusCode'=>400,'msg'=>$validation->getMessageBag()->first()), 400);
        }
        $news = StartupNews::where('startup_id',$input['startup_id'])
                        ->where('startup_news_id','<',$input['last_news_id'])
                        ->take(3)
                        ->orderBy('startup_news_id','desc')
                        ->get();
                        //dd($news);
        if(count($news)>0){
            $last_news_id=$news->last()->startup_news_id;
        }else{
            $last_news_id=0;
        }
        if(!empty($input['more_count']) && $input['more_count']>=3){
            $more_count = $input['more_count']-3;
        }else{
            $more_count = 0;
        }
        //$news = $news->reverse();
        $news_html='';
        $placeholder=asset('web/img/news_placeholder.png');
        if(count($news)>0){
            $news_html.='<div class="row">';
            foreach ($news as $key => $val) {
                $news_html.='<div class="col-lg-4 col-md-6 col-12"><div class="newsBlk" id="'.$val->startup_news_id.'">
                        <div class="figBlock">
                            <a href="'.$val->url.'" target="_blank">';
                    if($val->image){
                        $news_html.='<img src="'.$val->image.'" alt="img">';
                    }
                    else{
                        $news_html.='<img src="'.$placeholder.'" alt="img">';
                    }
                    $news_html.='</a>
                             </div>
                             <div class="time"> <span class="greenIcon"> <span></span> </span>'.$val->created_at->diffForHumans().'</div>
                             <div class="newsText">'.$val->description.'
                             </div>
                          </div></div>';
            }
            $news_html.='</div>';
        }

        //return response
        return response()->json(array('success'=>1,'statusCode'=>200,'msg'=>'News', 'news'=>$news_html, 'last_news_id'=>$last_news_id, 'more_count'=>$more_count), 200);
    }

    /*public function getCompanyNews(Request $request){
        $input = $request->all();
        //validation rules
        $rules = array(
                    'startup_id' => 'required|exists:startups,startup_id',
                    'last_news_id' => 'required|exists:startup_news,startup_news_id'
                );
        //validate input
        $validation = Validator::make($input,$rules);
        if($validation->fails()){
            return response()->json(array('success'=>0,'statusCode'=>400,'msg'=>$validation->getMessageBag()->first()), 400);
        }
        $news = StartupNews::where('startup_id',$input['startup_id'])
                        ->where('startup_news_id','<',$input['last_news_id'])
                        ->take(3)
                        ->orderBy('startup_news_id','desc')
                        ->get();
                        //dd($news);
        if(count($news)>0){
            $last_news_id=$news->last()->startup_news_id;
        }else{
            $last_news_id=0;
        }
        if(!empty($input['more_count']) && $input['more_count']>=3){
            $more_count = $input['more_count']-3;
        }else{
            $more_count = 0;
        }
        //$news = $news->reverse();
        $news_html='';
        $placeholder=asset('web/img/news_placeholder.png');
        if(count($news)>0){
            $news_html.='<div class="row">';
            foreach ($news as $key => $val) {
                $news_html.='<div class="col-lg-4 col-md-6 col-12"><div class="newsBlk" id="'.$val->startup_news_id.'">
                        <div class="figBlock">
                            <a href="'.$val->url.'" target="_blank">';
                    if($val->image){
                        $news_html.='<img src="'.$val->image.'" alt="img">';
                    }
                    else{
                        $news_html.='<img src="'.$placeholder.'" alt="img">';
                    }
                    $news_html.='</a>
                             </div>
                             <div class="time"> <span class="greenIcon"> <span></span> </span>'.$val->created_at->diffForHumans().'</div>
                             <div class="newsText">'.$val->description.'
                             </div>
                          </div></div>';
            }
            $news_html.='</div>';
        }

        //return response
        return response()->json(array('success'=>1,'statusCode'=>200,'msg'=>'News', 'news'=>$news_html, 'last_news_id'=>$last_news_id, 'more_count'=>$more_count), 200);
    }
*/
    /*
        Get the investor details by the name and show the other companies invested in
    */

    public function getInvestorDetails(Request $request, $investor_name){
        $investor = Investor::where('slug',$investor_name)->first();

        if(!$investor)
            return Redirect::route('web-home');
            DB::enableQueryLog(); // Enable query log
        $investor_companies = Startup::whereIn('startup_id',function($q) use($investor){
            $q->select('startup_id')->from('startup_investors')->where('investor_id',$investor->investor_id);
        })->get();
        // dd(DB::getQueryLog());
        $total_invests=StartupRound::where('investors_name','Like','%'.$investor->investor_id.'%')->sum('amount');
        // $country_wise=StartupRound::where('investors_name','Like','%'.$investor->investor_id.'%')->groupBy('country_id')->sum('amount');
        $country_wise=\DB::table('startup_rounds')->where('investors_name','Like','%'.$investor->investor_id.'%')
    ->select('country_id','country_name',\DB::raw('sum(amount) * 100 / (select sum(amount) from startup_rounds) as count'))
    ->groupBy('country_id','country_name')
    ->get()->toArray();


    $stage_wise=\DB::table('startup_rounds')->where('investors_name','Like','%'.$investor->investor_id.'%')
->select('round_id','round_name as name',\DB::raw('sum(amount) * 100 / (select sum(amount) from startup_rounds) as y'))
->groupBy('round_id','round_name')
->get()->toArray();
// dd($stage_wise);
$industry_wise=\DB::table('startup_rounds')->where('investors_name','Like','%'.$investor->investor_id.'%')
->select('industry_id','industry_name as name',\DB::raw('sum(amount) * 100 / (select sum(amount) from startup_rounds) as y'))
->groupBy('industry_id','industry_name')
->get()->toArray();
// dd($industry_wise);
// dd($industry_wise);

        // $total_country_wise=$country_wise/$total_invests*100;
        // $country_wise=StartupRound::
        // $countries_data = Startup::distinct('country_id')->whereIn('startup_id',function($q) use($investor){
        //     $q->select('startup_id')->from('startup_investors')->where('investor_id',$investor->investor_id);
        // })->get();
        //
        // // dd($countries_data);
        // $investor_companies2 = Startup::whereIn('startup_id',function($q) use($investor){
        //     $q->select('startup_id')->from('startup_investors')->where('investor_id',$investor->investor_id);
        // })->count();
        // $total_invests=StartupRound::where('investors_name','Like','%'.$investor->investor_id.'%')->sum('amount');
        //
        // foreach($investor_companies as $investor_companie){
        // $investor_companies1 = Startup::where('startup_id',$investor_companie->startup_id)->groupby('country_id')->count();
        // // $investor_companies3[] = StartupIndustry::where('startup_id',$investor_companie->startup_id)->groupby('startup_id')->count();
        // $totl[]=$investor_companies1/$investor_companies2*100;
        // }
        //
        // $total_invests_start[]=StartupRound::where('investors_name','Like','%'.$investor->investor_id.'%')->groupBy('startup_id')->sum('amount');
        //  // dd($total_invests_start);


        // dd($investor_companies);


        // for create form //
        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }
        $industries     = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();
        $founders       = Founder::select('founder_id','name')->orderBy('name')->get();
        $investors      = Investor::select('investor_id','name')->orderBy('name')->get();
        $industries_tags= Industry::pluck('name');

        $countries = Country::select('country_id','name')->orderBy('name')->get();

        $title = $investor->name.' | Contxto';;
        $description = $investor->name;

        //NEWS API URL
        $url = 'https://contxto.com/wp-json/wp/v2/posts?search='.$investor->slug;
        //create a new cURL resource
        $ch = curl_init($url);
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute the POST request
        $result = curl_exec($ch);
        //close cURL resource
        curl_close($ch);
        $all_news = json_decode($result);

        foreach($investor_companies as $company){
        //NEWS API URL
        $url1 = 'https://contxto.com/wp-json/wp/v2/posts?search='.$company->slug;
        //create a new cURL resource
        $ch1 = curl_init($url1);
        //set the content type to application/json
        curl_setopt($ch1, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($ch1, CURLOPT_RETURNTRANSFER, true);
        //execute the POST request
        $result1 = curl_exec($ch1);
        //close cURL resource
        curl_close($ch1);
        $all_news1 = json_decode($result1);

       }

        return view('investor-details')->with(compact('industry_wise','stage_wise','country_wise','all_news1','title','investor','investor_companies','industries','founders','investors','industries_tags','countries','founded_years','title','description','all_news','countries_data'));
    }

    public function getFounderDetails(Request $request, $founder_name){
        $founder = Founder::where('slug',$founder_name)->first();

        if(!$founder)
            return Redirect::route('web-home');
        $founder_companies = Startup::whereIn('startup_id',function($q) use($founder){
            $q->select('startup_id')->from('startup_founders')->where('founder_id',$founder->founder_id);
        })->get();



        // for create form //
        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }
        $industries     = Industry::select('industry_id','name')->where('used','1')->orderBy('name')->get();
        $founders       = Founder::select('founder_id','name')->orderBy('name')->get();
        $investors      = Investor::select('investor_id','name')->orderBy('name')->get();
        $industries_tags= Industry::pluck('name');

        $countries = Country::select('country_id','name')->orderBy('name')->get();

        $title = $founder->name.' | Contxto';;
        $description = $founder->name;

        //NEWS API URL
        $url = 'https://contxto.com/wp-json/wp/v2/posts?search='.$founder->slug;
        //create a new cURL resource
        $ch = curl_init($url);
        //set the content type to application/json
        curl_setopt($ch, CURLOPT_HTTPHEADER, array('Content-Type:application/json'));
        //return response instead of outputting
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        //execute the POST request
        $result = curl_exec($ch);
        //close cURL resource
        curl_close($ch);
        $all_news = json_decode($result);

        return view('founder-details')->with(compact('all_news','title','investor','founder_companies','founder','industries','founders','investors','industries_tags','countries','founded_years','title','description'));
    }

    /*
        Return company create Form
    */

    public function companyCreate(Request $request){

        // generate year //
        $rand=rand();
        $founded_years = array();
        $year_count   = 30;
        $current_year = (int)date('Y');
        while ($year_count) {
            $founded_years[] = $current_year;
            $current_year--;
            $year_count--;
        }

        $countries      = Country::select('country_id','name')->orderBy('name')->get();
        $industries     = Industry::select('industry_id','name')->where('used','1')->where('used','1')->orderBy('name')->get();
        $founders       = Founder::select('founder_id','name')->orderBy('name')->get();
        $investors      = Investor::select('investor_id','name')->orderBy('name')->get();

        return view('company-create')->with(compact('countries','industries','founders','investors','founded_years','rand'));
    }

    /*
        company create request
    */

    public function founderSave(Request $request){

      $validator = Validator::make($request->all(),[
              'founder_email' =>  'required|email',
              'founder_name'  =>  'required',
              'description'   =>  'required',
          ]);
      if($validator->fails())
          return redirect()->back()->withErrors($validator)->withInput();

      $founder = new Founder();
      $founder->name          = $request->input('name');
      $startup->description   = $request->input('description','');
      $startup->country_id    = $request->input('country_id');
      $startup->city_id       = 0;
      $startup->industry_id   = 0;
      $startup->logo          = '';
      $startup->founded       = $request->input('founded');
      $startup->website       = $request->input('website');
      $startup->facebook      = $request->input('facebook','');
      $startup->instagram     = $request->input('instagram','');
      $startup->twitter       = $request->input('twitter','');
      $startup->linkedin      = $request->input('linkedin','');
      $startup->valuation     = '';
      $startup->ipo_symbol    = '';
      $startup->created_by    = 'Api';
      $startup->created_from  = 'form';

      $startup->is_public      = 0;
      if($startup->save()){
          // link countries
          DB::table('startup_countries')->insert([
                  'startup_id'    =>  $startup->startup_id,
                  'country_id'    =>  $request->input('country_id'),
                  'created_at'    =>  Carbon::now(),
                  'updated_at'    =>  Carbon::now()
              ]);

          // link industries
          $industries     = explode(',',$request->input('industries'));
          $industry_ids   = array();
          $all_industries = Industry::whereIn('name',$industries)->where('used','1')->pluck('industry_id')->all();
          if(!empty($all_industries)){
              foreach ($all_industries as $key => $value) {
                  DB::table('startup_industries')->insert([
                          'startup_id'   =>   $startup->startup_id,
                          'industry_id'  =>   $value,
                          'source'       =>   'Api',
                          'created_at'   =>   Carbon::now(),
                          'updated_at'   =>   Carbon::now()
                      ]);
              }
          }

          // link founder //
          $founder_name = $request->input('founder_name');
          $founder = new Founder();
          $founder->name = $founder_name;
          $founder->is_blocked = 0;
          $founder->save();

          DB::table('startup_founders')->insert([
                  'startup_id'    =>  $startup->startup_id,
                  'founder_id'    =>  $founder->founder_id,
                  'founder_link'  =>  '',
                  'roles'         =>  '',
                  'source'        =>  'Web1',
                  'created_at'    =>  Carbon::now(),
                  'updated_at'    =>  Carbon::now()
              ]);

          $country = Country::where('country_id',$request->input('country_id'))->first();
          //send mail for the new startup request //

          $job = (new NewCompanyRequest($startup,$country,$request->input('industries'),$request->input('creator_email'),$request->input('creator_name'),$request->input('founder_name')));
          dispatch($job);

          return redirect()->route('web-home')->with('msg', 'Your request has been sent');
      }

      return redirect()->route('web-home')->with('error', 'Something went wrong');
  }


    public function companySave(Request $request){
        //dd($request->all());
        // validate //
        $validator = Validator::make($request->all(),[
                'creator_email' =>  'required|email',
                'creator_name'  =>  'required',
                'name'          =>  'required|unique:startups,name',
                'description'   =>  'required',
                'country_id'    =>  'required|exists:countries,country_id',
                'founded'       =>  'numeric',
                'founder_name'  =>  'required',
                'website'       =>  'required',
                'industries'    =>  'required'
            ]);
        if($validator->fails())
            return redirect()->back()->withErrors($validator)->withInput();

        $startup = new Startup();
        $startup->name          = $request->input('name');
        $startup->description   = $request->input('description','');
        $startup->country_id    = $request->input('country_id');
        $startup->city_id       = 0;
        $startup->industry_id   = 0;
        $startup->logo          = '';
        $startup->founded       = $request->input('founded');
        $startup->website       = $request->input('website');
        $startup->facebook      = $request->input('facebook','');
        $startup->instagram     = $request->input('instagram','');
        $startup->twitter       = $request->input('twitter','');
        $startup->linkedin      = $request->input('linkedin','');
        $startup->valuation     = '';
        $startup->ipo_symbol    = '';
        $startup->created_by    = 'Api';
        $startup->created_from  = 'form';

        $startup->is_public      = 0;
        if($startup->save()){
            // link countries
            DB::table('startup_countries')->insert([
                    'startup_id'    =>  $startup->startup_id,
                    'country_id'    =>  $request->input('country_id'),
                    'created_at'    =>  Carbon::now(),
                    'updated_at'    =>  Carbon::now()
                ]);

            // link industries
            $industries     = explode(',',$request->input('industries'));
            $industry_ids   = array();
            $all_industries = Industry::whereIn('name',$industries)->where('used','1')->pluck('industry_id')->all();
            if(!empty($all_industries)){
                foreach ($all_industries as $key => $value) {
                    DB::table('startup_industries')->insert([
                            'startup_id'   =>   $startup->startup_id,
                            'industry_id'  =>   $value,
                            'source'       =>   'Api',
                            'created_at'   =>   Carbon::now(),
                            'updated_at'   =>   Carbon::now()
                        ]);
                }
            }

            // link founder //
            $founder_name = $request->input('founder_name');

            $founder = new Founder();
            $founder->name = $founder_name;
            $founder->is_blocked = 0;
            $founder->save();

            DB::table('startup_founders')->insert([
                    'startup_id'    =>  $startup->startup_id,
                    'founder_id'    =>  $founder->founder_id,
                    'founder_link'  =>  '',
                    'roles'         =>  '',
                    'source'        =>  'Web1',
                    'created_at'    =>  Carbon::now(),
                    'updated_at'    =>  Carbon::now()
                ]);

            $country = Country::where('country_id',$request->input('country_id'))->first();
            //send mail for the new startup request //
            $job = (new NewCompanyRequest($startup,$country,$request->input('industries'),$request->input('creator_email'),$request->input('creator_name'),$request->input('founder_name')));
            dispatch($job);

            return redirect()->route('web-home')->with('msg', 'Your request has been sent');
        }

        return redirect()->route('web-home')->with('error', 'Something went wrong');
    }

    /*
        NewC ompany
    */

    public function newCompany(Request $request){
        // validate //
        // dd(@$request->input('founder_name'));
        $validator = Validator::make($request->all(),[
                'creator_email' =>  'required|email',
                'creator_name'  =>  'required',
                'name'          =>  'required|unique:startups,name',
                'name'          =>  'required|unique:startups_temp,name',
                'description'   =>  'required',
                'country_id'    =>  'required|exists:countries,country_id',
                'founded'       =>  'numeric',
                // 'founder_name'  =>  'required',
                'website'       =>  'required',
                'industries'    =>  'required',
                'company-logo' => 'image|mimes:jpeg,png,jpg,gif,svg,webp'
            ],
            [
                'company-logo.image'=>'Company Logo image format is not supported',
                'name.unique' => 'A company has already been registered with this name',

            ]
        );

        if($validator->fails()){
            return response()->json(array('success'=>0,'msg'=>$validator->getMessageBag()->first()), 400);
        }
        $hunter= "https://api.hunter.io/v2/domain-search?domain={$request->input('website')}&api_key=43eb332caf0dcd8d7b9e5f59ecc73dd07dcf17d9";
        $client  = new \GuzzleHttp\Client();
        $details = $client->request('GET', $hunter);
        $hunters  = json_decode($details->getBody(),true);


        $slug = strtolower(str_replace(" ","-",$request->input('name')));
        //check if image is valid
        $image = $request->file('company-logo');
        if(isset($image)){
            if($image->isValid())
            {
                //get extension
                $extension =$image->getClientOriginalExtension();
                $imagename = $slug.time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/Uploads');
                $image->move($destinationPath, $imagename);
            }else{
                return response()->json(array('success'=>0,'msg'=>'Image is not valid'), 400);
            }
        }else{
            $imagename = '';
        }

        $startup = new StartupTemp();
        $startup->name          = $request->input('name');
        $startup->description   = $request->input('description','');
        $startup->country_id    = $request->input('country_id');
        $startup->industry_id   = 0;
        $startup->logo          = $imagename;
        $startup->founded       = $request->input('founded');
        $startup->website       = $request->input('website');
        $startup->facebook      = $request->input('facebook','');
        $startup->instagram     = $request->input('instagram','');
        $startup->twitter       = $request->input('twitter','');
        $startup->linkedin      = $request->input('linkedin','');
        $startup->slug = $slug;
        $startup->hunter = json_encode($hunters);
        $startup->valuation     = '';
        $startup->ipo_symbol    = '';
        $startup->created_by    = 'Api';
        $startup->created_from  = 'form';
        $startup->is_public      = 1;

        if($startup->save()){
            // link countries
            DB::table('startup_countries_temp')->insert([
                    'startup_id'    =>  $startup->startup_id,
                    'country_id'    =>  $request->input('country_id'),
                    'created_at'    =>  Carbon::now(),
                    'updated_at'    =>  Carbon::now()
                ]);

            // link industries
            $industries = $request->input('industries');
            if(count($industries)>0){
                foreach ($industries as $key => $value) {
                    DB::table('startup_industries_temp')->insert([
                            'startup_id'   =>   $startup->startup_id,
                            'industry_id'  =>   $value,
                            'source'       =>   'Api',
                            'created_at'   =>   Carbon::now(),
                            'updated_at'   =>   Carbon::now()
                        ]);
                }
            }

            // link founder //
            $founder_name = @$request->input('founder_name');

            if(count(@$founder_name)>0){
                foreach (@$founder_name as $key => $value) {

            DB::table('startup_founders_temp')->insert([
                    'startup_id'    =>  $startup->startup_id,
                    'founder_id'    =>  $value,
                    'founder_link'  =>  '',
                    'roles'         =>  '',
                    'source'        =>  'Web1',
                    'created_at'    =>  Carbon::now(),
                    'updated_at'    =>  Carbon::now()
                  ]);
          }
      }

// return response()->json(array($hunters));







            $country = Country::where('country_id',$request->input('country_id'))->first();
            $all_industries = Industry::whereIn('industry_id',$industries)->where('used','1')->pluck('name')->all();
            $all_industries = implode(',',$all_industries);
            $all_founders = Founder::whereIn('founder_id',$founder_name)->pluck('name')->all();
            $all_founders = implode(',',$all_founders);
            //send mail for the new startup request //
            $email=$request->input('creator_email');
            $company_name=$request->input('name');
            $link='http://contxto.netsolutionindia.com/company/profile/'.$slug;
            $data = array('name'=>$request->input('name'),'link'=>$link);
            Mail::send('mails.company-profile-update', $data, function($message) use($email,$company_name){
               $message->to('sanchit@code-brew.com', 'Tutorials Point')->subject
                  ('Contxto - Update Profile');
               $message->from('victor@contxto.com',$company_name);
            });
            $job = (new NewCompanyRequest($link,$startup,$country,$all_industries,$all_founders,$request->input('creator_email'),$request->input('creator_name')));
            dispatch($job);
            //return response
            return response()->json(array('success'=>1,'msg'=>'success'), 200);
        }
        return response()->json(array('success'=>0,'statusCode'=>400,'msg'=>'Validation error'), 400);
    }



    public function newFounder(Request $request){
        // validate //
        $validator = Validator::make($request->all(),[
                'founder_email' =>  'required|email',
                'founder_name'  =>  'required',
                'founder-image' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]
            // [
            //     'founder_email.unique' => 'A Founder has already been registered with this email',
            // ]
        );

        if($validator->fails()){
            return response()->json(array('success'=>0,'msg'=>$validator->getMessageBag()->first()), 400);
        }
        $foundertempexist=FounderTemp::where('email',$request->founder_email)->first();
        $founderexist=Founder::where('email',$request->founder_email)->first();

        if(@$founderexist->founder_id!=''){
          @$newfounders=FounderTemp::where('founder_id',@$founderexist->founder_id)->first();
          return response()->json(array('foundername'=>@$newfounders->name,'founderid'=>@$newfounders->founder_id,'data'=>$newfounders,'success'=>1,'msg'=>'success'), 200);
        }
        if(@$foundertempexist->founder_id!=''){
          @$newfounders=FounderTemp::where('founder_id',@$foundertempexist->founder_id)->first();
          return response()->json(array('foundername'=>@$newfounders->name,'founderid'=>@$newfounders->founder_id,'data'=>$newfounders,'success'=>1,'msg'=>'success'), 200);
        }
        $slug = strtolower(str_replace(" ","-",$request->input('founder_name')));
        //check if image is valid
        $image = $request->file('founder-image');
        if(isset($image)){
            if($image->isValid())
            {
                //get extension
                $extension =$image->getClientOriginalExtension();
                $imagename = $slug.time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/Uploads/Founders');
                $image->move($destinationPath, $imagename);
            }else{
                return response()->json(array('success'=>0,'msg'=>'Image is not valid'), 400);
            }
        }else{
            $imagename = '';
        }
        $founders = new FounderTemp();
        $founders->name          = $request->input('founder_name');
        $founders->rand          = $request->input('randam');
        $founders->email          = $request->input('founder_email');
        $founders->nationality    = $request->input('nationality');
        $founders->pic          = $imagename;
        $founders->facebook      = $request->input('facebook','');
        $founders->instagram     = $request->input('instagram','');
        $founders->twitter       = $request->input('twitter','');
        $founders->slug       = $slug;
        $founders->linkedin      = $request->input('linkedin','');
        if($founders->save()){
            // link countries


            // link industries


            // link founder //

         @$newfounders=FounderTemp::where('founder_id',@$founders->founder_id)->first();



            //send mail for the new startup request //

            //return response
            return response()->json(array('foundername'=>@$newfounders->name,'founderid'=>@$newfounders->founder_id,'data'=>$newfounders,'success'=>1,'msg'=>'success'), 200);
        }
        return response()->json(array('success'=>0,'statusCode'=>400,'msg'=>'Validation error'), 400);
    }

    public function newInvestor(Request $request){
        // validate //
        // return response()->json(array('part'=>$request->investor_email));
        $validator = Validator::make($request->all(),[
                'investor_email' =>  'required|email',
                'investor_name'  =>  'required',
                'country_id'    =>  'required|exists:countries,country_id',
                'website'       =>  'required',
                'investor-image' => 'image|mimes:jpeg,png,jpg,gif,svg'
            ]
            // [
            //     'name.unique' => 'A company has already been registered with this name',
            // ]
        );

        if($validator->fails()){
            return response()->json(array('success'=>0,'msg'=>$validator->getMessageBag()->first()), 400);
        }

        $investors=Investor::where('email',$request->input('investor_email'))->first();

        if(@$investors->investor_id!=''){
          DB::table('startup_investors')->insert([
                  'startup_id'    =>  $request->startupid,
                  'investor_id'    => @$investors->investor_id,
                  'source'    =>  'Web1',
                  'investor_link'    =>  $request->website,
              ]);
              return response()->json(array('success'=>1,'msg'=>'success'), 200);
        }
        $slug = strtolower(str_replace(" ","-",$request->input('investor_name')));
        //check if image is valid
        $image = $request->file('investor-image');
        if(isset($image)){
            if($image->isValid())
            {
                //get extension
                $extension =$image->getClientOriginalExtension();
                $imagename = $slug.time().'.'.$image->getClientOriginalExtension();
                $destinationPath = public_path('/Uploads');
                $image->move($destinationPath, $imagename);
            }else{
                return response()->json(array('success'=>0,'msg'=>'Image is not valid'), 400);
            }
        }else{
            $imagename = '';
        }
        $investor = new Investor();
        $investor->name          = $request->input('investor_name');
        $investor->investor_type_id='0';
        $investor->created_by   = 'Cron';
        $investor->slug    = $slug;
        $investor->email   = $request->input('investor_email');
        $investor->description          = $request->input('description');
        $investor->image       = $imagename;
        $investor->facebook       = $request->input('facebook');
        $investor->instagram      = $request->input('instagram','');
        $investor->twitter     = $request->input('twitter','');
        $investor->linkedin       = $request->input('linkedin','');
        $investor->country_id      = $request->input('country_id','');


        if($investor->save()){
            // link countries
            DB::table('startup_investors')->insert([
                    'startup_id'    =>  $request->startupid,
                    'investor_id'    => $investor->investor_id,
                    'source'    =>  'Web1',
                    'investor_link'    =>  $request->website,
                ]);


            //return response
            return response()->json(array('success'=>1,'msg'=>'success'), 200);
        }
        return response()->json(array('success'=>0,'statusCode'=>400,'msg'=>'Validation error'), 400);
    }

    public function companyCheck(Request $request){

       $company=$request->company;
       $curl = curl_init();

 curl_setopt_array($curl, array(
     CURLOPT_URL => "https://api.hunter.io/v2/domain-search?domain=". $company ."&type=generic&limit=1&api_key=cd316ebd618cb66a223e01a27a19c13af9bba921",
     CURLOPT_RETURNTRANSFER => true,
     CURLOPT_ENCODING => "",
     CURLOPT_TIMEOUT => 30000,
     CURLOPT_HTTP_VERSION => CURL_HTTP_VERSION_1_1,
     CURLOPT_CUSTOMREQUEST => "GET",
     CURLOPT_HTTPHEADER => array(
     	// Set Here Your Requesred Headers
         'Content-Type: application/json',
     ),
 ));
 $response = curl_exec($curl);
 $err = curl_error($curl);
 curl_close($curl);

 if ($err) {
     echo "cURL Error #:" . $err;
 } else {
      print_r(json_decode($response));
 }
       // $link='http://contxto.netsolutionindia.com/company/profile/'.$company_name
       // $data = array('name'=>$company_name,'link'=>'');
       // // $hunter= "https://api.hunter.io/v2/domain-search?domain={$company}&type=generic&limit=1&api_key=cd316ebd618cb66a223e01a27a19c13af9bba921";
       // //
       // //      $client  = new \GuzzleHttp\Client();
       // //      $details = $client->request('GET', $hunter);
       // //
       // //      $result  = json_decode($details->getBody(),true);
       // Mail::send('mails.company-profile-update', $data, function($message) use($email,$company_name) {
       //    $message->to('sanchit@code-brew.com', 'Tutorials Point')->subject
       //       ('Contxto - Update Profile');
       //    $message->from('xyz@gmail.com',$company_name  );
       // });
       // return response()->json(array('company'=>'done'));
    }

    public function profileCompany(){
      return view('company-update');
    }

    public function getId(Request $request){
      if(@$request->country=='all'){
        $country='0';
      }
      else{
      $country=Country::select('country_id')->where('slug',@$request->country)->first();
       }

       if(@$request->city=='all'){
       $city=0;
       }
       else{
      $city=City::select('city_id')->where('slug',@$request->city)->first();
        }
       if(@$request->industry=='all'){
         $industry=0;
       }
       else{
      $industry=Industry::select('industry_id')->where('slug',@$request->industry)->first();
      }

      return response()->json(['success'=>1,@$country,@$city,@$industry]);
    }

    public function getSlug(Request $request){

      $company = Startup::select('slug')->where('name',$request->keyword)->first();
      if($company){
      return response()->json(array('slug'=>$company));
    }
    else{
      return response()->json(array('slug'=>1));
    }
    }

    public function createCompany(){
      $founded_years = array();
      $year_count   = 30;
      $current_year = (int)date('Y');
      while ($year_count) {
          $founded_years[] = $current_year;
          $current_year--;
          $year_count--;
      }
      $countries=Country::all();
      $industries=Industry::all();
      $founders=Founder::all();
      return view('create-new-company',compact('countries','founded_years','industries','founders'));
    }

    public function createInvestor(){
      $founded_years = array();
      $year_count   = 30;
      $current_year = (int)date('Y');
      while ($year_count) {
          $founded_years[] = $current_year;
          $current_year--;
          $year_count--;
      }
      $startups=Startup::where('is_public','0')->get();
      $countries=Country::all();
      $industries=Industry::all();
      $founders=Founder::all();
      return view('create-new-investors',compact('startups','countries','founded_years','industries','founders'));
    }
}

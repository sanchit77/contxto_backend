<?php

namespace App\Http\Middleware;

use Request;
use Closure;
use Session;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Input;

use Illuminate\Support\Facades\Route;

use App\Models\Admin;

class AdminMiddleware
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
        {
            $currentPath = Route::currentRouteName();

            if(!Request::cookie('adminAccessToken') || !Request::cookie('admintId') )
                return Redirect::route('admin_login_get')->withErrors('Please Login First');

            $admin_id = Request::cookie('admintId');

            $admin = Admin::where('admin_id',$admin_id)->first();

            $request['timezonez'] =  Request::cookie('admintz');// $time->format('P');

            \Cookie::queue('adminAccessToken', Request::cookie('adminAccessToken'), 60);
            \Cookie::queue('admintz', Request::cookie('admintz'), 60);
            \Cookie::queue('admintId', $admin_id, 60);

            \App::instance('admin', $admin);
            \View::share('admin', $admin);

            return $next($request);

        }

}

<?php

namespace App\Http\Middleware;

use Illuminate\Foundation\Http\Middleware\VerifyCsrfToken as Middleware;

class VerifyCsrfToken extends Middleware
{
    /**
     * Indicates whether the XSRF-TOKEN cookie should be set on the response.
     *
     * @var bool
     */
    protected $addHttpCookie = true;

    /**
     * The URIs that should be excluded from CSRF verification.
     *
     * @var array
     */
    protected $except = [
        //
        'admin/index_data',
        'admin/startups/index_data',
        'admin/index_data1',
        'admin/startups/index',
        'admin/test',
        'admin/startups/approve',
        
        '/admin/startups/add/has/many/relation',
        'admin/startups/countries',
        'admin/startups/cities',
        'admin/startups/target_countries',

        'admin/founders/index',
        'admin/investors/index',
        'admin/industries/index'
        
    ];
}

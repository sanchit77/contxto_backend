<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\SyncLog;
use App\Http\Controllers\Sync\SyncDbCont;

class StartupWeb2 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $startup;
    protected $type;
    protected $source;

    public function __construct($startup, $source)
    {
        $this->startup = $startup;
        $this->source = $source;
        $this->type = "S_".$source."_CRUD";
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("Startup Update Add started Web1");

        $result = SyncDbCont::startup_web2_crud($this->startup, $this->source);

        SyncLog::insert([
            'source'=> $this->source,
            'type' => $this->type,
            'result' => 'Success',
            'details' => $result,
            'data' => json_encode($this->startup)
        ]);
    }
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
    */
    public function failed(Exception $exception)
    {
        SyncLog::insert([
            'source'=> $this->source,
            'type' => $this->type,
            'result' => 'Error',
            'details' => $exception,
            'data' => json_encode($this->startup)
        ]);

        \Log::error("Startup ".$this->source." CRUD Error", $exception->getMessage());
    }
}

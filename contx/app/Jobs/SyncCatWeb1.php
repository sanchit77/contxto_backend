<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\SyncLog;
use App\Http\Controllers\Sync\SyncDbCont;

class SyncCatWeb1 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $categories;
    protected $source;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($categories, $source)
    {
        //
        $this->categories = $categories;
        $this->source = $source;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {

        \Log::info("Request cycle with Categories Queues started");

        $result = SyncDbCont::update_industries($this->categories, $this->source);

        SyncLog::insert([
            'source'=> $this->source,
            'type' => 'Industries',
            'result' => 'Success',
            'details' => $result,
            'data' => json_encode($this->categories)
        ]);

        //
    }
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
    public function failed(Exception $exception)
    {

        SyncLog::insert([
            'source'=> $this->source,
            'type' => 'Industries',
            'result' => 'Error',
            'details' => $exception,
            'data' => json_encode($this->categories)
        ]);

        \Log::error("Sync Cat1 Error", $exception->getMessage());

        // Send user notification of failure, etc...
    }


}

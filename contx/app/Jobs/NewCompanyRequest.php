<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;
use Mail;

class NewCompanyRequest implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    protected $company;
    protected $country;
    protected $industries;
    protected $name;
    protected $email;
    protected $founder_name;
    /**
     * Create a new job instance.
     *
     * @return void
     */
    public function __construct($company,$country,$industries,$email,$name,$founder_name)
    {
        $this->company = $company;
        $this->country = $country;
        $this->industries = $industries;
        $this->email = $email;
        $this->name  = $name;
        $this->founder_name = $founder_name;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        // send mail to 
        $mail_to = ['victor@contxto.com','pargat@code-brew.com'];
        //$mail_to = ['gurvindra@code-brew.com','gurvindrasingh@gmail.com'];
        $subject = 'New company add request';
        Mail::send('mails.new-company-request', ['company' => $this->company, 'country'=>$this->country, 'industries'=>$this->industries, 'email' => $this->email, 'name'=>$this->name, 'founder_name'=>$this->founder_name ], function ($m) use($subject,$mail_to) {
            $m->from('notification@code-brew.com','Contxto');
            $m->to($mail_to)->subject($subject);
        });
    }
}

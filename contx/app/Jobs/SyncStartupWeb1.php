<?php

namespace App\Jobs;

use Illuminate\Bus\Queueable;
use Illuminate\Queue\SerializesModels;
use Illuminate\Queue\InteractsWithQueue;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Foundation\Bus\Dispatchable;

use App\Models\SyncLog;
use App\Http\Controllers\Sync\SyncDbCont;

class SyncStartupWeb1 implements ShouldQueue
{
    use Dispatchable, InteractsWithQueue, Queueable, SerializesModels;

    /**
     * Create a new job instance.
     *
     * @return void
     */
    protected $startups;
    protected $source;
    protected $type = "Startups";
    public function __construct($startups, $source)
    {
        $this->startups = $startups;
        $this->source = $source;
    }

    /**
     * Execute the job.
     *
     * @return void
     */
    public function handle()
    {
        \Log::info("Request cycle with Startups Queues started");

        $result =SyncDbCont::update_startups($this->startups, $this->source);

        SyncLog::insert([
            'source'=> $this->source,
            'type' => $this->type,
            'result' => 'Success',
            'details' => $result,
            'data' => json_encode($this->startups)
        ]);

    }
    /**
     * The job failed to process.
     *
     * @param  Exception  $exception
     * @return void
     */
public function failed(Exception $exception)
    {
        SyncLog::insert([
            'source'=> $this->source,
            'type' => $this->type,
            'result' => 'Error',
            'details' => $exception,
            'data' => json_encode($this->startups)
        ]);

        \Log::error("Sync Startups Error", $exception->getMessage());
    }

}

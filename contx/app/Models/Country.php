<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Country
 *
 * @property int $country_id
 * @property string $name
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $cities
 * @property \Illuminate\Database\Eloquent\Collection $startup_countries
 * @property \Illuminate\Database\Eloquent\Collection $startups
 *
 * @package App\Models
 */
class Country extends Eloquent
{
	protected $primaryKey = 'country_id';

	protected $casts = [
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'name',
		'is_blocked'
	];

	public function cities()
	{
		return $this->hasMany(\App\Models\City::class);
	}

	public function startup_countries()
	{
		return $this->hasMany(\App\Models\StartupCountry::class);
	}

	public function startups()
	{
		return $this->hasMany(\App\Models\Startup::class,'country_id','country_id');
	}

	public function investors()
	{
		return $this->hasMany(\App\Models\Investor::class,'country_id','country_id');
	}

	public function founders()
	{
		return $this->hasMany(\App\Models\Founder::class,'country_id','country_id');
	}
}

<?php

/**
 * Created by Reliese Model.
 * Date: Fri, 01 Feb 2019 09:42:08 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupIndustry
 * 
 * @property int $startup_industry_id
 * @property int $startup_id
 * @property int $industry_id
 * @property string $source
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class StartupIndustry extends Eloquent
{
	protected $primaryKey = 'startup_industry_id';

	protected $casts = [
		'startup_id' => 'int',
		'industry_id' => 'int'
	];

	protected $fillable = [
		'startup_id',
		'industry_id',
		'source'
	];
}

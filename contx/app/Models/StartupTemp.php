<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Startup
 *
 * @property int $startup_id
 * @property int $country_id
 * @property int $city_id
 * @property int $industry_id
 * @property string $name
 * @property string $logo
 * @property string $description
 * @property string $founded
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property string $linkedin
 * @property string $instagram
 * @property string $valuation
 * @property string $ipo_symbol
 * @property bool $is_public
 * @property string $created_by
 * @property string $created_from
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Country $country
 * @property \App\Models\City $city
 * @property \Illuminate\Database\Eloquent\Collection $startup_countries
 * @property \Illuminate\Database\Eloquent\Collection $founders
 * @property \Illuminate\Database\Eloquent\Collection $startup_investors
 * @property \Illuminate\Database\Eloquent\Collection $startup_news
 * @property \Illuminate\Database\Eloquent\Collection $rounds
 *
 * @package App\Models
 */
class StartupTemp extends Eloquent
{
	protected $table = 'startups_temp';
	protected $primaryKey = 'startup_id';

	protected $casts = [
		'country_id' => 'int',
		'city_id' => 'int',
		'industry_id' => 'int',
		'is_public' => 'bool',
	
	];

	protected $fillable = [
		'country_id',
		'city_id',
		'industry_id',
		'name',
		'logo',
		'description',
		'founded',
		'website',
		'facebook',
		'twitter',
		'linkedin',
		'instagram',
		'valuation',
		'ipo_symbol',
		'is_public',
		'created_by',
		'created_from',
		'hunter'
	];

	///////////////		Make Filter Query 	///////////////////////
	public static function make_filter_query($columns, $order)
	{
		$having_raw_string = "(startup_id != 0)";

		$founder_key = "";//4
		$investors_key = "";//5
		$target_countries_key = "";//7
		$industry_key = "";//12
		$startup_news_count_key = "";//13

		foreach($columns as $key => $column)
			{
				if($column['search']['value'] == null)
					continue;

				if(in_array($key, [0, 1, 2, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 20]))
					{
						$having_raw_string = $having_raw_string.' AND ('.$column['data'].' LIKE "%'.$column['search']['value'].'%")';
					}
				elseif($key == 4)
					{
						$founder_key = $column['search']['value'];
					}
				elseif($key == 5)
					{
						$investors_key = $column['search']['value'];
					}
				elseif($key == 7)
					{
						$target_countries_key = $column['search']['value'];
					}
				elseif($key == 12)
					{
						$industry_key = $column['search']['value'];
					}
				elseif($key == 13)
					{
						$startup_news_count_key = $column['search']['value'];
					}
			}

	//////////////////		Sorting 	////////////////////////
		$column = $order[0]['column'];
		$direction = $order[0]['dir'];

		if(in_array($column, [0, 1, 2, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 20]))
			$sort_key = $columns[$column]["data"];
		elseif($column == 4)
			$sort_key = "founders_count";
		elseif($column == 5)
			$sort_key = "investors_count";
		elseif($column == 7)
			$sort_key = "startup_countries_count";
		elseif($column == 12)
			$sort_key = "startup_industries_count";
		elseif($column == 13)
			$sort_key = "startup_news_count";
		else
			$sort_key = "startup_id";
	//////////////////		Sorting 	/////////////////////////

		// var allColumns = [
		// 	{ "data": "startup_rounds", "orderable": false },//6
		// ];

		return [
			"having_raw_string"=> $having_raw_string,
			"founder_key" => $founder_key,//4
			"investors_key" => $investors_key,//5
			"target_countries_key" => $target_countries_key,//7
			"industry_key" => $industry_key,//12
			"startup_news_count_key" => $startup_news_count_key,//13
			'sort_key' => $sort_key,
			'direction' => $direction
		];

	}

	////////////////	Admin Dynamic Searching Count 	////////////
	public static function admin_dynamic_searching_count($data)
	{
		$startups = StartupTemp::select("startups_temp.startup_id", "startups_temp.name", "startups_temp.valuation", "startups_temp.description", "startups_temp.founded", "startups_temp.website", "startups_temp.facebook", "startups_temp.twitter", "startups_temp.linkedin", "startups_temp.instagram", "startups_temp.ipo_symbol",
		\DB::RAW("COALESCE(countries.name, '') as country"),
		\DB::RAW("COALESCE(cities.name, '') as city"),
		\DB::RAW("(CASE WHEN is_public IS true THEN 'Public' else 'Private' END) as is_public")
		)
		->leftjoin('countries', 'countries.country_id', 'startups_temp.country_id')
		->leftjoin('cities', 'cities.city_id', 'startups_temp.city_id');

		////////////		Other Pages Filtering 		//////////////////
		if(isset($data['industry_id']) && ($data['industry_id'] != 0))
			{
				$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_industries_temp SI WHERE SI.startup_id=startups_temp.startup_id AND SI.industry_id='.$data['industry_id'].') > 0 )';
			}
		if(isset($data['founder_id']) && ($data['founder_id'] != 0))
			{
				$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_founders_temp SF WHERE SF.startup_id=startups_temp.startup_id AND SF.founder_id='.$data['founder_id'].') > 0 )';
			}

		////////////		Other Pages Filtering 		//////////////////

		$startups->havingRaw($data['filter']['having_raw_string']);

		/////////		Founder Filter 	///////////////////
		if($data['filter']['founder_key'] != '')
			{
				$filter = $data['filter']['founder_key'];

				$startups->whereHas('founders_temp', function($q1) use ($filter){

					$q1->join('founders_temp', 'founders_temp.founder_id', 'startup_founders_temp.founder_id');

					$q1->where('founders_temp.name', 'LIKE', "%".$filter."%");

				});
			}
		/////////		Founder Filter 	///////////////////

		/////////		Investor Filter ///////////////////
		if($data['filter']['investors_key'] != '')
			{
				$filter = $data['filter']['investors_key'];

				$startups->whereHas('investors', function($q1) use ($filter){

					$q1->join('investors', 'investors.investor_id', 'startup_investors_temp.investor_id');

					$q1->where('investors.name', 'LIKE', "%".$filter."%");

				});
			}
		/////////		Investor Filter ///////////////////

		/////////		Target Countries Filter ///////////
		if($data['filter']['target_countries_key'] != '')
		{
			$filter = $data['filter']['target_countries_key'];

			$startups->whereHas('startup_countries_temp', function($q1) use ($filter){

				$q1->join('countries', 'countries.country_id', 'startup_countries_temp.country_id');

				$q1->where('countries.name', 'LIKE', $filter."%");

			});
		}
		/////////		Target Countries Filter ///////////

		///////////		Industries Filter /////////////////
		if($data['filter']['industry_key'] != '')
		{
			$filter = $data['filter']['industry_key'];

			$startups->whereHas('startup_industries_temp', function($q1) use ($filter){

				$q1->join('industries', 'industries.industry_id', 'startup_industries_temp.industry_id');

				$q1->where('industries.name', 'LIKE', $filter."%");

			});
		}
		///////////		Industries Filter /////////////////
		return $startups->get();

	}

	////////////////	Admin Dynamic Searching 	///////////////
	public static function admin_dynamic_searching($data)
	{

		//dd($data['filter']['founder_key']);
		\DB::enableQueryLog();

		$startups = StartupTemp::select("startups_temp.startup_id", "startups_temp.name", "startups_temp.description", "startups_temp.founded", "startups_temp.website", "startups_temp.facebook", "startups_temp.twitter", "startups_temp.linkedin", "startups_temp.instagram", "startups_temp.ipo_symbol", "logo", "startups_temp.country_id", "startups_temp.city_id",
		\DB::RAW("COALESCE(countries.name, '') as country"),
		\DB::RAW("COALESCE(cities.name, '') as city"),
		\DB::RAW("(CASE WHEN is_public IS true THEN 'Public' else 'Private' END) as is_public")
		)
		->leftjoin('countries', 'countries.country_id', 'startups_temp.country_id')
		->leftjoin('cities', 'cities.city_id', 'startups_temp.city_id')
		->withCount([
			'startup_industries_temp',
			'startup_rounds',
			'startup_countries_temp',
			'founders_temp'
		])
		->with([
			'startup_industries_temp',
			'startup_rounds',
			'startup_countries_temp',
			'founders_temp'
		]);

		////////////		Other Pages Filtering 		//////////////////
		if(isset($data['industry_id']) && ($data['industry_id'] != 0))
			{
				$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_industries_temp SI WHERE SI.startup_id=startups_temp.startup_id AND SI.industry_id='.$data['industry_id'].') > 0 )';
			}
		if(isset($data['founder_id']) && ($data['founder_id'] != 0))
			{
				$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_founders_temp SF WHERE SF.startup_id=startups_temp.startup_id AND SF.founder_id='.$data['founder_id'].') > 0 )';
			}
		if(isset($data['investor_id']) && ($data['investor_id'] != 0))
			{
				$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_investors_temp SI WHERE SI.startup_id=startups_temp.startup_id AND SI.investor_id='.$data['investor_id'].') > 0 )';
			}
		////////////		Other Pages Filtering 		//////////////////

		$startups->havingRaw($data['filter']['having_raw_string']);

		/////////		Founder Filter 	///////////////////
		if($data['filter']['founder_key'] != '')
			{
				$filter = $data['filter']['founder_key'];
				$startups->whereHas('founders', function($q1) use ($filter){
					$q1->join('founders', 'founders.founder_id', 'startup_founders_temp.founder_id');
					$q1->where('founders.name', 'LIKE', "%".$filter."%")->orWhere("roles", 'LIKE', "%".$filter."%")->orWhere("source", 'LIKE', "%".$filter."%");
				});
			}
		/////////		Founder Filter 	///////////////////

		/////////		Target Countries Filter ///////////
			if($data['filter']['target_countries_key'] != '')
			{
				$filter = $data['filter']['target_countries_key'];
				$startups->whereHas('startup_countries_temp', function($q1) use ($filter){
					$q1->join('countries', 'countries.country_id', 'startup_countries_temp.country_id');
					$q1->where('countries.name', 'LIKE', $filter."%");
				});
			}
		/////////		Target Countries Filter ///////////

		///////////		Industries Filter /////////////////
		if($data['filter']['industry_key'] != '')
		{
			$filter = $data['filter']['industry_key'];
			$startups->whereHas('startup_industries_temp', function($q1) use ($filter){
				$q1->join('industries', 'industries.industry_id', 'startup_industries_temp.industry_id');
				$q1->where('industries.name', 'LIKE', $filter."%");
			});
		}
		///////////		Industries Filter /////////////////

		$startups->orderBy($data['filter']['sort_key'], $data['filter']['direction']);

		//$temp = $startups;
		$startups = $startups->offset($data['start'])->limit($data['length']);

		return $startups
		->get()
		->toArray();

	}

	public function startup_industries_temp()
	{
		return $this->hasMany(\App\Models\StartupIndustriesTemp::class, 'startup_id', 'startup_id')
		->join('industries', 'industries.industry_id', 'startup_industries_temp.industry_id')
		->select('startup_industries_temp.industry_id', 'startup_industries_temp.startup_id', 'name')
		;
	}

	public function startup_rounds()
	{
		return $this->hasMany(\App\Models\StartupRound::class, 'startup_id', 'startup_id')
		->join('rounds', 'rounds.round_id', 'startup_rounds.round_id')
		->join('investors', 'investors.investor_id', 'startup_rounds.investor_id')
		->select('startup_rounds.startup_round_id', 'startup_rounds.round_id', 'startup_rounds.investor_id', 'startup_rounds.startup_id', 'rounds.name as round_name', 'investors.name as investor_name', 'amount', 'dt');

	}

	public function startup_countries_temp()
	{
		return $this->hasMany(\App\Models\StartupCountriesTemp::class, 'startup_id', 'startup_id')
		->join('countries', 'countries.country_id', 'startup_countries_temp.country_id')
		->select('startup_countries_temp.country_id', 'startup_countries_temp.startup_id', 'name');
	}

	public function founders_temp()
	{
		return $this->hasMany(\App\Models\StartupFoundersTemp::class, 'startup_id', 'startup_id')
		->select('startup_founder_id', 'startup_founders_temp.startup_id', 'name','startup_founders_temp.founder_id', 'startup_founders_temp.founder_id as dynamic_db_id', 'founder_link', 'roles')
		->join('founders_temp','founders_temp.founder_id', 'startup_founders_temp.founder_id')
		;
	}
}

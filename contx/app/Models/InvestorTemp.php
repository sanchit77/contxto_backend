<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Investor
 *
 * @property int $investor_id
 * @property int $investor_type_id
 * @property string $name
 * @property string $created_by
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $startup_inverstors
 * @property \Illuminate\Database\Eloquent\Collection $startup_rounds
 *
 * @package App\Models
 */
class InvestorTemp extends Eloquent
{
  protected $table='investors_temp';
	protected $primaryKey = 'investor_id';

	protected $casts = [
		'is_blocked' => 'bool',
		'investor_type_id' => 'int'
	];

	protected $fillable = [
		'investor_type_id',
		'name',
		'created_by',
		'is_blocked',
    'stage_id'
	];

	public function startup_inverstors()
	{
		return $this->hasMany(\App\Models\StartupInvestor::class, 'investor_id', 'investor_id');
	}

	public function startup_rounds()
	{
		return $this->hasMany(\App\Models\StartupRound::class, 'investor_id', 'investor_id');
	}

	public function country()
	{
		return $this->hasOne(\App\Models\Country::class, 'country_id', 'country_id');
	}

	////////	Add new Investor 	//////////////////////////
	public static function add_new($name)
		{

			$investor = new InvestorTemp();

			$investor->name = $name;
			$investor->save();

			return $investor->investor_id;

		}

	////////	Add Investor 	///////////////////////////////
	public static function create($name, $investor_type_id, $created_by)
	{

		$investor = new InvestorTemp();

		$investor->name = $name;
		$investor->investor_type_id = $investor_type_id;
		$investor->created_by = $created_by;

		$investor->save();

		return $investor;

	}

	///////////		Statup investors Ajax 	/////////////////////////
	public static function ajax_data($data)
	{
		$searchHavingRaw = "(investor_id != 0)";

		foreach($data['columns'] as $key => $column)
			{//////		Search Query 	/////////////////
				$col_name = $column['data'];
				$search = $column['search']['value'];

				if($search == null)
					continue;

				$searchHavingRaw = $searchHavingRaw.' AND ('.$col_name.' LIKE "%'.$search.'%")';
			}//////		Search Query 	/////////////////

		$colId = $data["order"][0]["column"];

		$sortColumn = $data['columns'][$colId]["data"];
		$sortOrder = $data["order"][0]["dir"];

		$investors = InvestorTemp::select("*",
        \DB::RAW("(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."')) as created_at"),
		\DB::RAW("(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."')) as updated_at"),
		\DB::RAW("(SELECT COUNT(*) FROM startup_investors SI WHERE SI.investor_id=investors_temp.investor_id) as startup_inverstors_count")
        )
		//->withCount('startup_inverstors')
		->havingRaw($searchHavingRaw)
		->orderBy($sortColumn, $sortOrder)
        ->offset($data['start'])
        ->limit($data['length'])
        ->get()
        ->toArray();

		return InvestorTemp::format_html($investors);

	}

///////////		Format HTML 	/////////////////////////////////
public static function format_html($investors)
	{

		$con = [];
        $len = COUNT($investors);

		for($i=0;$i<$len;$i++)
			{
				$temp_id = $investors[$i]['investor_id'];//0

				$col_name = "table_".$temp_id."_".$i."_";// table_id_rowno_colno

				$temp_name = $investors[$i]['name'];
				$temp_startup_inverstors_count = $investors[$i]['startup_inverstors_count'];
				$temp_created_at = $investors[$i]['created_at'];
				$temp_updated_at = $investors[$i]['updated_at'];

				$name = '<button class="btn btn-default" id="'.$col_name.'0" onClick ="update_name(this.id)" title="'.$temp_name.'">'.$temp_name.'</button>';//0

				$startup_inverstors_count = '<a class="btn btn-default" id="'.$col_name.'0"  href="'.route('admin.investors.startups',['investor_id' => $temp_id]).'" title="Startups">'.$temp_startup_inverstors_count.'</a>';//1

				$created_at = '<i class="fa fa-clock-o"></i> '.$temp_created_at;//2
				$updated_at = '<i class="fa fa-clock-o"></i> '.$temp_updated_at;//3

				//<i class="fa fa-clock-o"></i> {{ $coupon->expires_atz }}

				$investors[$i]['name'] = $name;
				$investors[$i]['startup_inverstors_count'] = $startup_inverstors_count;
				$investors[$i]['created_at'] = $created_at;
				$investors[$i]['updated_at'] = $updated_at;


			}

		return $investors;

	}




}

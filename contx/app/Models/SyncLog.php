<?php

/**
 * Created by Reliese Model.
 * Date: Wed, 30 Jan 2019 11:29:06 +0000.
 */

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class SyncLog
 * 
 * @property int $sync_log_id
 * @property string $website
 * @property string $type
 * @property string $result
 * @property string $error
 * @property string $details
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class SyncLog extends Eloquent
{
	protected $primaryKey = 'sync_log_id';

	protected $fillable = [
		'website',
		'type',
		'result',
		'error',
		'details'
	];
}

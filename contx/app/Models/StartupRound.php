<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupRound
 *
 * @property int $startup_round_id
 * @property int $round_id
 * @property int $investor_id
 * @property int $startup_id
 * @property string $amount
 * @property \Carbon\Carbon $dt
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Round $round
 * @property \App\Models\Investor $investor
 * @property \App\Models\Startup $startup
 *
 * @package App\Models
 */
class StartupRound extends Eloquent
{
	protected $primaryKey = 'startup_round_id';

	protected $casts = [
		'round_id' => 'int',
		'investor_id' => 'int',
		'startup_id' => 'int'
	];

	protected $dates = [
		//'dt'
	];

	protected $fillable = [
		'round_id',
		'investor_id',
		'startup_id',
		'amount',
		'dt',
		'country_id',
		'industry_id'
	];

	public function round()
	{
		return $this->belongsTo(\App\Models\Round::class,'round_id','round_id');
	}

	public function investor()
	{
		return $this->belongsTo(\App\Models\Investor::class,'investor_id','investor_id');
	}

	public function startup()
	{
		return $this->belongsTo(\App\Models\Startup::class,'startup_id','startup_id');
	}

	public function country_name()
	{
		return $this->belongsTo(\App\Models\Country::class,'country_id','country_id');
	}
}

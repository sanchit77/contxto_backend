<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class City
 * 
 * @property int $city_id
 * @property int $country_id
 * @property string $name
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Country $country
 * @property \Illuminate\Database\Eloquent\Collection $startups
 *
 * @package App\Models
 */
class City extends Eloquent
{
	protected $primaryKey = 'city_id';

	protected $casts = [
		'country_id' => 'int',
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'country_id',
		'name',
		'is_blocked'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class, 'country_id', 'country_id');
	}

	public function startups()
	{
		return $this->hasMany(\App\Models\Startup::class, 'city_id', 'city_id');
	}

//////////		Create Industry 	///////////////////
public static function create($name, $country_id)
	{
		$city = new City();

		$city->name = $name;
		$city->country_id = $country_id;

		$city->save();

		return $city->city_id;

	}
//////////////

}

<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Admin
 * 
 * @property int $admin_id
 * @property string $name
 * @property string $job
 * @property string $email
 * @property string $password
 * @property string $profile_pic
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Admin extends Eloquent
{
	protected $primaryKey = 'admin_id';
	//public $incrementing = false;

	protected $casts = [
		'admin_id' => 'int'
	];

	protected $hidden = [
		'password'
	];

	protected $fillable = [
		'admin_id',
		'name',
		'job',
		'email',
		'password',
		'profile_pic'
	];
//////////////////////////		Admin Update 		/////////////////////////////
public static function super_admin_update($data,$admin)
	{
		return Admin::where('admin_id',$admin->admin_id)->update(array(
			'name'=>isset($data['name']) ? $data['name'] : $admin->name,
			'job'=>isset($data['job']) ? $data['job'] : $admin->job,
			'profile_pic'=>isset($data['profile_pic']) ? $data['profile_pic'] : $admin->profile_pic,
			'email'=>isset($data['email']) ? $data['email'] : $admin->email,
			'password'=>isset($data['password']) ? \Hash::make($data['password']) : $admin->password
		));
	}


}

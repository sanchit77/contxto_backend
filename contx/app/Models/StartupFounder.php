<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupFounder
 *
 * @property int $startup_founder_id
 * @property int $startup_id
 * @property int $founder_id
 * @property string $founder_link
 * @property string $source
 * @property string $roles
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Startup $startup
 * @property \App\Models\Founder $founder
 *
 * @package App\Models
 */
class StartupFounder extends Eloquent
{
	protected $primaryKey = 'startup_founder_id';

	protected $casts = [
		'startup_id' => 'int',
		'founder_id' => 'int'
	];

	protected $fillable = [
		'startup_id',
		'founder_id',
		'founder_link',
		'roles',
		'source'
	];

	public function startup()
	{
		return $this->belongsTo(\App\Models\Startup::class);
	}

	public function founder()
	{
		return $this->belongsTo(\App\Models\Founder::class);
	}

	////////	Add new founder 	//////////////////////////
	public static function add_new($startup_id, $founder_id)
		{

			$startup_founder = new StartupFounder();

			$startup_founder->startup_id = $startup_id;
			$startup_founder->founder_id = $founder_id;
			$startup_founder->save();

			return $startup_founder->founder_id;

		}
//////////		Create Startup Founder 	/////////////////////
// public static function create($startup_id, $founder_id, $source, $roles, $founder_link)
// {
//
// 	$startup_founder = new StartupFounder();
//
// 	$startup_founder->startup_id = $startup_id;
// 	$startup_founder->founder_id = $founder_id;
// 	$startup_founder->source = $source;
// 	$startup_founder->roles = $roles;
// 	$startup_founder->founder_link = $founder_link;
// 	$startup_founder->save();
//
// 	return $startup_founder->founder_id;
//
// }


}

<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Founder
 *
 * @property int $founder_id
 * @property string $name
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $startups
 *
 * @package App\Models
 */
class Founder extends Eloquent
{
  // use SoftDeletes;
	protected $dates = ['deleted_at'];
	protected $primaryKey = 'founder_id';
  protected $softDelete = true;
	protected $casts = [
		'is_blocked' => 'bool'
	];

protected $guarded = [];

		public function startups()
	{
		return $this->belongsToMany(\App\Models\Startup::class, 'startup_founders', 'startup_id', 'startup_id')
					->withPivot('startup_founder_id')
					->withTimestamps();
	}

	public function startup_founders()
	{
		return $this->hasMany(\App\Models\StartupFounder::class, 'founder_id', 'founder_id');
	}

	public function country()
	{
		return $this->hasOne(\App\Models\Country::class, 'country_id', 'country_id');
	}

	////////	Add new founder 	//////////////////////////
	public static function add_new($name)
		{

			$founder = new Founder();

			$founder->name = $name;
			$founder->save();

			return $founder->founder_id;

		}

	//////////	Add New Founder 	///////////////////////////
	// public static function create($name)
	// {
	//
	// 	$founder = new Founder();
	//
	// 	$founder->name = $name;
	// 	$founder->save();
	//
	// 	return $founder;
	//
	// }

	///////////		Statup Founders Ajax 	/////////////////////////
	public static function ajax_data($data)
	{
		$searchHavingRaw = "(founder_id != 0)";

		foreach($data['columns'] as $key => $column)
			{//////		Search Query 	/////////////////
				$col_name = $column['data'];
				$search = $column['search']['value'];

				if($search == null)
					continue;

				$searchHavingRaw = $searchHavingRaw.' AND ('.$col_name.' LIKE "%'.$search.'%")';
			}//////		Search Query 	/////////////////

		$colId = $data["order"][0]["column"];

		$sortColumn = $data['columns'][$colId]["data"];
		$sortOrder = $data["order"][0]["dir"];

		$founders = Founder::select("founders.*",
        \DB::RAW("(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."')) as created_at"),
		\DB::RAW("(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."')) as updated_at"),
		\DB::RAW("(SELECT COUNT(*) FROM startup_founders SF WHERE SF.founder_id=founders.founder_id) as startups_count")
        // \DB::RAW("( date_format(CONVERT_TZ(founders.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_at")
        )
		//->withCount('startups')
		->havingRaw($searchHavingRaw)
		->orderBy($sortColumn, $sortOrder)
        ->offset($data['start'])
        ->limit($data['length'])
        ->get()
        ->toArray();

		return Founder::format_html($founders);

	}

///////////		Format HTML 	/////////////////////////////////
public static function format_html($founders)
	{

		$con = [];
        $len = COUNT($founders);

		for($i=0;$i<$len;$i++)
			{
				$temp_id = $founders[$i]['founder_id'];//0

				$col_name = "table_".$temp_id."_".$i."_";// table_id_rowno_colno

				$temp_name = $founders[$i]['name'];
				$temp_startups_count = $founders[$i]['startups_count'];
				$temp_created_at = $founders[$i]['created_at'];
				$temp_updated_at = $founders[$i]['updated_at'];

				$name = '<button class="btn btn-default" id="'.$col_name.'0" onClick ="update_name(this.id)" title="'.$temp_name.'">'.$temp_name.'</button>';//0
				$startups_count = '<a class="btn btn-default" id="'.$col_name.'0"  href="'.route('admin.founders.startups',['founder_id' => $temp_id]).'" title="Roles">'.$temp_startups_count.'</a>';//1

				$created_at = '<i class="fa fa-clock-o"></i> '.$temp_created_at;//2
				$updated_at = '<i class="fa fa-clock-o"></i> '.$temp_updated_at;//3
				$delete = '<button data-confirm="Are you sure to delete?" class="btn btn-default delete" id="'.$col_name.'0" onClick ="delete_name(this.id)" title="Delete">Delete</button>';//4

				//<i class="fa fa-clock-o"></i> {{ $coupon->expires_atz }}

				$founders[$i]['name'] = $name;
				$founders[$i]['startups_count'] = $startups_count;
				$founders[$i]['created_at'] = $created_at;
				$founders[$i]['updated_at'] = $updated_at;
				$founders[$i]['delete'] = $delete;


			}

		return $founders;

	}


}

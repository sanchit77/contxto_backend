<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupCountry
 * 
 * @property int $startup_country_id
 * @property int $startup_id
 * @property int $country_id
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Startup $startup
 * @property \App\Models\Country $country
 *
 * @package App\Models
 */
class StartupCountriesTemp extends Eloquent
{
	protected $table = 'startup_countries_temp';
	protected $primaryKey = 'startup_country_id';

	protected $casts = [
		'startup_id' => 'int',
		'country_id' => 'int'
	];

	protected $fillable = [
		'startup_id',
		'country_id'
	];
}

<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupFounder
 * 
 * @property int $startup_founder_id
 * @property int $startup_id
 * @property int $founder_id
 * @property string $founder_link
 * @property string $source
 * @property string $roles
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Startup $startup
 * @property \App\Models\Founder $founder
 *
 * @package App\Models
 */
class StartupFoundersTemp extends Eloquent
{
	protected $table = 'startup_founders_temp';
	protected $primaryKey = 'startup_founder_id';

	protected $casts = [
		'startup_id' => 'int',
		'founder_id' => 'int'
	];

	protected $fillable = [
		'startup_id',
		'founder_id',
		'founder_link',
		'roles',
		'source'
	];
}

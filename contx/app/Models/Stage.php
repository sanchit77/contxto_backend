<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Investor
 *
 * @property int $investor_id
 * @property int $investor_type_id
 * @property string $name
 * @property string $created_by
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $startup_inverstors
 * @property \Illuminate\Database\Eloquent\Collection $startup_rounds
 *
 * @package App\Models
 */
class Stage extends Eloquent
{
  protected $table='stages';
	protected $primaryKey = 'stage_id';
  protected $timestamp=true;


	protected $fillable = [
		'name',
	];
}

<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Startup
 *
 * @property int $startup_id
 * @property int $country_id
 * @property int $city_id
 * @property int $industry_id
 * @property string $name
 * @property string $logo
 * @property string $description
 * @property string $founded
 * @property string $website
 * @property string $facebook
 * @property string $twitter
 * @property string $linkedin
 * @property string $instagram
 * @property string $valuation
 * @property string $ipo_symbol
 * @property bool $is_public
 * @property string $created_by
 * @property string $created_from
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Country $country
 * @property \App\Models\City $city
 * @property \Illuminate\Database\Eloquent\Collection $startup_countries
 * @property \Illuminate\Database\Eloquent\Collection $founders
 * @property \Illuminate\Database\Eloquent\Collection $startup_investors
 * @property \Illuminate\Database\Eloquent\Collection $startup_news
 * @property \Illuminate\Database\Eloquent\Collection $rounds
 *
 * @package App\Models
 */
class Startup extends Eloquent
{
	protected $primaryKey = 'startup_id';

	protected $casts = [
		'country_id' => 'int',
		'city_id' => 'int',
		'industry_id' => 'int',
		'is_public' => 'bool'
	];

	protected $fillable = [
		'country_id',
		'city_id',
		'industry_id',
		'name',
		'logo',
		'description',
		'founded',
		'website',
		'facebook',
		'twitter',
		'linkedin',
		'instagram',
		'valuation',
		'ipo_symbol',
		'is_public',
		'created_by',
		'created_from',
		'slug',
		'hunter'
	];

	public function country()
	{
		return $this->belongsTo(\App\Models\Country::class, 'country_id', 'country_id');
	}

	public function startup_founders()
	{
		return $this->belongsTo(\App\Models\StartupFounder::class, 'startup_id', 'startup_id');
	}

	public function city()
	{
		return $this->belongsTo(\App\Models\City::class, 'city_id', 'city_id');
	}

	public function startup_industries()
	{
		return $this->hasMany(\App\Models\StartupIndustry::class, 'startup_id', 'startup_id')
		->join('industries', 'industries.industry_id', 'startup_industries.industry_id')
		->select('startup_industries.industry_id', 'startup_industries.startup_id', 'name','industries.slug')
		;
	}
	public function startup_industries_api()
	{
		return $this->hasMany(\App\Models\StartupIndustry::class, 'startup_id', 'startup_id')
		->join('industries', 'industries.industry_id', 'startup_industries.industry_id')
		->select('startup_industries.startup_id', 'name')
		->orderBy('name','ASC');
	}

	public function startup_countries()
	{
		return $this->hasMany(\App\Models\StartupCountry::class, 'startup_id', 'startup_id')
		->join('countries', 'countries.country_id', 'startup_countries.country_id')
		->select('startup_countries.country_id', 'startup_countries.startup_id', 'name')
		;
	}

	public function founders()
	{
		return $this->hasMany(\App\Models\StartupFounder::class, 'startup_id', 'startup_id')
		->select('startup_founder_id', 'startup_founders.startup_id', 'name','startup_founders.founder_id', 'startup_founders.founder_id as dynamic_db_id', 'founder_link', 'roles','slug')
		->join('founders','founders.founder_id', 'startup_founders.founder_id')
		;
	}

	public function investors()
	{
		return $this->hasMany(\App\Models\StartupInvestor::class, 'startup_id', 'startup_id')
		->select('startup_investor_id', 'startup_investors.startup_id', 'investors.name as investory_name', 'investors.investor_id', 'investors.investor_id as dynamic_db_id',
		\DB::RAW("COALESCE(investor_types.name) as investory_type_name")
		)
		->join('investors', 'investors.investor_id', 'startup_investors.investor_id')
		->leftjoin('investor_types', 'investor_types.investor_type_id', 'investors.investor_type_id');

	}

	public function startup_rounds()
	{
		return $this->hasMany(\App\Models\StartupRound::class, 'startup_id', 'startup_id')
		->join('rounds', 'rounds.round_id', 'startup_rounds.round_id')
		->join('investors', 'investors.investor_id', 'startup_rounds.investor_id')
		->select('startup_rounds.startup_round_id', 'startup_rounds.round_id', 'startup_rounds.investor_id', 'startup_rounds.startup_id', 'rounds.name as round_name', 'investors.name as investor_name', 'amount', 'dt','investors_name');
	}



	public function startup_rounds_cal()
	{
		return $this->hasMany(\App\Models\StartupRound::class, 'startup_id', 'startup_id');

	}

	public function startup_rounds_sum()
	{
		return $this->hasMany(\App\Models\StartupRound::class, 'startup_id', 'startup_id');

	}

	public function rounds()
	{
		return $this->belongsToMany(\App\Models\Round::class, 'startup_rounds', 'round_id', 'round_id')
			->withPivot('startup_round_id', 'investor_id', 'amount', 'dt')
			->withTimestamps();
	}

	public function startup_news()
	{
		return $this->hasMany(\App\Models\StartupNews::class, 'startup_id', 'startup_id');
	}



////////		Admin Listings 		/////////////////////

public static function admin_listings($start, $length)
{

	$startups = Startup::select("*")
	->with([
			'country',
			'city'
	])
	->offset($start)
	->limit($length);

	return $startups->get();

}

///////////////		Make Filter Query 	///////////////////////
public static function make_filter_query($columns, $order)
{
	$having_raw_string = "(startup_id != 0)";

	$founder_key = "";//4
	$investors_key = "";//5
	$target_countries_key = "";//7
	$industry_key = "";//12
	$startup_news_count_key = "";//13

	foreach($columns as $key => $column)
		{
			if($column['search']['value'] == null)
				continue;

			if(in_array($key, [0, 1, 2, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 20]))
				{
					$having_raw_string = $having_raw_string.' AND ('.$column['data'].' LIKE "%'.$column['search']['value'].'%")';
				}
			elseif($key == 4)
				{
					$founder_key = $column['search']['value'];
				}
			elseif($key == 5)
				{
					$investors_key = $column['search']['value'];
				}
			elseif($key == 7)
				{
					$target_countries_key = $column['search']['value'];
				}
			elseif($key == 12)
				{
					$industry_key = $column['search']['value'];
				}
			elseif($key == 13)
				{
					$startup_news_count_key = $column['search']['value'];
				}
		}

//////////////////		Sorting 	////////////////////////
	$column = $order[0]['column'];
	$direction = $order[0]['dir'];

	if(in_array($column, [0, 1, 2, 8, 9, 10, 11, 14, 15, 16, 17, 18, 19, 20]))
		$sort_key = $columns[$column]["data"];
	elseif($column == 4)
		$sort_key = "founders_count";
	elseif($column == 5)
		$sort_key = "investors_count";
	elseif($column == 7)
		$sort_key = "startup_countries_count";
	elseif($column == 12)
		$sort_key = "startup_industries_count";
	elseif($column == 13)
		$sort_key = "startup_news_count";
	else
		$sort_key = "startup_id";
//////////////////		Sorting 	/////////////////////////

	// var allColumns = [
	// 	{ "data": "startup_rounds", "orderable": false },//6
	// ];

	return [
		"having_raw_string"=> $having_raw_string,
		"founder_key" => $founder_key,//4
		"investors_key" => $investors_key,//5
		"target_countries_key" => $target_countries_key,//7
		"industry_key" => $industry_key,//12
		"startup_news_count_key" => $startup_news_count_key,//13
		'sort_key' => $sort_key,
		'direction' => $direction
	];

}

////////////////	Admin Dynamic Searching Count 	////////////
public static function admin_dynamic_searching_count($data)
{
	$startups = Startup::select("startups.startup_id", "startups.name", "startups.valuation", "startups.description", "startups.founded", "startups.website", "startups.facebook", "startups.twitter", "startups.linkedin", "startups.instagram", "startups.ipo_symbol",
	\DB::RAW("COALESCE(countries.name, '') as country"),
	\DB::RAW("COALESCE(cities.name, '') as city"),
	\DB::RAW("(CASE WHEN is_public IS true THEN 'Public' else 'Private' END) as is_public")
	)
	->leftjoin('countries', 'countries.country_id', 'startups.country_id')
	->leftjoin('cities', 'cities.city_id', 'startups.city_id')
	->withCount('startup_news');

	////////////		Other Pages Filtering 		//////////////////
	if(isset($data['industry_id']) && ($data['industry_id'] != 0))
		{
			$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_industries SI WHERE SI.startup_id=startups.startup_id AND SI.industry_id='.$data['industry_id'].') > 0 )';
		}
	if(isset($data['founder_id']) && ($data['founder_id'] != 0))
		{
			$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_founders SF WHERE SF.startup_id=startups.startup_id AND SF.founder_id='.$data['founder_id'].') > 0 )';
		}
	if(isset($data['investor_id']) && ($data['investor_id'] != 0))
		{
			$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_investors SI WHERE SI.startup_id=startups.startup_id AND SI.investor_id='.$data['investor_id'].') > 0 )';
		}
	////////////		Other Pages Filtering 		//////////////////

	$startups->havingRaw($data['filter']['having_raw_string']);

	/////////		Founder Filter 	///////////////////
	if($data['filter']['founder_key'] != '')
		{
			$filter = $data['filter']['founder_key'];

			$startups->whereHas('founders', function($q1) use ($filter){

				$q1->join('founders', 'founders.founder_id', 'startup_founders.founder_id');

				$q1->where('founders.name', 'LIKE', "%".$filter."%");

			});
		}
	/////////		Founder Filter 	///////////////////

	/////////		Investor Filter ///////////////////
	if($data['filter']['investors_key'] != '')
		{
			$filter = $data['filter']['investors_key'];

			$startups->whereHas('investors', function($q1) use ($filter){

				$q1->join('investors', 'investors.investor_id', 'startup_investors.investor_id');

				$q1->where('investors.name', 'LIKE', "%".$filter."%");

			});
		}
	/////////		Investor Filter ///////////////////

	/////////		Target Countries Filter ///////////
	if($data['filter']['target_countries_key'] != '')
	{
		$filter = $data['filter']['target_countries_key'];

		$startups->whereHas('startup_countries', function($q1) use ($filter){

			$q1->join('countries', 'countries.country_id', 'startup_countries.country_id');

			$q1->where('countries.name', 'LIKE', $filter."%");

		});
	}
	/////////		Target Countries Filter ///////////

	///////////		Industries Filter /////////////////
	if($data['filter']['industry_key'] != '')
	{
		$filter = $data['filter']['industry_key'];

		$startups->whereHas('startup_industries', function($q1) use ($filter){

			$q1->join('industries', 'industries.industry_id', 'startup_industries.industry_id');

			$q1->where('industries.name', 'LIKE', $filter."%");

		});
	}
	///////////		Industries Filter /////////////////

	///////////		News Filter ///////////////////////
	if($data['filter']['startup_news_count_key'] != '')
		{
			$filter = $data['filter']['startup_news_count_key'];

			$startups->whereHas('startup_news', function($q1) use ($filter){

				$q1->where('startup_news.description', 'LIKE', "%".$filter."%")->orWhere('startup_news.url', 'LIKE', "%".$filter."%");

			});
		}
	///////////		News Filter ///////////////////////


	//return $startups->count();
	return $startups->get();

}

////////////////	Admin Dynamic Searching 	///////////////
public static function admin_dynamic_searching($data)
{

	//dd($data['filter']['founder_key']);

	\DB::enableQueryLog();

	$startups = Startup::select("startups.startup_id", "startups.name", "startups.valuation", "startups.description", "startups.founded", "startups.website", "startups.facebook", "startups.twitter", "startups.linkedin", "startups.instagram", "startups.ipo_symbol", "logo", "startups.country_id", "startups.city_id",
	\DB::RAW("COALESCE(countries.name, '') as country"),
	\DB::RAW("COALESCE(cities.name, '') as city"),
	\DB::RAW("(CASE WHEN is_public IS true THEN 'Public' else 'Private' END) as is_public")
	)
	->leftjoin('countries', 'countries.country_id', 'startups.country_id')
	->leftjoin('cities', 'cities.city_id', 'startups.city_id')
	->withCount([
		'startup_news',

		'startup_industries',
		'investors',
		'startup_rounds',
		'startup_countries',
		'founders'
	])
	->with([
		'startup_industries',

		'investors',
		'startup_rounds',
		'startup_countries',
		'founders'
	]);

	////////////		Other Pages Filtering 		//////////////////
	if(isset($data['industry_id']) && ($data['industry_id'] != 0))
		{
			$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_industries SI WHERE SI.startup_id=startups.startup_id AND SI.industry_id='.$data['industry_id'].') > 0 )';
		}
	if(isset($data['founder_id']) && ($data['founder_id'] != 0))
		{
			$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_founders SF WHERE SF.startup_id=startups.startup_id AND SF.founder_id='.$data['founder_id'].') > 0 )';
		}
	if(isset($data['investor_id']) && ($data['investor_id'] != 0))
		{
			$data['filter']['having_raw_string'] = $data['filter']['having_raw_string']. ' AND ( (SELECT COUNT(*) FROM startup_investors SI WHERE SI.startup_id=startups.startup_id AND SI.investor_id='.$data['investor_id'].') > 0 )';
		}
	////////////		Other Pages Filtering 		//////////////////

	$startups->havingRaw($data['filter']['having_raw_string']);

	/////////		Founder Filter 	///////////////////
	if($data['filter']['founder_key'] != '')
		{
			$filter = $data['filter']['founder_key'];

			$startups->whereHas('founders', function($q1) use ($filter){

				$q1->join('founders', 'founders.founder_id', 'startup_founders.founder_id');

				$q1->where('founders.name', 'LIKE', "%".$filter."%")->orWhere("roles", 'LIKE', "%".$filter."%")->orWhere("source", 'LIKE', "%".$filter."%");

			});
		}
	/////////		Founder Filter 	///////////////////

	/////////		Investor Filter ///////////////////
	if($data['filter']['investors_key'] != '')
		{
			$filter = $data['filter']['investors_key'];

			$startups->whereHas('investors', function($q1) use ($filter){

				$q1->join('investors', 'investors.investor_id', 'startup_investors.investor_id');

				$q1->where('investors.name', 'LIKE', "%".$filter."%");

			});
		}
	/////////		Investor Filter ///////////////////

	/////////		Target Countries Filter ///////////
		if($data['filter']['target_countries_key'] != '')
		{
			$filter = $data['filter']['target_countries_key'];

			$startups->whereHas('startup_countries', function($q1) use ($filter){

				$q1->join('countries', 'countries.country_id', 'startup_countries.country_id');

				$q1->where('countries.name', 'LIKE', $filter."%");

			});
		}
	/////////		Target Countries Filter ///////////

	///////////		Industries Filter /////////////////
	if($data['filter']['industry_key'] != '')
	{
		$filter = $data['filter']['industry_key'];

		$startups->whereHas('startup_industries', function($q1) use ($filter){

			$q1->join('industries', 'industries.industry_id', 'startup_industries.industry_id');

			$q1->where('industries.name', 'LIKE', $filter."%");

		});
	}
	///////////		Industries Filter /////////////////

	///////////		News Filter ///////////////////////
	if($data['filter']['startup_news_count_key'] != '')
		{
			$filter = $data['filter']['startup_news_count_key'];

			$startups->whereHas('startup_news', function($q1) use ($filter){

				$q1->where('startup_news.description', 'LIKE', "%".$filter."%")->orWhere('startup_news.url', 'LIKE', "%".$filter."%");

			});
		}
	///////////		News Filter ///////////////////////

	$startups->orderBy($data['filter']['sort_key'], $data['filter']['direction']);

	//$temp = $startups;
	$startups = $startups->offset($data['start'])->limit($data['length']);
	//$startups1 = $temp->offset($data['start'])->limit($data['length'])
	//->toSql();

	return $startups
	->get()
	->toArray();

}

//////////////		Startup Create 		///////////////////////////
public static function create_startup($data)
{

	$startup = new Startup();

	$startup->country_id = $data['country_id'];
	$startup->city_id = isset($data['city_id']) ? $data['city_id'] : 0;
	$startup->industry_id = isset($data['industry_id']) ? $data['industry_id'] : 0;

	$startup->name = $data['name'];
	$startup->logo = isset($data['logo']) ? $data['logo'] : '';
	$startup->description = isset($data['description']) ? $data['description'] : '';
	$startup->founded = isset($data['founded']) ? $data['founded'] : '';

	$startup->website = isset($data['website']) ? $data['website'] : '';
	$startup->facebook = isset($data['facebook']) ? $data['facebook'] : '';
	$startup->twitter = isset($data['twitter']) ? $data['twitter'] : '';
	$startup->linkedin = isset($data['linkedin']) ? $data['linkedin'] : '';
	$startup->instagram = isset($data['instagram']) ? $data['instagram'] : '';
	$startup->valuation = isset($data['valuation']) ? $data['valuation'] : '';

	$startup->ipo_symbol = isset($data['ipo_symbol']) ? $data['ipo_symbol'] : '';
	$startup->is_public = isset($data['is_public']) ? $data['is_public'] : '1';

	$startup->created_by = $data['created_by'];
	$startup->created_from = $data['created_from'];

	$startup->save();

	return $startup;

}

//////////////		Startup Update 		///////////////////////////
public static function update_startup($data, $old)
{

	return Startup::where('startup_id', $data['startup_id'])->update([
		'country_id' => $data['country_id'],
		'city_id' => isset($data['city_id']) ? $data['city_id'] : $old->city_id,
		'industry_id' => isset($data['industry_id']) ? $data['industry_id'] : $old->industry_id,

		'name' => $data['name'],
		'logo' => isset($data['logo']) ? $data['logo'] : $old->logo,
		'description' => isset($data['description']) ? $data['description'] : $old->description,
		'founded' => isset($data['founded']) ? $data['founded'] : $old->founded,

		'website' => isset($data['website']) ? $data['website'] : $old->website,
		'facebook' => isset($data['facebook']) ? $data['facebook'] : $old->facebook,
		'twitter' => isset($data['twitter']) ? $data['twitter'] : $old->twitter,
		'linkedin' => isset($data['linkedin']) ? $data['linkedin'] : $old->linkedin,
		'instagram' => isset($data['instagram']) ? $data['instagram'] : $old->instagram,
		'valuation' => isset($data['valuation']) ? $data['valuation'] : $old->valuation,

		'ipo_symbol' => isset($data['ipo_symbol']) ? $data['ipo_symbol'] : $old->ipo_symbol,
		'is_public' => isset($data['is_public']) ? $data['is_public'] : $old->is_public,

	]);


}

/////////////////	Api Statup Listing 	///////////////////////////
public static function startups_list($data)
{
	//\DB::enableQueryLog();

	$startups = Startup::select("startup_id", "countries.name as country_name", "cities.name as city_name", "code", "startups.name", "logo", "description", "website", "facebook", "twitter", "linkedin", "instagram");

	$startups->leftjoin('countries', 'countries.country_id', 'startups.country_id');
	$startups->leftJoin('cities', 'cities.city_id', 'startups.city_id');

	////////////		Startup Industries 	///////////////////////
	$startups->with('startup_industries_api');
	////////////		Startup Industries 	///////////////////////

	///////////			Filtering Where 	///////////////////////
	if(isset($data['filter_countries']))
		{
			$startups->whereIn('startups.country_id',$data['filter_countries']);
		}
	if(isset($data['filter_industries']) && COUNT($data['filter_industries']))
		{
			$data['filter_industries'] = implode(',',$data['filter_industries']);
			$startups->whereRaw("( (SELECT COUNT(*) FROM startup_industries SI WHERE SI.startup_id=startups.startup_id AND SI.industry_id IN (".$data['filter_industries'].")  ) > 0 )");
		}
	if(isset($data['filter_city']) && ($data['filter_city'] != ''))
		$startups->having('city_name', 'LIKE', '%'.$data['filter_city'].'%');
	///////////			Filtering Where 	///////////////////////

	$startups->orderBy('name','ASC');

	$startups->skip($data['skip'])->take($data['take']);

	$startups = $startups->get();
	//dd(\DB::getQueryLog());

	return $startups;

}

//////////////////

}

<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class InvestorType
 * 
 * @property int $investor_type_id
 * @property string $name
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class InvestorType extends Eloquent
{
	protected $primaryKey = 'investor_type_id';

	protected $casts = [
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'name',
		'is_blocked'
	];
}

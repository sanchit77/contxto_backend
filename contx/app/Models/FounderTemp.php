<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Founder
 *
 * @property int $founder_id
 * @property string $name
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \Illuminate\Database\Eloquent\Collection $startups
 *
 * @package App\Models
 */
class FounderTemp extends Eloquent
{
	protected $table = 'founders_temp';
	protected $primaryKey = 'founder_id';

	protected $guarded = [];
}

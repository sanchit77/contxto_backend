<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Industry
 *
 * @property int $industry_id
 * @property string $name
 * @property string $source
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @package App\Models
 */
class Industry extends Eloquent
{
	protected $primaryKey = 'industry_id';

	protected $casts = [
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'name',
		'source',
		'is_blocked'
	];

	public function startups()
	{
		return $this->belongsToMany(\App\Models\Startup::class, 'startup_industries', 'industry_id', 'industry_id')
			->withPivot('startup_industry_id')
				->withTimestamps();
	}


//////////		Create Industry 	///////////////////
public static function create($name, $source)
{
	$industry = new Industry();

	$industry->name = $name;
	$industry->source = $source;

	$industry->save();

	return $industry->industry_id;

}

///////////		Ajax Data 	/////////////////////////
	public static function ajax_data($data)
	{
		$searchHavingRaw = "(industry_id != 0)";

		foreach($data['columns'] as $key => $column)
			{//////		Search Query 	/////////////////
				$col_name = $column['data'];
				$search = $column['search']['value'];

				if($search == null)
					continue;

				$searchHavingRaw = $searchHavingRaw.' AND ('.$col_name.' LIKE "%'.$search.'%")';
			}//////		Search Query 	/////////////////

		$colId = $data["order"][0]["column"];

		$sortColumn = $data['columns'][$colId]["data"];
		$sortOrder = $data["order"][0]["dir"];

		$industries = Industry::select("industries.*",
        \DB::RAW("(CONVERT_TZ(created_at,'+00:00','".$data['timezonez']."')) as created_at"),
        \DB::RAW("(CONVERT_TZ(updated_at,'+00:00','".$data['timezonez']."')) as updated_at")
        // \DB::RAW("( date_format(CONVERT_TZ(industries.updated_at,'+00:00','".$data['timezonez']."'),'%M %d %Y. %h:%i %p') ) as updated_at")
        )
		->withCount('startups')
		->havingRaw($searchHavingRaw)
		->orderBy($sortColumn, $sortOrder)
        ->offset($data['start'])
        ->limit($data['length'])
        ->get()
        ->toArray();

		//return $industries;
		return Industry::format_html($industries);

	}

///////////		Format HTML 	/////////////////////////////////
public static function format_html($industries)
	{

		$con = [];
        $len = COUNT($industries);

		for($i=0;$i<$len;$i++)
			{
				$temp_id = $industries[$i]['industry_id'];//0

				$col_name = "table_".$temp_id."_".$i."_";// table_id_rowno_colno

				$temp_name = $industries[$i]['name'];
				$temp_startups_count = $industries[$i]['startups_count'];
				$temp_created_at = $industries[$i]['created_at'];
				$temp_updated_at = $industries[$i]['updated_at'];

				$name = '<button class="btn btn-default" id="'.$col_name.'0" onClick ="update_name(this.id)" title="'.$temp_name.'">'.$temp_name.'</button>';//0
				$startups_count = '<a class="btn btn-default" id="'.$col_name.'0"  href="'.route('admin.industries.startups',['industry_id' => $temp_id]).'" title="Roles">'.$temp_startups_count.'</a>';//1
//
				$created_at = '<i class="fa fa-clock-o"></i> '.$temp_created_at;//2
				$updated_at = '<i class="fa fa-clock-o"></i> '.$temp_updated_at;//3
        $delete = '<button data-confirm="Are you sure to delete?" class="btn btn-default delete" id="'.$col_name.'0" onClick ="delete_name(this.id)" title="Delete">Delete</button>';//4
				//<i class="fa fa-clock-o"></i> {{ $coupon->expires_atz }}

				$industries[$i]['name'] = $name;
				$industries[$i]['startups_count'] = $startups_count;
				$industries[$i]['created_at'] = $created_at;
				$industries[$i]['updated_at'] = $updated_at;
        $industries[$i]['delete'] = $delete;

			}

		return $industries;

	}

}

<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupNews
 * 
 * @property int $startup_news_id
 * @property int $startup_id
 * @property string $url
 * @property string $image
 * @property string $description
 * @property string $source
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \App\Models\Startup $startup
 *
 * @package App\Models
 */
class StartupNews extends Eloquent
{
	protected $primaryKey = 'startup_news_id';

	protected $casts = [
		'startup_id' => 'int',
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'startup_id',
		'url',
		'image',
		'description',
		'source',
		'is_blocked'
	];

	public function startup()
	{
		return $this->belongsTo(\App\Models\Startup::class,'startup_id','startup_id');
	}
}

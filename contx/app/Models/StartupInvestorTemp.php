<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class StartupInvestor
 *
 * @property int $startup_investor_id
 * @property int $startup_id
 * @property int $investor_id
 * @property bool $is_blocked
 * @property bool $source
 * @property bool $investor_link
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 *
 * @property \App\Models\Startup $startup
 * @property \App\Models\Investor $investor
 *
 * @package App\Models
 */
class StartupInvestorTemp extends Eloquent
{
  protected $table='startup_investors_temp';
	protected $primaryKey = 'startup_investor_id';

	protected $casts = [
		'startup_id' => 'int',
		'investor_id' => 'int',
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'startup_id',
		'investor_id',
		'is_blocked',
		'source',
		'investor_link'
	];

	public function startup()
	{
		return $this->belongsTo(\App\Models\Startup::class, 'startup_id', 'startup_id');
	}

	public function investor()
	{
		return $this->belongsTo(\App\Models\InvestorTemp::class, 'investor_id', 'investor_id');
	}

	////////	Add new founder 	//////////////////////////
	public static function add_new($startup_id, $investor_id, $source, $investor_link)
		{

			$startup_investor = new StartupInvestorTemp();

			$startup_investor->startup_id = $startup_id;
			$startup_investor->investor_id = $investor_id;
			$startup_investor->source= $source;
			$startup_investor->investor_link = $investor_link;
			$startup_investor->save();

			return $startup_investor->investor_id;

		}

}

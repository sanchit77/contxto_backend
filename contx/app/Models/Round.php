<?php

namespace App\Models;

use Reliese\Database\Eloquent\Model as Eloquent;

/**
 * Class Round
 * 
 * @property int $round_id
 * @property string $name
 * @property bool $is_blocked
 * @property \Carbon\Carbon $created_at
 * @property \Carbon\Carbon $updated_at
 * 
 * @property \Illuminate\Database\Eloquent\Collection $startups
 *
 * @package App\Models
 */
class Round extends Eloquent
{
	protected $primaryKey = 'round_id';

	protected $casts = [
		'is_blocked' => 'bool'
	];

	protected $fillable = [
		'name',
		'is_blocked'
	];

	public function startups()
	{
		return $this->belongsToMany(\App\Models\Startup::class, 'startup_rounds')
					->withPivot('startup_round_id', 'investor_id', 'amount', 'dt')
					->withTimestamps();
	}
}

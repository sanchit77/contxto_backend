<?php

namespace App\Console;

use Illuminate\Console\Scheduling\Schedule;
use Illuminate\Foundation\Console\Kernel as ConsoleKernel;

class Kernel extends ConsoleKernel
{
    /**
     * The Artisan commands provided by your application.
     *
     * @var array
     */
    protected $commands = [
        'App\Console\Commands\SyncWeb1',
    ];

    /**
     * Define the application's command schedule.
     *
     * @param  \Illuminate\Console\Scheduling\Schedule  $schedule
     * @return void
     */
    protected function schedule(Schedule $schedule)
    {
        // $schedule->command('inspire')
        //          ->hourly();

        $schedule->command('sync:web1')
            //->hourlyAt(8);
            ->twiceDaily(1, 13)
            ->timezone('Asia/Calcutta');
            //->everyMinute();

        $schedule->command('sync:web2345 --id=2')
            ->twiceDaily(2, 14)
            ->timezone('Asia/Calcutta');

        $schedule->command('sync:web2345 --id=3')
            ->twiceDaily(3, 15)
            ->timezone('Asia/Calcutta');

        $schedule->command('sync:web2345 --id=4')
            ->twiceDaily(4, 16)
            ->timezone('Asia/Calcutta');

        $schedule->command('sync:web2345 --id=5')
            ->twiceDaily(5, 17)
            ->timezone('Asia/Calcutta');

    }

    /**
     * Register the commands for the application.
     *
     * @return void
     */
    protected function commands()
    {
        $this->load(__DIR__.'/Commands');

        require base_path('routes/console.php');
    }
}

<?php

namespace App\Console\Commands;

use Illuminate\Console\Command;

use App\Http\Controllers\Sync\SyncCont;

class SyncWeb2345 extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'sync:web2345 {--id=2}';
    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Sync Website 2, 3, 4, 5';
    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }
    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {
        $id = $this->option('id') ? $this->option('id') : 2;
        //dd($this->options(), $id);
        SyncCont::sync2($id);
        //
    }
}

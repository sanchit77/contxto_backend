<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStartupInverstorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startup_inverstors', function(Blueprint $table)
		{
			$table->bigInteger('startup_inverstor_id', true)->unsigned();
			$table->bigInteger('startup_id')->unsigned()->index('startup_id');
			$table->bigInteger('investor_id')->unsigned()->index('investor_id');
			$table->boolean('is_blocked')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startup_inverstors');
	}

}

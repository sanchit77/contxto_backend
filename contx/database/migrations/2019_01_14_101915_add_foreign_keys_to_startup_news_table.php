<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStartupNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('startup_news', function(Blueprint $table)
		{
			$table->foreign('startup_id', 'startup_news_ibfk_1')->references('startup_id')->on('startups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('startup_news', function(Blueprint $table)
		{
			$table->dropForeign('startup_news_ibfk_1');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStartupRoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('startup_rounds', function(Blueprint $table)
		{
			$table->foreign('round_id', 'startup_rounds_ibfk_1')->references('round_id')->on('rounds')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('investor_id', 'startup_rounds_ibfk_2')->references('investor_id')->on('investors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('startup_id', 'startup_rounds_ibfk_3')->references('startup_id')->on('startups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('startup_rounds', function(Blueprint $table)
		{
			$table->dropForeign('startup_rounds_ibfk_1');
			$table->dropForeign('startup_rounds_ibfk_2');
			$table->dropForeign('startup_rounds_ibfk_3');
		});
	}

}

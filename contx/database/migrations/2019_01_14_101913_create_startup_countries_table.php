<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStartupCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startup_countries', function(Blueprint $table)
		{
			$table->bigInteger('startup_country_id', true)->unsigned();
			$table->bigInteger('startup_id')->unsigned()->index('startup_id');
			$table->bigInteger('country_id')->unsigned()->index('country_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startup_countries');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStartupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startups', function(Blueprint $table)
		{
			$table->bigInteger('startup_id', true)->unsigned();
			$table->bigInteger('country_id')->unsigned()->index('country_id');
			$table->bigInteger('city_id')->unsigned()->index('city_id');
			$table->bigInteger('industry_id');
			$table->string('name');
			$table->string('logo', 100);
			$table->text('description', 65535)->nullable();
			$table->timestamp('founded')->default(DB::raw('CURRENT_TIMESTAMP'));
			$table->string('website', 100)->nullable();
			$table->string('facebook', 100)->nullable();
			$table->string('twitter', 100)->nullable();
			$table->string('linkedin', 100)->nullable();
			$table->string('instagram', 100)->nullable();
			$table->string('valuation', 100)->nullable();
			$table->string('ipo_symbol', 100);
			$table->boolean('is_public')->default(1);
			$table->boolean('is_blocked')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startups');
	}

}

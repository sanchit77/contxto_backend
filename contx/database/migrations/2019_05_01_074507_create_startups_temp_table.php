<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartupsTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startups_temp', function (Blueprint $table) {
            $table->charset = 'utf8';
            $table->collation = 'utf8_unicode_ci';
            $table->bigInteger('startup_id', true)->unsigned();
            $table->bigInteger('country_id')->unsigned()->index('country_id');
            $table->bigInteger('city_id')->unsigned()->index('city_id')->nullable();
            $table->bigInteger('industry_id');
            $table->string('name');
            $table->string('logo', 100);
            $table->text('description', 65535)->nullable();
            $table->string('founded',20);
            $table->string('website', 100)->nullable();
            $table->string('facebook', 100)->nullable();
            $table->string('twitter', 100)->nullable();
            $table->string('linkedin', 100)->nullable();
            $table->string('instagram', 100)->nullable();
            $table->string('valuation', 100)->nullable();
            $table->string('ipo_symbol', 100);
            $table->boolean('is_public')->default(1);
            $table->string('created_by',5)->default('Cron');
            $table->string('created_from',5)->nullable();
            $table->string('slug')->nullable();
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('country_id', 'startups_temp_ibfk_1')->references('country_id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('city_id', 'startups_temp_ibfk_2')->references('city_id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startups_temp');
    }
}

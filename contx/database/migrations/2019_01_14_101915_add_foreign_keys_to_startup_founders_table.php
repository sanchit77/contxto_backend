<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStartupFoundersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('startup_founders', function(Blueprint $table)
		{
			$table->foreign('startup_id', 'startup_founders_ibfk_1')->references('startup_id')->on('startups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('founder_id', 'startup_founders_ibfk_2')->references('founder_id')->on('founders')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('startup_founders', function(Blueprint $table)
		{
			$table->dropForeign('startup_founders_ibfk_1');
			$table->dropForeign('startup_founders_ibfk_2');
		});
	}

}

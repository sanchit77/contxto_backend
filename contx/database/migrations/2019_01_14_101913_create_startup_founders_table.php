<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStartupFoundersTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startup_founders', function(Blueprint $table)
		{
			$table->bigInteger('startup_founder_id', true)->unsigned();
			$table->bigInteger('startup_id')->unsigned()->index('startup_id');
			$table->bigInteger('founder_id')->unsigned()->index('founder_id');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startup_founders');
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStartupNewsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startup_news', function(Blueprint $table)
		{
			$table->bigInteger('startup_news_id', true)->unsigned();
			$table->bigInteger('startup_id')->unsigned()->index('startup_id');
			$table->string('url');
			$table->string('image', 100);
			$table->text('description', 65535)->nullable();
			$table->string('source', 20);
			$table->boolean('is_blocked')->default(0);
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startup_news');
	}

}

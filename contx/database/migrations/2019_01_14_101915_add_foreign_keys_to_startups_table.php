<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStartupsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('startups', function(Blueprint $table)
		{
			$table->foreign('country_id', 'startups_ibfk_1')->references('country_id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('city_id', 'startups_ibfk_2')->references('city_id')->on('cities')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('startups', function(Blueprint $table)
		{
			$table->dropForeign('startups_ibfk_1');
			$table->dropForeign('startups_ibfk_2');
		});
	}

}

<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class CreateStartupRoundsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::create('startup_rounds', function(Blueprint $table)
		{
			$table->bigInteger('startup_round_id', true)->unsigned();
			$table->bigInteger('round_id')->unsigned()->index('round_id');
			$table->bigInteger('investor_id')->unsigned()->index('investor_id');
			$table->bigInteger('startup_id')->unsigned()->index('startup_id');
			$table->string('amount', 50);
			$table->date('dt');
			$table->timestamps();
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::drop('startup_rounds');
	}

}

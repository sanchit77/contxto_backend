<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStartupInverstorsTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('startup_inverstors', function(Blueprint $table)
		{
			$table->foreign('startup_id', 'startup_inverstors_ibfk_1')->references('startup_id')->on('startups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('investor_id', 'startup_inverstors_ibfk_2')->references('investor_id')->on('investors')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('startup_inverstors', function(Blueprint $table)
		{
			$table->dropForeign('startup_inverstors_ibfk_1');
			$table->dropForeign('startup_inverstors_ibfk_2');
		});
	}

}

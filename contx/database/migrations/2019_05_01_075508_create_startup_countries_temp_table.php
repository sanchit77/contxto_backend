<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateStartupCountriesTempTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('startup_countries_temp', function (Blueprint $table) {
            $table->bigInteger('startup_country_id', true)->unsigned();
            $table->bigInteger('startup_id')->unsigned()->index('startup_id');
            $table->bigInteger('country_id')->unsigned()->index('country_id');
            $table->timestamp('created_at')->default(DB::raw('CURRENT_TIMESTAMP'));
            $table->timestamp('updated_at')->default(DB::raw('CURRENT_TIMESTAMP'));

            $table->foreign('startup_id', 'startup_countries_temp_ibfk_1')->references('startup_id')->on('startups_temp')->onUpdate('RESTRICT')->onDelete('RESTRICT');
            $table->foreign('country_id', 'startup_countries_temp_ibfk_2')->references('country_id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('startup_countries_temp');
    }
}

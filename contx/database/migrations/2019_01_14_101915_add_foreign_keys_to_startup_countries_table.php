<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;

class AddForeignKeysToStartupCountriesTable extends Migration {

	/**
	 * Run the migrations.
	 *
	 * @return void
	 */
	public function up()
	{
		Schema::table('startup_countries', function(Blueprint $table)
		{
			$table->foreign('startup_id', 'startup_countries_ibfk_1')->references('startup_id')->on('startups')->onUpdate('RESTRICT')->onDelete('RESTRICT');
			$table->foreign('country_id', 'startup_countries_ibfk_2')->references('country_id')->on('countries')->onUpdate('RESTRICT')->onDelete('RESTRICT');
		});
	}


	/**
	 * Reverse the migrations.
	 *
	 * @return void
	 */
	public function down()
	{
		Schema::table('startup_countries', function(Blueprint $table)
		{
			$table->dropForeign('startup_countries_ibfk_1');
			$table->dropForeign('startup_countries_ibfk_2');
		});
	}

}

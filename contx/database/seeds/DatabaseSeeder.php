<?php

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // $this->call(UsersTableSeeder::class);
        $this->call(CountriesTableSeeder::class);
        $this->call(CitiesTableSeeder::class);
        $this->call(FoundersTableSeeder::class);
        $this->call(IndustriesTableSeeder::class);
        $this->call(InvestorsTableSeeder::class);
        $this->call(InvestorTypesTableSeeder::class);
        $this->call(RoundsTableSeeder::class);
        $this->call(StartupsTableSeeder::class);
        $this->call(StartupCountriesTableSeeder::class);
        $this->call(StartupFoundersTableSeeder::class);
    }
}

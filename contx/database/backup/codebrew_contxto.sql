-- phpMyAdmin SQL Dump
-- version 4.5.4.1deb2ubuntu2.1
-- http://www.phpmyadmin.net
--
-- Host: localhost
-- Generation Time: Jan 30, 2019 at 05:02 PM
-- Server version: 5.7.25-0ubuntu0.16.04.2
-- PHP Version: 7.0.32-0ubuntu0.16.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `codebrew_contxto`
--

-- --------------------------------------------------------

--
-- Table structure for table `admins`
--

CREATE TABLE `admins` (
  `admin_id` bigint(20) NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `job` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `email` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `profile_pic` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `password` text COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `admins`
--

INSERT INTO `admins` (`admin_id`, `name`, `job`, `email`, `profile_pic`, `password`, `created_at`, `updated_at`) VALUES
(1, 'Admin', 'Admin', 'admin@gmail.com', '992a795214ce61764b7bfac7fe0e36753f366ddeimQcKLPzb64lUUugDPL7AeA0u.jpg', '$2y$10$XLtqKo2QWV8OK9U0Jbdg3.giOezjTW1CZTytbRKZnkR8rjAjPLwoi', '2019-01-14 13:01:55', '2019-01-29 04:35:00');

-- --------------------------------------------------------

--
-- Table structure for table `cities`
--

CREATE TABLE `cities` (
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `cities`
--

INSERT INTO `cities` (`city_id`, `country_id`, `name`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 1, 'City 1', 0, '2019-01-21 12:36:30', '2019-01-21 12:36:30'),
(2, 1, 'City 12', 0, '2019-01-21 12:36:54', '2019-01-21 12:36:54'),
(4, 1, 'City 13', 0, '2019-01-21 12:49:25', '2019-01-21 12:49:25'),
(5, 1, '1', 0, '2019-01-21 12:49:58', '2019-01-21 12:49:58'),
(6, 1, '1', 0, '2019-01-21 12:51:29', '2019-01-21 12:51:29'),
(7, 1, '2', 0, '2019-01-21 12:53:15', '2019-01-21 12:53:15'),
(8, 3, '1', 0, '2019-01-23 12:07:05', '2019-01-23 12:07:05'),
(9, 2, 'City 12', 0, '2019-01-23 12:07:55', '2019-01-23 12:07:55'),
(10, 99, 'Chandigarh', 0, '2019-01-23 12:07:55', '2019-01-23 12:07:55'),
(11, 99, 'Bangalore', 0, '2019-01-23 12:07:55', '2019-01-23 12:07:55'),
(12, 97, 'Test', 0, '2019-01-23 13:23:37', '2019-01-23 13:23:37'),
(13, 95, 'TT', 0, '2019-01-23 13:24:55', '2019-01-23 13:24:55'),
(14, 95, 'adwdawdawd', 0, '2019-01-23 13:25:39', '2019-01-23 13:25:39'),
(15, 95, 'awdawdawdawdw', 0, '2019-01-23 13:26:04', '2019-01-23 13:26:04'),
(16, 95, 'awdwdawd', 0, '2019-01-23 13:26:44', '2019-01-23 13:26:44'),
(17, 95, 'adawdwadawdawdawd', 0, '2019-01-23 13:27:50', '2019-01-23 13:27:50'),
(18, 95, '23233', 0, '2019-01-23 13:29:42', '2019-01-23 13:29:42'),
(19, 95, '23e3123131231312', 0, '2019-01-23 13:31:24', '2019-01-23 13:31:24');

-- --------------------------------------------------------

--
-- Table structure for table `countries`
--

CREATE TABLE `countries` (
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `code` varchar(2) COLLATE utf8_unicode_ci NOT NULL,
  `name` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `countries`
--

INSERT INTO `countries` (`country_id`, `code`, `name`, `created_at`, `updated_at`) VALUES
(1, 'AF', 'Afghanistan', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(2, 'AL', 'Albania', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(3, 'DZ', 'Algeria', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(4, 'DS', 'American Samoa', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(5, 'AD', 'Andorra', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(6, 'AO', 'Angola', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(7, 'AI', 'Anguilla', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(8, 'AQ', 'Antarctica', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(9, 'AG', 'Antigua and Barbuda', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(10, 'AR', 'Argentina', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(11, 'AM', 'Armenia', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(12, 'AW', 'Aruba', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(13, 'AU', 'Australia', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(14, 'AT', 'Austria', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(15, 'AZ', 'Azerbaijan', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(16, 'BS', 'Bahamas', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(17, 'BH', 'Bahrain', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(18, 'BD', 'Bangladesh', '2019-01-21 10:28:44', '2019-01-21 10:28:44'),
(19, 'BB', 'Barbados', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(20, 'BY', 'Belarus', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(21, 'BE', 'Belgium', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(22, 'BZ', 'Belize', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(23, 'BJ', 'Benin', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(24, 'BM', 'Bermuda', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(25, 'BT', 'Bhutan', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(26, 'BO', 'Bolivia', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(27, 'BA', 'Bosnia and Herzegovina', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(28, 'BW', 'Botswana', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(29, 'BV', 'Bouvet Island', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(30, 'BR', 'Brazil', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(31, 'IO', 'British Indian Ocean Territory', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(32, 'BN', 'Brunei Darussalam', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(33, 'BG', 'Bulgaria', '2019-01-21 10:28:45', '2019-01-21 10:28:45'),
(34, 'BF', 'Burkina Faso', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(35, 'BI', 'Burundi', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(36, 'KH', 'Cambodia', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(37, 'CM', 'Cameroon', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(38, 'CA', 'Canada', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(39, 'CV', 'Cape Verde', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(40, 'KY', 'Cayman Islands', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(41, 'CF', 'Central African Republic', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(42, 'TD', 'Chad', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(43, 'CL', 'Chile', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(44, 'CN', 'China', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(45, 'CX', 'Christmas Island', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(46, 'CC', 'Cocos (Keeling) Islands', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(47, 'CO', 'Colombia', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(48, 'KM', 'Comoros', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(49, 'CG', 'Congo', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(50, 'CK', 'Cook Islands', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(51, 'CR', 'Costa Rica', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(52, 'HR', 'Croatia (Hrvatska)', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(53, 'CU', 'Cuba', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(54, 'CY', 'Cyprus', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(55, 'CZ', 'Czech Republic', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(56, 'DK', 'Denmark', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(57, 'DJ', 'Djibouti', '2019-01-21 10:28:46', '2019-01-21 10:28:46'),
(58, 'DM', 'Dominica', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(59, 'DO', 'Dominican Republic', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(60, 'TP', 'East Timor', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(61, 'EC', 'Ecuador', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(62, 'EG', 'Egypt', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(63, 'SV', 'El Salvador', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(64, 'GQ', 'Equatorial Guinea', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(65, 'ER', 'Eritrea', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(66, 'EE', 'Estonia', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(67, 'ET', 'Ethiopia', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(68, 'FK', 'Falkland Islands (Malvinas)', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(69, 'FO', 'Faroe Islands', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(70, 'FJ', 'Fiji', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(71, 'FI', 'Finland', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(72, 'FR', 'France', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(73, 'FX', 'France, Metropolitan', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(74, 'GF', 'French Guiana', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(75, 'PF', 'French Polynesia', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(76, 'TF', 'French Southern Territories', '2019-01-21 10:28:47', '2019-01-21 10:28:47'),
(77, 'GA', 'Gabon', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(78, 'GM', 'Gambia', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(79, 'GE', 'Georgia', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(80, 'DE', 'Germany', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(81, 'GH', 'Ghana', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(82, 'GI', 'Gibraltar', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(83, 'GK', 'Guernsey', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(84, 'GR', 'Greece', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(85, 'GL', 'Greenland', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(86, 'GD', 'Grenada', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(87, 'GP', 'Guadeloupe', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(88, 'GU', 'Guam', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(89, 'GT', 'Guatemala', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(90, 'GN', 'Guinea', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(91, 'GW', 'Guinea-Bissau', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(92, 'GY', 'Guyana', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(93, 'HT', 'Haiti', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(94, 'HM', 'Heard and Mc Donald Islands', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(95, 'HN', 'Honduras', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(96, 'HK', 'Hong Kong', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(97, 'HU', 'Hungary', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(98, 'IS', 'Iceland', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(99, 'IN', 'India', '2019-01-21 10:28:48', '2019-01-21 10:28:48'),
(100, 'IM', 'Isle of Man', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(101, 'ID', 'Indonesia', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(102, 'IR', 'Iran (Islamic Republic of)', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(103, 'IQ', 'Iraq', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(104, 'IE', 'Ireland', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(105, 'IL', 'Israel', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(106, 'IT', 'Italy', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(107, 'CI', 'Ivory Coast', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(108, 'JE', 'Jersey', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(109, 'JM', 'Jamaica', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(110, 'JP', 'Japan', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(111, 'JO', 'Jordan', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(112, 'KZ', 'Kazakhstan', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(113, 'KE', 'Kenya', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(114, 'KI', 'Kiribati', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(115, 'KP', 'Korea, Democratic People\'s Republic of', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(116, 'KR', 'Korea, Republic of', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(117, 'XK', 'Kosovo', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(118, 'KW', 'Kuwait', '2019-01-21 10:28:49', '2019-01-21 10:28:49'),
(119, 'KG', 'Kyrgyzstan', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(120, 'LA', 'Lao People\'s Democratic Republic', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(121, 'LV', 'Latvia', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(122, 'LB', 'Lebanon', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(123, 'LS', 'Lesotho', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(124, 'LR', 'Liberia', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(125, 'LY', 'Libyan Arab Jamahiriya', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(126, 'LI', 'Liechtenstein', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(127, 'LT', 'Lithuania', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(128, 'LU', 'Luxembourg', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(129, 'MO', 'Macau', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(130, 'MK', 'Macedonia', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(131, 'MG', 'Madagascar', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(132, 'MW', 'Malawi', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(133, 'MY', 'Malaysia', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(134, 'MV', 'Maldives', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(135, 'ML', 'Mali', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(136, 'MT', 'Malta', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(137, 'MH', 'Marshall Islands', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(138, 'MQ', 'Martinique', '2019-01-21 10:28:50', '2019-01-21 10:28:50'),
(139, 'MR', 'Mauritania', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(140, 'MU', 'Mauritius', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(141, 'TY', 'Mayotte', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(142, 'MX', 'Mexico', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(143, 'FM', 'Micronesia, Federated States of', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(144, 'MD', 'Moldova, Republic of', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(145, 'MC', 'Monaco', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(146, 'MN', 'Mongolia', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(147, 'ME', 'Montenegro', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(148, 'MS', 'Montserrat', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(149, 'MA', 'Morocco', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(150, 'MZ', 'Mozambique', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(151, 'MM', 'Myanmar', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(152, 'NA', 'Namibia', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(153, 'NR', 'Nauru', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(154, 'NP', 'Nepal', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(155, 'NL', 'Netherlands', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(156, 'AN', 'Netherlands Antilles', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(157, 'NC', 'New Caledonia', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(158, 'NZ', 'New Zealand', '2019-01-21 10:28:51', '2019-01-21 10:28:51'),
(159, 'NI', 'Nicaragua', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(160, 'NE', 'Niger', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(161, 'NG', 'Nigeria', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(162, 'NU', 'Niue', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(163, 'NF', 'Norfolk Island', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(164, 'MP', 'Northern Mariana Islands', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(165, 'NO', 'Norway', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(166, 'OM', 'Oman', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(167, 'PK', 'Pakistan', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(168, 'PW', 'Palau', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(169, 'PS', 'Palestine', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(170, 'PA', 'Panama', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(171, 'PG', 'Papua New Guinea', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(172, 'PY', 'Paraguay', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(173, 'PE', 'Peru', '2019-01-21 10:28:52', '2019-01-21 10:28:52'),
(174, 'PH', 'Philippines', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(175, 'PN', 'Pitcairn', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(176, 'PL', 'Poland', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(177, 'PT', 'Portugal', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(178, 'PR', 'Puerto Rico', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(179, 'QA', 'Qatar', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(180, 'RE', 'Reunion', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(181, 'RO', 'Romania', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(182, 'RU', 'Russian Federation', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(183, 'RW', 'Rwanda', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(184, 'KN', 'Saint Kitts and Nevis', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(185, 'LC', 'Saint Lucia', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(186, 'VC', 'Saint Vincent and the Grenadines', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(187, 'WS', 'Samoa', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(188, 'SM', 'San Marino', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(189, 'ST', 'Sao Tome and Principe', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(190, 'SA', 'Saudi Arabia', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(191, 'SN', 'Senegal', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(192, 'RS', 'Serbia', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(193, 'SC', 'Seychelles', '2019-01-21 10:28:53', '2019-01-21 10:28:53'),
(194, 'SL', 'Sierra Leone', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(195, 'SG', 'Singapore', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(196, 'SK', 'Slovakia', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(197, 'SI', 'Slovenia', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(198, 'SB', 'Solomon Islands', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(199, 'SO', 'Somalia', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(200, 'ZA', 'South Africa', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(201, 'GS', 'South Georgia South Sandwich Islands', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(202, 'SS', 'South Sudan', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(203, 'ES', 'Spain', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(204, 'LK', 'Sri Lanka', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(205, 'SH', 'St. Helena', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(206, 'PM', 'St. Pierre and Miquelon', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(207, 'SD', 'Sudan', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(208, 'SR', 'Suriname', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(209, 'SJ', 'Svalbard and Jan Mayen Islands', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(210, 'SZ', 'Swaziland', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(211, 'SE', 'Sweden', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(212, 'CH', 'Switzerland', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(213, 'SY', 'Syrian Arab Republic', '2019-01-21 10:28:54', '2019-01-21 10:28:54'),
(214, 'TW', 'Taiwan', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(215, 'TJ', 'Tajikistan', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(216, 'TZ', 'Tanzania, United Republic of', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(217, 'TH', 'Thailand', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(218, 'TG', 'Togo', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(219, 'TK', 'Tokelau', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(220, 'TO', 'Tonga', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(221, 'TT', 'Trinidad and Tobago', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(222, 'TN', 'Tunisia', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(223, 'TR', 'Turkey', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(224, 'TM', 'Turkmenistan', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(225, 'TC', 'Turks and Caicos Islands', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(226, 'TV', 'Tuvalu', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(227, 'UG', 'Uganda', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(228, 'UA', 'Ukraine', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(229, 'AE', 'United Arab Emirates', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(230, 'GB', 'United Kingdom', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(231, 'US', 'United States', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(232, 'UM', 'United States minor outlying islands', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(233, 'UY', 'Uruguay', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(234, 'UZ', 'Uzbekistan', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(235, 'VU', 'Vanuatu', '2019-01-21 10:28:55', '2019-01-21 10:28:55'),
(236, 'VA', 'Vatican City State', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(237, 'VE', 'Venezuela', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(238, 'VN', 'Vietnam', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(239, 'VG', 'Virgin Islands (British)', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(240, 'VI', 'Virgin Islands (U.S.)', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(241, 'WF', 'Wallis and Futuna Islands', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(242, 'EH', 'Western Sahara', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(243, 'YE', 'Yemen', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(244, 'ZR', 'Zaire', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(245, 'ZM', 'Zambia', '2019-01-21 10:28:56', '2019-01-21 10:28:56'),
(246, 'ZW', 'Zimbabwe', '2019-01-21 10:28:56', '2019-01-21 10:28:56');

-- --------------------------------------------------------

--
-- Table structure for table `founders`
--

CREATE TABLE `founders` (
  `founder_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `founders`
--

INSERT INTO `founders` (`founder_id`, `name`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 'Found 1', 0, '2019-01-18 15:13:01', '2019-01-29 06:37:35'),
(2, 'Found 2', 0, '2019-01-18 15:13:01', '2019-01-18 15:13:01'),
(3, 'Found 3333', 0, '2019-01-18 15:13:01', '2019-01-19 06:55:54'),
(4, 'Found 4', 0, '2019-01-18 15:13:01', '2019-01-18 15:13:01'),
(6, 'Founder 4', 0, '2019-01-19 05:14:17', '2019-01-19 05:14:17'),
(7, '50000', 0, '2019-01-19 06:07:04', '2019-01-19 06:57:04'),
(8, '5', 0, '2019-01-19 06:07:05', '2019-01-19 06:07:05'),
(9, 'awdwadd', 0, '2019-01-19 06:08:11', '2019-01-24 08:15:01'),
(10, 'awdawdd1', 0, '2019-01-19 06:09:04', '2019-01-24 05:57:31'),
(11, 'wdawdadwd', 0, '2019-01-19 06:09:50', '2019-01-19 06:09:50'),
(12, 'waddawdwad', 0, '2019-01-19 06:11:58', '2019-01-19 06:11:58'),
(13, 'awdwadawdwd', 0, '2019-01-19 06:12:02', '2019-01-19 06:12:02'),
(14, 'awdad1', 0, '2019-01-19 06:12:05', '2019-01-19 06:12:05'),
(15, '1111111111111111', 0, '2019-01-19 06:12:08', '2019-01-29 05:41:02'),
(16, 'Test', 0, '2019-01-24 08:14:42', '2019-01-24 08:14:53'),
(17, 'Test', 0, '2019-01-29 04:50:56', '2019-01-29 04:50:56'),
(18, 'Test', 0, '2019-01-29 04:51:01', '2019-01-29 04:51:01'),
(19, 'Testz', 0, '2019-01-29 11:09:25', '2019-01-29 11:09:25'),
(20, 'ZZ', 0, '2019-01-29 11:11:29', '2019-01-29 11:11:29'),
(22, 'SSSSSS', 0, '2019-01-29 12:16:44', '2019-01-29 12:16:44');

-- --------------------------------------------------------

--
-- Table structure for table `industries`
--

CREATE TABLE `industries` (
  `industry_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `industries`
--

INSERT INTO `industries` (`industry_id`, `name`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, '1', 0, '2019-01-15 08:55:09', '2019-01-15 08:55:09'),
(2, '2', 0, '2019-01-15 08:55:09', '2019-01-15 08:55:09'),
(3, 'Industry 1', 0, '2019-01-23 14:11:46', '2019-01-23 14:11:46');

-- --------------------------------------------------------

--
-- Table structure for table `investors`
--

CREATE TABLE `investors` (
  `investor_id` bigint(20) UNSIGNED NOT NULL,
  `investor_type_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `investors`
--

INSERT INTO `investors` (`investor_id`, `investor_type_id`, `name`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 1, 'Invest 1', 0, '2019-01-19 12:35:09', '2019-01-24 09:30:26'),
(2, 1, 'Invest 2', 0, '2019-01-19 12:35:09', '2019-01-24 11:47:52'),
(3, 1, 'Invest 31', 0, '2019-01-19 12:35:09', '2019-01-24 11:47:52'),
(4, 4, 'Invest 332', 0, '2019-01-19 07:13:48', '2019-01-24 08:02:34'),
(5, 1, 'hbvjvhjvjh', 0, '2019-01-19 07:14:12', '2019-01-24 11:47:52'),
(6, 1, 'Invest 334', 0, '2019-01-19 07:14:58', '2019-01-24 11:47:52'),
(7, 5, 'TT1', 0, '2019-01-23 08:45:17', '2019-01-24 09:31:00'),
(8, 2, 'Invest 1', 0, '2019-01-24 13:29:11', '2019-01-24 08:01:25'),
(9, 4, 'Invest 300007', 0, '2019-01-24 13:32:57', '2019-01-24 08:03:09');

-- --------------------------------------------------------

--
-- Table structure for table `investor_types`
--

CREATE TABLE `investor_types` (
  `investor_type_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `investor_types`
--

INSERT INTO `investor_types` (`investor_type_id`, `name`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 'Type 1', 0, '2019-01-24 11:48:05', '2019-01-24 09:30:26'),
(2, 'Type 2', 0, '2019-01-24 11:48:05', '2019-01-24 08:02:23'),
(4, 'Type 3', 0, '2019-01-24 13:32:34', '2019-01-24 08:03:09'),
(5, 'TT2', 0, '2019-01-24 15:01:00', '2019-01-24 15:01:00');

-- --------------------------------------------------------

--
-- Table structure for table `jobs`
--

CREATE TABLE `jobs` (
  `id` bigint(20) UNSIGNED NOT NULL,
  `queue` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `payload` longtext COLLATE utf8mb4_unicode_ci NOT NULL,
  `attempts` tinyint(3) UNSIGNED NOT NULL,
  `reserved_at` int(10) UNSIGNED DEFAULT NULL,
  `available_at` int(10) UNSIGNED NOT NULL,
  `created_at` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

-- --------------------------------------------------------

--
-- Table structure for table `migrations`
--

CREATE TABLE `migrations` (
  `id` int(10) UNSIGNED NOT NULL,
  `migration` varchar(255) COLLATE utf8mb4_unicode_ci NOT NULL,
  `batch` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_unicode_ci;

--
-- Dumping data for table `migrations`
--

INSERT INTO `migrations` (`id`, `migration`, `batch`) VALUES
(1, '2019_01_14_101913_create_cities_table', 0),
(2, '2019_01_14_101913_create_countries_table', 0),
(3, '2019_01_14_101913_create_founders_table', 0),
(4, '2019_01_14_101913_create_industries_table', 0),
(5, '2019_01_14_101913_create_investor_types_table', 0),
(6, '2019_01_14_101913_create_investors_table', 0),
(7, '2019_01_14_101913_create_rounds_table', 0),
(8, '2019_01_14_101913_create_startup_countries_table', 0),
(9, '2019_01_14_101913_create_startup_founders_table', 0),
(10, '2019_01_14_101913_create_startup_inverstors_table', 0),
(11, '2019_01_14_101913_create_startup_news_table', 0),
(12, '2019_01_14_101913_create_startup_rounds_table', 0),
(13, '2019_01_14_101913_create_startups_table', 0),
(14, '2019_01_14_101915_add_foreign_keys_to_cities_table', 0),
(15, '2019_01_14_101915_add_foreign_keys_to_startup_countries_table', 0),
(16, '2019_01_14_101915_add_foreign_keys_to_startup_founders_table', 0),
(17, '2019_01_14_101915_add_foreign_keys_to_startup_inverstors_table', 0),
(18, '2019_01_14_101915_add_foreign_keys_to_startup_news_table', 0),
(19, '2019_01_14_101915_add_foreign_keys_to_startup_rounds_table', 0),
(20, '2019_01_14_101915_add_foreign_keys_to_startups_table', 0),
(21, '2019_01_30_075728_create_jobs_table', 1);

-- --------------------------------------------------------

--
-- Table structure for table `rounds`
--

CREATE TABLE `rounds` (
  `round_id` bigint(20) UNSIGNED NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `rounds`
--

INSERT INTO `rounds` (`round_id`, `name`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 'Round 1', 0, '2019-01-24 13:49:19', '2019-01-28 06:06:42'),
(2, 'Round 2', 0, '2019-01-28 11:13:38', '2019-01-28 06:05:41');

-- --------------------------------------------------------

--
-- Table structure for table `startups`
--

CREATE TABLE `startups` (
  `startup_id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `city_id` bigint(20) UNSIGNED NOT NULL,
  `industry_id` bigint(20) NOT NULL,
  `name` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `logo` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `founded` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `website` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `facebook` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `twitter` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `linkedin` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `instagram` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `valuation` varchar(100) COLLATE utf8_unicode_ci DEFAULT NULL,
  `ipo_symbol` varchar(100) COLLATE utf8_unicode_ci NOT NULL,
  `is_public` tinyint(1) NOT NULL DEFAULT '1',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `startups`
--

INSERT INTO `startups` (`startup_id`, `country_id`, `city_id`, `industry_id`, `name`, `logo`, `description`, `founded`, `website`, `facebook`, `twitter`, `linkedin`, `instagram`, `valuation`, `ipo_symbol`, `is_public`, `created_at`, `updated_at`) VALUES
(2, 95, 19, 2, 'Copm1', '', 'awdawdw1', '02 2011', 'web', 'Fb', 'adwdtwitter', 'linkedin', 'insta`1', 'wdwadadwadd', '', 1, '2019-01-15 08:55:37', '2019-01-28 13:18:16'),
(3, 1, 2, 3, 'wdwdad21', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/86f7b63ec108af2676d1a8e26f745cb85256139fwB47NPt5Eli2zl9k2fGMVT8OS.jpg', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 0, '2019-01-15 08:55:37', '2019-01-28 08:43:33'),
(4, 1, 2, 2, '2', '1', '1', '01 Jan 2019', NULL, NULL, NULL, NULL, NULL, 'adawdawddwadd', '', 1, '2019-01-15 08:55:37', '2019-01-28 08:49:35'),
(5, 1, 2, 3, '21111', '1', '1', '01 Jan 2019', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-28 08:48:03'),
(6, 1, 2, 3, '1awdawdawdjnbjknk2', '1', '1', '08 Jan 2019', NULL, NULL, NULL, NULL, 'cacw dwadwd', NULL, '', 0, '2019-01-15 08:55:37', '2019-01-28 08:49:10'),
(7, 1, 5, 1, 'adawdadwwdwdw12332312', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, 'awdawd', '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(8, 1, 6, 1, '1', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(9, 1, 7, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, 'wdwda dwdawdw dawdawdawd', '1111111', '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(10, 1, 1, 1, '2', '1', '1', '07 Mar 2019', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 06:35:24'),
(11, 1, 1, 1, '2', '1', '1', '11 Apr 2019', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 06:30:19'),
(12, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(13, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(14, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(15, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(16, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(17, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(18, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(19, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(20, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(21, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(22, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(23, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(24, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(25, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(26, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(27, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(28, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(29, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(30, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(31, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(32, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(33, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(34, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(35, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(36, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(37, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(38, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(39, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(40, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(41, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(42, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(43, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(44, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(45, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(46, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(47, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(48, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19'),
(49, 1, 1, 1, '2', '1', '1', '02 2011', NULL, NULL, NULL, NULL, NULL, NULL, '', 1, '2019-01-15 08:55:37', '2019-01-23 11:39:19');

-- --------------------------------------------------------

--
-- Table structure for table `startup_countries`
--

CREATE TABLE `startup_countries` (
  `startup_country_id` bigint(20) UNSIGNED NOT NULL,
  `startup_id` bigint(20) UNSIGNED NOT NULL,
  `country_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `startup_countries`
--

INSERT INTO `startup_countries` (`startup_country_id`, `startup_id`, `country_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2019-01-21 13:13:25', '2019-01-21 13:13:25'),
(2, 2, 2, '2019-01-21 13:13:31', '2019-01-21 13:13:31'),
(12, 7, 2, '2019-01-21 14:12:20', '2019-01-21 14:12:20'),
(15, 4, 99, '2019-01-21 14:14:03', '2019-01-21 14:14:03'),
(16, 3, 1, '2019-01-21 14:20:00', '2019-01-21 14:20:00');

-- --------------------------------------------------------

--
-- Table structure for table `startup_founders`
--

CREATE TABLE `startup_founders` (
  `startup_founder_id` bigint(20) UNSIGNED NOT NULL,
  `startup_id` bigint(20) UNSIGNED NOT NULL,
  `founder_id` bigint(20) UNSIGNED NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `startup_founders`
--

INSERT INTO `startup_founders` (`startup_founder_id`, `startup_id`, `founder_id`, `created_at`, `updated_at`) VALUES
(1, 2, 1, '2019-01-18 15:13:13', '2019-01-18 15:13:13'),
(2, 2, 2, '2019-01-18 15:13:13', '2019-01-18 15:13:13'),
(3, 2, 3, '2019-01-18 15:13:13', '2019-01-18 15:13:13'),
(4, 2, 6, '2019-01-19 05:14:17', '2019-01-19 05:14:17'),
(5, 2, 7, '2019-01-19 06:07:05', '2019-01-19 06:07:05'),
(6, 2, 8, '2019-01-19 06:07:05', '2019-01-19 06:07:05'),
(7, 4, 9, '2019-01-19 06:08:11', '2019-01-19 06:08:11'),
(8, 5, 10, '2019-01-19 06:09:04', '2019-01-19 06:09:04'),
(9, 6, 11, '2019-01-19 06:09:51', '2019-01-19 06:09:51'),
(10, 8, 12, '2019-01-19 06:11:58', '2019-01-19 06:11:58'),
(11, 9, 13, '2019-01-19 06:12:02', '2019-01-19 06:12:02'),
(12, 9, 14, '2019-01-19 06:12:05', '2019-01-19 06:12:05'),
(13, 9, 15, '2019-01-19 06:12:08', '2019-01-19 06:12:08'),
(14, 3, 16, '2019-01-24 08:14:42', '2019-01-24 08:14:42'),
(15, 7, 15, '2019-01-29 04:50:56', '2019-01-29 05:41:03'),
(16, 7, 20, '2019-01-29 04:51:01', '2019-01-29 05:41:29'),
(19, 7, 1, '2019-01-29 12:09:51', '2019-01-29 12:09:51'),
(20, 8, 1, '2019-01-29 12:10:14', '2019-01-29 12:10:14'),
(22, 7, 22, '2019-01-29 12:16:44', '2019-01-29 12:16:44');

-- --------------------------------------------------------

--
-- Table structure for table `startup_investors`
--

CREATE TABLE `startup_investors` (
  `startup_investor_id` bigint(20) UNSIGNED NOT NULL,
  `startup_id` bigint(20) UNSIGNED NOT NULL,
  `investor_id` bigint(20) UNSIGNED NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `startup_investors`
--

INSERT INTO `startup_investors` (`startup_investor_id`, `startup_id`, `investor_id`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 3, 9, 0, '2019-01-19 12:35:41', '2019-01-24 08:03:09'),
(2, 3, 8, 0, '2019-01-19 12:35:41', '2019-01-24 08:01:25'),
(3, 2, 6, 0, '2019-01-19 07:14:58', '2019-01-19 07:14:58'),
(4, 2, 7, 0, '2019-01-23 08:45:17', '2019-01-23 08:45:17'),
(5, 3, 1, 0, '2019-01-24 09:30:02', '2019-01-24 09:30:02'),
(6, 3, 1, 0, '2019-01-24 09:30:26', '2019-01-24 09:30:26'),
(7, 3, 7, 0, '2019-01-24 09:31:01', '2019-01-24 09:31:01');

-- --------------------------------------------------------

--
-- Table structure for table `startup_news`
--

CREATE TABLE `startup_news` (
  `startup_news_id` bigint(20) UNSIGNED NOT NULL,
  `startup_id` bigint(20) UNSIGNED NOT NULL,
  `url` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `image` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `description` text COLLATE utf8_unicode_ci,
  `source` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `is_blocked` tinyint(1) NOT NULL DEFAULT '0',
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `startup_news`
--

INSERT INTO `startup_news` (`startup_news_id`, `startup_id`, `url`, `image`, `description`, `source`, `is_blocked`, `created_at`, `updated_at`) VALUES
(1, 3, 'Test', 'Test', 'Test', 'Test', 0, '2019-01-28 11:47:51', '2019-01-28 11:47:51'),
(2, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/8d5e9c609b23ed694139baccb96d35071d8e55adSWeYClCQm7MBloY0d5Qvh8Bot.jpg', 'Test', 'Test', 0, '2019-01-28 12:51:50', '2019-01-28 12:51:50'),
(3, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/af7d0aa7b7e98aa3968bc8e502137300f37884f2mtOIbm1OpzPl663dVbUitkEJm.jpg', 'Test', 'Test', 0, '2019-01-28 12:53:19', '2019-01-28 12:53:19'),
(4, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/66a386cb1d4ba4f069077df149c161834969c866i9t7GjkRkaJzJYAktdQpwNH3y.jpg', 'Test', 'Test', 0, '2019-01-28 12:54:07', '2019-01-28 12:54:07'),
(5, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/0826d6e0f801ac81a7de07ff27178ef5d3521cca6ej3acQocYaS9sNl51WifYSsk.jpg', 'dwdwdw', 'dadawd', 0, '2019-01-28 12:55:53', '2019-01-28 12:55:53'),
(6, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/b921026c11a856b5e00c1f2f13ba626d56a01915lMFWY2rXCActnCaquocxUYyV1.jpg', 'Test', 'Test', 0, '2019-01-28 12:57:08', '2019-01-28 12:57:08'),
(7, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/cba065646353e570573a7cd6de8dc83c5af05834RoBIiGm2E9t8yLwlu7lkmImhE.jpg', 'awdawd', 'dawdawd', 0, '2019-01-28 12:57:57', '2019-01-28 12:57:57'),
(8, 3, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/635558b882cab5f054a289e9bec4e12180abf7b9l59NgiV6v50WvkPidVSjWjSWf.jpg', 'awdawd', 'dawdawd', 0, '2019-01-28 12:58:04', '2019-01-28 12:58:04'),
(9, 4, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/5685fd4340f13a821f91d2481d875a0a6c80ed4fVFquIdOgcBSt9YhRUVMjF3TA7.jpg', 'awdawd', 'dawdawd', 0, '2019-01-28 12:58:15', '2019-01-28 12:58:15'),
(10, 5, 'http://google.com', 'http://localhost/Laptop_Laravel/Ongoing/Contxto/public/Uploads/a9623093e121cdd963079b1edae103f6918a7e1bCjucVIMIujakvW8VQwq5awTWd.jpg', 'wdawd', 'wddadw', 0, '2019-01-28 13:01:11', '2019-01-28 13:01:11');

-- --------------------------------------------------------

--
-- Table structure for table `startup_rounds`
--

CREATE TABLE `startup_rounds` (
  `startup_round_id` bigint(20) UNSIGNED NOT NULL,
  `round_id` bigint(20) UNSIGNED NOT NULL,
  `investor_id` bigint(20) UNSIGNED NOT NULL,
  `startup_id` bigint(20) UNSIGNED NOT NULL,
  `amount` varchar(50) COLLATE utf8_unicode_ci NOT NULL,
  `dt` date NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `startup_rounds`
--

INSERT INTO `startup_rounds` (`startup_round_id`, `round_id`, `investor_id`, `startup_id`, `amount`, `dt`, `created_at`, `updated_at`) VALUES
(1, 1, 2, 3, '$4 Billions', '2019-01-18', '2019-01-24 13:51:56', '2019-01-28 05:44:05'),
(3, 2, 2, 3, '2 Trillion Paissa', '2019-01-31', '2019-01-28 11:35:21', '2019-01-28 11:35:21'),
(4, 2, 2, 3, '2 Trillion Paissa', '2019-01-31', '2019-01-28 11:35:41', '2019-01-28 11:35:41'),
(5, 1, 1, 4, '10 Trillion Aana', '2019-01-23', '2019-01-28 11:36:42', '2019-01-28 11:36:42');

-- --------------------------------------------------------

--
-- Table structure for table `sync_logs`
--

CREATE TABLE `sync_logs` (
  `sync_log_id` bigint(20) NOT NULL,
  `website` varchar(255) COLLATE utf8_unicode_ci NOT NULL,
  `type` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `result` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `error` varchar(20) COLLATE utf8_unicode_ci NOT NULL,
  `details` longtext COLLATE utf8_unicode_ci NOT NULL,
  `created_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `updated_at` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP ON UPDATE CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Indexes for dumped tables
--

--
-- Indexes for table `admins`
--
ALTER TABLE `admins`
  ADD PRIMARY KEY (`admin_id`),
  ADD KEY `email` (`email`);

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`city_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`country_id`);

--
-- Indexes for table `founders`
--
ALTER TABLE `founders`
  ADD PRIMARY KEY (`founder_id`);

--
-- Indexes for table `industries`
--
ALTER TABLE `industries`
  ADD PRIMARY KEY (`industry_id`);

--
-- Indexes for table `investors`
--
ALTER TABLE `investors`
  ADD PRIMARY KEY (`investor_id`),
  ADD KEY `investor_type_id` (`investor_type_id`);

--
-- Indexes for table `investor_types`
--
ALTER TABLE `investor_types`
  ADD PRIMARY KEY (`investor_type_id`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`id`),
  ADD KEY `jobs_queue_index` (`queue`);

--
-- Indexes for table `migrations`
--
ALTER TABLE `migrations`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `rounds`
--
ALTER TABLE `rounds`
  ADD PRIMARY KEY (`round_id`);

--
-- Indexes for table `startups`
--
ALTER TABLE `startups`
  ADD PRIMARY KEY (`startup_id`),
  ADD KEY `city_id` (`city_id`),
  ADD KEY `country_id` (`country_id`),
  ADD KEY `industry_id` (`industry_id`);

--
-- Indexes for table `startup_countries`
--
ALTER TABLE `startup_countries`
  ADD PRIMARY KEY (`startup_country_id`),
  ADD KEY `startup_id` (`startup_id`),
  ADD KEY `country_id` (`country_id`);

--
-- Indexes for table `startup_founders`
--
ALTER TABLE `startup_founders`
  ADD PRIMARY KEY (`startup_founder_id`),
  ADD KEY `startup_id` (`startup_id`),
  ADD KEY `founder_id` (`founder_id`);

--
-- Indexes for table `startup_investors`
--
ALTER TABLE `startup_investors`
  ADD PRIMARY KEY (`startup_investor_id`),
  ADD KEY `startup_id` (`startup_id`),
  ADD KEY `investor_id` (`investor_id`);

--
-- Indexes for table `startup_news`
--
ALTER TABLE `startup_news`
  ADD PRIMARY KEY (`startup_news_id`),
  ADD KEY `startup_id` (`startup_id`);

--
-- Indexes for table `startup_rounds`
--
ALTER TABLE `startup_rounds`
  ADD PRIMARY KEY (`startup_round_id`),
  ADD KEY `round_id` (`round_id`),
  ADD KEY `investor_id` (`investor_id`),
  ADD KEY `startup_id` (`startup_id`);

--
-- Indexes for table `sync_logs`
--
ALTER TABLE `sync_logs`
  ADD PRIMARY KEY (`sync_log_id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `admins`
--
ALTER TABLE `admins`
  MODIFY `admin_id` bigint(20) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;
--
-- AUTO_INCREMENT for table `cities`
--
ALTER TABLE `cities`
  MODIFY `city_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;
--
-- AUTO_INCREMENT for table `countries`
--
ALTER TABLE `countries`
  MODIFY `country_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=247;
--
-- AUTO_INCREMENT for table `founders`
--
ALTER TABLE `founders`
  MODIFY `founder_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `industries`
--
ALTER TABLE `industries`
  MODIFY `industry_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `investors`
--
ALTER TABLE `investors`
  MODIFY `investor_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;
--
-- AUTO_INCREMENT for table `investor_types`
--
ALTER TABLE `investor_types`
  MODIFY `investor_type_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `jobs`
--
ALTER TABLE `jobs`
  MODIFY `id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `migrations`
--
ALTER TABLE `migrations`
  MODIFY `id` int(10) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=22;
--
-- AUTO_INCREMENT for table `rounds`
--
ALTER TABLE `rounds`
  MODIFY `round_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;
--
-- AUTO_INCREMENT for table `startups`
--
ALTER TABLE `startups`
  MODIFY `startup_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=50;
--
-- AUTO_INCREMENT for table `startup_countries`
--
ALTER TABLE `startup_countries`
  MODIFY `startup_country_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=17;
--
-- AUTO_INCREMENT for table `startup_founders`
--
ALTER TABLE `startup_founders`
  MODIFY `startup_founder_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=23;
--
-- AUTO_INCREMENT for table `startup_investors`
--
ALTER TABLE `startup_investors`
  MODIFY `startup_investor_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;
--
-- AUTO_INCREMENT for table `startup_news`
--
ALTER TABLE `startup_news`
  MODIFY `startup_news_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;
--
-- AUTO_INCREMENT for table `startup_rounds`
--
ALTER TABLE `startup_rounds`
  MODIFY `startup_round_id` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=6;
--
-- AUTO_INCREMENT for table `sync_logs`
--
ALTER TABLE `sync_logs`
  MODIFY `sync_log_id` bigint(20) NOT NULL AUTO_INCREMENT;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `startup_countries`
--
ALTER TABLE `startup_countries`
  ADD CONSTRAINT `startup_countries_ibfk_1` FOREIGN KEY (`startup_id`) REFERENCES `startups` (`startup_id`);

--
-- Constraints for table `startup_founders`
--
ALTER TABLE `startup_founders`
  ADD CONSTRAINT `startup_founders_ibfk_1` FOREIGN KEY (`startup_id`) REFERENCES `startups` (`startup_id`),
  ADD CONSTRAINT `startup_founders_ibfk_2` FOREIGN KEY (`founder_id`) REFERENCES `founders` (`founder_id`);

--
-- Constraints for table `startup_investors`
--
ALTER TABLE `startup_investors`
  ADD CONSTRAINT `startup_investors_ibfk_1` FOREIGN KEY (`startup_id`) REFERENCES `startups` (`startup_id`),
  ADD CONSTRAINT `startup_investors_ibfk_2` FOREIGN KEY (`investor_id`) REFERENCES `investors` (`investor_id`);

--
-- Constraints for table `startup_news`
--
ALTER TABLE `startup_news`
  ADD CONSTRAINT `startup_news_ibfk_1` FOREIGN KEY (`startup_id`) REFERENCES `startups` (`startup_id`);

--
-- Constraints for table `startup_rounds`
--
ALTER TABLE `startup_rounds`
  ADD CONSTRAINT `startup_rounds_ibfk_1` FOREIGN KEY (`round_id`) REFERENCES `rounds` (`round_id`),
  ADD CONSTRAINT `startup_rounds_ibfk_2` FOREIGN KEY (`investor_id`) REFERENCES `investors` (`investor_id`),
  ADD CONSTRAINT `startup_rounds_ibfk_3` FOREIGN KEY (`startup_id`) REFERENCES `startups` (`startup_id`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;

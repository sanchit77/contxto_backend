<div class="col-lg-8 col-md-7 order-md-1 col-12">
  <div class="custom-tabs">
     <ul class="nav nav-tabs">
        <li class="nav-item">
           <a class="nav-link{{ (\Request::route()->getName() == 'web-home' || \Request::route()->getName() == 'web-home-industries') ? ' active' : ''}}" href="{{route('web-home')}}">COMPANIES</a>
        </li>
        <li class="nav-item">
           <a class="nav-link{{ (\Request::route()->getName() == 'web-investors') ? ' active' : ''}}" href="{{route('web-investors')}}">INVESTORS</a>
        </li>
        <li class="nav-item">
           <a class="nav-link{{ (\Request::route()->getName() == 'web-founders') ? ' active' : ''}}" href="{{route('web-founders')}}">FOUNDERS</a>
        </li>
     </ul>
  </div>
</div>

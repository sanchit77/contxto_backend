<nav class="navbar navbar-expand">
  <ul class="navbar-nav mt-3">
    <li class="nav-item{{ (\Request::route()->getName() == 'web-company-details') ? ' active' : ''}}">
      <a class="nav-link" href="{{route('web-home')}}"> COMPANIES</a>
    </li>               
    <li class="nav-item{{ (\Request::route()->getName() == 'web-investors') ? ' active' : ''}}">
      <a class="nav-link" href="{{route('web-investors')}}">INVESTORS</a>
    </li>               
    <li class="nav-item{{ (\Request::route()->getName() == 'web-founders') ? ' active' : ''}}">
      <a class="nav-link" href="{{route('web-founders')}}">FOUNDERS</a>
    </li>
  </ul>
</nav>
@forelse($investors as $investor)
    <div class="profileCard">
	  <div class="profilePic mr-3">
	     <!-- <img src="{{ asset('web/img/120*120.png')}}" alt="img"> -->
	     <a href="#" class="firstLetter">{{strtoupper(substr($investor->name, 0, 1))}}</a>
	  </div>
	  <div class="profileInfo">
	    <a href="{{ route('web-investor-details',['investor_name' => $investor->slug ])}}" class="name">{{ucfirst($investor->name)}} </a>
	     <span class="compName">{{$investor->startup_inverstors_count.($investor->startup_inverstors_count>1 ? ' Companies' : ' Company')   }}</span>
	  </div>
	</div>
@empty
<div class="noResFound text-center">
  <img class="img-fluid" src="{{ asset('web/img/no_result_found.svg')}}" alt="img">

  <h4>No result found!</h4>
<!-- <p>Don’t worry you can submit your startup as well. </br>
Just click <a harf="#" class="createCompBtn"> Add your startup</a></p> -->
</div>
@endforelse
<div class="customPagination">
    {{ $investors->appends(['keyword' => $keyword,'country_id' => $country_id,'city_id' => $city_id,'industries' => $industries_str])->links() }}
</div>

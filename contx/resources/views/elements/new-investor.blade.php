<div class="modal" id="createInvestor">
   <div class="modal-dialog" style="max-width:960px">
      <div class="modal-content">
         <div class="modal-header p-0">
            <button type="button" class="close" data-dismiss="modal">
            <img src="{{asset('web/img/close_popup.png')}}" alt="img">
            </button>
         </div>
         <div class="modal-body">
            <section class="formMain mt-4">
               <div class="container">
                  <div class="form-header">
                     <h4>Investor Personal Information</h4>
                     <p>Add your information below.</p>
                  </div>
                  <form action="#" id="new-investor" method="POST" enctype="multipart/form-data">
                     {{csrf_field()}}
                     <input type="hidden" name="startupid" value="{{@$company->startup_id}}">
                  <div class="uploadSection" >
                     <div class="inputFiles">
                      <img id="company-logo" src="#" />
                     </div>
                     <div class="inputFilesActions">
                        <p>Add Your Profile Image</p>
                        <div class="row">
                           <div class="col-md-6 col-7">
                              <div class="uploadImg">
                                 <input type='file' name="investor-image" id="company-logo-input"  accept="image/*" onchange="readURL(this);" />
                                 <span>Upload Image</span>
                              </div>
                           </div>
                           <div class="col-md-6 col-5">
                              <a class="btn btn-remove" id="remove-logo">Remove</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="custom-form-main">
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Investor Name</label>
                              <input class="form-control" type="text" id="name" name="investor_name" placeholder="Enter your full name...">
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Investor Email</label>
                              <input class="form-control" type="text" name="investor_email" id="investor_email" placeholder="Enter your email..." onkeyup="checkinvestor()">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Country</label>
                              <select class="form-control" name="country_id">
                                 @foreach($countries as $country)
                                 <option value="{{$country->country_id}}">{{$country->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Website</label>
                              <input class="form-control" type="url" name="website" id="website" placeholder="Enter your Website...">
                           </div>
                        </div>

                     </div>
                  
                     <div class="row">
                        <div class="col-12">
                           <div class="form-group">
                              <label>Description</label>
                              <textarea  class="form-control" placeholder="Description…" id="desc" name="description"></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12">
                           <div class="form-header mt-5">
                              <h4>Social Profiles Information</h4>
                              <p>Add your social profiles information below.</p>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Facebook</label>
                              <input class="form-control" type="text" id="facebook" name="facebook" >
                              <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Twitter</label>
                              <input class="form-control" type="text" id="twitter" name="twitter" >
                              <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Linked In</label>
                              <input class="form-control" type="text" id="linked" name="linkedin" >
                              <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Instagram</label>
                              <input class="form-control" type="text" id="instagram" name="instagram" >
                              <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="customBtn">
                     <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                     <button type="submit" class="btn btn-primary ml-4">Submit</button>
                  </div>
                  </form>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>

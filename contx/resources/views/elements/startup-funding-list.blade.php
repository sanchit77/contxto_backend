<div class="col-lg-3 col-12">
 <div class="sidebar">
    <h2 class="sidebarHeading">
       LATEST LATIN AMERICAN STARTUP FUNDINGS
    </h2>
    <div class="sidebarList">
       @foreach($fundings as $funding)
       <div class="SideItem">

          <div class="row">
             <div class="col-12">
              <div class="row">
                <div class="col-8">
                @if($funding && ($funding->amount=="Undisclosed" || $funding->amount=="undisclosed"))
                  <a href="{{ route('web-company-details',['company_name' => $funding->startup->name ])}}" class="price">
                    <div class="undisclosed"> <span><img src="{{ asset('web/img/undisclosed.svg')}}" alt="img"></span> Undisclosed</div>
                  </a>
                @else
                  <a href="{{ route('web-company-details',['company_name' => $funding->startup->name ])}}" class="price">
                     ${{ $funding->amount }}<span>USD</span>
                  </a>
                @endif

                </div>
                <div class="col-4">
                    <div class="siteDetalsBtn">
                   <a href="{{ route('web-company-details',['company_name' => $funding->startup->name ])}}" class="btn">{{$funding->round->name}}</a>
                </div>
                </div>
              </div>
                <div class="siteDetals">
                   <h3><a href="{{ route('web-company-details',['company_name' => $funding->startup->name ])}}"> {{ucfirst($funding->startup->name)}}</a></h3>
                   <a href="javascript://" class="locStartup">
                    <i> <img src="{{ asset('web/img/locIcon.png')}}" alt="img"> </i> @if(isset($funding->startup->city)){{$funding->startup->city->name}},@endif @if(isset($funding->startup->country->name)) {{ $funding->startup->country->name }} @endif</a>
                </div>
              </div>
          </div>

       </div>
       @endforeach
    </div>
 </div>
</div>

@forelse($founders as $founder)
    <div class="profileCard">
	  <div class="profilePic mr-3">
	   <!--   <img src="{{ asset('web/img/120*120.png')}}" alt="img"> -->
	   <a href="#" class="firstLetter">{{strtoupper(substr($founder->name, 0, 1))}}</a>
	  </div>
	  <div class="profileInfo">
	    <a href="{{ route('web-founder-details',['founder_name' => $founder->slug ])}}" class="name">{{ucfirst($founder->name)}} </a>
	     <span class="compName">Founders other details will be here</span>
	  </div>
	</div>
@empty
<div class="noResFound text-center">
  <img class="img-fluid" src="{{ asset('web/img/no_result_found.svg')}}" alt="img">

  <h4>No result found!</h4>
<!-- <p>Don’t worry you can submit your startup as well. </br>
Just click <a harf="#" class="createCompBtn"> Add your startup</a></p> -->
</div>
@endforelse
<div class="customPagination">
    {{ $founders->appends(['keyword' => $keyword,'country_id' => $country_id,'city_id' => $city_id,'industries' => $industries_str])->links() }}
</div>

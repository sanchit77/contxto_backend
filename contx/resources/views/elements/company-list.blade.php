@forelse($startups as $startup)
 <div class="col-lg-3 col-6">
    <div class="compBlock">
       <div class="figBlock">
         @if($startup->logo)
         <a href="{{ route('web-company-details',['company_name' => $startup->slug ])}}"> <img src="/resize/{{ $startup->logo }}" alt="img-{{$startup->name}}"></a>
         @else
          <a href="{{ route('web-company-details',['company_name' => $startup->slug ])}}" class="firstLetter">{{strtoupper(substr($startup->name, 0, 1))}} </a>
         @endif
       </div>
       <h5>
         <a href="{{ route('web-company-details',['company_name' => $startup->slug ])}}">
           {{ucfirst($startup->name)}}
           @if(count($startup->founders)>0)
           <!-- (
           @foreach($startup->founders as $k=>$f)
           {{ $f->name }}
           {{ ($loop->last ? '' : ',') }}
           @endforeach
           ) -->
           @endif
       </a>
     </h5>
     @if(isset($startup->country))
       <p class="startUp"> <i><img src="{{ asset('web/img/locIcon.png')}}" alt="img"> </i> {{$startup->country->name}}</p>
        @endif
    </div>
 </div>
 @empty
 <div class="noResFound text-center" style="margin-left: 152px;">
   <img class="img-fluid" src="{{ asset('web/img/no_result_found.svg')}}" alt="img">

   <h4>No result found!</h4>
 <p>Don’t worry you can submit your startup as well. </br>
 Just click <a href="{{route('web-company-create-new')}}" class="createCompBtn1"> Add your startup</a></p>
 </div>
 @endforelse


<div class="customPagination">
    {{ $startups->appends(['keyword' => $keyword,'country_id' => $country_id,'city_id' => $city_id,'industries' => $industries_str])->links() }}
</div>
<script type="text/javascript">
    $(".read-more").click(function(){
        $('.more-content-'+$(this).attr('data-id')).toggle();
        $('.less-content-'+$(this).attr('data-id')).toggle();
    });
    $("#submitCompBtnw,.createCompBtn").click(function(){
      $("#new-company").trigger("reset");
      $("#new-company label.error").hide();
      $("#createComp").modal("toggle");
    });
</script>

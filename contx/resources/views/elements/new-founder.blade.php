<div class="modal" id="createFounder">
   <div class="modal-dialog" style="max-width:960px">
      <div class="modal-content">
         <div class="modal-header p-0">
            <button type="button" class="close" data-dismiss="modal">
            <img src="{{asset('web/img/close_popup.png')}}" alt="img">
            </button>
         </div>
         <div class="modal-body">
            <section class="formMain mt-4">
               <div class="container">
                  <div class="form-header">
                     <h4>Companies Founder Information</h4>
                     <p>Add your new Founder information below.</p>
                  </div>
                  <form action="#" id="new-founder" method="POST" enctype="multipart/form-data">
                     {{csrf_field()}}
                     <input type="hidden" name="randam" id="randam">
                  <div class="uploadSection" >
                     <div class="inputFiles">
                      <img class="company-logo" src="#" />
                     </div>
                     <div class="inputFilesActions">
                        <p>Add Your Founder Image</p>
                        <div class="row">
                           <div classs="col-md-6 col-7">
                              <div class="uploadImg">
                                 <input type='file' name="founder-image" id="founder-logo-input"  accept="image/*" onchange="readURL(this);" />
                                 <span>Upload Image</span>
                              </div>
                           </div>
                           <div class="col-md-6 col-5">
                              <a class="btn btn-remove remove-logo" id="remove-logo" style="display:none">Remove</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="custom-form-main">
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Founder Name</label>
                              <input class="form-control" type="text" name="founder_name" id="founder_name" placeholder="Enter your full name...">
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Founder Email</label>
                              <input class="form-control" type="text" name="founder_email" id="founder_email" placeholder="Enter your email...">
                           </div>
                        </div>
                     </div>
                     <br>
                      <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Nationality</label>
                              <input class="form-control" type="text" name="nationality" id="nationality" placeholder="Enter your nationality...">
                           </div>
                        </div>
                      </div>



                     <div class="row">
                        <div class="col-12">
                           <div class="form-header mt-5">
                              <h4>Social Profiles Information</h4>
                              <p>Add your social profiles information below.</p>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Facebook</label>
                              <input class="form-control" type="text" id="faceb" name="facebook" >
                              <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Twitter</label>
                              <input class="form-control" type="text" id="twitt" name="twitter" >
                              <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Linked In</label>
                              <input class="form-control" type="text" id="linkd" name="linkedin" >
                              <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Instagram</label>
                              <input class="form-control" type="text" id="insta" name="instagram" >
                              <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="customBtn">
                     <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                     <button type="submit" class="btn btn-primary ml-4">Submit</button>
                  </div>
                  </form>
               </div>
            </section>
         </div>
      </div>
   </div>
</div>

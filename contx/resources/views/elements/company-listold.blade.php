@forelse($startups as $startup)

    <div class="listItem">
     <div class="d-flex">
        <div class="brandName">
            @if($startup->logo)
            <a href="{{ route('web-company-details',['company_name' => $startup->slug ])}}"> <img src="/resize/{{ $startup->logo }}" alt="img-{{$startup->name}}"></a>
            @else
             <a href="{{ route('web-company-details',['company_name' => $startup->slug ])}}" class="firstLetter">{{strtoupper(substr($startup->name, 0, 1))}} </a>
            @endif
        </div>

        <div class="itemDetals">
            <h3>
                <a href="{{ route('web-company-details',['company_name' => $startup->slug ])}}">
                    {{ucfirst($startup->name)}}
                    @if(count($startup->founders)>0)
                    <!-- (
                    @foreach($startup->founders as $k=>$f)
                    {{ $f->name }}
                    {{ ($loop->last ? '' : ',') }}
                    @endforeach
                    ) -->
                    @endif
                </a>
            </h3>
           <ul>
              @foreach($startup->startup_industries as $ind)
              <li>
                <button class="multiSelectBx1 multiSelect1" id="industries1" onclick="clickindustry({{ $ind->industry_id }})">{{ $ind->name }}</button>

              </li>
              @endforeach
           </ul>
           @if(isset($startup->country))
           <div class="resLoc">
              <i> <img src="{{ asset('web/img/locIcon.png')}}" alt="img"> </i> <span>{{$startup->country->name}}</span>
           </div>
           @endif
        </div>
     </div>
     <div class="detalsView">
        <p class="more-content-{{$startup->startup_id}}">{{ str_limit($startup->description, $limit = 150, $end = '...')  }}
        @if(strlen($startup->description)>150)
            <button class="read-more more" data-id="{{$startup->startup_id}}">read more</button>
        @endif
        </p>
        <p class="less-content-{{$startup->startup_id}}" style="display: none;">{{ $startup->description }} <button class="read-more less" data-id="{{$startup->startup_id}}">read less</button></p>
     </div>
    </div>
@empty
<div class="noResFound text-center">
  <img class="img-fluid" src="{{ asset('web/img/no_result_found.svg')}}" alt="img">

  <h4>No result found!</h4>
<p>Don’t worry you can submit your startup as well. </br>
Just click <a harf="#" class="createCompBtn"> Add your startup</a></p>
</div>

@endforelse
<div class="customPagination">
    {{ $startups->appends(['keyword' => $keyword,'country_id' => $country_id,'city_id' => $city_id,'industries' => $industries_str])->links() }}
</div>
<script type="text/javascript">
    $(".read-more").click(function(){
        $('.more-content-'+$(this).attr('data-id')).toggle();
        $('.less-content-'+$(this).attr('data-id')).toggle();
    });
    $("#submitCompBtnw,.createCompBtn").click(function(){
      $("#new-company").trigger("reset");
      $("#new-company label.error").hide();
      $("#createComp").modal("toggle");
    });
</script>

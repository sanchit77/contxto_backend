@extends('layouts.web')
@section('content')
	<div class="container">
		<h4>Create Company :</h4>

		<div>
			@foreach ($errors->all() as $error)
			    <div class="alert alert-danger alert-dismissible fade show" role="alert">
				  <strong>Error!</strong> {{ $error }}
				  <button type="button" class="close" data-dismiss="alert" aria-label="Close">
				    <span aria-hidden="true">&times;</span>
				  </button>
				</div>
			@endforeach
		</div>

		<form action="{{route('web-company-save')}}" method="post">

		  <div class="form-group">
		    <label for="creator_name">Your Name</label>
		    <input type="text" class="form-control" id="creator_name" name="creator_name" placeholder="Your Name" value="{{ Request::old('creator_name') }}">
		  </div>

		  <div class="form-group">
		    <label for="creator_email">Your Email</label>
		    <input type="email" class="form-control" id="creator_email" name="creator_email" placeholder="Your Email" value="{{ Request::old('creator_email') }}">
		  </div>

		  <div class="form-group">
		    <label for="name">Company Name</label>
		    <input type="text" class="form-control" id="name" name="name" placeholder="Name" value="{{ Request::old('name') }}">
		  </div>

		  <div class="form-group">
		    <label for="description">Description</label>
		    <textarea class="form-control" id="description" name="description" value="{{ Request::old('description') }}" rows="3"></textarea>
		  </div>

		  <div class="form-group">
		    <label for="country_id">Select Country</label>
		    <select class="form-control" id="country_id" name="country_id">
		    	@foreach($countries as $country)
		    		<option value="{{$country->country_id}}" {{ (Request::old("country_id") == $country->country_id ? "selected":"") }} >{{$country->name}}</option>
		    	@endforeach
		    </select>
		  </div>

		  <div class="form-group">
		    <label for="founded">Founded In</label>
		    <select class="form-control" id="founded" name="founded">
		    	@foreach($founded_years as $founded)
		    		<option value="{{$founded}}" {{ (Request::old("founded") == $founded ? "selected":"") }} >{{$founded}}</option>
		    	@endforeach
		    </select>
		  </div>

		  <div class="form-group">
		    <label for="founder_name">Founder Name</label>
		    <input type="text" class="form-control" id="founder_name" name="founder_name" value="{{ Request::old('founder_name') }}" placeholder="Founder Name">
		  </div>

		  <div class="form-row align-items-center">
		    <div class="col-sm-12 my-1">
		      <label class="sr-only" for="website">Website</label>
		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text">Website</div>
		        </div>
		        <input type="url" class="form-control" id="website" name="website" value="{{ Request::old('website') }}" placeholder="example.com">
		      </div>
		    </div>
		  </div>

		  <div class="form-row align-items-center">
		    <div class="col-sm-12 my-1">
		      <label class="sr-only" for="facebook">Facebook</label>
		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text"><i class="fa fa-facebook" aria-hidden="true"></i></div>
		        </div>
		        <input type="url" class="form-control" id="facebook" name="facebook" value="{{ Request::old('facebook') }}" placeholder="">
		      </div>
		    </div>
		  </div>

		  <div class="form-row align-items-center">
		    <div class="col-sm-12 my-1">
		      <label class="sr-only" for="instagram">Instagram</label>
		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text"><i class="fa fa-instagram" aria-hidden="true"></i></div>
		        </div>
		        <input type="url" class="form-control" id="instagram" name="instagram" value="{{ Request::old('instagram') }}" placeholder="">
		      </div>
		    </div>
		  </div>

		  <div class="form-row align-items-center">
		    <div class="col-sm-12 my-1">
		      <label class="sr-only" for="twitter">Twitter</label>
		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text"><i class="fa fa-twitter" aria-hidden="true"></i></div>
		        </div>
		        <input type="url" class="form-control" id="twitter" name="twitter" value="{{ Request::old('twitter') }}" placeholder="">
		      </div>
		    </div>
		  </div>

		  <div class="form-row align-items-center">
		    <div class="col-sm-12 my-1">
		      <label class="sr-only" for="linkedin">LinkedIn</label>
		      <div class="input-group">
		        <div class="input-group-prepend">
		          <div class="input-group-text"><i class="fa fa-linkedin" aria-hidden="true"></i></div>
		        </div>
		        <input type="url" class="form-control" id="linkedin" name="linkedin" value="{{ Request::old('linkedin') }}" placeholder="">
		      </div>
		    </div>
		  </div>


		  <div class="form-group">
		    <label for="industries">Industry</label>
		    <select class="form-control selectpicker_multi" id="industries" value="{{ Request::old('industries[]') }}" name="industries[]">
		      @foreach($industries as $industry)
		      <option value="{{$industry->industry_id}}">{{$industry->name}}</option>
		      @endforeach
		    </select>
		  </div>

		  {{csrf_field()}}
		  <button type="submit" class="btn btn-primary">Submit</button>

		</form>
	</div>

<script type="text/javascript">
	$(document).ready(function(){
		$('.selectpicker').select2();
		$('.selectpicker_multi').select2({
		    multiple: true
		});
	
	});

</script>
@endsection

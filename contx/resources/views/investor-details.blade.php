@extends('layouts.web')
@section('content')
@section('title', $investor->name .' | Contxto')
@section('description', '')
@section('image_link', URL::to('/') . '/resize/'.$investor->image)
@section('imagename', $investor->name .' Logo')
@section('url',url()->current())
     <section class="detailsPageHeader">
      <div class="container">
         <div class="row">
           <div class="col-md-5">
             @include('elements.detail-navbar')
           </div>
            <div class="col-md-7">
               <div class="seachbox">
                  <input type="text" name="keyword" id="keyword" placeholder="Search Companies">
                  <button onClick="searchHome()"><img src="{{ asset('WebAssets/img/search.png')}}"></button>
               </div>
            </div>
          </div>
            <!-- <div class="col-md-4">
               <button type="button" class="create"> CREATE NEW COMPANY</button>
            </div> -->
            <div class="row">
              <div class="col-md-8">
                <div class="mainInfo">
                 <div class="companyLogo">
                    @if($investor->image)
                    <img src="/resize/{{ $investor->image }}" alt="img">
                    @else
                    <div class="firstLetter">{{strtoupper(substr(@$investor->name, 0, 1))}}</div>
                    @endif
                 </div>
                 <div class="companyInfo">
                    <h1 class="mb-3">{{ucfirst(@$investor->name)}}</h1>
                    <div class="compnayLocation mb-3">  <span class="pl-2" style="color: lightgreen;">ACTIVE</span></div>
                    @if(@$investor->country_id)
                     <div class="compnayLocation mb-3"> <i> <img src="{{ asset('web/img/location_white.png')}}" alt="img"> </i> <span class="pl-2">{{$investor->country->name}}</span></div>
                    @endif
                    <!-- <div class="compnayWebsite"> <i> <img src="{{ asset('web/img/web.png')}}" alt="img"> </i> <span class="pl-1">{{rtrim(@$investor->website,'/')}}</span> </div> -->
                 </div>
                </div>
              </div>
               <div class="col-md-4">
                    <div class="social-media">
                 <ul>
                    @if($investor->linkedin)
                    <li> <a href="{{$investor->linkedin}}" target="_blank">
                       <img src="{{ asset('web/img/linkdIn.svg')}}" alt="img">
                       </a>
                    </li>
                    @endif
                    @if($investor->twitter)
                    <li> <a href="{{$investor->twitter}}" target="_blank">
                       <img src="{{ asset('web/img/twitter.svg')}}" alt="img">
                       </a>
                    </li>
                    @endif
                    @if($investor->instagram)
                    <li> <a href="{{$investor->instagram}}" target="_blank">
                       <img src="{{ asset('web/img/insta.svg')}}" alt="img">
                       </a>
                    </li>
                    @endif
                    @if($investor->facebook)
                    <li> <a href="{{$investor->facebook}}" target="_blank">
                       <img src="{{ asset('web/img/facebook.svg')}}" alt="img">
                       </a>
                    </li>
                    @endif
                 </ul>
              </div>
               </div>
            </div>
          </div>
        </section>
<br><br><br><br>
        <section class="belowSection">
           <div class="container">





              <div class="heading3">
                 <span class="line"></span>
                 <h2 class="text"> PORTFOLIO</h2>
              </div>
              <div class="similarCompnies">
                 <div class="row">
                      @forelse($investor_companies as $similar)
                      <div class="col-lg-4 col-md-6 col-12">
                           <div class="profileCard">
                              <div class="profilePic mr-3">
                                 @if($similar->logo)
                                <a href="{{ route('web-company-details',['company_name' => $similar->slug ])}}"><img src="/resize/{{ $similar->logo }}/120" alt="img"></a>
                                 @else
                                 <!-- <img src="{{asset('web/img/ic_company_placeholder_full.png')}}" alt="img"> -->
                                  <a href="{{ route('web-company-details',['company_name' => $similar->slug ])}}" class="firstLetter">{{strtoupper(substr($similar->name, 0, 1))}}</a>
                                 @endif
                              </div>
                              <div class="profileInfo">
                                 <a href="{{ route('web-company-details',['company_name' => $similar->slug ])}}" class="name">{{ $similar->name }}</a>
                                 <!-- <span class="compName">11 – 50 employees</span> -->
                              </div>

                           </div>
                      </div>
                    @empty

                   <div class="col-12"> No Company Found</div>
                    @endforelse
                 </div>
              </div>@if($country_wise)
                   <br><br><br><br><br><br>


                    <div class="heading4">
                          <h2 class="text">INVESTMENTS BY COUNTRY</h2>
                    </div>
                    @foreach($country_wise as $country_wi)

                    <div class="progressCustom">
                    <h4>{{@$country_wi->country_name}}</h4>
                    <div class="progressOuter">
                    <div class="progress">
                     <div class="progress-bar" style="width:{{ round($country_wi->count,2)}}%;">{{ round($country_wi->count,2)}}%</div>
                     </div>
                    </div>

                 </div>
                 @endforeach
                 @endif
                 <!-- <div class="progressCustom">
                    <h4>Argentina</h4>
                    <div class="progressOuter">
                    <div class="progress">
                     <div class="progress-bar" style="width:50%;">50%</div>
                     </div>
                    </div>

                 </div>
                 <div class="progressCustom">
                    <h4>Brazil</h4>
                    <div class="progressOuter">
                    <div class="progress">
                     <div class="progress-bar" style="width:85%;">85%</div>
                     </div>
                    </div>

                 </div> -->


                    <div class="row mt-5">
                      @if($industry_wise)
                       <div class="col-md-6">
                             <div class="heading4">
                                   <h2 class="text-center">INVESTMENTS BY INDUSTRY</h2>
                             </div>

                          <div id="investIndus" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>



                       </div>
                        @endif
                          @if($stage_wise)
                       <div class="col-md-6">
                             <div class="heading4">
                                   <h2 class="text-center">INVESTMENTS BY STAGE</h2>
                             </div>

                          <div id="investStage" style="min-width: 310px; height: 400px; max-width: 600px; margin: 0 auto"></div>



                       </div>
                      @endif

                    </div>




              <!-- <div class="heading3">
                 <span class="line"></span>
                 <h2 class="text">INVESTOR NEWS</h2>
              </div>
              <div class="news-listing">
                  <div class="row">

                      @if($all_news)
                        @forelse($all_news as $news)
                            <div class="col-lg-4 col-md-6 col-12">
                              <div class="newsBlk">
                                 <div class="figBlock">
                                    <a href="{{$news->link}}" target="_blank">
                                    @if($news->jetpack_featured_media_url)
                                    <img src="{{$news->jetpack_featured_media_url}}" alt="img">
                                    @else
                                    <img src="{{asset('web/img/news_placeholder.png')}}" alt="img">
                                    @endif
                                    </a>
                                     <p class="newsHeading">{!!$news->title->rendered!!}</p>
                                 </div>

                                 <div class="time"> <span class="greenIcon"> <span></span> </span>{{\Carbon\Carbon::parse(date('Y-m-d H:i:s', strtotime($news->date)))->diffForHumans()}}</div>
                                 <div class="newsText">{!!$news->excerpt->rendered!!}<a class="read-more" target="_blank" href="{{$news->link}}">Read More</a>
                                 </div>
                              </div>
                           </div>
                        @empty
                       <div class="col-12">No News Found</div>
                        @endforelse
                      @else
                        <div class="col-12">No News Found</div>
                      @endif
                  </div>
              </div> -->

              <div class="heading3">
                 <span class="line"></span>
                 <h2 class="text">PORTFOLIO NEWS</h2>
              </div>
              <div class="news-listing">
                  <div class="row">

                      @if(@$all_news1)
                        @forelse(@$all_news1 as $news)
                            <div class="col-lg-4 col-md-6 col-12">
                              <div class="newsBlk">
                                 <div class="figBlock">
                                    <a href="{{$news->link}}" target="_blank">
                                    @if($news->jetpack_featured_media_url)
                                    <img src="{{$news->jetpack_featured_media_url}}" alt="img">
                                    @else
                                    <img src="{{asset('web/img/news_placeholder.png')}}" alt="img">
                                    @endif
                                    </a>
                                     <p class="newsHeading">{!!$news->title->rendered!!}</p>
                                 </div>

                                 <div class="time"> <span class="greenIcon"> <span></span> </span>{{\Carbon\Carbon::parse(date('Y-m-d H:i:s', strtotime($news->date)))->diffForHumans()}}</div>
                                 <div class="newsText">{!!$news->excerpt->rendered!!}<a class="read-more" target="_blank" href="{{$news->link}}">Read More</a>
                                 </div>
                              </div>
                           </div>
                        @empty
                       <div class="col-12">No News Found</div>
                        @endforelse
                      @else
                        <div class="col-12">No News Found</div>
                      @endif
                  </div>
              </div>
              {{--@if($company->startup_news_count>3)
              <div class="moreNews">
                 <a href="javascript://" data-last-id="{{$last_news_id}}" data-more-count="{{$company->startup_news_count-3}}" id="load-more-news">+{{$company->startup_news_count-3}} more</a>
              </div>
              @endif--}}

           </div>
        </section>


   <!-- start disclosed funding section -->

   <!-- end disclosed funding section -->


   <!--  CREATE NEW COMPANY popup start -->
   <div class="modal" id="createComp">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Create New Company</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <form action="{{route('web-company-save')}}" method="post">
               <div class="custom-form-main">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Your Name</label>
                           <input class="form-control" type="text" name="creator_name" id="creator_name" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Your Email</label>
                           <input class="form-control" type="text" name="creator_email" id="creator_email" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Company Name</label>
                           <input class="form-control" type="text" name="name" id="name" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Country</label>
                           <select class="form-control" id="country_id" name="country_id">
                            @foreach($countries as $country)
                              <option value="{{$country->country_id}}" {{ (Request::old("country_id") == $country->country_id ? "selected":"") }} >{{$country->name}}</option>
                            @endforeach
                          </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Founded In</label>
                           <select class="form-control" id="founded" name="founded">
                            @foreach($founded_years as $founded)
                              <option value="{{$founded}}" {{ (Request::old("founded") == $founded ? "selected":"") }} >{{$founded}}</option>
                            @endforeach
                          </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Founder Name</label>
                           <input class="form-control" type="text" name="founder_name" id="founder_name" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="form-group">
                           <label>Description</label>
                           <textarea  class="form-control" id="description" name="description" required></textarea>
                        </div>
                     </div>
                      <div class="col-12">
                        <div class="form-group">
                           <label>Website</label>
                           <input class="form-control" type="text" name="website" id="website" required>
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Facebook</label>
                           <input class="form-control" type="text" name="facebook" id="facebook" required>
                           <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Instagram</label>
                           <input class="form-control" type="text" name="instagram" id="instagram">
                            <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                      <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Twitter</label>
                           <input class="form-control" type="text" name="twitter" id="twitter">
                           <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Linked In</label>
                           <input class="form-control" type="text" name="linkedin" id="linkedin">
                           <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                         <div class="row">
                     <div class="col-12">
                   <div class="form-group popupForm">
                        <label>Industry</label>

                         <input type="text"  data-role="tagsinput" value="" id="testtag" name="industries" required>
                     </div>
                  </div>
               </div>

               </div>
            </div>
            {{csrf_field()}}
            <div class="modal-footer">
               <div class="customBtn">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  <!-- <button type="submit" class="btn btn-primary"  data-dismiss="modal">Submit</button> -->
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
   <!--  CREATE NEW COMPANY popup end -->

   <script type="text/javascript">

      $(document).ready(function(){
        $('#keyword').on('change keyup',function(e) {
            if(e.keyCode == 13){
              // var url = "{{route('web-home')}}";
              // var keyword="{{app('request')->input('keyword')}}";
              // var page = 1;
              // var keyword = $('#keyword').val();
              // url += "?page="+page+"&keyword="+keyword;
              // window.location = url;

              var keyword = $('#keyword').val();
              $.ajax({
                url: "/getslug",
                type: "get",
                data:{
                  keyword:keyword,
                },
                datatype: "json"
              }).done(function(data){
                var key=data.slug.slug;
                var key2=data.slug;
                if(key2!='1'){
                  window.location.href="{{route('web-company-details')}}/"+key;
                }else{
                  $('#keyword').focus();
                  alert('No Company found');
                }
              });
            }
         });
      });

      function searchHome(){
           // var url = "{{route('web-home')}}";
           // var keyword="app('request')->input('keyword')";
           // var page = 1;
           // var keyword = $('#keyword').val();
           // url += "?page="+page+"&keyword="+keyword;
           // window.location = url;

           var keyword = $('#keyword').val();
           $.ajax({
             url: "/getslug",
             type: "get",
             data:{
               keyword:keyword,
             },
             datatype: "json"
           }).done(function(data){
             var key=data.slug.slug;
             var key2=data.slug;
             if(key2!='1'){
               window.location.href="{{route('web-company-details')}}/"+key;
             }else{
               $('#keyword').focus();
                alert('No Company found');
             }
           });
       }


      $('#testtag').tagsinput({
         confirmKeys: [13, 44],
         maxTags: 5,
         typeahead: {
           source: {!! $industries_tags !!}
         }
       });

      function goToUrl(url){
         window.location = url;
       }


   </script>
   <script type="application/ld+json">
   {"@context":"https://schema.org",
   "@type":"Organization",
   "url":"http://contxto.netsolutionindia.com/investor/{{$investor->slug}}",
   "sameAs":[
     "{{$investor->facebook}}",
     "{{$investor->instagram}}",
     "{{$investor->linkedin}}",
     "{{$investor->twitter}}"
   ],
   "@id":"https://www.contxto.com/en/investors",
   "name":"{{$investor->name}}",
   "logo":"http://contxto.netsolutionindia.com/resize/{{$investor->image}}"}
   </script>

   <script>

         //INVESTMENTS BY STAGE function start
   //       var pieColors = (function () {
   //
   //  Highcharts.setOptions({
   //       colors: ['#2EC16D', '#000000', '#7BFFC5', '#3DFF95']
   //      });
   //
   //  }());
   //
   //       Highcharts.chart('investStage', {
   //     chart: {
   //         type: 'variablepie'
   //     },
   //     credits: {
   //       enabled: false
   //   },
   //   exporting: {
   //      enabled: false
   //      },
   //      legend: {
   //       enabled: false,
   //
   //     },
   //
   //     title: {
   //         text: ''
   //     },
   //      plotOptions: {
   //       variablepie: {
   //             dataLabels: {
   //                 enabled: false
   //             }
   //         }
   //     },
   //
   //     series: [{
   //         minPointSize: 10,
   //         innerSize: '40%',
   //         zMin: 0,
   //
   //         data: [{
   //
   //             y: 505370,
   //             z: 92.9
   //         }, {
   //
   //             y: 551500,
   //             z: 118.7
   //         }, {
   //
   //             y: 312685,
   //             z: 124.6
   //         },
   //          {
   //
   //             y: 357022,
   //             z: 235.6
   //         }]
   //     }]
   // });

        //INVESTMENTS BY STAGE function end
        </script>


   <script>

    //INVESTMENTS BY INDUSTRY function start
   var pieColors = (function () {

   Highcharts.setOptions({
        colors: ['#2EC16D', '#7CFC00', '#7BFFC5', '#3DFF95']
       });

   }());
   Highcharts.chart('investStage', {
     chart: {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false,
       type: 'pie'
     },
     credits: {
         enabled: false
     },
     exporting: {
        enabled: false
        },
     title: {
       text: ''
     },
     tooltip: {
       pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
     },
     plotOptions: {
       pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         colors: pieColors,
         dataLabels: {
           enabled: false,
           format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
           distance: -50,
           filter: {
             property: 'percentage',
             operator: '>',
             value: 4
           }
         }
       }
     },
     series: [{
       name: 'Share',
       data:<?php echo json_encode($stage_wise);?>
     }]
   });
   // Build the chart
   Highcharts.chart('investIndus', {
     chart: {
       plotBackgroundColor: null,
       plotBorderWidth: null,
       plotShadow: false,
       type: 'pie'
     },
     credits: {
         enabled: false
     },
     exporting: {
        enabled: false
        },
     title: {
       text: ''
     },
     tooltip: {
       pointFormat: '{series.name}: <b>{point.percentage:.1f}%</b>'
     },
     plotOptions: {
       pie: {
         allowPointSelect: true,
         cursor: 'pointer',
         colors: pieColors,
         dataLabels: {
           enabled: false,
           format: '<b>{point.name}</b><br>{point.percentage:.1f} %',
           distance: -50,
           filter: {
             property: 'percentage',
             operator: '>',
             value: 4
           }
         }
       }
     },
     series: [{
       name: 'Share',
       data:<?php echo json_encode($industry_wise);?>
     }]
   });
   //INVESTMENTS BY INDUSTRY function end
   </script>
   @stop

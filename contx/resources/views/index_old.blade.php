@extends('layouts.web')

@section('content')

<div class="searchArea">
 <div class="container">
    <div class="row">
       <div class="col-md-8">
          <div class="seachbox">
             <input type="text" name="keyword" id="keyword" @if($keyword) value="{{$keyword}}" @endif placeholder="Search Companies">
             <button onClick="searchHome()"><img src="{{ asset('WebAssets/img/search.png') }}"></button>
          </div>
          @if(Session::has('msg'))
              <div class="alert alert-success">
                  <a class="close" data-dismiss="alert">×</a>
                  <strong>Success!</strong> {!!Session::get('msg')!!}
              </div>
          @endif

          @if(Session::has('error'))
              <div class="alert alert-danger">
                  <a class="close" data-dismiss="alert">×</a>
                  <strong>Error!</strong> {!!Session::get('error')!!}
              </div>
          @endif

       </div>
       <div class="col-md-4">
          <button type="button" class="create" data-toggle="modal" data-target="#createComp"> CREATE NEW COMPANY</button>
       </div>
    </div>
 </div>
</div>
<section id="detalsPage">
 <div class="container">
    <div class="row">
       <div class="custom-tabs">
          <ul class="nav nav-tabs" role="tablist">
             <li class="nav-item">
                <a class="nav-link active" data-toggle="tab" href="#news">Companies</a>
             </li>
             <li class="nav-item">
                <a class="nav-link"  data-toggle="tab" href="#cpmpanies">News</a>
             </li>
             <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#investors">Investors</a>
             </li>
             <li class="nav-item">
                <a class="nav-link" data-toggle="tab" href="#founders">Founders</a>
             </li>
          </ul>
       </div>
    </div>
 </div>
 <div class="contenArea">
    <div class="container">
       <div class="row">
          <div class="col-xl-9 col-lg-8 col-12">
             <!-- Tab panes -->
             <div class="tab-content">
                <div id="news" class="container tab-pane active">
                   <div class="btn-group">
                      <div class="ddGroup">
                         <div class="dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @if($country_name)
                            {{ $country_name }} 
                            @else
                            Country 
                            @endif
                            </button>
                            <div class="dropdown-menu">
                               @foreach($countries as $country)  
                               <a class="dropdown-item" href="#" onClick="changeCountry({{$country->country_id}})">{{$country->name}}</a>
                               @endforeach
                            </div>
                         </div>
                         <div class="dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @if($city_name)
                            {{ $city_name }} 
                            @else
                            CITY
                            @endif
                            </button>
                            <div class="dropdown-menu">
                               @foreach($cities as $city)
                               <a class="dropdown-item" href="#" onClick="changeCity({{ $city->city_id }})">{{$city->name}}</a>
                               @endforeach
                            </div>
                         </div>
                         <div class="dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @if($industry_name)
                            {{ $industry_name }}
                            @else
                            INDUSTRY
                            @endif 
                            </button>
                            <div class="dropdown-menu">
                               @foreach($industries as $industry)
                               <a class="dropdown-item" href="#" onClick="changeIndustry({{$industry->industry_id}})" >{{$industry->name}}</a>
                               @endforeach
                            </div>
                         </div>
                         <div class="dropdown">
                            <button type="button" class="btn dropdown-toggle" data-toggle="dropdown">
                            @if($investor_name)
                            {{ $investor_name }}
                            @else
                            INVESTOR
                            @endif 
                            </button>
                            <div class="dropdown-menu">
                               @foreach($investors as $investor)
                               <a class="dropdown-item" onClick="changeInvestor({{$investor->investor_id}})">{{$investor->name}}</a>
                               @endforeach
                            </div>
                         </div>
                      </div>
                   </div>

                   @if(!empty($country_name) || !empty($city_name) || !empty($industry_name))
                   <div class="result">
                      <ul>
                         @if($country_name)
                         <li><button type="button">{{$country_name}}</button></li>
                         <li><a onClick="removeFilter('country_id')"><img src="{{asset('WebAssets/img/close.png')}}"/></a></li>
                         @endif
                         @if($city_name)
                         <li><button type="button">{{$city_name}}</button></li>
                         <li><a onClick="removeFilter('city_id')"><img src="{{asset('WebAssets/img/close.png')}}"/></a></li>
                         @endif
                         @if($industry_name)
                         <li><button type="button">{{$industry_name}}</button></li>
                         <li><a onClick="removeFilter('industry_id')"><img src="{{asset('WebAssets/img/close.png')}}"/></a></li>
                         @endif
                         @if($investor_name)
                         <li><button type="button">{{$investor_name}}</button></li>
                         <li><a onClick="removeFilter('investor_id')"><img src="{{asset('WebAssets/img/close.png')}}"/></a></li>
                         @endif
                      </ul>
                   </div>
                   @endif

                   <div class="listView">
                        @foreach($startups as $startup)
                        <div class="listItem">
                            <div class="brandName">
                                @if($startup->logo)
                                <img src="/resize/{{ $startup->logo }}" onClick="goToUrl('{{ route('web-company-details',['company_name' => $startup->slug ])}}')" style="margin: 0 auto;">
                                @else
                                <h2 onClick="goToUrl('{{ route('web-company-details',['company_name' => $startup->slug ])}}')">{{ $startup->name }}</h2>
                                @endif
                            </div>
                            <div class="itemDetals">
                                <h3>{{$startup->name}} @if(!empty($startup->founders)) ( @foreach($startup->founders as $f) {{ $f->name }}, @endforeach ) @endif</h3>
                                <div class="add">
                                   <ul>
                                      @if(isset($startup->country))<li><span><img src="{{ asset('WebAssets/img/location.png')}}"></span>{{$startup->country->name}}</li>@endif
                                      @foreach($startup->startup_industries as $ind)
                                      <li><span><img src="{{ asset('WebAssets/img/teg.png')}}"></span>{{ $ind->name }}</li>
                                      @endforeach
                                   </ul>
                                </div>
                                <div class="detalsView">
                                   <p>{{ $startup->description }}</p>
                                </div>
                            </div>
                        </div>
                        @endforeach

                        <div class="listItem">
                        {{ $startups->appends(['country_id' => $country_id,'city_id'=> $city_id ,'industry_id'=>$industry_id ,'keyword' => $keyword])->links() }}
                        </div>
                      <!-- <div class="listItem">
                         <div class="brandName">
                            <h2>grooves</h2>
                         </div>
                         <div class="itemDetals">
                            <h3>Grooves (Skill Shift, Forkwell, Crowd Agent</h3>
                            <div class="add">
                               <ul>
                                  <li><span><img src="{{ asset('WebAssets/img/location.png')}}"></span>Japan</li>
                                  <li><span><img src="{{ asset('WebAssets/img/teg.png')}}"></span>Lifestyle </li>
                               </ul>
                            </div>
                            <div class="detalsView">
                               <p>Grooves is a social recruiting platform that launched Skill Shift, a service that connects people looking for a side job or opportunities in non-urban areas, Forkwell, an IT engineer recruiting platform, and Crowd Agent, a service that connects non-engineer job seekers to employers.</p>
                            </div>
                         </div>
                      </div> -->
                   </div>
                </div>
                <div id="cpmpanies" class="container tab-pane fade">
                   <br>
                   <!-- <h3>Menu 1</h3>
                   <p>Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat.</p> -->
                   @foreach($news as $nw)
                   <h3>{{ $nw->startup->name }}</h3>
                   <p>{{ $nw->description }}</p>
                   @endforeach

                   <div class="listItem">
                        {{ $news->appends(['country_id' => $country_id,'city_id'=> $city_id ,'industry_id'=>$industry_id ,'keyword' => $keyword])->links() }}
                    </div>
                </div>

                <div id="investors" class="container tab-pane fade">
                   <br>
                   @foreach($investors_data as $inv)
                      <div class="row">
                        <div class="col-md-12">
                           <img  src="https://via.placeholder.com/64">
                           <div class="company-description">
                              <h6>{{ $inv->name }}</h6>
                              <span>4 companies</span>
                           </div>
                        </div>
                      </div>
                    @endforeach

                   <div class="listItem">
                        {{ $investors_data->appends(['country_id' => $country_id,'city_id'=> $city_id ,'industry_id'=>$industry_id ,'keyword' => $keyword])->links() }}
                    </div>
                </div>

                <div id="founders" class="container tab-pane fade">
                   <br>
                   @foreach($founders_data as $fnd)
                   <h3>{{ $fnd->name }}</h3>
                   <p>other details</p>
                   @endforeach

                   <div class="listItem">
                        {{ $founders_data->appends(['country_id' => $country_id,'city_id'=> $city_id ,'industry_id'=>$industry_id ,'keyword' => $keyword])->links() }}
                    </div>
                </div>

             </div>
          </div>
          <div class="col-xl-3 col-lg-4 col-12">
             <div class="sidebar">
                <header>
                   LATEST LATIN AMERICAN STARTUP FUNDINGS
                </header>
                <div class="sidebarList">
                    @foreach($fundings as $funding)
                   <div class="SideItem">
                      <div class="price">
                         ${{ $funding->amount }}<span>USD</span>
                      </div>
                      <div class="siteDetals">
                         <h3 onClick="goToUrl('{{ route('web-company-details',['company_name' => $startup->name ])}}')" >{{$funding->startup->website}}</h3>
                         <span>@if(isset($funding->startup->city)){{$funding->startup->city->name}},@endif @if(isset($funding->startup->country->name)) {{ $funding->startup->country->name }} @endif</span>
                      </div>
                      <div class="siteDetalsBtn">
                         <button type="button" class="btn">Brigd</button>
                      </div>
                   </div>
                    @endforeach
                   <!-- <div class="SideItem">
                      <div class="price">
                         $1.8M<span>USD</span>
                      </div>
                      <div class="siteDetals">
                         <h3>yotepresto.com</h3>
                         <span>Guadalajara,Mexico</span>
                      </div>
                      <div class="siteDetalsBtn">
                         <button type="button" class="btn">Brigd</button>
                      </div>
                   </div> -->
                </div>
             </div>
          </div>
       </div>
    </div>
 </div>
</section>

<!--  CREATE NEW COMPANY popup start -->
   <div class="modal" id="createComp">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Create New Company</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <form action="{{route('web-company-save')}}" method="post">
               <div class="custom-form-main">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Your Name</label>
                           <input class="form-control" type="text" name="creator_name" id="creator_name" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Your Email</label>
                           <input class="form-control" type="text" name="creator_email" id="creator_email" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Company Name</label>
                           <input class="form-control" type="text" name="name" id="name" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Country</label>
                           <select class="form-control" id="country_id" name="country_id">
                            @foreach($countries as $country)
                              <option value="{{$country->country_id}}" {{ (Request::old("country_id") == $country->country_id ? "selected":"") }} >{{$country->name}}</option>
                            @endforeach
                          </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Founded In</label>
                           <select class="form-control" id="founded" name="founded"">
                            @foreach($founded_years as $founded)
                              <option value="{{$founded}}" {{ (Request::old("founded") == $founded ? "selected":"") }} >{{$founded}}</option>
                            @endforeach
                          </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Founder Name</label>
                           <input class="form-control" type="text" name="founder_name" id="founder_name" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="form-group">
                           <label>Description</label>
                           <textarea  class="form-control" id="description" name="description" required></textarea>  
                        </div>
                     </div>
                      <div class="col-12">
                        <div class="form-group">
                           <label>Website</label>
                           <input class="form-control" type="text" name="website" id="website" required>
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Facebook</label>
                           <input class="form-control" type="text" name="facebook" id="facebook" required>
                           <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Instagram</label>
                           <input class="form-control" type="text" name="instagram" id="instagram">
                            <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                      <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Twitter</label>
                           <input class="form-control" type="text" name="twitter" id="twitter">
                           <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Linked In</label>
                           <input class="form-control" type="text" name="linkedin" id="linkedin">
                           <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                         <div class="row">
                     <div class="col-12">
                   <div class="form-group popupForm">
                        <label>Industry</label>   
         
                         <input type="text"  data-role="tagsinput" value="" id="testtag" name="industries" required>
                     </div>
                  </div>
               </div>

               </div>
            </div>
            {{csrf_field()}}
            <div class="modal-footer">
               <div class="customBtn">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  <!-- <button type="submit" class="btn btn-primary"  data-dismiss="modal">Submit</button> -->
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
   <!--  CREATE NEW COMPANY popup end -->

<script type="text/javascript">
    $(document).ready(function(){
        $('#keyword').on('change keyup',function(e) {
            if(e.keyCode == 13){
              var url = "{{route('web-home')}}";
              var keyword="{{app('request')->input('keyword')}}";
              var page = 1;
              var keyword = $('#keyword').val();
              url += "?page="+page+"&keyword="+keyword;
              window.location = url;

            }
        });
    });

    function searchHome(){
        var url = "{{route('web-home')}}";
        var keyword="app('request')->input('keyword')";
        var page = 1;
        var keyword = $('#keyword').val();
        url += "?page="+page+"&keyword="+keyword;
        window.location = url;
    }

    function changeCountry(country_id){
        var url = "{{ route('web-home') }}";
        var keyword="{{ app('request')->input('keyword') }}";
        var page = 1;
        var keyword = $('#keyword').val();
        url += "?page="+page;
        if(keyword)
          url += "&keyword="+keyword;
        url += "&country_id="+country_id;
        window.location = url;
    }

    function changeCity(city_id){
        var url = "{{ route('web-home') }}";
        var keyword="{{ app('request')->input('keyword') }}";
        var country_id={{ app('request')->input('country_id',0) }};
        var page = 1;
        var keyword = $('#keyword').val();
        url += "?page="+page;
        if(keyword)
          url += "&keyword="+keyword;
        if(country_id)
          url += "&country_id="+country_id;
        url += "&city_id="+city_id;
        window.location = url;
    }

    function changeIndustry(industry_id){
        var url = "{{ route('web-home') }}";
        var keyword="{{ app('request')->input('keyword') }}";
        var country_id={{ app('request')->input('country_id',0) }};
        var city_id={{ app('request')->input('city_id',0) }};
        var investor_id={{ app('request')->input('investor_id',0) }};
        var page = 1;
        var keyword = $('#keyword').val();
        url += "?page="+page;
        if(keyword)
          url += "&keyword="+keyword;
        if(country_id)
          url += "&country_id="+country_id;
        if(city_id)
          url += "&city_id="+city_id;
        if(investor_id)
          url += "&investor_id="+investor_id;
        url += "&industry_id="+industry_id;
        window.location = url;
    }

    function changeInvestor(investor_id){
        var url = "{{ route('web-home') }}";
        var keyword="{{ app('request')->input('keyword') }}";
        var country_id={{ app('request')->input('country_id',0) }};
        var city_id={{ app('request')->input('city_id',0) }};
        var industry_id={{ app('request')->input('industry_id',0) }};
        var page = 1;
        var keyword = $('#keyword').val();
        url += "?page="+page;
        if(keyword)
          url += "&keyword="+keyword;
        if(country_id)
          url += "&country_id="+country_id;
        if(city_id)
          url += "&city_id="+city_id;
        if(industry_id)
          url += "&industry_id="+industry_id;
        url += "&investor_id="+investor_id;
        window.location = url;
    }

    function removeFilter(filter_name){

      var url = new URL(window.location.href);
      url.searchParams.set(filter_name, 0); // setting your param
      var newUrl = url.href; 
      window.location = newUrl;
    }

    function goToUrl(url){
      window.location = url;
    }

    /*
        get form data and validate
        ajax-submit
        return status
    */
    function submitCreateCompany(){
      var creator_name = $('#creator_name').val();
      var creator_email= $('#creator_email').val();
      var company_name = $('#company_name').val();
      var company_id   = $('#country_id').val();
      var founded      = $('#founded').val();
      var founder_name = $('#founder_name').val();
      var description  = $('#description').val();
      var website      = $('#website').val();
      var facebook     = $('#facebook').val();
      var instagram    = $('#instagram').val();
      var linkedin     = $('#linkedin').val();
      var twitter      = $('#twitter').val();
      var industries   = $("#testtag").tagsinput('items');


    }

    $( "#createCompanyForm" ).submit(function( event ) {
      
      // Stop form from submitting normally
      event.preventDefault();
     
      // Get some values from elements on the page:
      var $form = $( this ),
        term = $form.find( "input[name='s']" ).val(),
        url = $form.attr( "action" );
     
      // Send the data using post
      var posting = $.post( url, { s: term } );
     
      // Put the results in a div
      posting.done(function( data ) {
        var content = $( data ).find( "#content" );
        $( "#result" ).empty().append( content );
      });
    });

    $('#testtag').tagsinput({
      confirmKeys: [13, 44],
      maxTags: 5,
      typeahead: {
        source: {!! $industries_tags !!}
      }
    });


</script>
@stop




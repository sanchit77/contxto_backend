@extends('layouts.web')
@section('content')

@section('title', @$title)
@section('description', @$description)
@section('image_link', @$image_link)
@section('imagename', @$imagename)
@section('url', @$url)
@section('keywords', '')
<div class="searchArea">
 <div class="container">
    <div class="row">
       <div class="col-12">
          <div class="seachbox">
             <input type="text" name="keyword" class="keyword" value="{{@$keyword}}" placeholder="Search Companies…">
             <button><img src="{{ asset('web/img/search.svg')}}"></button>
          </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-4 col-md-5 order-md-2 col-12 text-center">
          @include('elements.add-company-btn')
       </div>
       @include('elements.main-navbar')
    </div>
 </div>
</div>
<section id="detalsPage">
 <div class="contenArea">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <p class="foundComp">{{$foundComp}}</p>
             @if(@$industry->name || @$country->name)
            <h1 class="h1value">{{ @$industry->name}} @if(@$industry->name) companies @else Companies @endif @if(@$country->name) in @endif {{ @$city->name}}@if(@$city->name),@endif{{@$country->name}}</h1>
            @else
            <h1 class="h1value"></h1>
            @endif
          </div>

          <div class="col-lg-9 col-12">
             <div class="tab-content">
                <div id="companies" class="tab-pane active">
                   <div class="btn-group">
                      <div class="row">
                         <div class="col-lg-3">

                            <button class="filter-btn"><i class="mr-2"> <img src="{{ asset('web/img/filter.png')}}" alt="img"></i> <span style="line-height:normal;">Filter</span><span class="fliterCount"></span></button>
                         </div>
                         <input type="hidden" name="clickindus" id="clickindus" value="{{@$ind_name1}}">
                         <div class="col-lg-9">
                            <div class="ddGroup">
                               <div class="row">
                                  <div class="col-md-5 col-6">

                                     <select class="customSelectBx" id="countries1" name="countr">
                                        <option value="0">All Countries</option>
                                        @foreach(@$countries as $countrys)

                                            <option value="{{@$countrys->country_id}}">{{@$countrys->name}} ({{@$countrys->startups_count}})</option>

                                        @endforeach
                                     </select>
                                  </div>
                                  <div class="col-md-4 col-6">
                                    <select class="customSelectBx" id="cities">
                                        @include('elements.cities')
                                    </select>
                                  </div>
                               </div>
                            </div>
                            <select class="multiSelectBx multiSelect" id="industries" multiple="multiple">
                                @foreach($industries as $industry)
                                    <option value="{{$industry->industry_id}}"  data-section="Industry">{{$industry->name}}</option>
                                @endforeach
                            </select>
                         </div>
                      </div>
                   </div>
                   <section class="compnayListing">
                      <div class="listView row" id="tag_container">
                         <div class="row">
                             @include('elements.company-list')
                          </div>
                        </div>
                      </section>
                   <!-- <div class="listView" id="tag_container">

                   </div> -->

                </div>
             </div>
          </div>
          @include('elements.startup-funding-list')
       </div>
    </div>
 </div>
</section>

<script type="text/javascript">
// $(document).ready(function()
// {
// window.location.replace("https://www.contxto.com/companies/");
//     });
    /*$(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });*/

    $( ".keyword" ).keyup(function() {
      var country_id =$("#countries1").val();
      var city_id =$("#cities").val();

      var industries =$("#industries").val();
      if(industries==''){
        var industries = $( "#clickindus" ).val();
      }
      var keyword = $( ".keyword" ).val();
      if(keyword!=''){
        getData(1,keyword,country_id,city_id,industries);
      }else{
        getData(1,'',country_id,city_id,industries);
      }
    });

    $(document).ready(function()
    {

        $(document).on('click', '.pagination a',function(event)
        {
            event.preventDefault();

            $('li').removeClass('active');
            $(this).parent('li').addClass('active');

            var myurl = $(this).attr('href');
            var page=$(this).attr('href').split('page=')[1];
            var keyword=$(this).attr('href').split('keyword=')[1];
            var country_id=$(this).attr('href').split('country_id=')[1];
            var city_id=$(this).attr('href').split('city_id=')[1];
            var industries=$(this).attr('href').split('industries=')[1];

            getData(page,keyword,country_id,city_id,industries);
        });

    });

    function getData(page,keyword,country_id,city_id,industries){

        $.ajax(
        {
            url: '?page=' + page + '&keyword=' + keyword + '&country_id=' + country_id + '&city_id=' + city_id + '&industries=' + industries,
            type: "get",
            datatype: "json"
        }).done(function(data){

            $("#scountry").val(data.countryslug);
            $("#tag_container").empty().html(data.html);
            $(".foundComp").empty().text(data.foundComp);
            // alert(data.countryslug);
            if(data.countryslug && data.indusslug && data.cityslug){
console.log(1);
            window.history.pushState("Details", "Title", "{{ route('web-home') }}/"+data.countryslug+"/"+data.cityslug+"/"+data.indusslug);
            $(".h1value").html(data.indusname + ' companies in ' + data.cityname +','+ data.countryname);

            }
            else if(data.countryslug && !data.indusslug && !data.cityslug){
              console.log(8);
              window.history.pushState("Details", "Title", "{{ route('web-home') }}/"+data.countryslug+"/all/all");
              $(".h1value").html(' Companies in ' + data.countryname);
            }
            else if(data.countryslug && data.indusslug && !data.cityslug){
console.log(2);
              window.history.pushState("Details", "Title", "{{ route('web-home') }}/"+data.countryslug+"/all/"+data.indusslug);
              $(".h1value").html(data.indusname + ' companies in ' + data.countryname);
            }
            else if(data.indusslug && !data.countryslug && !data.cityslug){
console.log(3);
              window.history.pushState("Details", "Title", "{{ route('web-home') }}/all/all/"+data.indusslug);
              $(".h1value").html(data.indusname + ' companies ');
            }
            else if(data.cityslug && !data.indusslug){
console.log(4);
              window.history.pushState("Details", "Title", "{{ route('web-home') }}/"+data.countryslug+"/"+data.cityslug+"/all");
              $(".h1value").html(' Companies in ' + data.cityname +','+ data.countryname);
            }
            else if(!data.keyword){
console.log(5);
    console.log("abc");
     // $("#countries1").append("<option value=''>"+data.countryslug+"</option>");
              var pathArray = window.location.pathname.split('/');
              var acountry = pathArray[2];
              var acity = pathArray[3];
              var aindustry = pathArray[4];
              if(acountry){
              $.ajax({
                url: "/getid",
                type: "get",
                data:{
                  country:acountry,
                  city:acity,
                  industry:aindustry,
                },
                datatype: "json"
              }).done(function(data){

                  country=data[0].country_id;
                  if (country==undefined) {
                    country='0';
                  }
                  city=data[1].city_id;
                  if (city==undefined) {
                    city='0';
                  }
                console.log(city);
                  industry=data[2].industry_id;
                  if (industry==undefined) {
                    industry='';
                  }
                getData(page,keyword,country,city,industry);
              });

              // window.history.pushState("Details", "Title", "http://contxto.netsolutionindia.com/companies");
              $(".keyword").val('');
            }
            }
            else{

              window.history.pushState("Details", "Title", "https://database.contxto.com/companies/");
            }
var pathArray = window.location.pathname.split('/');
var country = pathArray[2];
if (country=='all') {
  country='';
}
var city = pathArray[3];
if (city=='all') {
  city='';
}
var industry = pathArray[4];
if (industry=='all') {
  industry='';
}
if(country && city && industry){
  console.log(1);
var name= industry + ' companies in ' + city +','+ country;
}
else if(country && !city && !industry){
  console.log(2);
var name=' Companies in ' + country;
}
else if(country && !city && industry){
  console.log(3);
var name=industry + ' companies in ' +country;
}
else if(!country && !city && industry){
  console.log(4);
var name=industry + ' companies ';
}
else if(city && !industry){
  console.log(5);
var name=' Companies in ' + city +','+ country;
}


            data = {
                           "@context": "https://schema.org",
                           "@type": "Organization",
                           "url": window.location.href,
                           "sameAs":[
                           "https://www.facebook.com/Contxto-495342720979440/",
                           "https://www.instagram.com/contxto_/",
                           "https://www.linkedin.com/company/contxto/",
                           "https://twitter.com/contxto_"
                           ],
                           "@id":"https://www.contxto.com/en/#organization",
                           "logo":"https://www.contxto.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-04-at-1.53.19-PM.jpeg",
                           "name": name
                        };

            //creating the script element and storing the JSON-LD

            var script = document.createElement('script');
            script.type = "application/ld+json";
            script.innerHTML = JSON.stringify(data);
            document.getElementsByTagName('head')[0].appendChild(script);

            //OR

            //storing the JSON-LD using ID
            $("#dynamicJSONLD").html(JSON.stringify(data));

            //location.hash = page;
        }).fail(function(jqXHR, ajaxOptions, thrownError){
              alert('No response from server');
        });
    }






    $( "#countries1" ).change(function() {
        var country_id =$( this ).val();
        console.log(country_id);
        var keyword = $( ".keyword" ).val();
        var industries = $( "#industries" ).val();
        if(industries==''){
          var industries = $( "#clickindus" ).val();
        }

        if(country_id==0){
          
              window.history.pushState("Details", "Title", "https://database.contxto.com/companies");
              $(".h1value").html('');
            getData(1,keyword,'','0',industries);
            $("#cities").empty().html('<option value="0">All Cities</option>');
        }else{
            getCities(country_id);
            getData(1,keyword,country_id,0,industries);
        }
    });

    function getCities(country_id){
        $.ajax(
        {
            url: "/cities"+'?country_id=' + country_id,
            type: "get",
            datatype: "html"
        }).done(function(data){
            $("#cities").empty().html(data);
        }).fail(function(jqXHR, ajaxOptions, thrownError){
              alert('No response from server');
        });
    }

    $( "#cities" ).change(function() {
        var city_id =$( this ).val();
        var country_id =$("#countries1").val();
        var industries =$("#industries").val();
        if(industries==''){
          var industries = $( "#clickindus" ).val();
        }
        var keyword = $( ".keyword" ).val();
        if(city_id==0){
            getData(1,keyword,country_id,0,industries);
        }else{
            getData(1,keyword,country_id,city_id,industries);
        }
    });



    $( "#industries" ).change(function() {
        var industries =$( this ).val();
        if(industries.length==0){
            $(".fliterCount").text(0).hide();
        }else{
            $(".fliterCount").text(industries.length).show();
        }

        var city_id =$("#cities").val();
        var country_id =$("#countries1").val();
        var keyword = $( ".keyword" ).val();
        getData(1,keyword,country_id,city_id,industries);
    });
</script>

 <script type="text/javascript">

  $('.tree-multiselect .selections').click(function(e) {
    $('.tree-multiselect .selections .section').toggle();
    e.stopPropagation();
  })
  $('.tree-multiselect .selections .section').click(function(e) {
    e.stopPropagation();
  })
  $(document).click(function(e) {

    $('.tree-multiselect .selections .section').addClass("collapsed");
    e.stopPropagation();
  })

$("#founder").click(function(){
  var rand=$("#rand").val();

  $("#randam").val(rand);
  $("#createComp").css("display", "none");
  $("#createComp").removeClass('show');
});

 function clickindustry($id){

   var industries =$id;
   $("#clickindus").val($id);


   var city_id =$("#cities").val();
   var country_id =$("#countries1").val();
   var keyword = $( ".keyword" ).val();
   getData(1,keyword,country_id,city_id,industries);
 }


</script>




@stop

<header>
   <div class="container">
      <div class="row align-items-center">
         <div class="col-xl-10 col-12">
            <nav class="navbar navbar-expand-lg   navbar-light">
               <a class="navbar-brand" href="https://contxto.com">
               <img src="{{ asset('WebAssets/img/logo.png')}}" alt="img">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse" id="collapsibleNavbar">
                  <ul class="navbar-nav">
                     <li class="nav-item">
                        <a class="nav-link" href="https://contxto.com">Home</a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Categories</a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Countries</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link active" href="#">Database</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.contxto.com/en/about-us/">About Us</a>
                     </li>
                     <!-- <li class="nav-item">
                        <a class="nav-link" href="#">ES</a>
                     </li> -->
                  </ul>
               </div>
            </nav>
         </div>
         <div class="col-xl-2 col-12">
            <div class="loginSignup">
               <ul>
                  <li>
                     <a class="paddingL0" href="javascript://">Sign Up</a>
                  </li>
                  <li class="border-right-0">
                     <a class="paddingR0" href="javascript://">Log In</a>
                  </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
</header>
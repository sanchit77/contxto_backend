<!DOCTYPE html>
<html lang="en">
   <head>
      <title>{{ isset($title)? $title : 'Contxto' }}</title>
       <meta name="title" content="@yield('title')" id="title">
       <meta name="keywords" content="@yield('keywords')">
       <meta name="description" content="@yield('description')">
       <meta property="og:title" content="@yield('title')">
       <meta property="og:type" content="website">
       <meta property="og:image" content="@yield('image_link')">
       <meta property="og:image:secure_url" content="@yield('image_link')">
       <meta property="og:image:alt" content="@yield('imagename')">
       <meta property="og:image:height" content="256">
       <meta property="og:image:width" content="256">
       <meta property="og:url" content="@yield('url')">
       <meta property="og:description" content="@yield('description')">
       <meta property="og:site_name" content="Contxto">
       <meta name="google-site-verification" content="-eVQQmOdxX7GXaAH_zKjngof2qxzP0qOah1Ng1aBlCA" />
      <meta charset="utf-8">
     <meta name="viewport" content="width=device-width, initial-scale=1">
      <link rel="shortcut icon" type="image/png" href="{{ asset('web/img/favicon.png')}}"/>
      <link rel="stylesheet" href="{{ asset('web/css/tagsinput.css')}}">
      <link rel="stylesheet" href="{{ asset('web/css/custom.css')}}">
      <link rel="stylesheet" href="{{ asset('web/css/chosen.css')}}">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:800" rel="stylesheet">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="{{ asset('web/css/jquery.tree-multiselect.min.css')}}">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link href="https://fonts.googleapis.com/css?family=Roboto:400,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:400,700" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,600,700,800" rel="stylesheet">
      <link href="https://fonts.googleapis.com/css?family=Raleway:400,600,700" rel="stylesheet">
      <!-- Sweet Alert -->
      <link href="{{ asset('web/css/sweetalert.css') }}" rel="stylesheet">

      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://ajax.aspnetcdn.com/ajax/jquery.validate/1.11.1/jquery.validate.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script src="{{ asset('web/js/jquery-ui.min.js')}}"></script>
      <script src="{{ asset('web/js/jquery.tree-multiselect.min.js')}}"></script>
      <script src="{{ asset('web/js/tagsinput.js')}}"></script>
      <script src="{{ asset('web/js/sweetalert.min.js') }}"></script>
      <script src="{{ asset('web/js/function.js')}}"></script>
      <script src="{{ asset('web/js/chosen.jquery.js')}}"></script>

      <link rel="stylesheet" href="{{ asset('new/css/tagsinput.css')}}">
      <link rel="stylesheet" href="{{ asset('new/css/responsive.css')}}">
      <link rel="stylesheet" href="{{ asset('new/css/custom.css')}}">
      <link href="https://fonts.googleapis.com/css?family=Montserrat:400,500,600,700,800&display=swap" rel="stylesheet">

      <script src="https://code.highcharts.com/highcharts.js"></script>
      <script src="https://code.highcharts.com/modules/exporting.js"></script>
   <script src="https://code.highcharts.com/modules/export-data.js"></script>
   <script src="https://code.highcharts.com/modules/variable-pie.js"></script>

      <style>
      #links{
        color:black;
        text-decoration:none;
      }
      </style>
   </head>
   <body class="body">
      <div class="loader-layer">
         <div class="roller"></div>
      </div>
      <!-- <div class="rating-bottom">Rated
<span itemscope="" itemtype="https://data-vocabulary.org/Review-aggregate">
<span itemprop="rating" itemscope="" itemtype="https://data-vocabulary.org/Rating">
<span itemprop="average" id="ratingValue">4.8</span> out of
<span itemprop="best">5.0</span> </span>by
<span itemprop="votes" id="totalClient">7285</span>+
<span itemprop="itemreviewed">contxto</span> clients on over
 <span itemprop="count">12350</span>+ projects.</span>
</div> -->
      @include('layouts.web_header')
      @yield('content')
      @include('layouts.web_footer')

   </body>
</html>
<script>
$("#website_name").focusout(function(){

  var company=$("#website_name").val();
  var company_name=$("#company_name").val();
  // $hunter= "https://api.hunter.io/v2/domain-search?domain="+ company +"&type=generic&limit=1&api_key=cd316ebd618cb66a223e01a27a19c13af9bba921";
    // var email=data.data.emails[0].value;
    //  console.log(data.data.emails[0].value);
     $.ajax({
        url:'checkCompany',
        type:'get',
        data:{

          company:company
        },
        datatype:'json'
     }).done(function(data){

     });

});

function investor(){
  $('html, body').animate({
         scrollTop: $(".ml-1").offset().top
     }, 1500);
}
</script>

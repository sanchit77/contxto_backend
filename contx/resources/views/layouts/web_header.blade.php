<header>
   <div class="container">
      <div class="row align-items-center">
         <div class="col-12">
            <nav class="navbar navbar-expand-lg navbar-light p-0">
               <a class="navbar-brand" href="https://contxto.com">
               <img style="max-width:222px;margin-left:-27px;" src="{{ asset('web/img/logo.png')}}" alt="img">
               </a>
               <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#collapsibleNavbar">
               <span class="navbar-toggler-icon"></span>
               </button>
               <div class="collapse navbar-collapse justify-content-end" id="collapsibleNavbar">
                  <ul class="navbar-nav">
                     <li class="nav-item">
                        <a class="nav-link" href="https://contxto.com">Home</a>
                     </li>
                     <li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Categories</a>
                        <ul class="dropdown-menu">
                            <li class="dropdown">
                                <a class="dropdown-item" href="#">Countries <i class="fa fa-angle-right float-right pt-1" aria-hidden="true"></i></a>
                                <ul class="dropdown-menu">
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/argentina/">Argentina</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/brazil/">Brazil</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/central-america/">Central America</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/chile/">Chile</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/colombia/">Colombia</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/mexico/">Mexico</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/peru/">Peru</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/spain/">Spain</a></li>
                                    <li><a class="dropdown-item" href="https://www.contxto.com/en/category/uruguay/">Uruguay</a></li>
                                </ul>
                            </li>
                            <li><a class="dropdown-item" href="https://www.contxto.com/en/category/how-tos/">How to’s</a></li>
                            <li><a class="dropdown-item" href="https://www.contxto.com/en/category/news/">News</a></li>
                            <li><a class="dropdown-item" href="https://www.contxto.com/en/category/interviews/">Interviews</a></li>
                            <li><a class="dropdown-item" href="https://www.contxto.com/en/category/listicles/">Listicles</a></li>
                            <li><a class="dropdown-item" href="https://www.contxto.com/en/category/market-map/">Market Map</a></li>
                        </ul>
                     </li>
                     <!--li class="nav-item dropdown">
                        <a class="nav-link dropdown-toggle" data-toggle="dropdown" href="#">Countries</a>
                     </li-->
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.contxto.com/en/investor-database/">Database</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.contxto.com/en/podcast/">Podcast</a>
                     </li>
                     <li class="nav-item">
                        <a class="nav-link" href="https://www.contxto.com/en/about-us/">About Us</a>
                     </li>
                  </ul>
               </div>
            </nav>
         </div>
         <!-- <div class="col-md-2 col-12">
            <div class="topRightNav">
               <ul>
                  <li class="mr-5">
                     <a href="javascript://">
                     <img src="{{ asset('web/img/language_flag.png')}}" alt="img">
                     </a>
                  </li>
                  <li >
                     <a data-toggle="collapse" data-target="#topSearch" href="javascript://">
                     <img src="{{ asset('web/img/dark_search.png')}}" alt="img">
                     </a>
                     <div id="topSearch" class="collapse">
                        <input type="search" name="">
                     </div>
                  </li>
               </ul>
            </div>
         </div> -->
      </div>
   </div>
</header>

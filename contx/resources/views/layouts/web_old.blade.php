<!DOCTYPE html>
<html lang="en">
   <head>
      <title>{{ isset($title)? $title : 'Contxto' }}</title>
      <meta charset="utf-8">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <meta name="description" content="{{ isset($description) ? $description : 'Contxto' }}">

      <link rel="stylesheet" href="{{ asset('WebAssets/css/responsive.css') }}">
      <link rel="stylesheet" href="{{ asset('WebAssets/css/custom.css') }}">
      <link rel="stylesheet" href="{{ asset('WebAssets/css/tagsinput.css') }}">
      <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css">
      <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
      <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/css/select2.min.css">
      <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.14.7/umd/popper.min.js"></script>
      <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/select2/4.0.6-rc.0/js/select2.min.js"></script>
      <script src="{{ asset('WebAssets/js/tagsinput.js') }}"></script>
      <script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-3-typeahead/4.0.2/bootstrap3-typeahead.min.js"></script>
   </head>
   <body>
      @include('layouts.web_header')
      @yield('content')
   </body>
</html>
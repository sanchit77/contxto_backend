<footer>
<div class="footerTop">
		   <div class="container">
	<div class="row">
		<div class="col-md-8 col-12">
			<div class="footerLeft">
				<div class="footerHeading">
					<span class="line"></span>
					<h4>Contxto</h4>
				</div>
				<p class="mb-1">Contxto is the prime website for Latin American tech and startup news. </p>
				<p class="mt-3">Providing content in both English and Spanish, Contxto covers all-things startups in Mexico, Brazil, Colombia, Argentina, Chile and all other Latin American countries.</p>
			</div>
		</div>
			<div class="col-md-4 col-12">
				<div class="footerHeading">
				<span class="line"></span>
				<h4>Popular Sections</h4>
			</div>
			<ul class="footerul">
				<li>
					<a target="_blank" href="https://www.contxto.com/es/category/fondeo/">Startup Fundings</a>
				</li>
				<li>
					<a target="_blank" href="https://www.contxto.com/es/category/mexico-es/">Mexico Startup News</a>
				</li>
				<li>
					<a target="_blank" href="https://www.contxto.com/es/category/brasil/">Brazil Startup News</a>
				</li>
				<li>
					<a target="_blank" href="https://www.contxto.com/es/category/colombia-es/">Colombia Startup News</a>
				</li>
				<li>
					<a target="_blank" href="https://www.contxto.com/es/category/argentina-es/">Argentina Startup News</a>
				</li>
				<li>
					<a target="_blank" href="https://www.contxto.com/es/category/chile-es/">Chile Startup News</a>
				</li>
			</ul>
		</div>
	</div>
</div>

</div>
<div class="bottomFooter">	
   <div class="container">
   	<div class="row align-items-center">
   		<div class="col-md-8 col-12">
      <p class="copyright">&copy;2019 Contxto All Rights Reserved. </p>
      </div>
      <div class="col-md-4 col-12 text-right">
      	<a class="aboutUs" href="https://www.contxto.com/en/about-us/">About Us</a>
      </div>
   </div>
</div>
</div>
</footer>
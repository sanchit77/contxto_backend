@extends('layouts.web')
@section('content')
@section('title', $company->name .' | Contxto')
@section('description', str_limit($company->description, $limit = 80, $end = ''))
@section('image_link', URL::to('/') . '/resize/'.$company->logo)
@section('imagename', $company->name .' Logo')
@section('url',url()->current())
    <section class="detailsPageHeader">
     <div class="container">
       <div class="row">
        <div class="col-md-5">
          @include('elements.detail-navbar')
        </div>
        <div class="col-md-7">
            <div class="seachbox">
               <input type="text" name="keyword" class="keyword" placeholder="Search Companies…">
               <button id="search-company"><img src="{{ asset('web/img/search.svg')}}"></button>
            </div>
        </div>
      </div>
      <div class="row">
        <div class="col-md-8">
          <div class="mainInfo">
           <div class="companyLogo">
              @if($company->logo)
              <img src="/resize/{{ $company->logo }}" alt="img">
              @else
              <div class="firstLetter">{{strtoupper(substr($company->name, 0, 1))}}</div>
              @endif
           </div>
           <div class="companyInfo">
              <h1 class="mb-3">{{ucfirst($company->name)}}</h1>
              @if($company->country)
               <div class="compnayLocation mb-3"> <i> <img src="{{ asset('web/img/location_white.png')}}" alt="img"> </i> <span class="pl-2">{{$company->country->name}}</span></div>
              @endif
              <div class="compnayWebsite"> <i> <img src="{{ asset('web/img/web.png')}}" alt="img"> </i> <span class="pl-1">{{rtrim($company->website,'/')}}</span> </div>
           </div>
          </div>
        </div>
         <div class="col-md-4">
              <div class="social-media">
           <ul>
              @if($company->linkedin)
              <li> <a href="{{$company->linkedin}}" target="_blank">
                 <img src="{{ asset('web/img/linkdIn.svg')}}" alt="img">
                 </a>
              </li>
              @endif
              @if($company->twitter)
              <li> <a href="{{$company->twitter}}" target="_blank">
                 <img src="{{ asset('web/img/twitter.svg')}}" alt="img">
                 </a>
              </li>
              @endif
              @if($company->instagram)
              <li> <a href="{{$company->instagram}}" target="_blank">
                 <img src="{{ asset('web/img/insta.svg')}}" alt="img">
                 </a>
              </li>
              @endif
              @if($company->facebook)
              <li> <a href="{{$company->facebook}}" target="_blank">
                 <img src="{{ asset('web/img/facebook.svg')}}" alt="img">
                 </a>
              </li>
              @endif
           </ul>
        </div>
         </div>
      </div>


     </div>
  </section>

  <section class="belowSection">
     <div class="container">
        <div class="companyDes">
           <ul>

              @foreach($company->startup_industries as $ind)
              <!-- <button id="industries1" onclick="clickindustry({{ $ind->industry_id }})">{{ $ind->name }}</button> -->
              <a href="{{url('companies/all/all',@$ind->slug)}}">
              <li>{{ $ind->name }}</li>
            </a>
              @endforeach
           </ul>
           <p>{{$company->description}}</p>
        </div>
        @if($amount_sum>0)
        <div class="disclsdFundBlk">
           <div class="blkHeader">
              <div class="row  align-items-center">
                 <div class="col-md-6 col-12">
                    <h2 class="heading">Total disclosed funding</h2>
                 </div>
                 <div class="col-md-6 col-12">
                    <div class="fundingAmt">$
                        @if(!$amount_sum)
                            0
                        @elseif($amount_sum > 1000000000000)
                            {{round(($amount_sum/1000000000000), 1)}}T
                        @elseif($amount_sum > 1000000000)
                            {{round(($amount_sum/1000000000), 1)}}B
                        @elseif($amount_sum > 1000000)
                            {{round(($amount_sum/1000000),1)}}M
                        @elseif ($amount_sum > 1000)
                            {{round(($amount_sum/1000), 1)}}K
                        @else
                            {{number_format($amount_sum)}}
                        @endif<span>USD</span>
                    </div>
                 </div>
              </div>
           </div>

           <div class="innerblk">
              <div class="headerBelow">

                 @foreach($company->startup_rounds_cal as $k=>$startup_round)
                     <div class="row">

                        <div class="col-md-6 col-12">
                           <h4><span style="color:black;text-decoration:none;">{!!$startup_round->investnameslinks!!}</span></h4>
                           <div class="date">{{ \Carbon\Carbon::parse($startup_round->dt)->format('d M Y')}}</div>
                        </div>
                        <div class="col-md-6 col-12 text-right">
                            @if($startup_round && ($startup_round->amount=="Undisclosed" || $startup_round->amount=="undisclosed"))
                              <div class="undisclosed"> <span><img src="{{ asset('web/img/undisclosed.svg')}}" alt="img"></span> Undisclosed</div>
                            @else
                              <div class="fundAmt">US${{$startup_round->amount}}</div>
                            @endif
                           <!--div class="newSource">(News source)</div-->
                        </div>
                     </div>
                     @if(!$loop->last)
                        <div class="clearfix"></div>
                     @endif
                 @endforeach
                 <!--div class="row">
                    <div class="col-md-6 col-12">
                       <h4>asdsadsasad</h4>
                       <div class="date">asdsadsa</div>
                    </div>
                    <div class="col-md-6 col-12 text-right">
                       <div class="undisclosed"> <span><img src="{{ asset('web/img/undisclosed.svg')}}" alt="img"></span> Undisclosed</div>
                    </div>
                 </div-->
              </div>

              <h4 class="heading2">Investors</h4>
              <div class="investorFounderBlk">
                 @forelse($company->investors as $k=>$i)

                    <div class="profileCard">
                        <div class="profilePic mr-3">
                        <!--    <img src="{{ asset('web/img/120*120.png')}}" alt="img"> -->
                           <div class="firstLetter">{{strtoupper(substr($i->investory_name, 0, 1))}}</div>
                        </div>
                        <div class="profileInfo">
                           <span class="name"><a href="{{url('/investor',$i->investor->slug)}}" style="color:black;text-decoration:none;">{{ $i->investory_name }}</a></span>
                        </div>
                     </div>
                 @empty
                 No Investor Found
                 @endforelse
              </div>
           </div>
        </div>
        @else
        No Fundings
        @endif
         <h4 class="heading2">Founders</h4>
          <div class="investorFounderBlk">
             @forelse($company->founders as $k=>$f)

                <div class="profileCard">
                  <div class="profilePic mr-3">
                  <!--    <img src="{{ asset('web/img/120*120.png')}}" alt="img"> -->
                  <div class="firstLetter">{{strtoupper(substr($f->name, 0, 1))}}</div>
                  </div>
                  <div class="profileInfo">
                     <span class="name"><a href="{{url('/founder',@$f->slug)}}" style="color:black;text-decoration:none;">{{ $f->name }}</a></span>
                  </div>
                </div>
             @empty
             No Founder Found
             @endforelse
          </div>
       <!--  <div class="submitReq">Is there anything wrong with listing?  <a href="javascript://">Submit request</a></div>
          <div class="compmayBtngrp">
           <button type="button" class="create mr-2" data-toggle="modal" data-target="#createComp">
           <i class="mr-2">+</i> <span>SUBMIT A COMPANY</span></button>
           <button type="button" class="create last ml-2" data-toggle="modal" data-target="#createComp">
           <i class="mr-2">+</i> <span>Submit a Investor</span></button>

        </div> -->

        <div class="heading3">
           <span class="line"></span>
           <h2 class="text"> Similar Companies</h2>
        </div>
        <div class="similarCompnies">
           <div class="row">
              @forelse($similar_companies as $similar)
                <div class="col-lg-4 col-md-6 col-12">
                     <div class="profileCard">
                        <div class="profilePic mr-3">
                           @if($similar->logo)
                          <a href="{{ route('web-company-details',['company_name' => $similar->slug ])}}"><img src="/resize/{{ $similar->logo }}/120" alt="img"></a>
                           @else
                           <!-- <img src="{{asset('web/img/ic_company_placeholder_full.png')}}" alt="img"> -->
                            <a href="{{ route('web-company-details',['company_name' => $similar->slug ])}}" class="firstLetter">{{strtoupper(substr($similar->name, 0, 1))}}</a>
                           @endif
                        </div>
                        <div class="profileInfo">
                           <a href="{{ route('web-company-details',['company_name' => $similar->slug ])}}" class="name">{{ $similar->name }}</a>
                           <!-- <span class="compName">11 – 50 employees</span> -->
                        </div>
                     </div>
                </div>
              @empty
             <div class="col-12"> No Company Found</div>
              @endforelse
           </div>
        </div>
        <div class="heading3">
           <span class="line"></span>
           <h2 class="text">News</h2>
        </div>
        <div class="news-listing">
            <div class="row">
                {{--@forelse($company->startup_news as $news)
                    <div class="col-lg-4 col-md-6 col-12">
                      <div class="newsBlk" id="{{$news->startup_news_id}}">
                         <div class="figBlock">
                            <a href="{{$news->url}}" target="_blank">
                            @if($news->image)
                            <img src="{{$news->image}}" alt="img">
                            @else
                            <img src="{{asset('web/img/news_placeholder.png')}}" alt="img">
                            @endif
                            </a>
                         </div>
                         <!--p class="newsHeading">Vegan and meat-free fast-food options are growing. Here's where to find them.</p-->
                          <div class="time"> <span class="greenIcon"> <span></span> </span>{{$news->created_at->diffForHumans()}}</div>
                         <div class="newsText">{{substr($news->description, 0, 150)}}...<a class="read-more" target="_blank" href="{{$news->url}}">Read More</a>
                         </div>
                      </div>
                   </div>
                @empty
               <div class="col-12">No News Found</div>
                @endforelse--}}
                @if($all_news)
                  @forelse($all_news as $news)
                      <div class="col-lg-4 col-md-6 col-12">
                        <div class="newsBlk">
                           <div class="figBlock">
                              <a href="{{$news->link}}" target="_blank">
                              @if($news->jetpack_featured_media_url)
                              <img src="{{$news->jetpack_featured_media_url}}" alt="img">
                              @else
                              <img src="{{asset('web/img/news_placeholder.png')}}" alt="img">
                              @endif
                              </a>
                               <p class="newsHeading">{!!$news->title->rendered!!}</p>
                           </div>

                           <div class="time"> <span class="greenIcon"> <span></span> </span>{{\Carbon\Carbon::parse(date('Y-m-d H:i:s', strtotime($news->date)))->diffForHumans()}}</div>
                           <div class="newsText">{!!$news->excerpt->rendered!!}<a class="read-more" target="_blank" href="{{$news->link}}">Read More</a>
                           </div>
                        </div>
                     </div>
                  @empty
                 <div class="col-12">No News Found</div>
                  @endforelse
                @else
                  <div class="col-12">No News Found</div>
                @endif
            </div>
        </div>
        {{--@if($company->startup_news_count>3)
        <div class="moreNews">
           <a href="javascript://" data-last-id="{{$last_news_id}}" data-more-count="{{$company->startup_news_count-3}}" id="load-more-news">+{{$company->startup_news_count-3}} more</a>
        </div>
        @endif--}}
         <!-- <div class="submitReq">Is there anything wrong with listing?  <a href="javascript://" id="submitRequestBtn">Submit request</a></div> -->
          <div class="compmayBtngrp">


        </div>
     </div>
  </section>

  @include('elements.new-company')
  @include('elements.new-investor')
  <script type="text/javascript">
    $("#load-more-news").click(function(){
        var last_news_id = $(this).attr('data-last-id');
        var more_count = $(this).attr('data-more-count');
        $.ajax({
            type: "GET",
            url: "{{route('web-company-news')}}?startup_id={{$company->startup_id}}&last_news_id="+last_news_id+"&more_count="+more_count,
            success: function(result) {
                $(".news-listing").append(result.news);
                $("#load-more-news").attr('data-last-id',result.last_news_id);
                $("#load-more-news").attr('data-more-count',result.more_count);
                if(result.more_count>0){
                    $('#load-more-news').text('+'+result.more_count+" more");
                }else{
                    $(".moreNews").remove();
                }
            },
            error: function(err){
                console.log(err);
            }
        });
    });
    $("#search-company").click(function(){
      redirectToCompanies();
    });

    $(document).keypress(function(event){
      var keycode = (event.keyCode ? event.keyCode : event.which);
      if(keycode == '13'){
        redirectToCompanies();
      }
    });
    function redirectToCompanies(){
      var keyword = $('.keyword').val();
      $.ajax({
        url: "/getslug",
        type: "get",
        data:{
          keyword:keyword,
        },
        datatype: "json"
      }).done(function(data){
        var key=data.slug.slug;
        var key2=data.slug;
        if(key2!='1'){
          window.location.href="{{route('web-company-details')}}/"+key;
        }else{
          $('.keyword').focus();
            alert('No Company found');
        }
      });

    }


    data = {
                   "@context": "https://schema.org",
                   "@type": "Organization",
                   "url": window.location.href,
                   "sameAs":[
                   "{{$company->facebook}}",
                   "{{$company->instagram}}",
                   "{{$company->linkedin}}",
                   "{{$company->twitter}}"
                   ],
                   "@id":"https://www.contxto.com/en/companies",
                   "logo":"http://contxto.netsolutionindia.com/resize/{{$company->logo}}",
                   "name": "{{$company->name}}"
                };

    //creating the script element and storing the JSON-LD

    var script = document.createElement('script');
    script.type = "application/ld+json";
    script.innerHTML = JSON.stringify(data);
    document.getElementsByTagName('head')[0].appendChild(script);

    //OR

    //storing the JSON-LD using ID
    $("#dynamicJSONLD").html(JSON.stringify(data));
  </script>

  <!-- <script type="application/ld+json">
  {"@context":"https://schema.org",
  "@type":"Organization",
  "url":"{{$company->name}}",
  "sameAs":[
  "https://www.facebook.com/Contxto-495342720979440/",
  "https://www.instagram.com/contxto_/",
  "https://www.linkedin.com/company/contxto/",
  "https://twitter.com/contxto_"
  ],
  "@id":"https://www.contxto.com/en/#organization",
  "name":"{{$company->name}}",
  "logo":"https://www.contxto.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-04-at-1.53.19-PM.jpeg"}
  </script> -->
@stop

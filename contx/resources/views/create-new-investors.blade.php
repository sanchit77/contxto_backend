@extends('layouts.web')
@section('content')
@include('elements.new-founder')
@include('elements.new-investor')
<style>
.chosen-search-input
{
  width:0px;
}
</style>
<div class="searchArea">
 <div class="container">
    <div class="row">
       <div class="col-12">
          <div class="seachbox">
             <input type="text" name="keyword" class="keyword" value="{{@$keyword}}" placeholder="Search Companies…">
             <button onClick="searchHome()"><img src="{{ asset('web/img/search.svg')}}"></button>
          </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-4 col-md-5 order-md-2 col-12 text-center">
          @include('elements.add-company-btn')
       </div>
       @include('elements.main-navbar')
    </div>
 </div>
</div>

<section class="formMain">
               <div class="container">
                  <div class="form-header">
                     <h4>Investor Personal Information</h4>
                     <p>Add your information below.</p>
                  </div>
                  <form action="" id="new-investor1" method="POST" enctype="multipart/form-data">
                     {{csrf_field()}}
                     <!-- <input type="hidden" name="startupid" value="{{@$company->startup_id}}"> -->
                  <div class="uploadSection" >
                     <div class="inputFiles">
                      <img class="company-logo" src="#" />
                     </div>
                     <div class="inputFilesActions">
                        <p>Add Your Profile Image</p>
                        <div class="row">
                           <div class="col-md-6 col-7">
                              <div class="uploadImg">
                                 <input type='file' name="investor-image" id="company-logo-input"  accept="image/*" onchange="readURL(this);" />
                                 <span>Upload Image</span>
                              </div>
                           </div>
                           <div class="col-md-6 col-5">
                              <a class="btn btn-remove remove-logo" id="remove-logo" style="display:none">Remove</a>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="custom-form-main">
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Investor Name</label>
                              <input class="form-control" type="text" id="name" name="investor_name" placeholder="Enter your full name...">
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Investor Email</label>
                              <input class="form-control" type="text" name="investor_email" id="investor_email" placeholder="Enter your email..." onkeyup="checkinvestor()">
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Country</label>
                              <select class="form-control" name="country_id">
                                  <option value="">Select Country</option>
                                 @foreach($countries as $country)
                                 <option value="{{$country->country_id}}">{{$country->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Website</label>
                              <input class="form-control" type="url" name="website" id="website" placeholder="Enter your Website...">
                           </div>
                        </div>

                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group">
                              <label>Startups</label>
                              <select class="form-control" name="startupid">
                                <option value="">Select Startup</option>
                                 @foreach($startups as $startup)
                                 <option value="{{$startup->startup_id}}">{{$startup->name}}</option>
                                 @endforeach
                              </select>
                           </div>
                        </div>


                     </div>

                     <div class="row">
                        <div class="col-12">
                           <div class="form-group">
                              <label>Description</label>
                              <textarea  class="form-control" placeholder="Description…" id="desc" name="description"></textarea>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-12">
                           <div class="form-header mt-5">
                              <h4>Social Profiles Information</h4>
                              <p>Add your social profiles information below.</p>
                           </div>
                        </div>
                     </div>
                     <div class="row">
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Facebook</label>
                              <input class="form-control" type="text" id="facebook" name="facebook" >
                              <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Twitter</label>
                              <input class="form-control" type="text" id="twitter" name="twitter" >
                              <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Linked In</label>
                              <input class="form-control" type="text" id="linked" name="linkedin" >
                              <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                           </div>
                        </div>
                        <div class="col-md-6 col-12">
                           <div class="form-group formGrpIcn">
                              <label>Instagram</label>
                              <input class="form-control" type="text" id="instagram" name="instagram" >
                              <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                           </div>
                        </div>
                     </div>
                  </div>
                  <div class="customBtn">
                     <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                     <button type="submit" class="btn btn-primary ml-4">Submit</button>
                  </div>
                  </form>
               </div>
            </section>

            <script>
            $(document).ready(function(){
              $('#keyword').on('change keyup',function(e) {
                  if(e.keyCode == 13){
                    // var url = "{{route('web-home')}}";
                    // var keyword="{{app('request')->input('keyword')}}";
                    // var page = 1;
                    // var keyword = $('#keyword').val();
                    // url += "?page="+page+"&keyword="+keyword;
                    // window.location = url;

                    var keyword = $('.keyword').val();
                    $.ajax({
                      url: "/getslug",
                      type: "get",
                      data:{
                        keyword:keyword,
                      },
                      datatype: "json"
                    }).done(function(data){
                      var key=data.slug.slug;
                      var key2=data.slug;
                      if(key2!='1'){
                        window.location.href="{{route('web-company-details')}}/"+key;
                      }else{
                        $('#keyword').focus();
                        alert('No Company found');
                      }
                    });
                  }
               });
            });

            function searchHome(){
                 // var url = "{{route('web-home')}}";
                 // var keyword="app('request')->input('keyword')";
                 // var page = 1;
                 // var keyword = $('#keyword').val();
                 // url += "?page="+page+"&keyword="+keyword;
                 // window.location = url;

                 var keyword = $('.keyword').val();
                 $.ajax({
                   url: "/getslug",
                   type: "get",
                   data:{
                     keyword:keyword,
                   },
                   datatype: "json"
                 }).done(function(data){
                   var key=data.slug.slug;
                   var key2=data.slug;
                   if(key2!='1'){
                     window.location.href="{{route('web-company-details')}}/"+key;
                   }else{
                     $('#keyword').focus();
                       alert('No Company found');
                   }
                 });
             }
            </script>
@endsection

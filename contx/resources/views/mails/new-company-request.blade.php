<!doctype html>
<html xmlns="http://www.w3.org/1999/xhtml" xmlns:v="urn:schemas-microsoft-com:vml" xmlns:o="urn:schemas-microsoft-com:office:office">
   <head>
      <!-- NAME: HERO IMAGE -->
      <!--[if gte mso 15]>
      <xml>
         <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
         </o:OfficeDocumentSettings>
      </xml>
      <![endif]-->
      <meta charset="UTF-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">
      <link href="https://fonts.googleapis.com/css?family=Open+Sans:300,400,600,700" rel="stylesheet">
      <title>
         New Company Request
      </title>
      <head>
         <style type="text/css">

            @media (max-width:649px) {
            .full-width {
            width: 100% !important;
            }
            }
         </style>
   </head>
   <body style="padding-left:15px; padding-right:15px;" leftmargin="0" marginwidth="0" topmargin="0" marginheight="0" offset="0">
      <table style="width:100%;"  border="0" cellpadding="0" cellspacing="0">
         <tr>
            <td align="center" style="padding-top:30px;">
               <table class="full-width" style="max-width:650px; width:100%; font-family: 'Open Sans', sans-serif; font-size: 15px;  line-height:24px; margin:0;
            padding:0;     border: solid 1px #eee;" border="0" cellpadding="0" cellspacing="0">
                  <tr>
                     <td style="text-align: center; padding-top:45px; ">
                        <a href="javascript://">
                        <img  src="logo.png" alt="Logo" />
                        </a>
                     </td>
                  </tr>

                   <tr>
                      <td style="text-align:center; padding-top:50px; padding-right:30px; padding-left:30px;">{{ @$name }},</td>
                   </tr>

                     <tr>
                      <td style="text-align:center; padding-right:30px; padding-left:30px;">{{ @$email }}</td>
                   </tr>

                      <tr>
                      <td style="text-align:center; padding-bottom:15px; font-weight:600; font-size:18px; padding-bottom:50px; padding-right:30px; padding-left:30px;">{{ @$company->name }}, {{ @$country->name }}</td>
                   </tr>


                  <tr>
                     <td style="text-align:center;   padding-top:15px; padding-bottom:50px; padding-right:30px; padding-left:30px;">{{ @$company->description }}</td>
                  </tr>

                  <tr>
                     <td style="font-size:16px; padding-right:30px; padding-left:30px;">
                           {{ @$founder_name }}

                     </td>
                  </tr>
                   <tr>
                     <td style="color:#777; font-size:13px; padding-right:30px; padding-left:30px;">
                          Founded In {{ @$company->founded }}

                     </td>
                  </tr>

                    <tr>
                     <td style="color:#777; font-size:13px; padding-right:30px; padding-left:30px;">
                       {{ @$industries }}

                     </td>
                  </tr>
                   <tr>
                      <td style="padding-bottom:30px; padding-right:30px; padding-left:30px;"> <a style="color:#000; font-weight:600; font-size:13px;" href="">{{ @$company->website }}</a> </td>
                   </tr>





                  <tr>
                     <td align="center" style="padding-bottom:30px; background-color:#eee; padding-right:30px; padding-left:30px;">
                        <table style="padding-top:30px;" border="0" cellpadding="0" cellspacing="0">
                           <tr>
                              <td style="padding:0 10px"> <a href="javascript://">
                                 <img   src="{{ asset('WebAssets/img/fb.png') }}" alt="Logo" />
                                 </a>
                              </td>
                              <td style="padding:0 10px"> <a href="javascript://">
                                 <img   src="{{ asset('WebAssets/img/twt.png') }}" alt="Logo" />
                                 </a>
                              </td>
                              <td style="padding:0 10px"> <a href="javascript://">
                                 <img   src="{{ asset('WebAssets/img/in.png') }}" alt="Logo" />
                                 </a>
                              </td>
                              <td  style="padding:0 10px"> <a href="javascript://">
                                 <img   src="{{ asset('WebAssets/img/insta.png') }}" alt="Logo" />
                                 </a>
                              </td>
                           </tr>

                        </table>
                     </td>
                  </tr>
                  <tr>
                     <td align="center" style="font-size:13px; padding-top:15px; background-color:#000; color:#fff; padding-right:30px; padding-left:30px; ">Copyright  &copy; 2019-2020 The Company (US), LLC.</td>
                  </tr>
                  <tr>
                     <td align="center" style="font-size:13px; background-color:#000; color:#fff; padding-bottom:15px; padding-right:30px; padding-left:30px;"> All rights reserved. Confidential and Proprietary.</td>
                  </tr>
               </table>
            </td>
         </tr>
      </table>
   </body>
   </body>
</html>

@extends('Admin.layout')

@section('title') 
     Password Update
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                    <div class="col-lg-10">
                        <h2> Password Update</h2>
                        <ol class="breadcrumb">
                            <li>
                                <a href="{{route('admin_dashboard')}}">Dashboard</a>
                            </li>
                            <li>
                                <a href="{{route('apassword_update_get')}}"><b> Password Update</b></a>
                            </li>                        
                        </ol>
                    </div>
                    <div class="col-lg-2">

                    </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

                <br>
                <form method="post" action="{{route('apassword_update_post')}}" enctype="multipart/form-data" id="passwordForm">

                        <input type="hidden" name="_token" value="{{ csrf_token() }}">

                    <div class="row">
                        <div class="form-group">
                            
                           <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
                                <label>Old Password</label>
                    <input type="password" placeholder="Old Password" autocomplete="off" class="form-control" required name="old_password" autofocus="on" id="old_password">
                            </div>

                        </div>
                    </div>

                <br>

                    <div class="row">
                        <div class="form-group">
                           
                            <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
                                <label>Password</label>
                    <input type="password" placeholder="Password" autocomplete="off" class="form-control" required name="password" id="password">
                            </div>

                        </div>
                    </div>
                <br>

                    <div class="row">
                        <div class="form-group">
                           
                            <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
                            <label>Password Confirmation</label>
                    <input type="password" placeholder="Paswword Confirmation" autocomplete="off" class="form-control" required name="password_confirmation" id="password_confirmation">
                            </div> 

                        </div>
                    </div>

    <br><br><br>

                    <div class="row">
                        <div class="form-group">
                            <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>                    
                                {!! Form::submit('Update Password', ['class' => 'btn btn-success']) !!}
                                {!! Form::reset('Reset', ['class' => 'btn btn-primary']) !!}
                            </div>
                        </div>
                    </div>
        
                </form>

            </div>

        </div>

    </div>

    </div>

    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/additional-methods.min.js"></script>

<script type="text/javascript">

        $("#dashboard").addClass("active");

        $("#passwordForm").validate({
            rules: {
                old_password: "required",
                password: {
                  required: true,
                  minlength: 6,
                  //notEqual: "#old_password"
                },
                password_confirmation: {
                    equalTo: "#password"
                }
            },
            messages: {
                old_password: "Please provide your old password",
                password: {
                    required: "Please provide new password",
                    minlength: "Your new password must be at least 6 characters long",
                    //notEqual: "New password cannot be same as old password"
                },
                password_confirmation: {
                    required: "Please provide the new password confirmation",
                    equalTo: "Please enter the same password confirmation as password"
                }
            }
        });

</script>


@stop




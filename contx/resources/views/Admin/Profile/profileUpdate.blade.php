@extends('Admin.layout')

@section('title') 
     Profile Update
@stop

    @section('content')

    <div class="wrapper wrapper-content animated fadeInRight">

    <div class="row">
        <div class="col-xs-12">
 
            <div class="row wrapper border-bottom white-bg page-heading">
                <div class="col-lg-10">
                    <h2> Profile Update</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="{{route('aprofile_update_get')}}"><b> Profile Update</b></a>
                        </li>                        
                    </ol>
                </div>
                <div class="col-lg-2">
                </div>
            </div>

            <div class="wrapper wrapper-content animated fadeInRight">

        <br>
            <form method="post" action="{{route('aprofile_update_post')}}" enctype="multipart/form-data" id="profileForm">

                <input type="hidden" name="_token" value="{{ csrf_token() }}">

                <div class="row">
                    <div class="form-group">

                        <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
                            <label>Name</label>
                <input type="text" placeholder="Name" autocomplete="off" class="form-control" required name="name" value="{!! $admin->name !!}" autofocus="on" id="name">
                        </div>

                    </div>
                </div>

                <br>

                <div class="row">
                    <div class="form-group">
                        
                        <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
                            <label>Job</label>
                <input type="text" placeholder="Job" autocomplete="off" class="form-control" required name="job" value="{!! $admin->job !!}" id="job">
                        </div>

                    </div>
                </div>
                
                <br>

                <div class="row">
                    <div class="form-group">            
                            <div class='col-lg-4 col-lg-offset-4 col-md-4 col-md-offset-4 col-sm-4 col-sm-offset-4'>
                                <label>Profile Pic</label>
                                <input type="file" name="profile_pic" class="form-control" accept="image/*">
                            </div>
                    </div>
                </div>

            <br><br><br>

                <div class="row">
                    <div class="form-group">
                        <div class='col-lg-5 col-lg-offset-5 col-md-5 col-md-offset-5 col-sm-5 col-sm-offset-5'>
                            {!! Form::submit('Update Profile', ['class' => 'btn btn-success']) !!}
                        </div>
                    </div>
                </div>
            </form>
        </div>
        </div>

    </div>
</div>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery-validate/1.17.0/jquery.validate.min.js"></script>

<script type="text/javascript">

        $("#dashboard").addClass("active");

        $("#profileForm").validate({
            rules: {
                name: {
                    required: true,
                    maxlength: 255,
                },
                job: {
                    required: true,
                    maxlength: 255,
                },
                image: {
                    //required: true,
                    accept: "image/*"
                }
            },
            messages: {
                name: {
                    required: "Please provide the name"
                },
                phone_number: {
                    required: "Please provide the phone number"
                },
                job: {
                    required: "Please provide the job"
                },
                image:{
                    accept: "Only Image is allowed"
                }
            }
        });

</script>


@stop




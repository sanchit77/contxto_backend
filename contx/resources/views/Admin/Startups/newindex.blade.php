@extends('Admin.layout')

@section('title') 
    Startups
@stop

@section('content')

<style>

.dataTables_filter{display:none !important}
        th{width: auto !important;}
        .selected_Button{display:none;}
        table{    border: 1px solid #e7eaec;}
        tr td{font-size: 13px}

        .toolbar {
    float:left;
}


tfoot {
     display: table-header-group;
}

.uif-hidden {
    display: none;
}
button.sneaky {
    border: 1px solid transparent;
    border-bottom: 1px dashed blue;
    display: inline-block;
    padding: 2px;
    background: transparent;
    /* width:100% !important; */
}
button.sneaky:hover {
    border: 1px solid #ccc;
    background: #ffffcc;
}
input, select, textarea {
    position: relative;
}


    </style>

      <!-- <link rel="stylesheet" href="https://cdn.datatables.net/1.10.11/css/jquery.dataTables.min.css"/> 
      <link rel="stylesheet" href="https://cdn.datatables.net/buttons/1.1.2/css/buttons.dataTables.min.css"/> 
      <link rel="stylesheet" href="https://cdn.datatables.net/select/1.1.2/css/select.dataTables.min.css"/>
      <link rel="stylesheet" href="https://cdn.datatables.net/responsive/2.0.2/css/responsive.dataTables.min.css"/> 

      <script src="http://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script> -->

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

<script src="https://cdn.datatables.net/fixedheader/3.1.5/js/dataTables.fixedHeader.min.js"></script>



    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
                <h2>Startups</h2>
                    <ol class="breadcrumb">
                        <li>
                            <a href="{{route('admin_dashboard')}}">Dashboard</a>
                        </li>
                        <li>
                            <a href="#"><b>Startups</b></a>
                        </li>
                    </ol>
        </div>
    </div>

    <br>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="footable table table-striped display" id="example1">
                    <thead>
                        @include("Admin.Partials.Tables.startupColsRow")
                        @include("Admin.Partials.Tables.startupColsRow")
                    </thead>
                    
                    <tbody>

                    </tbody>

                    <!-- <tfoot>
                        @include("Admin.Partials.Tables.startupColsRow")
                    </tfoot> -->

                </table>

                            </div>
                        </div>
                    </div>

    <br><br>

    <script>

        $("#startups").addClass("active");
        var dataTable;
        var order = 0;

        var allColumns = [
            { "data": "startup_id" },
            { "data": "name" },
            { "data": "valuation" },
            { "data": "industry_id" },
            { "data": "startup_id" },
            { "data": "country_id" },
            { "data": "city_id" },
            { "data": "description" },
            { "data": "startup_id" },
            { "data": "country_id" },
            { "data": "city_id" },
            { "data": "industry_id" },
            { "data": "startup_id" },
            { "data": "website" },
            { "data": "facebook" },
            { "data": "twitter" },
            { "data": "linkedin" },
            { "data": "instagram" },
            { "data": "ipo_symbol" },
            { "data": "logo" }
        ];

    // $('#example1 tfoot th').each( function (i) {
    //     var title = $(this).text();
    //     //console.log(i,i,i, title);
    //     $(this).html( '<input type="text" placeholder="Search '+title+'" />' );
    // });

    $('#example1 thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" data-column='+i+' placeholder="Search '+title+'" class="search-input-text"/>' );

    });

    ////////////        Append Save or Cancel Btn   //////////////////
    function appendButtons(el) {

        var con = $('#'+el).parent().append('<div><button class="save btn btn-info btn-circle" type="button"><i class="fa fa-check"></i></button><button class="cancel btn btn-warning btn-circle" type="button"><i class="fa fa-times"></i></button></div>');

        console.log('#'+el);
    }

    $('#example1').on('click', 'button.save', function () {

        console.log('Saved');

        var nEl = $(this).parent().find('input,select,textarea').val();
        $(this).parent().find('.sneaky').text(nEl).show().focus();
        $(this).parent().find('input,select,textarea').hide();
        //$(this).parent().effect('highlight', {}, 3000);
        $(this).parent().find('.cancel').remove();
        $(this).parent().find('.save').remove();
    });

    $('#example1').on('click', 'button.cancel', function () {

        console.log('Cancelled');

        $(this).parent().find('.sneaky').show().focus();
        $(this).parent().find('input,select,textarea').hide();
        $(this).parent().find('.save').remove();
        $(this).parent().find('.cancel').remove();
    });


    $('#example1').on('click', 'button.sneaky', function() {

        var el = $(this).attr('id').slice(0, -1), val = $(this).text();

        $(this).parent().find('input,select,texarea').val(val).show().focus();
        appendButtons(el);
        $(this).hide();

        console.log($(this).attr('id'), el, val);
    });




    $(document).ready( function () {

        /////////   Get Data    /////////////
    getJsonData = (route) => {

    dataTable = $('#example1').DataTable({

        initComplete: function ()
            {
            },

            // "aoColumns": [
            //     {"column_number": 0, "bSearchable":true}, // job_id
            // ],


        //"searching": false,
        pagingType: "full_numbers",
        "sDom": '<"table_body" <"table-responsive" t> ><"pagination_area pagination-sec clearfix" <"selected_Button pull-left " > <"pagination-right" flip> >',
        "processing": true,
        "serverSide": true,
        "pageLength":10,
        "info":true,
        //"order": [[order,"desc" ]],
                //"lengthMenu": [[10, 20, 30, 40, 50], [10 , 20, 30, 40, 50]],
        "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ){
            $('td', nRow).addClass( "id_class" );
        },
    
        "ajax":{
            url : route, // json datasource
            type: "post",  // method  , by default get
            dataSrc: 'startups',
            error: function(xmlhttp){  // error handling
                if(xmlhttp.status == 403)
                    {
                        window.location.reload();
                    }
                $("#example").append('<tbody class="example-error"><tr><th colspan="10">No data found in the server</th></tr></tbody>');
                $("#example_processing").css("display","none");
                }
        },
        "columns": allColumns,
        "orderCellsTop": true,
        //"fixedHeader": true
    });

/////////////       Search Boxes        ///////////////////////////


// Apply the search
   $('#example1 thead tr:eq(1) th').each( function (i) {

        $( 'input', this ).on( 'keyup change', function () {
            if ( dataTable.column(i).search() !== this.value ) {

                dataTable
                    .column(i)
                    .search(this.value)
                    .draw();

                console.log(dataTable.searchCols);
            }
        });

    });




    // // Apply the search
    // dataTable.columns().every( function () {
    //     var that = this;
 
    //     $( 'input', this.footer() ).on( 'keyup change', function () {
    //         if ( that.search() !== this.value ) {

    //             dataTable.columns(i)['search']['value'] = this.value;

    //             that
    //                 .search( this.value )
    //                 .draw();
    //         }
    //     } );
    // } );



//    $('#example1 thead tr').clone(true).appendTo( '#example1 thead' );

//    $('#example1 thead tr:eq(1) th').each( function (i) {

//         var title = $(this).text();
//         $(this).html( '<input type="text" data-column='+i+' placeholder="Search '+title+'" class="search-input-text"/>' );

//         $( 'input', this ).on( 'keyup change', function () {
//             if ( dataTable.column(i).search() !== this.value ) {
//                 dataTable
//                     .column(i)
//                     .search( this.value )
//                     .draw();
//             }
//         });

//     });

//     $('.search-input-text').on( 'keyup click' ,function () {   // for text boxes

//         var i = $(this).attr('data-column');  // getting column index
//         var v = $(this).val();  // getting search input value
//             //dataTable.columns(i).search.value = v;
//         //dataTable.columns(i)['search']['value'] = v;
//         let tt = dataTable.column(i).search(this.value).draw();

//         console.log(i, v, tt);

//             //columns[0][search][value]

//         } );

    /////////////       Search Boxes        ///////////////////////////

    appendFirstRow();

    }
    /////////   Get Data    /////////////

    appendFirstRow = () => {

        var counter = 0;

        var newRow = "<tr><td>row 3, cell 1</td><td>row 3, cell 2</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td><td>row 3, cell 1</td></tr>";
        //var table = $('table').DataTable();
        dataTable.row.add($(newRow )).draw();

        // dataTable.row.add({
     
        //     "startup_id": "<input type='text' name='startupId'>",
        //     "name": "",
        //     "valuation": "",
        //     "industry_id": "",
        //     "logo": "",
        //     "country_id": "",
        //     "startup_id": "",
        //     "city_id": "",
        //     "startup_id": "",
        //     "startup_id": "",
        //     // { "data": "startup_id" },
        //     // { "data": "country_id" },
        //     // { "data": "city_id" },
        //     // { "data": "industry_id" },
        //     // { "data": "startup_id" },
        //     // { "data": "country_id" },
        //     // { "data": "city_id" },
        //     // { "data": "industry_id" },
        //     // { "data": "startup_id" },
        //     // { "data": "country_id" },
        //     // { "data": "city_id" },
        //     // { "data": "industry_id" },
        //     // { "data": "startup_id" },
        //     // { "data": "country_id" },
        //     // { "data": "city_id" },
        //     // { "data": "industry_id" },
        //     // { "data": "name" },
        //     // { "data": "logo" },
        //     // { "data": "name" },
        //     // { "data": "logo" }

        //     // counter +'.1',
        //     // counter +'.2',
        //     // counter +'.3',
        //     // counter +'.4',
        //     // counter +'.5',
        //     // counter +'.1',
        //     // counter +'.2',
        //     // counter +'.3',
        //     // counter +'.4',
        //     // counter +'.5',
        //     // counter +'.1',
        //     // counter +'.2',
        //     // counter +'.3',
        //     // counter +'.4',
        //     // counter +'.5',
        //     // counter +'.1',
        //     // counter +'.2',
        //     // counter +'.3',
        //     // counter +'.4',
        //     // counter +'.5'
        // }).draw( false );


    }


    getJsonData('{{route("startups_data")}}');
        
    });

    

    </script>

@stop




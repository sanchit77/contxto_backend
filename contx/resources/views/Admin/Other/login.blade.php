<!DOCTYPE html>
<html lang="en">
<head>
	<title>{{env('APP_NAME')}} - Admin Sign In</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">

    <link rel="shortcut icon" href="{{ URL::asset('AdminAssets/Images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('AdminAssets/Images/favicon.ico') }}" type="image/x-icon">
<!--===============================================================================================-->	

    <script src="{{ URL::asset('AdminAssets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/bootstrap.min.js') }}"></script>
    <link href="{{ URL::asset('AdminAssets/css/bootstrap.min.css') }}" rel="stylesheet">

<!--===============================================================================================-->	
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('AdminAssets/Login/fonts/iconic/css/material-design-iconic-font.min.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('AdminAssets/Login/css/util.css') }}">
	<link rel="stylesheet" type="text/css" href="{{ URL::asset('AdminAssets/Login/css/main.css') }}">
<!--===============================================================================================-->

</head>
<body>	
	
	<div class="container-login100" style="background-image: url('{{ URL::asset('AdminAssets/Login/images/bg-01.jpg') }}');">

		<div class="wrap-login100 p-l-35 p-r-35 p-t-50 p-b-20" style="padding-top: 25px !important;">
			<form class="login100-form validate-form" method="post" action="{{route('admin_login_post')}}">

				<input type="hidden" name="_token" value="{{ csrf_token() }}">
				<input type="hidden" name="timezone" value="" id="timezone">

				<span class="login100-form-title p-b-37">
					<center>
						<img src="{{ URL::asset('AdminAssets/Images/logo.jpg') }}" style="max-height: 120px; max-height: 120px; ">
					</center>
			</span>

				<div class="wrap-input100 validate-input m-b-20" data-validate="Enter Email">
					<input class="input100" type="email" name="email" required autocomplete name="username" placeholder="Email" value="{{Request::old('email')}}" autofocus="on">
					<span class="focus-input100"></span>
				</div>

				<div class="wrap-input100 validate-input m-b-25" data-validate = "Enter password">
					<input class="input100" type="password" name="password" placeholder="Password">
					<span class="focus-input100"></span>
				</div>

			        @foreach($errors->all() as $error)
			            <div class="alert alert-dismissable alert-danger">
			                {!! $error !!}
			            </div>
			        @endforeach

			        @if (session('status'))
			            <div class="alert alert-success">
			                {{ session('status') }}
			            </div>
			        @endif

				<div class="container-login100-form-btn">
					<button class="login100-form-btn">
						Sign In
					</button>
				</div>

			</form>
			
		</div>
	</div>	

<!--===============================================================================================-->
	<script src="{{ URL::asset('AdminAssets/Login/js/main.js') }}"></script>
	<script src="{{ URL::asset('AdminAssets/Login/vendor/bootstrap/js/popper.js') }}"></script>

	<script type="text/javascript">
		
		document.getElementById('timezone').value = Intl.DateTimeFormat().resolvedOptions().timeZone;

	</script>

</body>
</html>
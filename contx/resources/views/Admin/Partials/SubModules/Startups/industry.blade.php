<div class="modal inmodal modal-industries modal-md" id="modal-industries" role="dialog" style="overflow:hidden;">
    <div class="modal-dialog">
        <div class="modal-content">
            
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal">
                    <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                </button>
                <h4 class="modal-title">Update Industries</h4>
            </div>
            
            <div class="modal-body">
                <div class="row">
                    <div id="country_parent" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                        <label>Industries</label><br>
                        <select type="text" name="industries[]" multiple="true" id="uindustries" class="form-control" autofocus="on" style="width:100%;"></select>
                    </div>
                </div>
                <br>
                <input type="hidden" name="indusBtnId" id="indusBtnId">
                <input type="hidden" name="indusParentBtnId" id="indusParentBtnId">
            </div>
            
            <div class="modal-footer">
                <button class="btn btn-primary" id="update_industry_click">Update</button>
                <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
            </div>
        </div>
    </div>
</div>


<script>

industry_select2 = null;
founder_industries = (btnId, parentBtnId) =>
    {

        $('#indusBtnId').val(btnId);
        $('#indusParentBtnId').val(parentBtnId);

        btnArray = btnId.split("_");
        let startupId = parseInt(btnArray[1]);

        $.ajax({
            url: '{{route("industries")}}',
            type: 'POST',
            data: {
                startup_id: startupId,
                _token: '{{ csrf_token() }}'
            },
            dataType: 'JSON',
            success: function (data) {

                if(industry_select2)
                    $('#uindustries').empty();

                industry_select2 = $("#uindustries").select2({
                    placeholder: 'Select Industries or add new',
                    data: data.industries,
                    tags: true
                });

                $('#uindustries').trigger('change');

                $('#modal-industries').modal('toggle');

                console.log(data.industries);

            }
        });

    }

$("#update_industry_click").click( () => {
        
    btnId = $('#indusBtnId').val();
    parentBtnId = $('#indusParentBtnId').val();
    industries = $('#uindustries').select2('data');

    btnArray = btnId.split("_");
    let startupId = parseInt(btnArray[1]);

    parentBtnArray = parentBtnId.split("_");
    delete parentBtnArray[3];
    let col_name = parentBtnArray.join("_");

    $.ajax({
            url: '{{route("ajax_update_industry")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}', 
                startup_id: startupId,
                col_name: col_name,
                industries: JSON.parse(JSON.stringify(industries))
            },
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $(`#${parentBtnId}`).replaceWith(data.new_html);
                        $('#modal-industries').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

});

</script>
    <!--                Target Countries  Modal                                -->
    <div class="modal inmodal modal-tcountries modal-md" id="modal-tcountries" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <i class="fa fa-map modal-icon"></i>
                        <h4 class="modal-title">Target Countries</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div id="country_parent" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Countries</label><br>
    <select type="text" name="tcountries[]" id="tcountries" class="form-control" autofocus="on" style="width:100%;" multiple="multiple"></select>
                            </div>
                        </div>
                        <br>

                        <input type="hidden" name="tstartup_id" id="tstartup_id" class="startup_id">
                        <input type="hidden" name="parent_btns_div" id="parent_btns_div" class="parent_btns_div">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="update_tcountries">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Target Countries  Modal                                -->

<script>

    var tcountries_select2 = null;
    target_countries_clicked = (btnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);

        $('#parent_btns_div').val(btnId);
        $('#tstartup_id').val(startup_id);
        $('#modal-tcountries').modal('toggle');

        $.ajax({
            url: '{{route("target_countries")}}',
            type: 'POST',
            data: {
                startup_id: startup_id,
                _token: '{{ csrf_token() }}', 
            },
            dataType: 'JSON',
            success: function (data) {

                if(tcountries_select2)
                    $('#tcountries').empty();                
                tcountries_select2 = $("#tcountries").select2({
                    placeholder: 'Select Countires',
                    data: data.target_countries
                });

            }
        });

    };

    $("#update_tcountries").click( () => {

        tcountries = $('#tcountries').select2('data');

        var startup_id = $('#tstartup_id').val();
        var new_ids = tcountries.map(tcountry => tcountry.id);

        var parentDivId = $('#parent_btns_div').val();

        $.ajax({
            url: '{{route("ajax_utarget_countries")}}',
            type: 'POST',
            data: {
                startup_id: startup_id,
                new_ids: new_ids,
                _token: '{{ csrf_token() }}', 
            },
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        toastr.success(data.msg, 'Success');
                        $('#'+parentDivId).html(data.new_html);
                        $('#modal-tcountries').modal('toggle');
                    }
            }
        });

    });


</script>
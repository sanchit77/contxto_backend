<div class="modal inmodal modal-newsRoundAdd modal-bg" id="modal-newsRoundAdd" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Add News</h4>
                    </div>
                    <div class="modal-body">
        <form method="post" name="news_add" enctype="multipart/form-data" id="news_add">
                        <div class="row">
                            <div class="col-sm-6">
                                <label>URL</label><br>
    <input type="url" name="url" id="nurl" class="form-control" autofocus="on" placeholder="URL" maxlength="255" required/>
                            </div>

                            <div class="col-sm-6">
                                <label>Image</label><br>
    <input type="file" name="image" id="nimage" class="form-control" placeholder="Image" required accept="image/*">
                            </div>

                        </div>
                        <br>
                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <label>Description</label><br>
    <textarea name="description" placeholder="Description" rows="3" id="ndescription" class="form-control" required></textarea>
                            </div>

                            <div class="col-sm-6">
                                <label>Source</label><br>
    <input type="text" name="source" id="nsource" class="form-control" placeholder="Source Eg - Google" maxlength="20" required/>
                            </div>
                        </div>
                        <br>

    <input type="hidden" name="col_name" id="ncol_name">
    <input type="hidden" name="startup_id" id="nstartup_id">
    <input type="hidden" name="nBtnId" id="nBtnId">
    <input type="hidden" name="nParentBtnId" id="nParentBtnId">

    @csrf
        </form>
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="add_news">Add</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>

<script>

    add_news_click = (btnId, parentBtnId) => {

        $('#nBtnId').val(btnId);
        $('#nParentBtnId').val(parentBtnId);

        btnArray = btnId.split("_");
        let startupId = parseInt(btnArray[1]);

        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];
        let col_name = parentBtnArray.join("_");

        $('#nstartup_id').val(startupId);
        $('#ncol_name').val(col_name);

        $('#modal-newsRoundAdd').modal('toggle');

    };

    $("form[name='news_add']").submit(function(e) {

        e.preventDefault();
        var formData = new FormData(this);

        $.ajax({
            url: "{{route('ajax_cnews')}}",
            type: "POST",
            data: formData,
            success: function (data) {

                if(data.success == 0)
                    {
                    toastr.error(data.msg, 'Error');
                    }
                else
                    {
                    $(`#${$('#nParentBtnId').val()}`).replaceWith(data.new_html);
                    $('#modal-newsRoundAdd').modal('toggle');

                    $("#news_add")[0].reset();

                    toastr.success(data.msg, 'Success');
                    }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    });

</script>
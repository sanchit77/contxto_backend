<!--  --------------      Founder Add Update SubModule     ---------------    -->

    <!--                Founder Update Modal                  -->
    <div class="modal inmodal modal-founderUpdate modal-bg" id="modal-founderUpdate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Update Founder</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-md-12">
                                <label>Founder</label><br>
    <select type="text" name="ufounder" id="ufounder" class="form-control" autofocus="on" style="width:100%;" required="true"></select>
                            </div>
                        </div>
                            <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Roles</label><br>
    <input type="text" name="uroles" id="uroles" class="form-control" style="width:100%;" required="true"/>
                            </div>
                        </div>
                            <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Profile Url</label><br>
    <input type="url" name="uprofile_url" id="uprofile_url" class="form-control" style="width:100%;" required="false"/>
                            </div>
                        </div>

    <input type="hidden" name="fiBtnId" id="fiBtnId">
    <input type="hidden" name="fiParentBtnId" id="fiParentBtnId">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" onClick="update_founder()">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Founder Update Modal                  -->

    <!--                Founder Add Modal                  -->
    <div class="modal inmodal modal-founderAdd modal-bg" id="modal-founderAdd" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Add Founder</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div id="investor_parent" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Founder</label><br>
    <select type="text" name="founder" id="afounder" class="form-control" autofocus="on" style="width:100%;"></select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Roles</label><br>
    <input type="text" name="aroles" id="aroles" class="form-control" style="width:100%;" required="true"/>
                            </div>
                        </div>
                            <br>
                        <div class="row">
                            <div class="col-md-12">
                                <label>Profile Url</label><br>
    <input type="url" name="aprofile_url" id="aprofile_url" class="form-control" style="width:100%;" required="false"/>
                            </div>
                        </div>

    <input type="hidden" name="faiBtnId" id="faiBtnId">
    <input type="hidden" name="faiParentBtnId" id="faiParentBtnId">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" onClick="add_founder()">Add</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Founder Add Modal                  -->

<script>

    var ufounder_select2 = null;
    founder_update = (btnId, parentBtnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);
        let startup_founder_id = parseInt(btnArray[4]);

        $("#fiBtnId").val(btnId);
        $("#fiParentBtnId").val(parentBtnId);

        var attsData = $(`#${btnId}`).data();

        $.ajax({
            url: '{{route("ajax_founder_data")}}',
            type: 'POST',
            data: {
                startup_id,
                startup_founder_id,
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                $("#uroles").val(attsData['roles']);
                $("#uprofile_url").val(attsData['founder_link']);

                if(ufounder_select2)
                    $('#ufounder').empty();

                ufounder_select2 = $("#ufounder").select2({
                    placeholder: 'Select Founder or add new',
                    data: data.founders,
                    tags: true
                });

                $('#ufounder').trigger('change');

                $('#modal-founderUpdate').modal('toggle');

            }
        });

    };

    update_founder = () => {

        let btnId = $("#fiBtnId").val();
        let parentBtnId = $("#fiParentBtnId").val();//["table", "3", "0", "5"]

        btnArray = btnId.split("_");//["table", "3", "0", "5", "1"]
        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];

        let col_name = parentBtnArray.join("_");

        let startup_id = parseInt(btnArray[1]);
        let startup_founder_id = parseInt(btnArray[4]);
        
        //////  Founder    //////////
        let founder_select = $('#ufounder').select2('data');
        let founder_val = founder_select[0]['text'];
        let founder_id = founder_select[0]['id'];
        //////  Founder    //////////

        let roles = $("#uroles").val();
        let founder_link = $("#uprofile_url").val();

        $.ajax({
            url: '{{route("ajax_founder_update")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startup_id,
                startup_founder_id,
                founder_val,
                founder_id,
                col_name,
                roles,
                founder_link,
                ajax_type: "Update"
            },
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        ufounder_select2.select2('destroy');
                        $('#ufounder').find('option').remove().end();

                        $('#'+parentBtnId).replaceWith(data.new_html);
                        $('#modal-founderUpdate').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    }

    var afounder_select2;
    add_founder_click = (btnId, parentBtnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);
        let startup_founder_id = 0;

        $("#faiBtnId").val(btnId);
        $("#faiParentBtnId").val(parentBtnId);

        $.ajax({
            url: '{{route("ajax_founder_data")}}',
            type: 'POST',
            data: {
                startup_id,
                startup_founder_id,
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                afounder_select2 = $("#afounder").select2({
                    placeholder: 'Select Founder add new',
                    data: data.founders,
                    tags: true
                });

                $('#modal-founderAdd').modal('toggle');

            }
        });

    }

    add_founder = () => {

        let btnId = $("#faiBtnId").val();
        let parentBtnId = $("#faiParentBtnId").val();//["table", "3", "0", "5"]

        btnArray = btnId.split("_");//["table", "3", "0", "5", "1"]
        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];

        let col_name = parentBtnArray.join("_");

        let startup_id = parseInt(btnArray[1]);
        let startup_founder_id = parseInt(btnArray[4]);
        
        //////  Founder    //////////
        let founder_select = $('#afounder').select2('data');
        let founder_val = founder_select[0]['text'];
        let founder_id = founder_select[0]['id'];
        //////  Founder    //////////

        let roles = $("#aroles").val();
        let founder_link = $("#aprofile_url").val();

        $.ajax({
            url: '{{route("ajax_founder_update")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startup_id,
                founder_val,
                founder_id,
                startup_founder_id: 0,
                col_name,
                roles,
                founder_link,
                ajax_type: "Add"
            },
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        afounder_select2.select2('destroy');
                        $('#afounder').find('option').remove().end();

                        $('#'+parentBtnId).replaceWith(data.new_html);
                        $('#modal-founderAdd').modal('toggle');

                        $("#aroles").val("");
                        $("#aprofile_url").val("")

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    }

</script>

<!--  --------------      Founder Add Update SubModule     ---------------    -->
<!--  ---------     Investor Round Add Update SubModule     -----------    -->

    <!--                Investor Round Modal Update                   -->
    <div class="modal inmodal modal-investorRoundUpdate modal-bg" id="modal-investorRoundUpdate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Update Investor Round</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Investor</label><br>
    <select type="text" name="ruinvestor[]" multiple id="ruinvestor" class="form-control" autofocus="on" style="width:100%;"></select>
                            </div>

                            <div class="col-sm-6">
                                <label>Round</label><br>
    <select type="text" name="ruround" id="ruround" class="form-control"  style="width:100%;"></select>
                            </div>

                        </div>
                        <br>
                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <label>Amount</label><br>
    <input type="text" name="ruamount" id="ruamount" class="form-control" style="width:100%;"/>
                            </div>

                            <div class="col-sm-6">
                                <label>Date</label><br>
    <input type="text" name="rudt" id="rudt" class="form-control" style="width:100%;"/>
                            </div>
                        </div>
                        <br>
    <input type="hidden" name="ruBtnId" id="ruBtnId">
    <input type="hidden" name="ruParentBtnId" id="ruParentBtnId">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="update_round">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Investor Round Modal Update                   -->


    <!--                Investor Round Modal Add                   -->
    <div class="modal inmodal modal-investorRoundAdd modal-bg" id="modal-investorRoundAdd" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Add Investor Round</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-6">
                                <label>Investor</label><br>
    <select type="text" name="rinvestor[]" multiple id="rinvestor" class="form-control" autofocus="on" style="width:100%;"></select>
                            </div>

                            <div class="col-sm-6">
                                <label>Round</label><br>
    <select type="text" name="rround" id="rround" class="form-control"  style="width:100%;"></select>
                            </div>

                        </div>
                        <br>
                        <br>
                        <div class="row">

                            <div class="col-sm-6">
                                <label>Amount</label><br>
    <input type="text" name="ramount" id="ramount" class="form-control" style="width:100%;"/>
                            </div>

                            <div class="col-sm-6">
                                <label>Date</label><br>
    <input type="text" name="rdt" id="rdt" class="form-control" style="width:100%;"/>
                            </div>
                        </div>
                        <br>
    <input type="hidden" name="rBtnId" id="rBtnId">
    <input type="hidden" name="rParentBtnId" id="rParentBtnId">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="add_round">Add</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Investor Round Modal Add                   -->

    <script type="text/javascript">


            $(document).ready(function() {

              var last_valid_selection = null;

              $('#rinvestor').change(function(event) {

                if ($(this).val().length > 5) {

                  $(this).val(last_valid_selection);
                } else {
                  last_valid_selection = $(this).val();
                }
              });
            });
            </script>

<script>

///////////////////////////////////
    //////////////////      Invest Round Clicked    //////////////
    var ruinvestor_select2 = null;
    var ruround_select2 =null;
    roundUpdateClick = (btnId, parentBtnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);
        let startup_round_id = parseInt(btnArray[4]);

        $("#ruBtnId").val(btnId);
        $("#ruParentBtnId").val(parentBtnId);

        $.ajax({
            url: '{{route("ajax_investor_round_data")}}',
            type: 'POST',
            data: {
                startup_id,
                startup_round_id,
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                if(ruinvestor_select2)
                    $('#ruinvestor').empty();
                ruinvestor_select2 = $("#ruinvestor").select2({
                    placeholder: 'Select Investor',
                    data: data.investors
                });

                if(ruround_select2)
                    $('#ruround').empty();
                ruround_select2 = $("#ruround").select2({
                    placeholder: 'Select Round or add new',
                    data: data.rounds,
                    tags: true
                });

                $("#ruamount").val(data.startup_round.amount);
                $("#rudt").val(data.startup_round.dt);

                $('#rudt').datepicker({
                    showButtonPanel: true,
                    format: 'yyyy-mm-dd',
                    maxDate: '0',
                    autoclose: true
                });

                $('#modal-investorRoundUpdate').modal('toggle');

            }
        });

    }

    $("#update_round").click( () => {

        let btnId = $("#ruBtnId").val();
        let parentBtnId = $("#ruParentBtnId").val();//["table", "3", "0", "5"]

        btnArray = btnId.split("_");//["table", "3", "0", "5", "1"]
        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];

        let col_name = parentBtnArray.join("_");
        let startup_id = parseInt(btnArray[1]);
        let startup_round_id = parseInt(btnArray[4]);

        //////  Investor    //////////
        let investor_select = $('#ruinvestor').select2('data');
        let investor_select1 = $('#ruinvestor').val();
        console.log(investor_select1);
        var investor_id = investor_select1[0];
        let investor_val = investor_select[0]['text'];
        // let investor_id = investor_select[0]['id'];
        //////  Investor    //////////

        //////  Investor Type   //////////
        let round_select = $('#ruround').select2('data');
        let round_val = round_select[0]['text'];
        let round_id = round_select[0]['id'];
        //////  Investor Type   //////////

        $.ajax({
            url: '{{route("ajax_uinvestor_round_data")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startup_id,
                startup_round_id,
                investor_val,
                investor_id,
                investor_select1,
                round_val,
                round_id,
                col_name,
                amount: $("#ruamount").val(),
                dt: $("#rudt").val()
            },
            dataType: 'JSON',
            success: function (data) {

                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $('#'+parentBtnId).replaceWith(data.new_html);
                        $('#modal-investorRoundUpdate').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    });

    add_round_click = (btnId, parentBtnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);

        $("#rBtnId").val(btnId);
        $("#rParentBtnId").val(parentBtnId);

        $.ajax({
            url: '{{route("ajax_investor_round_data")}}',
            type: 'POST',
            data: {
                startup_id,
                startup_round_id: 0,
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                $("#rinvestor").select2({
                    placeholder: 'Select Investor',
                    data: data.investors
                });

                $("#rround").select2({
                    placeholder: 'Select Round or add new',
                    data: data.rounds,
                    tags: true
                });

                $('#rdt').datepicker({
                    showButtonPanel: true,
                    format: 'yyyy-mm-dd',
                    maxDate: '0',
                    autoclose: true
                });

                $('#modal-investorRoundAdd').modal('toggle');

            }
        });

    }

    $("#add_round").click( () => {

        let btnId = $("#rBtnId").val();
        let parentBtnId = $("#rParentBtnId").val();//["table", "3", "0", "5"]

        btnArray = btnId.split("_");//["table", "3", "0", "5", "1"]
        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];

        let col_name = parentBtnArray.join("_");
        let startup_id = parseInt(btnArray[1]);
        let startup_round_id = parseInt(btnArray[4]);

        //////  Investor    //////////
        let investor_select = $('#rinvestor').select2('data');
        let investor_select1 = $('#rinvestor').val();
        console.log(investor_select1);
        var investor_id = investor_select1[0];
        // alert(nameArr);

        let investor_val = investor_select[0]['text'];

        // let investor_id = investor_select[0]['id'];
        //////  Investor    //////////

        //////  Investor Type   //////////
        let round_select = $('#rround').select2('data');
        let round_val = round_select[0]['text'];
        let round_id = round_select[0]['id'];
        //////  Investor Type   //////////

        $.ajax({
            url: '{{route("ajax_cinvestor_round_data")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startup_id,
                investor_val,
                investor_id,
                round_val,
                round_id,
                col_name,
                investor_select1,
                amount: $("#ramount").val(),
                dt: $("#rdt").val()
            },
            dataType: 'JSON',
            success: function (data) {

                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $('#'+parentBtnId).replaceWith(data.new_html);
                        $('#modal-investorRoundAdd').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    });

</script>

<!--  ---------     Investor Round Add Update SubModule     -----------    -->

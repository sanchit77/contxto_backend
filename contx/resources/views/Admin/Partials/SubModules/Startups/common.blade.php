<script>

    //////      Element Change  ///////////////
    $('#example1').on('focus', '.sneaky', function() {

        var el = $(this).attr('id').slice(0, -1), 
        val = $(this).text();

        $(this).parent().find('input,select,texarea').val(val).show().focus();
        $(this).hide();

    });
    //////      Element Change  ///////////////

    update_many_relation_btn_click = (btnId, dynamic_db_id) => {

        var textId = btnId.slice(0, -1);
        var newVal = $('#'+textId).val();

        var all_param = btnId.split("_");

        let startup_id = parseInt(all_param[1]);
        let row_id = parseInt(all_param[2]);
        let column_id = parseInt(all_param[3]);
        let db_row_id = parseInt(all_param[4]);

        var data = {
            _token: '{{ csrf_token() }}', 
            newVal, 
            startup_id,
            row_id,
            column_id,
            db_row_id,
            dynamic_db_id
        };

        $.ajax({
            url: '{{route("ajax_update_has_many_relation")}}',
            type: 'POST',
            data: data,
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    }

/////////       Has Many Btn Add Click  .//////
    add_many_relation_btn_click = (btnId, parentId) => {

        var all_param = btnId.split("_");

        let startup_id = parseInt(all_param[1]);
        let row_id = parseInt(all_param[2]);
        let column_id = parseInt(all_param[3]);

        let textName = allColumns[column_id].text;

        add_pop_up_func(btnId, parentId, all_param, startup_id, row_id, column_id, textName);

    }
/////////       Has Many Btn Add Click  .//////

    add_pop_up_func = (btnId, parentDivId, all_param, startup_id, row_id, column_id, textName) => {

        swal({
            title: "Add New "+textName,
            type: "input",
            showCancelButton: true,
            closeOnConfirm: false,
            showLoaderOnConfirm: true,
            inputPlaceholder: textName+" name"
        }, function (inputValue) {

            if (inputValue === false) return false;
            if (inputValue === "") {
                swal.showInputError("You need to write name of the "+textName);
                return false;
            }

            $.ajax({/////////       Ajax Call       ////////////
                type: "post",
                url: "{{route('ajax_add_has_many_relation')}}",
                data: {
                    _token: '{{ csrf_token() }}',
                    startup_id,
                    column_id,
                    inputValue,
                    row_id
                },
                success: function(data){
                }
            })
            .done(function(data) {

                if(data.success == 0)
                    swal("Oops", data.msg, "error");
                else
                    {
                        swal.close();
                        $('#'+parentDivId).replaceWith(data.new_html);
                        toastr.success(data.msg, 'Success');
                    }

            })
            .error(function(data) {
                swal("Oops", data.responseJSON.msg, "error");
            });/////////       Ajax Call       ////////////

        });

    };

    ///////      Update Value Ajax       ///////
    ajaxUpdateText = (route, params) => {

        $.ajax({
            url: route,
            type: 'POST',
            data: params,
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    toastr.error(data.msg, 'Error');
                else
                    toastr.success(data.msg, 'Success');
            }
        });

    }//////      Update Value Ajax       //////

    /////////   Key Press Release Func      /////////
    inputKeyDown = (newVal, inputId, oldVal, event) => {

        var code = event.keyCode || event.which;

        if(code == 27 || code == 13)
            {
                inputLostFocus(newVal, inputId, oldVal);

                $('#'+inputId).parent().find('input[type="text"],select,textarea').hide();

                $('#'+inputId+'s').text(newVal);
                $('#'+inputId+'s').show();
            }

    };////   Key Press Release Func      ////

    /////////       Save Input Box      /////
    inputLostFocus = (newVal, inputId, oldVal) => {

        if(oldVal === newVal)
            return true;

        ajaxUpdateText(
            '{{route("ajax_update_text")}}', 
            {_token: '{{ csrf_token() }}', newVal, inputId }
        );

        $('#'+inputId+'').attr('data-oval', newVal);

    };/////       Save Input Box      ///////

</script>
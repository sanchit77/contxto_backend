<!--  --------------      Logo Update SubModule     ---------------    -->

<div class="modal inmodal modal-logoUpdate modal-bg" id="modal-logoUpdate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Update Logo</h4>
                    </div>
                    <div class="modal-body">
        <form method="post" name="update_logo" enctype="multipart/form-data" id="update_logo">
                        <div class="row">
                            <div class="col-sm-12">
                                <label>Image</label><br>
    <input type="file" name="logo" id="limage" class="form-control" placeholder="Logo" required accept="image/*">
                            </div>
                        </div>
                        <br>
    <input type="hidden" name="col_name" id="lcol_name">
    <input type="hidden" name="startup_id" id="lstartup_id">
    <input type="hidden" name="lBtnId" id="lBtnId">
    <input type="hidden" name="lParentBtnId" id="lParentBtnId">
    @csrf
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>

<script>

    update_logo_func = (btnId, parentBtnId) => {

        $('#lBtnId').val(btnId);
        $('#lParentBtnId').val(parentBtnId);

        btnArray = btnId.split("_");
        let startupId = parseInt(btnArray[1]);

        let parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];
        let col_name = parentBtnArray.join("_");

        $('#lstartup_id').val(startupId);
        $('#lcol_name').val(col_name);

        $('#modal-logoUpdate').modal('toggle');

    }

    $("form[name='update_logo']").submit(function(e) {

        e.preventDefault();

        var formData = new FormData(this);

        $.ajax({
            url: "{{route('invajax_update_logo')}}",
            type: "POST",
            data: formData,
            success: function (data) {

                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $(`#${$('#lParentBtnId').val()}`).replaceWith(data.new_html);
                        $('#modal-logoUpdate').modal('toggle');

                        $("#update_logo")[0].reset();

                        toastr.success(data.msg, 'Success');
                    }
            },
            cache: false,
            contentType: false,
            processData: false
        });

    });

</script>

<!--  --------------      Logo Update SubModule     ---------------    -->

<div class="modal inmodal modal-countryUpdate modal-bg" id="modal-countryUpdate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Update Country & City</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div id="country_parent" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Country</label><br>
    <select type="text" name="country" id="country" class="form-control" autofocus="on" style="width:100%;"></select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>City</label><br>
    <select type="text" name="city" id="city" class="form-control" style="width:100%;"></select>
                            </div>                            
                        </div>
                        <br>
    <input type="hidden" name="cstartup_id" id="cstartup_id" class="cstartup_id">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="update_country_city">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>

<script>

var tempId;
var country_select2 = null;
var city_select2 = null;
initialize_country_city_data = (btnId, oldCountryId, oldCityId) => {

    btnArray = btnId.split("_");
    let startupId = parseInt(btnArray[1]);
    
    $('#cstartup_id').val(startupId);

    tempId = btnId;

    $.ajax({
            url: '{{route("countries")}}',
            type: 'POST',
            data: {
                oldCountryId,
                oldCityId,
                startupId,
                _token: '{{ csrf_token() }}'
            },
            dataType: 'JSON',
            success: function (data) {

                if(country_select2)
                    $('#country').empty();
                country_select2 = $("#country").select2({
                    placeholder: 'Select an country',
                    data: data.countries,
                })
                .on('select2:select', function (e) {
                    startup_country_changed(btnId, oldCountryId, oldCityId, startupId, e.params.data.id);
                });

                if(city_select2)
                    $('#city').empty();
                city_select2 = $("#city").select2({
                    placeholder: 'Select an city',
                    data: data.cities,
                    tags: true
                });

                $('#modal-countryUpdate').modal('toggle');

            }
        });

    }

startup_country_changed = (btnId, oldCountryId, oldCityId, startupId, newCountryId) => {

    $.ajax({
        url: '{{route("cities")}}',
        type: 'POST',
        data: {
            newCountryId,
            oldCityId,
            startupId,
            _token: '{{ csrf_token() }}', 
        },
        dataType: 'JSON',
        success: function (data) {

            city_select2.select2('destroy');
            $('#city').find('option').remove().end();

            if(city_select2)
                $('#city').empty();

            city_select2.select2({
                data: data.cities,
                tags: true
            });

        }
    });

}

    //Finally Update CIty Country   ///////////////
    $("#update_country_city").click( () => {

        startupId = $('#cstartup_id').val();
        city = $('#city').select2('data');
        country = $('#country').select2('data');

        $.ajax({
            url: '{{route("ajax_update_loc")}}',
            type: 'POST',
            data: {
                startup_id: startupId,
                country_id: country[0].id,
                city_val: city[0].text,
                _token: '{{ csrf_token() }}', 
            },
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $('#modal-countryUpdate').modal('toggle');

                        var countryBtnId = tempId;                        
                        var cityBtnId = tempId.split('_');
                        cityBtnId[3] = "11";
                        cityBtnId = cityBtnId.join("_");

                        $("#"+countryBtnId).html(country[0].text);
                        $("#"+cityBtnId).html(city[0].text);

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    });


</script>
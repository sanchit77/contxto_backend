    <div class="modal inmodal modal-founded modal-md" id="modal-founded" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Founded</h4>
                    </div>
                    <div class="modal-body">
                    </div>
                    <div class="modal-footer">

    <input type="hidden" name="fparent_btns_div" id="fparent_btns_div">

    <button class="btn btn-primary" id="update_tfounded">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>

    <script>

        $('#modal-founded').on('shown.bs.modal', function() {
            $('#DateRequired').datepicker({
                showButtonPanel: true,
                format: 'dd M yyyy',
                maxDate: '0',
                autoclose: true,
                container: '#modal-founded modal-body',
            });
        });

        founded_click = (btnId, val) => {

            $("#modal-founded .modal-header h4").html("Update Founded");

            $("#modal-founded .modal-body").html('<div class="input-append date"><input type="text" class="form-control" id="DateRequired" value="'+val+'" /><span class="add-on"><i data-time-icon="icon-time" data-date-icon="icon-calendar"></i></span></div>');

            $('#DateRequired').css('z-index', 99999999999999);

            btnArray = btnId.split("_");
            let startup_id = parseInt(btnArray[1]);

            $("#fparent_btns_div").val(btnId);

        };

        $("#update_tfounded").click( () => {

            newVal = $("#DateRequired").val();
            inputId = $("#fparent_btns_div").val();

            $.ajax({
                url: '{{route("ajax_update_text")}}',
                type: 'POST',
                data: {
                    _token: '{{ csrf_token() }}', 
                    newVal, 
                    inputId 
                },
                dataType: 'JSON',
                success: function (data) {
                    if(data.success == 0)
                        {
                            toastr.error(data.msg, 'Error');
                        }
                    else
                        {
                            $('#'+inputId+'').text(newVal);
                            $('#modal-founded').modal('toggle');

                            toastr.success(data.msg, 'Success');
                        }
                }
            });

        });

    </script>
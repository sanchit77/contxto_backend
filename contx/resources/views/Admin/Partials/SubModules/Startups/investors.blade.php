<!--  --------------      Investor Add Update SubModule     ---------------    -->

    <!--                Investor Modal                                -->
    <div class="modal inmodal modal-investorCreate modal-bg" id="modal-investorCreate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Create Investor</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div id="investor_parent" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Investor</label><br>
    <select type="text" name="investor[]" multiple id="cinvestor" class="form-control" autofocus="on" style="width:100%;"></select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Type</label><br>
    <select type="text" name="investor_type" id="cinvestor_type" class="form-control" style="width:100%;"></select>
                            </div>
                        </div>
                        <br>
    <input type="hidden" name="ciBtnId" id="ciBtnId" class="ciBtnId">
    <input type="hidden" name="ciParentBtnId" id="ciParentBtnId" class="ciParentBtnId">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="create_investor">Create</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Investor Modal                                -->


    <!--                Investor Modal Update                         -->
    <div class="modal inmodal modal-investorUpdate modal-bg" id="modal-investorUpdate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Update Investor</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div id="investor_parent" class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Investor</label><br>
    <select type="text" name="investor" id="investor" class="form-control" autofocus="on" style="width:100%;"></select>
                            </div>
                        </div>
                        <br>
                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Type</label><br>
    <select type="text" name="investor_type" id="investor_type" class="form-control" style="width:100%;"></select>
                            </div>
                        </div>
                        <br>
    <input type="hidden" name="iBtnId" id="iBtnId" class="iBtnId">
    <input type="hidden" name="iParentBtnId" id="iParentBtnId" class="iParentBtnId">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" id="update_investor">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Investor Modal Update                         -->


<script>

    var uinvestor_select2 = null;
    var uinvestor_type_select2 = null;
    investorUpdateClick = (btnId, parentBtnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);
        let startup_investor_id = parseInt(btnArray[4]);

        $("#iBtnId").val(btnId);
        $("#iParentBtnId").val(parentBtnId);

        $.ajax({
            url: '{{route("investors_ajax")}}',
            type: 'POST',
            data: {
                startup_id,
                startup_investor_id,
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                if(uinvestor_select2)
                    $('#investor').empty();
                uinvestor_select2 = $("#investor").select2({
                    placeholder: 'Select Investor or add new',
                    data: data.investors,
                    tags: true
                });

                if(uinvestor_type_select2)
                    $('#investor_type').empty();
                uinvestor_type_select2 = $("#investor_type").select2({
                    placeholder: 'Select Investor Type or add new',
                    data: data.investor_types,
                    tags: true
                });

                $('#modal-investorUpdate').modal('toggle');

            }
        });

    };

    $("#update_investor").click( () => {

        let btnId = $("#iBtnId").val();
        let parentBtnId = $("#iParentBtnId").val();//["table", "3", "0", "5"]

        btnArray = btnId.split("_");//["table", "3", "0", "5", "1"]
        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];

        let col_name = parentBtnArray.join("_");

        let startup_id = parseInt(btnArray[1]);
        let startup_investor_id = parseInt(btnArray[4]);

        //////  Investor    //////////
        let investor_select = $('#investor').select2('data');
        let investor_val = investor_select[0]['text'];
        let investor_id = investor_select[0]['id'];
        //////  Investor    //////////

        //////  Investor Type   //////////
        let investor_type_select = $('#investor_type').select2('data');
        let investor_type_val = investor_type_select[0]['text'];
        let investor_type_id = investor_type_select[0]['id'];
        //////  Investor Type   //////////

        $.ajax({
            url: '{{route("ajax_update_investor")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startup_id,
                startup_investor_id,
                investor_val,
                investor_id,
                investor_type_val,
                investor_type_id,
                col_name
            },
            dataType: 'JSON',
            success: function (data) {
                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $('#'+parentBtnId).replaceWith(data.new_html);
                        $('#modal-investorUpdate').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }
            }
        });

    });

    add_investor_click = (btnId, parentBtnId) => {

        btnArray = btnId.split("_");
        let startup_id = parseInt(btnArray[1]);

        $("#ciBtnId").val(btnId);
        $("#ciParentBtnId").val(parentBtnId);

        $.ajax({
            url: '{{route("investors_ajax")}}',
            type: 'POST',
            data: {
                startup_id,
                startup_investor_id: 0,
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                $("#cinvestor").select2({
                    placeholder: 'Select Investor',
                    data: data.investors,
                    tags: true
                });

                $("#cinvestor_type").select2({
                    placeholder: 'Select Investor Type or add new',
                    data: data.investor_types,
                    tags: true
                });

                $('#modal-investorCreate').modal('toggle');

            }
        });

    };

    $("#create_investor").click( () => {

        let btnId = $("#ciBtnId").val();
        let parentBtnId = $("#ciParentBtnId").val();//["table", "3", "0", "5"]

        btnArray = btnId.split("_");//["table", "3", "0", "5", "1"]
        parentBtnArray = parentBtnId.split("_");
        delete parentBtnArray[3];

        let col_name = parentBtnArray.join("_");

        let startup_id = parseInt(btnArray[1]);

        //////  Investor    //////////
        // let investor_select = $('#cinvestor').select2('data');
        let investor_select = $('#cinvestor').val();
        let investor_val = investor_select[0]['text'];
        let investor_id = investor_select[0]['id'];
        //////  Investor    //////////

        //////  Investor Type   //////////
        let investor_type_select = $('#cinvestor_type').select2('data');
        let investor_type_val = investor_type_select[0]['text'];
        let investor_type_id = investor_type_select[0]['id'];
        //////  Investor Type   //////////

        $.ajax({
            url: '{{route("ajax_create_investor")}}',
            type: 'POST',
            data: {
                _token: '{{ csrf_token() }}',
                startup_id,
                investor_val,
                investor_id,
                investor_type_val,
                investor_type_id,
                col_name,
                investor_select
            },
            dataType: 'JSON',
            success: function (data) {

                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $('#'+parentBtnId).replaceWith(data.new_html);
                        $('#modal-investorCreate').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }
                  $('#cinvestor').val('');
            }
        });

    });

</script>

<!--  --------------      Investor Add Update SubModule     ---------------    -->

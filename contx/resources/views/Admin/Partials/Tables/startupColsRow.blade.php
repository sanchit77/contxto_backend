<link href="{{ URL::asset('AdminAssets/css/plugins/select2/select2.min.css') }}" rel="stylesheet">
<script src="{{ URL::asset('AdminAssets/js/plugins/select2/select2.full.min.js') }}"></script>

<link href="{{ URL::asset('AdminAssets/css/plugins/datapicker/datepicker3.css') }}" rel="stylesheet">
    <script src="{{ URL::asset('AdminAssets/js/plugins/datapicker/bootstrap-datepicker.js') }}"></script>

<style>
.select2-close-mask{
    z-index: 2099;
}
.select2-dropdown{
    z-index: 3051;
}

.datepicker{z-index:9999 !important}

</style>

<script>

var allColumns = [
            { "data": "startup_id" },//0
            { "data": "name" },//1
            { "data": "valuation" },//2
            { "data": "logo", "orderable": false },//3
            { "data": "founders", text: "Founder" },//4
            { "data": "investors", text: "Investor" },//5
            { "data": "startup_rounds" },//6
            { "data": "target_countries" },//7
            { "data": "description" },//8
            { "data": "founded" },//9
            { "data": "country" },//10
            { "data": "city" },//11
            { "data": "industry" },//12
            { "data": "startup_news_count" },//13
            { "data": "website" },//14
            { "data": "facebook" },//15
            { "data": "twitter" },//16
            { "data": "linkedin" },//17
            { "data": "instagram" },//18
            { "data": "ipo_symbol" },//19
            { "data": "is_public" }//20
        ];

</script>



<tr>
    <th>Sno</th><!-- 0 -->
    <th>Name</th><!-- 1 -->
    <th>Valuation</th><!-- 2 -->
    <th>Logo</th><!-- 3 -->
    <th>Founders</th><!-- 4 -->
    <th>Investors</th><!-- 5 -->
    <th>Investment Rounds</th><!-- 6 -->
    <th>Target Countries</th><!-- 7 -->
    <th>Desc</th><!-- 8 -->
    <th>Founded</th><!-- 9 -->
    <th>Country</th><!-- 10 -->
    <th>City</th><!-- 11 -->
    <th>Industries</th><!-- 12 -->
    <th>News Articles</th><!-- 13 -->

    <th>Website</th><!-- 14 -->
    <th>FB</th><!-- 15 -->
    <th>Twitter</th><!-- 16 -->
    <th>Linkedin</th><!-- 17 -->
    <th>Insta</th><!-- 18 -->

    <th>IPO Symbol</th><!-- 19 -->
    <th>is Public</th><!-- 20 -->
</tr>
<script>
    var allColumns = [
        { "data": "investor_id" },//0
        { "data": "name" },//1
        { "data": "logo", "orderable": false },//3
        { "data": "startup_inverstors_count" },//3
        { "data": "created_at" },//4
        { "data": "updated_at" },//5
        { "data": "delete" }//6
    ];

</script>

<tr>
    <th>Sno</th><!-- 0 -->
    <th>Name</th><!-- 1 -->
    <th>Logo</th><!-- 2 -->
    <th>Startups</th><!-- 3 -->
    <th>Created At</th><!-- 4 -->
    <th>Updated At</th><!-- 5 -->
    <th>Delete</th><!-- 6 -->
</tr>


    <!--                Investor Update Modal                  -->
    <div class="modal inmodal modal-investorUpdate modal-bg" id="modal-investorUpdate" role="dialog" style="overflow:hidden;">
        <div class="modal-dialog">
                <div class="modal-content">
                    <div class="modal-header">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span><span class="sr-only">Close</span>
                        </button>
                        <h4 class="modal-title">Update Investor</h4>
                    </div>
                    <div class="modal-body">

                        <div class="row">
                            <div class="col-sm-12 col-md-12 col-lg-12 col-xs-12">
                                <label>Name</label><br>
    <input type="text" name="investor" id="uname" class="form-control" autofocus="on" style="width:100%;" required></input>
                            </div>
                        </div>
                        <br>
    <input type="hidden" name="fBtnId" id="fBtnId" value="">
                    </div>
                    <div class="modal-footer">
    <button class="btn btn-primary" onClick="update_investor()">Update</button>
    <button type="button" class="btn btn-white" data-dismiss="modal">Close</button>
                    </div>
                </div>
        </div>
    </div>
    <!--                Founder Update Modal                  -->

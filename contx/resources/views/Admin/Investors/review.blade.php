@extends('Admin.layout')

@section('title')
    Investors
@stop

@section('content')

<style>

.dataTables_filter{display:none !important}
        th{width: auto !important;}
        .selected_Button{display:none;}
        table{    border: 1px solid #e7eaec;}
        tr td{font-size: 13px}

        .toolbar {
    float:left;
}

</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Investors</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('admin_dashboard')}}">Dashboard</a>
                </li>
                <li>
                    <a href="{{route('admin.investors.review')}}"><b>Investors Review</b></a>
                </li>
            </ol>
        </div>
    </div>

    <br>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="footable table table-striped display" id="su_reviews">
                    <thead>
                        @include("Admin.Partials.Tables.investorsReviewColsRow")
                        @include("Admin.Partials.Tables.investorsReviewColsRow")
                    </thead>

                    <tbody>

                    </tbody>

                </table>

                            </div>
                        </div>
                    </div>

    <br><br>

    <script>

    $("#investors_review").addClass("active");
    var dataTable;

    //////       Search Box On Columns   ///
    $('#su_reviews thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" data-column='+i+' placeholder="Search '+title+'" class="search-input-text"/>' );
    });//////       Search Box On Columns   ///

    //////      Element Change  ///////////////
    $('#su_reviews').on('focus', '.sneaky', function() {
        var el = $(this).attr('id').slice(0, -1),
        val = $(this).text();

        $(this).parent().find('input,select,texarea').val(val).show().focus();
        $(this).hide();

    });//////      Element Change  ///////////////

//////////  Document Ready  ///////////////////////////
    $(document).ready( function () {

        getJsonData = (route) => {/////////   Get Data    /////////////

            dataTable = $('#su_reviews').DataTable({

                initComplete: function ()
                    {
                    },
                "pagingType": "full_numbers",
                "sDom": '<"table_body" <"table-responsive" t> ><"pagination_area pagination-sec clearfix" <"selected_Button pull-left " > <"pagination-right" flip> >',
                "processing": true,
                "serverSide": true,
                "pageLength":10,
                "info":true,
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ){
                    $('td', nRow).addClass( "id_class" );
                },

                "ajax":{
                    url : route, // json datasource
                    type: "post",  // method  , by default get
                    dataSrc: 'investors',
                    data: {
                    _token: '{{ csrf_token() }}',
                    },
                },
                "columns": allColumns,
                "orderCellsTop": true,
                //"fixedHeader": true
            });

        /////////////       Search Boxes        //////////////
        $('#su_reviews thead tr:eq(1) th').each( function (i) {// Apply the search

            $('input',this).on('keyup change', function () {
                if (dataTable.column(i).search() !== this.valued) {
                    dataTable
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });

        });// Apply the search
        /////////////       Search Boxes        //////////////

    }/////////   Get Data    /////////////

        getJsonData('{{route("investors_data_review")}}');

    });
//////////  Document Ready  ///////////////////////////


//////////////////      Investors Clicked    ///////////
    update_name = (btnId) => {

        $("#fBtnId").val(btnId);// BTN Id
        $("#uname").val($(`#${btnId}`).text());//Name

        $('#modal-investorUpdate').modal('toggle');
        //console.log("Founder Updated", btnId, parentBtnId, `#${btnId}`, name);
    };
//////////////////      Investors Clicked    ///////////

/////////////////       Update Founder Clicked  ////////
    update_investor = () => {

        let fBtnId = $("#fBtnId").val();
        btnArray = fBtnId.split("_");//["tableID", "InvestorID", "ROWNo", "COLNo"]

        $.ajax({
            url: '{{route("admin.investor.update")}}',
            type: 'POST',
            data: {
                investor_id: btnArray[1],
                fBtnId,
                name: $("#uname").val(),
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                $('#'+fBtnId).replaceWith(data.new_html);

                $('#modal-investorUpdate').modal('toggle');

                toastr.success(data.msg, 'Success');

            }
        });

    }


    </script>

@stop

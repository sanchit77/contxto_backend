<!DOCTYPE html>
<html>

<head>

    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">

    <link rel="shortcut icon" href="{{ URL::asset('AdminAssets/Images/favicon.ico') }}" type="image/x-icon">
    <link rel="icon" href="{{ URL::asset('AdminAssets/Images/favicon.ico') }}" type="image/x-icon">

    <title>{{env('APP_NAME')}} - @yield('title')</title>

    <script src="{{ URL::asset('AdminAssets/js/jquery-2.1.1.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/bootstrap.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/bootstrap.min.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">

    <link href="{{ URL::asset('AdminAssets/css/animate.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/style.css') }}" rel="stylesheet">
    <link href="{{ URL::asset('AdminAssets/css/plugins/sweetalert/sweetalert.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('AdminAssets/css/plugins/toastr/toastr.min.css') }}" rel="stylesheet" type="text/css">
    <link href="{{ URL::asset('AdminAssets/css/plugins/blueimp/css/blueimp-gallery.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/toastr/toastr.min.js') }}"></script>

    <style>

        body
            {
                font-size:12px !important;
            }
        .btn
            {
                font-size: 12px !important;
            }
        .lightBoxGallery
            {
                text-align: center;
            }
        .lightBoxGallery img
            {
                margin: 5px;
            }
        .img-circle
            {
                width: 50px !important;
                height: 50px !important;
            }
        .tableclass
            {
                    overflow-x: overlay;
            }

        th,tr
            {
                text-align: center !important;
            }

        td
            {
                padding-top:15px !important;
            }
        .low_padding
            {
                padding-top:5px !important;
            }

        input[type=number]::-webkit-inner-spin-button, input[type=number]::-webkit-outer-spin-button
            {
                -webkit-appearance: none;
                margin: 0;
            }

    </style>

</head>

<body class="pace-done" cz-shortcut-listen="true">

<div id="wrapper">

    <nav class="navbar-default navbar-static-side" role="navigation">
        <div class="sidebar-collapse">
            <ul class="nav metismenu" id="side-menu">

                <li class="nav-header">
                    <div class="dropdown profile-element">

                        <span>
                            <a href="{{config('app.RESIZE_URL')}}{{ $admin->profile_pic }}" title="{{ $admin->name }}" class="lightBoxGallery" data-gallery="">
                                <img alt="image" class="img-circle" style="max-width:100px !important;max-height:70px !important;" src="{{config('app.RESIZE_URL')}}{{ $admin->profile_pic }}/150/150" />
                            </a>
                        </span>

                        <a data-toggle="dropdown" class="dropdown-toggle" href="#">
                            <span class="clear">
                                <span class="block m-t-xs">
                                    <strong class="font-bold">{{ $admin->name }}</strong>
                                </span>
                                <span class="text-muted text-xs block">
                                    {{ $admin->job }}
                                    <b class="caret"></b>
                                </span>
                            </span>
                         </a>

                        <ul class="dropdown-menu animated fadeInRight m-t-xs">
                            <li>
                                <a href="{{route('aprofile_update_get')}}"><i class="fa fa-rouble"></i> Profile Update</a>
                            </li>
                            <li>
                                <a href="{{route('apassword_update_get')}}"><i class="fa fa-cog"></i> Password Update</a>
                            </li>
                            <li class="divider"></li>
                            <li>
                                <a id="demo331" class="demo331" ><i class="fa fa-sign-out"></i> Logout</a>
                            </li>
                        </ul>
                    </div>
                    <div class="logo-element">
                        CTX
                    </div>
                </li>

                <li id="dashboard" title="Dashboard">
                    <a href="{!! route('admin_dashboard') !!}">
                        <i class="fa fa-th-large"></i>
                        <span class="nav-label">Dashboard</span>
                    </a>
                </li>

                <li id="startups1" title="Startups">
                    <a href="{!! route('admin.startups.index') !!}">
                        <i class="fa fa-angellist"></i>
                        <span class="nav-label">Startups</span>
                    </a>
                </li>
                <li id="startups_review" title="Startups Review">
                    <a href="{!! route('admin.startups.review') !!}">
                        <i class="fa fa-angellist"></i>
                        <span class="nav-label">Startups review</span>
                    </a>
                </li>

                <li id="founders" title="Founders">
                    <a href="{!! route('admin.founders.index') !!}">
                        <i class="fa fa-user-secret"></i>
                        <span class="nav-label">Founders</span>
                    </a>
                </li>

                <li id="investors" title="Investors">
                    <a href="{!! route('admin.investors.index') !!}">
                        <i class="fa fa-line-chart"></i>
                        <span class="nav-label">Investors</span>
                    </a>
                </li>

                <li id="investors_review" title="Investors Review">
                    <a href="{!! route('admin.stage.index') !!}">
                        <i class="fa fa-line-chart"></i>
                        <span class="nav-label">Stage</span>
                    </a>
                </li>

                <li id="industries" title="Industries">
                    <a href="{!! route('admin.industries.index') !!}">
                        <i class="fa fa-wrench"></i>
                        <span class="nav-label">Industries</span>
                    </a>
                </li>


            </ul>

        </div>
    </nav>

    <div id="page-wrapper" class="gray-bg">

        <div class="row border-bottom">
            <nav class="navbar navbar-static-top white-bg" role="navigation" style="margin-bottom: 0">
                <div class="navbar-header">
    <a class="navbar-minimalize minimalize-styl-2 btn btn-primary " href="#" style="background-color: #3AA8E4; border-color: #3AA8E4;">
        <i class="fa fa-bars"></i>
    </a>
                </div>
            </nav>
        </div>


        @foreach($errors->all() as $error)
            <div class="alert alert-dismissable alert-danger">
                {!! $error !!}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
        @endforeach

        @if(session('status'))
            <div class="alert alert-success">
                {{ session('status') }}
                    <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>
            </div>
        @endif

       @yield('content')


    </div>


            <div class="footer fixed">
                {{env('APP_NAME')}} &copy {{ now()->year }}
            </div>

    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/sweetalert/sweetalert.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/metisMenu/jquery.metisMenu.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/slimscroll/jquery.slimscroll.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/inspinia.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/pace/pace.min.js') }}"></script>

    <script src="{{ URL::asset('AdminAssets/js/plugins/blueimp/jquery.blueimp-gallery.min.js') }}"></script>
    <script src="{{ URL::asset('AdminAssets/js/plugins/idle-timer/idle-timer.min.js') }}"></script>

    <script>

        $(document).ready(function()
            {
            ///////////////////////////////////  Logout Function /////////////////////////////////////////////////
                $('.demo331').click(function ()
                    {

                        swal({
                            title: "Are you sure?",
                            text: "You want to logout!",
                            type: "warning",
                            showCancelButton: true,
                            confirmButtonColor: "#DD6B55",
                            confirmButtonText: "Yes, Logout!",
                            closeOnConfirm: false
                        },
                        function(){
                            window.location.href = '{{route('admin_logout')}}';
                        });

                    });
                        ///////////////////////////////////////         Toaster  //////////////////////////////////////////////////

                // $(document).idleTimer(300000);

                // $(document).bind("idle.idleTimer", function(){
                //     window.location.href = '';
                // });

                $(document).on( "active.idleTimer", function(event, elem, obj, triggerevent)
                    {
                        toastr.clear();
                        $('.success-alert').fadeOut();
                        toastr.success('Great.','You are back. ');

                    });

            });

    </script>

</div>

    <div id="blueimp-gallery" class="blueimp-gallery">
        <div class="slides"></div>
        <h3 class="title"></h3>
        <a class="prev">‹</a>
        <a class="next">›</a>
        <a class="close">×</a>
        <a class="play-pause"></a>
        <ol class="indicator"></ol>
    </div>

</body>

</html>

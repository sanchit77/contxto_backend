@extends('Admin.layout')

@section('title')
     Dashboard
@stop

@section('content')

    <link href="{{ URL::asset('AdminAssets/css/mystyle.min.css') }}" rel="stylesheet">

    <script src="{{ URL::asset('AdminAssets/js/plugins/fullcalendar/moment.min.js') }}"></script>

    <link href="{{ URL::asset('AdminAssets/css/plugins/daterangepicker/daterangepicker-bs3.css') }}" rel="stylesheet">

    <div class="wrapper wrapper-content animated" data-animation="rotateInUpLeft">

        <div class="row">
            <div class="col-md-offset-2 col-lg-8 col-lg-offset-2 col-md-8 col-sm-offset-2  col-sm-8">
                <center>
                    <label>Filter Data</label>
                </center>
                <center>
                    <input date-range-picker id="daterange" name="daterange" class="form-control date-picker active" type="text" clearable="true" options="dateRangeOptions" value="{{$data['starting_dt']}} - {{$data['ending_dt']}}"/>
                </center>
                <br>
            </div>
        </div>

        <div class="row">

            <div class="col-lg-3" onclick="javascript:location.href='{{route("admin.startups.index")}}'">
                <div class="small-box bg-teal">
                    <div class="inner">
                        <h3 id="order_counts" class="order_counts">{{@$startups}}</h3>
                            <p>Startups</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-angellist"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3" onclick="javascript:location.href='{{route("admin.founders.index")}}'">
                <div class="small-box bg-green">
                    <div class="inner">
                        <h3 id="founder_counts" class="founder_counts">{{@$founders}}</h3>
                            <p>Founders</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-user-secret"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3" onclick="javascript:location.href='{{route("admin.investors.index")}}'">
                <div class="small-box bg-blue">
                    <div class="inner">
                        <h3 id="founder_counts" class="founder_counts">{{@$investors}}</h3>
                            <p>Investors</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-line-chart"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>

            <div class="col-lg-3" onclick="javascript:location.href='{{route("admin.industries.index")}}'">
                <div class="small-box bg-red">
                    <div class="inner">
                        <h3 id="industry_counts" class="industry_counts">{{@$industry}}</h3>
                            <p>Industries</p>
                    </div>
                    <div class="icon">
                        <i class="fa fa-wrench"></i>
                    </div>
                    <a href="#" class="small-box-footer">
                        More info <i class="fa fa-arrow-circle-right"></i>
                    </a>
                </div>
            </div>


        </div>

    </div>

    <br><br>

    <script src="{{ URL::asset('AdminAssets/js/plugins/daterangepicker/daterangepicker.js') }}"></script>

    <script>

    $("#dashboard").addClass("active");

    $(document).ready(function()
        {

            toastr.success('Welcome to {{env("APP_NAME")}} Dashboard', '{{ $admin->name }}');

                $('input[name="daterange"]').daterangepicker({
                    timePicker: false,
                    minYear: 2019,
                    startDate: moment("{{$data['starting_dt']}}"),
                    endDate: moment("{{$data['ending_dt']}}"),
                    locale: {
                    format: 'YYYY-MM-DD'
                    }
                });

                setInterval(function()
                    {
                        new_get_dashboard_data();
                    }, 60000);

                $('#daterange').on('apply.daterangepicker', function(ev, picker)
                    {
                        new_get_dashboard_data();
                    });

            });

        function new_get_dashboard_data()
            {

                dt = document.getElementById('daterange').value;
                yr = document.getElementById('starting_date').value;

                dt = encodeURIComponent(dt);
                route = '{!! route('admin_dash_data') !!}?daterange='+dt+'&yr_only='+yr+'&latitude='+latitude+'&longitude='+longitude;
                console.log(route);
                $.ajax
                    ({
                        url: route,
                        type: 'GET',
                        async: true,
                        dataType: "json",
                        success: function (data)
                            {
                                if(data.success == '0')
                                    toastr.error('Error.',data.message);
                                else
                                    {
                                        assign_dashboard_data(data.stats);
                                        graph_data(data);
                                    }
                            }
                    });

            }

        function assign_dashboard_data(stats)
            {
                //document.getElementById('order_counts').innerHTML = stats.order_counts;
                return '1';
            }

            console.log("{{$data['starting_dt']}}");

    </script>

@stop

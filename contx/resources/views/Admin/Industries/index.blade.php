@extends('Admin.layout')

@section('title')
    Industries
@stop

@section('content')

<style>

.dataTables_filter{display:none !important}
        th{width: auto !important;}
        .selected_Button{display:none;}
        table{    border: 1px solid #e7eaec;}
        tr td{font-size: 13px}

        .toolbar {
    float:left;
}

tfoot {
     display: table-header-group;
}



    </style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Industries</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('admin_dashboard')}}">Dashboard</a>
                </li>
                <li>
                    <a href="{{route('admin.industries.index')}}"><b>Industries</b></a>
                </li>
            </ol>
        </div>
    </div>

    <br>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="footable table table-striped display" id="example1">
                    <thead>
                        @include("Admin.Partials.Tables.industriesColsRow")
                        @include("Admin.Partials.Tables.industriesColsRow")
                    </thead>

                    <tbody>

                    </tbody>

                </table>

                            </div>
                        </div>
                    </div>

    <br><br>

    <script>

    $("#industries").addClass("active");
    var dataTable;
    var order = 0;

    //////       Search Box On Columns   ///
    $('#example1 thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();
        $(this).html( '<input type="text" data-column='+i+' placeholder="Search '+title+'" class="search-input-text"/>' );
    });//////       Search Box On Columns   ///

    //////      Element Change  ///////////////
    $('#example1').on('focus', '.sneaky', function() {
        var el = $(this).attr('id').slice(0, -1),
        val = $(this).text();

        $(this).parent().find('input,select,texarea').val(val).show().focus();
        $(this).hide();

    });//////      Element Change  ///////////////

//////////  Document Ready  ///////////////////////////
    $(document).ready( function () {

        getJsonData = (route) => {/////////   Get Data    /////////////

            dataTable = $('#example1').DataTable({
                initComplete: function ()
                    {
                    },
                "pagingType": "full_numbers",
                "sDom": '<"table_body" <"table-responsive" t> ><"pagination_area pagination-sec clearfix" <"selected_Button pull-left " > <"pagination-right" flip> >',
                "processing": true,
                "serverSide": true,
                "pageLength":10,
                "info":true,
                "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ){
                    $('td', nRow).addClass( "id_class" );
                },

                "ajax":{
                    url : route, // json datasource
                    type: "post",  // method  , by default get
                    dataSrc: 'industries',
                },
                "columns": allColumns,
                "orderCellsTop": true,
                //"fixedHeader": true
            });

        /////////////       Search Boxes        //////////////
        $('#example1 thead tr:eq(1) th').each( function (i) {// Apply the search

            $('input',this).on('keyup change', function () {
                if (dataTable.column(i).search() !== this.valued) {
                    dataTable
                        .column(i)
                        .search(this.value)
                        .draw();
                }
            });

        });// Apply the search
        /////////////       Search Boxes        //////////////

    }/////////   Get Data    /////////////

        getJsonData('{{route("admin.industries.data")}}');

    });
//////////  Document Ready  ///////////////////////////


//////////////////      Industry Clicked    ///////////
    update_name = (btnId) => {

        $("#fBtnId").val(btnId);// BTN Id
        $("#uname").val($(`#${btnId}`).text());//Name

        $('#modal-industryUpdate').modal('toggle');
    };
//////////////////      Industry Clicked    ///////////

/////////////////       Industry Founder Clicked  ////////
    update_industry = () => {

        let fBtnId = $("#fBtnId").val();
        btnArray = fBtnId.split("_");//["tableID", "IndustryId", "ROWNo", "COLNo"]

        $.ajax({
            url: '{{route("admin.industry.update")}}',
            type: 'POST',
            data: {
                industry_id: btnArray[1],
                fBtnId,
                name: $("#uname").val(),
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        $('#'+fBtnId).replaceWith(data.new_html);
                        $('#modal-industryUpdate').modal('toggle');

                        toastr.success(data.msg, 'Success');
                    }


            }
        });

    }

    delete_name = (btnId) => {

       $("#fBtnId").val(btnId);
        btnArray = btnId.split("_");//["tableID", "FounderID", "ROWNo", "COLNo"]
        // alert(btnArray);
        var x = confirm("Are you sure you want to delete?");
        if(x){
        $.ajax({
            url: '{{route("admin.industries.delete")}}',
            type: 'POST',
            data: {
                industry_id: btnArray[1],
                _token: '{{ csrf_token() }}',
            },
            dataType: 'JSON',
            success: function (data) {

                if(data.success == 0)
                    {
                        toastr.error(data.msg, 'Error');
                    }
                else
                    {
                        location.reload();
                        alert('Deleted Succesfully');
                        toastr.success(data.msg, 'Success');
                    }

            }
        });
      }
      else{

      }

    }

    </script>

@stop

@extends('Admin.layout')

@section('title') 
    {{$industry->name}} Startups
@stop

@section('content')

<style>

.dataTables_filter{display:none !important}
        th{width: auto !important;}
        .selected_Button{display:none;}
        table{    border: 1px solid #e7eaec;}
        tr td{font-size: 13px}

        .toolbar {
    float:left;
}
tfoot {
     display: table-header-group;
}
.uif-hidden {
    display: none;
}
button.sneaky {
    border: 1px solid transparent;
    border-bottom: 1px dashed blue;
    display: inline-block;
    padding: 2px;
    background: transparent;
}
button.sneaky:hover {
    border: 1px solid #ccc;
    background: #ffffcc;
}
input, select, textarea {
    position: relative;
}

    </style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2><b>{{$industry->name}}</b> Startups</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('admin_dashboard')}}">Dashboard</a>
                </li>
                <li>
                    <a href="{{route('admin.industries.index')}}">Industries</a>
                </li>
                <li>
                    <a href="{{route('admin.industries.startups',['industry_id'=>$industry->industry_id])}}"><b>Startups</b></a>
                </li>
            </ol>
        </div>
    </div>

    <br>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="footable table table-striped display" id="example1">
                    <thead>
                        @include("Admin.Partials.Tables.startupColsRow")
                        @include("Admin.Partials.Tables.startupColsRow")
                    </thead>
                    
                    <tbody>

                    </tbody>

                </table>

                            </div>
                        </div>
                    </div>

    <br><br>

    <script>

    $("#industries").addClass("active");
    var dataTable;
    var order = 0;

    //////       Search Box On Columns   ///
    $('#example1 thead tr:eq(1) th').each( function (i) {
        var title = $(this).text();

        if(i != 3)
            $(this).html( '<input type="text" data-column='+i+' placeholder="Search '+title+'" class="search-input-text"/>' );
        else
            $(this).html( '<input type="text" disabled data-column='+i+' placeholder="Search '+title+'" class="search-input-text"/>' );

    });//////       Search Box On Columns   ///

    $(document).ready( function () {

        getJsonData = (route) => {/////////   Get Data    /////////////

        dataTable = $('#example1').DataTable({
            "pagingType": "full_numbers",
            "sDom": '<"table_body" <"table-responsive" t> ><"pagination_area pagination-sec clearfix" <"selected_Button pull-left " > <"pagination-right" flip> >',
            "processing": true,
            "serverSide": true,
            "pageLength":10,
            "info":true,
            "fnRowCallback": function( nRow, aData, iDisplayIndex, iDisplayIndexFull ){
                $('td', nRow).addClass( "id_class" );
            },    
            "ajax":{
                url : route, // json datasource
                type: "post",  // method  , by default get
                dataSrc: 'startups',
                data: {
                    _token: '{{ csrf_token() }}',
                    industry_id: '{{$industry->industry_id}}'
                },
                error: function(xmlhttp){  // error handling
                    if(xmlhttp.status == 403)
                        {
                            window.location.reload();
                        }
                    $("#example").append('<tbody class="example-error"><tr><th colspan="10">No data found in the server</th></tr></tbody>');
                    $("#example_processing").css("display","none");
                }
            },
            "columns": allColumns,
            "orderCellsTop": true
        });

    /////////////       Search Boxes        ///////////////////////////

        $('#example1 thead tr:eq(1) th').each( function (i) {// Apply the search

                $('input', this).on('keyup change', function () {
                    if ( dataTable.column(i).search() !== this.value ) {
                        dataTable.column(i).search(this.value).draw();
                    }
                });

            });// Apply the search

        };/////////   Get Data    /////////////

        getJsonData('{{route("startups_data1")}}');
        
    });

    </script>

    @include('Admin.Partials.SubModules.Startups.common')
    @include('Admin.Partials.SubModules.Startups.logo')
    @include('Admin.Partials.SubModules.Startups.founders')
    @include('Admin.Partials.SubModules.Startups.industry')
    @include('Admin.Partials.SubModules.Startups.investors')
    @include('Admin.Partials.SubModules.Startups.investorRounds')
    @include('Admin.Partials.SubModules.Startups.countryCity')
    @include('Admin.Partials.SubModules.Startups.targetCountries')
    @include('Admin.Partials.SubModules.Startups.founded')
    @include('Admin.Partials.SubModules.Startups.news')
    
    

@stop




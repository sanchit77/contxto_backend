@extends('Admin.layout')

@section('title')
    Investors
@stop

@section('content')
<?php
$i=1; ?>
<style>

.dataTables_filter{display:none !important}
        th{width: auto !important;}
        .selected_Button{display:none;}
        table{    border: 1px solid #e7eaec;}
        tr td{font-size: 13px}

        .toolbar {
    float:left;
}

</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Investors</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('admin_dashboard')}}">Dashboard</a>
                </li>
                <li>
                    <a href="{{route('admin.stage.index')}}"><b>Stages</b></a>
                </li>

                <a href="{{route('admin.stage.add')}}"><button class="btn btn-default delete" id="table_1_0_0" title="Add">Add Stages</button></a>

            </ol>

        </div>

    </div>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="table-responsive">
                <form action="{{route('admin.stage.update')}}" method="post">
                  @csrf
                  <input type="hidden" name="stage_id" value="{{ $rounds->round_id}}">
                  <label>Stages</label>
                  <input type="text" value="{{$rounds->name}}" name="name">
                  <input type="submit" value="EDIT">
                </form>

                            </div>
                        </div>
                    </div>
@stop

@extends('Admin.layout')

@section('title')
    Investors
@stop

@section('content')
<?php
$i=1; ?>
<style>

.dataTables_filter{display:none !important}
        th{width: auto !important;}
        .selected_Button{display:none;}
        table{    border: 1px solid #e7eaec;}
        tr td{font-size: 13px}

        .toolbar {
    float:left;
}

</style>

<link rel="stylesheet" href="https://cdn.datatables.net/1.10.19/css/jquery.dataTables.min.css">
<script src="https://cdn.datatables.net/1.10.19/js/jquery.dataTables.min.js"></script>

    <div class="row wrapper border-bottom white-bg page-heading">
        <div class="col-lg-12">
            <h2>Investors</h2>
            <ol class="breadcrumb">
                <li>
                    <a href="{{route('admin_dashboard')}}">Dashboard</a>
                </li>
                <li>
                    <a href="{{route('admin.stage.index')}}"><b>Stages</b></a>
                </li>

                <a href="{{route('admin.stage.add')}}"><button class="btn btn-default delete" id="table_1_0_0" title="Add">Add Stages</button></a>

            </ol>

        </div>

    </div>

    <div class="ibox float-e-margins">
        <div class="ibox-content">
            <div class="table-responsive">
                <table class="footable table table-striped display" id="example1">
                    <thead>
                      <tr>
                          <th>Sno</th><!-- 0 -->
                          <th>Name</th><!-- 1 -->
                          <th>Created At</th><!-- 4 -->
                          <th>Updated At</th><!-- 5 -->
                          <th>Delete</th><!-- 6 -->
                      </tr>
                    </thead>

                    <tbody>
                      @foreach($stages as $stage)
                      <tr>
                          <th>{{ @$i}}</th><!-- 0 -->
                          <th>{{@$stage->name}}</th><!-- 1 -->
                          <th>{{@$stage->created_at}}</th><!-- 4 -->
                          <th>{{@$stage->updated_at}}</th><!-- 5 -->
                          <th><a href="{{url('admin/stage/edit',@$stage->round_id)}}"><button class="btn btn-default delete" title="Edit">Edit</button></a>
                          <button data-confirm="Are you sure to delete?" class="btn btn-default delete" onclick="delete_name({{$stage->round_id}})" title="Delete">Delete</button></th><!-- 6 -->
                      </tr>
                      <?php $i++; ?>
                      @endforeach
                    </tbody>

                </table>

                            </div>
                        </div>
                    </div>
@stop
<script>
   function delete_name($id){
     var id=$id;
     $.ajax(
     {
         url: "{{url('admin/stage/delete')}}/"+id,
         type: "get",
         datatype: "json"
     }).done(function(data){


     });
   }
</script>

@extends('layouts.web')
@section('content')
@section('title', @$title)
@section('description', @$description)
@section('image_link', @$image_link)
@section('imagename', @$imagename)
@section('url', @$url)
@section('keywords', '')
<div class="searchArea">
 <div class="container">
    <div class="row">
       <div class="col-12">
          <div class="seachbox">
             <input type="text" name="keyword" class="keyword" value="{{$keyword}}" placeholder="Search Founders…">
             <button><img src="{{ asset('web/img/search.svg')}}"></button>
          </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-4 col-md-5 order-md-2 col-12 text-center">
          @include('elements.add-company-btn')
       </div>
       @include('elements.main-navbar')
    </div>
 </div>
</div>
<section id="detalsPage">
 <div class="contenArea">
    <div class="container">
       <div class="row">
          <div class="col-12">
             <p class="foundFounders">{{$foundFounders}}</p>
             @if(@$industry->name || @$country->name)
            <h1 class="h1value">{{ @$industry->name}} @if(@$industry->name) companies @else Companies @endif founders @if(@$country->name) in @endif {{@$country->name}}</h1>
            @else
            <h1 class="h1value"></h1>
            @endif
          </div>
          <div class="col-lg-9 col-12">
             <div class="tab-content">
                <div id="investors" class="tab-pane active">
                    <div class="btn-group">
                      <div class="row">
                         <div class="col-lg-3">
                            <button class="filter-btn"><i class="mr-2"> <img src="{{ asset('web/img/filter.png')}}" alt="img"></i> <span style="line-height:normal;">Filter</span><span class="fliterCount"></span></button>
                         </div>
                         <div class="col-lg-9">
                            <div class="ddGroup">
                               <div class="row">
                                  <div class="col-md-5 col-6">
                                     <select class="customSelectBx" id="countries">
                                        <option value="0">All Countries</option>
                                        @foreach($countries as $country)
                                            <option value="{{$country->country_id}}">{{$country->name}} ({{$country->founders_count}})</option>
                                            <!-- ({{$country->founders_count}}) -->
                                        @endforeach
                                     </select>
                                  </div>
                                  <div class="col-md-5 col-6">
                                    <select class="customSelectBx" id="sort">
                                       <option value="0">Sort</option>
                                           <option value="1">Low to High</option>
                                           <option value="2">High to Low</option>

                                    </select>
                                  </div>
                               </div>
                            </div>
                            <select class="multiSelectBx multiSelect" id="industries" multiple="multiple">
                                @foreach($industries as $industry)
                                    <option value="{{$industry->industry_id}}"  data-section="Industry">{{$industry->name}}</option>
                                @endforeach
                            </select>
                         </div>
                      </div>
                   </div>
                   <div id="founders_container">
                      @include('elements.founders-list')
                   </div>
                </div>
             </div>
          </div>
          @include('elements.startup-funding-list')
       </div>
    </div>
 </div>
</section>
@include('elements.new-company')
<script type="text/javascript">
    /*$(window).on('hashchange', function() {
        if (window.location.hash) {
            var page = window.location.hash.replace('', '');
            if (page == Number.NaN || page <= 0) {
                return false;
            }else{
                getData(page);
            }
        }
    });*/


    $( ".keyword" ).keyup(function() {
      var country_id =$("#countries").val();
      var city_id =$("#cities").val();
      var industries =$("#industries").val();
      var sort =$("#sort").val();
      var keyword = $( ".keyword" ).val();
      if(keyword!=''){
        getData(1,keyword,country_id,city_id,industries,sort);
      }else{
        getData(1,'',country_id,city_id,industries,sort);
      }
    });

    $(document).on('click', '.pagination a',function(event)
    {
        event.preventDefault();

        $('li').removeClass('active');
        $(this).parent('li').addClass('active');

        var myurl = $(this).attr('href');
        var page=$(this).attr('href').split('page=')[1];
        var keyword=$(this).attr('href').split('keyword=')[1];
        var country_id=$(this).attr('href').split('country_id=')[1];
        var city_id=$(this).attr('href').split('city_id=')[1];
        var sort=$(this).attr('href').split('sort=')[1];
        var industries=$(this).attr('href').split('industries=')[1];

        getData(page,keyword,country_id,city_id,industries,sort);
    });

    function getData(page,keyword,country_id,city_id,industries,sort){
        $.ajax(
        {
            url: '?page=' + page + '&keyword=' + keyword + '&country_id=' + country_id + '&city_id=' + '0' + '&industries=' + industries + '&sort=' + sort,
            type: "get",
            datatype: "json"
        }).done(function(data){

            $("#founders_container").empty().html(data.html);
            $(".foundFounders").empty().text(data.foundFounders);

            if(data.countryslug && data.indusslug && data.cityslug){
            window.history.pushState("Details", "Title", "{{ route('web-founders') }}/"+data.countryslug+"/"+data.cityslug+"/"+data.indusslug);
            $(".h1value").html(data.indusname + ' companies founders in ' + data.cityname +','+ data.countryname);
            }
            else if(data.countryslug && !data.indusslug && !data.cityslug){
              window.history.pushState("Details", "Title", "{{ route('web-founders') }}/"+data.countryslug+"/all/all");
              $(".h1value").html(' Companies founders in ' + data.countryname);
            }
            else if(data.countryslug && data.indusslug && !data.cityslug){
              window.history.pushState("Details", "Title", "{{ route('web-founders') }}/"+data.countryslug+"/all/"+data.indusslug);
              $(".h1value").html(data.indusname + ' companies founders in ' + data.countryname);
            }
            else if(data.indusslug && !data.countryslug && !data.cityslug){
              window.history.pushState("Details", "Title", "{{ route('web-founders') }}/all/all"+data.indusslug);
              $(".h1value").html(data.indusname + ' companies founders');
            }
            else if(data.cityslug && !data.indusslug){
              window.history.pushState("Details", "Title", "{{ route('web-founders') }}/"+data.countryslug+"/"+data.cityslug+"/all");
              $(".h1value").html(' Companies founders in ' + data.cityname +','+ data.countryname);
            }
              else if(!data.keyword){
                console.log("abc");

                          var pathArray = window.location.pathname.split('/');
                          var acountry = pathArray[2];
                          var acity = pathArray[3];
                          var aindustry = pathArray[4];

                          $.ajax({
                            url: "/getid",
                            type: "get",
                            data:{
                              country:acountry,
                              city:acity,
                              industry:aindustry,
                            },
                            datatype: "json"
                          }).done(function(data){

                              country=data[0].country_id;
                              if (country==undefined) {
                                country='0';
                              }
                              city=data[1].city_id;
                              if (city==undefined) {
                                city='0';
                              }
                            console.log(city);
                              industry=data[2].industry_id;
                              if (industry==undefined) {
                                industry='';
                              }
                            getData(page,keyword,country,city,industry);
                          });

                          // window.history.pushState("Details", "Title", "http://contxto.netsolutionindia.com/companies");
                          $(".keyword").val('');
                        }
            var pathArray = window.location.pathname.split('/');
            var country = pathArray[2];
            if (country=='all') {
              country='';
            }
            var city = pathArray[3];
            if (city=='all') {
              city='';
            }
            var industry = pathArray[4];
            if (industry=='all') {
              industry='';
            }
            if(country && city && industry){
              console.log(1);
            var name= industry + ' companies founders  in ' + city +','+ country;
            }
            else if(country && !city && !industry){
              console.log(2);
            var name=' Companies founders  in ' + country;
            }
            else if(country && !city && industry){
              console.log(3);
            var name=industry + ' companies founders  in ' +country;
            }
            else if(!country && !city && industry){
              console.log(4);
            var name=industry + ' companies founders ';
            }
            else if(city && !industry){
              console.log(5);
            var name=' Companies founders  in ' + city +','+ country;
            }


                        data = {
                                       "@context": "https://schema.org",
                                       "@type": "Organization",
                                       "url": window.location.href,
                                       "sameAs":[
                                       "https://www.facebook.com/Contxto-495342720979440/",
                                       "https://www.instagram.com/contxto_/",
                                       "https://www.linkedin.com/company/contxto/",
                                       "https://twitter.com/contxto_"
                                       ],
                                       "@id":"https://www.contxto.com/en/#organization",
                                       "logo":"https://www.contxto.com/wp-content/uploads/2018/12/WhatsApp-Image-2018-12-04-at-1.53.19-PM.jpeg",
                                       "name": name
                                    };

                        //creating the script element and storing the JSON-LD

                        var script = document.createElement('script');
                        script.type = "application/ld+json";
                        script.innerHTML = JSON.stringify(data);
                        document.getElementsByTagName('head')[0].appendChild(script);

                        //OR

                        //storing the JSON-LD using ID
                        $("#dynamicJSONLD").html(JSON.stringify(data));
              // window.history.pushState("Details", "Title", "http://contxto.netsolutionindia.com/founders");
              // $(".keyword").val('');

            //location.hash = page;
        }).fail(function(jqXHR, ajaxOptions, thrownError){
              alert('No response from server');
        });
    }

    $( "#countries" ).change(function() {
        var country_id =$( this ).val();
        var keyword = $( ".keyword" ).val();
        var industries = $( "#industries" ).val();
        var sort = $( "#sort" ).val();
        if(country_id==0){
            getData(1,keyword,0,0,industries,sort);
            $("#cities").empty().html('<option value="0">All Cities</option>');
        }else{
            getCities(country_id);
            getData(1,keyword,country_id,0,industries,sort);
        }
    });

    $( "#sort" ).change(function() {
        var sort =$( this ).val();
        var keyword = $( ".keyword" ).val();
        var industries = $( "#industries" ).val();
        var country_id = $( "#countries" ).val();
        getData(1,keyword,country_id,0,industries,sort);
    });

    function getCities(country_id){
        $.ajax(
        {
            url: "/cities"+'?country_id=' + country_id,
            type: "get",
            datatype: "html"
        }).done(function(data){
            $("#cities").empty().html(data);
        }).fail(function(jqXHR, ajaxOptions, thrownError){
              alert('No response from server');
        });
    }

    $( "#cities" ).change(function() {
        var city_id =$( this ).val();
        var country_id =$("#countries").val();
        var industries =$("#industries").val();
        var sort = $( "#sort" ).val();
        var keyword = $( ".keyword" ).val();
        if(city_id==0){
            getData(1,keyword,country_id,0,industries,sort);
        }else{
            getData(1,keyword,country_id,city_id,industries,sort);
        }
    });

    $( "#industries" ).change(function() {
        var industries =$( this ).val();
        var sort = $( "#sort" ).val();
        if(industries.length==0){
            $(".fliterCount").text(0).hide();
        }else{
            $(".fliterCount").text(industries.length).show();
        }
        $(".section").addClass('collapsed');
        var city_id =$("#cities").val();
        var country_id =$("#countries").val();
        var keyword = $( ".keyword" ).val();
        getData(1,keyword,country_id,city_id,industries,sort);
    });
</script>


@stop

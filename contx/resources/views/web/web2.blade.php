
<!DOCTYPE html>

<html lang="en">
<title>Startups in  Mexico city
            •  •
             Mexico city Startups Lists
</title>

<style>
body:after{
    display:none;
    content: url(/img/bgcity/mexico.jpg);
}
</style>
<meta name="description" content="All start up companies based in Mexico city.   ">
<meta property="og:description" content="The top and newest startup companies based and built in Mexico city.   ">
<meta name="thumbnail" content="https://mexico.startups-list.com/img/tb2/mexico.jpg" />
<meta property="og:image" content="https://mexico.startups-list.com/img/tb2/mexico.jpg" />
<meta name="twitter:image:src" content="https://mexico.startups-list.com/img/tb2/mexico.jpg">

<meta property="og:site_name" content="Mexico city Startups List" />
<meta name="twitter:card" content="summary_large_image">
<meta name="twitter:site" content="@exit_list">
<meta name="twitter:creator" content="@exit_list">
<meta name="twitter:title" content="Mexico city Startups List">
<meta name="twitter:description" content="Tech startup companies built in Mexico city, the people behind them, the events they organize. ">

<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta http-equiv="cleartype" content="on">
<link href="data:image/x-icon;base64,AAABAAEAEBAQAAEABAAoAQAAFgAAACgAAAAQAAAAIAAAAAEABAAAAAAAgAAAAAAAAAAAAAAAEAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAAD/fwAA/j8AAPwfAAD4DwAA8AcAAOADAADAAwAAwAEAAIAAAACAAAAAgAAAAIAAAADAgQAAwcEAAPPnAAD//wAA" rel="icon" type="image/x-icon" />

<meta http-equiv="Cache-Control" content="no-cache, no-store, must-revalidate, max-stale=0, post-check=0, pre-check=0" />
<meta http-equiv="Pragma" content="no-cache" />
<meta http-equiv="Expires" content="-1" />
<meta http-equiv="Vary" content="*" />


<link rel="stylesheet" href="//netdna.bootstrapcdn.com/bootstrap/3.1.1/css/bootstrap.min.css">

<link href="//netdna.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.css" rel="stylesheet">

<link href='//fonts.googleapis.com/css?family=Roboto+Slab:400,100,300,700|Roboto:400,100,100italic,300,300italic,400italic,500,500italic,700,700italic,900,900italic' rel='stylesheet' type='text/css'>
<link rel="stylesheet" href="/css/layout.css">
<link rel="stylesheet" href="/css/about.css">
<link rel="stylesheet" href="/css/citypages/listings.css">
<link rel="stylesheet" href="/css/promoband.css">
<link href="/css/vendor/pushy.css" rel="stylesheet">

</head>
<body id="body">

<div class='block-mouse'></div>
<style>
.block-mouse {
  width: 100px;
  height: 100px;
  -webkit-transform: translate3d(0,0,0);
  transform: translate3d(0,0,0);
  -webkit-transform-origin: 0 0;
  position: fixed;
  top: 0;
  left: 0;
  z-index: 9;
  opacity: 0;
  pointer-events: none;
}

</style>
<img src="https://mexico.startups-list.com/img/tb2/mexico.jpg" style="display:none;" />

<nav class="pushy pushy-left">
<ul style="padding: 0;">
<li><a href="/" class="active">
<i class="fa fa-rocket fa-lg"></i>
Startups
</a></li>
<li><a href="/people">
<i class="fa fa-users fa-lg"></i>
People
</a></li>

<i class="fa fa-suitcase fa-lg"></i>
Jobs
</a>
<li><a href="/2015" class=" promo2015  ">
<i class="fa fa-asterisk fa-lg"></i>
2015 roadmap
</a></li>
</ul>
</nav>
<div class="site-overlay"></div>
<div class="modal translucid  fade " id="report_modal">
<div class="modal-header">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<h3>What's the matter with this startup?</h3>
</div>
<div class="modal-body">
<button class="report btn" data-type="acquired">It's been acquired</button>
<br>
<button class="report btn" data-type="dead-innactive">It's dead or innactive </button>
<br>
<button class="report btn" data-type="big-company">It's a big company</button>
<br>
<button class="report btn" data-type="not-here">It's not based here</button>
<br>
<button class="report btn" data-type="not-a-startup">It's not a startup company (VC, agencies, etc)</button>
<br>
<button class="report btn" data-type="missing">Missing data (website/icon)</button>
<br>
<button class="report btn" data-type="not-launched">It's not launched yet </button>
<br>
<button class="report btn" data-type="scam">Inappropriate / scammy</button>
<br>
<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">I don't like them</a>
</div>
<div class="modal-footer">
<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Cancel</a>
</div>
</div>
<div class="modal translucid  fade  category_modal" id="category_modal">
<div class="modal-header hidden">
</div>
<div class="modal-body">
<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
<div class="we-ll  " style="margin-left:35px; max-width:1000px;  ">
<h1>
Explore Mexico city Startups
</h1>
<nav class="categories">
<a class="btn btn-outline  " href="/" style="margin-right:5px; ">
All Startups</a>
<a class="btn btn-outline  " href="/startups/e-commerce" data-id="id29" style="margin-right:5px; ">E-Commerce </a>
<a class="btn btn-outline  " href="/startups/mobile" data-id="id3" style="margin-right:5px; ">Mobile </a>
<a class="btn btn-outline  " href="/startups/small_and medium businesses" data-id="id340" style="margin-right:5px; ">Small and Medium Businesses </a>
<a class="btn btn-outline  " href="/startups/social_media" data-id="id6" style="margin-right:5px; ">Social Media </a>
<a class="btn btn-outline  " href="/startups/education" data-id="id43" style="margin-right:5px; ">Education </a>
<a class="btn btn-outline  " href="/startups/digital_media" data-id="id4" style="margin-right:5px; ">Digital Media </a>
<a class="btn btn-outline  " href="/startups/financial_services" data-id="id17" style="margin-right:5px; ">Financial Services </a>
<a class="btn btn-outline  " href="/startups/mobile_commerce" data-id="id94" style="margin-right:5px; ">Mobile Commerce </a>
<a class="btn btn-outline  " href="/startups/retail" data-id="id32" style="margin-right:5px; ">Retail </a>
<a class="btn btn-outline  " href="/startups/health_care" data-id="id13" style="margin-right:5px; ">Health Care </a>
<a class="btn btn-outline  " href="/startups/SaaS" data-id="id10" style="margin-right:5px; ">SaaS </a>
<a class="btn btn-outline  " href="/startups/sales_and marketing" data-id="id51" style="margin-right:5px; ">Sales and Marketing </a>
<a class="btn btn-outline  " href="/startups/marketplaces" data-id="id162" style="margin-right:5px; ">Marketplaces </a>
<a class="btn btn-outline  " href="/startups/advertising" data-id="id34" style="margin-right:5px; ">Advertising </a>
<a class="btn btn-outline  " href="/startups/fashion" data-id="id93" style="margin-right:5px; ">Fashion </a>
<a class="btn btn-outline  " href="/startups/social_commerce" data-id="id230" style="margin-right:5px; ">Social Commerce </a>
<a class="btn btn-outline  " href="/startups/sports" data-id="id73" style="margin-right:5px; ">Sports </a>
<a class="btn btn-outline  " href="/startups/mobile_advertising" data-id="id205" style="margin-right:5px; ">Mobile Advertising </a>
<a class="btn btn-outline  " href="/startups/Latin_America" data-id="id151980" style="margin-right:5px; ">Latin America </a>
<a class="btn btn-outline  " href="/startups/entrepreneur" data-id="id1474" style="margin-right:5px; ">Entrepreneur </a>
<a class="btn btn-outline  " href="/startups/transportation" data-id="id631" style="margin-right:5px; ">Transportation </a>
<a class="btn btn-outline  " href="/startups/brand_marketing" data-id="id686" style="margin-right:5px; ">Brand Marketing </a>
<a class="btn btn-outline  " href="/startups/restaurants" data-id="id263" style="margin-right:5px; ">Restaurants </a>
<a class="btn btn-outline  " href="/startups/finance_technology" data-id="id2947" style="margin-right:5px; ">Finance Technology </a>
<a class="btn btn-outline  " href="/startups/social_media marketing" data-id="id450" style="margin-right:5px; ">Social Media Marketing </a>
<a class="btn btn-outline  " href="/startups/social_media platforms" data-id="id269" style="margin-right:5px; ">Social Media Platforms </a>
<a class="btn btn-outline  " href="/startups/food_and beverages" data-id="id72" style="margin-right:5px; ">Food and Beverages </a>
<a class="btn btn-outline  " href="/startups/events" data-id="id556" style="margin-right:5px; ">Events </a>
<a class="btn btn-outline  " href="/startups/ios" data-id="id408" style="margin-right:5px; ">iOS </a>
<a class="btn btn-outline  " href="/startups/personal_finance" data-id="id374" style="margin-right:5px; ">Personal Finance </a>
<span id="more" style="display:none;">
<a class="btn btn-outline  " href="/startups/health_care information technology" data-id="id175" style="margin-right:5px; ">Health Care Information Technology </a>
<a class="btn btn-outline  " href="/startups/pets" data-id="id675" style="margin-right:5px; ">Pets </a>
<a class="btn btn-outline  " href="/startups/location_based services" data-id="id57" style="margin-right:5px; ">Location Based Services </a>
<a class="btn btn-outline  " href="/startups/kids" data-id="id481" style="margin-right:5px; ">Kids </a>
<a class="btn btn-outline  " href="/startups/Mexico,_US, LatAm" data-id="id154243" style="margin-right:5px; ">Mexico, Us, Lat Am </a>
<a class="btn btn-outline  " href="/startups/telecommunications" data-id="id49" style="margin-right:5px; ">Telecommunications </a>
<a class="btn btn-outline  " href="/startups/android" data-id="id416" style="margin-right:5px; ">Android </a>
<a class="btn btn-outline  " href="/startups/enterprise_software" data-id="id12" style="margin-right:5px; ">Enterprise Software </a>
<a class="btn btn-outline  " href="/startups/big_data analytics" data-id="id13197" style="margin-right:5px; ">Big Data Analytics </a>
<a class="btn btn-outline  " href="/startups/travel_&amp; tourism" data-id="id9100" style="margin-right:5px; ">Travel &amp; Tourism </a>
<a class="btn btn-outline  " href="/startups/entertainment_industry" data-id="id2812" style="margin-right:5px; ">Entertainment Industry </a>
<a class="btn btn-outline  " href="/startups/governments" data-id="id251" style="margin-right:5px; ">Governments </a>
<a class="btn btn-outline  " href="/startups/design" data-id="id386" style="margin-right:5px; ">Design </a>
<a class="btn btn-outline  " href="/startups/crowdsourcing" data-id="id246" style="margin-right:5px; ">Crowdsourcing </a>
<a class="btn btn-outline  " href="/startups/k_12 education" data-id="id1469" style="margin-right:5px; ">K-12 Education </a>
<a class="btn btn-outline  " href="/startups/web_development" data-id="id3127" style="margin-right:5px; ">Web Development </a>
<a class="btn btn-outline  " href="/startups/finance" data-id="id321" style="margin-right:5px; ">Finance </a>
<a class="btn btn-outline  " href="/startups/consulting" data-id="id253" style="margin-right:5px; ">Consulting </a>
<a class="btn btn-outline  " href="/startups/startups" data-id="id448" style="margin-right:5px; ">Startups </a>
<a class="btn btn-outline  " href="/startups/music" data-id="id47" style="margin-right:5px; ">Music </a>
<a class="btn btn-outline  " href="/startups/clean_technology" data-id="id26" style="margin-right:5px; ">Clean Technology </a>
<a class="btn btn-outline  " href="/startups/deals" data-id="id424" style="margin-right:5px; ">Deals </a>
<a class="btn btn-outline  " href="/startups/technology" data-id="id1056" style="margin-right:5px; ">Technology </a>
<a class="btn btn-outline  " href="/startups/analytics" data-id="id52" style="margin-right:5px; ">Analytics </a>
<a class="btn btn-outline  " href="/startups/young_adults" data-id="id1153" style="margin-right:5px; ">Young Adults </a>
<a class="btn btn-outline  " href="/startups/consumer_lending" data-id="id2795" style="margin-right:5px; ">Consumer Lending </a>
<a class="btn btn-outline  " href="/startups/security" data-id="id62" style="margin-right:5px; ">Security </a>
<a class="btn btn-outline  " href="/startups/payments" data-id="id71" style="margin-right:5px; ">Payments </a>
<a class="btn btn-outline  " href="/startups/gamification" data-id="id1295" style="margin-right:5px; ">Gamification </a>
<a class="btn btn-outline  " href="/startups/Mexico" data-id="id154998" style="margin-right:5px; ">Mexico </a>
<a class="btn btn-outline  " href="/startups/crowdfunding" data-id="id3010" style="margin-right:5px; ">Crowdfunding </a>
<a class="btn btn-outline  " href="/startups/mobile_games" data-id="id490" style="margin-right:5px; ">Mobile Games </a>
<a class="btn btn-outline  " href="/startups/news" data-id="id201" style="margin-right:5px; ">News </a>
<a class="btn btn-outline  " href="/startups/retail_technology" data-id="id379" style="margin-right:5px; ">Retail Technology </a>
<a class="btn btn-outline  " href="/startups/construction" data-id="id680" style="margin-right:5px; ">Construction </a>
<a class="btn btn-outline  " href="/startups/real_estate" data-id="id16" style="margin-right:5px; ">Real Estate </a>
<a class="btn btn-outline  " href="/startups/publishing" data-id="id87" style="margin-right:5px; ">Publishing </a>
<a class="btn btn-outline  " href="/startups/personal_health" data-id="id418" style="margin-right:5px; ">Personal Health </a>
<a class="btn btn-outline  " href="/startups/health_and wellness" data-id="id1103" style="margin-right:5px; ">Health and Wellness </a>
<a class="btn btn-outline  " href="/startups/information_services" data-id="id65" style="margin-right:5px; ">Information Services </a>
<a class="btn btn-outline  " href="/startups/big_data" data-id="id198" style="margin-right:5px; ">Big Data </a>
<a class="btn btn-outline  " href="/startups/iphone" data-id="id277" style="margin-right:5px; ">iPhone </a>
<a class="btn btn-outline  " href="/startups/Europe" data-id="id152196" style="margin-right:5px; ">Europe </a>
<a class="btn btn-outline  " href="/startups/art" data-id="id407" style="margin-right:5px; ">Art </a>
<a class="btn btn-outline  " href="/startups/social_network media" data-id="id2531" style="margin-right:5px; ">Social Network Media </a>
<a class="btn btn-outline  " href="/startups/business_development" data-id="id1113" style="margin-right:5px; ">Business Development </a>
<a class="btn btn-outline  " href="/startups/cloud_computing" data-id="id38" style="margin-right:5px; ">Cloud Computing </a>
<a class="btn btn-outline  " href="/startups/teenagers" data-id="id912" style="margin-right:5px; ">Teenagers </a>
<a class="btn btn-outline  " href="/startups/online_travel" data-id="id78" style="margin-right:5px; ">Online Travel </a>
<a class="btn btn-outline  " href="/startups/travel" data-id="id1161" style="margin-right:5px; ">Travel </a>
<a class="btn btn-outline  " href="/startups/families" data-id="id314" style="margin-right:5px; ">Families </a>
<a class="btn btn-outline  " href="/startups/tablets" data-id="id615" style="margin-right:5px; ">Tablets </a>
<a class="btn btn-outline  " href="/startups/e_books" data-id="id498" style="margin-right:5px; ">E-Books </a>
<a class="btn btn-outline  " href="/startups/customer_service" data-id="id710" style="margin-right:5px; ">Customer Service </a>
<a class="btn btn-outline  " href="/startups/event_management" data-id="id16089" style="margin-right:5px; ">Event Management </a>
<a class="btn btn-outline  " href="/startups/mobile_payments" data-id="id473" style="margin-right:5px; ">Mobile Payments </a>
<a class="btn btn-outline  " href="/startups/creative_industries" data-id="id1262" style="margin-right:5px; ">Creative Industries </a>
<a class="btn btn-outline  " href="/startups/digital_entertainment" data-id="id1429" style="margin-right:5px; ">Digital Entertainment </a>
<a class="btn btn-outline  " href="/startups/women-focused" data-id="id553" style="margin-right:5px; ">Women-Focused </a>
<a class="btn btn-outline  " href="/startups/mobile_coupons" data-id="id443" style="margin-right:5px; ">Mobile Coupons </a>
<a class="btn btn-outline  " href="/startups/loyalty_programs" data-id="id224" style="margin-right:5px; ">Loyalty Programs </a>
<a class="btn btn-outline  " href="/startups/consumer_electronics" data-id="id286" style="margin-right:5px; ">Consumer Electronics </a>
<a class="btn btn-outline  " href="/startups/automotive" data-id="id69" style="margin-right:5px; ">Automotive </a>
<a class="btn btn-outline  " href="/startups/social_fundraising" data-id="id427" style="margin-right:5px; ">Social Fundraising </a>
<a class="btn btn-outline  " href="/startups/software" data-id="id841" style="margin-right:5px; ">Software </a>
<a class="btn btn-outline  " href="/startups/educational_games" data-id="id9912" style="margin-right:5px; ">Educational Games </a>
<a class="btn btn-outline  " href="/startups/United_States" data-id="id151002" style="margin-right:5px; ">United States </a>
<a class="btn btn-outline  " href="/startups/photography" data-id="id130" style="margin-right:5px; ">Photography </a>
<a class="btn btn-outline  " href="/startups/weddings" data-id="id555" style="margin-right:5px; ">Weddings </a>
<a class="btn btn-outline  " href="/startups/pharmaceuticals" data-id="id23" style="margin-right:5px; ">Pharmaceuticals </a>
<a class="btn btn-outline  " href="/startups/point_of sale" data-id="id1321" style="margin-right:5px; ">Point of Sale </a>
<a class="btn btn-outline  " href="/startups/Entrepreneurs" data-id="id156283" style="margin-right:5px; ">Entrepreneurs </a>
<a class="btn btn-outline  " href="/startups/young_entrepreneurs" data-id="id155497" style="margin-right:5px; ">Young Entrepreneurs </a>
<a class="btn btn-outline  " href="/startups/local_advertising" data-id="id673" style="margin-right:5px; ">Local Advertising </a>
<a class="btn btn-outline  " href="/startups/shopping" data-id="id692" style="margin-right:5px; ">Shopping </a>
<a class="btn btn-outline  " href="/startups/university_students" data-id="id2613" style="margin-right:5px; ">University Students </a>
<a class="btn btn-outline  " href="/startups/Cell_Phones" data-id="id152497" style="margin-right:5px; ">Cell Phones </a>
<a class="btn btn-outline  " href="/startups/parenting" data-id="id320" style="margin-right:5px; ">Parenting </a>
<a class="btn btn-outline  " href="/startups/all_students" data-id="id11600" style="margin-right:5px; ">All Students </a>
<a class="btn btn-outline  " href="/startups/social_travel" data-id="id1076" style="margin-right:5px; ">Social Travel </a>
<a class="btn btn-outline  " href="/startups/doctors" data-id="id223" style="margin-right:5px; ">Doctors </a>
<a class="btn btn-outline  " href="/startups/business_intelligence" data-id="id97" style="margin-right:5px; ">Business Intelligence </a>
<a class="btn btn-outline  " href="/startups/logistics" data-id="id1545" style="margin-right:5px; ">Logistics </a>
<a class="btn btn-outline  " href="/startups/communities" data-id="id181" style="margin-right:5px; ">Communities </a>
<a class="btn btn-outline  " href="/startups/general_public worldwide" data-id="id9407" style="margin-right:5px; ">General Public Worldwide </a>
<a class="btn btn-outline  " href="/startups/B2B" data-id="id42" style="margin-right:5px; ">B2B </a>
<a class="btn btn-outline  " href="/startups/services" data-id="id249" style="margin-right:5px; ">Services </a>
<a class="btn btn-outline  " href="/startups/trading" data-id="id1406" style="margin-right:5px; ">Trading </a>
<a class="btn btn-outline  " href="/startups/bitcoin" data-id="id93839" style="margin-right:5px; ">Bitcoin </a>
<a class="btn btn-outline  " href="/startups/venture_capital" data-id="id856" style="margin-right:5px; ">Venture Capital </a>
<a class="btn btn-outline  " href="/startups/discounts" data-id="id441" style="margin-right:5px; ">Discounts </a>
<a class="btn btn-outline  " href="/startups/billing" data-id="id16090" style="margin-right:5px; ">Billing </a>
<a class="btn btn-outline  " href="/startups/freelancers" data-id="id9847" style="margin-right:5px; ">Freelancers </a>
<a class="btn btn-outline  " href="/startups/search" data-id="id67" style="margin-right:5px; ">Search </a>
<a class="btn btn-outline  " href="/startups/social_recruiting" data-id="id431" style="margin-right:5px; ">Social Recruiting </a>
<a class="btn btn-outline  " href="/startups/graphics" data-id="id129" style="margin-right:5px; ">Graphics </a>
<a class="btn btn-outline  " href="/startups/predictive_analytics" data-id="id1470" style="margin-right:5px; ">Predictive Analytics </a>
<a class="btn btn-outline  " href="/startups/accounting" data-id="id585" style="margin-right:5px; ">Accounting </a>
<a class="btn btn-outline  " href="/startups/babies" data-id="id837" style="margin-right:5px; ">Babies </a>
<a class="btn btn-outline  " href="/startups/user_experience design" data-id="id11826" style="margin-right:5px; ">User Experience Design </a>
<a class="btn btn-outline  " href="/startups/commercial_real estate" data-id="id9708" style="margin-right:5px; ">Commercial Real Estate </a>
<a class="btn btn-outline  " href="/startups/gambling" data-id="id64" style="margin-right:5px; ">Gambling </a>
<a class="btn btn-outline  " href="/startups/development_platforms" data-id="id409" style="margin-right:5px; ">Development Platforms </a>
<a class="btn btn-outline  " href="/startups/peer-to-peer" data-id="id134" style="margin-right:5px; ">Peer-to-Peer </a>
<a class="btn btn-outline  " href="/startups/environmental_innovation" data-id="id2525" style="margin-right:5px; ">Environmental Innovation </a>
<a class="btn btn-outline  " href="/startups/green" data-id="id334" style="margin-right:5px; ">Green </a>
<a class="btn btn-outline  " href="/startups/credit" data-id="id577" style="margin-right:5px; ">Credit </a>
<a class="btn btn-outline  " href="/startups/public_transportation" data-id="id1116" style="margin-right:5px; ">Public Transportation </a>
<a class="btn btn-outline  " href="/startups/ventures_for good" data-id="id208" style="margin-right:5px; ">Ventures for Good </a>
<a class="btn btn-outline  " href="/startups/corporate_training" data-id="id2806" style="margin-right:5px; ">Corporate Training </a>
<a class="btn btn-outline  " href="/startups/sustainability" data-id="id1948" style="margin-right:5px; ">Sustainability </a>
<a class="btn btn-outline  " href="/startups/human_resources" data-id="id641" style="margin-right:5px; ">Human Resources </a>
<a class="btn btn-outline  " href="/startups/voip" data-id="id901" style="margin-right:5px; ">VoIP </a>
<a class="btn btn-outline  " href="/startups/franchises" data-id="id9259" style="margin-right:5px; ">Franchises </a>
<a class="btn btn-outline  " href="/startups/mobility" data-id="id12731" style="margin-right:5px; ">Mobility </a>
<a class="btn btn-outline  " href="/startups/hotels" data-id="id355" style="margin-right:5px; ">Hotels </a>
<a class="btn btn-outline  " href="/startups/turism" data-id="id3134" style="margin-right:5px; ">Turism </a>
<a class="btn btn-outline  " href="/startups/E-Learn" data-id="id157024" style="margin-right:5px; ">E Learn </a>
<a class="btn btn-outline  " href="/startups/Venue_and events" data-id="id154807" style="margin-right:5px; ">Venue And Events </a>
<a class="btn btn-outline  " href="/startups/adult" data-id="id63" style="margin-right:5px; ">Adult </a>
<a class="btn btn-outline  " href="/startups/loans" data-id="id156534" style="margin-right:5px; ">Loans </a>
<a class="btn btn-outline  " href="/startups/house" data-id="id157693" style="margin-right:5px; ">House </a>
<a class="btn btn-outline  " href="/startups/influencer_marketing" data-id="id155487" style="margin-right:5px; ">Influencer Marketing </a>
<a class="btn btn-outline  " href="/startups/high_schools" data-id="id10914" style="margin-right:5px; ">High Schools </a>
<a class="btn btn-outline  " href="/startups/home_decor" data-id="id10969" style="margin-right:5px; ">Home Decor </a>
<a class="btn btn-outline  " href="/startups/invest_online" data-id="id129903" style="margin-right:5px; ">Invest Online </a>
<a class="btn btn-outline  " href="/startups/social_innovation" data-id="id15213" style="margin-right:5px; ">Social Innovation </a>
<a class="btn btn-outline  " href="/startups/augmented_reality" data-id="id1054" style="margin-right:5px; ">Augmented Reality </a>
<a class="btn btn-outline  " href="/startups/insurance_companies" data-id="id14033" style="margin-right:5px; ">Insurance Companies </a>
<a class="btn btn-outline  " href="/startups/high_school students" data-id="id9600" style="margin-right:5px; ">High School Students </a>
<a class="btn btn-outline  " href="/startups/mobile_health" data-id="id1258" style="margin-right:5px; ">Mobile Health </a>
<a class="btn btn-outline  " href="/startups/Working_professionals" data-id="id156282" style="margin-right:5px; ">Working Professionals </a>
<a class="btn btn-outline  " href="/startups/video_games" data-id="id100" style="margin-right:5px; ">Video Games </a>
<a class="btn btn-outline  " href="/startups/small_companies" data-id="id151742" style="margin-right:5px; ">Small Companies </a>
<a class="btn btn-outline  " href="/startups/market_research" data-id="id342" style="margin-right:5px; ">Market Research </a>
<a class="btn btn-outline  " href="/startups/sales_automation" data-id="id309" style="margin-right:5px; ">Sales Automation </a>
<a class="btn btn-outline  " href="/startups/lifestyle_products" data-id="id693" style="margin-right:5px; ">Lifestyle Products </a>
<a class="btn btn-outline  " href="/startups/blogging_platforms" data-id="id552" style="margin-right:5px; ">Blogging Platforms </a>
<a class="btn btn-outline  " href="/startups/journalism" data-id="id640" style="margin-right:5px; ">Journalism </a>
<a class="btn btn-outline  " href="/startups/content_discovery" data-id="id1259" style="margin-right:5px; ">Content Discovery </a>
<a class="btn btn-outline  " href="/startups/advertising_platforms" data-id="id339" style="margin-right:5px; ">Advertising Platforms </a>
<a class="btn btn-outline  " href="/startups/nonprofits" data-id="id324" style="margin-right:5px; ">Nonprofits </a>
<a class="btn btn-outline  " href="/startups/musicians" data-id="id9166" style="margin-right:5px; ">Musicians </a>
<a class="btn btn-outline  " href="/startups/business_analytics" data-id="id2800" style="margin-right:5px; ">Business Analytics </a>
<a class="btn btn-outline  " href="/startups/soccer" data-id="id1433" style="margin-right:5px; ">Soccer </a>
<a class="btn btn-outline  " href="/startups/active_lifestyle" data-id="id16414" style="margin-right:5px; ">Active Lifestyle </a>
<a class="btn btn-outline  " href="/startups/web_design" data-id="id332" style="margin-right:5px; ">Web Design </a>
<a class="btn btn-outline  " href="/startups/virtual_worlds" data-id="id106" style="margin-right:5px; ">Virtual Worlds </a>
<a class="btn btn-outline  " href="/startups/estimation_and quoting" data-id="id2969" style="margin-right:5px; ">Estimation and Quoting </a>
<a class="btn btn-outline  " href="/startups/jewelry" data-id="id15202" style="margin-right:5px; ">Jewelry </a>
<a class="btn btn-outline  " href="/startups/professional_services" data-id="id384" style="margin-right:5px; ">Professional Services </a>
<a class="btn btn-outline  " href="/startups/social_games" data-id="id41" style="margin-right:5px; ">Social Games </a>
<a class="btn btn-outline  " href="/startups/content_delivery" data-id="id8962" style="margin-right:5px; ">Content Delivery </a>
<a class="btn btn-outline  " href="/startups/wholesale" data-id="id1122" style="margin-right:5px; ">Wholesale </a>
<a class="btn btn-outline  " href="/startups/ticketing" data-id="id604" style="margin-right:5px; ">Ticketing </a>
<a class="btn btn-outline  " href="/startups/handmade" data-id="id81663" style="margin-right:5px; ">Handmade </a>
<a class="btn btn-outline  " href="/startups/telephony" data-id="id103" style="margin-right:5px; ">Telephony </a>
<a class="btn btn-outline  " href="/startups/consumer_goods" data-id="id31" style="margin-right:5px; ">Consumer Goods </a>
<a class="btn btn-outline  " href="/startups/creative" data-id="id1414" style="margin-right:5px; ">Creative </a>
<a class="btn btn-outline  " href="/startups/leisure" data-id="id1458" style="margin-right:5px; ">Leisure </a>
<a class="btn btn-outline  " href="/startups/business_services" data-id="id45" style="margin-right:5px; ">Business Services </a>
<a class="btn btn-outline  " href="/startups/energy_management" data-id="id773" style="margin-right:5px; ">Energy Management </a>
<a class="btn btn-outline  " href="/startups/CRM" data-id="id83" style="margin-right:5px; ">CRM </a>
<a class="btn btn-outline  " href="/startups/business_travelers" data-id="id2453" style="margin-right:5px; ">Business Travelers </a>
<a class="btn btn-outline  " href="/startups/subscription_businesses" data-id="id1891" style="margin-right:5px; ">Subscription Businesses </a>
<a class="btn btn-outline  " href="/startups/games" data-id="id14" style="margin-right:5px; ">Games </a>
<a class="btn btn-outline  " href="/startups/local_businesses" data-id="id10341" style="margin-right:5px; ">Local Businesses </a>
<a class="btn btn-outline  " href="/startups/pc_gaming" data-id="id12773" style="margin-right:5px; ">PC Gaming </a>
<a class="btn btn-outline  " href="/startups/agriculture" data-id="id702" style="margin-right:5px; ">Agriculture </a>
<a class="btn btn-outline  " href="/startups/ipad" data-id="id360" style="margin-right:5px; ">iPad </a>
<a class="btn btn-outline  " href="/startups/recruiting" data-id="id468" style="margin-right:5px; ">Recruiting </a>
<a class="btn btn-outline  " href="/startups/windows_phone 7" data-id="id966" style="margin-right:5px; ">Windows Phone 7 </a>
<a class="btn btn-outline  " href="/startups/clean_energy" data-id="id27" style="margin-right:5px; ">Clean Energy </a>
<a class="btn btn-outline  " href="/startups/classifieds" data-id="id2811" style="margin-right:5px; ">Classifieds </a>
<a class="btn btn-outline  " href="/startups/taxis" data-id="id1097" style="margin-right:5px; ">Taxis </a>
<a class="btn btn-outline  " href="/startups/angel_investing" data-id="id15805" style="margin-right:5px; ">Angels </a>
</span>
<a class="label pill label-default" id="bt_more" type="button" href="#" style="margin-right:5px; opacity:0.5; ">More Categories... </a>
</nav>
</div>
</div>
<div class="modal-footer hidden">
<a href="#" class="btn" data-dismiss="modal" aria-hidden="true">Close</a>
</div>
</div>
<div id="container">
<nav style="display:none;">
<a href="https://amsterdam.startups-list.com" class=" "> Best Amsterdam Startups </a>
<a href="https://atlanta.startups-list.com" class=" "> Best Atlanta Startups </a>
<a href="https://austin.startups-list.com" class=" "> Best Austin Startups </a>
<a href="https://bangalore.startups-list.com" class=" "> Best Bangalore Startups </a>
<a href="https://barcelona.startups-list.com" class=" "> Best Barcelona Startups </a>
<a href="https://belo-horizonte.startups-list.com" class=" "> Best Belo Horizonte Startups </a>
<a href="https://berlin.startups-list.com" class=" "> Best Berlin Startups </a>
<a href="https://birmingham.startups-list.com" class=" "> Best Birmingham Startups </a>
<a href="https://bostonstartups.net" class=" "> Best Boston Startups </a>
<a href="https://boulderstartups.net" class=" "> Best Boulder Startups </a>
<a href="https://brisbane.startups-list.com" class=" "> Best Brisbane Startups </a>
<a href="https://bristol.startups-list.com" class=" "> Best Bristol Startups </a>
<a href="https://buenos-aires.startups-list.com" class=" "> Best Buenos Aires Startups </a>
<a href="https://cape-town.startups-list.com" class=" "> Best Cape Town Startups </a>
<a href="https://startupschicago.net" class=" "> Best Chicago Startups </a>
<a href="https://dallas.startups-list.com" class=" "> Best Dallas Startups </a>
<a href="https://denver.startups-list.com" class=" "> Best Denver Startups </a>
<a href="https://detroit.startups-list.com" class=" "> Best Detroit Startups </a>
<a href="https://dublin.startups-list.com" class=" "> Best Dublin Startups </a>
<a href="https://edinburgh.startups-list.com" class=" "> Best Edinburgh Startups </a>
<a href="https://frankfurt.startups-list.com" class=" "> Best Frankfurt Startups </a>
<a href="https://glasgow.startups-list.com" class=" "> Best Glasgow Startups </a>
<a href="https://hamburg.startups-list.com" class=" "> Best Hamburg Startups </a>
<a href="https://hong-kong.startups-list.com" class=" "> Best Hong Kong Startups </a>
<a href="https://houston.startups-list.com" class=" "> Best Houston Startups </a>
<a href="https://hyderabad.startups-list.com" class=" "> Best Hyderabad Startups </a>
<a href="https://istanbul.startups-list.com" class=" "> Best Istanbul Startups </a>
<a href="https://jerusalem.startups-list.com" class=" "> Best Jerusalem Startups </a>
<a href="https://johannesburg.startups-list.com" class=" "> Best Johannesburg Startups </a>
<a href="https://kuala-lumpur.startups-list.com" class=" "> Best Kuala Lumpur Startups </a>
<a href="https://kiev.startups-list.com" class=" "> Best Kyiv Startups </a>
<a href="https://las-vegas.startups-list.com" class=" "> Best Las Vegas Startups </a>
<a href="https://leeds.startups-list.com" class=" "> Best Leeds Startups </a>
<a href="https://lisbon.startups-list.com" class=" "> Best Lisboa Startups </a>
<a href="https://liverpool.startups-list.com" class=" "> Best Liverpool Startups </a>
<a href="https://london.startups-list.com" class=" "> Best London Startups </a>
<a href="https://startupsla.com" class=" "> Best Los Angeles Startups </a>
<a href="https://manchester.startups-list.com" class=" "> Best Manchester Startups </a>
<a href="https://melbourne.startups-list.com" class=" "> Best Melbourne Startups </a>
<a href="https://mexico.startups-list.com" class=" "> Best Mexico city Startups </a>
<a href="https://miami.startups-list.com" class=" "> Best Miami Startups </a>
<a href="https://builtinmtl.com" class=" "> Best Montréal Startups </a>
<a href="https://moscow.startups-list.com" class=" "> Best Moscow Startups </a>
<a href="https://mumbai.startups-list.com" class=" "> Best Mumbai Startups </a>
<a href="https://munich.startups-list.com" class=" "> Best Munich Startups </a>
<a href="https://new-delhi.startups-list.com" class=" "> Best New Delhi Startups </a>
<a href="https://new-orleans.startups-list.com" class=" "> Best New Orleans Startups </a>
<a href="https://nycstartups.net" class=" "> Best New York Startups </a>
<a href="https://nigeria.startups-list.com" class=" "> Best Nigeria Startups </a>
<a href="https://oakland.startups-list.com" class=" "> Best Oakland Startups </a>
<a href="https://oklahoma.startups-list.com" class=" "> Best Oklahoma Startups </a>
<a href="https://paris.startups-list.com" class=" "> Best Paris Startups </a>
<a href="https://philadelphia.startups-list.com" class=" "> Best Philadelphia Startups </a>
<a href="https://phoenix.startups-list.com" class=" "> Best Phoenix Startups </a>
<a href="https://portland.startups-list.com" class=" "> Best Portland Startups </a>
<a href="https://porto.startups-list.com" class=" "> Best Porto Startups </a>
<a href="https://prague.startups-list.com" class=" "> Best Prague Startups </a>
<a href="https://providence.startups-list.com" class=" "> Best Providence Startups </a>
<a href="https://quebecstartups.com" class=" "> Best Québec Startups </a>
<a href="https://sacramento.startups-list.com" class=" "> Best Sacramento Startups </a>
<a href="https://salt-lake-city.startups-list.com" class=" "> Best Salt Lake City Startups </a>
<a href="https://san-diego.startups-list.com" class=" "> Best San Diego Startups </a>
<a href="https://sanfrancisco.startups-list.com" class=" "> Best San Francisco Startups </a>
<a href="https://san-jose.startups-list.com" class=" "> Best San Jose Startups </a>
<a href="https://santiago.startups-list.com" class=" "> Best Santiago Startups </a>
<a href="https://sao-paulo.startups-list.com" class=" "> Best Sao Paulo Startups </a>
<a href="https://seattle.startups-list.com" class=" "> Best Seattle Startups </a>
<a href="https://singapore.startups-list.com" class=" "> Best Singapore Startups </a>
<a href="https://stockholm.startups-list.com" class=" "> Best Stockholm Startups </a>
<a href="https://sydney.startups-list.com" class=" "> Best Sydney Startups </a>
<a href="https://tel-aviv.startups-list.com" class=" "> Best Tel Aviv Startups </a>
<a href="https://tokyo.startups-list.com" class=" "> Best Tokyo Startups </a>
<a href="https://toronto.startups-list.com" class=" "> Best Toronto Startups </a>
<a href="https://transport.startups-list.com" class=" "> Best Transportation Startups </a>
<a href="https://vancouver.startups-list.com" class=" "> Best Vancouver Startups </a>
<a href="https://washington.startups-list.com" class=" "> Best Washington DC Startups </a>
<a href="https://waterloo.startups-list.com" class=" "> Best Waterloo Startups </a>
<a href="https://writers.startups-list.com" class=" "> Best Writing apps Startups </a>
</nav>
<header data-stellar-background-ratio="0.5" class="city mexico  " style="background-image: url('/img/bgcity/mexico.jpg'); background-size: cover; ">
<div class="menu-btn">&#9776; </div>

<form>
<select accesskey="S" id="cityDrop" style="position:absolute; right:10px; top:10px; color:#000;   ">
<option value="" selected>Other Cities</option>
<option value="https://atlanta.startups-list.com">Atlanta </option>
<option value="https://austin.startups-list.com">Austin </option>
<option value="https://bangalore.startups-list.com">Bangalore </option>
<option value="https://berlin.startups-list.com">Berlin </option>
<option value="https://bostonstartups.net">Boston </option>
<option value="https://boulderstartups.net">Boulder </option>
<option value="https://startupschicago.net">Chicago </option>
<option value="https://hong-kong.startups-list.com">Hong Kong </option>
<option value="https://houston.startups-list.com">Houston </option>
<option value="https://london.startups-list.com">London </option>
<option value="https://startupsla.com">Los Angeles </option>
<option value="https://melbourne.startups-list.com">Melbourne </option>
<option value="https://builtinmtl.com">Montréal </option>
<option value="https://moscow.startups-list.com">Moscow </option>
<option value="https://munich.startups-list.com">Munich </option>
<option value="https://new-delhi.startups-list.com">New Delhi </option>
<option value="https://nycstartups.net">New York </option>
<option value="https://paris.startups-list.com">Paris </option>
<option value="https://sanfrancisco.startups-list.com">San Francisco </option>
<option value="https://santiago.startups-list.com">Santiago </option>
<option value="https://sao-paulo.startups-list.com">Sao Paulo </option>
<option value="https://seattle.startups-list.com">Seattle </option>
<option value="https://singapore.startups-list.com">Singapore </option>
<option value="https://stockholm.startups-list.com">Stockholm </option>
<option value="https://sydney.startups-list.com">Sydney </option>
<option value="https://tel-aviv.startups-list.com">Tel Aviv </option>
<option value="https://tokyo.startups-list.com">Tokyo </option>
<option value="https://toronto.startups-list.com">Toronto </option>
<option value="https://vancouver.startups-list.com">Vancouver </option>
<option value="https://washington.startups-list.com">Washington DC </option>
<option value="https://startups-list.com">More... </option>
</select>
</form>
<h1 class="title">
Mexico city Startups List

</h1>
<form class="form" role=" " id="form_city_update" style="float:NOT; background: rgba(0,0,0,0.6); margin-top: 20px; width:340px; margin-left:50px; padding: 15px; position:absolute; top:100px; right:10px;">
<h4 style="margin: 0;">
Sign up for updates
</h4> <br>
<div class="form-group">
<div class="input-group" style="width:250px;">
<input type="email" class="form-control input-md" name="email" id="email" placeholder="Enter your Email ">
<span class="input-group-btn">
<button type="submit" class="btn btn-default btn-success btn-md" style="background:#0AC930"> Go &nbsp; <i class="fa fa-chevron-circle-right fa-lg-no" style="margin:0;"></i></button>
</span>
</div>
</div>
</form>
<div class="lead" id="thanks" style="display:none; float: right; width:400px; ">
<h3>
<br> <br>
<strong>Thank you!</strong> We'll keep you posted :)
<br> <br>
</h3>
</div>
<p class="lead  ">
Map of the local innovation industry.<br> Meet some of the best and newest startups based in Mexico city.</p>
<div id="tweet" class="  ">
<a href="https://twitter.com/share" data-text="The best reference around Mexico city Startups! #startup #data" class="twitter-share-button" data-url="https://mexico.startups-list.com" data-related="exit_list,felix_m" data-lang="en" data-size="large" data-count="horizontal" data-via="felix_m"> </a>
</div>
<nav>
<a href="/" class="active">
<i class="fa fa-rocket fa-lg"></i>
Startups
</a>
<a href="/people">
<i class="fa fa-users fa-lg"></i>
People
</a>
<a href="/2015" class=" promo2015 hidden ">
<i class="fa fa-exclamation-circle fa-lg"></i>
Get involved
</a>
</nav>
</header>
<div id="backtotop">
<i class="fa fa-arrow-circle-up fa-2x"></i>
</div>

<div class="list_nav hidden-phone">
<div class="pull-right hidden-phone">
<div role="presentation" class="dropdown " style="display: inline-block; margin-right: 20px;">
<a id="drop6" href="#" class="dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" role="button" aria-expanded="false">
Newest
<span class="caret"></span> 
</a>
<ul id="menu3" class="dropdown-menu" role="menu" aria-labelledby="drop6">
<li role="presentation"><a role="menuitem" tabindex="-1" href="?sort=top">Most Popular</a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="?sort=new">Newest </a></li>
<li role="presentation"><a role="menuitem" tabindex="-1" href="?sort=old">Oldest </a></li>
 <li role="presentation"><a role="menuitem" tabindex="-1" href="?sort=alpha">A — Z</a></li>

</ul>
</div>
<button type="button" href="#category_modal" class="bt_categories btn btn-default " data-toggle="modal">
<i class="fa fa-list-ul"></i> &nbsp;
Categories
</button>
</div>
<h4>

Explore
the newest
startups in Mexico city
</h4>
<div class="clearfix"></div>
<style>
#report_modal{background:transparent;}
</style>
</div>
<div class="well hide-phone hidden" style="margin-left:35px; width:50%; margin-top:40px; ">
<h2>Filter by categories</h2>
<nav class="categories">
<a class="label pill label-default" href="/startups/e-commerce" data-id="id29" style="margin-right:5px; ">E-Commerce </a>
<a class="label pill label-default" href="/startups/mobile" data-id="id3" style="margin-right:5px; ">Mobile </a>
<a class="label pill label-default" href="/startups/small_and medium businesses" data-id="id340" style="margin-right:5px; ">Small and Medium Businesses </a>
<a class="label pill label-default" href="/startups/social_media" data-id="id6" style="margin-right:5px; ">Social Media </a>
<a class="label pill label-default" href="/startups/education" data-id="id43" style="margin-right:5px; ">Education </a>
<a class="label pill label-default" href="/startups/digital_media" data-id="id4" style="margin-right:5px; ">Digital Media </a>
<a class="label pill label-default" href="/startups/financial_services" data-id="id17" style="margin-right:5px; ">Financial Services </a>
<a class="label pill label-default" href="/startups/mobile_commerce" data-id="id94" style="margin-right:5px; ">Mobile Commerce </a>
<a class="label pill label-default" href="/startups/retail" data-id="id32" style="margin-right:5px; ">Retail </a>
<a class="label pill label-default" href="/startups/health_care" data-id="id13" style="margin-right:5px; ">Health Care </a>
<a class="label pill label-default" href="/startups/SaaS" data-id="id10" style="margin-right:5px; ">SaaS </a>
<a class="label pill label-default" href="/startups/sales_and marketing" data-id="id51" style="margin-right:5px; ">Sales and Marketing </a>
<a class="label pill label-default" href="/startups/marketplaces" data-id="id162" style="margin-right:5px; ">Marketplaces </a>
<a class="label pill label-default" href="/startups/advertising" data-id="id34" style="margin-right:5px; ">Advertising </a>
<a class="label pill label-default" href="/startups/fashion" data-id="id93" style="margin-right:5px; ">Fashion </a>
<a class="label pill label-default" href="/startups/social_commerce" data-id="id230" style="margin-right:5px; ">Social Commerce </a>
<a class="label pill label-default" href="/startups/sports" data-id="id73" style="margin-right:5px; ">Sports </a>
<a class="label pill label-default" href="/startups/mobile_advertising" data-id="id205" style="margin-right:5px; ">Mobile Advertising </a>
<a class="label pill label-default" href="/startups/Latin_America" data-id="id151980" style="margin-right:5px; ">Latin America </a>
<a class="label pill label-default" href="/startups/entrepreneur" data-id="id1474" style="margin-right:5px; ">Entrepreneur </a>
<a class="label pill label-default" href="/startups/transportation" data-id="id631" style="margin-right:5px; ">Transportation </a>
<a class="label pill label-default" href="/startups/brand_marketing" data-id="id686" style="margin-right:5px; ">Brand Marketing </a>
<a class="label pill label-default" href="/startups/restaurants" data-id="id263" style="margin-right:5px; ">Restaurants </a>
<a class="label pill label-default" href="/startups/finance_technology" data-id="id2947" style="margin-right:5px; ">Finance Technology </a>
<a class="label pill label-default" href="/startups/social_media marketing" data-id="id450" style="margin-right:5px; ">Social Media Marketing </a>
<a class="label pill label-default" href="/startups/social_media platforms" data-id="id269" style="margin-right:5px; ">Social Media Platforms </a>
<a class="label pill label-default" href="/startups/food_and beverages" data-id="id72" style="margin-right:5px; ">Food and Beverages </a>
<a class="label pill label-default" href="/startups/events" data-id="id556" style="margin-right:5px; ">Events </a>
<a class="label pill label-default" href="/startups/ios" data-id="id408" style="margin-right:5px; ">iOS </a>
<a class="label pill label-default" href="/startups/personal_finance" data-id="id374" style="margin-right:5px; ">Personal Finance </a>
<span id="more" style="display:none;">
<a class="label pill label-default" href="/startups/health_care information technology" data-id="id175" style="margin-right:5px; ">Health Care Information Technology </a>
<a class="label pill label-default" href="/startups/pets" data-id="id675" style="margin-right:5px; ">Pets </a>
<a class="label pill label-default" href="/startups/location_based services" data-id="id57" style="margin-right:5px; ">Location Based Services </a>
<a class="label pill label-default" href="/startups/kids" data-id="id481" style="margin-right:5px; ">Kids </a>
<a class="label pill label-default" href="/startups/Mexico,_US, LatAm" data-id="id154243" style="margin-right:5px; ">Mexico, Us, Lat Am </a>
<a class="label pill label-default" href="/startups/telecommunications" data-id="id49" style="margin-right:5px; ">Telecommunications </a>
<a class="label pill label-default" href="/startups/android" data-id="id416" style="margin-right:5px; ">Android </a>
<a class="label pill label-default" href="/startups/enterprise_software" data-id="id12" style="margin-right:5px; ">Enterprise Software </a>
<a class="label pill label-default" href="/startups/big_data analytics" data-id="id13197" style="margin-right:5px; ">Big Data Analytics </a>
<a class="label pill label-default" href="/startups/travel_&amp; tourism" data-id="id9100" style="margin-right:5px; ">Travel &amp; Tourism </a>
<a class="label pill label-default" href="/startups/entertainment_industry" data-id="id2812" style="margin-right:5px; ">Entertainment Industry </a>
<a class="label pill label-default" href="/startups/governments" data-id="id251" style="margin-right:5px; ">Governments </a>
<a class="label pill label-default" href="/startups/design" data-id="id386" style="margin-right:5px; ">Design </a>
<a class="label pill label-default" href="/startups/crowdsourcing" data-id="id246" style="margin-right:5px; ">Crowdsourcing </a>
<a class="label pill label-default" href="/startups/k_12 education" data-id="id1469" style="margin-right:5px; ">K-12 Education </a>
<a class="label pill label-default" href="/startups/web_development" data-id="id3127" style="margin-right:5px; ">Web Development </a>

<a class="label pill label-default" href="/startups/finance" data-id="id321" style="margin-right:5px; ">Finance </a>
<a class="label pill label-default" href="/startups/consulting" data-id="id253" style="margin-right:5px; ">Consulting </a>
<a class="label pill label-default" href="/startups/startups" data-id="id448" style="margin-right:5px; ">Startups </a>
<a class="label pill label-default" href="/startups/music" data-id="id47" style="margin-right:5px; ">Music </a>
<a class="label pill label-default" href="/startups/clean_technology" data-id="id26" style="margin-right:5px; ">Clean Technology </a>
<a class="label pill label-default" href="/startups/deals" data-id="id424" style="margin-right:5px; ">Deals </a>
<a class="label pill label-default" href="/startups/technology" data-id="id1056" style="margin-right:5px; ">Technology </a>
<a class="label pill label-default" href="/startups/analytics" data-id="id52" style="margin-right:5px; ">Analytics </a>
<a class="label pill label-default" href="/startups/young_adults" data-id="id1153" style="margin-right:5px; ">Young Adults </a>
<a class="label pill label-default" href="/startups/consumer_lending" data-id="id2795" style="margin-right:5px; ">Consumer Lending </a>
<a class="label pill label-default" href="/startups/security" data-id="id62" style="margin-right:5px; ">Security </a>
<a class="label pill label-default" href="/startups/payments" data-id="id71" style="margin-right:5px; ">Payments </a>
<a class="label pill label-default" href="/startups/gamification" data-id="id1295" style="margin-right:5px; ">Gamification </a>
<a class="label pill label-default" href="/startups/Mexico" data-id="id154998" style="margin-right:5px; ">Mexico </a>
<a class="label pill label-default" href="/startups/crowdfunding" data-id="id3010" style="margin-right:5px; ">Crowdfunding </a>
<a class="label pill label-default" href="/startups/mobile_games" data-id="id490" style="margin-right:5px; ">Mobile Games </a>
<a class="label pill label-default" href="/startups/news" data-id="id201" style="margin-right:5px; ">News </a>
<a class="label pill label-default" href="/startups/retail_technology" data-id="id379" style="margin-right:5px; ">Retail Technology </a>
<a class="label pill label-default" href="/startups/construction" data-id="id680" style="margin-right:5px; ">Construction </a>
<a class="label pill label-default" href="/startups/real_estate" data-id="id16" style="margin-right:5px; ">Real Estate </a>
<a class="label pill label-default" href="/startups/publishing" data-id="id87" style="margin-right:5px; ">Publishing </a>
<a class="label pill label-default" href="/startups/personal_health" data-id="id418" style="margin-right:5px; ">Personal Health </a>
<a class="label pill label-default" href="/startups/health_and wellness" data-id="id1103" style="margin-right:5px; ">Health and Wellness </a>
<a class="label pill label-default" href="/startups/information_services" data-id="id65" style="margin-right:5px; ">Information Services </a>
<a class="label pill label-default" href="/startups/big_data" data-id="id198" style="margin-right:5px; ">Big Data </a>
<a class="label pill label-default" href="/startups/iphone" data-id="id277" style="margin-right:5px; ">iPhone </a>
<a class="label pill label-default" href="/startups/Europe" data-id="id152196" style="margin-right:5px; ">Europe </a>
<a class="label pill label-default" href="/startups/art" data-id="id407" style="margin-right:5px; ">Art </a>
<a class="label pill label-default" href="/startups/social_network media" data-id="id2531" style="margin-right:5px; ">Social Network Media </a>
<a class="label pill label-default" href="/startups/business_development" data-id="id1113" style="margin-right:5px; ">Business Development </a>
<a class="label pill label-default" href="/startups/cloud_computing" data-id="id38" style="margin-right:5px; ">Cloud Computing </a>
<a class="label pill label-default" href="/startups/teenagers" data-id="id912" style="margin-right:5px; ">Teenagers </a>
<a class="label pill label-default" href="/startups/online_travel" data-id="id78" style="margin-right:5px; ">Online Travel </a>
<a class="label pill label-default" href="/startups/travel" data-id="id1161" style="margin-right:5px; ">Travel </a>
<a class="label pill label-default" href="/startups/families" data-id="id314" style="margin-right:5px; ">Families </a>
<a class="label pill label-default" href="/startups/tablets" data-id="id615" style="margin-right:5px; ">Tablets </a>
<a class="label pill label-default" href="/startups/e_books" data-id="id498" style="margin-right:5px; ">E-Books </a>
<a class="label pill label-default" href="/startups/customer_service" data-id="id710" style="margin-right:5px; ">Customer Service </a>
<a class="label pill label-default" href="/startups/event_management" data-id="id16089" style="margin-right:5px; ">Event Management </a>
<a class="label pill label-default" href="/startups/mobile_payments" data-id="id473" style="margin-right:5px; ">Mobile Payments </a>
<a class="label pill label-default" href="/startups/creative_industries" data-id="id1262" style="margin-right:5px; ">Creative Industries </a>
<a class="label pill label-default" href="/startups/digital_entertainment" data-id="id1429" style="margin-right:5px; ">Digital Entertainment </a>
<a class="label pill label-default" href="/startups/women-focused" data-id="id553" style="margin-right:5px; ">Women-Focused </a>
<a class="label pill label-default" href="/startups/mobile_coupons" data-id="id443" style="margin-right:5px; ">Mobile Coupons </a>
<a class="label pill label-default" href="/startups/loyalty_programs" data-id="id224" style="margin-right:5px; ">Loyalty Programs </a>
<a class="label pill label-default" href="/startups/consumer_electronics" data-id="id286" style="margin-right:5px; ">Consumer Electronics </a>
<a class="label pill label-default" href="/startups/automotive" data-id="id69" style="margin-right:5px; ">Automotive </a>
<a class="label pill label-default" href="/startups/social_fundraising" data-id="id427" style="margin-right:5px; ">Social Fundraising </a>
<a class="label pill label-default" href="/startups/software" data-id="id841" style="margin-right:5px; ">Software </a>
<a class="label pill label-default" href="/startups/educational_games" data-id="id9912" style="margin-right:5px; ">Educational Games </a>
<a class="label pill label-default" href="/startups/United_States" data-id="id151002" style="margin-right:5px; ">United States </a>
<a class="label pill label-default" href="/startups/photography" data-id="id130" style="margin-right:5px; ">Photography </a>
<a class="label pill label-default" href="/startups/weddings" data-id="id555" style="margin-right:5px; ">Weddings </a>
<a class="label pill label-default" href="/startups/pharmaceuticals" data-id="id23" style="margin-right:5px; ">Pharmaceuticals </a>
<a class="label pill label-default" href="/startups/point_of sale" data-id="id1321" style="margin-right:5px; ">Point of Sale </a>
<a class="label pill label-default" href="/startups/Entrepreneurs" data-id="id156283" style="margin-right:5px; ">Entrepreneurs </a>
<a class="label pill label-default" href="/startups/young_entrepreneurs" data-id="id155497" style="margin-right:5px; ">Young Entrepreneurs </a>
<a class="label pill label-default" href="/startups/local_advertising" data-id="id673" style="margin-right:5px; ">Local Advertising </a>
<a class="label pill label-default" href="/startups/shopping" data-id="id692" style="margin-right:5px; ">Shopping </a>
<a class="label pill label-default" href="/startups/university_students" data-id="id2613" style="margin-right:5px; ">University Students </a>
<a class="label pill label-default" href="/startups/Cell_Phones" data-id="id152497" style="margin-right:5px; ">Cell Phones </a>
<a class="label pill label-default" href="/startups/parenting" data-id="id320" style="margin-right:5px; ">Parenting </a>
<a class="label pill label-default" href="/startups/all_students" data-id="id11600" style="margin-right:5px; ">All Students </a>
<a class="label pill label-default" href="/startups/social_travel" data-id="id1076" style="margin-right:5px; ">Social Travel </a>
<a class="label pill label-default" href="/startups/doctors" data-id="id223" style="margin-right:5px; ">Doctors </a>
<a class="label pill label-default" href="/startups/business_intelligence" data-id="id97" style="margin-right:5px; ">Business Intelligence </a>
<a class="label pill label-default" href="/startups/logistics" data-id="id1545" style="margin-right:5px; ">Logistics </a>
<a class="label pill label-default" href="/startups/communities" data-id="id181" style="margin-right:5px; ">Communities </a>
<a class="label pill label-default" href="/startups/general_public worldwide" data-id="id9407" style="margin-right:5px; ">General Public Worldwide </a>
<a class="label pill label-default" href="/startups/B2B" data-id="id42" style="margin-right:5px; ">B2B </a>
<a class="label pill label-default" href="/startups/services" data-id="id249" style="margin-right:5px; ">Services </a>
<a class="label pill label-default" href="/startups/trading" data-id="id1406" style="margin-right:5px; ">Trading </a>
<a class="label pill label-default" href="/startups/bitcoin" data-id="id93839" style="margin-right:5px; ">Bitcoin </a>
<a class="label pill label-default" href="/startups/venture_capital" data-id="id856" style="margin-right:5px; ">Venture Capital </a>
<a class="label pill label-default" href="/startups/discounts" data-id="id441" style="margin-right:5px; ">Discounts </a>
<a class="label pill label-default" href="/startups/billing" data-id="id16090" style="margin-right:5px; ">Billing </a>
<a class="label pill label-default" href="/startups/freelancers" data-id="id9847" style="margin-right:5px; ">Freelancers </a>
<a class="label pill label-default" href="/startups/search" data-id="id67" style="margin-right:5px; ">Search </a>
<a class="label pill label-default" href="/startups/social_recruiting" data-id="id431" style="margin-right:5px; ">Social Recruiting </a>
<a class="label pill label-default" href="/startups/graphics" data-id="id129" style="margin-right:5px; ">Graphics </a>
<a class="label pill label-default" href="/startups/predictive_analytics" data-id="id1470" style="margin-right:5px; ">Predictive Analytics </a>
<a class="label pill label-default" href="/startups/accounting" data-id="id585" style="margin-right:5px; ">Accounting </a>
<a class="label pill label-default" href="/startups/babies" data-id="id837" style="margin-right:5px; ">Babies </a>
<a class="label pill label-default" href="/startups/user_experience design" data-id="id11826" style="margin-right:5px; ">User Experience Design </a>
<a class="label pill label-default" href="/startups/commercial_real estate" data-id="id9708" style="margin-right:5px; ">Commercial Real Estate </a>
<a class="label pill label-default" href="/startups/gambling" data-id="id64" style="margin-right:5px; ">Gambling </a>
<a class="label pill label-default" href="/startups/development_platforms" data-id="id409" style="margin-right:5px; ">Development Platforms </a>
<a class="label pill label-default" href="/startups/peer-to-peer" data-id="id134" style="margin-right:5px; ">Peer-to-Peer </a>
<a class="label pill label-default" href="/startups/environmental_innovation" data-id="id2525" style="margin-right:5px; ">Environmental Innovation </a>
<a class="label pill label-default" href="/startups/green" data-id="id334" style="margin-right:5px; ">Green </a>
<a class="label pill label-default" href="/startups/credit" data-id="id577" style="margin-right:5px; ">Credit </a>
<a class="label pill label-default" href="/startups/public_transportation" data-id="id1116" style="margin-right:5px; ">Public Transportation </a>
<a class="label pill label-default" href="/startups/ventures_for good" data-id="id208" style="margin-right:5px; ">Ventures for Good </a>
<a class="label pill label-default" href="/startups/corporate_training" data-id="id2806" style="margin-right:5px; ">Corporate Training </a>
<a class="label pill label-default" href="/startups/sustainability" data-id="id1948" style="margin-right:5px; ">Sustainability </a>
<a class="label pill label-default" href="/startups/human_resources" data-id="id641" style="margin-right:5px; ">Human Resources </a>
<a class="label pill label-default" href="/startups/voip" data-id="id901" style="margin-right:5px; ">VoIP </a>
<a class="label pill label-default" href="/startups/franchises" data-id="id9259" style="margin-right:5px; ">Franchises </a>
<a class="label pill label-default" href="/startups/mobility" data-id="id12731" style="margin-right:5px; ">Mobility </a>
<a class="label pill label-default" href="/startups/hotels" data-id="id355" style="margin-right:5px; ">Hotels </a>
<a class="label pill label-default" href="/startups/turism" data-id="id3134" style="margin-right:5px; ">Turism </a>
<a class="label pill label-default" href="/startups/E-Learn" data-id="id157024" style="margin-right:5px; ">E Learn </a>
<a class="label pill label-default" href="/startups/Venue_and events" data-id="id154807" style="margin-right:5px; ">Venue And Events </a>
<a class="label pill label-default" href="/startups/adult" data-id="id63" style="margin-right:5px; ">Adult </a>
<a class="label pill label-default" href="/startups/loans" data-id="id156534" style="margin-right:5px; ">Loans </a>
<a class="label pill label-default" href="/startups/house" data-id="id157693" style="margin-right:5px; ">House </a>
<a class="label pill label-default" href="/startups/influencer_marketing" data-id="id155487" style="margin-right:5px; ">Influencer Marketing </a>
<a class="label pill label-default" href="/startups/high_schools" data-id="id10914" style="margin-right:5px; ">High Schools </a>
<a class="label pill label-default" href="/startups/home_decor" data-id="id10969" style="margin-right:5px; ">Home Decor </a>
<a class="label pill label-default" href="/startups/invest_online" data-id="id129903" style="margin-right:5px; ">Invest Online </a>
<a class="label pill label-default" href="/startups/social_innovation" data-id="id15213" style="margin-right:5px; ">Social Innovation </a>
<a class="label pill label-default" href="/startups/augmented_reality" data-id="id1054" style="margin-right:5px; ">Augmented Reality </a>
<a class="label pill label-default" href="/startups/insurance_companies" data-id="id14033" style="margin-right:5px; ">Insurance Companies </a>
<a class="label pill label-default" href="/startups/high_school students" data-id="id9600" style="margin-right:5px; ">High School Students </a>
<a class="label pill label-default" href="/startups/mobile_health" data-id="id1258" style="margin-right:5px; ">Mobile Health </a>
<a class="label pill label-default" href="/startups/Working_professionals" data-id="id156282" style="margin-right:5px; ">Working Professionals </a>
<a class="label pill label-default" href="/startups/video_games" data-id="id100" style="margin-right:5px; ">Video Games </a>
<a class="label pill label-default" href="/startups/small_companies" data-id="id151742" style="margin-right:5px; ">Small Companies </a>
<a class="label pill label-default" href="/startups/market_research" data-id="id342" style="margin-right:5px; ">Market Research </a>
<a class="label pill label-default" href="/startups/sales_automation" data-id="id309" style="margin-right:5px; ">Sales Automation </a>
<a class="label pill label-default" href="/startups/lifestyle_products" data-id="id693" style="margin-right:5px; ">Lifestyle Products </a>
<a class="label pill label-default" href="/startups/blogging_platforms" data-id="id552" style="margin-right:5px; ">Blogging Platforms </a>
<a class="label pill label-default" href="/startups/journalism" data-id="id640" style="margin-right:5px; ">Journalism </a>
<a class="label pill label-default" href="/startups/content_discovery" data-id="id1259" style="margin-right:5px; ">Content Discovery </a>
<a class="label pill label-default" href="/startups/advertising_platforms" data-id="id339" style="margin-right:5px; ">Advertising Platforms </a>
<a class="label pill label-default" href="/startups/nonprofits" data-id="id324" style="margin-right:5px; ">Nonprofits </a>
<a class="label pill label-default" href="/startups/musicians" data-id="id9166" style="margin-right:5px; ">Musicians </a>
<a class="label pill label-default" href="/startups/business_analytics" data-id="id2800" style="margin-right:5px; ">Business Analytics </a>
<a class="label pill label-default" href="/startups/soccer" data-id="id1433" style="margin-right:5px; ">Soccer </a>
<a class="label pill label-default" href="/startups/active_lifestyle" data-id="id16414" style="margin-right:5px; ">Active Lifestyle </a>
<a class="label pill label-default" href="/startups/web_design" data-id="id332" style="margin-right:5px; ">Web Design </a>
<a class="label pill label-default" href="/startups/virtual_worlds" data-id="id106" style="margin-right:5px; ">Virtual Worlds </a>
<a class="label pill label-default" href="/startups/estimation_and quoting" data-id="id2969" style="margin-right:5px; ">Estimation and Quoting </a>
<a class="label pill label-default" href="/startups/jewelry" data-id="id15202" style="margin-right:5px; ">Jewelry </a>
<a class="label pill label-default" href="/startups/professional_services" data-id="id384" style="margin-right:5px; ">Professional Services </a>
<a class="label pill label-default" href="/startups/social_games" data-id="id41" style="margin-right:5px; ">Social Games </a>
<a class="label pill label-default" href="/startups/content_delivery" data-id="id8962" style="margin-right:5px; ">Content Delivery </a>
<a class="label pill label-default" href="/startups/wholesale" data-id="id1122" style="margin-right:5px; ">Wholesale </a>
<a class="label pill label-default" href="/startups/ticketing" data-id="id604" style="margin-right:5px; ">Ticketing </a>
<a class="label pill label-default" href="/startups/handmade" data-id="id81663" style="margin-right:5px; ">Handmade </a>
<a class="label pill label-default" href="/startups/telephony" data-id="id103" style="margin-right:5px; ">Telephony </a>
<a class="label pill label-default" href="/startups/consumer_goods" data-id="id31" style="margin-right:5px; ">Consumer Goods </a>
<a class="label pill label-default" href="/startups/creative" data-id="id1414" style="margin-right:5px; ">Creative </a>
<a class="label pill label-default" href="/startups/leisure" data-id="id1458" style="margin-right:5px; ">Leisure </a>
<a class="label pill label-default" href="/startups/business_services" data-id="id45" style="margin-right:5px; ">Business Services </a>
<a class="label pill label-default" href="/startups/energy_management" data-id="id773" style="margin-right:5px; ">Energy Management </a>
<a class="label pill label-default" href="/startups/CRM" data-id="id83" style="margin-right:5px; ">CRM </a>
<a class="label pill label-default" href="/startups/business_travelers" data-id="id2453" style="margin-right:5px; ">Business Travelers </a>
<a class="label pill label-default" href="/startups/subscription_businesses" data-id="id1891" style="margin-right:5px; ">Subscription Businesses </a>
<a class="label pill label-default" href="/startups/games" data-id="id14" style="margin-right:5px; ">Games </a>
<a class="label pill label-default" href="/startups/local_businesses" data-id="id10341" style="margin-right:5px; ">Local Businesses </a>
<a class="label pill label-default" href="/startups/pc_gaming" data-id="id12773" style="margin-right:5px; ">PC Gaming </a>
<a class="label pill label-default" href="/startups/agriculture" data-id="id702" style="margin-right:5px; ">Agriculture </a>
<a class="label pill label-default" href="/startups/ipad" data-id="id360" style="margin-right:5px; ">iPad </a>
<a class="label pill label-default" href="/startups/recruiting" data-id="id468" style="margin-right:5px; ">Recruiting </a>
<a class="label pill label-default" href="/startups/windows_phone 7" data-id="id966" style="margin-right:5px; ">Windows Phone 7 </a>
<a class="label pill label-default" href="/startups/clean_energy" data-id="id27" style="margin-right:5px; ">Clean Energy </a>
<a class="label pill label-default" href="/startups/classifieds" data-id="id2811" style="margin-right:5px; ">Classifieds </a>
<a class="label pill label-default" href="/startups/taxis" data-id="id1097" style="margin-right:5px; ">Taxis </a>
<a class="label pill label-default" href="/startups/angel_investing" data-id="id15805" style="margin-right:5px; ">Angels </a>
</span>
<a class="label pill label-default" id="bt_more" type="button" href="#" style="margin-right:5px; opacity:0.5; ">More Categories... </a>
</nav>
</div>
<pre class="hide">   
   </pre>
<pre class="hide">  
  </pre>
<div class="clearfix"></div>
<div class="hide alert ">
<br>---------<br>
<a href="http://twitter.com/@3wlarroja" target="_blank">@3wlarroja</a>,<a href="http://twitter.com/gotalkie" target="_blank">gotalkie</a>,<a href="https://twitter.com/dasolucionesit" target="_blank">dasolucionesit</a>,<a href="http://twitter.com/Cafetzin" target="_blank">Cafetzin</a>,<a href="https://twitter.com/Prendanet" target="_blank">Prendanet</a>,<a href="https://twitter.com/prendamovil" target="_blank">prendamovil</a>,<a href="https://twitter.com/DrGadget_Mx" target="_blank">DrGadget_Mx</a>,<a href="http://twitter.com/@CPachamama" target="_blank">@CPachamama</a>,<a href="https://twitter.com/Bajatuseguro1" target="_blank">Bajatuseguro1</a>,<a href="http://twitter.com/@animento" target="_blank">@animento</a>,<a href="https://twitter.com/SanesyMx" target="_blank">SanesyMx</a>,<a href="https://twitter.com/mostradorV1" target="_blank">mostradorV1</a>,<a href="http://twitter.com/@petwik2" target="_blank">@petwik2</a>,<a href="http://www.twitter.com/juun_club" target="_blank">juun_club</a>,<a href="http://twitter.com/@voyconmimascota" target="_blank">@voyconmimascota</a>,<a href="http://www.twitter.com/magisterchef" target="_blank">magisterchef</a>,<a href="http://twitter.com/@doggycommunity" target="_blank">@doggycommunity</a>,<a href="https://twitter.com/sparkywatchdog" target="_blank">sparkywatchdog</a>,<a href="https://www.twitter.com/drofskath" target="_blank">drofskath</a>,<a href="http://twitter.com/@ficusnet" target="_blank">@ficusnet</a>,<a href="https://twitter.com/Finple" target="_blank">Finple</a>,<a href="https://twitter.com/teragu" target="_blank">teragu</a>,<a href="http://twitter.com/twitter_" target="_blank">twitter_</a>,<a href="https://twitter.com/HotStreetApp" target="_blank">HotStreetApp</a>,<a href="http://www.twitter.com/instadrinks" target="_blank">instadrinks</a>,<a href="http://twitter.com/ncleo_co" target="_blank">ncleo_co</a>,<a href="https://twitter.com/alrconsultores" target="_blank">alrconsultores</a>,<a href="https://twitter.com/GrilloAlerta" target="_blank">GrilloAlerta</a>,<a href="http://twitter.com/@PortalVaivén" target="_blank">@PortalVaivén</a>,<a href="http://twitter.com/iintercambiame" target="_blank">iintercambiame</a>,<a href="http://twitter.com/@musicsoundlab" target="_blank">@musicsoundlab</a>,<a href="http://twitter.com/@latinventurehub" target="_blank">@latinventurehub</a>,<a href="http://twitter.com/@cuantificare" target="_blank">@cuantificare</a>,<a href="http://twitter.com/@futbolitosinc" target="_blank">@futbolitosinc</a>,<a href="https://twitter.com/welovegivu" target="_blank">welovegivu</a>,<a href="http://twitter.com/@neurocrowd" target="_blank">@neurocrowd</a>,<a href="http://www.twitter.com/emeequis32" target="_blank">emeequis32</a>,<a href="https://twitter.com/codigofacilito" target="_blank">codigofacilito</a>,<a href="http://twitter.com/@RockOnFireMX" target="_blank">@RockOnFireMX</a>,<a href="https://twitter.com/Mexbt" target="_blank">Mexbt</a>,<a href="http://twitter.com/coinbatch" target="_blank">coinbatch</a>,<a href="https://twitter.com/Come_Casero" target="_blank">Come_Casero</a>,<a href="http://www.twitter.com/Quincenas" target="_blank">Quincenas</a>,<a href="http://twitter.com/@chulelmx" target="_blank">@chulelmx</a>,<a href="http://twitter.com/getsparkjoy" target="_blank">getsparkjoy</a>,<a href="http://twitter.com/@Carloslopezjone" target="_blank">@Carloslopezjone</a>,<a href="https://twitter.com/ofertify" target="_blank">ofertify</a>,<a href="https://twitter.com/Planetary_HQ" target="_blank">Planetary_HQ</a>,<a href="http://www.twitter.com/canvazmx" target="_blank">canvazmx</a>,<a href="http://twitter.com/emedics_" target="_blank">emedics_</a>,<a href="https://twitter.com/LoyaltyRefunds" target="_blank">LoyaltyRefunds</a>,<a href="http://twitter.com/qompit" target="_blank">qompit</a>,<a href="http://twitter.com/nodistroti" target="_blank">nodistroti</a>,<a href="http://twitter.com/@jose_smeke" target="_blank">@jose_smeke</a>,<a href="http://twitter.com/mejormudanza" target="_blank">mejormudanza</a>,<a href="https://twitter.com/Algebraix" target="_blank">Algebraix</a>,<a href="http://twitter.com/@Jorgejery" target="_blank">@Jorgejery</a>,<a href="https://twitter.com/konfiomx" target="_blank">konfiomx</a>,<a href="http://twitter.com/elysasalud" target="_blank">elysasalud</a>,<a href="http://twitter.com/YadaMx" target="_blank">YadaMx</a>,<a href="http://twitter.com/#ameve" target="_blank">#ameve</a>,<a href="http://twitter.com/@allgomx" target="_blank">@allgomx</a>,<a href="http://twitter.com/@cardinaleyewear" target="_blank">@cardinaleyewear</a>,<a href="http://twitter.com/helloverbal" target="_blank">helloverbal</a>,<a href="https://twitter.com/epic_queen" target="_blank">epic_queen</a>,<a href="https://twitter.com/factigomx" target="_blank">factigomx</a>,<a href="https://twitter.com/RocketColombia" target="_blank">RocketColombia</a>,<a href="http://twitter.com/@buensocio" target="_blank">@buensocio</a>,<a href="http://twitter.com/@casachilangacom" target="_blank">@casachilangacom</a>,<a href="http://twitter.com/@zaveapp" target="_blank">@zaveapp</a>,<a href="http://twitter.com/@crearton.com.mx" target="_blank">@crearton.com.mx</a>,<a href="http://twitter.com/@PROUNIVE" target="_blank">@PROUNIVE</a>,<a href="https://twitter.com/Eduproy" target="_blank">Eduproy</a>,<a href="http://www.twitter.com/opencolours" target="_blank">opencolours</a>,<a href="https://twitter.com/kmsmex" target="_blank">kmsmex</a>,<a href="http://twitter.com/TADE_MX" target="_blank">TADE_MX</a>,<a href="http://twitter.com/@zerowastemx" target="_blank">@zerowastemx</a>,<a href="https://twitter.com/DinoDocManager" target="_blank">DinoDocManager</a>,<a href="https://twitter.com/FacturamaMX" target="_blank">FacturamaMX</a>,<a href="http://twitter.com/gamerstrike" target="_blank">gamerstrike</a>, <br>---------<br>
<a href="https://twitter.com/armandodiseos" target="_blank">armandodiseos</a>,<a href="http://twitter.com/@mobilender_mx" target="_blank">@mobilender_mx</a>,<a href="http://twitter.com/@mayorear" target="_blank">@mayorear</a>,<a href="http://twitter.com/autochilango" target="_blank">autochilango</a>,<a href="http://twitter.com/@thecitywifisyou" target="_blank">@thecitywifisyou</a>,<a href="http://twitter.com/@funpracticemx" target="_blank">@funpracticemx</a>,<a href="http://twitter.com/@99minutos" target="_blank">@99minutos</a>,<a href="http://twitter.com/@ArtKon" target="_blank">@ArtKon</a>,<a href="http://twitter.com/@zabvio" target="_blank">@zabvio</a>,<a href="https://twitter.com/entucelcom" target="_blank">entucelcom</a>,<a href="https://twitter.com/Central_df" target="_blank">Central_df</a>,<a href="https://twitter.com/Caos_Inc" target="_blank">Caos_Inc</a>,<a href="http://twitter.com/@alartmx" target="_blank">@alartmx</a>,<a href="http://www.twitter.com/glokalbox" target="_blank">glokalbox</a>,<a href="http://twitter.com/descubre_la" target="_blank">descubre_la</a>,<a href="http://twitter.com/@VoyalDoc" target="_blank">@VoyalDoc</a>,<a href="http://twitter.com/humanamx" target="_blank">humanamx</a>,<a href="http://twitter.com/@instafitmx" target="_blank">@instafitmx</a>,<a href="http://twitter.com/@saludfacil1" target="_blank">@saludfacil1</a>,<a href="http://www.twitter.com/fricmartinez" target="_blank">fricmartinez</a>,<a href="http://twitter.com/BrandMe" target="_blank">BrandMe</a>,<a href="http://twitter.com/FundwiseMx" target="_blank">FundwiseMx</a>,<a href="http://twitter.com/@prestadero" target="_blank">@prestadero</a>,<a href="http://twitter.com/@troompet" target="_blank">@troompet</a>,<a href="http://twitter.com/@Descfira" target="_blank">@Descfira</a>,<a href="https://twitter.com/cotizaycontrata" target="_blank">cotizaycontrata</a>,<a href="https://twitter.com/SeenapseIt" target="_blank">SeenapseIt</a>,<a href="http://twitter.com/@hostspotmx" target="_blank">@hostspotmx</a>,<a href="http://www.twitter.com/teknobuilding" target="_blank">teknobuilding</a>,<a href="https://twitter.com/UhmaSalud" target="_blank">UhmaSalud</a>,<a href="http://twitter.com/@GONTAGCo" target="_blank">@GONTAGCo</a>,<a href="http://twitter.com/TelusMX" target="_blank">TelusMX</a>,<a href="https://twitter.com/AnipassJp" target="_blank">AnipassJp</a>,<a href="http://twitter.com/@Clubguaf" target="_blank">@Clubguaf</a>,<a href="http://twitter.com/@_sentisis" target="_blank">@_sentisis</a>,<a href="https://twitter.com/FondeadoraMx" target="_blank">FondeadoraMx</a>,<a href="https://twitter.com/ridesmx" target="_blank">ridesmx</a>,<a href="http://twitter.com/pachucodigital" target="_blank">pachucodigital</a>,<a href="https://twitter.com/SoloStocks‎" target="_blank">SoloStocks‎</a>,<a href="http://twitter.com/quantmogul" target="_blank">quantmogul</a>,<a href="https://twitter.com/cinemasapp" target="_blank">cinemasapp</a>,<a href="https://twitter.com/ReddocMX" target="_blank">ReddocMX</a>,<a href="https://twitter.com/lokuaz_com" target="_blank">lokuaz_com</a>,<a href="http://www.twitter.com/elbazar_mx" target="_blank">elbazar_mx</a>,<a href="http://twitter.com/@petsymx" target="_blank">@petsymx</a>,<a href="http://twitter.com/@regresapronto" target="_blank">@regresapronto</a>,<a href="http://www.twitter.com/plasmr" target="_blank">plasmr</a>,<a href="https://twitter.com/WishbirdMexico" target="_blank">WishbirdMexico</a>,<a href="http://twitter.com/dameunaventonmx" target="_blank">dameunaventonmx</a>,<a href="https://twitter.com/headhunters_mx" target="_blank">headhunters_mx</a>,<a href="http://twitter.com/qritiqr" target="_blank">qritiqr</a>,<a href="http://twitter.com/creadescuentos" target="_blank">creadescuentos</a>,<a href="http://twitter.com/@MoneyMenttor" target="_blank">@MoneyMenttor</a>,<a href="http://twitter.com/@axeleratum" target="_blank">@axeleratum</a>,<a href="http://twitter.com/dadaroom" target="_blank">dadaroom</a>,<a href="http://twitter.com/stringpublisher" target="_blank">stringpublisher</a>,<a href="https://twitter.com/GarageCoders" target="_blank">GarageCoders</a>,<a href="http://twitter.com/POPContacts" target="_blank">POPContacts</a>,<a href="http://twitter.com/clicab.com" target="_blank">clicab.com</a>,<a href="http://twitter.com/rowsapp" target="_blank">rowsapp</a>,<a href="http://twitter.com/incuvox" target="_blank">incuvox</a>,<a href="http://twitter.com/@Nokincard" target="_blank">@Nokincard</a>,<a href="http://twitter.com/@bebitosmx" target="_blank">@bebitosmx</a>,<a href="http://twitter.com/Urban360News" target="_blank">Urban360News</a>,<a href="https://twitter.com/YaxiApp" target="_blank">YaxiApp</a>,<a href="http://twitter.com/midrco" target="_blank">midrco</a>,<a href="https://twitter.com/rubber_it" target="_blank">rubber_it</a>,<a href="http://twitter.com/@etrainingmx" target="_blank">@etrainingmx</a>,<a href="http://twitter.com/gasofiacom" target="_blank">gasofiacom</a>,<a href="http://twitter.com/@vistankcom" target="_blank">@vistankcom</a>,<a href="http://www.twitter.com/ecolike" target="_blank">ecolike</a>,<a href="http://twitter.com/aventones" target="_blank">aventones</a>,<a href="http://www.twitter.com/venddo" target="_blank">venddo</a>,<a href="http://twitter.com/@osotrava" target="_blank">@osotrava</a>,<a href="http://twitter.com/sindelantalmx" target="_blank">sindelantalmx</a>,<a href="http://twitter.com/@GoVentureMexico" target="_blank">@GoVentureMexico</a>,<a href="http://twitter.com/@fogonomada" target="_blank">@fogonomada</a>,<a href="http://twitter.com/@swstrategists" target="_blank">@swstrategists</a>,<a href="https://twitter.com/iBazarMX" target="_blank">iBazarMX</a>,<a href="http://twitter.com/cuponzote" target="_blank">cuponzote</a>,
<br>
</div>

<div class='ads' id="ads" style=" ">
<style>
      /* todo: hide on mobile */
      @media (max-width: 800px) {
        .ad_desk{ display:none;}
      }
      @media (min-width: 800px) {
        #ads{
          display:block;
          width: 340px;
          height: 100%;
          min-height: 600px;
          float:right;
          text-align: left;
        }
        #wrap{ margin-right: 340px; }
      }
      </style>

<ins class="adsbygoogle ad_desk" style="display:inline-block;width:300px;height:600px" data-ad-client="ca-pub-7691543043621777" data-ad-slot="4859111230"></ins>

<ins class="adsbygoogle" style="display:inline-block;width:200px;height:90px" data-ad-client="ca-pub-7691543043621777" data-ad-slot="8242754835"></ins>

</div>
<div class="ads top-links">

<ins class="adsbygoogle" style="display:inline-block;width:728px;height:15px" data-ad-client="ca-pub-7691543043621777" data-ad-slot="1277891235"></ins>
</div>
<div id="wrap" class="wrap" style="  ">

<div class="startupNOT goog_ad">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" type="8c10126f8364ee93003dfebe-text/javascript"></script>

<ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-7691543043621777" data-ad-slot="5228724435"></ins>
<script type="8c10126f8364ee93003dfebe-text/javascript">
(adsbygoogle = window.adsbygoogle || []).push({});
</script>
</div>

<div data-id="619489" data-name="4yopping" data-href="http://www.4yopping.com" class="card startup   id29 id94 id2462  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.4yopping.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.4yopping.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/619489-80efd76be93d20c5bb8245bb19c7f4cf-thumb_jpg.jpg?buster=1424797081" property="image" alt="4yopping -  e-commerce mobile commerce internet of things" />
<h1 property="name">
4yopping
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The new generation of e-commerce platforms<br></strong>
We are developing a [software, hardware and data] technology suite that will cover all the steps in the e-commerce process. Focused on the mobile user, IoT and using Big Data techniques we want to contribute to the future of e-commerce. It is time to focus on the ...</p>
</a>
</div>

<div data-id="602286" data-name="Towi" data-href="http://wwww.towi.com.mx" class="card startup   id481 id841 id1469 id9912  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://wwww.towi.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://wwww.towi.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/602286-f062b3666834f48bb09cf91d420bc16f-thumb_jpg.jpg?buster=1423291943" property="image" alt="Towi -  kids software k 12 education educational games" />
<h1 property="name">
Towi
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Brain training program for kids<br></strong>
Towi is an online game-based learning platform for kids between 4-12 years old who are not attracted to traditional educational methods, aimed at developing their cognitive abilities. As a Software as a Service and Software as a Product, Towi consists of personalized ...</p>
</a>
</div>

<div data-id="584349" data-name="Keubble" data-href="http://www.keubble.com" class="card startup   id154243  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.keubble.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.keubble.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/584349-2ecc9c00fa4007e5f7be6a99f0cbdbd1-thumb_jpg.jpg?buster=1421764926" property="image" alt="Keubble -  Mexico, US, LatAm" />
<h1 property="name">
Keubble
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Curiosidades<br></strong>
Curiosidades de distintas categorias, que no dejan de sorprender - Keubble</p>
</a>
</div>

<div data-id="568186" data-name="Paneles" data-href="http://www.paneles.co" class="card startup   id29 id52  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.paneles.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.paneles.co" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/568186-3deba9c09bcd00b50527077be879b121-thumb_jpg.jpg?buster=1420223321" property="image" alt="Paneles -  e-commerce analytics" />
<h1 property="name">
Paneles
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Paneles Solares para Latinoamerica<br></strong>
Reduce tu consumo energético con paneles solares.</p>
</a>
</div>

<div data-id="567308" data-name="plusU" data-href="http://www.plusu.me" class="card startup   id151002 id151980 id152196 id153227  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.plusu.me" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.plusu.me" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/567308-d49cac1f40df9904fe77f1e44f9a7814-thumb_jpg.jpg?buster=1420064134" property="image" alt="plusU -  United States Latin America Europe Ciudad de México, Mexico" />
<h1 property="name">
plusU
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Bring social life back to the real world.<br></strong>
In a hyper connected world, plusU brings social life back to the real world by connecting people online to go live experiences offline.  Seeking to capitalize a $4B USD opportunity in an untapped Latin American market.</p>
</a>
</div>

<div class="startupNOT goog_ad second_rect">
<script async src="//pagead2.googlesyndication.com/pagead/js/adsbygoogle.js" type="8c10126f8364ee93003dfebe-text/javascript"></script>

<ins class="adsbygoogle" style="display:inline-block;width:300px;height:250px" data-ad-client="ca-pub-7691543043621777" data-ad-slot="5228724435"></ins>
</div>
 <div data-id="555041" data-name="larroja.com" data-href="http://larroja.com" class="card startup   id154243  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://larroja.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@3wlarroja" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://larroja.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/555041-a3863773fb1a4aa96f6f465522d1b0e9-thumb_jpg.jpg?buster=1418277665" property="image" alt="larroja.com -  Mexico, US, LatAm" />
<h1 property="name">
larroja.com
</h1>
<span href="http://twitter.com/@3wlarroja" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We create perfect softwares<br></strong>
We are deloppers. We can to create all type of software to web pages, apps(iOS, and Android), Smart Tv and Google glass.</p>
</a>
</div>

<div data-id="541640" data-name="Grupo IBC Online" data-href="http://www.grupoibc.net" class="card startup   id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.grupoibc.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.grupoibc.net" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/541640-6a7619c1634668a74d2ece587981c7c4-thumb_jpg.jpg?buster=1416792224" property="image" alt="Grupo IBC Online -  Latin America" />
<h1 property="name">
Grupo IBC Online
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Soluciones de negocio para emprendedores<br></strong>
Soluciones de Negocio para vender a traves de internet con estrategias de marketing definidas por analisis web.</p>
</a>
</div>

<div data-id="530060" data-name="Talkie" data-href="https://gotalkie.co" class="card startup   id3 id10 id49 id901  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://gotalkie.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/gotalkie" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="https://gotalkie.co" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/530060-ce0091e13151dd77a8e9f1f174807ea2-thumb_jpg.jpg?buster=1415473500" property="image" alt="Talkie -  mobile SaaS telecommunications voip" />
<h1 property="name">
Talkie
</h1>
<span href="http://twitter.com/gotalkie" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Communication tool for businesses and customers<br></strong>
Talkie is a voice communication tool to allow businesses communicate with its customers in a more efficient way. It offers social authentication and contextual calling to improve the response time and go &quot;straight to the point&quot; in the call.
Customers get to experience ...</p>
</a>
</div>

<div data-id="529905" data-name="Mac Mex Photgraphy" data-href="http://www.macmexphotography.com/" class="card startup   id130 id407 id555 id556 id157074  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.macmexphotography.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.macmexphotography.com/" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/529905-8eef3b7de36fee28d2a6361c1d27df77-thumb_jpg.jpg?buster=1415436637" property="image" alt="Mac Mex Photgraphy -  photography art weddings events Aerial Videography" />
<h1 property="name">
Mac Mex Photgraphy
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> instagram<br></strong>
Puerto Vallarta, Wedding, Photographers</p>
</a>
</div>

<div data-id="529258" data-name="DA Soluciones IT" data-href="https://www.dasolucionesit.com.mx" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://www.dasolucionesit.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://es-es.facebook.com/AmbitoDigitalMexico" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/dasolucionesit" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="https://www.dasolucionesit.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/529258-6863cfb890bf25fd9def5a5c536d49da-thumb_jpg.jpg?buster=1415368074" property="image" alt="DA Soluciones IT - " />
<h1 property="name">
DA Soluciones IT
</h1>
<span href="https://twitter.com/dasolucionesit" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Software Development<br></strong>
</p>
</a>
</div>

<div data-id="516794" data-name="cafe del profesor" data-href="https://www.facebook.com/Cafedelprofe?ref=bookmarks" class="card startup   id1492 id9259 id155788  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://www.facebook.com/Cafedelprofe?ref=bookmarks" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://www.facebook.com/Cafedelprofe?ref=bookmarks" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/516794-1ece8e2aecb3bb0f7b572f748c8eb325-thumb_jpg.jpg?buster=1413964226" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="cafe del profesor -  coffee franchises Sales" />
<h1 property="name">
cafe del profesor
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> coffee shop franchising<br></strong>
not convencional coffee shop franchising , luxury experience, signature recipe and ancient rituals with coffee</p>
</a>
</div>

<div data-id="511300" data-name="theMobys" data-href="https://themobys.com/" class="card startup   id3 id2531 id12731 id151435  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

 <a href="https://themobys.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://themobys.com/" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/511300-28e83363f4e6013a5898f6835942340c-thumb_jpg.jpg?buster=1413337923" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="theMobys -  mobile social network media mobility Urban Mobility" />
<h1 property="name">
theMobys
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social network for mobility<br></strong>
theMobys is a social network in which people will be able to know 'where are' and also how 'they are moving' their theMobys
Can be used by merchants which want to sell a product trough paypal brand showed on the middle of the road users, friends, students, workers, ...</p>
</a>
</div>

<div data-id="511254" data-name="Mobintum" data-href="http://www.mobintum.com" class="card startup   id23 id32 id1113 id1321  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.mobintum.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.mobintum.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/511254-ef7410dc12f829d1ab55afba354a2ad7-thumb_jpg.jpg?buster=1414596183" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Mobintum -  pharmaceuticals retail business development point of sale" />
<h1 property="name">
Mobintum
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile Point of Sale<br></strong>
Mobintum is a mobility solution for the current needs of the market where the efficiency and value of information are key growth companies and the competition factor.
Within 5 min gives your sales force the tools needed anywhere and grow your business.
The main ...</p>
</a>
</div>

<div data-id="510979" data-name="Cash &amp; Grow" data-href="http://on progress" class="card startup   id340 id789 id156283  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://on progress" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://on progress" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/510979-6a20525f899875f6105c04910cf195d1-thumb_jpg.jpg?buster=1413318066" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cash &amp; Grow -  small and medium businesses micro enterprises Entrepreneurs" />
<h1 property="name">
Cash &amp; Grow
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Fair treatment pawn shop <br></strong>
Cash and Grow is the first fair treatment pawn shop in Mexico whose market includes mini, small and medium entreprises. They represent 98% of the country's market but only 14% are able to receive loans from the traditional national bank.
Due to this market emergency ...</p>
</a>
</div>

<div data-id="510780" data-name="GraphMe: Graphing Calculator" data-href="http://qed.mx/" class="card startup   id408 id416 id3127  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://qed.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://qed.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/510780-39a1c03931a87afdafaa34459e1b2e34-thumb_jpg.jpg?buster=1413305863" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="GraphMe: Graphing Calculator -  ios android web development" />
<h1 property="name">
GraphMe: Graphing Calculator
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Advanced mathematics graphing calculator<br></strong>
We have created a powerful and easy to use graphing calculator that allows you to plot real-valued functions, trajectories, and even vector fields on the cartesian plane. Everything with amazing retina graphics.
GraphMe takes full advantage of the multitouch capabilities ...</p>
</a>
</div>

<div data-id="510303" data-name="Photo Book" data-href="http://www.facebook.com/davidcohens" class="card startup   id130 id372 id464 id948  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

 <a href="http://www.facebook.com/davidcohens" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.facebook.com/davidcohens" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/510303-46212a4635390efe670251027283295a-thumb_jpg.jpg?buster=1413259084" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Photo Book -  photography photo sharing photo editing printing" />
<h1 property="name">
Photo Book
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> make photo albums &amp; print<br></strong>
Make an print photo albums whit digital photos (vacation, special event etc.)</p>
</a>
</div>

<div data-id="510048" data-name="Utradex" data-href="http://utradex.com" class="card startup   id12 id340 id1474 id155497  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://utradex.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://utradex.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/510048-a8c333e71b6de870e1a46cefd436dd0f-thumb_jpg.jpg?buster=1413238063" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Utradex -  enterprise software small and medium businesses entrepreneur young entrepreneurs" />
<h1 property="name">
Utradex
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> International business calculator online<br></strong>
Utradex is landed cost tool, which is available on the web, it´s an easy and quick tool for import/export business. It is a practical and reliable bridge between an international idea and the real world.</p>
</a>
</div>

<div data-id="509851" data-name="KA'AM" data-href="http://kaam.com.mx" class="card startup   id435 id2636 id151267 id154057  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://kaam.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://kaam.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/509851-eaff9d589462a2141c514ac786e5d479-thumb_jpg.jpg?buster=1413227722" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="KA'AM -  beauty organic Natural Skin Care Donations" />
<h1 property="name">
KA'AM
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> ORGANIC NATURAL BEAUTY PRODUCTS<br></strong>
WE ARE STARTING WITH ONE PRODUCT ONLY, A LIP BALM THAT HELP US RAISE MONEY FOR ILLNESS THIS YEAR WILL BE DIABETES.</p>
</a>
</div>

<div data-id="509489" data-name="Cafetzin " data-href="http://www.cafetzin.com.mx" class="card startup   id162 id263 id355  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.cafetzin.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/cafetzin" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/Cafetzin" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.cafetzin.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/509489-2ca8757869dc108dfb7a01430e2f1372-thumb_jpg.jpg?buster=1413197672" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cafetzin  -  marketplaces restaurants hotels" />
<h1 property="name">
Cafetzin
</h1>
<span href="http://twitter.com/Cafetzin" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> mexican speciality coffee<br></strong>
Cafetzin is an innovative coffee brand, we select the best Mexican coffee available in the market. Our low-fire hand crafted roast, delights the most exquisite palates. By providing a perfect espresso coffee cup.</p>
</a>
</div>

<div data-id="509035" data-name="Tequila Sunset Hostal" data-href="http://www.tequilasunsethostal.com.mx" class="card startup   id263 id3134 id15325  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.tequilasunsethostal.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.tequilasunsethostal.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/509035-779ae3d012cdb7c4e9e67a0b2b34b5d7-thumb_jpg.jpg?buster=1413131121" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tequila Sunset Hostal -  restaurants turism brewing" />
<h1 property="name">
Tequila Sunset Hostal
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> hostal / bar / craft beer production / brewing school <br></strong>
For the moment we are a hostal ubicated in Morelia´s downtown, soon we will be open a bar at the roof of the place with craft beer production (microbrewer) for us and more restaurants in the area, also we will have a brewing school, brewing supplies and we let ...</p>
</a>
</div>

<div data-id="508819" data-name="Cuatro semanas " data-href="http://cuatrosemanas.com" class="card startup   id43 id157024  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://cuatrosemanas.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://cuatrosemanas.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508819-fd3317cb714612049c8e07f9c3c43749-thumb_jpg.jpg?buster=1413074112" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cuatro semanas  -  education E-Learn" />
<h1 property="name">
Cuatro semanas
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Four week project based courses <br></strong>
Based in Mexico, Cuatro semanas is focusing on the topics and skills that really matter to build and maintain websites. We are starting with our first class about HTML / CSS and developing other courses that complement that knowledge.
In Cuatro semanas we think ...</p>
</a>
</div>

<div data-id="508796" data-name="SLM Sistemas" data-href="http://www.slmsistemas.com" class="card startup   id38 id49 id306 id10494  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.slmsistemas.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.slmsistemas.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508796-13bf1f7c5fe12f48064a2aec1da589ba-thumb_jpg.jpg?buster=1413071666" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SLM Sistemas -  cloud computing telecommunications cloud management cloud infrastructure" />
<h1 property="name">
SLM Sistemas
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Cloud Orchestrator and Marketplace in Latam<br></strong>
Our products enable datacenters and Telcos to massive sale Cloud Products like IaaS, DaaS, Backup, Web Presence, Email, etc, we recently sign 2 contracts with telecommunications companies, one in Colombia and other in Mexico.
We are talking with 4 more telcos ...</p>
</a>
</div>

<div data-id="508788" data-name="Distrito Web" data-href="http://www.distritoweb.com.mx" class="card startup   id348 id673  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.distritoweb.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.distritoweb.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508788-42a647c0392c59e300c99dd849f84342-thumb_jpg.jpg?buster=1413070379" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Distrito Web -  e commerce platforms local advertising" />
<h1 property="name">
Distrito Web
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Website, E-commerce Development<br></strong>
Distrito Shop, E-commerce Platform and ROI Calculator</p>
</a>
</div>

<div data-id="508775" data-name="Limonadapp" data-href="http://limonadapp.com" class="card startup   id94 id340 id692 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://limonadapp.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://limonadapp.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508775-e936f716e26823dab69add09c90ca8cf-thumb_jpg.jpg?buster=1413264633" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Limonadapp -  mobile commerce small and medium businesses shopping big data analytics" />
<h1 property="name">
Limonadapp
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Best promos anywhere<br></strong>
Limonadapp is the smart way to do your shopping and promote your products and/or services anywhere with BigData analytics
Limonadapp gathers information about users, like preferences, characteristics and pleasures through social networks to provide the best designed ...</p>
</a>
</div>

<div data-id="508694" data-name="Yieppa" data-href="http://www.sferea.com" class="card startup   id556 id154807  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.sferea.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.sferea.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508694-fe0c8a08b9a5ba61c05cc6428357907e-thumb_jpg.jpg?buster=1413319043" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Yieppa -  events Venue and events" />
<h1 property="name">
Yieppa
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Best local events with discounted tickets<br></strong>
Yieppa es una plataforma móvil que te muestra los mejores eventos cercanos a tu ubicación filtrando por categoría, fecha y horario de cada evento, además de permitirte comprar boletos a precios de remate.</p>
</a>
</div>

<div data-id="508678" data-name="Comercializadora Union Global " data-href="http://www.unitrade.com.mx" class="card startup   id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.unitrade.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.unitrade.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508678-88eb70828af22093ec046ba8697f6b69-thumb_jpg.jpg?buster=1413055783" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Comercializadora Union Global  -  Latin America" />
<h1 property="name">
Comercializadora Union Global
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Trading, Consulting, Commerce<br></strong>
Knowledge and understanding of the dynamics in the Asian market.
• Advice and validation on selection of the manufacturers.
• Visits to manufacturers and constant monitoring of activities.
• Inspections throughout the process
• Improvement in product quality and ...</p>
</a>
</div>

<div data-id="508640" data-name="Hangy" data-href="http://It's only an idea : (" class="card startup   id3 id63 id912 id1153  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://It's only an idea : (" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://It's only an idea : (" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/508640-de08ad766cf828ab9e58cfe54d3c25c9-thumb_jpg.jpg?buster=1413054500" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Hangy -  mobile adult teenagers young adults" />
<h1 property="name">
Hangy
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> A Web App to meet new people and go out.<br></strong>
Sometimes you feel &quot;forever alone&quot;.
Get a life! Meet new people! There are plenty of things to do!
But how?
Hangy is a Web App to have a social life, enjoy new experiences and meet new people with similar interests. Also you can discover new places and activities ...</p>
</a>
</div>

<div data-id="507056" data-name="APOYOS EXPRESS" data-href="http://www.apoyosexpress.com" class="card startup   id321 id374 id2613 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.apoyosexpress.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.apoyosexpress.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/507056-fb40c3047016df4ad29316c449ed46b6-thumb_jpg.jpg?buster=1412870576" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="APOYOS EXPRESS -  finance personal finance university students finance technology" />
<h1 property="name">
APOYOS EXPRESS
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> small credits online<br></strong>
We provide credits online from 1,000 to 3,000 pesos per person in one day, the payday is from 7 to 30 day and is only one payment.</p>
</a>
</div>

<div data-id="506505" data-name="Souvenir-Box" data-href="http://souvenirsartesanales.com" class="card startup   id162 id361 id9100 id152871  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://souvenirsartesanales.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://souvenirsartesanales.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/506505-07ed965a93a84a495eb8dae81bfb8828-thumb_jpg.jpg?buster=1412807666" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Souvenir-Box -  marketplaces tourism travel &amp; tourism Gift Boxes" />
<h1 property="name">
Souvenir-Box
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> discover the world in a box<br></strong>
A monthly box with craft souvenirs for a specific country/town. Discover the culture of many countries. The travellers should love.</p>
</a>
</div>

<div data-id="506451" data-name="Prendanet" data-href="http://prendanet.mx/" class="card startup   id156534  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://prendanet.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/prendanetmx?fref=ts" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/Prendanet" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://prendanet.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/506451-2737059a6bb893b3d9c40894405ac1c8-thumb_jpg.jpg?buster=1412803966" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Prendanet -  loans" />
<h1 property="name">
Prendanet
</h1>
<span href="https://twitter.com/Prendanet" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Prendanet is a financial institution that provides personal loans.<br></strong>
PRENDANET is a financial institution that provides loans from $ 5,000 up to $ 10 million pesos in less than 24 hours. We accept stones and metals, watches, cars, motorcycles, planes, helicopters and buildings. We are a pawn company.</p>
</a>
</div>

<div data-id="506290" data-name="Prendamovil" data-href="http://www.prendamovil.com/" class="card startup   id2795  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.prendamovil.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/Prendamovil?fref=ts" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/prendamovil" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.prendamovil.com/" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/506290-928c4d3f4aa00f886e839fb1e9c0bf04-thumb_jpg.jpg?buster=1412793236" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Prendamovil -  consumer lending" />
<h1 property="name">
Prendamovil
</h1>
<span href="https://twitter.com/prendamovil" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We are a financial institution that provides immediate personal loans.<br></strong>
We are a financial institution that provides immediate personal loans, from $ 5,000 to $ 500,000 pesos in just 50 minutes with the warranty of your car. We are a pawn company.</p>
</a>
</div>

<div data-id="506171" data-name="Dr. Gadget" data-href="http://drgadget.mx/" class="card startup   id152497  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://drgadget.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/drgadgetreparaciones?fref=ts" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/DrGadget_Mx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://drgadget.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/506171-cd07dfe4ff11a350a927a4321dfdac1a-thumb_jpg.jpg?buster=1412788111" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Dr. Gadget -  Cell Phones" />
<h1 property="name">
Dr. Gadget
</h1>
<span href="https://twitter.com/DrGadget_Mx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Repair of gadgets. <br></strong>
We repair and deliver your gadgets at the door of your home. We also buy your broken gadgets.</p>
</a>
</div>

<div data-id="506166" data-name="SOCIAL DEVELOPMENT TECHNOLOGIES" data-href="http://En construcción" class="card startup   id6 id450  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://En construcción" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://En construcción" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/506166-2b6dd3ada838444f8d1005b48db6062e-thumb_jpg.jpg?buster=1412784854" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SOCIAL DEVELOPMENT TECHNOLOGIES -  social media social media marketing" />
<h1 property="name">
SOCIAL DEVELOPMENT TECHNOLOGIES
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> La Opinión Pública real de cualquier cosa.<br></strong>
Mi nombre es Saul Alvidrez, tengo 26 años, y la plataforma que estamos desarrollando permite a sus usuarios:
- Informarse sobre la opinión pública de cualquier cosa, idea, situación, personaje, producto, etc., en cualquier momento.
- Consultar el estado de ánimo ...</p>
</a>
</div>

<div data-id="505843" data-name="Pinustech" data-href="http://www.webhostermexico.com/mycast" class="card startup   id62 id320 id340 id157693  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.webhostermexico.com/mycast" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.webhostermexico.com/mycast" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505843-701372873b628387829bf5172d55ffe0-thumb_jpg.jpg?buster=1412744249" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pinustech -  security parenting small and medium businesses house" />
<h1 property="name">
Pinustech
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> MyCast: Quick pictures/snap shots and video of what you need, where you need it.<br></strong>
Forget about bulky CCTV systems for your home, office or business. MyCast is a wifi connected camera, with a very sexy and stylished cubic shape that will allow you to get snapshots or video through an app, of anything you can think of. For example, you can place ...</p>
</a>
</div>

<div data-id="505841" data-name="Pinustech" data-href="http://www.webhostermexico.com/deliveryangel" class="card startup   id340 id11600 id83526 id153558  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.webhostermexico.com/deliveryangel" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.webhostermexico.com/deliveryangel" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505841-0c1b2d4d3a51a0f86da439e580015929-thumb_jpg.jpg?buster=1412743555" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pinustech -  small and medium businesses all students computers everyone with a smartphone" />
<h1 property="name">
Pinustech
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Delivery Angel, e-mail delivery for busy and distracted people<br></strong>
Delivery Angel, as its name indicates, is an angel that will protect you and give you advice so you don't ever again fall on the classic &quot;sins&quot; of responding inappropriately to the wrong person, at the wrong time. You can also schedule your e-mails if you decide ...</p>
</a>
</div>

<div data-id="505825" data-name="Iamerito" data-href="http://www.iamerito.com" class="card startup   id73 id1226 id154243 id157810  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.iamerito.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.iamerito.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505825-a148d49e610b1eeffb91a0f5a2d99fbc-thumb_jpg.jpg?buster=1412741356" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Iamerito -  sports universities Mexico, US, LatAm Athletes" />
<h1 property="name">
Iamerito
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Linkedin for rising athletes <br></strong>
It is a platform for connecting rising athletes and coaches seeking for talent. The athletes (users) use the platform for free to know where and when are the scouting’s happening as to promote themselves by uploading personal relevant information as metrics and ...</p>
</a>
</div>

<div data-id="505802" data-name="a" data-href="http://www.google.com" class="card startup   id154807  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.google.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.google.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505802-09d0debfcb730cc72160399d31461def-thumb_jpg.jpg?buster=1412738258" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="a -  Venue and events" />
<h1 property="name">
a
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> a<br></strong>
a</p>
</a>
</div>

<div data-id="505792" data-name="Tiendus" data-href="http://tiendus.com" class="card startup   id4 id29 id71 id155487  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://tiendus.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://tiendus.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505792-3eb0a1e805e2172a34fa8074454468e4-thumb_jpg.jpg?buster=1412737709" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tiendus -  digital media e-commerce payments influencer marketing" />
<h1 property="name">
Tiendus
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Sell digital products<br></strong>
Tiendus allows Latin American online content creators to sell digital products directly to their audience. We take care of the file storage, processing and delivery of the product to the customer’s inbox.
We think there needs to be a service that provides an ...</p>
</a>
</div>

<div data-id="505759" data-name="Travella" data-href="http://emmanuelorozco.com" class="card startup   id78 id1076 id1161 id9100  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://emmanuelorozco.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://emmanuelorozco.com" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505759-b8b22bb2d664a656484db87c1ea85a4c-thumb_jpg.jpg?buster=1412736912" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Travella -  online travel social travel travel travel &amp; tourism" />
<h1 property="name">
Travella
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Travel better. Now<br></strong>
When people travel, they like to plan their trips.
They like to take pictures, write what they feel. Make friends on the road
That’s why travel is funny. Very funny.
People is not always aware of this. That’s why Travella is born. To help to remember people around ...</p>
</a>
</div>

<div data-id="505501" data-name="BLANCHY " data-href="http://www.blanchy.com.mx" class="card startup   id3 id6 id154219 id154220  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.blanchy.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.blanchy.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505501-3d57942ff3d014a26d107508ae19614a-thumb_jpg.jpg?buster=1412716682" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="BLANCHY  -  mobile social media laundry  Dry cleaning" />
<h1 property="name">
BLANCHY
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Laundry and Dry Cleaning web and mobile service<br></strong>
We are welcoming to the XXI century your laundry and dry cleaning web and mobile service. With our “uber laundry service”, our direct customer service and delivery system, your laundry is clean within 24 HOURS.
Blanchy is movil/ web platform that provides laundry ...</p>
</a>
</div>

<div data-id="505471" data-name="Hop in" data-href="http://hopin.com.mx" class="card startup   id4 id631 id155787  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://hopin.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://hopin.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505471-d466902a50b14fa2d3d91b6250c9c2d5-thumb_jpg.jpg?buster=1417288592" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Hop in -  digital media transportation Car" />
<h1 property="name">
Hop in
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Carpooling platform<br></strong>
&quot;Hop in&quot; is intentioned to be a peer-to-peer carpooling platform that allows users publish and/or seek trips in order to share expenses, save time, and travel in a more comfortable and safer way.
It is focused in daily workers that need to use their cars and/or ...</p>
</a>
</div>

<div data-id="505368" data-name="Comunidad Coin" data-href="http://www.comunidadcoin.com/coin" class="card startup   id6 id1295 id9145  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.comunidadcoin.com/coin" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.comunidadcoin.com/coin" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/505368-3b09b209fd886d4ad2a6f1b7e98f26da-thumb_jpg.jpg?buster=1412709954" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Comunidad Coin -  social media gamification self development" />
<h1 property="name">
Comunidad Coin
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online Network Marketing Community <br></strong>
Coin Community
Its a platform that allows people achieve their dreams having fun, combining three streams:
- Gamification. What if you could accomplish your dreams in a fun and engaging way.
- Social Networking. Being part of a community of dreamers like you, ...</p>
</a>
</div>

<div data-id="504745" data-name="ZEVACH" data-href="http://www.cpachamama.org" class="card startup   id314 id989 id1720 id10914  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.cpachamama.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/colectivo.pachamama" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@CPachamama" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.cpachamama.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504745-4606ddf542891ab426a9bf9736cac928-thumb_jpg.jpg?buster=1412683077" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ZEVACH -  families hospitals home owners high schools" />
<h1 property="name">
ZEVACH
</h1>
<span href="http://twitter.com/@CPachamama" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> PORTABLE WATER PURIFIER<br></strong>
IONIZER is a domestic equipment ozone generator to eliminate pathogenic bacteria found in water. To purify water diseases such as cholera, diphtheria , typhoid , amoebiasis , diarrhea avoided, among others. It also functions as a sterilizing food , objects and ...</p>
</a>
</div>

<div data-id="504707" data-name="ETHNICOM" data-href="http://www.ethnicom.weebly.com" class="card startup   id29 id93 id407 id10969  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ethnicom.weebly.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ethnicom.weebly.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504707-4caa692ab4841fbcd5d8236cc8a74c52-thumb_jpg.jpg?buster=1412661931" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ETHNICOM -  e-commerce fashion art home decor" />
<h1 property="name">
ETHNICOM
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Etnic world online<br></strong>
ETHNICOM aims to create a plaltform reuniting unique crafted pieces from all over the world, in an online store. All the world's unique treasures, at the eyes of customer's in this online ethnical ''pangea''.
Best quality, ''Artesanías'' (crafts) from Mexico ...</p>
</a>
</div>

<div data-id="504686" data-name="Pronovatek" data-href="http://pronovahome.tk/" class="card startup   id62 id942 id11022 id153832  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://pronovahome.tk/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://pronovahome.tk/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504686-a259ca1ff898ff8fada43c9baf9da753-thumb_jpg.jpg?buster=1412722391" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pronovatek -  security embedded hardware and software smart building smart home" />
<h1 property="name">
Pronovatek
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Apple's Missing iHome<br></strong>
PronovaHome is an open, flexible and scalable solution that converts a conventional Home to a SmartHome.
Easiness, modularity and user understanding are the main pillars in which the solution resides.
The Smart System integrates a central server where it can ...</p>
</a>
</div>

<div data-id="504681" data-name="Ecoshield" data-href="http://www.ecoshield.com.mx" class="card startup   id105 id615 id152497  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ecoshield.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/ecoshieldnano" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ecoshield.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504681-da5f84ae9b8761d940718371505d3cac-thumb_jpg.jpg?buster=1412655195" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Ecoshield -  nanotechnology tablets Cell Phones" />
<h1 property="name">
Ecoshield
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Nanotecnology, waterproof<br></strong>
What is Ecoshield?
EcoShield creates an invisible layer designed for any mobile device. Using our specially formulated nanotechnology will protect your devices from liquid damage and scratches.
EcoShield has super hydrophobic properties. When applied externally ...</p>
</a>
</div>

<div data-id="504633" data-name="GUARDIAN LIST" data-href="http://www.guardianlist.com" class="card startup   id63 id320 id155497 id156283  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.guardianlist.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.guardianlist.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504633-70cdbcf69ad382f96549f1a77ff014f2-thumb_jpg.jpg?buster=1412694544" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="GUARDIAN LIST -  adult parenting young entrepreneurs Entrepreneurs" />
<h1 property="name">
GUARDIAN LIST
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> ONLINE DATA DELIVERY<br></strong>
GUARDIAN LIST, IS A PRICELESS APP FOR EVERYONE TO USE. THE MAIN PURPOUSE FOR GUARDIAN LIST IS TO PROVIDE AN ONLINE BACK UP ARCHIVE OF THE MOST VALUABLE POSESSIONS (MATERIAL OR NOT) IN THEIR LIVES.
NOBODY KNOWS FOR CERTAIN FOR HOW LONG WE'RE GOING TO BE AROUND, ...</p>
</a>
</div>

<div data-id="504490" data-name="Yopompo" data-href="http://Www.yopompo.com" class="card startup   id3127 id152462 id152852 id154998  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://Www.yopompo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://Www.yopompo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504490-8c0363bd8100b011918e78a8501fe43e-thumb_jpg.jpg?buster=1412640078" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Yopompo -  web development ecommerce Ethical Fashion Mexico" />
<h1 property="name">
Yopompo
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online fashion retail <br></strong>
Yopompo is an online retail focused on fashion and art. Collaborating with local artists we help development of small businesses by providing logistic solutions, branding, among others.</p>
</a>
</div>

<div data-id="504462" data-name="Fund me" data-href="http://fundme.com.mx" class="card startup   id3010 id129903 id156283 id156285  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://fundme.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://fundme.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504462-f292ab0778722b28313eb6699262eaf4-thumb_jpg.jpg?buster=1412641974" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fund me -  crowdfunding invest online Entrepreneurs investors" />
<h1 property="name">
Fund me
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Equity crowdfunding (Crowdcube, Crowdfunder)<br></strong>
We are primarily focused on solving the lack of seed capital, venture capital and mentoring for entrepreneurs and startups.
We are being disruptive over the actual crowdfunding models, we are not going to charge a fee for every deal made at the beginning, we are ...</p>
</a>
</div>

<div data-id="504367" data-name="Dream IT Salud" data-href="http://www.dreamit.la" class="card startup   id13 id175 id223  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.dreamit.la" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.dreamit.la" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504367-e82654c1ea1bbaa968158952f25c41aa-thumb_jpg.jpg?buster=1412631229" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Dream IT Salud -  health care health care information technology doctors" />
<h1 property="name">
Dream IT Salud
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> working and connection platform for doctors<br></strong>
An online work platform specially designed to meet the very specific operational and informational requirements of current health professionals: digitalized clinical record, private practice management apps, medical databases, colleague contact, specialty colleges ...</p>
</a>
</div>

<div data-id="504358" data-name="Baja tu seguro " data-href="http://www.bajatuseguro.com" class="card startup   id29 id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bajatuseguro.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/bajatusegurocom?fref=ts" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/Bajatuseguro1" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bajatuseguro.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504358-4c6062d5765df1d689997f23659edb8a-thumb_jpg.jpg?buster=1412630892" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Baja tu seguro  -  e-commerce Latin America" />
<h1 property="name">
Baja tu seguro
</h1>
<span href="https://twitter.com/Bajatuseguro1" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> innovation, coustomer service, app, attencion, online comparision<br></strong>
First fully online insurance broker in Latin America focused in selling micro-insurance to a population sector that has no access to any type of insurance coverage, backed up by a 24 hours call center which provides personalized service to all of ours clients.</p>
</a>
</div>

<div data-id="504205" data-name="Animal Sphere" data-href="http://www.animalsphere.com" class="card startup   id205 id269 id675 id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.animalsphere.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>


</div>
</div>
<a class="main_link" href="http://www.animalsphere.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504205-8fc5a25b5566c04db4c9a101db20909d-thumb_jpg.jpg?buster=1412623358" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Animal Sphere -  mobile advertising social media platforms pets Latin America" />
<h1 property="name">
Animal Sphere
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Facebook for pets and pet lovers<br></strong>
Animal Sphere is a social network focused on improving the culture of the responsible pet ownership and therefore reduce the number of stray animals in Mexico and Latam. Pet lovers can interact in the community by creating a profile for their pets, share and comment ...</p>
</a>
</div>

<div data-id="504100" data-name="Pollstr" data-href="http://pollstr.io" class="card startup   id52 id97 id283 id155487  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://pollstr.io" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://pollstr.io" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/504100-c92b3c02bdbbca709528b07494404981-thumb_jpg.jpg?buster=1415468626" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pollstr -  analytics business intelligence natural language processing influencer marketing" />
<h1 property="name">
Pollstr
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Helping you understand what people think<br></strong>
Pollstr allows you to categorize large batches of textual survey data within minutes. Our product produces a report that includes trends, keywords and even categories within the survey. When fed with previous data, Pollstr produces results that are as accurate ...</p>
</a>
</div>

<div data-id="503901" data-name="The Ship Room" data-href="http://www.theshiproom.com" class="card startup   id367 id631 id1545 id153471  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.theshiproom.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.theshiproom.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503901-59ec0d987ac5fc207f141cdb8158ee2b-thumb_jpg.jpg?buster=1412608722" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="The Ship Room -  supply chain management transportation logistics trucking" />
<h1 property="name">
The Ship Room
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Innovación tecnológica en logística terrestre<br></strong>
Desarrollamos un sistema p2p entre operadores logísticos terrestres y consumidores finales, un sitio especializado en el tema con un intercambio de oferta y demanda.
Los proveedores ofrecen servicios de transportes sin carga a precios especiales y el consumidor ...</p>
</a>
</div>

<div data-id="503837" data-name="Lepic" data-href="http://Any at the moment" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://Any at the moment" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://Any at the moment" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503837-7f9a2866b0328b5c731ece6970362186-thumb_jpg.jpg?buster=1412643872" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Lepic - " />
<h1 property="name">
Lepic
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Low power epic LED bulb<br></strong>
</p>
</a>
</div>

<div data-id="503729" data-name="Wingspreneur" data-href="http://www.wingspreneur.net" class="card startup   id269 id1474 id155497 id155582  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.wingspreneur.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.wingspreneur.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503729-195e2a68083a0fe59e9f30b1dfd6efcf-thumb_jpg.jpg?buster=1412595081" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Wingspreneur -  social media platforms entrepreneur young entrepreneurs Shorten Links," />
<h1 property="name">
Wingspreneur
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Digital Business Platform<br></strong>
Wingspreneur is a Digital Business Platform where a limited amount of Entrepreneurs can upload the executive summary of their projects and be in the scope of the most prestigious and experienced Investment groups that focus in Venture Capital, Seed Investments, ...</p>
</a>
</div>

<div data-id="503522" data-name="Dprisa México" data-href="http://Dprisa México" class="card startup   id29  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://Dprisa México" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://Dprisa México" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503522-15029d343363f4ef63af6f9ccc0d5f43-thumb_jpg.jpg?buster=1412571773" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Dprisa México -  e-commerce" />
<h1 property="name">
Dprisa México
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Best online shopping experience<br></strong>
Dprisa let you buy whatever you want, whenever you want, offering the best quality and prices in all our products and free shipping on orders over $60 USD</p>
</a>
</div>

<div data-id="503470" data-name="Quack" data-href="http://quack.mx/" class="card startup   id3 id263 id11700  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://quack.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://quack.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
 <img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503470-bce82696ae02a07395fdb6ecc0fb8355-thumb_jpg.jpg?buster=1412565268" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Quack -  mobile restaurants it management" />
<h1 property="name">
Quack
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Restaurant management system<br></strong>
Quack is a management system that is integrated by 2 fields:
1. Internal. For the restaurant, 5 different devices:
-Control device in the kitchen.
-Administrator device for the manager.
-Waiter's notification device.
-Reception’s device.
-Smart devices on ...</p>
</a>
</div>

<div data-id="503367" data-name="OmniTutor" data-href="http://omnitutor.co" class="card startup   id43 id2012  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://omnitutor.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://omnitutor.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503367-01cca08fb66dfef9c748bd81a72ecf7c-thumb_jpg.jpg?buster=1412555072" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="OmniTutor -  education tutoring" />
<h1 property="name">
OmniTutor
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The official tutoring community in your country<br></strong>
OmniTutor is an online one on one marketplace for private tutoring that enables students to connect with tutors. On OmniTutor, you’ll always find a tutor to answer your questions.</p>
</a>
</div>

<div data-id="503284" data-name="BAYO STUDIOS" data-href="http://www.bayostudios.com" class="card startup   id94 id205 id490 id15213  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bayostudios.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bayostudios.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/503284-81095d72d4d96cebcdee2d46f210d34c-thumb_jpg.jpg?buster=1412543164" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="BAYO STUDIOS -  mobile commerce mobile advertising mobile games social innovation" />
<h1 property="name">
BAYO STUDIOS
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> innovative games<br></strong>
BAYO Studios offers entertainment mobile apps, for both users and companies, each creation keeps originality innovation engaging and fun as a distinctive mark.
We are creating an iOS Skill game based on Mexico's Subway, codename METRO GAME, surrounded by familiar ...</p>
</a>
</div>

<div data-id="502953" data-name="IncogniStudios" data-href="https://sites.google.com/site/incognistudio" class="card startup   id4 id43 id498 id1054  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://sites.google.com/site/incognistudio" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://sites.google.com/site/incognistudio" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502953-fb519f8a3a32b5497fbd7b02fdcdb1f4-thumb_jpg.jpg?buster=1412486226" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="IncogniStudios -  digital media education e books augmented reality" />
<h1 property="name">
IncogniStudios
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Augmented Reality Books (Book in)<br></strong>
The Bookin Makes Reading Better, Helps to understand complex reading.
and Complex Situations,
Also games on books if its needed.
We bring books to people not people to books.</p>
</a>
</div>

<div data-id="502863" data-name="Time Is The New Money" data-href="http://coming soon" class="card startup   id181 id186 id152730 id153875  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://coming soon" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://coming soon" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502863-0960ac420dbdca3fb6013f9f89eb47e0-thumb_jpg.jpg?buster=1412470160" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Time Is The New Money -  communities networking Social Entrepreneurship new business" />
<h1 property="name">
Time Is The New Money
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Connecting projects <br></strong>
TITNM, connecting projects. We are a digital platform that puts together people to share job skills in order to enhance independent projects.
TITNM unite the resources of different individuals or companies to increase the scope of a project through the exchange ...</p>
</a>
</div>

<div data-id="502774" data-name="Blue Hand" data-href="http://www.bluehand.com.mx" class="card startup   id340 id152006  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bluehand.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bluehand.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502774-19ae082effdcf2b307948c68ac3fadab-thumb_jpg.jpg?buster=1412455857" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Blue Hand -  small and medium businesses Medium to Large Enterprises with large numbers of skilled workers" />
<h1 property="name">
Blue Hand
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Custom-made mobile apps<br></strong>
AuditSuit app helps you to convert any paper form into an efficient smart mobile form in minutes. Now you can have real time info collected by your users (field workers, clients, surveys, etc.). Make important decisions with the power of real time data, the app ...</p>
</a>
</div>

<div data-id="502678" data-name="start" data-href="http://www.startmx.com  in construction" class="card startup   id34 id49 id450 id152497  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.startmx.com  in construction" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/red start mexico" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

 </div>
</div>
<a class="main_link" href="http://www.startmx.com  in construction" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502678-8aacb0bbfa58c7fb5011ef2d2ed18f3b-thumb_jpg.jpg?buster=1412444201" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="start -  advertising telecommunications social media marketing Cell Phones" />
<h1 property="name">
start
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> telecom network marketing<br></strong>
Start sells a membership that allows people to make money from the monthly cell phone bill of other people (familiy,friends)by only buying their cell phone prepaid minutes with our system, they will receive the same companies service but beacuse we do the recharge ...</p>
</a>
</div>

<div data-id="502658" data-name="Voz Voice " data-href="http://www.vozvoice.com" class="card startup   id57 id201 id2554 id2812  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.vozvoice.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/vozvoiceofficial" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.vozvoice.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502658-f07cdc8c65c0cfb1c6cecb125302222c-thumb_jpg.jpg?buster=1412525694" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Voz Voice  -  location based services news mobile security entertainment industry" />
<h1 property="name">
Voz Voice
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Geo Localization News (Waze)<br></strong>
Voz Voice is a newspaper by geo localization, this means the society and government of each city around the world can publish or get information of whats happening around them (e.g Entertainment, Warnings Advices, Crimes) or go to some other places that the user ...</p>
</a>
</div>

<div data-id="502555" data-name="animento" data-href="http://www.animento.com.mx" class="card startup   id29 id51 id675  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.animento.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/animentomexico" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@animento" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.animento.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502555-d23227067727f7d5139c6647c5f3190e-thumb_jpg.jpg?buster=1412421255" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="animento -  e-commerce sales and marketing pets" />
<h1 property="name">
animento
</h1>
<span href="http://twitter.com/@animento" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Innovador, Fácil, seguro, rentable y confiable<br></strong>
animento es una empresa mexicana, situada en el Distrito Federal, dedicada a la venta en línea de alimento para mascotas. Nuestras principales ventajas son: brindar el mejor servicio, efectividad, calidad, seguridad y precio al consumidor, ya que en estos tiempos ...</p>
</a>
</div>

<div data-id="502333" data-name="Sanesy" data-href="https://sanesy.com.mx/" class="card startup   id3 id62 id314 id157693  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://sanesy.com.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/SanesyMx" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/SanesyMx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="https://sanesy.com.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502333-db148a363c02f53440bb36812b8036a4-thumb_jpg.jpg?buster=1412654656" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sanesy -  mobile security families house" />
<h1 property="name">
Sanesy
</h1>
<span href="https://twitter.com/SanesyMx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Safe Neighbourhood System<br></strong>
Sanesy is a system that improves the security of closed gate communities and neighborhoods, by providing all people that live there with an app that can be downloaded with any mobile device. This app has 4 features: blog, announcements, messages and SOS alerts; ...</p>
</a>
</div>
 
<div data-id="502215" data-name="mostrador" data-href="http://mostrador.mx" class="card startup   id32  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://mostrador.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/mostrador.mx" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/mostradorV1" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://mostrador.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502215-4a5fd65ed1416995f4599b2c578f6dbf-thumb_jpg.jpg?buster=1412367853" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="mostrador -  retail" />
<h1 property="name">
mostrador
</h1>
<span href="https://twitter.com/mostradorV1" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Point of sales stocks invoices<br></strong>
Mostrador offers point of sales, stocks and electronic invoices, do reports to owners and managers of small and medium enterprises that sells thousands of products and services in many stores.</p>
</a>
</div>

<div data-id="502019" data-name="PETWIK" data-href="http://petwik.com/" class="card startup   id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://petwik.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@petwik2" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://petwik.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/502019-3a3c28d06bb0740feb493f4d2de7f6d8-thumb_jpg.jpg?buster=1412352352" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="PETWIK -  Latin America" />
<h1 property="name">
PETWIK
</h1>
<span href="http://twitter.com/@petwik2" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> PETWIK es una red social móvil para el mercado de mascotas<br></strong>
PETWIK es una red móvil que ayuda en casos de perdida o robo de mascotas a su vez va creando un marketplace donde el mcommerce, gamification y servicios adicionales le dan valor a la Red.</p>
</a>
</div>

<div data-id="501731" data-name="Juun Club" data-href="http://www.juun-club.com" class="card startup   id29 id498 id151481  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.juun-club.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/juunclub" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://www.twitter.com/juun_club" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.juun-club.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/501731-8344ad727e7e6e93640f15d204133738-thumb_jpg.jpg?buster=1412308563" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Juun Club -  e-commerce e books Gifts" />
<h1 property="name">
Juun Club
</h1>
<span href="http://www.twitter.com/juun_club" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Monthly Home Delivery Book Club<br></strong>
Juun Club es un servicio que te lleva un libro a tu casa según tus gustos. Puedes ajustar la frecuencia de envíos de forma mensual, cada dos meses o cada tres meses.
Es una excelente manera para encontrar qué leer y crear hábitos de lectura. Es muy fácil personalizar ...</p>
</a>
</div>

<div data-id="501726" data-name="RYE.MX" data-href="http://www.rye.mx" class="card startup   id32 id710 id10481 id151264  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rye.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rye.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/501726-ffe6ee4c0dc7e2490c05196b2112b2db-thumb_jpg.jpg?buster=1412308399" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="RYE.MX -  retail customer service automated kiosk Retail Grocery Chains" />
<h1 property="name">
RYE.MX
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Retail booth for direct customer feedback<br></strong>
Our product is a booth / kiosk to obtain customer feedback directly at the sales floor. The design is different to a normal service kiosk, it´s bright and has integrated information displays all over it´s body.
The main goal is to obtain customer feedback directly ...</p>
</a>
</div>

<div data-id="501664" data-name="AVI Green" data-href="http://avidecongreen. com" class="card startup   id34  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://avidecongreen. com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/avidecongreen" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

</div>
</div>
<a class="main_link" href="http://avidecongreen. com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/501664-d6bd92251a67f5c83bcfa86a5dfa52e8-thumb_jpg.jpg?buster=1412297519" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="AVI Green -  advertising" />
<h1 property="name">
AVI Green
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> destruye micro organismos, esporas, bactrias, virus y hongos<br></strong>
Este producto es eficaz en remediacion y descontaminación en areas con problemas de bactrias y contamination de esporas como el H1N1. Y mas virus y tenemos un producto que ayuda al departamento de bomberos, este producto ya es utilizado en USA en varias dependencias ...</p>
</a>
</div>

<div data-id="501454" data-name="Content-o" data-href="http://contento-prod.herokuapp.com/" class="card startup   id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://contento-prod.herokuapp.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://contento-prod.herokuapp.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/501454-7a0fc296e73eacc2929671190225aac5-thumb_jpg.jpg?buster=1412282113" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Content-o -  Latin America" />
<h1 property="name">
Content-o
</h1>
 <span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> CMS for all platforms<br></strong>
Content-o is an object management system designed to provide content to all platforms. Manage everything under one platform: website, apps, social media and see analytics in real time.
Through our templates you just need to upload content to your distribution ...</p>
</a>
</div>

<div data-id="501416" data-name="BarroNegro" data-href="https://itunes.apple.com/us/artist/barronegro/id383639190" class="card startup   id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://itunes.apple.com/us/artist/barronegro/id383639190" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://itunes.apple.com/us/artist/barronegro/id383639190" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/501416-8e8f971b6a49618f45da6881cc93eec8-thumb_jpg.jpg?buster=1412279016" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="BarroNegro -  Latin America" />
<h1 property="name">
BarroNegro
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Multimedia Education for Mexico<br></strong>
iOS and Android Apps to teach children basic education topics thorough interaction and multimedia.
We have several published iOS Apps focused in Mexican History. For us this is our MVP. The revenue is minimal right now. But we are confident that if we broaden ...</p>
</a>
</div>

<div data-id="501211" data-name="Rice Import Consulting Export Services" data-href="http://www.rices.com.mx" class="card startup   id379 id680 id14033  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rices.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rices.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/501211-67d277885529668d914546804f063dd1-thumb_jpg.jpg?buster=1412265168" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Rice Import Consulting Export Services -  retail technology construction insurance companies" />
<h1 property="name">
Rice Import Consulting Export Services
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Home and Office Security Products<br></strong>
SEGURIKIT is a security system that gives ease to its user, thanks to its rapid and practical usage and interface that link to the sensible areas of your home, business or office. SEGURIKIT has the best prices accesible to every economic population level.
RICES ...</p>
</a>
</div>

<div data-id="500810" data-name="Spanish To Move" data-href="http://spanishtomove.com" class="card startup   id9407 id151002  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://spanishtomove.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://spanishtomove.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/500810-b5bdd8b8443516a82f668def0110a3f4-thumb_jpg.jpg?buster=1412216742" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Spanish To Move -  general public worldwide United States" />
<h1 property="name">
Spanish To Move
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Speak Spanish. It's time to move forward!<br></strong>
According to Instituto Cervantes data, there are approximately 21 million Spanish students around the world.
Spanish To Move® offers courses and materials to facilitate the learning of the Spanish language, by a self-teaching method.
Our mission is to produce, ...</p>
</a>
</div>

<div data-id="500746" data-name="URBINGO" data-href="http://www.urbingo.com" class="card startup   id16  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.urbingo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.urbingo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/500746-c4fefeb5aaa011cbbb47e40f26577859-thumb_jpg.jpg?buster=1412209438" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="URBINGO -  real estate" />
<h1 property="name">
URBINGO
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Real State Mexico<br></strong>
The name Urbingo comes from the combination of 2 words, urban and bingo.
Urbingo has the friendliest interfaces within the housing market, both for people looking for real estate properties who want to promote their properties for sale or rent, as it allows uploading ...</p>
</a>
</div>

<div data-id="499771" data-name="Fric Colaboration" data-href="http://www.fricmartinez.com (estará dentro de esta plataforma)" class="card startup   id686 id154573  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.fricmartinez.com (estará dentro de esta plataforma)" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.fricmartinez.com (estará dentro de esta plataforma)" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/499771-e24a29c3071c14b6bcb60a4b2d73335a-thumb_jpg.jpg?buster=1412113482" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fric Colaboration -  brand marketing Brand Development" />
<h1 property="name">
Fric Colaboration
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Creación de animación colaborativamente (crowdsourcing)<br></strong>
Actualmente las marcas contratan a una agencia de publicidad para crear campañas y contenido, pero al final dependen de un equipo creativo de unas pocas personas que pueden estar hartas de su trabajo.
En Fric Colaboration las marcas podrán escoger entre 3 ideas ...</p>
</a>
</div>

<div data-id="498461" data-name="GRUPO AKIJABARA" data-href="http://www.invercero.com" class="card startup   id29 id42  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.invercero.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.invercero.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/498461-e75ffd86813e60ac2529eba8d03da7f8-thumb_jpg.jpg?buster=1412014635" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="GRUPO AKIJABARA -  e-commerce B2B" />
<h1 property="name">
GRUPO AKIJABARA
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> B2B Web page in México<br></strong>
It's a new Mexican company of technology and electronic services that provide information through an Internet webpage that links customers with suppliers and vice versa in order to maximize the potential of doing business and reducing costs by finding them with ...</p>
</a>
</div>

<div data-id="496716" data-name="TravelBid" data-href="http://www.travelbid.com.mx" class="card startup   id42 id94 id230 id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.travelbid.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.travelbid.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/496716-50a6a2db249df0d36557fa593e27eed8-thumb_jpg.jpg?buster=1411765520" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="TravelBid -  B2B mobile commerce social commerce Latin America" />
<h1 property="name">
TravelBid
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online Travel Agency<br></strong>
Hotel and Flight services, regular vacational packages</p>
</a>
</div>

<div data-id="495891" data-name="Bright Brains SC" data-href="http://www.brightbrains.org" class="card startup   id481 id1469 id154030  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.brightbrains.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.brightbrains.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/495891-d287dec6434d93d95d3a5d14323ae7b8-thumb_jpg.jpg?buster=1411678359" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bright Brains SC -  kids k 12 education Brain Health" />
<h1 property="name">
Bright Brains SC
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Extra curricular Courses<br></strong>
We provide different services for children and adults who want to improve their math and reading skills. We help them train their brain offering different courses for all ages.
We work with different programs depending on their needs. We help children who need ...</p>
</a>
</div>

<div data-id="495218" data-name="GoGuaya" data-href="http://www.goguaya.com" class="card startup   id29 id9002  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.goguaya.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/goguaya" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.goguaya.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/495218-cd4d8f7b0039d85f52ec4a538f1dc04f-thumb_jpg.jpg?buster=1411615425" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="GoGuaya -  e-commerce home &amp; garden" />
<h1 property="name">
GoGuaya
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Plant nurseries Online<br></strong>
GoGuaya is the perfect website to find everything you need to create the garden of your dreams.
You can shop online any kind of plants and trees and personalize them according to your needs. All, delivered to the door of your house.
In GoGuaya you can also find ...</p>
</a>
</div>

<div data-id="494747" data-name="Pre-College" data-href="http://www.precollege.mx" class="card startup   id912 id9600  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.precollege.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.precollege.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/494747-8666dcb6b1358e4f1b4b1157d9277124-thumb_jpg.jpg?buster=1411580397" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pre-College -  teenagers high school students" />
<h1 property="name">
Pre-College
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> is a guidance counselor with different and original methods<br></strong>
Our services offers three major lines:
One to One
Vocational guidance professionals will take care of the young, with the support of interviews, tests and multimodal resources, in order to find his true calling.
Professional Experience
According to the results ...</p>
</a>
</div>

<div data-id="494127" data-name="voyconmimascota" data-href="http://www.voyconmimascota.com.mx" class="card startup   id6 id87 id205 id675  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.voyconmimascota.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/voyconomimascota" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@voyconmimascota" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.voyconmimascota.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/494127-ac0be0d7f4d2bf4f9f81227d5a44a095-thumb_jpg.jpg?buster=1412347897" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="voyconmimascota -  social media publishing mobile advertising pets" />
<h1 property="name">
voyconmimascota
</h1>
<span href="http://twitter.com/@voyconmimascota" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The best guide to places Petfriendly of Mexico<br></strong>
</p>
</a>
</div>

<div data-id="493184" data-name="Latin Run" data-href="http://www.latinrun.com" class="card startup   id29 id287 id151980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.latinrun.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.latinrun.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/493184-e683d6cfc2f0f33f7c36a0e87f482ff1-thumb_jpg.jpg?buster=1411437832" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Latin Run -  e-commerce sporting goods Latin America" />
<h1 property="name">
Latin Run
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The most agresive ecommerce/community 100% Running in Latín América<br></strong>
The most agresive ecommerce/community 100% Running in Latín América</p>
</a>
</div>

<div data-id="492666" data-name="Yoga Cloud" data-href="http://yogacloud.net" class="card startup   id175 id418 id1103 id1153 id1258 id156282  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://yogacloud.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://yogacloud.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/492666-c66e25d72a646fb0e1be9994ad2bfb1e-thumb_jpg.jpg?buster=1411401875" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Yoga Cloud -  health care information technology personal health health and wellness young adults mobile health Working professionals" />
<h1 property="name">
Yoga Cloud
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Hyperpersonalized yoga experience.<br></strong>
Yoga Cloud is a yoga platform that creates a yoga journey for each person depending on the physical condition, illness or yoga experience, you can look at your progress and win badges for doing yoga regularly, it's also a community where you can get to know new ...</p>
</a>
</div>

<div data-id="491721" data-name="HAUSFUNDING" data-href="http://www.hausfunding.mx" class="card startup   id6 id17 id321 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.hausfunding.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.hausfunding.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/491721-93af61dbae922801f9770282d9e86818-thumb_jpg.jpg?buster=1411240847" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="HAUSFUNDING -  social media financial services finance finance technology" />
<h1 property="name">
HAUSFUNDING
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Crowdfunding de bienes raíces en México<br></strong>
Siendo en México actualmente la pobre inclusión financiera, un problema crítico y latente, mediante HAUSFUNDING proporcionaremos una puerta de entrada al mercado inmobiliario, con acceso para cualquier persona, de cualquier estrato social, mediante un modelo de ...</p>
</a>
</div>

<div data-id="489736" data-name="Vincubo" data-href="http://www.vincubo.com" class="card startup   id154998  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.vincubo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.vincubo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/489736-50e054744650dd6a855a7706fdb5c423-thumb_jpg.jpg?buster=1410991138" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Vincubo -  Mexico" />
<h1 property="name">
Vincubo
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> End to end space search<br></strong>
Vincubo is an early stage, technology-driven real estate search and discount brokerage services startup.
We are building a unified end-to-end web and mobile platform that streamlines the process and provides both customers and our agents with everything they ...</p>
</a>
</div>

<div data-id="489514" data-name="Inversiones Feromar" data-href="http://www.magisterchef.com" class="card startup   id6 id72 id11600 id157024  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.magisterchef.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/magisterchef" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://www.twitter.com/magisterchef" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.magisterchef.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/489514-8a191222f6ce930dcc21fe42a5c67620-thumb_jpg.jpg?buster=1410974149" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Inversiones Feromar -  social media food and beverages all students E-Learn" />
<h1 property="name">
Inversiones Feromar
</h1>
<span href="http://www.twitter.com/magisterchef" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Culinary training portal (OpenEnglish+Netflix)<br></strong>
MagisterChef is a culinary training portal which helps the customer getting in touch with the best chefs in the world. In order to create an specialized social network we develop a place where foodies can get together and talk about mutual interests.
Our HQ ...</p>
</a>
</div>

<div data-id="487664" data-name="Chakz Armada" data-href="http://www.chakzarmada.mx" class="card startup   id2735 id2812 id154091 id154517  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.chakzarmada.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.chakzarmada.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/487664-cdbd0ca93a845027c713bdc27e430e47-thumb_jpg.jpg?buster=1410800390" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Chakz Armada -  licensing entertainment industry Animation Merchandising " />
<h1 property="name">
Chakz Armada
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Animated characters series and licensing label<br></strong>
Animated series based on mexican creators' characters. Licensing of this characters by the Chakz Armada label. The customers are teenagers of all economic and ethnic groups.</p>
</a>
</div>

<div data-id="486779" data-name="immig" data-href="http://www.immig.co" class="card startup   id17 id162 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.immig.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.immig.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/486779-4c321c81670e4557f0f71b27bacf9e97-thumb_jpg.jpg?buster=1410652319" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="immig -  financial services marketplaces finance technology" />
<h1 property="name">
immig
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> FinTech App que enlaza paisanos en Estados Unidos con Microfinancieras en México y EEUU<br></strong>
Nuestra empresa se llama Immig (migrante inteligente), cuya propuesta de valor se basa en es una plataforma FinTech, que enlaza a mexicanos en los EEUU y Canadá con fuentes de microcréditos (microfinancieras) en México o EEUU, sin importar el lugar donde se encuentren, ...</p>
</a>
</div>

<div data-id="486389" data-name="V-Life, S.A. de C.V." data-href="http://eduardodiaz0.wix.com/v-life" class="card startup   id154243  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://eduardodiaz0.wix.com/v-life" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://eduardodiaz0.wix.com/v-life" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/486389-6c2e29e78b89c3b1cdacd2e48449a467-thumb_jpg.jpg?buster=1410578079" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="V-Life, S.A. de C.V. -  Mexico, US, LatAm" />
<h1 property="name">
V-Life, S.A. de C.V.
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Virtual 3D Shopping Mall<br></strong>
V-Life is the first virtual 3D shopping mall that allows users to buy products directly of their favorite brands and companies while they play and have fun, or make new friends and interact with them, get promotions and discounts, tour by the virtual mall, among ...</p>
</a>
</div>

<div data-id="484890" data-name="Doggy Community" data-href="http://doggycommunity.com" class="card startup   id152026 id152196 id154998  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

 <a href="http://doggycommunity.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@doggycommunity" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://doggycommunity.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/484890-4ff6af223fce71ae81898b0dcacafa56-thumb_jpg.jpg?buster=1410404615" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Doggy Community -  USA Europe Mexico" />
<h1 property="name">
Doggy Community
</h1>
<span href="http://twitter.com/@doggycommunity" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Conectando ladridos, Facebook para perros<br></strong>
1.- El producto
Doggy Community es una nueva red social especializada en el sector de mascotas puntualmente en la industria canina, diseñada como las redes sociales más importantes del mundo.
2.- ¿Quién es el cliente?
La red social esta pensada para dos tipos ...</p>
</a>
</div>

<div data-id="484858" data-name="Doggy Community" data-href="http://doggycommunity.com" class="card startup   id152196 id154243  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://doggycommunity.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@doggycommunity" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://doggycommunity.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/484858-ee831a82f3e99bb68f89f3c85413beab-thumb_jpg.jpg?buster=1410399494" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Doggy Community -  Europe Mexico, US, LatAm" />
<h1 property="name">
Doggy Community
</h1>
<span href="http://twitter.com/@doggycommunity" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Conectando ladridos, Facebook para perros<br></strong>
1.- El producto
Doggy Community es una nueva red social especializada en el sector de mascotas puntualmente en la industria canina, diseñada como las redes sociales más importantes del mundo.
2.- ¿Quién es el cliente?
La red social esta pensada para dos tipos ...</p>
</a>
</div>

<div data-id="484834" data-name="Doggy Community" data-href="http://doggycommunity.com" class="card startup   id152196 id154998  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://doggycommunity.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@doggycommunity" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://doggycommunity.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/484834-3583124e103852a10e640f48739a9b0d-thumb_jpg.jpg?buster=1410395799" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Doggy Community -  Europe Mexico" />
<h1 property="name">
Doggy Community
</h1>
<span href="http://twitter.com/@doggycommunity" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Conectando ladridos, Facebook para perros<br></strong>
1.- El producto
Doggy Community es una nueva red social especializada en el sector de mascotas puntualmente en la industria canina, diseñada como las redes sociales más importantes del mundo.
2.- ¿Quién es el cliente?
La red social esta pensada para dos tipos ...</p>
</a>
</div>

<div data-id="484056" data-name="Virtual Case" data-href="http://virtualcase.com.mx" class="card startup   id43 id51 id100 id680 id2805  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://virtualcase.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/VirtualCase" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

</div>
</div>
<a class="main_link" href="http://virtualcase.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/484056-f9f1e3c1652b97e11dd45572b21a190c-thumb_jpg.jpg?buster=1411970489" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Virtual Case -  education sales and marketing video games construction technical continuing education" />
<h1 property="name">
Virtual Case
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Virtual reallity glasses for smartphone, use your smartphone as virtual reallity glasses.<br></strong>
Virtual Case offers a new platform to enjoy video games, movies, virtual tours or augmented reality aplications in order to show items in design stage like buildings or new products. Virtual Case opens new posibilities for low cost virtual and augmented reality ...</p>
</a>
</div>

<div data-id="483995" data-name="Cicero Online Services, S.A. de C.V." data-href="http://In construction" class="card startup   id249 id1145  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://In construction" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://In construction" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/483995-8ab8f7c9ccbb3068ee22b98ab1018100-thumb_jpg.jpg?buster=1412713121" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cicero Online Services, S.A. de C.V. -  services legal" />
<h1 property="name">
Cicero Online Services, S.A. de C.V.
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Low Cost Legal Services Online<br></strong>
General Description
Online platform pursuant to which a potential customer will be able to:
1. Draft agreements for free;
2. Get fast and good quality legal advice;
3. Obtain useful and understandable legal information.
And lawyers will be able to find potential ...</p>
</a>
</div>

<div data-id="481113" data-name="Eco-Ticket" data-href="http://www.eco-ticket.com" class="card startup   id17 id51 id151742 id153557  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.eco-ticket.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.eco-ticket.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/481113-b07535f5ec5c22401f2f57c48a9057e2-thumb_jpg.jpg?buster=1409938327" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Eco-Ticket -  financial services sales and marketing small companies big companies" />
<h1 property="name">
Eco-Ticket
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Financial network to analyze customers behavior<br></strong>
Eco-ticket is an electronic ticket that substitute all paper tickets impresions.
Is free for the customer.</p>
</a>
</div>

<div data-id="477748" data-name="Sparky" data-href="http://sparkyguardian.com/" class="card startup   id17 id314 id1469 id10783  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://sparkyguardian.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/thesparkyapp" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/sparkywatchdog" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://sparkyguardian.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/477748-b6e59067c65ec9517c75960a0513346a-thumb_jpg.jpg?buster=1409601484" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sparky -  financial services families k 12 education health and insurance" />
<h1 property="name">
Sparky
</h1>
<span href="https://twitter.com/sparkywatchdog" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The kids guardian dog for the online world<br></strong>
Sparky is a global solution that targets families with kids. Sparky is an antibullying app that notifies parents about risks on their kids social networks such as sexual predators, sharing private information and aggression. We uses sentiment analysis and other ...</p>
</a>
</div>

<div data-id="470460" data-name="Drofskath" data-href="http://www.drofskath.com" class="card startup   id6 id76 id151656 id153056  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.drofskath.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/drofskath" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://www.twitter.com/drofskath" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.drofskath.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/470460-ab2b3ea70da404b923e204cfeb3f63d4-thumb_jpg.jpg?buster=1410304037" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Drofskath -  social media SEO marketing programming" />
<h1 property="name">
Drofskath
</h1>
<span href="https://www.twitter.com/drofskath" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Espacio digital para emprendedores con información y herramientas gratuitas.<br></strong>
Drofskath is a project born from the idea of supporting the Latin American entrepreneur since we are too limited information, tools and / or support. With the experience we’ve had along this path and in some projects, we want to share with you everything we’ve ...</p>
</a>
</div>

<div data-id="469566" data-name="Ficus - Share Your Wisdom" data-href="http://www.tryficus.com" class="card startup   id253 id448 id1113 id151860  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.tryficus.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/ficusnet" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@ficusnet" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.tryficus.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/469566-c0495c1517583b9916c7d286319be9c2-thumb_jpg.jpg?buster=1414272585" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Ficus - Share Your Wisdom -  consulting startups business development sharing economy" />
<h1 property="name">
Ficus - Share Your Wisdom
</h1>
<span href="http://twitter.com/@ficusnet" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Wisdom sharing community <br></strong>
Ficus is a big family around the world sharing knowledge and experience to make better and wiser decisions in our lives to improve humankind.
Ficus leverages our excess capacity of knowledge and experience acquired through time to be shared among ourselves. All ...</p>
</a>
</div>

<div data-id="468419" data-name="TVendo" data-href="http://www.tvendo.es" class="card startup   id16488 id154243  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.tvendo.es" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.tvendo.es" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/468419-7dcd5144bf7b741609cc64f04091cda3-thumb_jpg.jpg?buster=1408592075" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="TVendo -  spain Mexico, US, LatAm" />
<h1 property="name">
TVendo
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Buy &amp; Sell with VIDEO<br></strong>
The first video classified mobile application. Record an easy 10 sec video (as easy as Vine), and your VIDEO AD is immediately geolocalized. Totally free for both seller and buyer, they are quickly put in touch with chat system very similar to WhatsApp.</p>
</a>
</div>

<div data-id="459829" data-name="Loyalty Refunds" data-href="http://loyaltyrefunds.com" class="card startup   id154243  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://loyaltyrefunds.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://loyaltyrefunds.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/459829-09a2537c278d04cee2e39a19c53f7968-thumb_jpg.jpg?buster=1407812620" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Loyalty Refunds -  Mexico, US, LatAm" />
<h1 property="name">
Loyalty Refunds
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Cloud Intelligence to make loyal customers (Payback)<br></strong>
Loyalty Refunds is business intelligence marketing software that works with a universal rewards points program. It allows companies to increase their sales and customer loyalty through a higher customer purchase rate and spending average. This by obtaining very ...</p>
</a>
</div>

<div data-id="447841" data-name="AlmaCore" data-href="http://www.almacore.net" class="card startup   id13 id251 id16089  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.almacore.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.almacore.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/447841-ad5538aaca3ae7f0b0475d965b544efc-thumb_jpg.jpg?buster=1406592401" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="AlmaCore -  health care governments event management" />
<h1 property="name">
AlmaCore
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Events, Fianncial and Health Care<br></strong>
As a Group, AlmaCore grows as an Advisor for the corporate in Mexico, bring them solutionsn guidenace to open new worlwide markets.</p>
</a>
</div>

<div data-id="433908" data-name="Postal52" data-href="http://postal52.com/en/" class="card startup   id6 id710 id3127 id155805  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://postal52.com/en/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://postal52.com/en/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/433908-a069c9dea2723978f517a4f350e60ec3-thumb_jpg.jpg?buster=1404821111" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Postal52 -  social media customer service web development digital marketing" />
<h1 property="name">
Postal52
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Growth Hacking<br></strong>
Postal52 helps startups and other accelerating companies grow their user and customer base through Growth Hacking. We provide SEO, Community Management, Social Media, Digital Marketing and Optimized Web Development strategies.</p>
</a>
</div>

<div data-id="425429" data-name="Finple" data-href="http://finple.co" class="card startup   id3 id4 id340 id1033  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://finple.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/Finple" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://finple.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/425429-d5376051d968718da637abb749c1d0a1-thumb_jpg.jpg?buster=1403671476" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Finple -  mobile digital media small and medium businesses public relations" />
<h1 property="name">
Finple
</h1>
<span href="https://twitter.com/Finple" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> find people<br></strong>
Consiste en una App (tanto en móvil como Web) Sobre conocer gente (no es Tinder) en base a tus gustos (que obtendrá de tus redes Sociales como: Facebook, twitter, instagram, etc.) lo cual te generara unas “parejas aproximadas” Automáticamente te dará consejos para ...</p>
</a>
</div>

<div data-id="423404" data-name="Teragu" data-href="http://www.teragu.com" class="card startup   id29 id32 id73 id154998  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.teragu.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/Teragu.com.mx" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/teragu" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.teragu.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/423404-15bd7536881244d657da3dbf09345c18-thumb_jpg.jpg?buster=1403400414" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Teragu -  e-commerce retail sports Mexico" />
<h1 property="name">
Teragu
</h1>
<span href="https://twitter.com/teragu" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Ecommerce platform for specialized sports.<br></strong>
In the last 3 years, Teragu has proven a successful record in Mexico’s ecommerce business.
Our goal is to provide the largest specialized sports catalog in Mexico for high-end customers.</p>
</a>
</div>

<div data-id="422166" data-name="Invercoin" data-href="http://www.invercoin.mx" class="card startup   id1406 id93839 id156625  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.invercoin.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.invercoin.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/422166-bdeba6da2e9091c8769f65d1df171a50-thumb_jpg.jpg?buster=1403207440" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Invercoin -  trading bitcoin digital currency" />
<h1 property="name">
Invercoin
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Exchange boutique of digital currencies in Mexico<br></strong>
Besides the exchange, Invercoin provides trading, custody (wallets and security) as well as investment strategy and coaching on cryptocurrency (mainly bitcoin) for individuals in Mexico.</p>
</a>
</div>

<div data-id="397221" data-name="RGM Investments" data-href="http://contacto.smart.investments@gmail.com" class="card startup   id856 id1925 id2724 id154397  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://contacto.smart.investments@gmail.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://contacto.smart.investments@gmail.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/397221-19f4679588dd96a233b175699c518271-thumb_jpg.jpg?buster=1399959202" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="RGM Investments -  venture capital risk management stock exchanges Foreign Investments" />
<h1 property="name">
RGM Investments
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> 60% Roi per year at least<br></strong>
We offer the best possible ROI in the foreign exchange market using
-Market depth
-Fusion analysis
-Technical analysis
-Many different kinds of arbitrage
-Orderflow</p>
</a>
</div>

<div data-id="394910" data-name="Le Plate" data-href="http://leplate.com" class="card startup   id72  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://leplate.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://leplate.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/394910-0bb3bbe631528e99d2b519c0be1d01a5-thumb_jpg.jpg?buster=1399572421" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Le Plate -  food and beverages" />
<h1 property="name">
Le Plate
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Healthy, fresh and chef´s cooked food delivered to your door<br></strong>
With Le Plate you will have a better way to eat, we deliver fresh, healthy and talented chefs cooked food to your door.
We adapt to your needs, gluten free, 0 carbs, etc!!
Pick up the 4 meals of the week and you´ll get faboulous prices.
What we do?
-Plan
-Shop
-Cook
-Deliver
All ...</p>
</a>
</div>

<div data-id="390117" data-name="Punto Catorce" data-href="http://www.puntocatorce.com" class="card startup   id34 id555 id556 id9100  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.puntocatorce.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.puntocatorce.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/390117-70b5cbc3797c9666806f45c19889e0dc-thumb_jpg.jpg?buster=1398880220" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Punto Catorce -  advertising weddings events travel &amp; tourism" />
<h1 property="name">
Punto Catorce
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Create Interactions through technology<br></strong>
MyWedApp helps providers related to social events services and products, specifically weddings to have a channel through which they can promote their offerings to a targeted audience according to the size of event, location, type, guests and budget accelerating ...</p>
</a>
</div>

<div data-id="390081" data-name="Pokapok" data-href="http://www.pokapok.mx" class="card startup   id73 id230 id269  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.pokapok.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.pokapok.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/390081-4fca14031c41f25c530599ef030e2960-thumb_jpg.jpg?buster=1398871870" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pokapok -  sports social commerce social media platforms" />
<h1 property="name">
Pokapok
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Play more, play weird.<br></strong>
Pokapok is an administration for amateur sports players, leagues and managers, built upon a social network environment. It helps you find a team for the sport of your preference i your local area according to your schedules, whether you want to play soccer, or ...</p>
</a>
</div>

<div data-id="389796" data-name="Rankari" data-href="http://www.rankari.com" class="card startup   id6 id230 id912  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rankari.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/twitter_" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rankari.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/389796-b509c5b55a806871a17f7e0ac90995b3-thumb_jpg.jpg?buster=1398827116" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Rankari -  social media social commerce teenagers" />
<h1 property="name">
Rankari
</h1>
<span href="http://twitter.com/twitter_" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> A place to rank your favourite stuff. (GoodReads)<br></strong>
Rankari is a website where you can rank everything. Rank your top Movies, Songs, Books, Cars, Sports, Sex Positions and much more! We limit your rank to 7 choices
We provide a place in the web where your &quot;official&quot; rankings will be recorded. Imagine how fun it ...</p>
</a>
</div>

<div data-id="389580" data-name="MÁS DESCUENTOS" data-href="http://www.masdescuentos.com.mx" class="card startup   id87 id441  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.masdescuentos.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.masdescuentos.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/389580-0e6119996773636d387234b5cf3322e2-thumb_jpg.jpg?buster=1398801978" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="MÁS DESCUENTOS -  publishing discounts" />
<h1 property="name">
MÁS DESCUENTOS
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Discount website<br></strong>
Más Descuentos is a website to post, access and share discounts.
Our goal is to provide a virtual space where people find discount products and services of different brands and products. And thereby contribute one hand, to promote consumption, helps to streamline ...</p>
</a>
</div>

<div data-id="388536" data-name="Hotstreet Hyperlocal Communication" data-href="http://app.hotstreet.mx/" class="card startup   id340 id342 id673 id153125  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://app.hotstreet.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/HotStreetApp" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://app.hotstreet.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/388536-b6660ebc9e7bd266c34af9f3e57c6a72-thumb_jpg.jpg?buster=1398700673" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Hotstreet Hyperlocal Communication -  small and medium businesses market research local advertising consumers survey" />
<h1 property="name">
Hotstreet Hyperlocal Communication
</h1>
<span href="https://twitter.com/HotStreetApp" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Analytics for hyperlocal social media<br></strong>
Hotstreet is the opportunity to transform social media leads into physical and hyperlocal sales to your business. The product is a platform of social media geolocalization. Subscribe the business to the platform and select the area where you want to get the information, ...</p>
</a>
</div>

<div data-id="388122" data-name="Cloudreamx" data-href="http://cloudreamx.com/" class="card startup   id153913  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://cloudreamx.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://cloudreamx.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/388122-b11a659e518561a9c2972efda0944701-thumb_jpg.jpg?buster=1398641712" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cloudreamx -  Social Network" />
<h1 property="name">
Cloudreamx
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Comparte tus Sueños<br></strong>
Cloudreamx es red social que conecta los sueños de las personas con sus amigos, se enlazan con sueños similares, descubren grandes ideas y resuelven proyectos mediante los sueños.
Cloudreamx se usa para compartir tus sueños, subir fotos que evidencian lo que soñaron, ...</p>
</a>
</div>

<div data-id="386201" data-name="Sin Acqua " data-href="http://www.sinacqua.com.mx" class="card startup   id215 id82753 id151653  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.sinacqua.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.sinacqua.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/386201-de7fbed9396e78c79658ba4cba8b447a-thumb_jpg.jpg?buster=1398310153" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sin Acqua  -  multi-level marketing cars cleaning" />
<h1 property="name">
Sin Acqua
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Waterless MLM Cleaning Company<br></strong>
Sin Acqua is a MLM company. We sell 21 amazing ecological products for cleaning cars, houses and industries. Besides saving lots of water and protecting the environment, our customers and final users protect and preserve all their belongings by making effortless ...</p>
</a>
</div>

<div data-id="385775" data-name="Inputs" data-href="http://inputs.mx" class="card startup   id3 id12 id17 id309 id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://inputs.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://inputs.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/385775-246b8f3152a56ef60d1d77b88cd80ea7-thumb_jpg.jpg?buster=1398274482" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Inputs -  mobile enterprise software financial services sales automation small and medium businesses" />
<h1 property="name">
Inputs
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Crea formularios fácil y rápido, compártelos con el mundo entero.<br></strong>
Con Inputs usted puede habilitar a todo su personal de campo para que consulte y capture cualquier información que necesite para su trabajo utilizando una amplia gama de teléfonos y tabletas de fácil uso y bajo costo.
Ubique en todo momento a su personal en ...</p>
</a>
</div>

<div data-id="385187" data-name="Telenews" data-href="http://todoen5.com" class="card startup   id3 id205 id152800  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://todoen5.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://todoen5.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/385187-30700de6a026f6a845c5b2fff1712a08-thumb_jpg.jpg?buster=1398989565" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Telenews -  mobile mobile advertising part-time jobs" />
<h1 property="name">
Telenews
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Jobs like fiverr<br></strong>
Aplicacion de alertas instantaneas sobre asuntos relacionados con la seguridad. Creando entornos en que l agente puede utilizar recursos para asegurar un poco de paz y tranquilidad hacia su persona y familiares que los rodean.</p>
</a>
</div>

<div data-id="385132" data-name="Crescendi" data-href="" class="card startup   id43 id47  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">


</div>
</div>
<a class="main_link" href="" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/385132-075f6d9f9ba5e350d7b50afea6507502-thumb_jpg.jpg?buster=1412710814" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Crescendi -  education music" />
<h1 property="name">
Crescendi
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Music for social change <br></strong>
</p>
</a>
</div>

<div data-id="382682" data-name="Hermes Quality and Innovation in Health" data-href="http://www.hermessalud.com" class="card startup   id6 id13 id175 id201  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.hermessalud.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.hermessalud.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/382682-8467a0303d84da6267295ef1d788773f-thumb_jpg.jpg?buster=1397805655" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Hermes Quality and Innovation in Health -  social media health care health care information technology news" />
<h1 property="name">
Hermes Quality and Innovation in Health
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Health news agency and investigation<br></strong>
Hermes info is a division of Hermes Quality and Innovation in Health. Hermes info provides high-quality health and healthcare information for non-scientific public trough 3 products:
Hermes Noticias: Subscription services of Health News that includes spanish articles.
AsTec: ...</p>
</a>
</div>

<div data-id="382509" data-name="Sr. Pago" data-href="https://senorpago.com/" class="card startup   id71 id473 id1321 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://senorpago.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://senorpago.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/382509-b87f42dcb9a50a52b9b5021c1a513080-thumb_jpg.jpg?buster=1397775889" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sr. Pago -  payments mobile payments point of sale finance technology" />
<h1 property="name">
Sr. Pago
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile payments platform for unbanked in Mexico<br></strong>
Sr. Pago started in 2010 by providing a virtual point of sale (VPOS) service for e-commerce. This enables online retailers to accept payment from customers at Mexico’s largest convenience store chain, OXXO, which has more than 11,000 stores throughout the country. ...</p>
</a>
</div>

<div data-id="381968" data-name="Venew.com" data-href="http://pickandrun.com/venew/" class="card startup   id152523 id152524 id152525 id152526  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://pickandrun.com/venew/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://pickandrun.com/venew/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/381968-ac9e710b05da342e69a8935d6f6ec539-thumb_jpg.jpg?buster=1397708605" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Venew.com -  Party planners event planners Wedding planners Conference makers" />
<h1 property="name">
Venew.com
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> AirBnb for Ballrooms<br></strong>
Venew focuses on helpping people to find the best venue rooms, ballrooms,banquets and events in Mexico.
Venew gives your the posibility to search with a map, by number of guests, date, type of event and more. Just to show you the best options for your event.
We ...</p>
</a>
</div>

<div data-id="381062" data-name="Come Bien" data-href="http://www.comebien.mx" class="card startup   id72  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.comebien.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.comebien.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/381062-0fe44657a47160f6ed374ce28accce57-thumb_jpg.jpg?buster=1397611835" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Come Bien -  food and beverages" />
<h1 property="name">
Come Bien
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Customized healthy food<br></strong>
→ Tha pain: According to a new UN report (July, 2013), Mexico is now the fattest country among populous nations. Mexico's City high density traffic, lack of pedestrian planning and poor access to public transportation are the main reasons that force people to stay ...</p>
</a>
</div>

<div data-id="380760" data-name="The Global Commentator Ltd." data-href="http://globalcommentator.com/" class="card startup   id151002  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://globalcommentator.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://globalcommentator.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/380760-4cc8225ddafc09eb24c98a850e6c4999-thumb_jpg.jpg?buster=1397586331" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="The Global Commentator Ltd. -  United States" />
<h1 property="name">
The Global Commentator Ltd.
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> writer-editor network creating politics magazine<br></strong>
The Global Commentator provides an online platform for students and professors of politics to write engaging opinion articles on news and current affairs. The platform provides a network of editors in order to ensure high-quality articles.
Thereby, The Global ...</p>
</a>
</div>

<div data-id="380756" data-name="InstaDrinks" data-href="http://www.instadrinks.mx" class="card startup   id72 id418 id693 id1103  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.instadrinks.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/instadrinks" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.instadrinks.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/380756-30c2d516827c840320a04b8ae275aeb8-thumb_jpg.jpg?buster=1397584622" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="InstaDrinks -  food and beverages personal health lifestyle products health and wellness" />
<h1 property="name">
InstaDrinks
</h1>
<span href="http://www.twitter.com/instadrinks" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Premium beverages online suscriptions.<br></strong>
InstaDrinks helps you getting your favorite premium beverages directly to your home or office. In 3 clicks you'll be able to buy premium beverages (Such as Dream Water, energy drinks, functional drinks, imported and artesanial beers, etc.) with low number or none ...</p>
</a>
</div>

<div data-id="380029" data-name="Craftingeek*" data-href="http://www.craftingeek.me" class="card startup   id4 id43 id956 id1262  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.craftingeek.me" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.craftingeek.me" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/380029-0e3475db0ab74f1601a3f3d48f0cbb3c-thumb_jpg.jpg?buster=1397506786" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Craftingeek* -  digital media education content creative industries" />
<h1 property="name">
Craftingeek*
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> All Crafts &amp; DIY for the hispanic market<br></strong>
Founded by Liz in 2009 as a Youtube Channel, Craftingeek has grown to be the biggest Youtube Channel for Crafts&amp;DIY for the hispanic Market. Ever since has developed other distribution channels and products.
Craftingeek is made to be THE Crafts &amp; DIY media company ...</p>
</a>
</div>

<div data-id="378649" data-name="NCLEO" data-href="http://ncleo.com" class="card startup   id4 id552 id640 id1259  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://ncleo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/ncleo_co" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://ncleo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/378649-9917472d608bc47870ba16a6ca9ddc17-thumb_jpg.jpg?buster=1398615847" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="NCLEO -  digital media blogging platforms journalism content discovery" />
<h1 property="name">
NCLEO
</h1>
<span href="http://twitter.com/ncleo_co" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We give you an awesome writing platform so you can connect with the right public.<br></strong>
NCLEO simplifies content customization and improves publishing, it's a web platform that gives you the necessary tools so you can post great-visually-beautiful content, and connect with the right public.
If you like to share your particular viewpoint of events ...</p>
</a>
</div>

<div data-id="377828" data-name="ALR Consultores" data-href="http://www.alrconsultores.com/" class="card startup   id12 id16090  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.alrconsultores.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/alrconsultores" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.alrconsultores.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/377828-d97960cbc4ab2f177b209a585c4068d8-thumb_jpg.jpg?buster=1397158739" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ALR Consultores -  enterprise software billing" />
<h1 property="name">
ALR Consultores
</h1>
<span href="https://twitter.com/alrconsultores" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> It Consulting Services<br></strong>
We are a company focused on providing consulting services and high-tech applications (IT) focused on business
With our platform Cloud Invoicing we serve thousands of electronic invoices for customers like ESPN, Yahoo or TMM Logistics</p>
</a>
</div>

<div data-id="377262" data-name="Signer" data-href="http://signer.mx" class="card startup   id29 id340 id1474 id9847  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://signer.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://signer.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/377262-0053dc0dcc50149acc33cff4f78e72bb-thumb_jpg.jpg?buster=1397079775" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Signer -  e-commerce small and medium businesses entrepreneur freelancers" />
<h1 property="name">
Signer
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The digital equivalent to handwritten signature<br></strong>
Signer is the easiest way to get documents signed digitally, with the same legal binding than a handrwitten signature.
On top of that, it is the easiest way close deals in minutes, manage and keep track of all your contracts and agreements online.
Right now, ...</p>
</a>
</div>

<div data-id="375571" data-name="Rank On Me, S.A. de C.V." data-href="http://www.rankon.me" class="card startup   id3 id6 id339 id410  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rankon.me" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rankon.me" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/375571-7b9e756e2653ae09049314166882242a-thumb_jpg.jpg?buster=1399056880" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Rank On Me, S.A. de C.V. -  mobile social media advertising platforms reviews and recommendations" />
<h1 property="name">
Rank On Me, S.A. de C.V.
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Reviews from people like you<br></strong>
RankOn is a platform that focuses on reviews of product, entertainment and places that are generated by
your friends or by people who are like you or live like you. It is a platform with a positive approach that will help you make better purchasing decisions and ...</p>
</a>
</div>

<div data-id="371838" data-name="Grillo" data-href="http://www.grillo.mx/" class="card startup   id251 id324 id340 id158232  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.grillo.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/grilloalerta" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/GrilloAlerta" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.grillo.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/371838-6d8b13df0687aae60ccbea1bb21604a7-thumb_jpg.jpg?buster=1399505150" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Grillo -  governments nonprofits small and medium businesses Residential" />
<h1 property="name">
Grillo
</h1>
<span href="https://twitter.com/GrilloAlerta" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Disaster notifications for developing countries<br></strong>
Our first product was a device, &quot;Grillo&quot;, that connects Mexicans with their local early-warning system for earthquakes, providing up to 120 seconds warning.
However now, our team has created &quot;Grillo Active&quot; which is a complete low cost early warning system for ...</p>
</a>
</div>

<div data-id="371529" data-name="Doctor Experto" data-href="http://www.doctorexperto.com" class="card startup   id13 id223  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.doctorexperto.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.doctorexperto.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/371529-225dd53a05c88c0411b0af34778637fb-thumb_jpg.jpg?buster=1411403281" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Doctor Experto -  health care doctors" />
<h1 property="name">
Doctor Experto
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Find Mexico's best doctors<br></strong>
Doctor Experto empowers patients with tools and information to help them make a better choice about their healthcare provider. By applying a rigorous selection process to the doctors showcased, we bring trust and security to the search for a doctor.
For patients, ...</p>
</a>
</div>

<div data-id="371356" data-name="eria" data-href="http://www.realidadaumentada.com.mx" class="card startup   id1429 id2812  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.realidadaumentada.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.realidadaumentada.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/371356-e46dd1cb21660a69150c2b29233d03bc-thumb_jpg.jpg?buster=1396310748" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="eria -  digital entertainment entertainment industry" />
<h1 property="name">
eria
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Enterntainment App<br></strong>
Our app will let you enjoy a good moment with special content. Our customer could be anyone that would like to laugh with the app's content and who would like to be part of the content....
Objetive, laugh, smile and laugh and smile and so on.
We will offer content ...</p>
</a>
</div>

<div data-id="371300" data-name="Vaivén Portal de ideas" data-href="http://www.elvaiven.com" class="card startup   id1153 id1474  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.elvaiven.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@PortalVaivén" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.elvaiven.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/371300-946c9791cd682661b08bfe5cd260c04d-thumb_jpg.jpg?buster=1396304492" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Vaivén Portal de ideas -  young adults entrepreneur" />
<h1 property="name">
Vaivén Portal de ideas
</h1>
<span href="http://twitter.com/@PortalVaivén" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> digital platform for young entrepreneurs <br></strong>
Vaivén is a multidisciplinary digital platform and a meeting space, where different topics of interest to young entrepreneurs converge. It is a portal that creates and shares new content and ideas, and provides a platform for the expression of innovation. Vaivén ...</p>
</a>
</div>

<div data-id="371297" data-name="Raqqet" data-href="http://www.raqqet.com" class="card startup   id26  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.raqqet.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.raqqet.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/371297-0c73210153b584ae39d1b0e07cdb63c6-thumb_jpg.jpg?buster=1396304870" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Raqqet -  clean technology" />
<h1 property="name">
Raqqet
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Result Matches- Social Network - Sales and Marketing - Entertainment - News <br></strong>
Raqqet it’s like twitter, you can follow your friends draw matches and check with whom they had played. Also can add images and videos from the played game.
The principal customer are the tennis players and the people who like to see or follow friend matches. This ...</p>
</a>
</div>

<div data-id="371223" data-name="iintercambiame.com" data-href="http://www.iintercambiame.com" class="card startup   id29 id94 id230  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.iintercambiame.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/iintercambiame" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.iintercambiame.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/371223-2263002184dc64974f5331e6f1bfdcdd-thumb_jpg.jpg?buster=1396297681" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="iintercambiame.com -  e-commerce mobile commerce social commerce" />
<h1 property="name">
iintercambiame.com
</h1>
<span href="http://twitter.com/iintercambiame" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Trade, sell and buy and get rewarded<br></strong>
Its a loyalty program consumer to consumer that allows to trade, buy and sell and gets you rewarded.</p>
</a>
</div>

<div data-id="368457" data-name="Dominios" data-href="http://www.dominios.pro" class="card startup   id415  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.dominios.pro" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.dominios.pro" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/368457-e471b2c12349b2e87d40e8274d78b40d-thumb_jpg.jpg?buster=1395861815" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Dominios -  domains" />
<h1 property="name">
Dominios
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Domain marketplace<br></strong>
Domain marketplace.</p>
</a>
</div>

<div data-id="368453" data-name="Garitas" data-href="http://www.garitas.org" class="card startup   id65  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.garitas.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.garitas.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/368453-3d525b380409993abd0b64272d50837f-thumb_jpg.jpg?buster=1395861107" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Garitas -  information services" />
<h1 property="name">
Garitas
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Border wait times.<br></strong>
Border Wait Times for 50,000 daily crossborder commuters.</p>
</a>
</div>

<div data-id="368450" data-name="Cupones" data-href="http://www.coupon.com.mx" class="card startup   id424  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.coupon.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.coupon.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/368450-45bc8c0a4edae742c9307639b8ea4961-thumb_jpg.jpg?buster=1395860899" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cupones -  deals" />
<h1 property="name">
Cupones
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Coupons for LATAM<br></strong>
Find the best deals and offers.</p>
</a>
</div>

<div data-id="368445" data-name="Buscar" data-href="http://www.buscar.pro" class="card startup   id67  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.buscar.pro" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.buscar.pro" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/368445-d9ffa83ad5dbdba175ec1165ad587726-thumb_jpg.jpg?buster=1395860779" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Buscar -  search" />
<h1 property="name">
Buscar
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Professional Searches<br></strong>
Find what you are looking for.</p>
</a>
</div>

<div data-id="368439" data-name="Licenciado" data-href="http://www.licenciado.net" class="card startup   id1000  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.licenciado.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.licenciado.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/368439-f243f8f5a338fb8e98451e85c166b1d1-thumb_jpg.jpg?buster=1395859755" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Licenciado -  private social networking" />
<h1 property="name">
Licenciado
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social network for professionals<br></strong>
Connect and keep in touch with professionals.</p>
</a>
</div>

<div data-id="368003" data-name="Circulo Viajero" data-href="https://pitch.liveplan.com/fdMTv/tqmMw" class="card startup   id78 id431 id1076 id2613  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://pitch.liveplan.com/fdMTv/tqmMw" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://pitch.liveplan.com/fdMTv/tqmMw" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/368003-469f9de2a7e448a9890b95c3a9966d3d-thumb_jpg.jpg?buster=1395806939" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Circulo Viajero -  online travel social recruiting social travel university students" />
<h1 property="name">
Circulo Viajero
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Diviértete viajando, vive ganando (Circulo Viajero)<br></strong>
Circulo Viajero is a travel agency that specializes in leisure tourism and travel. It will provide consulting and custom travel arrangements and packages. Circulo Viajero employees and owner are travel enthusiasts,
looking to create a travel club with members all ...</p>
</a>
</div>

<div data-id="367600" data-name="Pantone321" data-href="http://www.pantone321.com" class="card startup   id129 id1474  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.pantone321.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.pantone321.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/367600-487ea4c8ad75adc7a4861057d9f859b2-thumb_jpg.jpg?buster=1395773720" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pantone321 -  graphics entrepreneur" />
<h1 property="name">
Pantone321
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> pantone321.com, Mexico Creative Crowdsourcing <br></strong>
Pantone321.com es una plataforma web, que tiene como objetivo resolver las necesidades de diseño gráfico que tienen los emprendedores y PYMES (pequeñas y medianas empresas), así como ofrecer un ambiente de creatividad y desarrollo de jóvenes diseñadores.
El sitio ...</p>
</a>
</div>

<div data-id="365983" data-name="Music Sound Lab" data-href="http://www.musicsoundlab.com" class="card startup   id34 id646 id1479 id9166  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.musicsoundlab.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@musicsoundlab" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.musicsoundlab.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/365983-1e88c925a5b941dfea86e12f67265c64-thumb_jpg.jpg?buster=1395606678" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Music Sound Lab -  advertising musical instruments music services musicians" />
<h1 property="name">
Music Sound Lab
</h1>
<span href="http://twitter.com/@musicsoundlab" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social Network for Musicians<br></strong>
musicsoundlab.com is the best , most trusted , professional and important social network for musicians in Mexico and Latin America. This platform features a powerful search engine for musicians, musical genres and bands that can be as specific as the user requires ...</p>
</a>
</div>

<div data-id="365261" data-name="Pulso Chic" data-href="http://www.fitandfashion.mx" class="card startup   id29 id32 id93 id553  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.fitandfashion.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.fitandfashion.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/365261-b482494a57a436ec66e2b39a5f0cebc2-thumb_jpg.jpg?buster=1395452149" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pulso Chic -  e-commerce retail fashion women-focused" />
<h1 property="name">
Pulso Chic
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> ebay for plus-size women's fashion<br></strong>
Pulso Chic (and our pilot Fit &amp; Fashion) is a fashion e-tailer for REAL women, aimed to the serve the fashion conscious women in Mexico who tend to be neglected by the traditional fashion retailers.
Our challenge is to bring FASHION to women who struggle with ...</p>
</a>
</div>

<div data-id="365122" data-name="Semant.io" data-href="http://www.semant.io" class="card startup   id6 id198 id1470 id2800  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.semant.io" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.semant.io" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/365122-7876935e61ba00322491c775e91b86a3-thumb_jpg.jpg?buster=1404759503" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Semant.io -  social media big data predictive analytics business analytics" />
<h1 property="name">
Semant.io
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Real-time predictive social analytics tool <br></strong>
Semant.io is a real-time predictive social analytics tool for business, organisations and politicians.
Semant.io do a real-time monitoring of social media, local digital media (newspaper, radio, television, magazines) and communities (both owned and earned) to ...</p>
</a>
</div>

<div data-id="365059" data-name="Enviando" data-href="http://www.latinventurehub.com" class="card startup   id1431  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.latinventurehub.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@latinventurehub" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.latinventurehub.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/365059-6bdec52fc8bfe5ffdbe7a8216413c2e1-thumb_jpg.jpg?buster=1395428562" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Enviando -  b2b express delivery" />
<h1 property="name">
Enviando
</h1>
<span href="http://twitter.com/@latinventurehub" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Express-delivery service for Latin American Cities <br></strong>
Enviando is a mobile and web based platform that connects existing same day courier providers such as: bike messengers, taxi drivers and other existing courier companies, with potential costumers who want to send/deliver goods that same day within their city.
We ...</p>
</a>
</div>

<div data-id="365038" data-name="Monterde y Antillon" data-href="http://monterdeyantillon.com/" class="card startup   id553 id555  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://monterdeyantillon.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://monterdeyantillon.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/365038-6428238964647b7857bded5f9faef794-thumb_jpg.jpg?buster=1395426875" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Monterde y Antillon -  women-focused weddings" />
<h1 property="name">
Monterde y Antillon
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
 <div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> wedding planner app<br></strong>
Planning a wedding is exciting and complicated at the same time. Unitl now the planning has been done using books or binders describing a process and specific steps. With the new smartphones and iPads, the binders have become applications that continue with the ...</p>
</a>
</div>

<div data-id="364994" data-name="Contalisto " data-href="http://www.contalisto.com" class="card startup   id17 id374 id585 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.contalisto.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.contalisto.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364994-3ac1569f08b79699943b212c941d249f-thumb_jpg.jpg?buster=1395421833" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Contalisto  -  financial services personal finance accounting finance technology" />
<h1 property="name">
Contalisto
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mi Contador de Bolsillo<br></strong>
Contalisto es la primera plataforma tecnológica contable en México, facilita de manera digital la contabilidad y presentación de impuestos de las personas físicas y contribuyentes obligados a declarar ante el fisco, enviando su documentación con la app y pagando ...</p>
</a>
</div>

<div data-id="364764" data-name="Cuantificare" data-href="http://cuantificare.com.mx" class="card startup   id680  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://cuantificare.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@cuantificare" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://cuantificare.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364764-aa568928fd770251b0be6427d8045726-thumb_jpg.jpg?buster=1395385866" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cuantificare -  construction" />
<h1 property="name">
Cuantificare

</h1>
<span href="http://twitter.com/@cuantificare" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> La herramienta más rápida para Cuantificar, Generar y Estimar las Obras en Construcción!<br></strong>
Cuantificare es una web-app que le permite a los contratistas de la construcción, generar en minutos, el expediente que comprueba los trabajos que ha concluido de construir y que por lo tanto ya puede cobrar. Dicho expediente lo llamamos ESTIMACION.
Nuestro sistema, ...</p>
</a>
</div>

<div data-id="364563" data-name="PeQe" data-href="http://Trabajando en ello" class="card startup   id3 id43  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://Trabajando en ello" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://Trabajando en ello" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364563-9abac3d26181bb40c399bbceeeb4c20b-thumb_jpg.jpg?buster=1395944702" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="PeQe -  mobile education" />
<h1 property="name">
PeQe
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Dispositivo de seguridad para niños<br></strong>
&quot;PeQe te ayuda a observar tus hijos crear su mundo sin perderles de vista&quot;
La aplicación PeQe, es de un lado una pulsera en forma de reloj que tu hijo traerá consigo y del otro una aplicación móvil en tu SMP que te permitira localizar a tu hijo en un rango de ...</p>
</a>
</div>

<div data-id="364561" data-name="Illumina" data-href="http://www.illumina.me" class="card startup   id481 id1469  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.illumina.me" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.illumina.me" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364561-821710cf1eb27ea13370c9c8d7414d7e-thumb_jpg.jpg?buster=1395358449" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Illumina -  kids k 12 education" />
<h1 property="name">
 Illumina
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Solving learning problems through games<br></strong>
Illumina is a web platform that joins educational contents with videogames. It helps students in their learning process by making simple but fun games while keeping track of their progress and giving them efective feedback during the process. It algo helps parents ...</p>
</a>
</div>

<div data-id="364422" data-name="Futbolitos" data-href="http://futbolitos.wix.com/futbolsiempre" class="card startup   id73 id481 id1433  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://futbolitos.wix.com/futbolsiempre" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@futbolitosinc" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://futbolitos.wix.com/futbolsiempre" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364422-4ed9e5f738b525ae7cfad861e08c9803-thumb_jpg.jpg?buster=1395374372" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Futbolitos -  sports kids soccer" />
<h1 property="name">
Futbolitos
</h1>
<span href="http://twitter.com/@futbolitosinc" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Football forever<br></strong>
Fantastic stadiums, amazing futbolitos, an incredible football league. All of these you can find it with the FUTBOLITOS, an exclusive brand for all the football lovers around the world.</p>
</a>
</div>

<div data-id="364364" data-name="veoveo" data-href="http://www.veo-veo.net" class="card startup   id93 id386 id481 id837  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.veo-veo.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.veo-veo.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364364-557b4cfd09183780b3dfbe637f47041f-thumb_jpg.jpg?buster=1395344322" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="veoveo -  fashion design kids babies" />
<h1 property="name">
veoveo
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> kids clothing<br></strong>
Veoveo creates clothing and accessories for babies and children (1-5 years old) that reflects in essence a world of imagination and play.
Our products are essentially practical, modern, high quality and design in its details and prints.
We are a proudly Mexican ...</p>
</a>
</div>

<div data-id="364232" data-name="GivU" data-href="http://givu.net" class="card startup   id4 id29 id269 id154390  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://givu.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/Givuusa?fref=ts" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/welovegivu" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://givu.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364232-32430d38d72f8a396b894c3c454a0e85-thumb_jpg.jpg?buster=1395333270" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="GivU -  digital media e-commerce social media platforms Online marketplaces" />
<h1 property="name">
GivU
</h1>
<span href="https://twitter.com/welovegivu" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The P2P online market where you can buy lifestyle products with what you have<br></strong>
GivU is the social marketplace that gives you a better and faster way to buy new and new-to-you lifestyle items with what you already have, by leveraging a new way to buy. Only GivU combines the fun of sharing and socializing, the excitement and satisfaction of ...</p>
</a>
</div>

<div data-id="364217" data-name="Médicos" data-href="http://medicos-app.com" class="card startup   id13 id19 id3136  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://medicos-app.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://medicos-app.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/364217-ca324433cbf7876ef7c3999508309a8a-thumb_jpg.jpg?buster=1395332306" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Médicos -  health care medical medical professionals" />
<h1 property="name">
Médicos
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Connecting doctors with patients<br></strong>
&quot;Médicos&quot; lets you contact directly and inmediatly healthcare professionals around you. It's free for users. It has, until now, more than 2000 downloads at App Store (since January 6th) and more than 115 healthcare professionals suscribed.</p>
</a>
</div>

<div data-id="363950" data-name="NeuroCrowd" data-href="http://neurocrowd.com/" class="card startup   id34 id686 id1056 id11826  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://neurocrowd.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@neurocrowd" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://neurocrowd.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/363950-a13acf136e2177a73b0facf1b0694a8f-thumb_jpg.jpg?buster=1418091918" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="NeuroCrowd -  advertising brand marketing technology user experience design" />
<h1 property="name">
NeuroCrowd
</h1>
<span href="http://twitter.com/@neurocrowd" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Leveraging Customers Emotions<br></strong>
There is an increasing demand for reliable information on the needs and preferences of consumers in the age of digital communication, which represents over $62Bn annually only in US, and $500Bn on a global scale. However the solutions available, are based on traditional ...</p>
</a>
</div>

<div data-id="363920" data-name="MX32" data-href="http://studiophi.net/mx32/" class="card startup   id73 id1103 id1143 id16414  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://studiophi.net/mx32/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

<a href="http://www.twitter.com/emeequis32" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://studiophi.net/mx32/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/363920-822f44a4ebbf1f88b65e94c3b7e2be27-thumb_jpg.jpg?buster=1395289692" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="MX32 -  sports health and wellness adventure travel active lifestyle" />
<h1 property="name">
MX32
</h1>
<span href="http://www.twitter.com/emeequis32" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Road trip / Travel-guide / Documentary of Mexico's Adventure Destinations <br></strong>
MX32 is a documentary web series that follow the steps of two photographers in the quest of discovering real Mexico, at the same time we will be creating the ultimate adventure / sports / travel guide, that will show you prices, how family or pet friendly the destination ...</p>
</a>
</div>

<div data-id="363891" data-name="alcanzar.com" data-href="http://www.alcanzar.com" class="card startup   id16 id856 id9708 id10167  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.alcanzar.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.alcanzar.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/363891-814e0662220922eee9f74b00f83f5090-thumb_jpg.jpg?buster=1395285088" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="alcanzar.com -  real estate venture capital commercial real estate real estate investors" />
<h1 property="name">
alcanzar.com
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Real Estate Social Network for Latam<br></strong>
There are different tools for real estate agents or agencies in the latin american market to manage, share, or advertise properties but none of these solutions really connect stakeholders effectively in order to increase sales or provide a better service for customers.
Alcanzar.com ...</p>
</a>
</div>

<div data-id="363420" data-name="Hundra Seven:7" data-href="https://twitter.com/Papps_latam" class="card startup   id3 id29 id34 id253  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://twitter.com/Papps_latam" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://twitter.com/Papps_latam" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/363420-a68c7fa55ad080fc53e42a36b825a46b-thumb_jpg.jpg?buster=1395251024" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Hundra Seven:7 -  mobile e-commerce advertising consulting" />
<h1 property="name">
Hundra Seven:7
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Code marketplace for Latin America<br></strong>
The name of our product is Papps (Plataforma de Apps). We think that our name is appealing because here in Mexico, ¨paps&quot; is common slang within young, tech savvy and middle and upper class people. Papps it´s a website where you can buy and sell source code for ...</p>
</a>
</div>

<div data-id="363089" data-name="Codigo Facilito" data-href="https://codigofacilito.com" class="card startup   id43 id156259  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://codigofacilito.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/codigofacilito" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/codigofacilito" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="https://codigofacilito.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/363089-2cc595634573cf38f1f0f5c8171f35cf-thumb_jpg.jpg?buster=1419308624" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Codigo Facilito -  education Code Schools" />
<h1 property="name">
Codigo Facilito
</h1>
<span href="https://twitter.com/codigofacilito" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Code education for every one in latin america<br></strong>
</p>
</a>
</div>

<div data-id="362942" data-name="MegaQuinielas" data-href="http://www.megaquinielabrasil2014.com" class="card startup   id64 id73 id1153  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.megaquinielabrasil2014.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.megaquinielabrasil2014.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/362942-f6c37fde33a13f17df3fb7d3ef0dd750-thumb_jpg.jpg?buster=1395182732" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="MegaQuinielas -  gambling sports young adults" />
<h1 property="name">
MegaQuinielas
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Quinielas Sociales<br></strong>
MegaQuinielas te permite crear quinielas sociales entre tus amigos con tu cuenta de facebook, Tu pones el monto de entrada a tu quiniela nosotros te ayudamos a administrarla es ¡muy fácil! , Demuéstrale a tus amigos que tanto sabes de futbol</p>
</a>
</div>

<div data-id="362885" data-name="Bitfel" data-href="http://bitfel.mx" class="card startup   id12 id332 id3127 id16090  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://bitfel.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://bitfel.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/362885-31ef44d116c38f8688d443f2c024a02a-thumb_jpg.jpg?buster=1417502871" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bitfel -  enterprise software web design web development billing" />
<h1 property="name">
Bitfel
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Electronic Billing Web Services<br></strong>
Since 2014 in Mexico, the &quot;Secretaria de Administración Tributaria&quot; stablished that every business must generate electronic bills. In base of that need we created BITFEL that is a web system of electronic billing focused on all kind of business who need to generate ...</p>
</a>
</div>

<div data-id="362832" data-name="LaMinaDeRoma" data-href="http://rockonfire.mx/la-mina-de-roma/" class="card startup   id9166  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://rockonfire.mx/la-mina-de-roma/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@RockOnFireMX" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://rockonfire.mx/la-mina-de-roma/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/362832-7521f9a8440a3f69d543a8fdcc5eb035-thumb_jpg.jpg?buster=1395173618" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="LaMinaDeRoma -  musicians" />
<h1 property="name">
LaMinaDeRoma
</h1>
<span href="http://twitter.com/@RockOnFireMX" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Digital Marketing for Rock Bands<br></strong>
LaMinaDeRoma is the boutique Digital Agency for emerging rock bands from the globe allowing them to have professional advise on digital marketing campaigns, press kits &amp; web design.
Focused on their new music releases LaMinaDeRoma agency will advise the rock groups ...</p>
</a>
</div>

<div data-id="362804" data-name="Nemesys Identificación Biométrica" data-href="http://www.nemesysbiometrics.com" class="card startup   id3 id12 id13 id38  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.nemesysbiometrics.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.nemesysbiometrics.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/362804-18673a3c6240cce9c1b79bdc89763bf2-thumb_jpg.jpg?buster=1395967846" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Nemesys Identificación Biométrica -  mobile enterprise software health care cloud computing" />
<h1 property="name">
Nemesys Identificación Biométrica
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Plug and Play Biométrico<br></strong>
Nemesys es un conjunto de softwares de identificación biométrica 100% mexicano.
Permite la optimización de recursos en los desarrollos de software multiplataforma con tecnología biométrica ya que da independencia en el uso de diferentes marcas y proveedores de ...</p>
</a>
</div>

<div data-id="361696" data-name="KOYBU" data-href="http://koybu.com/demo/koybuflash/" class="card startup   id106 id340 id1980 id9100  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://koybu.com/demo/koybuflash/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://koybu.com/demo/koybuflash/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/361696-775815425fd63b4bfa0fc8cc11a3500d-thumb_jpg.jpg?buster=1395053618" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="KOYBU -  virtual worlds small and medium businesses social crm travel &amp; tourism" />
<h1 property="name">
KOYBU
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> 3D-Cities Business-Network Virtual-Tours<br></strong>
KOYBU  helps Small &amp; Medium Entreprises owners to empower their internet presence and to show their installations with virtual tours (way beyond Google Street View). 
Market:
- Tourism-related companies who want to offer vacational deals and want to show their ...</p>
</a>
</div>

<div data-id="361241" data-name="Canier" data-href="http://www.canier.com.mx/" class="card startup   id29 id424 id692 id1432  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.canier.com.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.canier.com.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/361241-df7f8774d55a830c062ab19ea130bc4f-thumb_jpg.jpg?buster=1394950860" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Canier -  e-commerce deals shopping online shopping" />
<h1 property="name">
Canier
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Amazon for Latin America<br></strong>
We are a company with the firm goal of facilitating online shopping with delivery times of 99 minutes.
Our greatest strengths will be quick delivery times and our superior quality in the before, during and after sales services.</p>
</a>
</div>

<div data-id="360657" data-name="HUBSTN" data-href="http://www.hubstn.com" class="card startup   id311 id448 id1113 id1946  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.hubstn.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.hubstn.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/360657-a9ca6706c0ef9d54342caf3ef703590b-thumb_jpg.jpg?buster=1394817953" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="HUBSTN -  collaboration startups business development coworking" />
<h1 property="name">
HUBSTN
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Binational Coworking Space<br></strong>
HUB STN is Mexico's first Binational Coworking Space that integrates the Entrepreneur, Innovative and Creative Communities of both San Diego in Tijuana with a focus of developing crossborder economies and collaboration.</p>
</a>
</div>

<div data-id="359561" data-name="estacionados.com" data-href="http://www.estacionados.com" class="card startup   id3 id4  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.estacionados.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.estacionados.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/359561-dca8cc3563b0fe011a72a6fc9f5d95ae-thumb_jpg.jpg?buster=1394658606" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="estacionados.com -  mobile digital media" />
<h1 property="name">
estacionados.com
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Aplicación digital para reservar estacionamiento<br></strong>
estacionados.com es una aplicación digital, que busca dar solución al enorme problema de encontrar estacionamiento en la ciudad de México, y otras grandes urbes latinoamericanas, a través de un sistema de reservaciones y/o pago, que está sincronizado a una red ...</p>
</a>
</div>

<div data-id="359555" data-name="meXBT " data-href="http://www.mexbt.com" class="card startup   id71 id1406 id2813 id93839  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.mexbt.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/mexbt" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/Mexbt" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.mexbt.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/359555-bed67b2f4173d03acad4e15f7a0e0dac-thumb_jpg.jpg?buster=1404835917" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="meXBT  -  payments trading p2p money transfer bitcoin" />
<h1 property="name">
meXBT
</h1>
<span href="https://twitter.com/Mexbt" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Trading platform for crypto currencies in Latin America. <br></strong>
meXBT / Crypto Exchange of the Americas is a full stack professional trading platform for crypto currencies that enable institutional &amp; retail customer the purchase of Crypto. - &quot;The Bitstamp of Latin America&quot;</p>
</a>
</div>

<div data-id="359488" data-name="elxox" data-href="http://www.elxox.com" class="card startup   id29  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.elxox.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.elxox.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/359488-2bf09cdb32080025aefb73f122e36ba9-thumb_jpg.jpg?buster=1395449276" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="elxox -  e-commerce" />
<h1 property="name">
elxox
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Exceptional prices for everyday<br></strong>
An exceptional site on prices for everyday needs all throughout Latin America, that provides customers with a unique online buying experience, providing customers with daily deals deeply discounted for a wide variety of audiences including men and mainly women ...</p>
</a>
</div>

<div data-id="359483" data-name="Cuponera" data-href="http://www.Cuponera.com" class="card startup   id174 id305 id424 id443  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.Cuponera.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.Cuponera.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/359483-cb11032b9db813073afa7711bcf4f469-thumb_jpg.jpg?buster=1394651351" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cuponera -  coupons custom retail deals mobile coupons" />
<h1 property="name">
Cuponera
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Pinterest for coupons<br></strong>
Cuponera is coupon social platform that lets you:
- Explore coupons based on your location &amp; interest
- Tag them to your coupon book, and
- Follow your favorite places to receive their offers and deals on your feed.
With cuponera we make it easy and fun to browse, ...</p>
</a>
</div>

<div data-id="358833" data-name="RedFin" data-href="http://www.redfin.com.mx" class="card startup   id17  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.redfin.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.redfin.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/358833-6c1b07dfcff6d1a99e2cb03705672fee-thumb_jpg.jpg?buster=1395195575" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="RedFin -  financial services" />
<h1 property="name">
RedFin
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Peer-to-peer lending for Postgraduate business communities<br></strong>
RedFin connects postgraduates looking for financing (student loan refinancing or credit solutions for their running or starting business) with other postgraduates and outside lenders willing to lend their money in exchange for higher returns within a familiar and ...</p>
</a>
</div>

<div data-id="358355" data-name="Freelan" data-href="http://www.freelan.com.mx" class="card startup   id4 id175 id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.freelan.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.freelan.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/358355-91dc5bbf1b92b6909da599e2c4ec228c-thumb_jpg.jpg?buster=1394519924" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Freelan -  digital media health care information technology small and medium businesses" />
<h1 property="name">
Freelan
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> SMB Marketing (i.e. Flock) &amp; thematic networks (i.e. InventMX)<br></strong>
We have 3 business units:
1) Marketing for SMBs: Offer medium and big (usually familiy-owned) companies marketing tools that international corporations use everyday. We focus on: digital marketing, PR, Big Data and web development.
This business unit is already ...</p>
</a>
</div>

<div data-id="358042" data-name="CoinBatch" data-href="http://coinbatch.com" class="card startup   id17 id93839 id154551 id155046  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://coinbatch.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/coinbatch" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/coinbatch" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://coinbatch.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/358042-e2f00140a88e9218756c6db4d02b6d58-thumb_jpg.jpg?buster=1394487166" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CoinBatch -  financial services bitcoin Remittance Bitcoin Exchange" />
<h1 property="name">
CoinBatch
</h1>
<span href="http://twitter.com/coinbatch" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Money Transfer Services using the Bitcoin Protocol<br></strong>
CoinBatch brings the Bitcoin technology to Mexico, disrupting the US$23 billion remittance market in Mexico by leveraging Bitcoin to cut transaction costs in half.</p>
</a>
</div>

<div data-id="358009" data-name="ComeCasero" data-href="http://www.comecasero.com" class="card startup   id72 id12088  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.comecasero.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/ComeCaseroMx" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/Come_Casero" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.comecasero.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/358009-e4ab806dce69e7d69aed43b793f9fd51-thumb_jpg.jpg?buster=1404353142" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ComeCasero -  food and beverages cooking" />
<h1 property="name">
ComeCasero
</h1>
<span href="https://twitter.com/Come_Casero" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online marketplace for homemade food<br></strong>
ComeCasero is an online marketplace for homemade food. Our goal is to contact people who are passionate about cooking with those who want to order something to eat. ComeCasero is a healthy and homey alternative when ordering food.</p>
</a>
</div>

<div data-id="357877" data-name="Quincenas" data-href="http://www.Quincenas.com.mx" class="card startup   id321  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.Quincenas.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/Quincenas" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.Quincenas.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/357877-a15d4fdbdadf9f67e020d07cc16e1ba8-thumb_jpg.jpg?buster=1394475042" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Quincenas -  finance" />
<h1 property="name">
Quincenas
</h1>
<span href="http://www.twitter.com/Quincenas" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Payday loans (Web-based Moneytree for Mexico)<br></strong>
Quincenas is a Payday Loan platform that helps active professionals find short term financial stress relief by offering payday advances and loans on their salaries through simple and affordable short term loans.
www.Quincenas.com.mx
www.facebook.com/Quincenas
www.twitter.com/Quincenas
www.Pinterest.com/Quincenas</p>
</a>
</div>

<div data-id="357876" data-name="Maatka" data-href="http://www.maatka.com" class="card startup   id409 id740  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.maatka.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.maatka.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/357876-ab3407694ed9a41780a679ffb8d5c24a-thumb_jpg.jpg?buster=1394474975" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Maatka -  development platforms service providers" />
<h1 property="name">
Maatka
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> SaaS framework for Telcos<br></strong>
Telcos are searching for new revenue streams without adding new operational burden to their current complexity. At least in Latin America, Telcos are solving this through strategic partnerships to deliver Value Added Services (VAS) to their current customer base. ...</p>
</a>
</div>

<div data-id="356201" data-name="Chulel" data-href="http://www.chulel.com.mx" class="card startup   id29 id32 id93  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.chulel.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@chulelmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.chulel.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/356201-24c41e67cd2ac9666bc43a41735f36fe-thumb_jpg.jpg?buster=1394213581" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Chulel -  e-commerce retail fashion" />
<h1 property="name">
Chulel
</h1>
<span href="http://twitter.com/@chulelmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Accessories with SOUL<br></strong>
Chulel &quot;soul&quot; in native language, is a mexican brand that creates fashion accessories inspired by the colors, shapes, and ways of living of ancient Mexican cultures.
Chulel main objective lies in preserving the old philosophy and traditions that have been passed ...</p>
</a>
</div>

<div data-id="355957" data-name="Shopliment" data-href="http://www.shopliment.com" class="card startup   id3 id32 id93 id692  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.shopliment.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.shopliment.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/355957-890f9c42806c73dbed4f6109eed0bae5-thumb_jpg.jpg?buster=1394141958" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Shopliment -  mobile retail fashion shopping" />
<h1 property="name">
Shopliment
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Never to miss the chance of owning what you want.<br></strong>
Shopliment a mobile shopping tools that extends your in-store shopping experience till you buy them later (online or in store).
It's easy to own what you want: Just save them for later purchase or be gifted by your loved one.</p>
</a>
</div>

<div data-id="353919" data-name="Sparkjoy" data-href="http://www.getsparkjoy.com" class="card startup   id97 id205 id379 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.getsparkjoy.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/getsparkjoy" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.getsparkjoy.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/353919-75e7a96ccb9a5e28fbebcbfddfe3bb00-thumb_jpg.jpg?buster=1409215581" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sparkjoy -  business intelligence mobile advertising retail technology big data analytics" />
<h1 property="name">
Sparkjoy
</h1>
<span href="http://twitter.com/getsparkjoy" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Adwords for the real world.<br></strong>
Sparkjoy allows merchants and brands to serve local-based advertising to their customers through push notifications.</p>
</a>
</div>

<div data-id="352889" data-name="Garage Video" data-href="http://www.video-garage.com" class="card startup   id34 id340 id448  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.video-garage.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.video-garage.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/352889-bc27e0fdb9957cb14153f4019751d2d7-thumb_jpg.jpg?buster=1393724933" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Garage Video -  advertising small and medium businesses startups" />
<h1 property="name">
Garage Video
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Henry Ford for videos<br></strong>
Over 95% of companies worldwide can not enter the Television due to high production costs and airtime.
Elsewhere on the Internet we are filled with mountains of information which makes it difficult to connect with our customers.
That´s why in Garage we are ...</p>
</a>
</div>

<div data-id="352418" data-name="Red Light Stock Exchange" data-href="http://www,tendencias.com.mx" class="card startup   id3 id17 id304 id374  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www,tendencias.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@Carloslopezjone" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www,tendencias.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/352418-71d2f24db0b192d6106a71019007096d-thumb_jpg.jpg?buster=1393622928" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Red Light Stock Exchange -  mobile financial services investment management personal finance" />
<h1 property="name">
Red Light Stock Exchange
</h1>
<span href="http://twitter.com/@Carloslopezjone" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Tool to invest in the Stock Exchange Market<br></strong>
Concept: It is an indicator for investors. Red keep out and Sell, Yellow: be careful or aware Green: Buy
It is very simple to use, Not complicated formulas, not complicated words, just three universal colours: Red, Yellow, Green
Example: Open your smartcell APP ...</p>
</a>
</div>

<div data-id="351825" data-name="Ofertify" data-href="http://www.ofertify.com" class="card startup   id29 id406  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ofertify.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/ofertify" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ofertify.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/351825-40715a346dc9891e90f87e1525823f93-thumb_jpg.jpg?buster=1393529733" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Ofertify -  e-commerce product search" />
<h1 property="name">
Ofertify
</h1>
<span href="https://twitter.com/ofertify" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Locally sourced amazon<br></strong>
Ofertify is has two primary customers. Businesses and consumers. And brands, but that's long term.
Businesses: We help businesses reach and engage their customers.
We offer several tools for acquiring , retaining and engaging customers. From the simple store positioning ...</p>
</a>
</div>

<div data-id="351368" data-name="Animal Protect and Love" data-href="http://www.animalprotectandlove.org/" class="card startup   id3 id29 id43 id230  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.animalprotectandlove.org/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.animalprotectandlove.org/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/351368-27a9fd96394115c577853b2f6ae6ade8-thumb_jpg.jpg?buster=1393462821" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Animal Protect and Love -  mobile e-commerce education social commerce" />
<h1 property="name">
Animal Protect and Love
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Platform adoption and animal care<br></strong>
Animal Protect and Love emerges as a sustainable social enterprise, which aims to be the solution of the problems of abandonment, overpopulation and animal abuse, through technology, it is why we created as a first contribution, a web and mobile platform for animal ...</p>
</a>
</div>

<div data-id="351276" data-name="Planetary" data-href="http://www.planetarios.url.ph" class="card startup   id318 id368 id448 id1056  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.planetarios.url.ph" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/Planetary_HQ" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.planetarios.url.ph" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/351276-86f5bf301ff2128a897847a5f3004330-thumb_jpg.jpg?buster=1393458109" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Planetary -  industrial automation product design startups technology" />
<h1 property="name">
Planetary
</h1>
<span href="https://twitter.com/Planetary_HQ" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We are a company based on two concepts: Mechatronics and Open Source. <br></strong>
Planetary is a company based on two concepts: mechatronics and open source.
Founded in 2013, Planetary is a company dedicated to the implementation of mechatronic design for complex problem solving.
Starting from the thesis work of its members, is currently ...</p>
</a>
</div>

<div data-id="349480" data-name="Canvaz" data-href="http://www.canvaz.mx" class="card startup   id246 id386  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.canvaz.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/canvazmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.canvaz.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/349480-79816924015a1cc1709d986243955dcf-thumb_jpg.jpg?buster=1393286277" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Canvaz -  crowdsourcing design" />
<h1 property="name">
Canvaz
</h1>
<span href="http://www.twitter.com/canvazmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> You submit and pick the best designs. We make them a reality.<br></strong>
Canvaz works with creative talent and helps them find their audience. Currently we are Threadless for Latin America but our vision expands much farther.
We believe the region is filled with talent. That is why we are creating a crowdsourcing platform for individuals ...</p>
</a>
</div>

<div data-id="349297" data-name="Mi Propiedad" data-href="http://www.mipropiedad.mx" class="card startup   id10 id2613 id9407 id9708  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.mipropiedad.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.mipropiedad.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/349297-9bd87f12c58f708cfc50c0d1f2517d51-thumb_jpg.jpg?buster=1393277051" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Mi Propiedad -  SaaS university students general public worldwide commercial real estate" />
<h1 property="name">
Mi Propiedad
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> SaaS, Real Estate, Property Management, Services, Search<br></strong>
 Mi Propiedad is a software as a service for Real Estate Agents and users to keep in touch. The platforms helps people to find properties in sale, rent, buildings, student rooms and vacations with the best filters for a smarter search, also is a review platform ...</p>
</a>
</div>

<div data-id="349296" data-name="Uncrowder" data-href="https://vimeo.com/60679003" class="card startup   id3 id246 id595  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://vimeo.com/60679003" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://vimeo.com/60679003" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/349296-b3ce767082d62c58800e8eb5da73c601-thumb_jpg.jpg?buster=1393276680" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Uncrowder -  mobile crowdsourcing maps" />
<h1 property="name">
Uncrowder
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> UnCrowding all the Places<br></strong>
There's the tech to define where is no traffic at all (Waze, Google Maps, etc). But what if we have the tech to define where's no people at all?
Here's UNCROWDER, the tech to discover non-crowded places. Discover new places and enjoy the uncrowd.</p>
</a>
</div>

<div data-id="348121" data-name="Sr. Pago" data-href="http://www.srpago.com" class="card startup   id29 id263 id1146 id1321  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.srpago.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.srpago.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/348121-e4160f4d6755a87972db29ba0f3ab6fc-thumb_jpg.jpg?buster=1393128968" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sr. Pago -  e-commerce restaurants app stores point of sale" />
<h1 property="name">
Sr. Pago
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> SocialCommerce Network<br></strong>
Cobra online and the brand Sr.Pago are focused on create a social payment network in Mexico City, our app let the people to receive payments using our three products, eCommerce for online payments, mPOS a mobile point of sale and Cobra Rapido, a link that allows ...</p>
</a>
</div>

<div data-id="347573" data-name="eMedics" data-href="http://emedics.co" class="card startup   id13  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://emedics.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/emedics_" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://emedics.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/347573-68779a8cc48d4c3b283fba439239cdf5-thumb_jpg.jpg?buster=1393957240" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="eMedics -  health care" />
<h1 property="name">
eMedics
</h1>
<span href="http://twitter.com/emedics_" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Electronic Health Record Made Easy<br></strong>
Take control of your medical history.
eMedics is a cloud platform that facilitates doctors to record the medical history of their patients electronically.
eMedics allows to take a detailed control of the patient’s visits to his doctor, recording its diagnosis ...</p>
</a>
</div>

<div data-id="346000" data-name="Brand Care" data-href="http://brandcare.com.mx" class="card startup   id23 id175 id8956  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://brandcare.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://brandcare.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/346000-d23d1fabbeb5f4a832252774ebfa6ce5-thumb_jpg.jpg?buster=1392837933" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Brand Care -  pharmaceuticals health care information technology physicians" />
<h1 property="name">
Brand Care
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Electronic Invoice, Drug Recipes and follow up for healthcare professionals<br></strong>
Create your recipes, invoices and follow up your patients in one click.
The idea is to solve the problem for many doctors in mexico to invoice electronically, or digitalise our recipes for their patients,</p>
</a>
</div>

<div data-id="345158" data-name="Prestinver" data-href="http://contacto@prestinver.com" class="card startup   id17 id134 id2795 id129903  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://contacto@prestinver.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://contacto@prestinver.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/345158-b9cf8d25768e8d59a69936ece88b97d6-thumb_jpg.jpg?buster=1392747868" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Prestinver -  financial services peer-to-peer consumer lending invest online" />
<h1 property="name">
Prestinver
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> P2P Lending in Mexico<br></strong>
Prestinver is the online platform so investors can meet people who wants to borrow money. Like this, loaners get great returns for their investments and borrowers pay lower rates.</p>
</a>
</div>

<div data-id="344747" data-name="CAYE " data-href="http://www.caye.mx" class="card startup   id34 id67 id1429 id9980  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.caye.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.caye.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/344747-5a9ff1167b1b7667372ab53153693a01-thumb_jpg.jpg?buster=1392696786" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CAYE  -  advertising search digital entertainment local search" />
<h1 property="name">
CAYE
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Culture, art and entertainment in your own city<br></strong>
CAYE is a web based platform that helps businesses in the cultural, art and entertainment industry to promote their events and promotions in a quickly and easy way.
Correspondingly, CAYE showcases a wide variety of local events and exciting city sightseeings for ...</p>
</a>
</div>

<div data-id="344647" data-name="Lavadero" data-href="http://www.lavadero.co/" class="card startup   id3  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.lavadero.co/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.lavadero.co/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/344647-b7f15d83edd3ba163d72129fa9fc4e11-thumb_jpg.jpg?buster=1392683069" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Lavadero -  mobile" />
<h1 property="name">
Lavadero
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Laundry Delivery in 24 hours.<br></strong>
Lavadero is an on-demand laundry and drycleaning service in Mexico City, with 24 hour delivery.
Through our web and mobile app we take care of our your dirty laundry, picking up and delivering it at your home/office at a time that suits you.
Our network of ...</p>
</a>
</div>

<div data-id="344329" data-name="Loyalty Refunds" data-href="http://www.loyaltyrefunds.com" class="card startup   id29 id42 id263 id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.loyaltyrefunds.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://es-es.facebook.com/LoyaltyRefunds" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/LoyaltyRefunds" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.loyaltyrefunds.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/344329-d08c938460c3bfdd372b987a9dbc300e-thumb_jpg.jpg?buster=1392655224" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Loyalty Refunds -  e-commerce B2B restaurants small and medium businesses" />
<h1 property="name">
Loyalty Refunds
</h1>
 <span href="https://twitter.com/LoyaltyRefunds" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Cloud Intelligence to make loyal customers - Payback<br></strong>
Loyalty Refunds is a cloud based business intelligence marketing software that works with a universal rewards points program. It’s accepted in any physical or virtual establishment, where each one gives and receives their own points.
It allows companies to increase ...</p>
</a>
</div>

<div data-id="343255" data-name="Qompit" data-href="http://qompit.com" class="card startup   id29 id51 id1259 id2969  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://qompit.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/qompit" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://qompit.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/343255-4be08537e30807a58e8ee26a5a17da8e-thumb_jpg.jpg?buster=1392426222" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Qompit -  e-commerce sales and marketing content discovery estimation and quoting" />
<h1 property="name">
Qompit
</h1>
<span href="http://twitter.com/qompit" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Reverse shopping platform<br></strong>
Qompit is a reverse auction platform developed with the idea of contacting customers and suppliers throughout Mexico, so they can locate, trading, sell or buy products through internet quickly and effectively.
Our platform automatically determines when providers ...</p>
</a>
</div>

<div data-id="342933" data-name="Marketing Innovations Technology" data-href="http://www.mitsolutions.com.mx" class="card startup   id29 id94 id224  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.mitsolutions.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.mitsolutions.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/342933-ff794f60f06c33b2644b216b25326cb8-thumb_jpg.jpg?buster=1392391334" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Marketing Innovations Technology -  e-commerce mobile commerce loyalty programs" />
<h1 property="name">
Marketing Innovations Technology
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Customer &amp; Experience Social Marketing Platform<br></strong>
La plataforma &quot;Customer &amp; Experience Social Marketing Platform&quot; es un concepto único en el mercado actualmente, esta junta los servicios tecnologicos y aplicabilidad del marketing 2.0 a través de los distintos medios electrónicos como son desde las redes sociales, ...</p>
</a>
</div>

<div data-id="341487" data-name="Chunches" data-href="http://www.chunches.com" class="card startup   id29 id93 id553 id15202  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.chunches.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.chunches.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/341487-2ae3e62b82b690261f943230da0f99e9-thumb_jpg.jpg?buster=1392214508" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Chunches -  e-commerce fashion women-focused jewelry" />
<h1 property="name">
Chunches
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Trendy fashion jewelry e-commerce &amp; marketplace in amazing prices<br></strong>
Chunches is an online store and an e-commerce for girls that want to buy trendy fashion jewelry in low prices.
Chunches will be the biggest online store of fashion jewelry.
We offer our clients a renewed collection of fashion accessories, inspired by the latest ...</p>
</a>
</div>

<div data-id="341257" data-name="Azul E-Commerce" data-href="http://azulecommerce.com" class="card startup   id340 id1474  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://azulecommerce.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://azulecommerce.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
 <img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/341257-b484deeb8756e1c85ec09712c247b095-thumb_jpg.jpg?buster=1392179745" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Azul E-Commerce -  small and medium businesses entrepreneur" />
<h1 property="name">
Azul E-Commerce
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> E-Commerce for every small-medium business<br></strong>
Our agency helps small non-tech business with everything they need to sell there products online. Super easy, super fast.</p>
</a>
</div>

<div data-id="340265" data-name="No Distroti" data-href="http://nodistroti.com" class="card startup   id6 id47 id73 id640  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://nodistroti.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/nodistroti" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://nodistroti.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/340265-8126596b8449dcaf025a07bedc64607b-thumb_jpg.jpg?buster=1392074373" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="No Distroti -  social media music sports journalism" />
<h1 property="name">
No Distroti
</h1>
<span href="http://twitter.com/nodistroti" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Original social content platform (No Distroti)<br></strong>
No Distroti is an original content website with a social platform. It uses reputation as an incentive for behavior and quality. Also, integrates gamification techniques to give points and unlocks badges to users when they share, comment, read, like, vote and publish ...</p>
</a>
</div>

<div data-id="340079" data-name="ProFacture MX" data-href="http://profacture.mx" class="card startup   id3 id332 id408 id416  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://profacture.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://profacture.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/340079-569c0811c8d203ff6d4714e71f4ae8a4-thumb_jpg.jpg?buster=1392063086" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ProFacture MX -  mobile web design ios android" />
<h1 property="name">
ProFacture MX
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Facturador Electrónico<br></strong>
ProFacture pretende ser una plataforma para llevar de una forma rápida, sencilla y segura la facturación y contabilidad básica desde un 'freelance', pequeño , mediano o grande empresa/negocio. Con un diseño intuitivo y 'responsive' lo único que se necesita es una ...</p>
</a>
</div>

<div data-id="340012" data-name="Crol.io" data-href="http://crol.io" class="card startup   id10 id198 id249 id286  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://crol.io" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://crol.io" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/340012-5b3f5ec7b6449abee511f9777d5e8f55-thumb_jpg.jpg?buster=1392056815" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Crol.io -  SaaS big data services consumer electronics" />
<h1 property="name">
Crol.io
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Electronics blue book<br></strong>
Crolio is a tool to find the average market price of pre-owned items. Prices are updated everyday, you can search up to 600k items. Just type the name, model or brand and Crolio will look it up for you.</p>
</a>
</div>

<div data-id="339907" data-name="i4 Capital Partners" data-href="http://www.i4capitalpartners.com/" class="card startup   id29 id448  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.i4capitalpartners.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.i4capitalpartners.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://angel.co/images/shared/nopic_startup.png" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="i4 Capital Partners -  e-commerce startups" />
<h1 property="name">
i4 Capital Partners
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Our ability to identify undervalued, over-structured, overvalued or overrated Companies<br></strong>
We are a private investment company specialized in structuring, conducting and disposing Internet companies throughout Latin America.
With a proven management team that has deep knowledge in M&amp;A transactions in the region; our philosophy strives to create value ...</p>
</a>
</div>

<div data-id="339678" data-name="BuscaSocios.Mx" data-href="http://www.buscasocios.mx/" class="card startup   id253 id1113 id3010  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.buscasocios.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.buscasocios.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/339678-a7b8fec415868101327db8c74bcdba52-thumb_jpg.jpg?buster=1392014529" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="BuscaSocios.Mx -  consulting business development crowdfunding" />
<h1 property="name">
BuscaSocios.Mx
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Crowdfunding platform to find business partners <br></strong>
BuscaSocios.Mx is the first mexican crowdfunding platform that allows entrepreneurs to find partners and not just money to fund their business. Through our website people are able to publish their business project to look out for donors, funders, business partners ...</p>
</a>
</div>

<div data-id="338809" data-name="answeryon" data-href="http://answeryon.com" class="card startup   id3 id201 id1153 id2812  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://answeryon.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://answeryon.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/338809-4a3466830e9bd0d55d5db2b48856f366-thumb_jpg.jpg?buster=1391821048" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="answeryon -  mobile news young adults entertainment industry" />
<h1 property="name">
answeryon
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> answeryon is a social network of questions and answers<br></strong>
answeryon is a social network where questions whatever and let your friends help you decide.</p>
</a>
</div>

<div data-id="337250" data-name="CuentasMx" data-href="http://www.smekefarca.com/cuentasMx" class="card startup   id10 id29 id94 id408  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.smekefarca.com/cuentasMx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@jose_smeke" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.smekefarca.com/cuentasMx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/337250-e755a20b6a69d698dddb23e75a7421f0-thumb_jpg.jpg?buster=1391634399" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CuentasMx -  SaaS e-commerce mobile commerce ios" />
<h1 property="name">
CuentasMx
</h1>
<span href="http://twitter.com/@jose_smeke" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> finanzas personales en dispositivos moviles<br></strong>
aplicacion gratuita para ios y andriod para la consulta y pago de servicios altenativos de servicios.
CuentasMx fue creada para concentrar los proveedores de servicios publicos como agua, luz telefono entre otros para facilitar al usuario la rececpcion de los recordatorios ...</p>
</a>
</div>

<div data-id="335743" data-name="Mejores Mudanzas" data-href="https://mejoresmudanzas.com" class="card startup   id384 id631  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://mejoresmudanzas.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/mejormudanza" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="https://mejoresmudanzas.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/335743-d4327e0e3ef849bce037268832c6336e-thumb_jpg.jpg?buster=1404353715" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Mejores Mudanzas -  professional services transportation" />
<h1 property="name">
Mejores Mudanzas
</h1>
<span href="http://twitter.com/mejormudanza" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Un gran cambio merece una mudanza inteligente<br></strong>
Ayudamos a conseguir las 3 mejores propuestas de empresas de mudanzas seguras y confiables, que pertenecen a nuestra red de proveedores confiables.
Mediante algoritmos de asignación calculamos la carga y los camiones o camionetas necesarias.
Quitamos del camino ...</p>
</a>
</div>

<div data-id="332974" data-name="Manqo Studio" data-href="http://www.manqostudio.com" class="card startup   id41 id408 id416 id490  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.manqostudio.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.manqostudio.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/332974-66e6b207bdde8f3bf32ffff9f721bf41-thumb_jpg.jpg?buster=1391036742" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Manqo Studio -  social games ios android mobile games" />
<h1 property="name">
Manqo Studio
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile Social Games<br></strong>
Mobile social games for Android and iOS devices. We have made a game for android https://play.google.com/store/apps/details?id=heady.kidz and we want to create more quality content for it, recreate it for other platforms (iOS, Windows Phone), and create more games.</p>
</a>
</div>

<div data-id="332810" data-name="Stopnic" data-href="" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">


</div>
</div>
<a class="main_link" href="" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/332810-cbe2c23027609d5e290d35bf8168db26-thumb_jpg.jpg?buster=1391027165" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Stopnic - " />
<h1 property="name">
Stopnic
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Playing Quit Smoking <br></strong>
</p>
</a>
</div>

<div data-id="332653" data-name="Gamification TV" data-href="http://www.gamification.com.mx" class="card startup   id4 id697 id1295 id8962  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.gamification.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.gamification.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/332653-950ee10b523372ded7d937b4491ba764-thumb_jpg.jpg?buster=1391015139" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Gamification TV -  digital media social television gamification content delivery" />
<h1 property="name">
Gamification TV
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Transmedia and Gamification TV<br></strong>
Gamification TV is a platform that helps TV Networks and video content creators to deliver interactive stories to their audiences. Empowering the content to give meaningful multi-platform experiences.</p>
</a>
</div>

<div data-id="331875" data-name="ABACIX, S.C." data-href="http://www.algebraix.com" class="card startup   id43 id198 id409 id841  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.algebraix.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/Algebraix" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.algebraix.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/331875-60e262d7de55876d012403dfc053ce20-thumb_jpg.jpg?buster=1391622215" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ABACIX, S.C. -  education big data development platforms software" />
<h1 property="name">
ABACIX, S.C.
</h1>
<span href="https://twitter.com/Algebraix" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Plataforma en línea de administración y control escolar.<br></strong>
Algebraix.
Plataforma en línea de administración y control escolar.
Una experiencia educativa excepcional
Algebraix facilita el trabajo administrativo y mejora todos los aspectos educativos en instituciones de enseñanza básica, media y superior, ayudándolas ...</p>
</a>
</div>

<div data-id="331731" data-name="Shopaholico.com" data-href="http://www.shopaholico.com" class="card startup   id29 id32 id1122 id1893  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.shopaholico.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.shopaholico.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/331731-14e612a6acf6bd82faf15d65b500ab5e-thumb_jpg.jpg?buster=1390930246" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Shopaholico.com -  e-commerce retail wholesale distribution" />
<h1 property="name">
Shopaholico.com
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Retail and wholesale E-commerce webiste<br></strong>
</p>
</a>
</div>

<div data-id="331051" data-name="exponess corp" data-href="http://exponess.com" class="card startup   id106 id896 id9407  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://exponess.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://exponess.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/331051-278f51d1efe2070328bac251b784b0be-thumb_jpg.jpg?buster=1390852795" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="exponess corp -  virtual worlds world domination general public worldwide" />
<h1 property="name">
exponess corp
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> business social network<br></strong>
Exponess.com is a social network for business events that's reinventing the tradeshow world.
By hosting trade shows online and building a social network around it, you obliterate time and space limitations, and extend the life and reach of the trade shows as long ...</p>
</a>
</div>

<div data-id="330097" data-name="SODA ENTERPRISE MÉXICO SAPI DE CV" data-href="" class="card startup   id3 id4 id29 id2498  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://twitter.com/@Jorgejery" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/330097-f67de6027230eb9ab7fbfd53c6ca637b-thumb_jpg.jpg?buster=1390800464" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SODA ENTERPRISE MÉXICO SAPI DE CV -  mobile digital media e-commerce innovation engineering" />
<h1 property="name">
SODA ENTERPRISE MÉXICO SAPI DE CV
</h1>
<span href="http://twitter.com/@Jorgejery" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
APP that integrates:
GLOBAL SOCIAL NETWORK+SPECIFIC COMMUNITIES+E-COMMERCE+ON LINE MARKETING+EVENT MARKETING+EXCLUSIVE ENTERTAINMENT+EDUCATION+
TRAVEL... and more, in only one.
We create an internet platform and app, in which each user registers, and make a profile, ...</p>
</a>
</div>

<div data-id="328154" data-name="Scorebox" data-href="http://www.scorebox.co" class="card startup   id10 id73 id1433  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.scorebox.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.scorebox.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/328154-25386de54d75fd7982b24797ce5e4743-thumb_jpg.jpg?buster=1390415566" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Scorebox -  SaaS sports soccer" />
<h1 property="name">
Scorebox
 </h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Sports tournaments made easy<br></strong>
Scorebox eases the management of sports tournaments by improving the connectivity between the administrator and players through a dynamic platform. No more Excel spreedsheets to control the tournament, you can create a call for your tournament, do the fixes in ...</p>
</a>
</div>

<div data-id="327599" data-name="Konfio" data-href="https://konfio.mx" class="card startup   id134 id374 id2947 id156534  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://konfio.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/konfio" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/konfiomx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="https://konfio.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/327599-869a1d5b78cd399d1c1a771c14c25576-thumb_jpg.jpg?buster=1390346049" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Konfio -  peer-to-peer personal finance finance technology loans" />
<h1 property="name">
Konfio
</h1>
<span href="https://twitter.com/konfiomx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online micro-business loans<br></strong>
Konfío is an online lending platform that helps micro-businesses in Latin America who don’t have access to credit obtain affordable loans, thanks to a propriety algorithm that uses technology to measure creditworthiness.
Our mission is to boost the regional economy ...</p>
</a>
</div>

<div data-id="322715" data-name="Elysa" data-href="http://elysa.co" class="card startup   id13 id175 id1056 id1103  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://elysa.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/elysasalud" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://elysa.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/322715-686f236555966234db757ff741c36de2-thumb_jpg.jpg?buster=1389657762" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Elysa -  health care health care information technology technology health and wellness" />
<h1 property="name">
Elysa
</h1>
<span href="http://twitter.com/elysasalud" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Unlimited subscription to premium health care services. <br></strong>
Elysa is a healthcare solution for unlimited medical attention at home, it works through a subscription that grants you access to unlimited primary health services such as integral check ups, medical consultation and nutritional couching.</p>
</a>
</div>

<div data-id="322295" data-name="Welcome Pal" data-href="http://welcomepal.weebly.com/" class="card startup   id3 id9100  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://welcomepal.weebly.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://welcomepal.weebly.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/322295-c122a5a33b965b5923882235f1f771c3-thumb_jpg.jpg?buster=1390848001" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Welcome Pal -  mobile travel &amp; tourism" />
<h1 property="name">
Welcome Pal
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Express welcoming service for international passengers<br></strong>
Welcome Pal is a platform that join visitors who arrive to international airports with local partners that allow to pick them up when they arrive to the airport. The international visitor can select a host in the app platform and make a book in order to be sure ...</p>
</a>
</div>

<div data-id="316825" data-name="YADA" data-href="http://www.yada.mx/" class="card startup   id34 id52 id57 id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.yada.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/YadaMx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.yada.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/316825-1178a348ab23b8371eb281d0b0caed39-thumb_jpg.jpg?buster=1421900573" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="YADA -  advertising analytics location based services small and medium businesses" />
<h1 property="name">
YADA
</h1>
<span href="http://twitter.com/YadaMx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Your Source for Real-Time Location-Based Offers<br></strong>
YADA is a discount, specials and reward perks location-based discovery app that pushes offers and discounts based on the customer's location and their profile. Our service provides trusted information about perks or specials given by commerces near you. Our customers ...</p>
</a>
</div>

<div data-id="316011" data-name="Eventik " data-href="http://eventikapp.com/" class="card startup   id47 id94 id604 id16089  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://eventikapp.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://eventikapp.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/316011-7a7363ca41b875a0a1f727369dd88f1f-thumb_jpg.jpg?buster=1388537323" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Eventik  -  music mobile commerce ticketing event management" />
<h1 property="name">
Eventik
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Eventik is the mobile application for event booking<br></strong>
The Mobile App for last minute tickets for live events!!!
Users find a curated list of events, select, pay and go to the event on the same evening
Go to the best events with one click!
Users can invite more friends on the go, get a ride to the event and receive ...</p>
</a>
</div>

<div data-id="314216" data-name="Onisom" data-href="http://mymovieslib.com/Movies" class="card startup   id4 id6 id67 id202  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://mymovieslib.com/Movies" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://mymovieslib.com/Movies" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/314216-38e9883c8007bd53834e96241937cf37-thumb_jpg.jpg?buster=1388099472" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Onisom -  digital media social media search internet tv" />
<h1 property="name">
Onisom
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> User-friendly Media Search<br></strong>
The solution allows the users to search media such as Movies by combining different smart filters. Currently there is no solution on the market that allows the users to combine complex filters to search for media.
If the user types in the link http://mymovieslib.com/Movies ...</p>
</a>
</div>

<div data-id="310935" data-name="CUIDÁNDOTE" data-href="http://www.amevesc.org.mx" class="card startup   id43 id418 id1221 id10974  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.amevesc.org.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/#ameve" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.amevesc.org.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/310935-325b155f210452d449043960116ca982-thumb_jpg.jpg?buster=1387301546" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CUIDÁNDOTE -  education personal health psychology nutrition" />
<h1 property="name">
CUIDÁNDOTE
</h1>
<span href="http://twitter.com/#ameve" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Cuidamos al que cuida<br></strong>
Por cada persona que padece algún tipo de discapacidad y/o enfermedad que requiere cuidado temporal o permanente, existe una persona cuidadora cuyas necesidades son totalmente invisibles a los ojos de la familia, la sociedad, los servicios de salud y la economía. ...</p>
</a>
</div>

<div data-id="310130" data-name="Readdict" data-href="http://www.readdict.net" class="card startup   id269 id498 id1556 id11537  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.readdict.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.readdict.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/310130-c0d537f5e1ac68c2060383a293640226-thumb_jpg.jpg?buster=1394053919" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Readdict -  social media platforms e books social news reading apps" />
<h1 property="name">
Readdict
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> streaming platform of books, magazines and newspapers (Spotify)<br></strong>
Readdict is a streaming platform of books, magazines and newspapers that helps customers to have all of their readings in one place, on any device and connect and discuss readings with friends. Our product gives you a wide catalog for free, and a unlimited premium ...</p>
</a>
</div>

<div data-id="308204" data-name="Alggo" data-href="http://allgo.mx" class="card startup   id87 id94 id2812 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://allgo.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/allgomx" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@allgomx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://allgo.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/308204-d593ef8fc1003822037c9722523f2e54-thumb_jpg.jpg?buster=1412791610" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Alggo -  publishing mobile commerce entertainment industry big data analytics" />
<h1 property="name">
Alggo
</h1>
<span href="http://twitter.com/@allgomx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
Arriving to Mexico City, inviting your girlfriend, partying with your friends… where &amp; when is always a problem.
allgo is the Mexico City´s top intelligent event agenda. Invite, share, buy, and explore events in Mexico City. allgo is the a platform where all ...</p>
</a>
</div>

<div data-id="308193" data-name="Cardinal Eyewear" data-href="http://www.cardinaleyewear.com" class="card startup   id93 id81663  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.cardinaleyewear.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/cardinaleyewear" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@cardinaleyewear" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.cardinaleyewear.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/308193-fb666f0f07d8fbd6a15796cafa21cfaa-thumb_jpg.jpg?buster=1412791678" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cardinal Eyewear -  fashion handmade" />
<h1 property="name">
Cardinal Eyewear
</h1>
<span href="http://twitter.com/@cardinaleyewear" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Sustainably handcrafted eyewear <br></strong>
What materials do Custom Projects use?
We use sustainable wood for all the frames. We use mostly teak and walnut. Proteak Re- newable Forestry (www.proteak.com) is our wood provider and our partner, they are certified by the FSC (Forest Stewardship Council) as ...</p>
</a>
</div>

<div data-id="304974" data-name="Radar ciudadano" data-href="http://www.desarrollo-municipal.com" class="card startup   id251  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.desarrollo-municipal.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.desarrollo-municipal.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/304974-27315904653e6c2e2945d73949591c55-thumb_jpg.jpg?buster=1386275989" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Radar ciudadano -  governments" />
<h1 property="name">
Radar ciudadano
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> App movil para denuncia ciudadana<br></strong>
It is a mobile app to report water leaks, extortion, emergency services, and municipal utilities potholes georeferenced manner.
It is a social network of citizen complaint with the possibility of being served by the municipal government to access the database ...</p>
</a>
</div>

<div data-id="304969" data-name="Iniciativa Resustenta AC" data-href="http://www.resustenta.com.mx" class="card startup   id269 id450 id556 id632  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.resustenta.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.resustenta.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/304969-630cfcdf19ccc3dc45a29d745421acf4-thumb_jpg.jpg?buster=1386275873" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Iniciativa Resustenta AC -  social media platforms social media marketing events email" />
<h1 property="name">
Iniciativa Resustenta AC
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Consultoría en Responsabilidad Social Empresarial<br></strong>
Capacitación y consultoría para organizaciones de todo tipo en servicios relacionados con la responsabilidad social empresarial con sustento en la guía ISO 26000</p>
</a>
</div>

<div data-id="302141" data-name="Verbal" data-href="http://helloverbal.com" class="card startup   id3 id49 id65 id103  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://helloverbal.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/helloverbal" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://helloverbal.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/302141-a4bce574ced086ed7d1b008fbaf61e7a-thumb_jpg.jpg?buster=1385748608" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Verbal -  mobile telecommunications information services telephony" />
 <h1 property="name">
Verbal
</h1>
<span href="http://twitter.com/helloverbal" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Verbal is the Siri for any kind of phone. <br></strong>
The digital divide remains a huge global issue: only 38% of the world population use the Internet (source: International Telecommunications Union). This is due to literacy, digital literacy, access to devices, and access to internet connection. Current efforts ...</p>
</a>
</div>

<div data-id="300419" data-name="widgetSolutions" data-href="http://www.naranya.com" class="card startup   id3  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.naranya.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.naranya.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/300419-7a7b7df44b7b4824a66bbb4dcc82e880-thumb_jpg.jpg?buster=1385441330" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="widgetSolutions -  mobile" />
<h1 property="name">
widgetSolutions
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> A smart algorithm that matches business models<br></strong>
It helps finding different business models for features,</p>
</a>
</div>

<div data-id="299494" data-name="Epic Queen" data-href="http://www.epicqueen.com" class="card startup   id4 id448 id553 id1056  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.epicqueen.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/epic_queen" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.epicqueen.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/299494-3022ae4624ae7997e8c80eeae1f85d5e-thumb_jpg.jpg?buster=1385339313" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Epic Queen -  digital media startups women-focused technology" />
<h1 property="name">
Epic Queen
 </h1>
<span href="https://twitter.com/epic_queen" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We create initiatives and leads for digital women <br></strong>
Epic Queen is web for content generation. Where we talk about technology, entrepreneurship, startups, business, digital marketing, social media, code but from the point of view of women.</p>
</a>
</div>

<div data-id="299271" data-name="Factigo" data-href="http://www.factigo.mx" class="card startup   id585 id151742 id156282  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.factigo.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/factigo" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/factigomx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.factigo.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/299271-a1b82351e4f8a1a262af8a248c9dcdae-thumb_jpg.jpg?buster=1414101292" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Factigo -  accounting small companies Working professionals" />
<h1 property="name">
Factigo
</h1>
<span href="https://twitter.com/factigomx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mis cuentas SAT<br></strong>
Organizes, controls and valid electronic invoices without complicated tasks. Simple as emailed. Get Tax Assistance.
It is the duty of each and every electronic receivers to validate XML files of these bills.
Our Product has the next benefits:
1. Pioneering tool ...</p>
</a>
</div>

<div data-id="295827" data-name="CIAM" data-href="http://www.e-ciam.com.mx" class="card startup   id2525  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.e-ciam.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.e-ciam.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/295827-76cf678890c37b796d731e1e46cb6766-thumb_jpg.jpg?buster=1384711409" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CIAM -  environmental innovation" />
<h1 property="name">
CIAM
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Biotecnología Ambiental<br></strong>
Somos un start up que tiene como objetivo crear tecnología verde para la restauración de zonas impactadas así como ofrecer servicios de consultoría ambiental.</p>
</a>
</div>

<div data-id="294091" data-name="Rocket.la" data-href="http://rocket.la" class="card startup   id17 id198 id374 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://rocket.la" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/RocketColombia" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://rocket.la" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/294091-6a5d0072f23f2877a01efc3c9ab3b0bb-thumb_jpg.jpg?buster=1384358826" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Rocket.la -  financial services big data personal finance finance technology" />
<h1 property="name">
Rocket.la
</h1>
<span href="https://twitter.com/RocketColombia" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Connecting the right clients with the correct Banks.<br></strong>
Rocket is a free and anonymous website with a completed set of tools that allows people to manage their money in an easy and efficient way.
With Rocket, the users are in control of their personal finances in order to achieve their financial goals, and find the ...</p>
</a>
</div>

<div data-id="292779" data-name="Makersforge" data-href="http://makersforge.com" class="card startup   id162 id246 id671  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://makersforge.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://makersforge.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
 <img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/292779-516ba6b1fd66ebfcdaffc724eeecc592-thumb_jpg.jpg?buster=1384185014" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Makersforge -  marketplaces crowdsourcing mass customization" />
<h1 property="name">
Makersforge
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Buy and sell customized manufactured products, made from FREE Open Designs.<br></strong>
Makersforge is an ecommerce marketplace to buy and sell customized products, also is an online Open Design repository, that hosts design drawings of the products that are commercialized in the platform.
We want to empower the people to fabricate whatever they ...</p>
</a>
</div>

<div data-id="291404" data-name="Envolta" data-href="http://envolta.com" class="card startup   id84 id2644 id2796  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://envolta.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://envolta.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/291404-88a7a3ff0729a5ee65fafbbbd9963866-thumb_jpg.jpg?buster=1383857558" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Envolta -  solar residential solar commercial solar" />
<h1 property="name">
Envolta
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Solar Leasing in Mexico<br></strong>
What is Envolta’s Solar Service?
Pay less for green solar power than you are currently paying for dirty power. You can start saving money immediately on your electricity costs and you don’t have to pay the high upfront cost to install solar equipment on your ...</p>
</a>
</div>

<div data-id="290198" data-name="Clinicas Cuidate" data-href="http://www.clinicascuidate.mx" class="card startup   id13 id2931  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.clinicascuidate.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.clinicascuidate.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/290198-409d9bbd7c10d9e6ecd562d910149fdd-thumb_jpg.jpg?buster=1383679569" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Clinicas Cuidate -  health care diabetes" />
<h1 property="name">
Clinicas Cuidate
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Specialized diabetes clinics<br></strong>
Cuidate is a service company that provides guidance, personal attention and monitoring to the increasing number of Mexicans with diabetes. With a highly innovative model that involves specialized clinics, diagnostic and monitoring technology and a long distance ...</p>
</a>
</div>

<div data-id="290089" data-name="Buen Socio" data-href="http://buensocio.com" class="card startup   id17 id321 id334 id577  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://buensocio.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@buensocio" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://buensocio.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/290089-dbc8ff4bf0b632d01ac0f29533a231fa-thumb_jpg.jpg?buster=1383669792" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Buen Socio -  financial services finance green credit" />
<h1 property="name">
Buen Socio
</h1>
<span href="http://twitter.com/@buensocio" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Your investment can make a difference. <br></strong>
Buen Socio connects ethical investors with entrepreneurs that generate social and environmental benefits.</p>
</a>
</div>

<div data-id="288668" data-name="Linio" data-href="http://www.linio.com" class="card startup   id29  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.linio.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.linio.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/288668-8ceac45255eebf29547c2fb338ed28ee-thumb_jpg.jpg?buster=1383325719" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Linio -  e-commerce" />
<h1 property="name">
Linio
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Largest eCommerce in Latin America<br></strong>
Linio, the so-called ‘Amazon of Latin America’, is a Rocket Internet-incubated e-commerce company. Linio offers a wide variety of products online in various categories, such as home electronics, technology, entertainment, home, appliances, fashion, babies and toys, ...</p>
</a>
</div>

<div data-id="284642" data-name="casachilanga.com" data-href="http://www.casachilanga.com" class="card startup   id16 id162 id374  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.casachilanga.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@casachilangacom" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.casachilanga.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/284642-b4ececed33f807f4cc2500b388163e4f-thumb_jpg.jpg?buster=1382545388" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="casachilanga.com -  real estate marketplaces personal finance" />
<h1 property="name">
casachilanga.com
</h1>
<span href="http://twitter.com/@casachilangacom" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Redfin for Mexico City<br></strong>
Largest brand new housing for sale marketplace in Mexico City.
Get professional and independent advise on which is your best option and buy your brand new home with us.
Receive 1 year free insurance on us, and buy related products and services such as mortgages, ...</p>
</a>
</div>

<div data-id="284634" data-name="Zave App" data-href="http://www.zaveapp.com" class="card startup   id17 id41 id374 id473  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.zaveapp.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/zaveapp" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@zaveapp" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.zaveapp.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/284634-22a036bb93df8d12c2585514559be2de-thumb_jpg.jpg?buster=1382546785" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Zave App -  financial services social games personal finance mobile payments" />
<h1 property="name">
Zave App
</h1>
<span href="http://twitter.com/@zaveapp" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Save as you go, your electronic piggy bank<br></strong>
When payments were made in cash, saving was easier (keep the change and save it in a piggy box), nowadays this is physically impossible and spending money is easier as you don't feel the physical detachment as hard as when paying cash.
With Zave App any user of ...</p>
</a>
</div>

<div data-id="283812" data-name=" Latin Singer Music Project" data-href="http://www.maritamusic.com" class="card startup   id47  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.maritamusic.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.maritamusic.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/283812-d80f8fd50313c538f8fb5e1eb2e4f5f8-thumb_jpg.jpg?buster=1382397587" src="/img/spacer.gif" class="lazy" border="0" property="image" alt=" Latin Singer Music Project -  music" />
<h1 property="name">
Latin Singer Music Project
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Colombian Latin singer of electronic pop music, with finished record.<br></strong>
Colombian Latin singer of electronic pop music, very good looking, and finished record, to develop in Mexico.
commercial music project aimed at radio, sales on iTunes and record stores and massive shows.
capital will be invested in radio stations, radio, press ...</p>
</a>
</div>

<div data-id="283263" data-name="Klustera" data-href="http://www.klustera.com" class="card startup   id34 id379 id685 id1470  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.klustera.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.klustera.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/283263-97fbc5da5f5536fe04b6606e32eb8494-thumb_jpg.jpg?buster=1389201582" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Klustera -  advertising retail technology data visualization predictive analytics" />
<h1 property="name">
Klustera
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Better PPC with Data Science<br></strong>
Klustera is a Search Engine Marketing optimization platform. Nowadays it functions as a data visualization and modelling tool that empowers human analysis, but these features are actually the core of a more sophisticated product that aims to automate SEM management ...</p>
</a>
</div>

<div data-id="279895" data-name="CREARTON" data-href="http://www.crearton.com.mx" class="card startup   id31  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.crearton.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@crearton.com.mx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.crearton.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/279895-83d51f3f14deff81125556c0f2901a37-thumb_jpg.jpg?buster=1381775539" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CREARTON -  consumer goods" />
<h1 property="name">
CREARTON
</h1>
<span href="http://twitter.com/@crearton.com.mx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Productos Sustentables de Cartón y Papel<br></strong>
Productos con menor impacto ambiental, en congruencia con el nuevo estilo de vida de las empresas verdes</p>
</a>
</div>

<div data-id="269826" data-name="BigFatFunMachine" data-href="http://www.bigfatfumachine.com." class="card startup   id3 id409 id1414 id11826  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bigfatfumachine.com." rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bigfatfumachine.com." target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/269826-daa1a84966580d863b37bc0dab96a77d-thumb_jpg.jpg?buster=1380070157" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="BigFatFunMachine -  mobile development platforms creative user experience design" />
<h1 property="name">
BigFatFunMachine
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Creative &amp; customized development.<br></strong>
We are building a new machine.
At the moment its main function has been the development of tailored, creative, innovative, websites &amp; platforms, based on our costumers needs &amp; requirements.
Our goal is to nurture and help the growth of the fun machine until ...</p>
</a>
</div>

<div data-id="268813" data-name="Montéz" data-href="https://www.kichink.com/stores/id/2817#.UkDuySi6CXI" class="card startup   id32  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://www.kichink.com/stores/id/2817#.UkDuySi6CXI" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://www.kichink.com/stores/id/2817#.UkDuySi6CXI" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268813-fb2a28c262394f773b1920c9f7b4d0fc-thumb_jpg.jpg?buster=1379989564" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Montéz -  retail" />
<h1 property="name">
Montéz
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Handmade mexican jewelry <br></strong>
Montez is a Mexican brand that aims to always try to please the customer giving you the opportunity to design and create your own accessories to be able to choose the color and pattern of your choice making Montéz a unique brand.</p>
</a>
</div>

<div data-id="268440" data-name="llantiprecio.com" data-href="http://www.llantiprecio.com" class="card startup   id29 id32 id69 id9785  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.llantiprecio.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.llantiprecio.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268440-341b8a1e06c63c757363312c3c5e5531-thumb_jpg.jpg?buster=1379958076" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="llantiprecio.com -  e-commerce retail automotive direct sales" />
<h1 property="name">
llantiprecio.com
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Simplifying the purchase of car tires online for everybody, hassle free. <br></strong>
Llantiprecio.com is the first simplified online store for buying car tires in Mexico. Our customers include every car owner who is not an expert in tires and definitely is not interested in becoming one. Basically, our customers are everybody who drives a car. ...</p>
</a>
</div>

<div data-id="268139" data-name="Tu Conductor Designado" data-href="http://www.tuconductordesignado.wix.com/tuconductordesignado" class="card startup   id631 id1096 id1116  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.tuconductordesignado.wix.com/tuconductordesignado" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.tuconductordesignado.wix.com/tuconductordesignado" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268139-9abbbc52f51f85fb96f191fc0ebc29c5-thumb_jpg.jpg?buster=1379911852" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tu Conductor Designado -  transportation limousines public transportation" />
<h1 property="name">
Tu Conductor Designado
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Your personal designated driver<br></strong>
Tu Conductor Designado lets you rent a designated driver for those special occasions when you do not want to drive. We make available a range of drivers throughout Mexico City to take the costumer wherever they want in the comfort and safety of their car with ...</p>
</a>
</div>

<div data-id="268124" data-name="Mobilecode" data-href="http://http:www.mcode.com.mx" class="card startup   id29 id51 id94  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://http:www.mcode.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://http:www.mcode.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268124-b4cdf8feffbe622ed57daed11b2ac04a-thumb_jpg.jpg?buster=1379909006" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Mobilecode -  e-commerce sales and marketing mobile commerce" />
<h1 property="name">
Mobilecode
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Ecommerce platform and digital strategies <br></strong>
Plataforma de comercio electrónico que facilite a las pymes expandir su mercado a un mayor número de personas de manera simple y fácil impulsando el crecimiento de su negocio mediante la implementación de estrategias digitales</p>
</a>
</div>

<div data-id="268103" data-name="Vacaciones en Guerrero" data-href="http://www.vacacionesenguerrero.com/" class="card startup   id94 id441 id1458 id9100  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.vacacionesenguerrero.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.vacacionesenguerrero.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268103-8c6b6c03f1445a8058e380fe16ff5e54-thumb_jpg.jpg?buster=1379905081" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Vacaciones en Guerrero -  mobile commerce discounts leisure travel &amp; tourism" />
<h1 property="name">
Vacaciones en Guerrero
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Discover and Travel in Guerrero with us! e-Travel Agency for Great People<br></strong>
Con el fin de promocionar a nivel nacional no sólo los destinos del Triángulo del Sol, sino lugares con vocación turística como Olinalá, Iguala o Ixcateopan, nace “Vacaciones en Guerrero” la primera agencia mayorista del DF que oferta exclusivamente a Guerrero.</p>
</a>
</div>

<div data-id="268058" data-name="Balam" data-href="http://balam.co" class="card startup   id10 id57 id1470 id2800  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://balam.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://balam.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268058-63fa3776a82a53bfb3d8722fb644f3e6-thumb_jpg.jpg?buster=1391062855" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Balam -  SaaS location based services predictive analytics business analytics" />
<h1 property="name">
Balam
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Google Analytics for the real world<br></strong>
Balam does behavior analytics.
We put the power of Facebook/Amazon recommendations for your own business.
Using our API you can personalize your app for every user, you can show the best offer, promotion for each user. Forget about complex analytics dashboards, ...</p>
</a>
</div>

<div data-id="268045" data-name="Zenttra" data-href="http://www.zenttra.net" class="card startup   id29 id57 id205 id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.zenttra.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.zenttra.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268045-fb90806896b08c30e0af2d98111410e8-thumb_jpg.jpg?buster=1379895612" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Zenttra -  e-commerce location based services mobile advertising small and medium businesses" />
<h1 property="name">
Zenttra
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> App for offers and promotions<br></strong>
Zenttra is an application that, depending on your location, you can get special offers and promotions from restaurants, bars, shops and any kind of business that are near you.
The customer gets discounts and promotions, and the shops and businesses, more sales.
You ...</p>
</a>
</div>

<div data-id="268041" data-name="Retail IQ" data-href="http://www.retailiq.mx" class="card startup   id97 id139 id379 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.retailiq.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.retailiq.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268041-ebe72e1cdddf455eebce004408f2c49f-thumb_jpg.jpg?buster=1380655507" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Retail IQ -  business intelligence quantitative marketing retail technology big data analytics" />
<h1 property="name">
Retail IQ
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Real-world Analytics.<br></strong>
By connecting the physical and digital worlds, Retail IQ delivers actionable shopper insights helping brick and mortar retailers better understand and engage their customers. Our shopper measurement platform senses and unlocks massive amounts of data about activity ...</p>
</a>
</div>

<div data-id="268036" data-name="miggols" data-href="http://www.miggols.com" class="card startup   id45 id162 id340 id686  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.miggols.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.miggols.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268036-1bc0b30bcbabf7d8d83d5acb9640b896-thumb_jpg.jpg?buster=1379894221" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="miggols -  business services marketplaces small and medium businesses brand marketing" />
<h1 property="name">
miggols
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
 <strong> real world links by logo recognition<br></strong>
</p>
</a>
</div>

<div data-id="268008" data-name="SILATAM " data-href="http://www.silatam.com" class="card startup   id208 id1474 id3011 id15213  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.silatam.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.silatam.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/268008-2aacd2024d9e8efde0e42fdf819caa4d-thumb_jpg.jpg?buster=1379890682" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SILATAM  -  ventures for good entrepreneur impact investing social innovation" />
<h1 property="name">
SILATAM
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> EMPOWERING SOCIAL IMPACT ENTREPRENEURS IN MEXICO &amp; LATAM<br></strong>
There are many entrepreneurs with social impact who have big projects but unfortunately these are paired with poor commercialization offers. Why do social-impact entrepreneurs not invest in strong marketing strategies as other sectors do? From 2008 to 2011, the ...</p>
</a>
</div>

<div data-id="267990" data-name="blove" data-href="http://blove.com.mx" class="card startup   id13 id230  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://blove.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://blove.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267990-ad221e860d5537bdd0ac8855649d0e0e-thumb_jpg.jpg?buster=1379907715" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="blove -  health care social commerce" />
<h1 property="name">
blove
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> SALVA VIDAS EN TIEMPO REAL<br></strong>
Blove Red Social de Donación
Miles de esfuerzos dirigidos a solucionar un problema que lleva años existiendo en México y en mayor o menor medida en el resto del mundo. NO HAY SUFICIENTE SANGRE CUANDO SE NECESITA EN EMERGENCIAS.
Los esfuerzos antes mencionados no ...</p>
</a>
</div>

<div data-id="267956" data-name="PROUNIVE" data-href="http://www.prounive.com" class="card startup   id4 id17 id246 id3010  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.prounive.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@PROUNIVE" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.prounive.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267956-119b8516d3300909d6b96abf76e6173c-thumb_jpg.jpg?buster=1379883657" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="PROUNIVE -  digital media financial services crowdsourcing crowdfunding" />
<h1 property="name">
PROUNIVE
</h1>
<span href="http://twitter.com/@PROUNIVE" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> First Crowd-partnership platform <br></strong>
We are currently building the first community of &quot; Crowd-partnership &quot; (association based on masses), working through physical and online platforms. Our interaction focuses on two main groups
1Entrepreneur Partners:
Entrepreneurs, or potential entrepreneurs, whom ...</p>
</a>
</div>

<div data-id="267803" data-name="Engitel SA de CV" data-href="http://www.engitel.com.mx" class="card startup   id49 id175 id631 id773  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.engitel.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.engitel.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267803-87044b0a155dbe02b7d13aeb16abb371-thumb_jpg.jpg?buster=1379837255" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Engitel SA de CV -  telecommunications health care information technology transportation energy management" />
<h1 property="name">
Engitel SA de CV
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> IT for Telecom<br></strong>
 Professional services and development of software tools to deployment telecom projects.</p>
</a>
</div>

<div data-id="267789" data-name="EduProy" data-href="http://portal.eduproy.com" class="card startup   id1469 id9600 id10914  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://portal.eduproy.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/Eduproy" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://portal.eduproy.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267789-44d043e434516a0781be149a40c33a31-thumb_jpg.jpg?buster=1379830587" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="EduProy -  k 12 education high school students high schools" />
<h1 property="name">
EduProy
</h1>
<span href="https://twitter.com/Eduproy" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Better school communication<br></strong>
EduProy enhances a better interaction among the school community. Administrators, teachers, parents and students can have a better interaction and everybody has information on time.
Parents keep reports of homework, duties, grades and every other thing related ...</p>
</a>
</div>

<div data-id="267788" data-name="Point Analytics" data-href="http://www.bishop.mx" class="card startup   id556 id607 id2622 id16089  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bishop.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bishop.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267788-13963c835650d26e6f1a10cc891ba573-thumb_jpg.jpg?buster=1379827771" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Point Analytics -  events concerts nightclubs event management" />
<h1 property="name">
Point Analytics
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Box app for ticket sales and admission control<br></strong>
PointAmalytics (Till) is a complete system of logistics and event handling when it comes to box office, ticketing and access is concerned, this can be adapted to the requirements and size of the event. The system has modules that connect to each other, allow the ...</p>
</a>
</div>

<div data-id="267749" data-name="bembo" data-href="http://www.bembo.co" class="card startup   id51 id52 id83 id1054  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bembo.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bembo.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267749-5e56a61889606545465ee3cdf6dfb699-thumb_jpg.jpg?buster=1379911917" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="bembo -  sales and marketing analytics CRM augmented reality" />
<h1 property="name">
bembo
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile surveys with benefits<br></strong>
In order to become more competitive and offer what consumers need/want, companies need to come closer to consumers and understand them. We will leverage the latest technologies to make this happen.
With Bembo companies will be able to generate surveys and target ...</p>
</a>
</div>

<div data-id="267656" data-name="Peeksil S.A.P.I" data-href="http://www.peeksil.com" class="card startup   id2 id13 id1031 id2453  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.peeksil.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.peeksil.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267656-7f132a236af7b5b75bb858fa90385095-thumb_jpg.jpg?buster=1379790979" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Peeksil S.A.P.I -  biotechnology health care aerospace business travelers" />
<h1 property="name">
Peeksil S.A.P.I
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Stand Finder<br></strong>
Stand Finder is an APP that allows to increase the productivity of visitors and exhibitors of Business Travel Market (Congress / Expos / Fairs), allows organizing creative, dynamic and sustainable events.
Allow the visitor:
- To have digital agenda (activities, ...</p>
</a>
</div>

<div data-id="267599" data-name="ColorFin" data-href="http://colorfin.cloudapp.net/" class="card startup   id43 id2531  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://colorfin.cloudapp.net/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://colorfin.cloudapp.net/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267599-5a318b7a30c2084e13e7ed8ef6560da3-thumb_jpg.jpg?buster=1379783021" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ColorFin -  education social network media" />
<h1 property="name">
ColorFin
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> LinkedIn for Education Community<br></strong>
ColorFin is a Virtual Space for learning interaction, to help Schools, kids, teachers and parents, improve education through the use of technology in classrooms and home, balancing the use of tech education resources, entertainment, socialization and safety. It’s ...</p>
</a>
</div>

<div data-id="267546" data-name="Peeksil S.A.P.I" data-href="http://www.peeksil.com ; http://www.stanfinder.mx" class="card startup   id2453  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.peeksil.com ; http://www.stanfinder.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.peeksil.com ; http://www.stanfinder.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267546-aa466fb17d9fde6897a854fd1a43ce46-thumb_jpg.jpg?buster=1379758350" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Peeksil S.A.P.I -  business travelers" />
<h1 property="name">
Peeksil S.A.P.I
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Stand Finder<br></strong>
Stand Finder App lets you to increase productivity of visitors and exhibitors an Congress / Expo / Fairs allows organizing creative, dynamic and sustainable events, generating a personalized experience.
The customer is the business travel market (Private &amp; Government)
Benefits ...</p>
</a>
</div>

<div data-id="267519" data-name="IHTEM" data-href="http://WWW.IHTEM-EDU.MX" class="card startup   id43 id2806  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://WWW.IHTEM-EDU.MX" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://WWW.IHTEM-EDU.MX" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267519-76f10494a8f4626371a27482cca50fc6-thumb_jpg.jpg?buster=1379746211" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="IHTEM -  education corporate training" />
<h1 property="name">
IHTEM
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> UNIVERSIDAD CON MODELO HUMANISTA BASADO EN COMPETENCIAS ÚNICO EN MÉXICO<br></strong>
Universidad con licenciaturas semi-presenciales, basados en un modelo de Desarrollo Humano y desarrollo de competencias; único en México.</p>
</a>
</div>

<div data-id="267494" data-name="LVDG" data-href="http://www.ourladyofguadalupe.co &amp; www.lavirgendeguadalupe.co" class="card startup   id238 id15202  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ourladyofguadalupe.co &amp; www.lavirgendeguadalupe.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ourladyofguadalupe.co &amp; www.lavirgendeguadalupe.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267494-ad4f40ec3d6c1391e9733be1e55837f3-thumb_jpg.jpg?buster=1379740144" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="LVDG -  religion jewelry" />
<h1 property="name">
LVDG
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Accessories inspired in Our Lady of Guadalupe <br></strong>
Product is all jewelry and accessories inspired in Our Lady of Guadalupe. It helps the customer because it offers in one stop shop unique and exclusive products inspired in our lady of guadalupe, which is the most important catholic icon worldwide. The customer ...</p>
</a>
</div>

<div data-id="267424" data-name="CIDUTEC S.A." data-href="http://www.cidutec.com" class="card startup   id3 id43 id51 id1056  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.cidutec.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.cidutec.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267424-10190a5d38c144876fddb25cecb4db3f-thumb_jpg.jpg?buster=1379721275" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CIDUTEC S.A. -  mobile education sales and marketing technology" />
<h1 property="name">
CIDUTEC S.A.
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Ideas que innovan con tecnologías que generan cambios<br></strong>
Buscamos desarrollar, capacitar e implementar soluciones tecnológicas como sistemas, aplicaciones móviles, marketing electrónico, social media, soluciones a la medida y consultoría en TIC's mediante una red nacional de profesionales del ramo.
Buscamos la implementación ...</p>
</a>
</div>

<div data-id="267417" data-name="Fashionalia " data-href="https://fashionalia.com.mx/" class="card startup   id29 id93 id181 id230  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://fashionalia.com.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://fashionalia.com.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267417-251b563dc7c3afd6a1d0803c9688ca4b-thumb_jpg.jpg?buster=1379720559" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fashionalia  -  e-commerce fashion communities social commerce" />
<h1 property="name">
Fashionalia
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
Fashionalia it´s an online plus size fashion store for women.
Right now our main costumers are mexican citizens although this concept is made with a future growth strategy to reach the international market.
Our products are made based on physical needs and tastes ...</p>
</a>
</div>

<div data-id="267409" data-name="Opencolours" data-href="http://www.opencolours.com" class="card startup   id4 id47 id686 id8962  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.opencolours.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/opencolours" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.opencolours.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267409-77c5e1d5b010e8fbe7c51f52022a8631-thumb_jpg.jpg?buster=1379719243" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Opencolours -  digital media music brand marketing content delivery" />
<h1 property="name">
Opencolours
</h1>
<span href="http://www.twitter.com/opencolours" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Innovative Digital Content Distribution<br></strong>
Opencolours is an online platform for digital media distribution using download codes that can be attached virtually to anything the user wants. We work together with our customers to develop the most innovative campaigns by exploiting creativity and design, so ...</p>
</a>
</div>

<div data-id="267366" data-name="KMS" data-href="http://www.kilometros.mx" class="card startup   id3 id73 id334 id418  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.kilometros.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/kmsmex" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.kilometros.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267366-2018255300b56c1d2e9cd44efe292f91-thumb_jpg.jpg?buster=1379712161" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="KMS -  mobile sports green personal health" />
<h1 property="name">
KMS
</h1>
<span href="https://twitter.com/kmsmex" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Move, improve and gain for it.<br></strong>
KMS is an app that counts the physical activity that the user does along the day through an intelligent bracelet. The data is visualized in kms, calories, CO2 reduction to the environment and money saving. The app interacts with the user through statidistics, daily ...</p>
</a>
</div>

<div data-id="267248" data-name="Bazar de Arte" data-href="http://bazardearte.mx" class="card startup   id29 id162  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://bazardearte.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://bazardearte.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267248-579c4c1d9eb47cfbe9bd8d59515f99b6-thumb_jpg.jpg?buster=1379817894" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bazar de Arte -  e-commerce marketplaces" />
<h1 property="name">
Bazar de Arte
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> mercado libre, etsy<br></strong>
Un marketplace (e-commerce) especializado en manualidades y artesanias.
Las personas tendran un espacio en donde puedan exponer y principalmente vender sus manualidades en México. Asi como la oportunidad de comprar productos unicos hechos a mano.
Cualquier persona ...</p>
</a>
</div>

<div data-id="267240" data-name="TADE" data-href="http://youngoffendersmedia.tumblr.com/" class="card startup   id43 id130 id386 id407  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://youngoffendersmedia.tumblr.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/TADE_MX" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://youngoffendersmedia.tumblr.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267240-d8063c5fd124073e1cbc564304fb4eca-thumb_jpg.jpg?buster=1379695643" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="TADE -  education photography design art" />
<h1 property="name">
TADE
</h1>
<span href="http://twitter.com/TADE_MX" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> E-learning for specific knowledge and skills<br></strong>
TADE is an E-Learning platform, which is able to learn from the best, very specific topics, skills and knowledge translated into a real-world application, this first stage focused on the world of digital creativity.
In the market there are deals that offer entire ...</p>
</a>
</div>

<div data-id="267053" data-name="Zero Waste Process Mexico" data-href="http://zerowastemx.com" class="card startup   id26 id1948 id2474  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://zerowastemx.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@zerowastemx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://zerowastemx.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/267053-bf9566cee9ba48ce584344aa43ebf36e-thumb_jpg.jpg?buster=1379641327" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Zero Waste Process Mexico -  clean technology sustainability recycling" />
<h1 property="name">
Zero Waste Process Mexico
</h1>
<span href="http://twitter.com/@zerowastemx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Plastic pellet recycling processes<br></strong>
AIIR is a waste management program based in best technical, operational and public policy practices.
AIIR contains three stages: i) diagnosis, ii) implementation, and iii) evaluation. Each of the stages involve education, waste pick up, inorganic and organic ...</p>
</a>
</div>

<div data-id="266974" data-name="VINKD" data-href="http://www.vinkd.mx" class="card startup   id29 id340 id1474 id9847  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.vinkd.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

 </div>
</div>
<a class="main_link" href="http://www.vinkd.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266974-a38577dfe85269ce8054fc934b23ff68-thumb_jpg.jpg?buster=1396841638" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="VINKD -  e-commerce small and medium businesses entrepreneur freelancers" />
<h1 property="name">
VINKD
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Business vinculation through passion <br></strong>
VINKD is a social network that seeks to link the best talent with the growing demand for specific profiles in Mexico. Our clients make sure to know your profile through projects uploaded to the web made by yourself.
Additionally, it functions as an informational ...</p>
</a>
</div>

<div data-id="266629" data-name="SNOB México" data-href="http://http:snob.mx" class="card startup   id201 id693 id1092 id13104  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://http:snob.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://http:snob.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266629-2abc8f3ed576f966edf983fe69e9d2f3-thumb_jpg.jpg?buster=1379560453" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SNOB México -  news lifestyle products lifestyle businesses mens specific" />
<h1 property="name">
SNOB México
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Men Magazine Mexico<br></strong>
SNOB.mx es una página de internet que busca ser la mayor referencia en el mercado de Life &amp; Style para hombres, con las últimas noticias y tendencias sobre temas que creemos le pueden interesar a los hombres desde 18 hasta 60 años. La página se dedica a crear noticias ...</p>
</a>
</div>

<div data-id="266598" data-name="white canvas creative" data-href="http://www.whitecanvascreative.com" class="card startup   id3 id277 id340 id416  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.whitecanvascreative.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.whitecanvascreative.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266598-e4af2e9a6aef6462d47a1a457bac3033-thumb_jpg.jpg?buster=1379554814" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="white canvas creative -  mobile iphone small and medium businesses android" />
<h1 property="name">
white canvas creative
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> itunes app<br></strong>
Se te descompuso el refri? APP “COMUNIDAD X”. Diario nos encontramos con pequeños o grandes problemas que no podemos solucionar con nuestras manos, también por que no todo lo sabemos y tampoco contamos con el tiempo ni el interés de hacerlas, es por eso que nace ...</p>
</a>
</div>

<div data-id="266502" data-name="Get In" data-href="http://www.get-in.com.mx" class="card startup   id3 id450 id686 id1474  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.get-in.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.get-in.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266502-886fb2e614bb848c29674815c8576834-thumb_jpg.jpg?buster=1379537774" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Get In -  mobile social media marketing brand marketing entrepreneur" />
<h1 property="name">
Get In
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Intelligent Platform of contextual marketing<br></strong>
We look to provide the market with information that up until now, it has not had access to. Brands spend millions on advertising campaigns without really knowing which campaigns result in successful purchases.
We are taking advantage of the fact that the consumer ...</p>
</a>
</div>

<div data-id="266499" data-name="DINO" data-href="http://www.dino.mx" class="card startup   id10 id38 id11317 id84551  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.dino.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/DinoDocManager" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.dino.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266499-3e6462f7cb1d2f97807ad3f00cb7024f-thumb_jpg.jpg?buster=1390942169" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="DINO -  SaaS cloud computing document management ecm" />
<h1 property="name">
DINO
</h1>
<span href="https://twitter.com/DinoDocManager" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Documents In, kNowledge Out<br></strong>
DINO is a full web document archiving solution for small and medium businesses. DINO is not just a cloud storage solution! It is a structured space for organizing and making sense of your company's documents. &quot; Documents In, kNowledge Out ! &quot;
Our ambition is to ...</p>
</a>
</div>

<div data-id="266370" data-name="Smirkr" data-href="http://smirkr.com" class="card startup   id3 id6 id57 id1067  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://smirkr.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://smirkr.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266370-ab2b43804313f9b18996f9bbfd34a840-thumb_jpg.jpg?buster=1380483639" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Smirkr -  mobile social media location based services online dating" />
<h1 property="name">
Smirkr
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social encounters app<br></strong>
Smirkr is a social app that lets you find people around you in order to arrange encounters, always showing how close they are to your location. It is safe, discrete, elegant and fun. You can also browse other users by interests and demographic information. Smirkr ...</p>
</a>
</div>

<div data-id="266221" data-name="FACTURAMA" data-href="http://www.facturama.mx" class="card startup   id12 id29 id585 id16090  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.facturama.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/facturama.mx?fref=ts" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/FacturamaMX" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.facturama.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266221-156101035caff2e9e16c96c8f82b239e-thumb_jpg.jpg?buster=1379477650" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="FACTURAMA -  enterprise software e-commerce accounting billing" />
<h1 property="name">
FACTURAMA
</h1>
<span href="https://twitter.com/FacturamaMX" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> ANY INDIVIDUAL AND SMEs CAN GROW A BUSINESS THROUGH FACTURAMA<br></strong>
65 out of 100 new companies created in one year, disappear 2 years later due to mismanagement.
So we created Facturama...
Facturama is a website platform that provides support to individuals and SMEs to have a better control on their daily business through electronic ...</p>
</a>
</div>

<div data-id="266194" data-name="Lucasian Labs México" data-href="http://www.lucasianmexico.com/" class="card startup   id12 id427  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.lucasianmexico.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.lucasianmexico.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266194-16b2c2727aaa632369a36a0c635589f3-thumb_jpg.jpg?buster=1379471578" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Lucasian Labs México -  enterprise software social fundraising" />
<h1 property="name">
Lucasian Labs México
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Find the best project to fund!<br></strong>
Apoya
Apoya is a funding request system for enterprises, it let's you create your funding systems from scratch.
Enterprises, governments, research agencies provide funding for projects, some of them handle a big amount of initiatives that are supposed to help ...</p>
</a>
</div>

<div data-id="266044" data-name="SMARTMED" data-href="http://www.smartmedalejandroavalos.blogspot.mx" class="card startup   id3 id6 id13 id29  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.smartmedalejandroavalos.blogspot.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.smartmedalejandroavalos.blogspot.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266044-e3439a8e716efd2e31ccef038eae55d5-thumb_jpg.jpg?buster=1379451601" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SMARTMED -  mobile social media health care e-commerce" />
<h1 property="name">
SMARTMED
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Data Manager for Doctors-patients-services<br></strong>
Online Medical first community with access to information and database of patients, with the support of financial management tools and support in complementary sectors such as pharmaceuticals and Social Security.
For Patients generate a par benefits, discounts ...</p>
</a>
</div>

<div data-id="266019" data-name="Bypath" data-href="http://bypath.mx" class="card startup   id71 id473 id1116 id2598  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://bypath.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://bypath.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266019-1ba1ff75054d368df22e4e72003d1381-thumb_jpg.jpg?buster=1379447878" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bypath -  payments mobile payments public transportation parking" />
<h1 property="name">
Bypath
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile Payment Tag<br></strong>
Bypath is a mobile application that allows payments simply and safely in congested areas such as parking lots, toll roads and public transport.
Bypath calculates the time or distance traveled to charge the fair rate of your service, you can pay by credit card, ...</p>
</a>
</div>

<div data-id="266003" data-name="Brad Corp" data-href="http://www.evernetsite.com" class="card startup   id6 id675 id11600  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.evernetsite.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.evernetsite.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/266003-c25cbe746d73ce3c7aa51da7aa6e5287-thumb_jpg.jpg?buster=1379447962" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Brad Corp -  social media pets all students" />
<h1 property="name">
Brad Corp
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mascotas y Objetos Perdidos Recuperar <br></strong>
Evernet
el sitio en el que puedes generar tus etiquetas para proteger a sus seres queridos y objetos de valor les da más protección que una sola etiqueta para identificarlos.
Usted tendrá la tranquilidad de saber que sus mascotas u objetos se protegido con nuestras ...</p>
</a>
</div>

<div data-id="265974" data-name="B.english" data-href="http://www.benglish.org" class="card startup   id43 id111 id1768  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.benglish.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.benglish.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/265974-ca47656c6c4f03f30b6ce1ca71dec4ef-thumb_jpg.jpg?buster=1379444995" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="B.english -  education language learning english speaking" />
<h1 property="name">
B.english
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Bilingüe, Rápido, Fácil y bajo costo.<br></strong>
Nuestro mercado son estudiantes a punto de concluir su carrera y egresados de áreas económico administrativas, Empresas que busquen mantener un alto nivel de capacitación de su capital humano.
Hacemos que la gente se vuelva inglesa dominando el idioma inglés ...</p>
</a>
</div>

<div data-id="265914" data-name="Building chains" data-href="http://not yet" class="card startup   id29  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://not yet" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://not yet" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/265914-c454465cd5ee841d7bcbc65b090ba764-thumb_jpg.jpg?buster=1383918493" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Building chains -  e-commerce" />
<h1 property="name">
Building chains
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Supply chain for Pymes<br></strong>
I want to start a business with the aim to increase the sales of PYMES and companies inside of our country, The most of the companies in México are PYMES and those companies hasn´t resources, knowledge, desire or volume to sale their products outside México, moreover, ...</p>
</a>
</div>

<div data-id="265671" data-name="Gamerstrike" data-href="http://gamerstrike.mx" class="card startup   id29 id100 id1891  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://gamerstrike.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/gamerstrike" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://gamerstrike.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/265671-2deca04b3b7c3148337c191d1bd5fdbd-thumb_jpg.jpg?buster=1379394551" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Gamerstrike -  e-commerce video games subscription businesses" />
<h1 property="name">
Gamerstrike
</h1>
<span href="http://twitter.com/gamerstrike" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online video game rental<br></strong>
Gamerstrike is the first online subscription based service in Mexico allowing its users to enjoy the latest console games for a flat monthly fee. Our subscriptions are designed for casual and hardcore gamers as well. Play as long as you want. No late fees.</p>
</a>
</div>

<div data-id="264612" data-name="WebGo" data-href="http://webgo.mx" class="card startup   id29 id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://webgo.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://webgo.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/264612-522eb998dc2acdf686b170d46b873a80-thumb_jpg.jpg?buster=1379195023" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="WebGo -  e-commerce small and medium businesses" />
<h1 property="name">
WebGo
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> You professional website instantly<br></strong>
WebGo is the easiest and fastest way to get a professional website. Exceptional image, responsive design, domain, hosting, unlimited emails, innovative online marketing tools, e-commerce tools, training ... Everything needed for the success of your website.</p>
</a>
</div>

<div data-id="264566" data-name="Teaching Valley" data-href="http://www.teachingvalley.com" class="card startup   id43  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.teachingvalley.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.teachingvalley.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/264566-235adb87f985a05038d032ae94969e59-thumb_jpg.jpg?buster=1379184960" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Teaching Valley -  education" />
<h1 property="name">
Teaching Valley
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Education<br></strong>
Teaching Valley provides a full private social network for Schools, including, School Control, Search engine specialized in education, searching over YouTube, Wikipedia and WolfranAlpha. Even the Parents could follow the school track of their children. It will ...</p>
</a>
</div>

<div data-id="264532" data-name="Amos de Casa" data-href="http://NONE.COM" class="card startup   id72 id710  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://NONE.COM" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://NONE.COM" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/264532-66b01033355cef7233202d09ef480bf6-thumb_jpg.jpg?buster=1379173462" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Amos de Casa -  food and beverages customer service" />
<h1 property="name">
Amos de Casa
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Servicio de limpieza, lavado de ropa y abastecimiento de insumos caseros a domicilio.<br></strong>
Seleccionar poblados cercanos a la ciudad de México. Poblados en el estado de México, Hidalgo, Morelos, Puebla y Queretaro que no se encuentren a una distancia mayor de 45 min. en auto y que cuenten con el material humano para trabajar en la empresa en la unidad ...</p>
</a>
</div>

<div data-id="264077" data-name="Bishop Consultores" data-href="http://www.bishop.mx" class="card startup   id43 id314 id481 id9912  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bishop.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bishop.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/264077-9d15bb319e693a7645735feb3a1a3902-thumb_jpg.jpg?buster=1379047996" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bishop Consultores -  education families kids educational games" />
<h1 property="name">
Bishop Consultores
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Fun Kids Science<br></strong>
Doc Dok is a multimedia hub, where children from 6 to 12 years learn science in a fun and playful way, we will develop animations, interactive games and multimedia products to teach science, with our professor Doc Dok and his faithful sidekick Pepe Topo will have ...</p>
</a>
 </div>

<div data-id="264028" data-name="Lottery Popcorn" data-href="http://www.lotterypopcorn.com" class="card startup   id14 id50 id72 id195  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.lotterypopcorn.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.lotterypopcorn.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/264028-539775e289acf0e4a8eaaf44b1f5ab23-thumb_jpg.jpg?buster=1379034334" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Lottery Popcorn -  games lead generation food and beverages lotteries" />
<h1 property="name">
Lottery Popcorn
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Lottery tickets with popcorn bags<br></strong>
Is the duo lottery ticket - popcorn bag.
Is intended for a small lottery that gives away $5000 MXN, at the begining every month, but later it will be weekly, according to the bussines growth.
In order to play you have to register the code that comes within ...</p>
</a>
</div>

<div data-id="263880" data-name="Figuratica" data-href="http://www.figuratica.com" class="card startup   id251 id340 id1429 id2812  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.figuratica.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/armandodiseos" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.figuratica.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/263880-6dde246f7974738ac4f6559d2820b476-thumb_jpg.jpg?buster=1379015346" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Figuratica -  governments small and medium businesses digital entertainment entertainment industry" />
<h1 property="name">
Figuratica
</h1>
<span href="https://twitter.com/armandodiseos" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Personalized Figures<br></strong>
All kinds of custom shapes for parties, cakes, weddings, parties, awards, trophies made ​​of plastic with base plates and custom box.Our clients are those who want an original touch to your moment, just want to figure out your favorite idol, added to this, we make ...</p>
</a>
</div>

<div data-id="263592" data-name="Logoskope ®" data-href="http://Logoskope.com" class="card startup   id129 id386 id1262 id1414  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://Logoskope.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://Logoskope.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/263592-152daed64ed4c136e3341427986f365a-thumb_jpg.jpg?buster=1378949623" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Logoskope ® -  graphics design creative industries creative" />
<h1 property="name">
Logoskope ®
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The amazing App to sketch your awesome logos!<br></strong>
The amazing app to sketch your awesome logos in a kaleidoscope fashion!
1. Choose your figures &amp; colors.
2. Then create some fancy effects.
3. Add a cool font and a title.
4. Share your Logoskope.</p>
</a>
</div>

<div data-id="263435" data-name="mobiLender" data-href="http://www.mobilender.mx" class="card startup   id17 id577 id2795  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.mobilender.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@mobilender_mx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.mobilender.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/263435-b0c5302b09aa9b4b5d6b91bbfe1b4cc8-thumb_jpg.jpg?buster=1401933582" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="mobiLender -  financial services credit consumer lending" />
<h1 property="name">
mobiLender
</h1>
<span href="http://twitter.com/@mobilender_mx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile exclusive Micro-Lending service with fast approval and 24/7 availability<br></strong>
We focus on emergency short-term micro loans in LATAM, targeting young people and Housewives. Service available 24/7, With a response of less than 40 mins. 100% Mobile Exclusive.
Product: 
•	Loans from $5.00 to $4,500.00 US / Avrg. Loan $50 US on a term of 12 ...</p>
</a>
</div>

<div data-id="263327" data-name="Mayorear" data-href="http://www.mayorear.com" class="card startup   id29 id51 id230  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.mayorear.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@mayorear" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.mayorear.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/263327-3ced92ff1a4b3febc27af50455c16d4f-thumb_jpg.jpg?buster=1379272160" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Mayorear -  e-commerce sales and marketing social commerce" />
<h1 property="name">
Mayorear
</h1>
<span href="http://twitter.com/@mayorear" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Wholesailing online shop <br></strong>
Wholesaling is a different online shop which intends to sell a variety of highly requested merchandise, which comes two differrent sources:
1.	Owned stock (including the imports fromChina and India) and
2.	Gathered through direct consignation for the manufacturer, ...</p>
</a>
</div>

<div data-id="263103" data-name="Languo" data-href="http://www.languo.co/" class="card startup   id3 id43  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.languo.co/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.languo.co/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/263103-5e796fc5490bd216700c86e9044cf8ec-thumb_jpg.jpg?buster=1384454466" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Languo -  mobile education" />
<h1 property="name">
Languo
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Practice languages with native speakers<br></strong>
We all know that is easy to learn other language, especialy English. We can learn it in school, university or any online course, and there are plenty of them. But very few have access to practice, improve, understand and speak perfectly.
Languo is an all-in-one ...</p>
</a>
</div>

<div data-id="262513" data-name="Novelistik" data-href="http://www.novelistik.com" class="card startup   id87 id269 id498  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.novelistik.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.novelistik.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/262513-cc15e02518806ce043d8b7fcc8897f82-thumb_jpg.jpg?buster=1378771393" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Novelistik -  publishing social media platforms e books" />
<h1 property="name">
Novelistik
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Wattpad with a social network and interactive books.<br></strong>
Novelistik is a social platform for publishing and reading. For authors, it offers the most powerful publishing tool which allows, among other things, the creation of interactive ebooks with great ease. It is also a social network which allows authors to interact ...</p>
</a>
</div>

<div data-id="262086" data-name="Artesaliz" data-href="http://www.artesaliz.com" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.artesaliz.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.artesaliz.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/262086-9939c413872afc95522e0f53f3013b91-thumb_jpg.jpg?buster=1378704464" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Artesaliz - " />
<h1 property="name">
Artesaliz
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="262062" data-name="Auto Chilango" data-href="http://autochilango.com" class="card startup   id1172  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://autochilango.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/autochilango" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://autochilango.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/262062-a2f445e7f0064bc2e2257da06a8f2f9b-thumb_jpg.jpg?buster=1378700002" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Auto Chilango -  utilities" />
<h1 property="name">
Auto Chilango
</h1>
<span href="http://twitter.com/autochilango" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Solving the problems of drivers in Mexico city<br></strong>
We built an app for iOS devices that helps people to manage everything that is necesary with having a car in Mexico City.
We adopt the &quot;in app purchase&quot; business model and gave the user three useful tools for free.
We'll alert you the dates you can't drive your ...</p>
</a>
</div>

<div data-id="261925" data-name="Bromente" data-href="http://bromente.com" class="card startup   id4 id6 id47 id9530  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://bromente.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://bromente.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261925-795e197e807a170b21dbd595ac874073-thumb_jpg.jpg?buster=1378670944" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bromente -  digital media social media music tv production" />
<h1 property="name">
Bromente
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Digital Media Portal para producir, monetizar y vender.<br></strong>
Bromente es un portal nativo de la idea global de Internet. Fusiona los medios audiovisuales como la televisión con los medios escritos como las revistas y ofrecemos al público un medio que abarca sus gustos, necesidades de entretenimiento e información, en base ...</p>
</a>
</div>

<div data-id="261551" data-name="City Wifi" data-href="http://www.citywifi.com.mx" class="card startup   id205 id340 id443 id10341  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.citywifi.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@thecitywifisyou" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.citywifi.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261551-4554523e38e9bcf7c298d11ff2ddd4a2-thumb_jpg.jpg?buster=1398746208" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="City Wifi -  mobile advertising small and medium businesses mobile coupons local businesses" />
<h1 property="name">
City Wifi
</h1>
<span href="http://twitter.com/@thecitywifisyou" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> WIFI MKT &amp; analytics<br></strong>
Is funny how brands are publishing mobile in Mexico:
drink a cup of coffee, visit their Site and get a coupon,
foursquare discounts,
APP purchasing.
But none of them worries about their mobiles battery and Internet access.
In mexico there are more than 4.3 million ...</p>
</a>
</div>

<div data-id="261467" data-name="Dephiance" data-href="http://www,dephce,com" class="card startup   id22  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www,dephce,com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www,dephce,com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261467-c1a960472c490e042ea4ee3cf84ef8e9-thumb_jpg.jpg?buster=1378503564" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Dephiance -  energy" />
<h1 property="name">
Dephiance

</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> assembly and design sistems and devices for energy savings<br></strong>
Assembly interfaces for lamp control powered by Ethernet, Energy savers in factory facilities (light, compressed air, SCADA etc ) Retrofit of luminaries to LED thechnology, Assemblage of solar power stations of type package with backup</p>
</a>
</div>

<div data-id="261465" data-name="Fun Practice Chinese" data-href="http://www.funpractice.com" class="card startup   id3 id43 id490 id12773  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.funpractice.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@funpracticemx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.funpractice.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261465-7a5541f75451c7d90c709c966683f4aa-thumb_jpg.jpg?buster=1378503364" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fun Practice Chinese -  mobile education mobile games pc gaming" />
<h1 property="name">
Fun Practice Chinese
</h1>
<span href="http://twitter.com/@funpracticemx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Digital material to learn Chinese for Spanish speaker students.<br></strong>
The reality shows that most of the material to learn chinese are in english, and for ex. people in Latin America face a big challenge when they want to learn this very important tool that can give them another chance to development.
Fun Practice is a site where ...</p>
</a>
</div>

<div data-id="261326" data-name="99minutos.com" data-href="http://www.99minutos.com" class="card startup   id14 id93 id286 id3065  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.99minutos.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@99minutos" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.99minutos.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261326-db7e6caaa4895e640acba4ebcf2ba6ce-thumb_jpg.jpg?buster=1399346700" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="99minutos.com -  games fashion consumer electronics toys" />
<h1 property="name">
99minutos.com
</h1>
<span href="http://twitter.com/@99minutos" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The fastest shipping for online shopping<br></strong>
De lo quiero a lo tengo en menos de 99minutos
Compra en linea y recibe en tiempo record, rastrea tu producto en vivo y paga el producto en contra entrega.</p>
</a>
</div>

<div data-id="261164" data-name="Bongo Studios" data-href="http://www.bongo-studios.com" class="card startup   id43 id320 id490 id9912  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bongo-studios.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bongo-studios.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261164-9c88515500bf16242cc8cbac62b0d5cc-thumb_jpg.jpg?buster=1378443085" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bongo Studios -  education parenting mobile games educational games" />
<h1 property="name">
Bongo Studios
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We teach you while you are having fun<br></strong>
Bongo Studios is a company focused on musical educational apps for children which can be used either on mobile devices such as tablets, smartphones or on the web and social networks, through games and study tools that allow children to learn and have fun.</p>
</a>
</div>

<div data-id="261095" data-name="Bromente" data-href="http://bromente.com" class="card startup   id4 id93 id290 id10926  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://bromente.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://bromente.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261095-4a82692c8c8b31a195fbe95c0538ca5b-thumb_jpg.jpg?buster=1380152215" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bromente -  digital media fashion video streaming independent music" />
<h1 property="name">
Bromente
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Bromente tu portal de entretenimiento y apoyo a emprendedores.<br></strong>
Bromente es un portal nativo de la idea global de Internet. Fusiona los medios audiovisuales como la televisión con los medios escritos como las revistas y ofrecemos al público un medio que abarca sus gustos, necesidades de entretenimiento e información, en base ...</p>
</a>
</div>

<div data-id="261086" data-name="Synergy BioTech" data-href="http://www.synergy-biotech.com" class="card startup   id72 id253 id702  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.synergy-biotech.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.synergy-biotech.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261086-24cf37aed007a510158f5f5aaf7c08f2-thumb_jpg.jpg?buster=1378424756" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Synergy BioTech -  food and beverages consulting agriculture" />
<h1 property="name">
Synergy BioTech
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> CREATING INNOVATION &amp; COMPETITIVENESS<br></strong>
Consulting firm in food science &amp; technology and food analysis laboratory</p>
</a>
</div>

<div data-id="261078" data-name="Sustainable Projects Engineering" data-href="http://inpros.jimdo.com" class="card startup   id220 id631 id9708 id11024  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://inpros.jimdo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@ArtKon" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://inpros.jimdo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261078-f8f2776ab06e19fabef6b8b7889a5f77-thumb_jpg.jpg?buster=1378423338" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sustainable Projects Engineering -  industrial transportation commercial real estate industrial energy efficiency" />
<h1 property="name">
Sustainable Projects Engineering
</h1>
<span href="http://twitter.com/@ArtKon" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Zero Emission Technologies<br></strong>
Clean Fuel Systems for transportation, industrial, commercial and domestic applications.
+ Vehicle Fuel Saving System: improves at least 25% vehicle fuel efficiency while reducing GHG emissions and fuel costs in affordable way. Is a cheap but reliable hybridizing ...</p>
</a>
</div>

<div data-id="261064" data-name="CiudadYOGA" data-href="http://www.ciudadyoga.com" class="card startup   id4 id10 id73  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ciudadyoga.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ciudadyoga.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261064-70d6b2cfee95c61a9a13e8aa957e55c1-thumb_jpg.jpg?buster=1378421487" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CiudadYOGA -  digital media SaaS sports" />
<h1 property="name">
CiudadYOGA
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online Yoga Classes in Spanish<br></strong>
CiudadYOGA is the leading Yoga Video Website in Spanish.
For one monthly price, members get access to all of our classes, anytime, anywhere, on nearly any Internet-connected screen.
Great community in Facebook with more than 650,000 followers.</p>
</a>
</div>

<div data-id="261058" data-name="Zabvio.com" data-href="http://www.zabvio.com" class="card startup   id17 id577 id721 id14033  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.zabvio.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@zabvio" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.zabvio.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/261058-b17ee53ee02183c5eb6d13fb849c3650-thumb_jpg.jpg?buster=1378421685" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Zabvio.com -  financial services credit edutainment insurance companies" />
<h1 property="name">
Zabvio.com
</h1>
<span href="http://twitter.com/@zabvio" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> the financial match.com <br></strong>
Web plataform that links people needing money with loans and financial institutions. We help our financial clients to find prospects. Users are the people who need credit. Our sistem evaluate and qualify the user request and once it completes its profile it is ...</p>
</a>
</div>

<div data-id="260999" data-name="OzCampus" data-href="http://www.ozcampus.net" class="card startup   id277 id360 id490 id12773  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ozcampus.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ozcampus.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260999-92dbb5d3b9a27a878dc8772a650654fd-thumb_jpg.jpg?buster=1378414636" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="OzCampus -  iphone ipad mobile games pc gaming" />
<h1 property="name">
OzCampus
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> E-learning, IT, Apps<br></strong>
&quot;Dating game&quot; (temporally name), it´s now a prototype, the app is a simulator of relationships, you need to seduce a woman (win points), you need to invite her (him), it´s a life simulator with the posibility to insert advertising of a kind. Very adictive and only ...</p>
</a>
</div>

<div data-id="260996" data-name="Entucel.com" data-href="http://entucel.com" class="card startup   id51 id339 id686 id761  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://entucel.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/entucelcom" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://entucel.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260996-919265062bcbbabab11d12010ea3c5c4-thumb_jpg.jpg?buster=1378414243" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Entucel.com -  sales and marketing advertising platforms brand marketing sms" />
<h1 property="name">
Entucel.com
</h1>
<span href="https://twitter.com/entucelcom" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Publicidad via SMS<br></strong>
El objetivo del Sitio Web es realizar campañas de publicidad por medio de mensajes de texto al celular (SMS), para ello ofrecemos y contamos con la plataforma adecuada.</p>
</a>
</div>

<div data-id="260969" data-name="Gift2Gift" data-href="http://www.gift2gift.me" class="card startup   id230  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.gift2gift.me" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.gift2gift.me" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260969-af3522087285da4c5d20c7b7263b8cfe-thumb_jpg.jpg?buster=1378412748" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Gift2Gift -  social commerce" />
<h1 property="name">
Gift2Gift
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
Gift2Gift is a web app that allows you to create the christmas exchanges very quickly, no longer have to get together with friends and write little papers and put them in a bag to know who will give you gift to each</p>
</a>
</div>

<div data-id="260958" data-name="Central DF" data-href="http://www.centraldf.mx" class="card startup   id386 id10969  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.centraldf.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/Central_df" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.centraldf.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260958-e9dd7e214cadb41313fdff1fbb9765d9-thumb_jpg.jpg?buster=1378411022" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Central DF -  design home decor" />
<h1 property="name">
Central DF
</h1>
<span href="https://twitter.com/Central_df" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="260957" data-name="Caos Inc" data-href="http://www.caosinc.com" class="card startup   id3 id450  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.caosinc.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/Caos_Inc" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.caosinc.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260957-495f6b6978297d9d2bae3f8413570877-thumb_jpg.jpg?buster=1378411197" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Caos Inc -  mobile social media marketing" />
<h1 property="name">
Caos Inc
</h1>
<span href="https://twitter.com/Caos_Inc" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="260536" data-name="Alart" data-href="http://alart.com.mx" class="card startup   id181 id407 id3134  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://alart.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@alartmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://alart.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260536-be5d19e82417cf51d8909a2182f5f199-thumb_jpg.jpg?buster=1379690616" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Alart -  communities art turism" />
<h1 property="name">
Alart
</h1>
<span href="http://twitter.com/@alartmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> art lovers community<br></strong>
Alart is an art lovers community powered by mobile technology that allows to be informed on the local art scene according to personal interests and opinions.
Alart members will become involved with events and actors considered relevant in their cultural context, ...</p>
</a>
</div>

<div data-id="260334" data-name="Test test test 123" data-href="http://www.thisisatest.com" class="card startup   id556  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.thisisatest.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.thisisatest.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/260334-90783e751913a70827055897e044022f-thumb_jpg.jpg?buster=1378308596" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Test test test 123 -  events" />
<h1 property="name">
Test test test 123
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The pitch<br></strong>
This is a test</p>
</a>
</div>

<div data-id="259462" data-name="Glokalbox" data-href="http://www.glokalbox.com" class="card startup   id263 id424 id443  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.glokalbox.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/glokalbox" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.glokalbox.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/259462-7363215354fcb15e12440d795a7b4c57-thumb_jpg.jpg?buster=1378147129" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Glokalbox -  restaurants deals mobile coupons" />
<h1 property="name">
Glokalbox
</h1>
<span href="http://www.twitter.com/glokalbox" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Geolocation based Daily Deals <br></strong>
Glokalbox is a new tool for business to launch daily deals, with Glokalbox they have control of the deals and are able to launch whenever they need or they want to offer something new in order to attract customers. Location based deals make easier to discover new ...</p>
</a>
</div>

<div data-id="259165" data-name="Maipo" data-href="http://Maipo.com" class="card startup   id383  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://Maipo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://Maipo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/259165-0dfa9717560b0d084f7afe03c194dd17-thumb_jpg.jpg?buster=1378069465" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Maipo -  insurance" />
<h1 property="name">
Maipo
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Community platform that impulses objectives, provides information and monitoring <br></strong>
Its an APP that helps sales people reach maximum standard that can be monitor by managers while creating a community to benchmark from.</p>
</a>
</div>

<div data-id="258484" data-name="Descúbrela" data-href="http://www.descubre.la" class="card startup   id3 id228 id556 id16089  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.descubre.la" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/descubre_la" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.descubre.la" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/258484-3f99965c82ac134fc6d645bbaa4230a7-thumb_jpg.jpg?buster=1378224215" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Descúbrela -  mobile local events event management" />
<h1 property="name">
Descúbrela
</h1>
 <span href="http://twitter.com/descubre_la" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Reinventing the way people discover the world around them.<br></strong>
Descúbrela is a web and mobile app that let's users discover what event are happening in their surrounding and recommends options according to their interests. For Free.</p>
</a>
</div>

<div data-id="255472" data-name="Sortis" data-href="https://www.facebook.com/pages/Sortis/419839101468543" class="card startup   id29 id64 id162 id15405  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="https://www.facebook.com/pages/Sortis/419839101468543" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="https://www.facebook.com/pages/Sortis/419839101468543" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/255472-556c4f25dec5c2e811874d9cce7140f6-thumb_jpg.jpg?buster=1378148704" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sortis -  e-commerce gambling marketplaces moneymaking" />
<h1 property="name">
Sortis
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> It's eBay on steroids. C2C + Online Raffling<br></strong>
We are a company that provides an e-commerce marketplace service that allows users get their desired products for a fraction of the retail price. At the same time, the sellers get to make bigger profits using our site than with traditional methods.
The method ...</p>
</a>
</div>

<div data-id="251976" data-name="Matador Projects" data-href="http://www.matadorprojects.com" class="card startup   id129 id386 id680 id11826  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.matadorprojects.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.matadorprojects.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/251976-ac42606eebcaf9a2b46fdfb7b12bc95d-thumb_jpg.jpg?buster=1376697218" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Matador Projects -  graphics design construction user experience design" />
<h1 property="name">
Matador Projects
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We are a creative studio 360° with global vison.<br></strong>
With Simplicity as our operating philosophy Matador Projects delivers services in brand identity and development, art direction, packaging, printed matter, interactive design, animation, art projects, exhibitions, fashion and product design.
Branding- and design ...</p>
</a>
</div>

<div data-id="251329" data-name="My Fives" data-href="http://www.myfives.co" class="card startup   id3 id29 id65 id2531  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.myfives.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.myfives.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/251329-88fcd29dd49df8dfffb204673aa6d6d0-thumb_jpg.jpg?buster=1376621037" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="My Fives -  mobile e-commerce information services social network media" />
<h1 property="name">
My Fives
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social Network Top Fives List<br></strong>
My fives let you create/share/explore lists about anything created for people in a fun way, it help to the people to have a social network list where you can find recommendations about something.the customer are people looking information or wants to share thoughts ...</p>
</a>
</div>

<div data-id="251315" data-name="My Fives" data-href="http://www.myfives.co" class="card startup   id3 id29 id65 id2531  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.myfives.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.myfives.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/251315-570ca03ed778720945470b3164771832-thumb_jpg.jpg?buster=1376618629" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="My Fives -  mobile e-commerce information services social network media" />
<h1 property="name">
My Fives
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> social network top fives list (thetoptens.com)<br></strong>
My fives let you create/share/explore lists about anything created for people in a fun way, it help to the people to have a social network list where you can find recommendations about something.the customer are people looking information or wants to share thoughts ...</p>
</a>
</div>

<div data-id="251294" data-name="bebe2go" data-href="http://www.bebe2go.com" class="card startup   id29 id837 id928 id930  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bebe2go.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bebe2go.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/251294-21aecb155b522df1cc820213b3132f9f-thumb_jpg.jpg?buster=1376614965" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="bebe2go -  e-commerce babies baby accessories baby safety" />
<h1 property="name">
bebe2go
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mexico´s online one-stop shop for baby products <br></strong>
One-stop Shop
￼Forget about going into different stores in order to buy what your baby needs. Our main priority is to offer a simple and friendly solution for all your baby needs. At bebe2go.com we know that your time is valuable!
To your doorstep!
Do not ...</p>
</a>
</div>

<div data-id="248737" data-name="PawHub" data-href="http://www.pawhub.me" class="card startup   id6 id675 id14268  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.pawhub.me" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.pawhub.me" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/248737-0396ccfc9f91439dea94e829c0f9221d-thumb_jpg.jpg?buster=1376341239" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="PawHub -  social media pets pet matching" />
<h1 property="name">
PawHub
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social platform for pet protection<br></strong>
Social platform pro companion animals. This integrates the efforts of companies, goverment, NGO's and independent people to generate more impact on the society. This platform offers different tools like Adoption Engine, Lost And Found, Abuse Reports, Crowdfunding ...</p>
</a>
</div>

<div data-id="248054" data-name="Brad Corp" data-href="http://www.evernetsite.com" class="card startup   id3 id6 id57 id388  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.evernetsite.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.evernetsite.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/248054-d8285a6acfec02f2bdae3aedea2e795f-thumb_jpg.jpg?buster=1379719572" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Brad Corp -  mobile social media location based services qr codes" />
<h1 property="name">
Brad Corp
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Pets lost objects<br></strong>
Evernet, the site where you can generate your tags to protect your loved ones or valuables give them more protection than a single tag to identify them.
You will have peace of mind that your pets or Objects will be protected with our QR code tags. If your pet ...</p>
</a>
</div>

<div data-id="246314" data-name="Dog and found" data-href="http://proyectos.checkbox.mx/dogfound/" class="card startup   id29 id675  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://proyectos.checkbox.mx/dogfound/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://proyectos.checkbox.mx/dogfound/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/246314-ea5f1900f61ac3d313b63c9325ff4ec7-thumb_jpg.jpg?buster=1375903765" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Dog and found -  e-commerce pets" />
<h1 property="name">
Dog and found
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Smart tags for pets<br></strong>
A tag for pets (mostly dogs) with a QR code and a 1-800 phone service that enables those who find a missing pet to report it online (via smart phone) or to our service line thus protecting the owners information.
Our aim is also to create a web page that will ...</p>
</a>
</div>

<div data-id="244346" data-name="Guarurapp" data-href="http://guarurapp.com/" class="card startup   id3 id62  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://guarurapp.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://guarurapp.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/244346-65627a1f972e873e261ad0f1cd64fc26-thumb_jpg.jpg?buster=1375692932" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Guarurapp -  mobile security" />
<h1 property="name">
Guarurapp
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Waze of Personal Security<br></strong>
Guarurapp is an mobile/web app that help you to reduce the possibility to being a victim of crime.
The app shows you the most dangerous places in a city , give you a better route to take and allows you to define the places of your interest.
Also if you're in a ...</p>
</a>
</div>

<div data-id="241859" data-name="VoyalDoc.com" data-href="http://www.voyaldoc.com/" class="card startup   id13  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.voyaldoc.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@VoyalDoc" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.voyaldoc.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/241859-b69ba5a3cb158ce3eb299ad0f06f6f3e-thumb_jpg.jpg?buster=1401750270" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="VoyalDoc.com -  health care" />
<h1 property="name">
VoyalDoc.com
</h1>
<span href="http://twitter.com/@VoyalDoc" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online medical care scheduling service<br></strong>
Voy al Doc is Mexico’s first online medical care scheduling service, which provides online search and booking options for patients free of charge.
Our service meets the needs of both patients and medical practitioners. It helps patients find a doctor of a particular ...</p>
</a>
</div>

<div data-id="241316" data-name="Viajamex" data-href="http://www.viajamex.com" class="card startup   id78 id1161 id9100  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.viajamex.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.viajamex.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/241316-027e8b297694ce19762119ecfb0b6600-thumb_jpg.jpg?buster=1375120033" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Viajamex -  online travel travel travel &amp; tourism" />
<h1 property="name">
Viajamex
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Best Travel Deals- Mexico<br></strong>
Find the best deals in Hotels, Flights, Car Rental, Getaways weekends and much more travel with just one phone call.
We help people with few economical resources make their travel dream come true, and in many cases travel by airplane for the first time.
Customer ...</p>
</a>
</div>

<div data-id="240222" data-name="Humana Innovacion Social" data-href="http://humana.org.mx/" class="card startup   id69 id208 id251 id680  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://humana.org.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/humanamx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://humana.org.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/240222-b6e1c14104e675467e16c0139ab5c579-thumb_jpg.jpg?buster=1374876344" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Humana Innovacion Social -  automotive ventures for good governments construction" />
<h1 property="name">
Humana Innovacion Social
</h1>
<span href="http://twitter.com/humanamx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Sustainable Social Impact<br></strong>
We help organisations create sustainable social impact.
To do so we use design and innovation to reducing the costs of their responsibility programs, even generating a return on investment.</p>
</a>
</div>

<div data-id="237737" data-name="InstaFit" data-href="http://www.instafit.com.mx" class="card startup   id29 id107 id418  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.instafit.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@instafitmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.instafit.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/237737-d4a94287a69b547e7edb40c85a7b6838-thumb_jpg.jpg?buster=1374507902" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="InstaFit -  e-commerce fitness personal health" />
<h1 property="name">
InstaFit
</h1>
<span href="http://twitter.com/@instafitmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online subscription fitness startup in Latam<br></strong>
INSTAFIT is an online fitness startup based in Mexico City and co-founded by international entrepreneurs Oswaldo Trava, Georg Stockinger and Natalia Amaya. The company aims to bring a fit lifestyle to Latin American homes through a proprietary web app that provides ...</p>
</a>
</div>

<div data-id="237253" data-name="Salud Fácil" data-href="http://www.saludfacil.org" class="card startup   id13 id321 id97955  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.saludfacil.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@saludfacil1" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.saludfacil.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/237253-caad775c3f1b51d73cf16313b162f11c-thumb_jpg.jpg?buster=1374357947" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Salud Fácil -  health care finance developing countries" />
<h1 property="name">
Salud Fácil
</h1>
<span href="http://twitter.com/@saludfacil1" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We give microloans to loan income individuals that need medical treatment.<br></strong>
Microloans for health related issued to people at the bottom of the pyramid.</p>
</a>
</div>

<div data-id="236565" data-name="Fric Martinez" data-href="http://www.fricmartinez.com" class="card startup   id246 id468  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.fricmartinez.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/fricmartinez" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.fricmartinez.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/236565-a48bc4703adc996499a0ff0aa8cbe6e4-thumb_jpg.jpg?buster=1374185170" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fric Martinez -  crowdsourcing recruiting" />
<h1 property="name">
Fric Martinez
</h1>
<span href="http://www.twitter.com/fricmartinez" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Creative media community and job board<br></strong>
Creative media community and job board with 30,000 members. 5,000 jobs have been possible.</p>
</a>
</div>

<div data-id="236480" data-name="BrandMe" data-href="http://www.brandme.la" class="card startup   id51 id342 id450 id686  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.brandme.la" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://facebook.com/BrandMe" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/BrandMe" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.brandme.la" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/236480-3970785c3bb9ee4132fd1c471299fdd9-thumb_jpg.jpg?buster=1406655766" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="BrandMe -  sales and marketing market research social media marketing brand marketing" />
<h1 property="name">
BrandMe
</h1>
<span href="http://twitter.com/BrandMe" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Sponsorship Marketplace crowdmarketing<br></strong>
BrandMe monetize campaigns on a crowdmarketing platform that connects brands, SME and agencies with an extensive influencers network ranging from students to celebrities in order to boost their campaigns through social networks in an self-service model. BrandMe ...</p>
</a>
</div>

<div data-id="235920" data-name="Fundwise" data-href="http://fundwise.co" class="card startup   id10 id427 id1295 id3010  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://fundwise.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/FundwiseMx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://fundwise.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/235920-d1118f2d67f0e599e17d60571eb64ed4-thumb_jpg.jpg?buster=1421850648" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fundwise -  SaaS social fundraising gamification crowdfunding" />
<h1 property="name">
Fundwise
</h1>
<span href="http://twitter.com/FundwiseMx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We simplify fundraising and compliance for social impact organizations.<br></strong>
68% of donors choose to give online. But 74% of the actual donation systems and sites are not effective, which translates into 47% of the donors giving up.
Fundwise.co is the software that simplifies online fundraising and compliance for social impact organizations ...</p>
</a>
 </div>

<div data-id="235802" data-name="Prestadero" data-href="http://www.prestadero.com" class="card startup   id17 id134 id2795 id124087  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.prestadero.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/Prestadero" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="http://twitter.com/@prestadero" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.prestadero.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/235802-4e4ba3460f517fec09f554e92e4d3d87-thumb_jpg.jpg?buster=1374074951" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Prestadero -  financial services peer-to-peer consumer lending curated web" />
<h1 property="name">
Prestadero
</h1>
<span href="http://twitter.com/@prestadero" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Peer to Peer Lending<br></strong>
Prestadero is the first, legally compliant, fully operational, peer to peer lending site for the Mexican market. Through Prestadero, borrowers can have access to the lowest rates in consumer loans in Mexico by a mile (from 8.9% a year for unsecured loans); on the ...</p>
</a>
</div>

<div data-id="235780" data-name="StellaSoft" data-href="http://www.stellasoft.com.mx" class="card startup   id212 id408 id416 id966  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.stellasoft.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.stellasoft.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/235780-8dc8cec1fd8b90a7493662879f7049ba-thumb_jpg.jpg?buster=1374123721" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="StellaSoft -  rim ios android windows phone 7" />
<h1 property="name">
StellaSoft
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Development of Apps.<br></strong>
StellaSoft provides as variety of services, from consulting to development for Mobile with integration in the Cloud.
Our application is stronger for manning instant messaging with a plus that the other Apps do not have.</p>
</a>
</div>

<div data-id="231225" data-name="Troompet" data-href="http://www.troompet.com" class="card startup   id57 id94 id205 id249  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.troompet.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@troompet" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.troompet.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/231225-c8f2ea3341c18bb6ac085a7cd9336a9e-thumb_jpg.jpg?buster=1373167766" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Troompet -  location based services mobile commerce mobile advertising services" />
<h1 property="name">
Troompet
</h1>
<span href="http://twitter.com/@troompet" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Send a troompet, get many budgets!<br></strong>
This is the Troompet history.
1. In our first venture we visited face-to-face over 1,200 small business offering my web design services.
2. We discovered that these small business wants are contacts, leads, more sales.
3. The internet became complicated, saturated ...</p>
</a>
</div>

<div data-id="230593" data-name="Descifra" data-href="http://www.descifra.mx" class="card startup   id57 id65 id10341 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.descifra.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@Descfira" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.descifra.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/230593-dcf04730f71e4b3a8644873d72cdcb21-thumb_jpg.jpg?buster=1372979964" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Descifra -  location based services information services local businesses big data analytics" />
<h1 property="name">
Descifra
</h1>
<span href="http://twitter.com/@Descfira" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Location-based Analytics Services <br></strong>
Descifra is a Mexican firm that focuses on creating location-based intelligence to analyze and investigate social and economic behaviors, with the objective of explaining the environment of local markets.
The core of our innovation lies in the construction of data ...</p>
</a>
</div>

<div data-id="230181" data-name="Comparateca" data-href="http://www.comparateca.com" class="card startup   id17 id49 id80 id286  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.comparateca.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.comparateca.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/230181-75aacab9b8a5fe3af83f6c349ff795b2-thumb_jpg.jpg?buster=1392585879" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Comparateca -  financial services telecommunications comparison shopping consumer electronics" />
<h1 property="name">
Comparateca
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We help consumers to make informed buying decisions.<br></strong>
Comparateca is the ultimate comparison platform, and we have a very simple goal: to help consumers make informed and wise purchases for products that involve important monetary decisions.
Comparateca collects all the important data, specs and characteristics ...</p>
</a>
</div>

<div data-id="229152" data-name="Cotiza &amp; Contrata" data-href="http://cotizaycontrata.com" class="card startup   id162 id384 id529 id2969  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://cotizaycontrata.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/cotizaycontrata" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://cotizaycontrata.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/229152-3863fb069f65bbc473cfdae302281e15-thumb_jpg.jpg?buster=1372699190" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cotiza &amp; Contrata -  marketplaces professional services local services estimation and quoting" />
<h1 property="name">
Cotiza &amp; Contrata
</h1>
<span href="https://twitter.com/cotizaycontrata" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Save time and money getting local services providers<br></strong>
Cotiza &amp; Contrata is the marketplace where companies or particulars in an easy way contact with professional services provides in order to get even five different quotes saving time and money and hiring the best provider.
For service providers just have to create ...</p>
</a>
</div>

<div data-id="229110" data-name="Seenapse" data-href="http://seenapse.it" class="card startup   id1262  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://seenapse.it" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/SeenapseIt" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://seenapse.it" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/229110-c0e7570241bc1c3f14f6bda0c831f9a4-thumb_jpg.jpg?buster=1386042780" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Seenapse -  creative industries" />
<h1 property="name">
Seenapse
</h1>
<span href="https://twitter.com/SeenapseIt" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Google for creativity<br></strong>
Seenapse is a search engine for creative people who are looking for non-obvious results to spark ideas. Its goal is to help people come up with ideas through mental associations. Everybody does this already on their own —it’s the natural process— but Seenapse will ...</p>
</a>
</div>

<div data-id="227740" data-name="Hostspot" data-href="http://hostspot.mx" class="card startup   id32 id198 id2563 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://hostspot.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@hostspotmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://hostspot.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/227740-97c5b8d05b3203414db603c22533860e-thumb_jpg.jpg?buster=1375149428" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Hostspot -  retail big data offline businesses big data analytics" />
<h1 property="name">
Hostspot
</h1>
<span href="http://twitter.com/@hostspotmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Google Analytics for the offline world<br></strong>
HostSpot is the winner of the 2013 @AngelHack in Silicon Valley. We provide insights that help businesses optimize the performance of their customer engagement, marketing, merchandising and operations. It does for the offline businesses what Google analytics ...</p>
</a>
</div>

<div data-id="227565" data-name="Chromatix" data-href="http://chromatix.co" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://chromatix.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://chromatix.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/227565-597d40508c6364c4a88f8cb464855354-thumb_jpg.jpg?buster=1372269872" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Chromatix - " />
<h1 property="name">
Chromatix
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="227202" data-name="Teknobuilding" data-href="http://www.teknobuilding.com" class="card startup   id263 id340 id355 id10277  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.teknobuilding.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/teknobuilding" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.teknobuilding.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/227202-3ca1463b7460b3595e2dfd4933de2be3-thumb_jpg.jpg?buster=1397655774" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Teknobuilding -  restaurants small and medium businesses hotels office space" />
<h1 property="name">
Teknobuilding
</h1>
<span href="http://www.twitter.com/teknobuilding" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Integrando tecnología a tu vida<br></strong>
</p>
</a>
</div>

<div data-id="227072" data-name="TROQUER" data-href="http://www.troquer.mx" class="card startup   id29 id32 id93 id552  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.troquer.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.troquer.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/227072-4d0a86ed3d55faddb33b9f9dc479e54b-thumb_jpg.jpg?buster=1372186091" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="TROQUER -  e-commerce retail fashion blogging platforms" />
<h1 property="name">
TROQUER
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Curated Designer Clothing Re-Sale E-Platform (e.g. Vestiaire Collective)<br></strong>
Troquer is an electronic platform that lets women exchange their designer clothes from other women, or buy the ones left on consignment. Our curatorial style and marketing skills will garantee the pieces available are stunning and very fashionable, yet at affordable ...</p>
</a>
</div>

<div data-id="226991" data-name="Carrot" data-href="http://www.carrot.mx" class="card startup   id26 id1948 id12731  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.carrot.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.carrot.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/226991-43d124f52546d4df023789e49fc63302-thumb_jpg.jpg?buster=1372177475" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Carrot -  clean technology sustainability mobility" />
<h1 property="name">
Carrot
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Carsharing in Mexico<br></strong>
We provide urban dwellers the possibility of accessing a car on demand without the need of owning one. It´s like having a car without the high costs and hazzles of ownership.
Cars of various sizes are kept in self-access pods around the city. Customers reserve ...</p>
</a>
</div>

<div data-id="226476" data-name="Uhma Salud" data-href="http://www.uhmasalud.com" class="card startup   id641 id952 id2806  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.uhmasalud.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/UhmaSalud" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.uhmasalud.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/226476-7c8f28a9d486d9bc8ecbb13de52a5864-thumb_jpg.jpg?buster=1372091358" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Uhma Salud -  human resources corporate wellness corporate training" />
<h1 property="name">
Uhma Salud
</h1>
<span href="https://twitter.com/UhmaSalud" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Effective Workplace Wellness Programs<br></strong>
Uhma provides Workplace Wellness Programs. Employees compete in teams to improve their health and win prizes. Our proprietary Web App allows participants to answer health surveys, receive personalized recommendations,
and book their biometric assessment at their ...</p>
</a>
</div>

<div data-id="225744" data-name="G O N T A G" data-href="http://www.GONTAG.com" class="card startup   id26 id27 id69 id2525  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.GONTAG.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@GONTAGCo" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.GONTAG.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/225744-d943726b35af0deafe95868d70e2afa2-thumb_jpg.jpg?buster=1373702007" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="G O N T A G -  clean technology clean energy automotive environmental innovation" />
<h1 property="name">
G O N T A G
</h1>
<span href="http://twitter.com/@GONTAGCo" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Apple for cars<br></strong>
GONTAG is currently working on its first product: the Luft LTS (Limitless Transportation System).
The Luft LTS is an electric/pneumatic in-wheel engine that self-recharges on the go.</p>
</a>
</div>

<div data-id="224937" data-name="Tu Ola" data-href="http://www.tuola.mx" class="card startup   id34 id224 id334 id2525  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.tuola.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.tuola.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/224937-0f8d20d9d612789866af087e0edb577e-thumb_jpg.jpg?buster=1371666987" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tu Ola -  advertising loyalty programs green environmental innovation" />
<h1 property="name">
Tu Ola
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Rewards program for engaging in green actions<br></strong>
Tu Ola is the first online platform in Mexico that rewards users for participating in positive actions like bike sharing, recycling, car sharing and reforestation.
It´s free!</p>
</a>
</div>

<div data-id="224857" data-name="Telus. Inteligencia Geopolítica" data-href="http://www.telus.mx" class="card startup   id251 id373 id2992 id13197  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.telus.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/TelusMX" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.telus.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/224857-857ba2b875a94e61bf7782f70dc8d1a7-thumb_jpg.jpg?buster=1371655070" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Telus. Inteligencia Geopolítica -  governments politics geospatial big data analytics" />
<h1 property="name">
Telus. Inteligencia Geopolítica
</h1>
<span href="http://twitter.com/TelusMX" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Geopolitical Intelligence<br></strong>
We are a political consulting firm that uses big data to do analysis. Everything we do is based on Geographic Information Systems, we work on own GIS platform. We have 3 main products: GeoVoto (for campaigns), GeoGobs (for goverments) and GeoSocial (for social ...</p>
</a>
</div>

<div data-id="224733" data-name="Anipass" data-href="http://anipass.jp" class="card startup   id912  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://anipass.jp" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/AnipassJp" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://anipass.jp" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/224733-872efe2b9c61582dddc43b765c0c81b5-thumb_jpg.jpg?buster=1371618269" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Anipass -  teenagers" />
<h1 property="name">
Anipass
</h1>
<span href="https://twitter.com/AnipassJp" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Netflix for Anime content based in LATAM<br></strong>
Anime Streaming for LATAM. The service will be free for the Anime catalog already done, and by payment for the Anime currently airing on Japan (Simulcast). Our customers are kids from 13 to 24 years old who loves Anime, Manga and all the things related with Japanese ...</p>
</a>
</div>

<div data-id="224623" data-name="Click Mexico" data-href="http://institute.vc/" class="card startup   id4 id29 id494 id9259  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://institute.vc/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://institute.vc/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/224623-dbac6e3f04dbd65ba9d8c36051f9458b-thumb_jpg.jpg?buster=1371601019" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Click Mexico -  digital media e-commerce internet service providers franchises" />
<h1 property="name">
Click Mexico
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mexican internet café retail and services network<br></strong>
Click provides microfranchise opportunities to independent internet cafés, gives better and cheaper internet access to the millions of underserved Mexicans, and works with digital and retail partners, as well as government initiatives, to give those millions of ...</p>
</a>
</div>

<div data-id="223912" data-name="Club Guaf" data-href="http://clubguaf.com/" class="card startup   id29 id31 id675 id1103  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://clubguaf.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@Clubguaf" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://clubguaf.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/223912-ae1a4deeb02a0bee5fedea7c3076a4ae-thumb_jpg.jpg?buster=1378232339" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Club Guaf -  e-commerce consumer goods pets health and wellness" />
<h1 property="name">
Club Guaf
</h1>
<span href="http://twitter.com/@Clubguaf" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> High quality dog products delivered to your house each month.<br></strong>
Club Guaf is a revenue generating e-commerce for pet products in Mexico. We focus on good quality products which are hard to come by and great customer service. We also have a recurring and reminding service so that you never forget to buy your pet's food again.</p>
</a>
</div>

<div data-id="223712" data-name="Sentisis" data-href="http://www.sentisis.com" class="card startup   id6 id52 id242 id450  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
 <div class="count">123</div>
</button>
<div class="social">

<a href="http://www.sentisis.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@_sentisis" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.sentisis.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/223712-db0ea622557c0b3eb470f1882257d088-thumb_jpg.jpg?buster=1371459979" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sentisis -  social media analytics social media monitoring social media marketing" />
<h1 property="name">
Sentisis
</h1>
<span href="http://twitter.com/@_sentisis" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social Media Analytics for the Spanish-speaking market<br></strong>
Sentisis is the qualitative Social Media analytics platform for the Spanish-speaking market. Instead of just monitoring the conversation, our products allow to extract valuable conclusions such as a precise opinion mining and topic extraction, identifying influential ...</p>
</a>
</div>

<div data-id="222833" data-name="Fondeadora" data-href="http://www.fondeadora.mx" class="card startup   id246 id427 id1262 id3010  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.fondeadora.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/FondeadoraMx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.fondeadora.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/222833-57bd2ef416b25d4e9fb5d1b339fd5f3e-thumb_jpg.jpg?buster=1398048085" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Fondeadora -  crowdsourcing social fundraising creative industries crowdfunding" />
<h1 property="name">
Fondeadora
</h1>
<span href="https://twitter.com/FondeadoraMx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The first and leading crowdfunding platform in Mexico<br></strong>
Fondeadora is the first and leading crowdfunding platform in Mexico. We bring crowdfunding as a funding tool for creatives in general by executing strong online-offline marketing strategies that spread and consolidate the model in the latin american market.
Because ...</p>
</a>
</div>

<div data-id="221799" data-name="Rides" data-href="http://www.rides.com.mx" class="card startup   id631 id1161  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rides.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/ridesmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rides.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/221799-6262eaf46cd061620d42d1a75169d620-thumb_jpg.jpg?buster=1370971129" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Rides -  transportation travel" />
<h1 property="name">
Rides
</h1>
<span href="https://twitter.com/ridesmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The Airbnb for your seats <br></strong>
We offer a platform to offer/find and pay for rides to travel from one city to another. Users have a way to save money in their trips, while traveling more comfortably and getting to know new people.</p>
</a>
</div>

<div data-id="219737" data-name="Schoolcontrol" data-href="http://www.schoolcontrol.com" class="card startup   id3 id43 id1295 id1469  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.schoolcontrol.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.schoolcontrol.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/219737-691f8ff2bba3d1726ddcaac69c081232-thumb_jpg.jpg?buster=1370478480" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Schoolcontrol -  mobile education gamification k 12 education" />
<h1 property="name">
Schoolcontrol
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> School Communication Platform<br></strong>
SchoolControl is a communication platform through which teachers, students and parents can find school information, such as: grades, announcements, calendars, events, reports, tasks, homework and details about the academic performance of the student.
They can ...</p>
</a>
</div>

<div data-id="218708" data-name="Scorebox" data-href="http://scorebox.co" class="card startup   id3 id10 id29 id73  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://scorebox.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://scorebox.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/218708-c9b5d376f531b22ffa95ee942f31ad52-thumb_jpg.jpg?buster=1390416134" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Scorebox -  mobile SaaS e-commerce sports" />
<h1 property="name">
Scorebox
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Sports tournaments made easy<br></strong>
Scorebox eases the management of sports tournaments by improving the connectivity between the administrator and players through a dynamic platform. No more Excel spreedsheets to control the tournament, you can create a call for your tournament, do the fixes in ...</p>
</a>
</div>

<div data-id="214694" data-name="Pachuco Digital" data-href="http://pachucodigital.com" class="card startup   id450 id686  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://pachucodigital.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/pachucodigital" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://pachucodigital.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/214694-4b835259adf48f81d5fd45af6713eb43-thumb_jpg.jpg?buster=1369979382" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pachuco Digital -  social media marketing brand marketing" />
<h1 property="name">
Pachuco Digital
</h1>
<span href="http://twitter.com/pachucodigital" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Digital marketing strategies<br></strong>
</p>
</a>
</div>

<div data-id="213978" data-name="SoloStocks" data-href="http://www.solostocks.com" class="card startup   id29 id162 id340 id1122  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.solostocks.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/SoloStocks‎" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.solostocks.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/213978-c577e9a8c6afc623506e9b54968e7a1f-thumb_jpg.jpg?buster=1380560179" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SoloStocks -  e-commerce marketplaces small and medium businesses wholesale" />
<h1 property="name">
SoloStocks
</h1>
<span href="https://twitter.com/SoloStocks‎" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Better Alibaba for Latin America<br></strong>
A localized Alibaba serving small businesses in Spain and Latin America (Mexico, Brazil, Argentina, others).
Users: 2.5+ million
Matches: 100,000 / month
Catalog: 1+ million products
SoloStocks focuses on businesses with &lt;50 people where the CEO/Purchasing manager ...</p>
</a>
</div>

<div data-id="212728" data-name="TodoTrips" data-href="http://www.todotrips.com" class="card startup   id78 id230 id1076 id1161  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.todotrips.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.todotrips.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/212728-3b9df812f68c6bee6e652bfde48fb139-thumb_jpg.jpg?buster=1369628963" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="TodoTrips -  online travel social commerce social travel travel" />
<h1 property="name">
TodoTrips
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social Travel Platform. Earn digital currency on every purchase. Donate to a local NGO.<br></strong>
Social Travel and Commerce platform that reduces the hotel room rates on every purchase. At the end of each auction or Gangas the discount will be reimbursed in TodoCash: digital currency to be used on next purchases. The more people buy, the more you save!
Choose ...</p>
</a>
</div>

<div data-id="212029" data-name="Quantmogul" data-href="http://quantmogul.com" class="card startup   id17 id162 id1406 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://quantmogul.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/quantmogul" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://quantmogul.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/212029-c6ce59512ba6eeb77ec091c9ee15760d-thumb_jpg.jpg?buster=1369367414" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Quantmogul -  financial services marketplaces trading finance technology" />
<h1 property="name">
Quantmogul
</h1>
<span href="http://twitter.com/quantmogul" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> An investment ideas marketplace, with quantitative finance tools for investors<br></strong>
An investment ideas marketplace, with quantitative finance tools for investors.
Quantmogul is a professional environment to let individual investors create quantitative strategies by using a state of the art scientific cluster, that is able to analyze data from ...</p>
</a>
</div>

<div data-id="211844" data-name="Cine+" data-href="http://cinemasapp.co" class="card startup   id85 id94 id604 id2812  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://cinemasapp.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/cinemasapp" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://cinemasapp.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/211844-e3733867c48c9982093cb33e7ed1f84b-thumb_jpg.jpg?buster=1380237298" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cine+ -  film mobile commerce ticketing entertainment industry" />
<h1 property="name">
Cine+
</h1>
<span href="https://twitter.com/cinemasapp" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The fastest way to buy tickets and discover movies with your phone<br></strong>
We are a new mobile platform for discovering movies and buying movie tickets in México. Right now, we can sell tickets for 90% of cinemas in the country and are working with movie distributors to reach advertisement agreements for new movies.</p>
</a>
</div>

<div data-id="209194" data-name="Sm4rt Security Services" data-href="http://www.sm4rt.com" class="card startup   id253  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.sm4rt.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.sm4rt.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/209194-097475c91aa3abab46015fb0134642f0-thumb_jpg.jpg?buster=1368733750" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Sm4rt Security Services -  consulting" />
<h1 property="name">
Sm4rt Security Services
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> IT Security Services for Enterprises<br></strong>
</p>
</a>
</div>

<div data-id="204567" data-name="Reddoc" data-href="http://www.reddoc.mx" class="card startup   id223 id1258 id1567  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.reddoc.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/ReddocMX" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.reddoc.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/204567-848e212a72dd53207e34d504ca40a8c3-thumb_jpg.jpg?buster=1367756553" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Reddoc -  doctors mobile health online scheduling" />
<h1 property="name">
Reddoc
</h1>
<span href="https://twitter.com/ReddocMX" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The Health Care Portal for LatAm<br></strong>
Reddoc is a complete appointment management platform for scheduling appointments with doctors and dentists instantly. It offers increased visibility and more patients for healthcare professionals. Reddoc was founded by two young entrepreneurs with the mission of ...</p>
</a>
</div>

<div data-id="198831" data-name="Lokuaz.com" data-href="http://www.lokuaz.com/" class="card startup   id64 id13227  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.lokuaz.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/lokuaz_com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.lokuaz.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/198831-a11f9ff3c43ece258e886298f8cf323e-thumb_jpg.jpg?buster=1366687367" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Lokuaz.com -  gambling low bid auctions" />
<h1 property="name">
Lokuaz.com
</h1>
<span href="https://twitter.com/lokuaz_com" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="196034" data-name="El Bazar Mx" data-href="http://www.elbazar.mx" class="card startup   id29 id162 id230 id81663  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.elbazar.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/elbazar_mx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.elbazar.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/196034-78b9d654bff6d77ea9588df934cf74ba-thumb_jpg.jpg?buster=1366080071" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="El Bazar Mx -  e-commerce marketplaces social commerce handmade" />
<h1 property="name">
El Bazar Mx
</h1>
<span href="http://www.twitter.com/elbazar_mx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Latin Crafts and Handmade mobile e-marketplace<br></strong>
El Bazar MX, Web and mobile app for digital distribution, marketing and e-commerce, to help artisans and independent designers publish &amp; promote their ideas and creations either to sell them or get crowdfunding resources, we want artisans to focus on what they ...</p>
</a>
</div>

<div data-id="190972" data-name="Petsy" data-href="http://www.petsy.mx" class="card startup   id29 id675 id2962 id3131  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.petsy.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@petsymx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.petsy.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/190972-a31150e710b62f0666c8bf938beaac88-thumb_jpg.jpg?buster=1371248574" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Petsy -  e-commerce pets veterinary animal feed" />
<h1 property="name">
Petsy
</h1>
<span href="http://twitter.com/@petsymx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mexico's premier petcare products e-tailer<br></strong>
Petsy aims to be Mexico's premier petcare products e-tailer. We make pet owners' lives a little simpler, one deliver at a time.
** For more information, check out our teaser --&gt; investors.petsy.mx **</p>
</a>
</div>

<div data-id="184592" data-name="Tok3n" data-href="http://tok3n.com" class="card startup   id3 id62 id171 id3127  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://tok3n.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://tok3n.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/184592-e19ddf57a04045e693117255771a1150-thumb_jpg.jpg?buster=1363663370" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tok3n -  mobile security enterprise security web development" />
<h1 property="name">
Tok3n
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Two-step-authentication for developers<br></strong>
Tok3n is an affordable two-step-authentication API for developers, and easy to use mobile app for users. Once the user connects with Tok3n, will be asked for his tok3n every time he logs in or for any other action the developer requests. The user uses the same ...</p>
</a>
</div>

<div data-id="181117" data-name="Regresa Pronto " data-href="http://www.regresapronto.com" class="card startup   id224 id379 id710  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.regresapronto.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@regresapronto" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.regresapronto.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/181117-8237c1fc5d0cb5ffcaa56cd6980c456e-thumb_jpg.jpg?buster=1378337552" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Regresa Pronto  -  loyalty programs retail technology customer service" />
<h1 property="name">
Regresa Pronto
</h1>
<span href="http://twitter.com/@regresapronto" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The most effective customer experience management system in LATAM<br></strong>
Test, control and improve in-store customer satisfaction, enhance loyalty and sales, easy for the customers, effective for the companies.
We install tablets directly on the establishment, allowing customers to provide feedback in seconds directly form the point ...</p>
</a>
</div>

<div data-id="179572" data-name="Plasmr" data-href="http://www.plasmr.org" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.plasmr.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/plasmr" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.plasmr.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/179572-fd3a8972247c5f380068998006f9eaa3-thumb_jpg.jpg?buster=1382023138" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Plasmr - " />
<h1 property="name">
Plasmr
</h1>
<span href="http://www.twitter.com/plasmr" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Social network for blood donation<br></strong>
</p>
</a>
</div>

<div data-id="178455" data-name="Saricy" data-href="http://www.saricy.com" class="card startup   id93 id1545  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.saricy.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.saricy.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/178455-d6e76247270d8b57bc9113c1bc47bfe4-thumb_jpg.jpg?buster=1362509246" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Saricy -  fashion logistics" />
<h1 property="name">
Saricy
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The link between retailers and apparel manufacturers<br></strong>
We provide apparel sourcing services to retailers in Latin America, Africa and Europe</p>
</a>
</div>

<div data-id="174015" data-name="joopsit" data-href="http://joopsit.com" class="card startup   id431  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://joopsit.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://joopsit.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/174015-e1e520d05b352015034b0deac79dc586-thumb_jpg.jpg?buster=1361813824" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="joopsit -  social recruiting" />
<h1 property="name">
joopsit
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> IT social job board.<br></strong>
IT social job board - Clique-based Social Networking</p>
</a>
</div>

<div data-id="173678" data-name="Integra Arrenda" data-href="http://www.financiera-integra.com.mx" class="card startup   id69 id321 id2795  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.financiera-integra.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.financiera-integra.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/173678-954a4f2083aab83eed131d51d997a28d-thumb_jpg.jpg?buster=1361766048" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Integra Arrenda -  automotive finance consumer lending" />
<h1 property="name">
Integra Arrenda
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="171726" data-name="ChipotleLabs" data-href="http://www.chipotlelabs.com" class="card startup   id3 id277 id360 id408  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.chipotlelabs.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.chipotlelabs.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/171726-189bdeccd1e857f4585c1462d4cb712d-thumb_jpg.jpg?buster=1361393405" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="ChipotleLabs -  mobile iphone ipad ios" />
<h1 property="name">
ChipotleLabs
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Apps to spice up your life<br></strong>
Instaplayer: A universal free iOS Instagram client. It let's users create a personalized animated wall of Instagram photos with hahstags, user names and Foursquare locations. Targeted for Instagram users.
Ninja Factor: A universal free &amp; paid iOS educational ...</p>
</a>
</div>

<div data-id="170659" data-name="Wishbird Experiences" data-href="http://www.wishbird.com.mx" class="card startup   id1458 id10483 id16414  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.wishbird.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://www.facebook.com/wishbird" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>
<a href="https://twitter.com/WishbirdMexico" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.wishbird.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/170659-c1d184355f368285a9d7e3f24fae2b36-thumb_jpg.jpg?buster=1361234258" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Wishbird Experiences -  leisure gift card active lifestyle" />
<h1 property="name">
Wishbird Experiences
</h1>
<span href="https://twitter.com/WishbirdMexico" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online experience gift company.<br></strong>
Wishbird is the place to book and gift experiences online in Mexico. We offer the booking of over 250 activities, like diving, hot air balloon flights, exclusive Spas and a lot more directly online, working together with top experience providers and multiple distribution ...</p>
</a>
</div>

<div data-id="167727" data-name="WFH" data-href="" class="card startup   id601 id641 id9847 id9848  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">


</div>
</div>
<a class="main_link" href="" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://angel.co/images/shared/nopic_startup.png" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="WFH -  virtual workforces human resources freelancers employment" />
<h1 property="name">
WFH
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
 <strong> Work from home<br></strong>
WFH will let you post your resume and work from home.
It's a website that will help thousands of people find projects to get involved.
The idea is to generate jobs in the country.
Since WFH will be a website it will help people from other countries reach to mexican ...</p>
</a>
</div>

<div data-id="161470" data-name="DameUnAventon" data-href="http://www.dameunaventon.com.mx/" class="card startup   id631  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.dameunaventon.com.mx/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/dameunaventonmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.dameunaventon.com.mx/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/161470-99602aa767d5ad736ff1aed45ab979f2-thumb_jpg.jpg?buster=1359561764" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="DameUnAventon -  transportation" />
<h1 property="name">
DameUnAventon
</h1>
<span href="http://twitter.com/dameunaventonmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> The first public carpooling platform in Mexico<br></strong>
DameUnAventon is the first public carpooling platform in Mexico. Using our web platform or Android mobile app, users can create and share a ride nationwide. Our web platform can be used for both short and long distance trips while our app is meant to be used for ...</p>
</a>
</div>

<div data-id="158407" data-name="headhunters.mx" data-href="http://www.headhunters.mx" class="card startup   id431 id468 id641  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.headhunters.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/headhunters_mx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.headhunters.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/158407-7c95dfa04a43401f8fba558d169cb40f-thumb_jpg.jpg?buster=1358776202" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="headhunters.mx -  social recruiting recruiting human resources" />
<h1 property="name">
headhunters.mx
</h1>
<span href="https://twitter.com/headhunters_mx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mexico´s premium career and recruitment marketplace.<br></strong>
Highly-qualified executive candidates network with validated and rated profiles (experience and skills) measuring their integrity and connecting to an exclusive job market offering, the largest and highest-quality selection of executive jobs across Mexico.
70% ...</p>
</a>
</div>

<div data-id="157099" data-name="Yourbanice" data-href="http://www.qritiqr.com" class="card startup    ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.qritiqr.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/qritiqr" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.qritiqr.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/157099-a5586b1cd664535ac2fe7346366692d5-thumb_jpg.jpg?buster=1360265249" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Yourbanice - " />
<h1 property="name">
Yourbanice
</h1>
<span href="http://twitter.com/qritiqr" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> simplified survey monkey for physical world <br></strong>
We generate simple satisfaction surveys in minutes for monitoring satisfaction of customers / clients so business can take customer-based decisions quickly. We have mobile web apps and also native apps for tablets. We serve as CRM tool so business can close loops, ...</p>
</a>
</div>

<div data-id="156917" data-name="CreaDescuentos" data-href="http://www.creadescuentos.com" class="card startup   id424 id441 id8626  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.creadescuentos.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/creadescuentos" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.creadescuentos.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/156917-54bf786a1761998a465f6f77e6ada0b9-thumb_jpg.jpg?buster=1377546275" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CreaDescuentos -  deals discounts promotional" />
<h1 property="name">
CreaDescuentos
</h1>
<span href="http://twitter.com/creadescuentos" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Best Deal News in Mexico<br></strong>
</p>
</a>
</div>

<div data-id="152910" data-name="MoneyMenttor" data-href="http://www.moneymenttor.com" class="card startup   id374  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.moneymenttor.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@MoneyMenttor" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.moneymenttor.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/152910-e339efcc250921368933806b6b99bcc9-thumb_jpg.jpg?buster=1357320379" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="MoneyMenttor -  personal finance" />
<h1 property="name">
MoneyMenttor
</h1>
<span href="http://twitter.com/@MoneyMenttor" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Your personal financial advisor.<br></strong>
</p>
</a>
</div>

<div data-id="149996" data-name="Axeleratum" data-href="http://www.axeleratum.com" class="card startup   id340  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.axeleratum.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@axeleratum" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.axeleratum.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/149996-2dd7e93fa9418f81278427189cd27ea9-thumb_jpg.jpg?buster=1357518553" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Axeleratum -  small and medium businesses" />
<h1 property="name">
Axeleratum
</h1>
<span href="http://twitter.com/@axeleratum" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="145767" data-name="DadaRoom" data-href="http://www.dadaroom.com" class="card startup   id16 id162 id584 id2811  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.dadaroom.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/dadaroom" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.dadaroom.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/145767-a69ad7d2ae8a3940a294c0dec2e3c100-thumb_jpg.jpg?buster=1394061162" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="DadaRoom -  real estate marketplaces shared services classifieds" />
<h1 property="name">
DadaRoom
</h1>
<span href="http://twitter.com/dadaroom" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Roommates matching in Latin America<br></strong>
www.dadaroom.com
Launched in January 2013 and based in Mexico City, DadaRoom.com is Mexico's fastest growing service for shared housing. We are a trusted community marketplace where users list rooms for rent and find the right roommates to live with. We provide ...</p>
</a>
</div>

<div data-id="144038" data-name="Pingstamp" data-href="http://www.pingstamp.mx" class="card startup   id34 id263 id340 id615  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.pingstamp.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.pingstamp.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/144038-36643cf1c62b60daf1985592df23a44f-thumb_jpg.jpg?buster=1354676008" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Pingstamp -  advertising restaurants small and medium businesses tablets" />
<h1 property="name">
Pingstamp
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Making loyalty easy and fun.<br></strong>
PingStamp is an easy to use customer loyalty platform that allows SMEs to generate customer behaviour insights to improve their customer relation strategies.
We provide real time metrics while generating business insights for marketing and CRM strategies in local ...</p>
</a>
</div>

<div data-id="143911" data-name="String Publisher" data-href="http://www.stringpublisher.com/" class="card startup   id87 id615  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.stringpublisher.com/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/stringpublisher" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.stringpublisher.com/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/143911-f9b8637a5f0e484edc2e34cf225a8d58-thumb_jpg.jpg?buster=1362860744" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="String Publisher -  publishing tablets" />
<h1 property="name">
String Publisher
</h1>
<span href="http://twitter.com/stringpublisher" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Drag and drop iPad publishing<br></strong>
The easiest way to create native publications for the iPad on your web browser, no desktop software required. Just drag and drop your content. Add video, audio, image galleries and more with just a few clicks, and without writing a single line of code.
One click ...</p>
</a>
</div>

<div data-id="139697" data-name="Garage Coders" data-href="http://www.garagecoders.net" class="card startup   id277 id841 id2965 id3041  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.garagecoders.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/GarageCoders" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.garagecoders.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
 <img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/139697-c789ab6748975a7437aba30297b0c212-thumb_jpg.jpg?buster=1373498067" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Garage Coders -  iphone software new product development mobile devices" />
<h1 property="name">
Garage Coders
</h1>
<span href="https://twitter.com/GarageCoders" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="139682" data-name="POP Contacts" data-href="http://www.popcontacts.com" class="card startup   id3 id408 id416 id966  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.popcontacts.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/POPContacts" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.popcontacts.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/139682-bbd2aa93e0e179216106f06a4ef07af5-thumb_jpg.jpg?buster=1353541582" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="POP Contacts -  mobile ios android windows phone 7" />
<h1 property="name">
POP Contacts
</h1>
<span href="http://twitter.com/POPContacts" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Relating people across all points of contact in one place. (iOS)<br></strong>
&quot;POP Contacts&quot; is an iOS App that simplifies the search for contacts by combining results from the web, your address book and your interaction in social networks, avoiding information overload, making these results content and context-aware.
We automate and make ...</p>
</a>
</div>

<div data-id="139209" data-name="PagoFacil" data-href="" class="card startup   id71 id427 id473  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">


</div>
</div>
<a class="main_link" href="" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/139209-b8c27f10a1ec2bc9d90853e994092982-thumb_jpg.jpg?buster=1355265454" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="PagoFacil -  payments social fundraising mobile payments" />
<h1 property="name">
PagoFacil
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="136796" data-name="Citizen.im" data-href="http://www.citizen.im" class="card startup   id251 id324  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.citizen.im" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.citizen.im" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/136796-5e492053b3870a1438774ed44f821b43-thumb_jpg.jpg?buster=1352742747" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Citizen.im -  governments nonprofits" />
<h1 property="name">
Citizen.im
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Connecting Government, NGO's and Citizens to make a better place to live<br></strong>
We build tools to make citizens aware of their active participation on the improvement of the community and giving them and government channels where they can build a trusted and efficient communication without the risks of corruption and with all the transparency ...</p>
</a>
</div>

<div data-id="135352" data-name="cliCab" data-href="http://www.clicab.com" class="card startup   id3 id1056 id1097 id1116  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.clicab.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/clicab.com" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.clicab.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/135352-cc7c68a38c4652516209e288bd12a485-thumb_jpg.jpg?buster=1352244181" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="cliCab -  mobile technology taxis public transportation" />
<h1 property="name">
cliCab
</h1>
<span href="http://twitter.com/clicab.com" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Safe Taxi Everywhere<br></strong>
Clicab will dispatch safe taxis to smartphones users in Mexico City. CliCab owns the technology for the product to liaise between customers and safe taxis.</p>
</a>
</div>

<div data-id="134846" data-name="Rows" data-href="http://www.rows.co" class="card startup   id246 id269 id302  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rows.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/rowsapp" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rows.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://angel.co/images/shared/nopic_startup.png" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Rows -  crowdsourcing social media platforms developer apis" />
<h1 property="name">
Rows
</h1>
<span href="http://twitter.com/rowsapp" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> A global collaboration platform<br></strong>
</p>
</a>
</div>

<div data-id="133160" data-name="WorlDxRadio" data-href="http://www.rock101.com" class="card startup   id3114  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.rock101.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.rock101.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/133160-5cef36db62fedc184ccf0fd7c7e4165f-thumb_jpg.jpg?buster=1351605695" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="WorlDxRadio -  internet radio market" />
<h1 property="name">
WorlDxRadio
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> 90 <br></strong>
WorlDxRadio is a company willing to provide the best music to all our listeners</p>
</a>
</div>

<div data-id="132027" data-name="Incuvox, SA de CV" data-href="http://incuvox.com" class="card startup   id49 id103 id901 id1606  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://incuvox.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/incuvox" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://incuvox.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/132027-c1b474e94bdd36b213454ab299a83a98-thumb_jpg.jpg?buster=1351117828" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Incuvox, SA de CV -  telecommunications telephony voip unifed communications" />
<h1 property="name">
Incuvox, SA de CV
</h1>
<span href="http://twitter.com/incuvox" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Don’t call us, We’ll call you.<br></strong>
Lorem ipsum dolor sit amet, consectetur adipiscing elit. Maecenas elementum posuere eleifend. Nunc nisl mi, viverra at lobortis in, aliquet et dui. Phasellus turpis nibh, semper eu laoreet ac, ultrices sit amet odio. Etiam consectetur metus eget tortor aliquet ...</p>
</a>
</div>

<div data-id="129616" data-name="Truekeo" data-href="http://www.truekeo.com" class="card startup   id6 id230 id269  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.truekeo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.truekeo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/129616-c4b1d282528b59b60b5886ceb9e565bb-thumb_jpg.jpg?buster=1350164526" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Truekeo -  social media social commerce social media platforms" />
<h1 property="name">
Truekeo
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Barter Page<br></strong>
What is truekeo.com?
Truekeo.com is a social website designed to swap / exchange anything!.
Truekeo.com was born from the idea that people have a free platform to exchange their goods, services and knowledge through the web.
Truekeo.com Vision:
Use e-trade as ...</p>
</a>
</div>

<div data-id="123972" data-name="PhonePlus" data-href="http://www.garagecoders.net" class="card startup   id3 id277 id408 id416  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.garagecoders.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.facebook.com/PhonePlusApp" rel="nofollow" target="_blank"><i class="fa fa-facebook-square"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.garagecoders.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/123972-abbb5be792edd77cbe5f5ce8b3f25e5f-thumb_jpg.jpg?buster=1348592563" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="PhonePlus -  mobile iphone ios android" />
<h1 property="name">
PhonePlus
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Your contact list on steroids<br></strong>
Phone+ integrates in a simple way your social networks, gathers all your contacts info, allowing you to make calls, send emails, see their recent activities and interact with them on Facebook And twitter. All in one screen!
Access to your favorites updates in ...</p>
</a>
</div>

<div data-id="122936" data-name="Nokincard" data-href="http://www.nokincard.com" class="card startup   id205 id224 id340 id1295  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.nokincard.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@Nokincard" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.nokincard.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/122936-0180958ac805013a1c0370c77a320a37-thumb_jpg.jpg?buster=1377036662" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Nokincard -  mobile advertising loyalty programs small and medium businesses gamification" />
<h1 property="name">
Nokincard
</h1>
<span href="http://twitter.com/@Nokincard" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Loyalty &amp; Social Platform for Latin-American SMB's<br></strong>
Nokincard is a consumer-facing loyalty platform and a merchant-facing marketing machine. For consumers,Nokincard eliminates the need to carry multiple punch cards with a seamless digital check-in to access rewards and perks. Using Nokincard merchants now have an ...</p>
</a>
</div>

<div data-id="122353" data-name="CX-Corp" data-href="http://www.cx-corp.com" class="card startup   id3 id1132 id3127  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.cx-corp.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.cx-corp.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/122353-bad92426ec8bbb8c23cfafe63049aa7d-thumb_jpg.jpg?buster=1347597777" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CX-Corp -  mobile developer tools web development" />
<h1 property="name">
CX-Corp
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
</p>
</a>
</div>

<div data-id="121887" data-name="Bebitos" data-href="http://www.bebitos.mx" class="card startup   id29 id837  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.bebitos.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@bebitosmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.bebitos.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/121887-3e5b4bc3dabe29efc6f303a91a6e91ae-thumb_jpg.jpg?buster=1347487686" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Bebitos -  e-commerce babies" />
<h1 property="name">
Bebitos
</h1>
<span href="http://twitter.com/@bebitosmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
 <i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> A baby ecommerce, akin to diapers.com, for the Mexican Market.<br></strong>
Bebitos sells the best baby-related products and offers a huge selection to our customers. Currently, have more than 3000+ SKUS and growing rapidly. These range from diapers and formula to clothing, cribs and strollers. The company’s philosophy is to “Make moms ...</p>
</a>
</div>

<div data-id="121639" data-name="Urban360" data-href="http://www.urban360.com.mx" class="card startup   id4 id201 id205 id87433  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.urban360.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/Urban360News" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.urban360.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/121639-fc556b80f51d617e38ba914eb505302f-thumb_jpg.jpg?buster=1347414114" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Urban360 -  digital media news mobile advertising navigation" />
<h1 property="name">
Urban360
</h1>
<span href="http://twitter.com/Urban360News" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobile Platform for Urban Services<br></strong>
Urban360 is a digital media company that thru an App-based mobile portal provides relevant information &amp; services to make life easier in large cities.
It is designed for mainstream users who get personalized traffic reports, earthquake alerts, public transportation ...</p>
</a>
</div>

<div data-id="121201" data-name="Tuiro" data-href="http://testastore.com/Tuiro/" class="card startup   id6  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://testastore.com/Tuiro/" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://testastore.com/Tuiro/" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/121201-477978e8dc22b4e0df6026531b1d58ad-thumb_jpg.jpg?buster=1347300143" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tuiro -  social media" />
<h1 property="name">
Tuiro
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Another Twitter client<br></strong>
Another Twitter client, develop in MVC .NET, the initiative is create a better client with some characteristics for help to causes for the nature</p>
</a>
</div>

<div data-id="120880" data-name="Modebo" data-href="http://www.modebo.com.mx" class="card startup   id26 id90 id773 id2678  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.modebo.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.modebo.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/120880-1280efdd8871d384988b4ccf47ef7493-thumb_jpg.jpg?buster=1347213040" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Modebo -  clean technology energy efficiency energy management green building" />
<h1 property="name">
Modebo
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Energy efficiency in buildings<br></strong>
Modebo helps you manage the energy consumption and the temperature comfort inside a building. Easy installation wireless monitoring devices and an easy to use online monitoring dashboard.</p>
</a>
</div>

<div data-id="118809" data-name="Yaxi" data-href="http://yaxi.mx" class="card startup   id3 id631 id1097 id1545  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://yaxi.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/YaxiApp" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://yaxi.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/118809-1ee7b8665769214cb0c92be24f1b153f-thumb_jpg.jpg?buster=1348520465" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Yaxi -  mobile transportation taxis logistics" />
<h1 property="name">
Yaxi
</h1>
<span href="https://twitter.com/YaxiApp" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Tap to get a safe ride<br></strong>
Yaxi gets you a verified and safe taxi with two taps.</p>
</a>
</div>

<div data-id="116983" data-name="Capptalog" data-href="http://www.capptalog.com" class="card startup   id3 id10 id340 id376  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.capptalog.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.capptalog.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/116983-730fe1c2847bc7645019a9e6b1ef3ec5-thumb_jpg.jpg?buster=1345755147" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Capptalog -  mobile SaaS small and medium businesses productivity software" />
<h1 property="name">
Capptalog
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Mobilize your catalogs now!<br></strong>
Capptalog allows any sales teams to create dynamic, digital, media enriched catalogs of your products or services through our web platform, for its further display in mobile devices, both cell phones and tablets for the main smartphone platforms.
Capptalog focuses ...</p>
</a>
</div>

<div data-id="116716" data-name="MiDr" data-href="http://www.midr.co" class="card startup   id175  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.midr.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/midrco" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.midr.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/116716-64006d44a9a490c5e76f0e4c9ba33197-thumb_jpg.jpg?buster=1345695717" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="MiDr -  health care information technology" />
<h1 property="name">
MiDr
</h1>
<span href="http://twitter.com/midrco" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
 <strong> Platform for AI diagnosis tool from medical records for latin america<br></strong>
We merge the social networking + emr + mobile + gpu power to improve your medical diangosis and your health.
Imagine we can compare millions of medical diagnostics and prescription history to get the best medication for you in seconds.</p>
</a>
</div>

<div data-id="116218" data-name="Boletia" data-href="http://www.boletia.com" class="card startup   id10 id29 id51 id556  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.boletia.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.boletia.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/116218-578e96e514adf2c69fd43b0cd43d80a6-thumb_jpg.jpg?buster=1345565428" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Boletia -  SaaS e-commerce sales and marketing events" />
<h1 property="name">
Boletia
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Online ticket selling made awesome<br></strong>
Boletia helps event organizers to unleash the potential of online sales without needing technical or online marketing expertise. It does this with an easy-to-use platform and a straightforward process focused on early promotion efforts, beautiful event pages and ...</p>
</a>
</div>

<div data-id="112454" data-name="rubberit" data-href="http://rubberit.co" class="card startup   id13 id1891 id13611  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://rubberit.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/rubber_it" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://rubberit.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/112454-9bbb2bb03a1de3caafe140a8dab98aa2-thumb_jpg.jpg?buster=1404350259" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="rubberit -  health care subscription businesses sex industry" />
<h1 property="name">
rubberit
</h1>
<span href="https://twitter.com/rubber_it" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Condom Subscription Service<br></strong>
Rubberit is a movement focused on curbing the growing number of unplanned pregnancies and the spread of STDs in Mexico/LatAm.
We are changing the paradigm of sexual health &amp; protection, distributing condoms through a web-based platform that offers home-delivery ...</p>
</a>
</div>

<div data-id="112418" data-name="etraining" data-href="http://etraining.mx" class="card startup   id43  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://etraining.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@etrainingmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://etraining.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/112418-8677019b3c8024aa8d991454ade5ba30-thumb_jpg.jpg?buster=1344563128" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="etraining -  education" />
<h1 property="name">
etraining
</h1>
<span href="http://twitter.com/@etrainingmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> e-learning &amp; crowdsourcing platform<br></strong>
etraining is an online learning system offering a wide range of training courses offered by true specialists.
Our great challenge is to change the paradigm of learning, making feasible the change of a model that has or explain things to a model that teaches how ...</p>
</a>
</div>

<div data-id="108165" data-name="Gasofia.com" data-href="http://www.gasofia.com" class="card startup   id3 id10 id32 id51  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.gasofia.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/gasofiacom" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.gasofia.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/108165-61ba5e1949b44d3a474c228858160658-thumb_jpg.jpg?buster=1343242033" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Gasofia.com -  mobile SaaS retail sales and marketing" />
<h1 property="name">
Gasofia.com
</h1>
<span href="http://twitter.com/gasofiacom" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Changing the way to find and purchase GAS<br></strong>
&quot;Simple gas purchase cardless from mobile devices, and the most simple way to control gas expenses in a secure form for SMB who has a fleet and individuals&quot;
Easy authorized payments without the need of cards just a cellphone and management of travel/expense/consumption ...</p>
</a>
</div>

<div data-id="108163" data-name="Paypergeek" data-href="http://www.paypergeek.com" class="card startup   id38 id59 id94 id2947  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.paypergeek.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.paypergeek.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/108163-402415550fe14d523ca3474708695a87-thumb_jpg.jpg?buster=1343240559" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Paypergeek -  cloud computing open source mobile commerce finance technology" />
<h1 property="name">
Paypergeek
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> High Performance Applications Consultancy Sevices<br></strong>
We think on the future of your app, we dont just code and sell. we look after you for the 3 main core features your app needs: simple, fast and reliable.</p>
</a>
</div>

<div data-id="102410" data-name="vistank" data-href="http://www.vistank.com" class="card startup   id45 id340 id363 id1474 id15805  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.vistank.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@vistankcom" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.vistank.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/102410-86005d61ad618e7c434c8ec6a4637ca3-thumb_jpg.jpg?buster=1341512496" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="vistank -  business services small and medium businesses financial exchanges entrepreneur angel investing" />
<h1 property="name">
vistank
</h1>
<span href="http://twitter.com/@vistankcom" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> marketplace for entrepreneurs and investors to exchange ideas &amp; business opportunities<br></strong>
Vistank is a business and investment opportunity marketplace for entrepreneurs, investors and professionals.
You can promote or find businesses worldwide and network with entrepreneurs, investors and startups. Join, connect and start making business happen.
Types ...</p>
</a>
</div>

<div data-id="97401" data-name="EcoLike" data-href="http://www.ecolike.net" class="card startup   id26  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ecolike.net" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/ecolike" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ecolike.net" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/97401-e9c1c0edc0e34cccd74108c44ab214f7-thumb_jpg.jpg?buster=1355949214" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="EcoLike -  clean technology" />
<h1 property="name">
EcoLike
</h1>
<span href="http://www.twitter.com/ecolike" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> One Green action per day it's good for you and your wallet.<br></strong>
EcoLike motivate people to take sustainable actions rewarding them for Sharing in social media. We will incorporate participating in activities, such as cleaning beaches, reforestation, recycling and more.
We will generate a loyalty card for Green actions.</p>
</a>
</div>

<div data-id="94932" data-name="Aventones" data-href="http://www.aventones.com" class="card startup   id631 id1948  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.aventones.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/aventones" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.aventones.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/94932-365afbf9697f0c57fab24a8255583b22-thumb_jpg.jpg?buster=1338828854" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Aventones -  transportation sustainability" />
<h1 property="name">
Aventones
</h1>
<span href="http://twitter.com/aventones" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Aventones creates carpooling culture within organizations<br></strong>
Aventones is a service that enables and promotes carpooling within organizations. We charge our corporate clients an annual fee and they get access to an online software which helps users find relevant matches according to itinerary and preferences, and a communication ...</p>
</a>
</div>

<div data-id="93446" data-name="Venddo" data-href="http://www.venddo.com" class="card startup   id51 id309 id450 id686  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.venddo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://www.twitter.com/venddo" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.venddo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/93446-1d8abb4832050d60e84a12932612391b-thumb_jpg.jpg?buster=1348536996" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Venddo -  sales and marketing sales automation social media marketing brand marketing" />
<h1 property="name">
Venddo
</h1>
<span href="http://www.twitter.com/venddo" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> We find your right audience on Twitter<br></strong>
We do the hard part for you. Our algorithms will show profiles with high interest in your product or service, so you just put yourself in contact with them and even can close the sale on our platform.
Monitoring your brand
Monitoring your competition
Provide technical ...</p>
</a>
</div>

<div data-id="93119" data-name="CITEC ING" data-href="http://www.citec.com.mx" class="card startup   id23 id1598  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.citec.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.citec.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/93119-119b827ef985e3c27be62b2e5b0a9d22-thumb_jpg.jpg?buster=1350259563" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="CITEC ING -  pharmaceuticals bio pharm" />
<h1 property="name">
CITEC ING
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> CITEC ING is a Mexico based firm that provides specialized services to the pharmaceutical industry<br></strong>
</p>
</a>
</div>

<div data-id="91250" data-name="TUYO" data-href="http://www.lomioestuyo.com" class="card startup   id29 id32 id286  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.lomioestuyo.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@osotrava" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.lomioestuyo.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/91250-52e32547a971f244c8b0cb50bbd6ebb1-thumb_jpg.jpg?buster=1337383770" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="TUYO -  e-commerce retail consumer electronics" />
<h1 property="name">
TUYO
</h1>
<span href="http://twitter.com/@osotrava" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Buy &amp; re-sell used consumer electronics online and at our stores in Mexico<br></strong>
TUYO offers consumers a convenient, safe and profitable way to recycle their used electronics in exchange for cash. We serve the people in the lower half of the mexican socioeconomic pyramid by granting them access to liquidity without the need for indebtedness ...</p>
</a>
</div>

<div data-id="90816" data-name="Tesseract Pages" data-href="http://www.tesseractpages.com" class="card startup   id481 id615 id2806  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.tesseractpages.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.tesseractpages.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/90816-17f3f95d5140704758c8c7d48b96866c-thumb_jpg.jpg?buster=1337219092" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Tesseract Pages -  kids tablets corporate training" />
<h1 property="name">
Tesseract Pages
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Cross-platform interactive ebooks.<br></strong>
Tesseract Pages creates cross-platform interactive digital products that integrate the experience of a connected device with the content of a book or a magazine.</p>
</a>
</div>

<div data-id="88811" data-name="SinDelantal" data-href="http://sindelantal.mx" class="card startup   id29 id72 id263  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://sindelantal.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/sindelantalmx" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://sindelantal.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/88811-52eec53ce5c6e97064af8774698af63d-thumb_jpg.jpg?buster=1336493728" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SinDelantal -  e-commerce food and beverages restaurants" />
<h1 property="name">
SinDelantal
</h1>
<span href="http://twitter.com/sindelantalmx" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> E-commerce platform for ordering food online from delivery and takeaway restaurants.<br></strong>
SinDelantal is an e-commerce platform for ordering food online from delivery and takeaway restaurants. With more than 1.300 restaurants, we're leading Spain and Mexico markets.</p>
</a>
</div>

<div data-id="85846" data-name="Go.Venture" data-href="http://www.goventure.co" class="card startup   id856 id15805  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.goventure.co" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@GoVentureMexico" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.goventure.co" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/85846-3de12d997767f70790ca548325f198eb-thumb_jpg.jpg?buster=1336427990" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Go.Venture -  venture capital angel investing" />
<h1 property="name">
Go.Venture
</h1>
<span href="http://twitter.com/@GoVentureMexico" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Seed Capital Investment Club for Mexican Ventures<br></strong>
Mexican Seed Angel Investment Club with a side car fund, funding high potential start-ups that come from formal incubating or acceleration programs, and that have been funded with pre-seed (typically under $50kUSD).
We bring value to the projects we fund providing ...</p>
</a>
</div>

<div data-id="75409" data-name="Yapp" data-href="http://www.yappme.com" class="card startup   id32 id94 id205 id443  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.yappme.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.yappme.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/75409-b6d77d20d91979261edd50ca1b9803c5-thumb_jpg.jpg?buster=1331577123" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Yapp -  retail mobile commerce mobile advertising mobile coupons" />
<h1 property="name">
Yapp
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Global Mobile Retail platform<br></strong>
Yapp is a global mobile retail platform that enables shopping through smartphones. Customers can scan products on any ad, catalogue, shopping wall or the product codes themselves.
Yapp leverages mobile payment, advertising campaigns and loyalty programs to expand ...</p>
</a>
</div>

<div data-id="75386" data-name="Frogtek" data-href="http://www.frogtek.org" class="card startup   id94 id169 id208 id2009  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.frogtek.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.frogtek.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/75386-cbfbd471abf7a85cf45401df7b52b4cc-thumb_jpg.jpg?buster=1351958513" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Frogtek -  mobile commerce business information systems ventures for good cloud data services" />
<h1 property="name">
Frogtek
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Quickbooks for shopkeepers in emerging markets<br></strong>
Frogtek helps small shopkeepers in emerging markets track and control their business. With a tablet and a barcode scanner, they register transactions, get useful metrics and charge credit cards.
Our data fuels a Marketing Analytics tool used by companies like ...</p>
</a>
</div>

<div data-id="73829" data-name="Neurona Digital Hub" data-href="http://www.neuronadigital.org" class="card startup   id686  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.neuronadigital.org" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.neuronadigital.org" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://angel.co/images/shared/nopic_startup.png" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Neurona Digital Hub -  brand marketing" />
<h1 property="name">
Neurona Digital Hub
</h1>
<span href="null" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Recruiting platform for MKT Industry through teaching <br></strong>
</p>
</a>
</div>

<div data-id="66358" data-name="FogoNomada" data-href="http://www.fogonomada.com.mx" class="card startup   id27 id386 id472 id2588  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.fogonomada.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@fogonomada" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.fogonomada.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/66358-a483bde86ef5c1e72fba203734e4926d-thumb_jpg.jpg?buster=1354038390" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="FogoNomada -  clean energy design interior design architecture" />
<h1 property="name">
FogoNomada
</h1>
<span href="http://twitter.com/@fogonomada" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> <br></strong>
Bio-ethanol powered fireplaces for indoor spaces. Great for apartments, coffee tables or simple decoration with fire. Easy to light and clean.</p>
</a>
</div>

<div data-id="66357" data-name="SWStrategists" data-href="http://www.swstrategists.com" class="card startup   id51 id253 id1429  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.swstrategists.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/@swstrategists" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.swstrategists.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/66357-f0edbb0b129504f348df32884cbe4862-thumb_jpg.jpg?buster=1369630019" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="SWStrategists -  sales and marketing consulting digital entertainment" />
<h1 property="name">
SWStrategists
</h1>
<span href="http://twitter.com/@swstrategists" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Building Shareholders Value with Digital Assets<br></strong>
Consulting firm specialized in the attainment of short-term results for the marketing, communications, content curation, mass consumption and telecommunications industries.</p>
</a>
</div>

<div data-id="39951" data-name="iBazar" data-href="http://www.ibazar.com.mx" class="card startup   id29 id2811  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.ibazar.com.mx" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="https://twitter.com/iBazarMX" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.ibazar.com.mx" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/39951-8bde17596a13e7909ba566b83e93e949-thumb_jpg.jpg?buster=1367432464" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="iBazar -  e-commerce classifieds" />
<h1 property="name">
iBazar
</h1>
<span href="https://twitter.com/iBazarMX" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Free classifieds site for Mexico - &quot;Google de los clasificados&quot; en México<br></strong>
Launched in Mexico, iBazar (www.ibazar.com.mx) is a free classified site that enables people to list products and services for sale or trade locally and safely. iBazar took inspiration from traditional bazaars, markets and tianguis that are an important part of ...</p>
</a>
</div>

<div data-id="21149" data-name="Cuponzote" data-href="http://www.cuponzote.com" class="card startup   id6 id34 id424 id673  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.cuponzote.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>
<a href="http://twitter.com/cuponzote" rel="nofollow" target="_blank"><i class="fa fa-twitter"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.cuponzote.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/21149-c96dfab77ea014ba6851fa3025c88e4b-thumb_jpg.jpg?buster=1331855628" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Cuponzote -  social media advertising deals local advertising" />
<h1 property="name">
Cuponzote
</h1>
<span href="http://twitter.com/cuponzote" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Local commerce adapted to Latin America through low-friction, pay-at-merchant deals<br></strong>
Cuponzote helps people answer the question: “What am I going to do today?” by presenting them with a broad but curated selection of well-regarded businesses offering exclusive deals and experiences.
By not requiring pre-payment we have a business model that is ...</p>
</a>
</div>

<div data-id="16777" data-name="EasyBroker" data-href="http://www.easybroker.com" class="card startup   id10 id16 id83  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.easybroker.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.easybroker.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/16777-4c6be3c2f384d25972312071156c7e44-thumb_jpg.jpg?buster=1315736020" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="EasyBroker -  SaaS real estate CRM" />
<h1 property="name">
EasyBroker
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> CRM for real estate agents in Latin America<br></strong>
EasyBroker is an online website that lets you manage your leads and properties, market your listings, and create your own real estate website in minutes. We're currently profitable and have clients in 14 countries with Mexico as our biggest market.
You can also ...</p>
</a>
</div>

<div data-id="1868" data-name="Knightsbridge Capital Partners" data-href="http://www.knightspartners.com" class="card startup   id702 id2401 id2473  ">
<div class="links_bar">
<button data-id="" class="vote btn btn-success hide">
<i class="fa fa-star"></i>
<div class="count">123</div>
</button>
<div class="social">

<a href="http://www.knightspartners.com" rel="nofollow" target="_blank"><i class="fa fa-globe"></i> </a>

</div>
</div>
<a class="main_link" href="http://www.knightspartners.com" target="_blank" vocab="http://schema.org/" typeof="Organization" rel="nofollow">
<img data-src="https://d1qb2nb5cznatu.cloudfront.net/startups/i/1868-3652555b2cb610cf5b625b135db8662a-thumb_jpg.jpg?buster=1315741835" src="/img/spacer.gif" class="lazy" border="0" property="image" alt="Knightsbridge Capital Partners -  agriculture oil and gas waste management" />
<h1 property="name">
Knightsbridge Capital Partners
</h1>
<span href="" property="url" class="hide"><i class="fa fa-twitter"></i></span>
<div href="#report_modal" role="button" class="bt_report not_hidden" data-toggle="modal">
<i class="fa fa-flag"></i>
Bad entry?
</div>
<p>
<strong> Emerging Markets fund<br></strong>
Emerging markets fund focused on Mexico with projects in Agriculture, Oil &amp; Gas, Waste Recycling &amp; Management</p>
</a>
</div>
</div>
</div>
<div id="promoBand" href="http://felixmenard.com" target="_blank" alt="">
<img style="" src="https://pbs.twimg.com/profile_images/3734823941/a269bd41a4ee3579a52bd45aaf4e8176_400x400.png">
<a href="https://www.linkedin.com/in/menard" target="_blank" alt="Product designer">
<i class="fa fa-linkedin-square"></i>
</a>

<h1>Keep in touch :)</h1>
<a href="https://twitter.com/felix_m" class="twitter-follow-button" data-show-count="false" data-size="large">Follow @felix_m</a>

<div class="clearfix"></div>
<div id="footer" style="margin-top: 60px; padding: 20px ; padding-top:100px;  ">
© 2017.
<strong>

<a href="https://www.startups-list.com/">Startups Lists</a>.


</strong>
<br> <br>

<div>
</div> 

<div class="addthis_toolbox addthis_floating_style addthis_32x32_style" style="right:0px;top:470px; background: none;">
<a class="addthis_button_preferred_1"></a>
<a class="addthis_button_preferred_2"></a>
<a class="addthis_button_preferred_3"></a>
<a class="addthis_button_preferred_4"></a>
<a class="addthis_button_compact"></a>
</div>
</body>
</html>


    <!DOCTYPE html>
    <html lang="en-US" prefix="og: http://ogp.me/ns#">

    <head>
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <meta charset="UTF-8">
        <meta name="LAVCA VC" content="LAVCA Venture Investors">
        <meta name="keywords" content="Venture Capital, Latin America, vc, investors, LatAm, news, LAVCA">
        <title>
            Latin American Startup Directory        </title>
        <link rel="profile" href="http://gmpg.org/xfn/11" />
                    <link rel="shortcut icon" href="https://lavca.org/wp-content/uploads/2016/07/LAVCA-Icon.png" type="image/x-icon" />
                            <link rel="alternate" type="application/rss+xml" title="LAVCA | The Association for Private Capital Investment in Latin America RSS Feed" href="https://lavca.org/feed/" />
                <link rel="alternate" type="application/atom+xml" title="LAVCA | The Association for Private Capital Investment in Latin America Atom Feed" href="https://lavca.org/feed/atom/" />
                <link rel="pingback" href="https://lavca.org/xmlrpc.php" />
                <title>Latin American Startup Directory</title>

<!-- End Google Tag Manager for WordPress by gtm4wp.com -->
<!-- This site is optimized with the Yoast SEO plugin v7.6.1 - https://yoast.com/wordpress/plugins/seo/ -->
<link rel="canonical" href="https://lavca.org/vc/startup-directory/" />
<meta property="og:locale" content="en_US" />
<meta property="og:type" content="article" />
<meta property="og:title" content="Latin American Startup Directory" />
<meta property="og:description" content="Using venture backing as an initial criteria, this directory tracks early stage Latin American companies that have received US$1m+ in funding and are still in operation." />
<meta property="og:url" content="https://lavca.org/vc/startup-directory/" />
<meta property="og:site_name" content="LAVCA | The Association for Private Capital Investment in Latin America" />
<meta property="article:section" content="Venture Capital" />
<meta property="og:image" content="https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA.png" />
<meta property="og:image:secure_url" content="https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA.png" />
<meta property="og:image:width" content="1017" />
<meta property="og:image:height" content="685" />
<meta property="og:image:alt" content="LAVCA Startup Directory" />
<meta name="twitter:card" content="summary" />
<meta name="twitter:description" content="Using venture backing as an initial criteria, this directory tracks early stage Latin American companies that have received US$1m+ in funding and are still in operation." />
<meta name="twitter:title" content="Latin American Startup Directory" />
<meta name="twitter:image" content="https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA.png" />

<link rel='dns-prefetch' href='//js.hs-scripts.com' />
<link rel='dns-prefetch' href='//s.w.org' />
<link rel="alternate" type="application/rss+xml" title="LAVCA | The Association for Private Capital Investment in Latin America &raquo; Feed" href="https://lavca.org/feed/" />
<link rel="alternate" type="application/rss+xml" title="LAVCA | The Association for Private Capital Investment in Latin America &raquo; Comments Feed" href="https://lavca.org/comments/feed/" />
		<style type="text/css">
img.wp-smiley,
img.emoji {
	display: inline !important;
	border: none !important;
	box-shadow: none !important;
	height: 1em !important;
	width: 1em !important;
	margin: 0 .07em !important;
	vertical-align: -0.1em !important;
	background: none !important;
	padding: 0 !important;
}
</style>
<!-- End Google Tag Manager for WordPress by gtm4wp.com --><meta name="generator" content="EDD DP v1.4.8" />
    <style type="text/css">
																																		a, .post-entry .penci-portfolio-filter ul li a:hover, .penci-portfolio-filter ul li a:hover, .penci-portfolio-filter ul li.active a, .post-entry .penci-portfolio-filter ul li.active a, .penci-countdown .countdown-amount, .archive-box h1, .post-entry a, .container.penci-breadcrumb span a:hover, .post-entry blockquote:before, .post-entry blockquote cite, .post-entry blockquote .author, .penci-pagination a:hover, ul.penci-topbar-menu > li a:hover, div.penci-topbar-menu > ul > li a:hover, .penci-recipe-heading a.penci-recipe-print { color: #9ac53c; }
		.penci-home-popular-post ul.slick-dots li button:hover, .penci-home-popular-post ul.slick-dots li.slick-active button, .archive-box:after, .archive-box:before, .penci-page-header:after, .penci-page-header:before, .post-entry blockquote .author span:after, .error-image:after, .error-404 .go-back-home a:after, .penci-header-signup-form, .woocommerce .page-title:before, .woocommerce .page-title:after, .woocommerce span.onsale, .woocommerce #respond input#submit:hover, .woocommerce a.button:hover, .woocommerce button.button:hover, .woocommerce input.button:hover, .woocommerce nav.woocommerce-pagination ul li span.current, .woocommerce div.product .entry-summary div[itemprop="description"]:before, .woocommerce div.product .entry-summary div[itemprop="description"] blockquote .author span:after, .woocommerce div.product .woocommerce-tabs #tab-description blockquote .author span:after, .woocommerce #respond input#submit.alt:hover, .woocommerce a.button.alt:hover, .woocommerce button.button.alt:hover, .woocommerce input.button.alt:hover, #top-search.shoping-cart-icon > a > span, #penci-demobar .buy-button, #penci-demobar .buy-button:hover, .penci-recipe-heading a.penci-recipe-print:hover, .penci-review-process span, .penci-review-score-total { background-color: #9ac53c; }
		.penci-pagination ul.page-numbers li span.current { color: #fff; background: #9ac53c; border-color: #9ac53c; }
		.footer-instagram h4.footer-instagram-title > span:before, .woocommerce nav.woocommerce-pagination ul li span.current, .penci-pagination.penci-ajax-more a.penci-ajax-more-button:hover, .penci-recipe-heading a.penci-recipe-print:hover { border-color: #9ac53c; }
		.woocommerce .woocommerce-error, .woocommerce .woocommerce-info, .woocommerce .woocommerce-message { border-top-color: #9ac53c; }
		.penci-slider ol.penci-control-nav li a.penci-active, .penci-slider ol.penci-control-nav li a:hover{ border-color: #9ac53c; background-color: #9ac53c; }
		.woocommerce .woocommerce-message:before, .woocommerce form.checkout table.shop_table .order-total .amount, .woocommerce ul.products li.product .price ins, .woocommerce ul.products li.product .price, .woocommerce div.product p.price ins, .woocommerce div.product span.price ins, .woocommerce div.product p.price, .woocommerce div.product .entry-summary div[itemprop="description"] blockquote:before, .woocommerce div.product .woocommerce-tabs #tab-description blockquote:before, .woocommerce div.product .entry-summary div[itemprop="description"] blockquote cite, .woocommerce div.product .entry-summary div[itemprop="description"] blockquote .author, .woocommerce div.product .woocommerce-tabs #tab-description blockquote cite, .woocommerce div.product .woocommerce-tabs #tab-description blockquote .author, .woocommerce div.product .product_meta > span a:hover, .woocommerce div.product .woocommerce-tabs ul.tabs li.active, .woocommerce ul.cart_list li .amount, .woocommerce ul.product_list_widget li .amount, .woocommerce table.shop_table td.product-name a:hover, .woocommerce table.shop_table td.product-price span, .woocommerce table.shop_table td.product-subtotal span, .woocommerce-cart .cart-collaterals .cart_totals table td .amount, .woocommerce .woocommerce-info:before, .woocommerce div.product span.price { color: #9ac53c; }
								.headline-title { background-color: #9ac53c; }
								.penci-headline-posts .slick-prev, .penci-headline-posts .slick-next { color: #717174; }
										a.penci-topbar-post-title:hover { color: #9ac53c; }
														ul.penci-topbar-menu > li a:hover, div.penci-topbar-menu > ul > li a:hover { color: #9ac53c; }
										.penci-topbar-social a:hover { color: #9ac53c; }
												#navigation, #navigation.header-layout-bottom { border-color: #e9eae2; }
								#navigation .menu li a:hover, #navigation .menu li.current-menu-item > a, #navigation .menu > li.current_page_item > a, #navigation .menu li:hover > a, #navigation .menu > li.current-menu-ancestor > a, #navigation .menu > li.current-menu-item > a { color:  #9ac53c; }
		#navigation ul.menu > li > a:before, #navigation .menu > ul > li > a:before { background: #9ac53c; }
								#navigation .menu .sub-menu, #navigation .menu .children, #navigation ul.menu ul a, #navigation .menu ul ul a { border-color:  #e9eae2; }
		#navigation .penci-megamenu .penci-mega-child-categories a.cat-active { border-top-color: #e9eae2; border-bottom-color: #e9eae2; }
		#navigation ul.menu > li.megamenu > ul.sub-menu > li:before, #navigation .penci-megamenu .penci-mega-child-categories:after { background-color: #e9eae2; }
																										#navigation .menu .sub-menu li a:hover, #navigation .menu .sub-menu li.current-menu-item > a { color:  #9ac53c; }
		#navigation ul.menu ul a:before, #navigation .menu ul ul a:before { background-color: #9ac53c;   -webkit-box-shadow: 5px -2px 0 #9ac53c;  -moz-box-shadow: 5px -2px 0 #9ac53c;  -ms-box-shadow: 5px -2px 0 #9ac53c;  box-shadow: 5px -2px 0 #9ac53c; }
												.penci-header-signup-form { padding: px 0; }
																						.header-social a:hover i {   color: #9ac53c; }
																										.featured-overlay-color, .penci-slider ul.slides li:after { opacity: ; }
		.featured-overlay-partent, .penci-slider ul.slides li:before { opacity: ; }
		.mag2slider-overlay:after { opacity: ; }
		.mag2-thumbnail:hover .mag2slider-overlay:after { opacity: ; }
																										.penci-magazine-slider ul.mag-wrap li .mag-overlay { opacity: ; }
		.penci-magazine-slider ul.mag-wrap .mag-content:hover .mag-overlay { opacity: ; }
																																										.penci-grid li.typography-style .overlay-typography { opacity: ; }
		.penci-grid li.typography-style:hover .overlay-typography { opacity: ; }
																																										.widget ul.side-newsfeed li .side-item .side-item-text h4 a:hover, .widget a:hover, .penci-sidebar-content .widget-social a:hover span, .widget-social a:hover span, .penci-tweets-widget-content .icon-tweets, .penci-tweets-widget-content .tweet-intents a, .penci-tweets-widget-content .tweet-intents span:after { color: #9ac53c; }
		.widget .tagcloud a:hover, .widget-social a:hover i, .widget input[type="submit"]:hover { color: #fff; background-color: #9ac53c; border-color: #9ac53c; }
		.about-widget .about-me-heading:before { border-color: #9ac53c; }
		.penci-tweets-widget-content .tweet-intents-inner:before, .penci-tweets-widget-content .tweet-intents-inner:after { background-color: #9ac53c; }
		.penci-slider.penci-tweets-slider ol.penci-control-nav li a.penci-active, .penci-slider.penci-tweets-slider ol.penci-control-nav li a:hover { border-color: #9ac53c; background-color: #9ac53c; }
																																																																														ul.homepage-featured-boxes .penci-fea-in:hover h4 span { color: #9ac53c; }
										.penci-home-popular-post .item-related h3 a:hover { color: #9ac53c; }
						.penci-home-popular-post .item-related span.date { color: #717174; }
																						.home-featured-cat-content .magcat-detail h3 a:hover { color: #9ac53c; }
						.home-featured-cat-content .grid-post-box-meta span a:hover { color: #000000; }
		.home-featured-cat-content .first-post .magcat-detail .mag-header:after { background: #000000; }
		.penci-slider ol.penci-control-nav li a.penci-active, .penci-slider ol.penci-control-nav li a:hover { border-color: #000000; background: #000000; }
						.home-featured-cat-content .mag-photo .mag-overlay-photo { opacity: ; }
		.home-featured-cat-content .mag-photo:hover .mag-overlay-photo { opacity: ; }
																										.inner-item-portfolio:hover .penci-portfolio-thumbnail a:after { opacity: ; }
										    </style>
    <link rel="icon" href="https://lavca.org/wp-content/uploads/2016/03/cropped-Favicon-Transparent-150x150.png" sizes="32x32" />
<link rel="icon" href="https://lavca.org/wp-content/uploads/2016/03/cropped-Favicon-Transparent.png" sizes="192x192" />
<link rel="apple-touch-icon-precomposed" href="https://lavca.org/wp-content/uploads/2016/03/cropped-Favicon-Transparent.png" />
<meta name="msapplication-TileImage" content="https://lavca.org/wp-content/uploads/2016/03/cropped-Favicon-Transparent.png" />

    </head>

    <body class="page-template page-template-page-vc-home page-template-page-vc-home-php page page-id-52418 page-child parent-pageid-27330">
                    <a id="close-sidebar-nav" class="header-1"><i class="fa fa-close"></i></a>

            <nav id="sidebar-nav" class="header-1">

                                    <div id="sidebar-nav-logo">
                                                        <a href="https://lavca.org/vc/"><img src="/wp-content/uploads/2016/07/LAVCA-Venture-Investors.png" alt="LAVCA VC" /></a>
                                                    </div>
                    
                                                                                    <div class="header-social sidebar-nav-social">
                                    <div class="inner-header-social">
				<a href="https://twitter.com/lavca_org" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.linkedin.com/company/lavca/" target="_blank"><i class="fa fa-linkedin"></i></a>
							<a href="https://www.youtube.com/user/LAVCA1?sub_confirmation=1" target="_blank"><i class="fa fa-youtube-play"></i></a>
									</div>                                </div>
                                                                    
                                        <ul id="menu-vc-menu" class="menu"><li id="menu-item-28564" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28564"><a href="https://lavca.org/vc/newsfeed/">News Feed</a></li>
<li id="menu-item-28563" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28563"><a href="https://lavca.org/vc/vc-data/">VC Data</a></li>
<li id="menu-item-35999" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35999"><a href="https://lavca.org/vc/lists/">Directories</a></li>
<li id="menu-item-32276" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32276"><a href="https://lavca.org/vc/featured-content/">Features ✚ Video</a></li>
<li id="menu-item-69001" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69001"><a href="https://lavca.org/tech-growth-coalition/">Tech Growth Coalition</a></li>
<li id="menu-item-28601" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-28601"><a href="https://lavca.org/vc/engage/">Engage</a>
<ul class="sub-menu">
	<li id="menu-item-28681" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28681"><a href="https://lavca.org/vc/engage/vc-council/">VC Council</a></li>
	<li id="menu-item-28682" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28682"><a href="https://lavca.org/vc/engage/vc-members/">VC Members</a></li>
</ul>
</li>
<li id="menu-item-27334" class="arrowadd menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-27334"><a target="_blank" href="http://lavca.org/">LAVCA</a></li>
</ul>            </nav>

            <!-- .wrapper-boxed -->
            <div class="wrapper-boxed header-style-header-1">

                <!-- Top Bar -->
                                    <div class="penci-top-bar">
	<div class="container">
		<div class="penci-headline">
            <ul id="" class="penci-topbar-menu">
                <li id="" class="memberloginbutton logged-out-button menu-item menu-item-type-post_type menu-item-object-page current-menu-item page_item page-item-12020 current_page_item menu-item-29977"><a rel="nofollow" href="https://lavca.org/wp-login.php">Member Login</a>                </li>
            </ul>
            
            <ul id="menu-top-bar" class="penci-topbar-menu"><li id="menu-item-29985" class="membersonly menu-item menu-item-type-post_type menu-item-object-page menu-item-29985"><a href="https://lavca.org/welcome/">Member Access</a></li>
</ul>            
										<div class="penci-topbar-social">
					<div class="inner-header-social">
				<a href="https://twitter.com/lavca_org" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.linkedin.com/company/lavca/" target="_blank"><i class="fa fa-linkedin"></i></a>
							<a href="https://www.youtube.com/user/LAVCA1?sub_confirmation=1" target="_blank"><i class="fa fa-youtube-play"></i></a>
									</div>				</div>
								</div>
	</div>
</div>                        
                            <header id="header" class="header-vc header-header-1 has-bottom-line">
                                <!-- #header -->
                                                                    <div class="inner-header">
                                        <div class="container">

                                            <div id="logo">
                                                <h1>
                    <a href="https://lavca.org/vc/"><img src="/wp-content/uploads/2016/07/LAVCA-Venture-Investors.png" alt="LAVCA VC" /></a>
                </h1>
                                            </div>
                                            
                                                                                                                                                                                    <!--
				<div class="header-banner header-style-3">
-->
                                                                                                                                                                                                                                                                                        <!--<img src="-->
                                                                                                                                                            <!--" alt="Banner" />-->
                                                                                                                                                                                                                                                            <!--
				</div>
-->
                                                                                        
                                                                                            
                                                                                                                                                                                                                                                                                                                        <div class="header-social">
                                                                                                                <div class="inner-header-social">
				<a href="https://twitter.com/lavca_org" target="_blank"><i class="fa fa-twitter"></i></a>
							<a href="https://www.linkedin.com/company/lavca/" target="_blank"><i class="fa fa-linkedin"></i></a>
							<a href="https://www.youtube.com/user/LAVCA1?sub_confirmation=1" target="_blank"><i class="fa fa-youtube-play"></i></a>
									</div>                                                                                                            </div>
                                                                                                                                                                                                                                                                    </div>
                                    </div>
                                    
                                                                    </header>
                            <!-- end #header -->

                                                            <!-- Navigation -->
                                <nav id="navigation" class=" header-layout-top header-1">
                                    <div class="container">
                                        <div class="button-menu-mobile header-1"><i class="fa fa-bars"></i></div>
                                        <ul id="menu-vc-menu-1" class="menu"><li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28564"><a href="https://lavca.org/vc/newsfeed/">News Feed</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28563"><a href="https://lavca.org/vc/vc-data/">VC Data</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-35999"><a href="https://lavca.org/vc/lists/">Directories</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-32276"><a href="https://lavca.org/vc/featured-content/">Features ✚ Video</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-69001"><a href="https://lavca.org/tech-growth-coalition/">Tech Growth Coalition</a></li>
<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-has-children menu-item-28601"><a href="https://lavca.org/vc/engage/">Engage</a>
<ul class="sub-menu">
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28681"><a href="https://lavca.org/vc/engage/vc-council/">VC Council</a></li>
	<li class="menu-item menu-item-type-post_type menu-item-object-page menu-item-28682"><a href="https://lavca.org/vc/engage/vc-members/">VC Members</a></li>
</ul>
</li>
<li class="arrowadd menu-item menu-item-type-custom menu-item-object-custom menu-item-home menu-item-27334"><a target="_blank" href="http://lavca.org/">LAVCA</a></li>
</ul>


                                            
                                                                                                            <div id="top-search">
                                                            <a class="search-click"><i class="fa fa-search"></i></a>
                                                            <div class="show-search">
                                                                <form role="search" method="get" id="searchform" action="https://lavca.org/">
    <div>
		<input type="text" class="search-input" placeholder="Type and hit enter..." name="s" id="s" />
	 </div>
</form>                                                                    <a class="search-click close-search"><i class="fa fa-close"></i></a>
                                                            </div>
                                                        </div>
                                                        
                                    </div>
                                </nav>
                                <!-- End Navigation -->
                                
                                    
                                            
	
	<div class="container penci_sidebar right-sidebar">
        <div id="main">
			<div class="theiaStickySidebar">
                                    <article id="post-52418" class="post-52418 page type-page status-publish has-post-thumbnail hentry category-vc-homepage-sticky category-venture-capital">
    	<div class="penci-page-header">
		<h1>Latin American Startup Directory</h1>
	</div>
	
			<div class="post-image">
			<a href="https://lavca.org/vc/startup-directory/"><img width="1017" height="685" src="https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA.png" class="attachment-penci-full-thumb size-penci-full-thumb wp-post-image" alt="LAVCA Startup Directory" srcset="https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA.png 1017w, https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA-575x387.png 575w, https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA-768x517.png 768w, https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA-585x394.png 585w" sizes="(max-width: 1017px) 100vw, 1017px" /></a>
		</div>
	
	<div class="post-entry">
		<div class="inner-post-entry">
			<p>Over the last five+ years, Latin America has produced a new generation of startups backed by the region’s tech investors. LAVCA <a href="https://lavca.org/industry-data/latin-america-venture-capital-five-year-trends/">established</a> the <span style="font-size: 10.5pt; color: #2fc5f2;"><strong>Latin American Startup Directory</strong></span> to identify and follow this universe of startups as they grow.</p>
<p><span style="font-size: 13.5pt;"><strong><span class="blue">(Directory Below &#x23ec;)</span></strong></span></p>
<p>Using <a href="https://lavca.org/research/lavca-data-methodology/">venture backing</a> as an initial criteria*, <strong>LAVCA identified <span style="color: #2fc5f2;">256</span> Latin American startups in the 2018 directory</strong>, up from<strong> <span style="color: #2fc5f2;">155</span></strong> in the 2017 directory, and <strong> <span style="color: #2fc5f2;">100</span></strong> in the <a href="https://lavca.org/industry-data/latin-america-venture-capital-five-year-trends/">inaugural list</a>.</p>
<p><span style="font-size: 13.5pt;"><strong>The 2018 Latin American Startup Directory includes early stage companies that have received US$1m+ in verified venture funding (by end of 2017) and are still in operation, as tracked by LAVCA, but not companies that have been acquired or realized an IPO.</strong></span></p>
<p><a href="https://lavca.org/m-and-a-startup-image-draft-02-09-18/"><img class="wp-image-65354 alignleft" src="https://lavca.org/wp-content/uploads/2018/02/M-and-A-Startup-Image-Draft-02.09.18-575x413.png" alt="Acquired Latin American Startups LAVCA" width="123" height="89" srcset="https://lavca.org/wp-content/uploads/2018/02/M-and-A-Startup-Image-Draft-02.09.18-575x413.png 575w, https://lavca.org/wp-content/uploads/2018/02/M-and-A-Startup-Image-Draft-02.09.18-768x552.png 768w, https://lavca.org/wp-content/uploads/2018/02/M-and-A-Startup-Image-Draft-02.09.18-585x420.png 585w, https://lavca.org/wp-content/uploads/2018/02/M-and-A-Startup-Image-Draft-02.09.18.png 902w" sizes="(max-width: 123px) 100vw, 123px" /></a><strong>Click <a href="https://lavca.org/2017/06/20/latin-american-companies-raised-us1m-via-2-rounds-acquiredlisted-2012-2017/">here</a> for a list of highlighted companies that have raised US$1m+ and have been acquired/listed, including <strong><span style="font-size: 10.5pt; color: #2fc5f2;">99, Globant, Despegar, Netshoes</span> and others.</strong></strong></p>
<p><span style="font-size: 9.5pt;"><em>*The 2016 and 2017 directories included early stage companies that had received US$1m+ in funding via a minimum of two rounds of investment and were still in operation. The 2018 directory includes all startups that have received US$1m+ in funding with no minimum number of rounds.</em></span></p>
<h3 class="inline" style="color: #9ac53c; text-align: center;"><strong> 2018 LATIN AMERICAN STARTUP DIRECTORY</strong></h3>
<p>&nbsp;</p>
<div class="penci-portfolio penci-portfolio-wrap column-2 loaded lists_organizations" style="position: relative;"><div class="inner-portfolio-posts"><h3 id="directory-header">Filter directories by keyword:</h3>
<ul id="directoryFilterOptions">
	<h3>Location: </h3>
	<li class="active"><a href="#" class="all">All</a></li>
	<li><a href="#" class="country-Argentina">AR</a></li>
	<li><a href="#" class="country-Brazil">BR</a></li>
	<li><a href="#" class="country-Chile">CL</a></li>
	<li><a href="#" class="country-Colombia">CO</a></li>
	<li><a href="#" class="country-CostaRica">CR</a></li>
	<li><a href="#" class="country-Ecuador">EC</a></li>
	<li><a href="#" class="country-Spain">ES</a></li>
	<li><a href="#" class="country-Mexico">MX</a></li>
	<li><a href="#" class="country-Peru">PE</a></li>
	<li><a href="#" class="country-Uruguay">UY</a></li>
	<li><a href="#" class="country-USA">USA</a></li>
</ul>
<ul id="directoryFilterOptions2" class="fullwidth_directory_filter">
	<h3>Sector: </h3>
	<li class="active"><a href="#" class="all">All</a></li>
<!--<li><a href="#" class="sector-accounting">Accounting</a></li>-->
<li><a href="#" class="sector-marketing">Adtech & Marketing</a></li>
<!--<li><a href="#" class="sector-apc">Advanced Process Control</a></li>-->
<li><a href="#" class="sector-agtech">Agtech</a></li>
<li><a href="#" class="sector-ai">AI</a></li>
<li><a href="#" class="sector-animalcare">Animal Care</a></li>
<!--<li><a href="#" class="sector-automotive">Automotive</a></li>-->
<li><a href="#" class="sector-b2b">B2B</a></li>
<li><a href="#" class="sector-bigdata">Big Data</a></li>
<li><a href="#" class="sector-biotechnology">Biotech</a></li>
<li><a href="#" class="sector-blockchaincryptocurrency">Blockchain/Cryptocurrency</a></li>
<li><a href="#" class="sector-businessservices">Business Services</a></li>
<!--<li><a href="#" class="sector-classifieds">Classifieds</a></li>-->
<li><a href="#" class="sector-cleantech">CleanTech/Alternative/Renewable Energy</a></li>
<li><a href="#" class="sector-cloud">Cloud Computing</a></li>
<!--<li><a href="#" class="sector-communications">Comm Infrastructure</a></li>-->
<li><a href="#" class="sector-retail">Consumer/Retail</a></li>
<!--<li><a href="#" class="sector-content">Content</a></li>-->
<li><a href="#" class="sector-crm">CRM</a></li>
<!--<li><a href="#" class="sector-credit">Credit</a></li>
<li><a href="#" class="sector-cybersecurity">Cybersecurity</a></li>-->
<li><a href="#" class="sector-delivery">Delivery</a></li>
<li><a href="#" class="sector-digitalsecurity">Digital Security</a></li>
<!--<li><a href="#" class="sector-entertainment">Digital Media & Entertainment</a></li>-->
<li><a href="#" class="sector-earthobservation">Earth Observation</a></li>
<li><a href="#" class="sector-ecommerce">e-commerce</a></li>
<li><a href="#" class="sector-edtech">Edtech</a></li>
<li><a href="#" class="sector-erp">ERP</a></li>
<li><a href="#" class="sector-financial">Financial Services</a></li>
<li><a href="#" class="sector-fintech">Fintech</a></li>
<li><a href="#" class="sector-games">Games</a></li>
<!--<li><a href="#" class="sector-geolocation">Geolocation</a></li>
<li><a href="#" class="sector-geomarketing">Geomarketing</a></li>-->
<li><a href="#" class="sector-govtech">Govtech</a></li>
<li><a href="#" class="sector-hardware">Hardware</a></li>
<li><a href="#" class="sector-healthcarelifesciences">Healthcare/Life Sciences</a></li>
<li><a href="#" class="sector-healthtech">Healthtech</a></li>
<li><a href="#" class="sector-hospitalitytraveltourism">Hospitality, Travel & Tourism</a></li>
<li><a href="#" class="sector-hrtech">HRtech</a></li>
<li><a href="#" class="sector-iot">IoT</a></li>
<!--<li><a href="#" class="sector-identity">Identity Management</a></li>
<li><a href="#" class="sector-it">IT</a></li>
<li><a href="#" class="sector-insurance">Insurance</a></li>-->
<li><a href="#" class="sector-legal">Legal</a></li>
<!--<li><a href="#" class="sector-lending">Lending</a></li>-->
<li><a href="#" class="sector-logistics">Logistics</a></li>
<li><a href="#" class="sector-marketplaces">Marketplaces</a></li>
<li><a href="#" class="sector-mediaentertainment">Media & Entertainment</a></li>
<li><a href="#" class="sector-mining">Mining</a></li>
<li><a href="#" class="sector-mobility">Mobility</a></li>
<li><a href="#" class="sector-proptech">Proptech</a></li>
<!--<li><a href="#" class="sector-medicaldevices">Medical Devices</a></li>
<li><a href="#" class="sector-mobility">Mobile</a></li>
<li><a href="#" class="sector-payments">Payments</a></li>
<li><a href="#" class="sector-pets">Pets</a></li>
<li><a href="#" class="sector-pharmaceutical">Pharma</a></li>
<li><a href="#" class="sector-realestate">Real Estate</a></li>
<li><a href="#" class="sector-restaurants">Restaurants</a></li>-->
<li><a href="#" class="sector-saas">SaaS</a></li>
<li><a href="#" class="sector-salesmanagement">Sales Management</a></li>
<!--<li><a href="#" class="sector-security">Security</a></li>-->
<li><a href="#" class="sector-social">Social Networks</a></li>
<li><a href="#" class="sector-space">Space Tech</a></li>
<li><a href="#" class="sector-telecommunications">Telecommunications</a></li>
<li><a href="#" class="sector-transportation">Transportation</a></li>
<!--<li><a href="#" class="sector-travel">Travel & Tourism</a></li>-->





	
	
</ul>
<ul id="directoryFilterOptions3" class="fullwidth_directory_filter">
	<h3>Stage: </h3>
	<li class="active"><a href="#" class="all">All</a></li>
	<li><a href="#" class="stage-seed">Seed/Incubator</a></li>
	<li><a href="#" class="stage-early">Early Stage</a></li>
	<li><a href="#" class="stage-expansion">Expansion Stage</a></li>
	<!--<li><a href="#" class="stage-growth">Growth Capital</a></li>-->

</ul>
<hr />

<ul id="directory-list" class="startup_directory_list">

<li class="member-directory country-Mexico  stage-early country-USA sector-biotechnology">
	<div>
		<h1><a href="https://www.unima.com.mx/" title="Unima" target="_blank">Unima</a></h1>
		<div class="listorg_description"><p><span style="font-weight: 400;">Unima is a fast and low-cost diagnostics and infectious disease surveillance technology for limited resource settings.</span></p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico, USA (San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Biotechnology</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/joseluisnuno/">Jose Luis Nuno</a>: CEO, <a href="https://www.linkedin.com/in/lauramendoza/">Laura Mendoza</a>: Chief Product Officer, <a href="https://www.linkedin.com/in/alejandronuno/">Alejandro Nuno</a>: Chief Scientific Officer, <a href="https://www.linkedin.com/in/rodrigo-nuno-585570a/">Rodrigo Nuno</a>: Chief Manufacturing Officer</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ycombinator.com/">Y Combinator</a>, <a href="https://www.investovc.com/">Investo</a>, <a href="http://www.socialcapital.com/">Social Capital</a>, <a href="http://avalancha.ventures/">Avalancha Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Unima" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Unima_" title="Twitter" target="_blank">@Unima_</a></h3></div>
		</div>
		
	</div>
</li>

<li class="member-directory country-Mexico  stage-early sector-ecommerce">
	<div>
		<h1><a href="http://www.bebitos.mx/" title="Bebitos" target="_blank">Bebitos</a></h1>
		
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bebitos" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/bebitosmx" title="Twitter" target="_blank">@bebitosmx</a></h3></div>
		</div>
		
	</div>
</li>

<li class="member-directory country-Argentina  stage-seed country-Colombia sector-fintech">
	<div>
		<h1><a href="https://www.123seguro.com/" title="123Seguro.com" target="_blank">123Seguro.com</a></h1>
		<div class="listorg_description"><p>123Seguro is an online insurance broker.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/martin-ferrari-248a153a/" target="_blank">Martin Ferrari</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>,  <a href="https://alaya-capital.com/en/" _blank"="">Alaya Capital Partners </a>,  <a href="https://www.mercadolibre.com/fund" _blank"="">Mercado Libre Fund </a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=123Seguro.com" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/123Seguro" title="Twitter" target="_blank">@123Seguro</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early">
	<div>
		<h1><a href="https://www.1doc3.com/" title="1DOC3" target="_blank">1DOC3</a></h1>
		<div class="listorg_description"><p>1doc3 is a e-health platform that connects  Spanish speaking people with licensed and trusted doctors.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/jcardona78/" target="_blank">Javier Andres Cardona Mora</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://theventure.city/" _blank"="">The Venture City</a>, <a href="http://www.mountainnazca.com/index.html" _blank"="">Mountain Nazca</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=1DOC3" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Doctor1DOC3" title="Twitter" target="_blank">@Doctor1DOC3</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech">
	<div>
		<h1><a href="https://www.acessocard.com.br/" title="Acesso Soluções de Pagamentos" target="_blank">Acesso Soluções de Pagamentos</a></h1>
		<div class="listorg_description"><p>Accesso issues and managers credit cards and means of prepaid payments.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/paulowk/" target="_blank">Paulo Kulikovsky</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.investtech.com.br/?lang=en/" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Acesso Soluções de Pagamentos" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Acessocard" title="Twitter" target="_blank">@Acessocard</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-healthcare">
	<div>
		<h1><a href="http://www.adavium.com/" title="Adavium Medical" target="_blank">Adavium Medical</a></h1>
		<div class="listorg_description"><p>Adavium Medical is a medical equipment and diagnostics company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/fred-aslan-7b2ba41/" target="_blank">Fred Aslan</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://cvfcapitalpartners.com/" _blank"="">CVF</a>, <a href="https://www.venrock.com/" _blank"="">Venrock</a>, <a href="http://www.aberdare.com/" _blank"="">Aberdare Ventures</a> , <a href="https://www.arboretumvc.com/wp/" _blank"="">Arboretum Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Adavium Medical" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-cloud">
	<div>
		<h1><a href="https://www.adtsys.com.br/" title="ADTSys" target="_blank">ADTSys</a></h1>
		<div class="listorg_description"><p>ADTsys provides technology solutions for cloud computing, cloud security, monitoring and servers orchestration.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Cloud Computing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pascoal-baldasso-b0b7b97/" target="_blank">Pascoal Baldasso</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ADTSys" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/adtsys" title="Twitter" target="_blank">@adtsys</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-Mexico country-Peru sector-fintech">
	<div>
		<h1><a href="https://www.afluenta.com/" title="Afluenta" target="_blank">Afluenta</a></h1>
		<div class="listorg_description"><p>Afluenta is a peer-to-peer lending network that brings together investors and creditworthy borrowers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Peru, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alejandrocosentino/" target="_blank">Alejandro Consentino</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ignia.mx/" _blank"="">IGNIA </a> , <a href="https://www.elevarequity.com/" _blank"="">Elevar Equity </a>, <a href="https://www.ifc.org/" _blank"="">International Finance Corporation</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Afluenta" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/afluenta" title="Twitter" target="_blank">@afluenta</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-agtech">
	<div>
		<h1><a href="https://agrosmart.com.br/en/" title="Agrosmart" target="_blank">Agrosmart</a></h1>
		<div class="listorg_description"><p>Agrosmart improves productivity and optimizes the use of resources in agriculture. Its solution monitors more than 10 environmental conditions and provides relevant data for a better decision-making.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Agtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/vasconcelosmariana/" target="_blank">Mariana Vasconselhos</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Agrosmart" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/agrosmartBR" title="Twitter" target="_blank">@agrosmartBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-saas">
	<div>
		<h1><a href="http://www.ahgora.com/en_" title="Ahgora Sistemas" target="_blank">Ahgora Sistemas</a></h1>
		<div class="listorg_description"><p>Ahgora Sistemas is a management company that specializes in products and services for point of control of company management.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lazaro-malta-santos-4299878" target="_blank"> Lazaro Malta Santos</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.investtech.com.br/?lang=en/" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ahgora Sistemas" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ahgora" title="Twitter" target="_blank">@ahgora</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-security">
	<div>
		<h1><a href="https://www.aker.com.br/" title="Aker N-Stalker" target="_blank">Aker N-Stalker</a></h1>
		<div class="listorg_description"><p>Aker N-Stalker provides solutions to protect corporations and individuals against digital threats that affect information systems. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2000</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rodrigo-jonas-fragola/" target="_blank"> Rodrigo Jonas Fragola </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.investtech.com.br/?lang=en/" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aker N-Stalker" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-retail">
	<div>
		<h1><a href="https://www.alavadeira.com/" title="Alavadeira" target="_blank">Alavadeira</a></h1>
		<div class="listorg_description"><p>ALavadeira offers laundry and dry-cleaning services on a subscription basis.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Consumer/Retail</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/humberto-de-andrade-soares-83b22/" target="_blank"> Humberto de Andrade Soares </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.crp.com.br/en/" _blank"="">CRP Companhia de Participações</a>, <a href="https://www.cventures.com.br/" _blank"="">Cventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Alavadeira" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Alavadeira" title="Twitter" target="_blank">@Alavadeira</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-early sector-biotechnology">
	<div>
		<h1><a href="http://www.algenis.com/" title="Algenis" target="_blank">Algenis</a></h1>
		<div class="listorg_description"><p>Algenis specializes in the production, formulation, and clinical development of bioactive molecules produced by marine microalgae with medical application as muscle relaxant and analgesic, as well as for the treatment of neurological diseases.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2002</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Biotechnology</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/constanza-sigala-b84a723" target="_blank"> Constanza Sigala </a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurus.com/AURUS/Inicio#" _blank"="">Aurus</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Algenis" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early">
	<div>
		<h1><a href="https://aliada.mx/" title="Aliada / Tandem" target="_blank">Aliada / Tandem</a></h1>
		<div class="listorg_description"><p>Aliada is a marketplace for finding a trusted cleaning professional and improving the working conditions of the domestic service industry. Tandem is a B2B service for offices, including cleaning and other offerings.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rodolfo-corcuera-meier-58377844/" target="_blank" rel="noopener"> Rodolfo Corcuera Meier</a>: Co-founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://capitalimx.com/es">Capital Invent</a>, <a href="http://www.variv.com/">VARIV Capital</a>, <a href="http://www.dilacapital.com/?lang=en">Dila Capital</a> , <a href="http://www.psm.org.mx/">Promotora Social Mexico</a> , <a href="https://www.gentera.com.mx/">Genetera</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aliada / Tandem" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/AliadaMX" title="Twitter" target="_blank">@AliadaMX</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Redwood City, cA)  stage-early country-Colombia sector-fintech">
	<div>
		<h1><a href="https://alkanza.us/" title="Alkanza" target="_blank">Alkanza</a></h1>
		<div class="listorg_description"><p>Alkanza is an AI based robo-advisor.&nbsp;</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Redwood City, cA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, USA (Redwood City, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/andresvillaquiran/" target="_blank"> Andres Villaquiran </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Alkanza" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/alkanzaUS" title="Twitter" target="_blank">@alkanzaUS</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="https://americanet.com.br/" title="Americanet" target="_blank">Americanet</a></h1>
		<div class="listorg_description"><p>AmericaNet is a telecommunications company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1996</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Telecommunications</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lincoln-oliveira-475a1913/" target="_blank"> Lincoln Oliveira </a>: President</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.investtech.com.br/?lang=en/" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Americanet" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-early sector-biotechnology">
	<div>
		<h1><a href="http://andesbio.com/" title="Andes Biotechnologies" target="_blank">Andes Biotechnologies</a></h1>
		<div class="listorg_description"><p>Andes Biotechnologies is a leading nucleic acid-based drug discovery and development company focused on solid tumor cancers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Biotechnology</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p>Pablo Valenzuela, Ph.D: Co-Founder, CEO &amp; President</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurus.com/AURUS/Inicio#" _blank"="">Aurus</a>, <a href="http://www.australcap.com" _blank"="">Austral Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Andes Biotechnologies" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://www.conecturma.com.br/" title="Aondê / Conecturma" target="_blank">Aondê / Conecturma</a></h1>
		<div class="listorg_description"><p>Aonde offers educational solutions for childer age 3 to 11.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rafaelparente/" target="_blank">Rafael Parente</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aondê / Conecturma" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Spain  stage-early country-Mexico sector-fintech">
	<div>
		<h1><a href="https://aplazame.com/" title="Aplazame" target="_blank">Aplazame</a></h1>
		<div class="listorg_description"><p>Aplazame is a is a consumer credit company that provides instant credit for online purchases with a risk-free solution. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Spain</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Spain, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/fcabelloastolfi/" target="_blank">Fernando Cabello-Astolfi </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aplazame" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/aplazame" title="Twitter" target="_blank">@aplazame</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early">
	<div>
		<h1><a href="https://www.apli.jobs/" title="APLI" target="_blank">APLI</a></h1>
		<div class="listorg_description"><p>Apli is a Mexican startup connecting workers to flexible job opportunities.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/veramakarov/" target="_blank">Vera Makarov </a>: Co-CEO                               <a href="https://www.linkedin.com/in/jmpertusa/?locale=de_DE&quot;" target="_blank">Jose Maria Pertusa</a>: Co-CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a>,  <a href="http://www.ignia.mx/" _blank"="">IGNIA </a>,  <a href="www.socialcapital.com/" _blank"="">Social Capital, </a> <a href="www.socialcapital.com/" _blank"="">Soldier Fields Angels </a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=APLI" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/aplijobs" title="Twitter" target="_blank">@aplijobs</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-bigdata">
	<div>
		<h1><a href="https://aquare.la/en/" title="Aquarela" target="_blank">Aquarela</a></h1>
		<div class="listorg_description"><p>Aquarela is a big data &amp; analytics company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcoshsantos/" target="_blank">Marcos Santos</a>:Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://portcapitalllc.com/" _blank"="">Port Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aquarela" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/aquare_la" title="Twitter" target="_blank">@aquare_la</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-games">
	<div>
		<h1><a href="http://www.aquiris.com.br/" title="Aquiris Game Studio" target="_blank">Aquiris Game Studio</a></h1>
		<div class="listorg_description"><p>Aquiris Game Studio develops games for a wide range of platforms, from Internet browsers to the PC, and to  mobile phones.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2007</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Games</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/mlongoni/" target="_blank">Mauricio Longoni</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.crp.com.br/en/" _blank"="">CRP Companhia de Participações</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aquiris Game Studio" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/AquirisGS" title="Twitter" target="_blank">@AquirisGS</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-retail">
	<div>
		<h1><a href="https://www.arkpad.com.br/" title="Arkpad" target="_blank">Arkpad</a></h1>
		<div class="listorg_description"><p>Arkpad is an online portal for arquitecture and design.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Consumer/Retail</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://br.linkedin.com/in/kiko-salomão-80b14015" target="_blank">Kiko Salomão</a>: Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.barninvest.com.br/en/" _blank"="">Barn Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Arkpad" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/arkpad" title="Twitter" target="_blank">@arkpad</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://arquivei.com.br/" title="Arquivei" target="_blank">Arquivei</a></h1>
		<div class="listorg_description"><p>Arquivei is a web application that monitors Brazilian Nfes (invoices) for businesses by  automatically downloading the XML and PDF files, and storing them in the cloud. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/christian-de-cico/" target="_blank"> Christian de Cico </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://monashees.com.br/en/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Arquivei" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ArquiveiOficial" title="Twitter" target="_blank">@ArquiveiOficial</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech sector-saas">
	<div>
		<h1><a href="https://www.asaas.com/" title="ASAAS" target="_blank">ASAAS</a></h1>
		<div class="listorg_description"><p>ASAAS offers a cash management and collection application for individual entrepreneurs, small businesses and individuals. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pierocontezini/" target="_blank"> Piero Contezin </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.cventures.com.br/" _blank"="">CVentures</a>, <a href="https://www.ifc.org/" _blank"="">International Finance Corporation</a>, <a href="https://www.fomin.org/" _blank"="">FOMIN</a>, <a href="http://www.finep.gov.br/" _blank"="">FINEP</a>,<a href="https://www.caf.com/en/investors/" _blank"="">CAF</a>,</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ASAAS" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Mexico sector-edtech">
	<div>
		<h1><a href="http://aulasamigas.com/" title="Aulas AMiGAS" target="_blank">Aulas AMiGAS</a></h1>
		<div class="listorg_description"><p>Aulas AMIGAS® platform offers a solution to integrate technology into classrooms.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2004</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://co.linkedin.com/in/jmloperaa" target="_blank">Juan Manuel Lopera</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://graymatterscap.com/" _blank"="">Gray Matters Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Aulas AMiGAS" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/aulasamigas" title="Twitter" target="_blank">@aulasamigas</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Seattle, WA)  stage-early country-Argentina sector-saas sector-security">
	<div>
		<h1><a href="https://auth0.com/" title="Auth0" target="_blank">Auth0</a></h1>
		<div class="listorg_description"><p>Auth0 provides universal identity platform for web, mobile, IoT, and internal applications.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Seattle, WA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, UK, USA (Seattle, WA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/eugeniop/" target="_blank">Eugenio Pace</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.cygnusvc.com/" _blank"="">Cygnus Capital</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="http://www.trinityventures.com" _blank"="">Trinity Ventures</a>, <a href="http://www.bvp.com" _blank"="">Bessemer Venture Partners</a>, <a href="http://www.svb.com" _blank"="">Silicon Valley Bank</a>, <a href="http://www.k9ventures.com" _blank"="">K9 Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Auth0" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/auth0" title="Twitter" target="_blank">@auth0</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://www.avante.com.vc/" title="Avante" target="_blank">Avante</a></h1>
		<div class="listorg_description"><p>Avante provides micro-financing to businesses operating in the bottom of the pyramid.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rafael-lopes-1346a13a/" target="_blank">Rafael Lopes</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a>, <a href="http://fiinlab.com/&quot;_blank&quot;"> FiinLab</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Avante" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/AvanteComVc" title="Twitter" target="_blank">@AvanteComVc</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="http://www.avenida.com.ar/" title="avenida.com" target="_blank">avenida.com</a></h1>
		<div class="listorg_description"><p>Avenida is an e-commerce company dedicated to the sale of products of all kinds, aimed at the final consumer.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/danieljejcic/" target="_blank">Daniel Jejcic</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p>Tiger Global Management, <a href="https://www.naspers.com" _blank"="">Naspers</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, Richmond Capital Management, IRSA/APSA, <a href="https://www.quasarbuilders.com/#" _blank"="">Quasar</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=avenida.com" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/avenida_ar" title="Twitter" target="_blank">@avenida_ar</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="http://www.beautydate.com.br/" title="Beauty Date" target="_blank">Beauty Date</a></h1>
		<div class="listorg_description"><p>Beauty Date is a scheduling system that allows users to make appointments with hairdressers, manicurists, masseurs, beauticians, and more.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alexandre-kleis-094b1619/" target="_blank">Alexandre Kleis</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Beauty Date" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/BeautyDateBR" title="Twitter" target="_blank">@BeautyDateBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="https://www.bebestore.com.br/" title="Bebê Store" target="_blank">Bebê Store</a></h1>
		<div class="listorg_description"><p>Bebê Store is an e-commerce company for buying baby products.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rlpoco/" target="_blank">Rodrigo Poço</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.atomico.com" _blank"="">Atomico</a>, W7, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bebê Store" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/bebestore" title="Twitter" target="_blank">@bebestore</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="https://www.belezanaweb.com.br/" title="BelezaNaWeb" target="_blank">BelezaNaWeb</a></h1>
		<div class="listorg_description"><p>Beleza na Web is an online beauty retailer.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alexandre-alex-serodio-351341/" target="_blank">Alexandre Serodio</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, Tiger Global Management</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=BelezaNaWeb" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/belezanawebvip" title="Twitter" target="_blank">@belezanawebvip</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico sector-retail">
	<div>
		<h1><a href="http://www.benandfrank.com/" title="Ben &amp; Frank" target="_blank">Ben &amp; Frank</a></h1>
		<div class="listorg_description"><p>Ben &amp; Frank is revolutionizing the eyewear industry through a digitally native vertically integrated brand. With an omnichannel execution, Ben &amp; Frank offers great designs at affordable pricing.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Consumer/Retail</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="http://www.linkedin.com/in/marianacastillol/">Mariana Castillo</a>: Co-CEO, <a href="http://www.linkedin.com/in/eduardo-paulsen-53260916/">Eduardo Paulsen</a>: Co-CEO, <a href="http://www.linkedin.com/in/maria-jose-madero-castelao-80537396/">Maria Jose Madero</a>: Creative Director</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.dilacapital.com">Dila Capital</a>, <a href="https://www.jaguarvc.com/">Jaguar Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ben &amp; Frank" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/benandfrank_mx" title="Twitter" target="_blank">@benandfrank_mx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech">
	<div>
		<h1><a href="https://www.bidu.com.br/" title="Bidu" target="_blank">Bidu</a></h1>
		<div class="listorg_description"><p>Bidu is an online recommendation, comparison and marketplace portal for insurance and financial services.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rodolphogurgel/" target="_blank">Rodolpho Gurgel</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.amadeuscapital.com/ " _blank"="">Amadeus Capital Partners</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.bertelsmann.com/divisions/bertelsmann-investments/#st-1" _blank"="">Bertelsmann</a>, Tsing Capital</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bidu" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/BiduCorretora" title="Twitter" target="_blank">@BiduCorretora</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early">
	<div>
		<h1><a href="https://bitso.com/" title="Bitso" target="_blank">Bitso</a></h1>
		<div class="listorg_description"><p>Bitso is a currency exchange platform for trading crypocurrencies with the Mexican Peso. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Blockchain/Cryptocurrency</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pablo-gonzalez-88057023/" target="_blank">Pablo Gonzalez</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.variv.com/" _blank"="">VARIV Capital</a>, <a href="http://dcg.co" _blank"="">Digital Currency Group</a>, <a href="https://fundersclub.com" _blank"="">FundersClub</a>, <a href="http://www.monexgroup.jp/en/index.html" _blank"="">Monex Group</a>, Xochi Ventures, Bitcoin Capital, <a href="http://btl.co" _blank"="">Blockchain Tech Limited</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bitso" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/bitsoex" title="Twitter" target="_blank">@bitsoex</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-fintech">
	<div>
		<h1><a href="https://bizcap.com.br/" title="BizCapital" target="_blank">BizCapital</a></h1>
		<div class="listorg_description"><p>BizCapital is an online lending platform for small businesses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/cristiano-rocha-731b9/" target="_blank">Cristiano Rocha</a>: Co-Founder , <a href="https://www.linkedin.com/in/chicao78/?locale=pt_BR" target="_blank">Francisco Ferreira </a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=BizCapital" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-saas">
	<div>
		<h1><a href="https://www.bling.com.br/home" title="Bling" target="_blank">Bling</a></h1>
		<div class="listorg_description"><p>Bling is an online management system that allows its users to manage finances, inventory, and send invoices.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://bozanoinvestimentos.com.br/" _blank"="">Bozano Investimentos</a>, <a href="http://http://triaxiscapital.com/en/" _blank"="">Triaxis Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bling" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-fintech">
	<div>
		<h1><a href="https://kitado.com.br/" title="BLU365 | Kitado" target="_blank">BLU365 | Kitado</a></h1>
		<div class="listorg_description"><p>BLU365 | Kitado offers solutions for people to  perform debt negotiation with companies.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://br.linkedin.com/in/alexandrelara1" target="_blank">Alexandre Lara</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=BLU365 | Kitado" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed">
	<div>
		<h1><a href="https://www.boaconsulta.com/" title="BoaConsulta" target="_blank">BoaConsulta</a></h1>
		<div class="listorg_description"><p>BoaConsulta is an online healthcare booking service.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, HealthTech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adriano-fontana-b6b3464a/" target="_blank">Adriano Fontana</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.performainvestimentos.com/#/" _blank"="">Performa Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=BoaConsulta" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/boaconsultabr" title="Twitter" target="_blank">@boaconsultabr</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early">
	<div>
		<h1><a href="https://boletia.com/" title="Boletia" target="_blank">Boletia</a></h1>
		<div class="listorg_description"><p>Boletia is an online ticket selling platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://mx.linkedin.com/in/joshuafrancia/en/" target="_blank">Joshua Francia Torres</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://livcapital.mx/" _blank"="">LIV Capital</a>,<a href="http://www.dilacapital.com/?lang=en" _blank"="">Dila Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Boletia" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/boletia" title="Twitter" target="_blank">@boletia</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-credit sector-fintech">
	<div>
		<h1><a href="http://www.bompracredito.com.br/" title="Bom pra Credito" target="_blank">Bom pra Credito</a></h1>
		<div class="listorg_description"><p>Bom Pra Crédito provides on-line credit products.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ricardo-kalichsztein-640b5911/" target="_blank">Ricardo Kalichsztein</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://astellainvest.com/en/" _blank"="">Astella Investimentos</a>, <a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bom pra Credito" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="http://www.bossanovafilms.com.br/" title="Bossa Nova Films" target="_blank">Bossa Nova Films</a></h1>
		<div class="listorg_description"><p>BossaNovaFilms is an audiovisual production company specialized in advertising, entertainment and brand content. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/eduardo-tibiriça-machado-b1333233" target="_blank">Eduardo Tibiriça Machado</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://investimage.com.br/en/team/" _blank"="">Investimage Asset Developement</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Bossa Nova Films" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-agtech">
	<div>
		<h1><a href="http://www.bovcontrol.com/" title="BovControl" target="_blank">BovControl</a></h1>
		<div class="listorg_description"><p>BovControl provides a data collection and analysis tool to improve performance on meat, milk and genetics production.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Agtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/daniloleao/" target="_blank">Danilo Leao</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://wayra.co/en/" _blank"="">Wayra</a>, <a href="https://www.nxtplabs.com/" _blank"="">NXTP Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=BovControl" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/BovControl" title="Twitter" target="_blank">@BovControl</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-early country-Argentina country-Colombia country-Spain country-Mexico sector-ecommerce">
	<div>
		<h1><a href="https://www.buscalibre.co/" title="Busca Libre" target="_blank">Busca Libre</a></h1>
		<div class="listorg_description"><p>Buscalibre S.A. owns and operates an e-commerce retail platform that enables Spanish-speaking people to purchase books from outside their country.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2007</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Chile, Colombia, Mexico, Spain</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/bkraizel/" target="_blank&quot;"> Boris Kraizel </a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurusinc.com/" _blank"="">Aurus</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Busca Libre" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/BuscaLibre" title="Twitter" target="_blank">@BuscaLibre</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-expansion sector-fintech">
	<div>
		<h1><a href="https://www.busqo.com/" title="Busqo/Asegurate Facil" target="_blank">Busqo/Asegurate Facil</a></h1>
		<div class="listorg_description"><p>Busqo.com and Asegurate Facil merged to form one online insurance broker.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/juan-camilo-ayala-99968516/" target="_blank">Juan Camilo Ayala</a>:Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a>, <a href="https://www.velumventures.com" _blank"="">Velum Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Busqo/Asegurate Facil" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Busqo_" title="Twitter" target="_blank">@Busqo_</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="https://www.canaldapeca.com.br/" title="Canal da Peça" target="_blank">Canal da Peça</a></h1>
		<div class="listorg_description"><p>Canal da Peça enables autoparts digital sales in the aftermarket, by connecting manufacturers, merchants, repair shops and vehicle owners.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/vinicius-dias-8b61b825/" target="_blank">Vinicius Dias</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Canal da Peça" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/canaldapeca" title="Twitter" target="_blank">@canaldapeca</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-logistics">
	<div>
		<h1><a href="http://www.cargox.com.br/" title="CargoX" target="_blank">CargoX</a></h1>
		<div class="listorg_description"><p>CargoX is a technology powered freight broker that provides shippers a smart and efficient solution to transport products in any parts of Brazil, through technology, information and know-how in transport. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Logistics</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/fedvega/" target="_blank">Federico Vega</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://lumiacapital.com/" _blank"="">Lumia Capital</a>, <a href="http://www.agility.com/EN/Pages/Default.aspx" _blank"="">Agility Logistics</a>, <a href="http://www.goldmansachs.com/" _blank"="">Goldman Sachs</a>, <a href="https://www.qualcommventures.com/" _blank"="">Qualcomm Ventures</a>, <a href="https://www.georgesoros.com/" _blank"="">Soros</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=CargoX" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/cargo_x" title="Twitter" target="_blank">@cargo_x</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-hardware">
	<div>
		<h1><a href="http://catacompany.com/" title="Cata Company" target="_blank">Cata Company</a></h1>
		<div class="listorg_description"><p>Cata Company is an IoT startup that develops cash management solutions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hardware</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/victor-levy-b152a110/" target="_blank">Victor Levy</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.fircapital.com/" _blank"="">FIR Capital</a>, <a href="https://bzplan.bz/" _blank"="">BZPlan</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Cata Company" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.chefsclub.com.br/" title="ChefsClub" target="_blank">ChefsClub</a></h1>
		<div class="listorg_description"><p>ChefsClub is a marketplace in which customers subscribe and get 30%-50% off restaurant checks while restauranteurs can increase the flow of new and loyal customers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/guilherme-mynssen-0a510834/?locale=en_US" target="_blank">Guilherme Mynssen</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://confrapar.com.br/en/" _blank"="">Confrapar</a>, <a href="http://www.altivia.net.br/" _blank"="">Altivia Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ChefsClub" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ChefsClub" title="Twitter" target="_blank">@ChefsClub</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-hardware">
	<div>
		<h1><a href="http://chipus-ip.com/" title="Chipus Microeletronica" target="_blank">Chipus Microeletronica</a></h1>
		<div class="listorg_description"><p>Chipus Microelectronics is an analog and mixed-signal semiconductor IP company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hardware</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/murilo-pessatti-253a2a8" target="_blank"> Murilo Pessatti </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://bozanoinvestimentos.com.br/" _blank"="">Bozano Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Chipus Microeletronica" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/chipus_ip" title="Twitter" target="_blank">@chipus_ip</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early country-Mexico sector-hospitality sector-travel">
	<div>
		<h1><a href="http://www.clickbus.com/" title="ClickBus" target="_blank">ClickBus</a></h1>
		<div class="listorg_description"><p>ClickBus operates as an Online Travel Agency (OTA), Global Distribution System (GDS) and Whitelabel developer for bus operators and bus terminals.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Mexico, Turkey</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hospitality, Travel &amp; Tourism</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/cesariomartins/?ppe=1" target="_blank">Cesario Martins</a>: Global Founder &amp; Co-CEO, <a href="https://www.linkedin.com/in/fernando-prado-4b0b726/" target="_blank">Fernando Prado</a>: Global Founder &amp; Co-CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.rocket-internet.com/" _blank"="">Rocket Internet</a>, Latin America Internet Holding (LIH), <a href="http://www.tev.de/" _blank"="">Tengelmann Ventures</a>, <a href="http://www.holtzbrinck-ventures.com/" _blank"="">Holtzbrinck Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ClickBus" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ClickBusBR" title="Twitter" target="_blank">@ClickBusBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-ecommerce">
	<div>
		<h1><a href="http://www.clickOnero.com.mx__/" title="Clickonero" target="_blank">Clickonero</a></h1>
		<div class="listorg_description"><p>clickOnero is an e-commerce platform that offers deals and special offers from different online shops.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/elias-zaga-buzali-6001946/" target="_blank">Elias Zaga Buzali</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.variv.com/" _blank"="">Variv Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Clickonero" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-security">
	<div>
		<h1><a href="http://www.clicksign.com/" title="Clicksign" target="_blank">Clicksign</a></h1>
		<div class="listorg_description"><p>Clicksign is an electronic signature application.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcelokramer/" target="_blank">Marcelo Kramer</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="https://astellainvest.com/en/" _blank"="">Astella Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Clicksign" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Clicksign" title="Twitter" target="_blank">@Clicksign</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-healthcare">
	<div>
		<h1><a href="http://www.clinicasim.com/" title="Clínica SiM" target="_blank">Clínica SiM</a></h1>
		<div class="listorg_description"><p>Clinica SiM (Medicine Inclusion Services) delivers affordable healthcare for people at the bottom of the pyramid in Brazil.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2007</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/cruzdenis/" target="_blank">Denis Xavier Cruz</a>:Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Clínica SiM" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Clinica_sim" title="Twitter" target="_blank">@Clinica_sim</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-fintech">
	<div>
		<h1><a href="http://www.clip.mx/" title="CLIP" target="_blank">CLIP</a></h1>
		<div class="listorg_description"><p>Clip enables anyone to accept card payments by turning their smartphone or tablet into a card terminal.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico, Salt Lake City, Silicon Valley</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adolfobabatz/" target="_blank">Adolfo Babatz</a>: Co-founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a>, <a href="https://www.americanexpress.com/us/content/amexventures/" _blank"="">American Express Ventures</a>, <a href="http://www.fondodefondos.com.mx/" _blank"="">Fondo de Fondos</a>, <a href="http://www.sierraventures.com/" _blank"="">Sierra Ventures</a>, <a href="http://angelventures.vc/" _blank"="">Angel Ventures</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="http://svlatamfund.co/" _blank"="">SV LATAM Fund</a>, <a href="http://www.generalatlantic.com/" _blank"="">General Atlantic</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=CLIP" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/clip_mx" title="Twitter" target="_blank">@clip_mx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-bigdata">
	<div>
		<h1><a href="http://www.colab.re/" title="colab.re" target="_blank">colab.re</a></h1>
		<div class="listorg_description"><p>Colab.re is a citizen-to-government engagement platform, where citizens socially interact to report city’s daily issues, suggest urban improvements and rate public services. On the other end, it offers governments a freemium CRM and Workflow management tool. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Govtech, Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/paulo-pandolfi-96625940/" target="_blank"> Paulo Pandolfi</a>: Co-Founder, <a href="https://www.linkedin.com/in/brunoaracaty/?ppe=1" target="_blank">Bruno Aracaty Lima, CFA</a>: Co-Founder,    <a href="https://www.linkedin.com/in/gustavommaia/" target="_blank">Gustavo Maia</a>: Co-Founder, <a href="https://www.linkedin.com/in/josemando/" target="_blank">Josemando Sobral</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.omidyar.com" _blank"="">Omidyar Network</a>, <a href="http://www.a5.com.br" _blank"="">A5 Capital Partners</a>, <a href="https://www.mdif.org" _blank"="">MDIF (Media Development Investment Fund),</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=colab.re" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Colab_re" title="Twitter" target="_blank">@Colab_re</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early sector-ecommerce">
	<div>
		<h1><a href="https://www.colchonesrem.com/" title="Colchones REM" target="_blank">Colchones REM</a></h1>
		<div class="listorg_description"><p>Colchones REM is an e-commerce focused on mattresses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/santiago-varenkow/" target="_blank"> Santiago Varenkow Rodriguez </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a>, <a href="https://www.velumventures.com" _blank"="">Velum Ventures</a>, <a href="http://www.mgminnovagroup.com/capital/" _blank"="">MGM Innova Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Colchones REM" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ColchonesREM" title="Twitter" target="_blank">@ColchonesREM</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-expansion country-Brazil country-Colombia sector-fintech">
	<div>
		<h1><a href="https://www.comparaonline.com/" title="ComparaOnline" target="_blank">ComparaOnline</a></h1>
		<div class="listorg_description"><p>ComparaOnline provides comparison tools for insurance and financial products. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Chile, Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/sebastianvalin/" target="_blank">Sebastián Valin</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://ribbitcap.com" _blank"="">Ribbit Capital</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="http://risecapital.com" _blank"="">Rise Capital</a> ,<br>
<a href="https://www.ifc.org/" _blank"="">International Finance Corporation</a> , <a href="http://www.bamboocp.com/about/" _blank"="">Bamboo Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ComparaOnline" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ComparaOnline, ComparaOnlineBR, ComparaOnlineCO" title="Twitter" target="_blank">@ComparaOnline, ComparaOnlineBR, ComparaOnlineCO</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="http://www.concil.com.br/" title="CONCIL" target="_blank">CONCIL</a></h1>
		<div class="listorg_description"><p>Concil specialzies in financial and accounting reconciliation with expertise in online payment transactions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1993</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/leonardo-campelo-junior-30715063/" target="_blank">Leonardo Campelo Junior</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=CONCIL" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/concilbr" title="Twitter" target="_blank">@concilbr</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech">
	<div>
		<h1><a href="http://www.conductor.com.br/" title="Conductor" target="_blank">Conductor</a></h1>
		<div class="listorg_description"><p>Conductor Provides credit card processing services.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1997</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/antonio-soares-b46a8511/" target="_blank">Antonio Soares</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Conductor" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-fintech">
	<div>
		<h1><a href="http://www.conekta.io/" title="Conekta" target="_blank">Conekta</a></h1>
		<div class="listorg_description"><p>Conekta is a payments engine that allows users to send and receive money.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/cardenashector/" target="_blank">Hector Cardenas</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.jaguarvc.com" _blank"="">Jaguar Ventures</a>, <a href="http://www.variv.com/" _blank"="">VARIV Capital</a>, FEMSA Comercio, <a href="http://conconi.ca/" _blank"="">Conconi Growth Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Conekta" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/conektaIO" title="Twitter" target="_blank">@conektaIO</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="https://consultadobem.com.br/" title="Consulta do Bem" target="_blank">Consulta do Bem</a></h1>
		<div class="listorg_description"><p>Consulta do Bem offers a solution to schedule and get health consultations online.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcus-vinicius-gimenes-85849840/" target="_blank"> Marcus Vinicius Gimenes </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Consulta do Bem" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/consultadobem" title="Twitter" target="_blank">@consultadobem</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech sector-saas">
	<div>
		<h1><a href="http://contaazul.com/" title="ContaAzul" target="_blank">ContaAzul</a></h1>
		<div class="listorg_description"><p>ContaAzul is a management solution for SMBs that allows them to control their financials, sales, inventory, and issue invoices.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/viniciusroveda/?ppe=1" target="_blank">Vinicius Roveda</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://ribbitcap.com" _blank"="">Ribbit Capital</a>, <a href="https://500.co" _blank"="">500 Startups</a>, <a href="http://napkn.co" _blank"="">Napkn Ventures</a>, <a href="http://www.valar.com/" _blank"="">Valar Ventures</a>, Tiger Global Management</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ContaAzul" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ContaAzul" title="Twitter" target="_blank">@ContaAzul</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech sector-saas">
	<div>
		<h1><a href="https://www.contabilizei.com.br/" title="Contabilizei" target="_blank">Contabilizei</a></h1>
		<div class="listorg_description"><p>Contabilizei is tax filing and accounting SaaS for SMBs.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/vitordtorres/" target="_blank">Vitor Torres</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Contabilizei" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/contabilizei" title="Twitter" target="_blank">@contabilizei</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early country-Colombia country-USA sector-security">
	<div>
		<h1><a href="https://converus.com/_" title="Converus" target="_blank">Converus</a></h1>
		<div class="listorg_description"><p>Converus provides scientifically validated credibility assessment technologies.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Mexico, USA (Lehi, UT)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/todd-mickelsen-9862271/" target="_blank"> Todd Mickelsen </a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Converus" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ConverusInc" title="Twitter" target="_blank">@ConverusInc</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion country-Chile sector-delivery">
	<div>
		<h1><a href="https://cornershopapp.com/" title="Cornershop" target="_blank">Cornershop</a></h1>
		<div class="listorg_description"><p>Cornershop is an on demand grocery delivery platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, Delivery</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/oskarhjertonsson/" target="_blank">Oskar Hjertonsson</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a>, <a href="https://www.accel.com" _blank"="">Accel Partners</a>, <a href="http://www.creandum.com" _blank"="">Creandum</a>, Grupo Bimbo Family Office, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="http://jsv.com" _blank"="">Jackson Square Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Cornershop" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/cornershopapp" title="Twitter" target="_blank">@cornershopapp</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-bigdata sector-saas">
	<div>
		<h1><a href="http://cortex-intelligence.com/" title="Cortex Intelligence" target="_blank">Cortex Intelligence</a></h1>
		<div class="listorg_description"><p>Cortex Intelligence<br>
is a data analytics platform for marketing and sales.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2003</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/leonardocostarangel/" target="_blank">Leonardo Costa Rangel</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Cortex Intelligence" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/becortex" title="Twitter" target="_blank">@becortex</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-fintech">
	<div>
		<h1><a href="https://coru.com/" title="Coru (formerly ComparaGuru)" target="_blank">Coru (formerly ComparaGuru)</a></h1>
		<div class="listorg_description"><p>Coru provides comparison tools for insurance and financial products. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/bernardo-prum-a7844321/" target="_blank"> Bernardo Prum </a>: Managing Director</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://qedinvestors.com" _blank"="">QED Investors</a>, <a href="http://www.struckcapital.com/" _blank"="">Struck Capital</a>, <a href="http://www.novafounders.com/" _blank"="">NOVA Founders Capital</a>, <a href="http://seayaventures.com/" _blank"="">Seaya Ventures</a>, <a href="https://foundersfund.com/team/peter-thiel/" _blank"="">Peter Thiel</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Coru (formerly ComparaGuru)" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Coru_mx" title="Twitter" target="_blank">@Coru_mx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-credit sector-fintech">
	<div>
		<h1><a href="https://www.creditas.com.br/" title="Creditas" target="_blank">Creditas</a></h1>
		<div class="listorg_description"><p>Creditas is a digital lending platform focused on secured lending.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/sergiofurio/" target="_blank">Sergio Furio</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.quona.com" _blank"="">Quona Capital</a>, <a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="https://www.accion.org/investments" _blank"="">Accion</a>, <a href="http://qedinvestors.com" _blank"="">QUED Investors</a>, <a href="http://www.vostokemergingfinance.com" _blank"="">Vostok Emerging Finance</a> , <a href="https://www.naspers.com" _blank"="">Naspers</a> , <a href="https://www.ifc.org/" _blank"="">International Finance Corporation</a> , <a href="https://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Creditas" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/CreditasBR" title="Twitter" target="_blank">@CreditasBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion">
	<div>
		<h1><a href="https://culturacolectiva.com/en/" title="Cultura Colectiva" target="_blank">Cultura Colectiva</a></h1>
		<div class="listorg_description"><p>Cultura Colectiva is a digital media company that creates content for Spanish-speaking millennials.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/luis-andr%C3%A9s-enriquez-arias-07b15a31/" target="_blank">Luis Andrés Enriquez Arias</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://daluscapital.com/en/" _blank"="">Dalus Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Cultura Colectiva" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/CulturaColectiv" title="Twitter" target="_blank">@CulturaColectiv</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion country-Argentina country-Chile country-Colombia sector-ecommerce">
	<div>
		<h1><a href="http://www.dafiti.com.br/" title="Dafiti" target="_blank">Dafiti</a></h1>
		<div class="listorg_description"><p>Dafiti is an e-commerce website for footwear, and fashion for both men and women.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Chile, Colombia, Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/philipp-povel-9a309718/" target="_blank">Philipp Povel</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.rocket-internet.com/" _blank"="">Rocket Internet</a>, <a href="http://www.jpmorganassetmanagement.com.br/en/showpage.aspx?pageID=2" _blank"="">JP Morgan Asset Management</a>, <a href="http://www.otpp.com" _blank"="">Ontario Teachers’ Pension Plan (OTPP)</a>, <a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a>, Quadrant Capital Advisors, <a href="http://www.kinnevik.com" _blank"="">Kinnevik AB</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Dafiti" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/dafiti_brasil" title="Twitter" target="_blank">@dafiti_brasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-healthcare">
	<div>
		<h1><a href="https://www.dentalia.mx/" title="Dentalia" target="_blank">Dentalia</a></h1>
		<div class="listorg_description"><p>Dentalia offers dental health solutions for individuals, businesses and governments through fixed clinics and mobile clinics.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/federico-w-2b1225/" target="_blank">Federico Weber</a>:Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Dentalia" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/dentalia_mx" title="Twitter" target="_blank">@dentalia_mx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-edtech">
	<div>
		<h1><a href="http://www.descomplica.com.br/" title="Descomplica" target="_blank">Descomplica</a></h1>
		<div class="listorg_description"><p>Descomplica offers an educational platform that provides students with subject and course materials.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcofisbhen/" target="_blank">Marco Fisbhen</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p>“<a href="" https:="" www.amadeuscapital.com="" ""_blank""="">Amadeus Capital Partners</a>“, <a href="www.socialcapital.com/" _blank"="">Social Capital, </a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.valar.com/" _blank"="">Valar Ventures</a>, <a href="https://500.co" _blank"="">500 Startups</a>, EL Area, <a href="http://www.zenstonevc.com" _blank"="">Zenstone Venture Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Descomplica" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/descomplica" title="Twitter" target="_blank">@descomplica</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-hardware">
	<div>
		<h1><a href="http://www.deshtec.com.br/" title="Desh Tecnologia" target="_blank">Desh Tecnologia</a></h1>
		<div class="listorg_description"><p>Desh provides wireless communication solutions (IoT) for industrial applications M2M, including telemetry energy systems. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hardware</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adriano-seiti-yamaoka-50962b/" target="_blank">Adriano Seiti Yamaoka</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://portcapitalllc.com/" _blank"="">Port Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Desh Tecnologia" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-Brazil sector-edtech">
	<div>
		<h1><a href="http://www.digitalhouse.com/" title="Digital House" target="_blank">Digital House</a></h1>
		<div class="listorg_description"><p>Digital House is a coding school where new generations of coders and digital professionals are trained.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/nelson-duboscq-a6080829" target="_blank"> Nelson Duboscq </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.tpg.com" _blank"="">TPG</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.omidyar.com" _blank"="">Omidyar Network</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Digital House" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/_digitalhouse" title="Twitter" target="_blank">@_digitalhouse</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion sector-saas">
	<div>
		<h1><a href="http://www.diverza.com/" title="Diverza" target="_blank">Diverza</a></h1>
		<div class="listorg_description"><p>Diverza offers electronic invoicing services.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2003</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/joseluisayala/" target="_blank">José Luis Ayala</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Diverza" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/diverza" title="Twitter" target="_blank">@diverza</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early country-Argentina">
	<div>
		<h1><a href="https://www.doghero.com.br/" title="DogHero" target="_blank">DogHero</a></h1>
		<div class="listorg_description"><p>DogHero offers solutions for dog owners to find walk and pet-hosting services. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, Animal Care</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/eduardobaer/?ppe=1" target="_blank">Eduardo Baer</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=DogHero" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/DogHeroBrasil" title="Twitter" target="_blank">@DogHeroBrasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-healthcare">
	<div>
		<h1><a href="https://www.drconsulta.com/" title="Dr. Consulta" target="_blank">Dr. Consulta</a></h1>
		<div class="listorg_description"><p>Dr. Consulta is a network of low-cost health clinics in Brazil.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/thomaz-srougi-7b9b8a/" target="_blank">Thomaz Srougi</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.lgtvp.com" _blank"="">LGT Venture Philantrophy</a>, <a href="https://www.omidyar.com" _blank"="">Omidyar Network</a> , <a href="https://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, Madrone Capital</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Dr. Consulta" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/dr_consulta" title="Twitter" target="_blank">@dr_consulta</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Miami, FL)  stage-early sector-fintech">
	<div>
		<h1><a href="https://www.dvdendo.com/" title="Dvdendo" target="_blank">Dvdendo</a></h1>
		<div class="listorg_description"><p>Dvdendo is a digital micro-investment platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Miami, FL)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>USA (Miami, FL)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/gabriel-montoya-08842953/" target="_blank">Gabriel Montoya</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ideasycapital.com/" _blank"="">Ideas Y Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Dvdendo" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/mydvdendo" title="Twitter" target="_blank">@mydvdendo</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="http://www.e-construmarket.com.br/" title="e-Construmarket" target="_blank">e-Construmarket</a></h1>
		<div class="listorg_description"><p>e-Construmarket provides online solutions for the construction industry.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://br.linkedin.com/in/jorge-alvarez-rocha-730512153" target="_blank">Jorge Alvarez</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.investtech.com.br/?lang=en/" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=e-Construmarket" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="https://eadbox.com/" title="Eadbox" target="_blank">Eadbox</a></h1>
		<div class="listorg_description"><p>Eadbox is a platform used by education businesses to create and sell online courses in their own website.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/nilsonfilatieri/" target="_blank">Nilson Filatieri</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://bzplan.bz/" _blank"="">BZPlan</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Eadbox" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-transportation">
	<div>
		<h1><a href="https://econduce.mx/" title="Econduce" target="_blank">Econduce</a></h1>
		<div class="listorg_description"><p>Econduce is a network of shared electric scooters.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Transportation</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alejandro-morales-heimlich-3325b320&quot;" target="_blank"> Alejandro Morales Heimlich </a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ideasycapital.com/">Ideas &amp; Capital</a>, <a href="http://www.adobecapital.org/">Adobe Capital</a>, <a href="http://www.dilacapital.com/?lang=en" _blank"="">Dila Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Econduce" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/econducemx" title="Twitter" target="_blank">@econducemx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-early">
	<div>
		<h1><a href="https://www.ecoseafarming.com/" title="Ecosea Farming" target="_blank">Ecosea Farming</a></h1>
		<div class="listorg_description"><p>EcoSea Farming engages in the development, production, and deployment of copper alloy mesh aquaculture systems.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile, Japan</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Mining</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p>Craig J. Craven: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurus.com/AURUS/Inicio#" _blank"="">Aurus</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ecosea Farming" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://www.eduk.com.br/" title="Eduk" target="_blank">Eduk</a></h1>
		<div class="listorg_description"><p>Eduk is an online platform for courses in gastronomy, handicrafts, photography, fashion,  beauty, and others.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/eduardosouzalima/" target="_blank">Eduardo Lima</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.accel.com" _blank"="">Accel Partners</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.felicis.com/" _blank"="">Felicis Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Eduk" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/CursoseduK" title="Twitter" target="_blank">@CursoseduK</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="http://www.elo7.com/" title="Elo7" target="_blank">Elo7</a></h1>
		<div class="listorg_description"><p>Elo7 is an online marketplace that allows its users to buy and sell handmade products.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/carloseduardocurioni/" target="_blank">Carlos Curioni</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.insightpartners.com/" _blank"="">Insight Venture Partners</a>, <a href="https://www.accel.com" _blank"="">Accel Partners</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Elo7" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Elo7" title="Twitter" target="_blank">@Elo7</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="http://www.enjoei.com.br/" title="Enjoei" target="_blank">Enjoei</a></h1>
		<div class="listorg_description"><p>Enjoei is an online marketplace for used products in Brazil.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/tielima/" target="_blank">Tie Lima</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://www.bvp.com" _blank"="">Bessemer Venture Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Enjoei" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/enjoei" title="Twitter" target="_blank">@enjoei</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-expansion country-Mexico">
	<div>
		<h1><a href="http://www.enmedio.com.co/" title="Enmedio Comunicacion Digital" target="_blank">Enmedio Comunicacion Digital</a></h1>
		<div class="listorg_description"><p>Enmedio is an advertising company specializing in digitail signage.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/sebastianobregon/" target="_blank">Sebastian Obregon</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Enmedio Comunicacion Digital" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-marketing">
	<div>
		<h1><a href="http://www.escale.com.br/" title="Escale" target="_blank">Escale</a></h1>
		<div class="listorg_description"><p>Escale is a customer acquisition company that uses data and technology to build optimized buying experiences for brands.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/matthewkligerman/" target="_blank">Matthew Kligerman</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://www.globalfounders.vc" _blank"="">Global Founders Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Escale" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="http://www.exactsales.com.br/" title="Exact Sales" target="_blank">Exact Sales</a></h1>
		<div class="listorg_description"><p>Exact Sales offers a sales processing automation software.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/theo-orosco-9982b548/" target="_blank">Theo Orosco</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.cventures.com.br/" _blank"="">CVentures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Exact Sales" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/exactsales" title="Twitter" target="_blank">@exactsales</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-financial">
	<div>
		<h1><a href="https://exten.com.br/" title="Exten (Formerly DunasPlus)" target="_blank">Exten (Formerly DunasPlus)</a></h1>
		<div class="listorg_description"><p>Exten provides invoice financing solutions to small and medium businesses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Financial Services</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/thomas-cauchois-291854b" target="_blank">Thomas Cauchois</a>: President</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://quona.com/" _blank"="">Quona Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Exten (Formerly DunasPlus)" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-ecommerce sector-saas">
	<div>
		<h1><a href="http://www.ezcommerce.com.br/" title="EzCommerce" target="_blank">EzCommerce</a></h1>
		<div class="listorg_description"><p>EZ Commerce is a company specialized in e-commerce solutions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, e-Commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/henrique-mengue-84132842/" target="_blank">Henrique Mengue</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://bozanoinvestimentos.com.br/" _blank"="">Bozano Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=EzCommerce" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ezcommercebr" title="Twitter" target="_blank">@ezcommercebr</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion sector-healthcare">
	<div>
		<h1><a href="" title="Farmacias Personalizadas" target="_blank">Farmacias Personalizadas</a></h1>
		<div class="listorg_description"><p>Farmacias Personalizadas is a specialty pharmaceuticals distributor that offers solutions to patients with chronic degenerative diseases. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Farmacias Personalizadas" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early">
	<div>
		<h1><a href="http://www.favmedia.com/" title="FAV!" target="_blank">FAV!</a></h1>
		<div class="listorg_description"><p>FAV produces, aggregates, distributes and monetizes original video content for the Latin American and Hispanic US markets.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/maxgoldenberg/" target="_blank">Max Goldenberg</a>: Co-Founder &amp; Interim CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="http://www.patagoniaventures.com/#1" _blank"="">Patagonia Ventures</a>, <a href="http://www.cygnusvc.com/" _blank"="">Cygnus Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=FAV!" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/favnetwork" title="Twitter" target="_blank">@favnetwork</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-ecommerce">
	<div>
		<h1><a href="https://www.fazen.com.br/" title="Fazen" target="_blank">Fazen</a></h1>
		<div class="listorg_description"><p>Fazen is an online retailer for the agricultural industry.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2017</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://br.linkedin.com/in/vasco-carvalho-oliveira-neto-410a301" target="_blank">Vasco Carvalho Oliveira Neto</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>                       , <a href="http://www.globalenvironmentfund.com/about-us/" _blank"="">Global Environment Fund</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Fazen" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Argentina country-Brazil country-Ecuador country-Mexico country-USA sector-marketing">
	<div>
		<h1><a href="http://www.fluvip.com/" title="Fluvip" target="_blank">Fluvip</a></h1>
		<div class="listorg_description"><p>Fluvip connect brands with social media influencers to create Social Media campaigns with presence in Europe, USA and Latam.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Colombia, Ecuador, Mexico, Venezuela, USA (New York, Miami)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/sebastianjasminoy/" target="_blank">Sebastián Jasmino</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.velumventures.com" _blank"="">Velum Ventures</a>, <a href="https://theventure.city/" _blank"="">The Venture City</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Fluvip" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/FLUVIPGlobal" title="Twitter" target="_blank">@FLUVIPGlobal</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-cleantech">
	<div>
		<h1><a href="http://www.energryn.com/" title="Fricaeco America" target="_blank">Fricaeco America</a></h1>
		<div class="listorg_description"><p>Fricaeco America produces solar energy heating solutions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>CleanTech/Alternative/Renewable Energy</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/andr%C3%A9s-mu%C3%B1oz-4aa69226/" target="_blank">Andrés Muñoz</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://daluscapital.com/en/" _blank"="">Dalus Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Fricaeco America" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/AndresM57533093" title="Twitter" target="_blank">@AndresM57533093</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-retail">
	<div>
		<h1><a href="http://gaiadesign.com.mx/" title="GAIA Design" target="_blank">GAIA Design</a></h1>
		<div class="listorg_description"><p>GAIA is a furniture brand in Mexico. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Consumer/Retail</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://mx.linkedin.com/in/philippe-cahuzac-96574a7" target="_blank">Philippe Cahuzac</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://capitalimx.com/es" _blank"="">Capital Invent</a>, <a href="http://risecapital.com" _blank"="">Rise Capital</a>, <a href="http://www.variv.com/" _blank"="">Variv Capital</a>, <a href="https://www.cubocapital.com.br/" _blank"="">Cubo Capitall</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=GAIA Design" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/gaiadesignmx" title="Twitter" target="_blank">@gaiadesignmx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="http://www.gaudena.com/" title="Gaudena" target="_blank">Gaudena</a></h1>
		<div class="listorg_description"><p>Gaudena is an e-commerce website for footwear, and fashion for both men and women.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.jaguarvc.com" _blank"="">Jaguar Ventures</a>, <a href="http://www.allvp.vc" _blank"="">ALLVP</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Gaudena" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/gaudena" title="Twitter" target="_blank">@gaudena</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://www.geekie.com.br/" title="Geekie" target="_blank">Geekie</a></h1>
		<div class="listorg_description"><p>Geekie is a web-based platform that provides personalized educational content using adaptive learning technology.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/claudio-sassaki-6843788b/" target="_blank">Claudio Sassaki </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.omidyar.com" _blank"="">Omidyar Network</a>, <a href="http://www.geraventure.com.br" _blank"="">Gera Venture</a>,<a href="https://www.mitsui.com/us/en/index.html" _blank"=""> Mitsui &amp; Co</a>, <a href="https://www.virtuose.community/" _blank"="">Virtuose</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Geekie" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Geekiebrasil" title="Twitter" target="_blank">@Geekiebrasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-seed sector-healthcare">
	<div>
		<h1><a href="" title="GeneproDX" target="_blank">GeneproDX</a></h1>
		<div class="listorg_description"><p>GeneproDX is a molecular diagnostics company that will comercialize ThyroidPrint®, a gene signature for improved diagnostic accuracy of thyroid fine needle aspiration cytology.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://cl.linkedin.com/in/natalia-mena-phd-mba-65b09316" target="_blank">Natalia Mena, PhD</a>: COO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurus.com/AURUS/Inicio#" _blank"="">Aurus</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=GeneproDX" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-agtech">
	<div>
		<h1><a href="http://www.genica.com.br/" title="Gênica" target="_blank">Gênica</a></h1>
		<div class="listorg_description"><p>Gênica focuses on research and development of new tools for agricultural production management.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Agtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/patrick-vilela-ba587094/" target="_blank">Patrick Vilela</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Gênica" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-marketing sector-saas">
	<div>
		<h1><a href="http://www.geofusion.com.br/" title="Geofusion" target="_blank">Geofusion</a></h1>
		<div class="listorg_description"><p>Geofusion is a geomarketing company, providing solutions in the market intelligence segment focused on geography. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1996</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pedro-figoli-50b01115/" target="_blank">Pedro Figoli</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a>, <a href="http://www.intelcapital.com" _blank"="">Intel Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Geofusion" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/geofusiongeomkt" title="Twitter" target="_blank">@geofusiongeomkt</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="https://www.getninjas.com.br/" title="GetNinjas" target="_blank">GetNinjas</a></h1>
		<div class="listorg_description"><p>GetNinjas is an online marketplace to offer and hire local services such as home renovation, cleaning, private lessons, and more.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/eduardolhotellier/?ppe=1" target="_blank">Eduardo L’Hotellier</a>:<br>
Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.ottocapital.com" _blank"="">Otto Capital</a>, Tiger Global Management</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=GetNinjas" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/getninjas" title="Twitter" target="_blank">@getninjas</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-expansion country-Brazil country-Chile country-Colombia country-Mexico country-Peru country-Uruguay sector-saas">
	<div>
		<h1><a href="http://www.gointegro.com/" title="GoIntegro" target="_blank">GoIntegro</a></h1>
		<div class="listorg_description"><p>GOintegro is a human resource digital platform that empowers  employee engagement in Latin America.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2002</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile, Colombia, Mexico, Peru, Uruguay</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, HRtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/germandyzenchauz/" target="_blank">German Dyzenchauz</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=GoIntegro" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Gointegro" title="Twitter" target="_blank">@Gointegro</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (San Francisco, CA)  stage-seed country-Argentina sector-bigdata">
	<div>
		<h1><a href="http://www.grandata.com/" title="GranData" target="_blank">GranData</a></h1>
		<div class="listorg_description"><p>Grandata integrates first-party and telco partner data to understand key market trends, predict customer behavior.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (San Francisco, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, USA (San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>AI, Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/mtravizano/" target="_blank"> Matias Travizano </a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="https://axventure.my/" _blank"="">AxVentures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=GranData" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/GrandataCorp" title="Twitter" target="_blank">@GrandataCorp</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-expansion country-Argentina country-Brazil country-Colombia country-Mexico country-Peru sector-ecommerce">
	<div>
		<h1><a href="https://www.groupon.cl/" title="Groupon LatAm" target="_blank">Groupon LatAm</a></h1>
		<div class="listorg_description"><p>Groupon LatAm operates a deal-of-the-day website that offers discounted gift certificates usable at local or national companies.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Colombia, Chile, Mexico,  Peru</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://cl.linkedin.com/in/felipe-henriquez-meyer-2a5a6466" target="_blank">Felipe Henriquez Meyer </a>: Chairman</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.mountainnazca.com/index.html" _blank"="">Mountain Nazca</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Groupon LatAm" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/GrouponEmpleos" title="Twitter" target="_blank">@GrouponEmpleos</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="http://www.4vets.com.br/" title="Grupo 4Vets" target="_blank">Grupo 4Vets</a></h1>
		<div class="listorg_description"><p>Grupo 4vets connects the distribution and retail network of veterinary and animal health products in Brazil and Latin America. Through its digital business platform, it connects industries and distributors with veterinarians, clinics, pet shops, farm shops and farmers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, Animal Care</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/bernardo-arrospide-6801b6/?ppe=1" target="_blank">Bernardo Arrospide</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Grupo 4Vets" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/4vetsBR" title="Twitter" target="_blank">@4vetsBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="https://www.vivareal.com.br/" title="Grupo ZAP" target="_blank">Grupo ZAP</a></h1>
		<div class="listorg_description"><p>VivaReal is an online real estate marketplace that connects buyers, sellers, and renters with properties in Brazil.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lucasvargas/" target="_blank">Lucas Vargas</a>: CEO, <a href="https://www.linkedin.com/in/brianrequarth/?ppe=1" target="_blank">Brian Requarth</a>: Co-Founder &amp; Executive Chairman</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.sparkcapital.com/" _blank"="">Spark Capital</a>, <a href="https://leadedgecapital.com/" _blank"="">Lead Edge Capital</a>, <a href="http://www.fjlabs.com" _blank"="">FJ Labs</a>, Valiant Capital Partners</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Grupo ZAP" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/VivaReal" title="Twitter" target="_blank">@VivaReal</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech">
	<div>
		<h1><a href="https://www.guiabolso.com.br/" title="GuiaBolso" target="_blank">GuiaBolso</a></h1>
		<div class="listorg_description"><p>GuiaBolso is a personal finance platform that connects Brazilian consumers to financial products via an in-app marketplace with real-time origination.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/thiagoalvarez/" target="_blank">Thiago Alvarez</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://ribbitcap.com" _blank"="">Ribbit Capital</a>, <a href="http://qedinvestors.com" _blank"="">QUED Investors</a>, <a href="https://www.omidyar.com" _blank"="">Omidyar Network</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a>, <a href="https://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a> , <a href="http://www.vostokemergingfinance.com" _blank"="">Vostok Emerging Finance</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=GuiaBolso" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/GuiaBolso" title="Twitter" target="_blank">@GuiaBolso</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-transportation">
	<div>
		<h1><a href="http://www.guichevirtual.com.br/" title="Guichê Virtual" target="_blank">Guichê Virtual</a></h1>
		<div class="listorg_description"><p>Guichê Virtual is an online bus booking platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Transportation</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/thiagomcarvalho/" target="_blank">Thiago Carvalho</a>:Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Guichê Virtual" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/GuicheVirtual" title="Twitter" target="_blank">@GuicheVirtual</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (New York, NY)  stage-expansion country-Brazil country-Chile country-Spain country-Mexico country-Uruguay">
	<div>
		<h1><a href="https://www.gympass.com/" title="Gympass" target="_blank">Gympass</a></h1>
		<div class="listorg_description"><p>Gympass is a platform that empowers companies to engage their workforce in physical activity by providing access to the largest global network of workout facilities.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (New York, NY)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Chile, Germany,  France, Ireland, Italy, Mexico, Netherlands, Portugal, San Marino, Spain, UK, Uruguay, USA</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, HealthTech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/cesar-carvalho-19579714/" target="_blank">Cesar Carvalho</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.atomico.com" _blank"="">Atomico</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.generalatlantic.com" _blank"="">General Atlantic</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Gympass" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/gympass" title="Twitter" target="_blank">@gympass</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-cleantech">
	<div>
		<h1><a href="http://www.h3polimeros.com.br/" title="H3 Polímeros" target="_blank">H3 Polímeros</a></h1>
		<div class="listorg_description"><p>H3 Polímeros manufactures polyamide (nylon) plastic resins from textile waste through proprietary technology.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>CleanTech/Alternative/Renewable Energy</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://inseedinvestimentos.com.br/en/" _blank"="">Inseed Investimientos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=H3 Polímeros" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="http://www.gohiper.com.br/" title="Hiper" target="_blank">Hiper</a></h1>
		<div class="listorg_description"><p>Hiper is a sales and store management software for inventory monitoring and financial control.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/tiagovailati/" target="_blank">Tiago Vailati</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.cventures.com.br/" _blank"="">Cventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Hiper" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-marketing">
	<div>
		<h1><a href="https://www.hive.com.br/" title="Hive Digital Media" target="_blank">Hive Digital Media</a></h1>
		<div class="listorg_description"><p>Hive is a digital marketing platform focused in the development, licening, and operation of digital platforms.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2004</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/mitikazu/" target="_blank">Mitikazu Lisboa</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://bozanoinvestimentos.com.br/" _blank"="">Bozano Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Hive Digital Media" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/hivedigital" title="Twitter" target="_blank">@hivedigital</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early sector-ecommerce">
	<div>
		<h1><a href="https://www.hogaru.com/" title="Hogaru" target="_blank">Hogaru</a></h1>
		<div class="listorg_description"><p>Hogaru provides professional cleaning services for small and medium sized businesses in Latin America.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/matteocera/" target="_blank"> Mateo Cera </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.velumventures.com" _blank"="">Velum Ventures</a>, <a href="https://theventure.city/" _blank"="">The Venture City</a>, <a href="http://www.ycombinator.com/" _blank"="">Y Combinator</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Hogaru" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/HogaruCol" title="Twitter" target="_blank">@HogaruCol</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-hospitality sector-travel">
	<div>
		<h1><a href="https://www.hurb.com/" title="Hurb (formerly Hotel Urbano)" target="_blank">Hurb (formerly Hotel Urbano)</a></h1>
		<div class="listorg_description"><p>Hotel Urbano<br>
Hotel Urbano is an online platform that enables travelers to find and reserve accommodation and activities.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hospitality, Travel &amp; Tourism</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/joaoricardomendes/" target="_blank">João Ricardo Mendes</a>: Co-Founder &amp; Co-CEO, <a href="https://www.linkedin.com/in/jmendes2/" target="_blank">Jose Eduardo Mendes</a>: Co-Founder &amp; Co-CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p>Tiger Global Management, <a href="https://www.insightpartners.com" _blank"="">Insight Venture Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Hurb (formerly Hotel Urbano)" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.i.systems.com.br/" title="I.Systems" target="_blank">I.Systems</a></h1>
		<div class="listorg_description"><p>I.Systems provides advanced control solutions for industrial processes.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Software, AI</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/igor-santiago-8b58439/" target="_blank">Igor Santiago</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.pitangainvest.com.br/pt/" _blank"="">Fundo Pitanga</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=I.Systems" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-Brazil country-Mexico country-Uruguay">
	<div>
		<h1><a href="http://www.iguanafix.com/" title="Iguana Fix" target="_blank">Iguana Fix</a></h1>
		<div class="listorg_description"><p>IguanaFix is a marketplace for services for individuals, households, cars, and enterprises.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Mexico, Uruguay</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/matiasmrecchia/" target="_blank">Matias Recchia</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://angelventures.vc/" _blank"="">Angel Ventures</a>, <a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, <a href="http://www.temasek.com.sg" _blank"="">Temasek</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Iguana Fix" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/IguanaFix" title="Twitter" target="_blank">@IguanaFix</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early country-USA sector-marketing">
	<div>
		<h1><a href="https://www.inlocomedia.com/" title="In Loco Media" target="_blank">In Loco Media</a></h1>
		<div class="listorg_description"><p>In Loco is a location tech company with solutions for media, apps and anti-fraud.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, USA (New York, NY, San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/andreferraz/" target="_blank">Andre Ferraz </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.naspers.com" _blank"="">Naspers</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=In Loco Media" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/inlocoglobal" title="Twitter" target="_blank">@inlocoglobal</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion country-Mexico sector-saas">
	<div>
		<h1><a href="http://www.infracommerce.com.br/" title="Infracommerce" target="_blank">Infracommerce</a></h1>
		<div class="listorg_description"><p>Infracommerce provides e-Commerce service solutions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, e-Commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/kai-schoppen-04a7a122/" target="_blank">Kai Schoppen</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.flybridge.com" _blank"="">Flybridge Capital Partners</a>, <a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Infracommerce" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.ingresse.com/" title="Ingresse" target="_blank">Ingresse</a></h1>
		<div class="listorg_description"><p>Ingresse is a social ticketing company that enables people to discover the latest concerts, nightclubs, parties, and theater plays.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/benarros/" target="_blank">Gabriel Benarros</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a>, <a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, <a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ingresse" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Ingresse" title="Twitter" target="_blank">@Ingresse</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-biotechnology">
	<div>
		<h1><a href="http://www.inprenha.com.br/" title="Inprenha" target="_blank">Inprenha</a></h1>
		<div class="listorg_description"><p>Inprenha Biotechnology develops products and solutions for the animal reproduction and genetics market.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Biotechnology</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ricardo-silva-66a489150/" target="_blank">Ricardo Silva</a>: Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Inprenha" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="https://www.instacarro.com/" title="Instacarro" target="_blank">Instacarro</a></h1>
		<div class="listorg_description"><p>InstaCarro is a car buying and selling service that offers a possibility of selling cars in up to one hour.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/diegofischer/" target="_blank">Diego Fischer</a>: Co-Founder &amp; CEO                                                    <a href="https://www.linkedin.com/in/lcafici/" target="_blank"> Luca Cafici </a>: COO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://lumiacapital.com/" _blank"="">Lumia Capital</a>, <a href="http://www.globalfounders.vc" _blank"="">Global Founders Capital</a>, <a href="http://risecapital.com" _blank"="">Rise Capital</a>, <a href="https://fundersclub.com" _blank"="">FundersClub</a>, <a href="http://www.fjlabs.com" _blank"="">FJ Labs</a>, <a href="http://www.bigsurventures.es" _blank"="">Big Sur Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Instacarro" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/instacarro" title="Twitter" target="_blank">@instacarro</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-bigdata">
	<div>
		<h1><a href="http://www.intellibrand.ai/" title="Intellibrand" target="_blank">Intellibrand</a></h1>
		<div class="listorg_description"><p>Intellibrand is a data analytics company for sales and marketing.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/chau-sanh-03a214/" target="_blank">Chau Sanh</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Intellibrand" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-seed">
	<div>
		<h1><a href="https://www.ipsumapp.co/" title="Ipsum" target="_blank">Ipsum</a></h1>
		<div class="listorg_description"><p>IPSUM is a provider of an online productivity management platform for the construction industry.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/francogiaquinto/" target="_blank">Franco Giaquinto</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.manutaraventures.com//" _blank"="">Manutara Ventures</a>,  <a href="https://www.cemexventures.com/" _blank"="">CEMEX Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ipsum" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ipsumapp" title="Twitter" target="_blank">@ipsumapp</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early">
	<div>
		<h1><a href="https://ivoy.mx/" title="iVoy" target="_blank">iVoy</a></h1>
		<div class="listorg_description"><p>iVoy offers businesses and individuals with a range of delivery and logistics services.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alvaro-de-juan-uriarte-15824321/" target="_blank"> Alvaro de Juan Uriarte </a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.variv.com/" _blank"="">Variv Capital</a>, <a href="http://www.dilacapital.com/?lang=en" _blank"="">Dila Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=iVoy" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/iVoy_" title="Twitter" target="_blank">@iVoy_</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-healthcare">
	<div>
		<h1><a href="http://jasaudeanimal.com.br/" title="JÁ Saúde Animal" target="_blank">JÁ Saúde Animal</a></h1>
		<div class="listorg_description"><p>J.A Animal Health specialized in the development, manufacturing, and marketing of products for animal health.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2002</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p>José Abdo Andrade Hellu: Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.crp.com.br/en/" _blank"="">CRP Companhia de Participações</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=JÁ Saúde Animal" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-UK  stage-early country-Argentina country-Brazil country-USA sector-marketing">
	<div>
		<h1><a href="http://jampp.com/" title="Jampp" target="_blank">Jampp</a></h1>
		<div class="listorg_description"><p>Jampp is a performance marketing platform for acquiring and retargeting mobile customers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>UK</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Germany, Singapore, South Africa, USA (San Francisco, CA), UK</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/martinanazco/" target="_blank">Martin Añazco</a>: Co-Founder, <a href="https://www.linkedin.com/in/diegomeller/" target="_blank">Diego Meller</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Jampp" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/jampp" title="Twitter" target="_blank">@jampp</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Palo Alto, CA)  stage-early country-Argentina sector-bigdata">
	<div>
		<h1><a href="http://junar.com/" title="Junar" target="_blank">Junar</a></h1>
		<div class="listorg_description"><p>Junar offers a cloud-based open data platform for businesses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Palo Alto, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, USA (San Francisco Bay Area)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Govtech, Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/javierpajaro/" target="_blank">Javier Pájaro</a>: Co-Founder &amp; CTO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurus.com/AURUS/Inicio#" _blank"="">Aurus</a>, <a href="http://www.australcap.com" _blank"="">Austral Capital</a>, <a href="www.caraov.com/" _blank"="">Carao Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Junar" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/junar" title="Twitter" target="_blank">@junar</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-legal">
	<div>
		<h1><a href="http://www.jusbrasil.com.br/" title="JusBrasil" target="_blank">JusBrasil</a></h1>
		<div class="listorg_description"><p>JusBrasil is a legal platform that connects consumers and attorneys through legal information.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Legal</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/murtadaniel/" target="_blank">Daniel Murta</a>: Co-Founder &amp; Co-CEO, <a href="https://www.linkedin.com/in/costarafael/?locale=en_US" target="_blank">Rafael Costa</a>: Co-Founder &amp; Co-CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://foundersfund.com" _blank"="">Founders Fund</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.bertelsmann.com/divisions/bertelsmann-investments/#st-1" _blank"="">Bertelsmann</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=JusBrasil" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Jusbrasil" title="Twitter" target="_blank">@Jusbrasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-seed sector-bigdata sector-social">
	<div>
		<h1><a href="https://www.karmapulse.com/" title="KarmaPulse" target="_blank">KarmaPulse</a></h1>
		<div class="listorg_description"><p>KarmaPulse is a social media analytics company for businesses in the Spanish speaking market.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Big Data, Social Networks</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ximena-i%2525C3%2525B1igo-3a78a93/" target="_blank">Ximena Iñigo</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ignia.mx/" _blank"="">IGNIA </a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=KarmaPulse" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/KarmaPulse" title="Twitter" target="_blank">@KarmaPulse</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-seed sector-ecommerce">
	<div>
		<h1><a href="http://www.kavak.com/" title="Kavak" target="_blank">Kavak</a></h1>
		<div class="listorg_description"><p>Kavak is an online marketplace for used cars.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/carlosjuliogarciaottati/" target="_blank">Carlos Garcia Ottati</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.mountainnazca.com/index.html" _blank"="">Mountain Nazca</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Kavak" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-social">
	<div>
		<h1><a href="http://www.kekanto.com/" title="Kekanto" target="_blank">Kekanto</a></h1>
		<div class="listorg_description"><p>Kekanto is a social network that can be used to exchange reviews and recommendations on places and services.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Social Networks</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/fernandookumura/" target="_blank">Fernando Okumura</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.accel.com" _blank"="">Accel Partners</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, W7</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Kekanto" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/kekanto" title="Twitter" target="_blank">@kekanto</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-seed country-Brazil sector-edtech">
	<div>
		<h1><a href="https://www.kinedu.com/" title="Kinedu" target="_blank">Kinedu</a></h1>
		<div class="listorg_description"><p>Kinedu offers content and tools for parents wishing to improve their children’s development.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/luisgza/?locale=en_US" target="_blank"> Luis Garza </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.dilacapital.com/?lang=en" _blank"="">Dila Capital</a>, <a href="http://www.psm.org.mx/" _blank"="">Promotora Social Mexico</a>, <a href="www.socialcapital.com/" _blank"="">Social Capital</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Kinedu" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/kinedu" title="Twitter" target="_blank">@kinedu</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-fintech">
	<div>
		<h1><a href="http://www.konfio.mx/" title="Konfio" target="_blank">Konfio</a></h1>
		<div class="listorg_description"><p>Konfio is an online lending platform for small businesses in Mexico that uses data for rapid credit assessment.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/davidaranaley/" target="_blank">David Arana</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.accion.org/investments" _blank"="">Accion</a>, <a href="http://www.quona.com" _blank"="">Quona Capital</a>, <a href="http://qedinvestors.com" _blank"="">QUED Investors</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.jaguarvc.com" _blank"="">Jaguar Ventures</a>, <a href="https://www.ifc.org/" _blank"="">International Finance Corporation</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Konfio" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Konfío" title="Twitter" target="_blank">@Konfío</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion sector-fintech sector-lending">
	<div>
		<h1><a href="http://www.kubofinanciero.com/" title="Kubo Financiero" target="_blank">Kubo Financiero</a></h1>
		<div class="listorg_description"><p>Kubo.Financiero is a Mexican P2P Lending Platform regulated by local authorities.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/vicente-fenoll-6751282b/?locale=en_US" target="_blank">Vicente Fenoll</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a>, <a href="http://www.bamboocp.com/" _blank"="">Bamboo Capital Partners</a>, <a href="http://capitalimx.com/es" _blank"="">Capital Invent</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Kubo Financiero" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/kubofinanciero" title="Twitter" target="_blank">@kubofinanciero</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-fintech">
	<div>
		<h1><a href="https://kueski.com/en" title="Kueski" target="_blank">Kueski</a></h1>
		<div class="listorg_description"><p>Kueski is an online micro-lender for the middle class of Mexico.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adalf/" target="_blank">Adalberto Flores</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.variv.com/" _blank"="">Variv Capital</a>, <a href="http://crunchfund.com" _blank"="">Crunchfund</a>, <a href="http://coreventuresgroup.com" _blank"="">Core Ventures Group</a>, <a href="http://angelventures.vc/" _blank"="">Angel Ventures</a>, <a href="http://www.rglobal.com" _blank"="">Richmond Global Ventures</a>, <a href="http://risecapital.com" _blank"="">Rise Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Kueski" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MiKueski" title="Twitter" target="_blank">@MiKueski</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-cleantech">
	<div>
		<h1><a href="http://www.lamiecco.com.br/" title="Lamiecco" target="_blank">Lamiecco</a></h1>
		<div class="listorg_description"><p>Lamiecco develops and produces PET laminates for furnishings and building elements.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>CleanTech/Alternative/Renewable Energy</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p>Alexandre Figueiró: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://inseedinvestimentos.com.br/en/" _blank"="">Inseed Investimientos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Lamiecco" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Argentina country-Chile country-Mexico sector-ecommerce">
	<div>
		<h1><a href="https://www.lentesplus.com/co" title="LentesPlus" target="_blank">LentesPlus</a></h1>
		<div class="listorg_description"><p>Lentesplus is an online retailer for contact lenses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Chile, Colombia, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/jaime-oriol-miranda-10262418/" target="_blank">Jaime Oriol</a>: Co-Founder , <a href="https://www.linkedin.com/in/diego-mari%C3%B1o-6325588/" target="_blank">Diego Mariño</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ignia.mx/" _blank"="">IGNIA </a>, <a href="http://www.stellamaris.mx/" _blank"="">Stella Maris Partners</a>, <a href="http://www.inqlab.co/" _blank"="">InQLab</a>
</p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=LentesPlus" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.levee.com.br/" title="LEVEE" target="_blank">LEVEE</a></h1>
		<div class="listorg_description"><p>Levee is a machine learning company that increases enterprise productivity.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, HRtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/jacobrosenbloom/?ppe=1" target="_blank">Jacob Rosenbloom</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://www.varaderocapital.com/index.html" _blank"="">Varadero Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=LEVEE" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/EmpregoLigado" title="Twitter" target="_blank">@EmpregoLigado</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (San Mateo, CA)  stage-early sector-healthcare">
	<div>
		<h1><a href="http://www.levitamag.com/" title="Levita Magnetics" target="_blank">Levita Magnetics</a></h1>
		<div class="listorg_description"><p>Levita Magnetics develops magnetic surgical platforms for less invasive surgery.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (San Mateo, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>USA (San Mateo, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/albertorodrigueznavarro/" target="_blank">Alberto Rodriguez-Navarro, MD FACS</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.aurus.com/AURUS/Inicio#" _blank"="">Aurus</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Levita Magnetics" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/LevitaMagnetics" title="Twitter" target="_blank">@LevitaMagnetics</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion country-Argentina country-Chile country-Colombia country-Ecuador country-Peru sector-ecommerce">
	<div>
		<h1><a href="http://www.linio.com/" title="Linio" target="_blank">Linio</a></h1>
		<div class="listorg_description"><p>Linio is an e-commerce company that offers a wide variety of products online in many categories.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Chile, Colombia, Ecuador, Mexico, Panama, Peru, Venezuela</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/andreas-mjelde-154290124/" target="_blank">Andreas Mjelde</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://livcapital.mx/" _blank"="">LIV Capital</a>, <a href="http://www.holtzbrinck-ventures.com/" _blank"="">Holtzbrinck Ventures</a>, <a href="http://www.accessindustries.com" _blank"="">Access Industries</a>, <a href="http://www.northgate.com" _blank"="">Northgate Capital</a>, <a href="http://www.summitpartners.com" _blank"="">Summit Partners</a>, <a href="http://www.kinnevik.com" _blank"="">Kinnevik AB</a>, <a href="https://www.rocket-internet.com/" _blank"="">Rocket Internet</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Linio" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/LinioMexico" title="Twitter" target="_blank">@LinioMexico</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-retail">
	<div>
		<h1><a href="https://www.livup.com.br/" title="LIV Up" target="_blank">LIV Up</a></h1>
		<div class="listorg_description"><p>Liv Up produces and delivers natural flash frozen food.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Consumer/Retail</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/victor-santos/" target="_blank">Victor Santos</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=LIV Up" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/livupoficial" title="Twitter" target="_blank">@livupoficial</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-Brazil country-Colombia country-Ecuador country-Mexico country-Peru sector-marketing">
	<div>
		<h1><a href="https://www.loganmedia.mobi/en/" title="Logan" target="_blank">Logan</a></h1>
		<div class="listorg_description"><p>Logan is a mobile marketing company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Colombia, Brazil, Ecuador, Guatemala, Mexico, Peru</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/jcgoldy/" target="_blank">Juan Goldy</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.pedralbes-partners.com/" _blank"="">Pedrables Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Logan" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-logistics">
	<div>
		<h1><a href="https://www.loggi.com/" title="Loggi" target="_blank">Loggi</a></h1>
		<div class="listorg_description"><p>Loggi is an urban logistics company that  uses motor an bike curriers for instant deliveries.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Logistics</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/fabienmendez/" target="_blank">Fabien Mendez</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, Dragoneer Investment Group</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Loggi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/LoggiBrazil" title="Twitter" target="_blank">@LoggiBrazil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="https://www.madeiramadeira.com.br/" title="MadeiraMadeira" target="_blank">MadeiraMadeira</a></h1>
		<div class="listorg_description"><p>MadeiraMadeira is an e-commerce store providing construction and finishing materials.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/danielscandian/" target="_blank">Daniel Scandian</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.flybridge.com" _blank"="">Flybridge Capital Partners</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=MadeiraMadeira" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MadeiraMadeira" title="Twitter" target="_blank">@MadeiraMadeira</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-healthcare">
	<div>
		<h1><a href="https://www.inovacoesmagnamed.com.br/" title="Magnamed" target="_blank">Magnamed</a></h1>
		<div class="listorg_description"><p>Magnamed is healthcare company specializing in pulmonary ventilation devices.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2005</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/wataru-ueda-0811a81/" target="_blank">Wataru Ueda</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Magnamed" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/magnamed" title="Twitter" target="_blank">@magnamed</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://magnetis.com.br/" title="Magnetis" target="_blank">Magnetis</a></h1>
		<div class="listorg_description"><p>Magnetis is a digital investment advisor.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lucianot/" target="_blank">Luciano Tavares</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="https://500.co" _blank"="">500 Startups</a>, <a href="http://www.nhinvestimentos.com.br/" _blank"="">NH Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Magnetis" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/magnetis" title="Twitter" target="_blank">@magnetis</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-logistics">
	<div>
		<h1><a href="http://www.mandae.com.br/" title="Mandaê" target="_blank">Mandaê</a></h1>
		<div class="listorg_description"><p>Mandaê offers parcel logistics services for SMBs in Brazil through a digital platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Logistics</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcelofujimoto/" target="_blank">Marcelo Fujimoto</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.kimaventures.com" _blank"="">Kima Ventures</a>, <a href="http://www.performainvestimentos.com/?lang=en" _blank"="">Performa Investimientos </a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mandaê" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MandaeBR" title="Twitter" target="_blank">@MandaeBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-cloud">
	<div>
		<h1><a href="https://www.mandic.com.br/" title="Mandic" target="_blank">Mandic</a></h1>
		<div class="listorg_description"><p>Mandic is a cloud computing solution for businesses in Latin America.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1990</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Cloud Computing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/mauriciocascao/" target="_blank">Mauricio Cascao</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a>, <a href="http://www.intelcapital.com" _blank"="">Intel Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mandic" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MandicSA" title="Twitter" target="_blank">@MandicSA</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-early">
	<div>
		<h1><a href="https://www.mediastre.am/" title="Mediastream" target="_blank">Mediastream</a></h1>
		<div class="listorg_description"><p>Mediastream is a streaming company. It works with audiovisual production, and content administration and distribution. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2007</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/luchoahumada/" target="_blank">Luis Ahumada</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.inversur.com/" _blank"="">Inversur</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mediastream" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/mediastream" title="Twitter" target="_blank">@mediastream</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="http://medicinia.com/" title="Medicinia" target="_blank">Medicinia</a></h1>
		<div class="listorg_description"><p>Medicinia is a communication platform that connect medical teams, patients, medical records and devices of hospitals and healthcare systems.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, HealthTech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rodrigocamilo/" target="_blank">Rodrigo Camilo</a>: Head of Operations</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://rpev.com.br" _blank"="">Redpoint eventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Medicinia" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Medicinia" title="Twitter" target="_blank">@Medicinia</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.meliuz.com.br/" title="Méliuz" target="_blank">Méliuz</a></h1>
		<div class="listorg_description"><p>Méliuz is a cash back marketplace where consumers can earn money while shopping at stores.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/israelsalmen/" target="_blank">Israel Salmen</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://lumiacapital.com/" _blank"="">Lumia Capital</a>, <a href="http://www.fjlabs.com" _blank"="">FJ Labs</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Méliuz" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MeliuzOficial" title="Twitter" target="_blank">@MeliuzOficial</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-saas">
	<div>
		<h1><a href="http://www.memed.com.br/" title="Memed" target="_blank">Memed</a></h1>
		<div class="listorg_description"><p>Memed is an online platform, free and exclusive for doctors, where they can find updated information about any drug available in the Brazilian market and make prescriptions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, HealthTech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ricamoraes/" target="_blank">Ricardo Moraes</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://rpev.com.br" _blank"="">Redpoint eventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Memed" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Memed_Online" title="Twitter" target="_blank">@Memed_Online</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-biotechnology">
	<div>
		<h1><a href="http://www.mendelics.com/" title="Mendelics" target="_blank">Mendelics</a></h1>
		<div class="listorg_description"><p>Mendelics is a genomics-based clinical diagnostics lab. It focuses on rare genetic diseases and cancer.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Biotechnology</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/schlesingerdavid/" target="_blank">David Schlesinger</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p>BBI Financial Gestão de Recursos</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mendelics" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/mendelics" title="Twitter" target="_blank">@mendelics</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion country-USA sector-ecommerce">
	<div>
		<h1><a href="https://www.me.com.br/" title="Mercado Eletrônico" target="_blank">Mercado Eletrônico</a></h1>
		<div class="listorg_description"><p>Mercado Eletrônico is a provider of solutions and services for Supply Chain and Procurement, helping companies to reduce costs and improve performance. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1994</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Portugal, USA</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://br.linkedin.com/in/eduardo-nader-326360" target="_blank">Eduardo Nader</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mercado Eletrônico" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Argentina country-Mexico sector-delivery">
	<div>
		<h1><a href="https://www.mercadoni.com.co/" title="Mercadoni" target="_blank">Mercadoni</a></h1>
		<div class="listorg_description"><p>Mercadoni is a delivery platform that connects users who want to purchase prepared foods, groceries, clothes, and more.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Colombia, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace, Delivery</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pedrocfreire/?ppe=1" target="_blank">Pedro Freire</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a>, <a href="http://www.pegasusvc.com" _blank"="">Pegasus Group</a>, <a href="https://www.movile.com/#/_blank">Movile</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mercadoni" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/mercadoni_co" title="Twitter" target="_blank">@mercadoni_co</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="https://meuspedidos.com.br/" title="Meus Pedidos" target="_blank">Meus Pedidos</a></h1>
		<div class="listorg_description"><p>MeusPedidos is a sales automation software that functions as an ordering system for sales representatives and as a sales force solution to distributors and industries. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>B2B, Sales Management, CRM</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/tiagobrandes/" target="_blank">Tiago Brandes</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Meus Pedidos" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/meus_pedidos" title="Twitter" target="_blank">@meus_pedidos</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://www.mindlab.com.br/" title="MindLab" target="_blank">MindLab</a></h1>
		<div class="listorg_description"><p>Mind Lab creates strategy games to develop the thinking abilities and life skills of K-12 students.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1994</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/valmir-j-pereira-61703912/" target="_blank">Valmir Pereira</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.accel.com" _blank"="">Accel Partners</a>, <a href="http://www.iadb.org/en/inter-american-development-bank,2837.html" _blank"="">Inter American Development Bank (IDB)</a>, <a href="http://www.meritechcapital.com" _blank"="">Meritech Capital Partners</a>, <a href="http://www.mhscapital.com" _blank"="">MHS Capital</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=MindLab" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MindLabBR" title="Twitter" target="_blank">@MindLabBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech">
	<div>
		<h1><a href="http://www.minutoseguros.com.br/" title="Minuto Seguros" target="_blank">Minuto Seguros</a></h1>
		<div class="listorg_description"><p>Minuto Seguros is an insurance brokerage company that specializes in selling insurance policies over the internet.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marceloblay/" target="_blank">Marcelo Blay</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Minuto Seguros" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MinutoSeguros" title="Twitter" target="_blank">@MinutoSeguros</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-Mexico sector-fintech">
	<div>
		<h1><a href="https://moni.com.ar/" title="Moni" target="_blank">Moni</a></h1>
		<div class="listorg_description"><p>Moni is a mobile banking solution for the underbanked in Latam that enables its customers to have instant access to credit and bill payments.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/aleestrada/" target="_blank">Alejandro Estrada</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Moni" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Moniprestamo" title="Twitter" target="_blank">@Moniprestamo</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://mosyle.com/" title="Mosyle" target="_blank">Mosyle</a></h1>
		<div class="listorg_description"><p>Mosyle provides mobile device management for parents and teachers to guide children’s mobile experiences.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Global</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alcyraraujo/" target="_blank"> Alcyr Araujo </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Mosyle" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Mosyle_Edu" title="Twitter" target="_blank">@Mosyle_Edu</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early sector-financial">
	<div>
		<h1><a href="https://www.movilred.co/" title="MovilRed" target="_blank">MovilRed</a></h1>
		<div class="listorg_description"><p>Movilred is a network of banks and transnational services provider in Colombia.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2002</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Financial Services</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a>, <a href="http://www.bamboocp.com/" _blank"="">Bamboo Capital Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=MovilRed" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/_MovilRed" title="Twitter" target="_blank">@_MovilRed</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-seed country-USA sector-saas">
	<div>
		<h1><a href="https://mural.co/" title="MURAL" target="_blank">MURAL</a></h1>
		<div class="listorg_description"><p>MURAL offers a cloud-based solution for design teams through a digital whiteboards that enables them to visually explore  challenges and collaborate on researching, brainstorming and designing ideas.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, USA (San Franciso, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/batmelon/" target="_blank">Mariano Suarez-Battan</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.intelcapital.com" _blank"="">Intel Capital</a>, <a href="http://altaventures.com/" _blank"="">Alta Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=MURAL" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/MURAL" title="Twitter" target="_blank">@MURAL</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion country-Mexico country-USA sector-fintech">
	<div>
		<h1><a href="http://www.muxi.com.br/portugues/?lid=2%2F" title="Muxi" target="_blank">Muxi</a></h1>
		<div class="listorg_description"><p>Muxi provides intelligent payment solutions for businesses that manage electronic transactions.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Mexico, USA</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/paulo-guzzo-192300" target="_blank">Paulo Guzzo</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://confrapar.com.br/en//" _blank"="">Confrapar</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Muxi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-expansion country-Brazil country-Chile country-Ecuador country-Mexico country-Peru">
	<div>
		<h1><a href="http://www.navent.com/" title="Navent" target="_blank">Navent</a></h1>
		<div class="listorg_description"><p>Navent is a real estate and jobs classifieds company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1999</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile, Ecuador, Mexico, Panama, Peru, Venezuela</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/nicolás-tejerina-3a6203/" target="_blank">Nicolás Tejerina</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a>, Tiger Global Management</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Navent" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/OpenNavent" title="Twitter" target="_blank">@OpenNavent</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-mobility sector-saas">
	<div>
		<h1><a href="http://navita.com.br/" title="Navita" target="_blank">Navita</a></h1>
		<div class="listorg_description"><p>Navita performs security and mobility management for mobile devices.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2003</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Mobility</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/dariva/" target="_blank">Roberto Dariva</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.intelcapital.com" _blank"="">Intel Capital</a>, <a href="http://www.dlminvista.com.br" _blank"="">DLM Invista</a>, <a href="http://www.investtech.com.br/?lang=en" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Navita" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/NavitaBR" title="Twitter" target="_blank">@NavitaBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-healthcare">
	<div>
		<h1><a href="https://neoprospecta.com/en/" title="Neoprospecta" target="_blank">Neoprospecta</a></h1>
		<div class="listorg_description"><p>Neoprospecta is a biotechnology company that aims to develop  solutions for large scale microorganism molecular analysis and diagnosis.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lfelipedeoliveira/" target="_blank"> Luiz Felipe Valter de Oliveira</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.cventures.com.br/" _blank"="">Cventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Neoprospecta" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/neoprospecta" title="Twitter" target="_blank">@neoprospecta</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion country-USA sector-bigdata sector-saas">
	<div>
		<h1><a href="http://www.neoway.com.br/" title="Neoway" target="_blank">Neoway</a></h1>
		<div class="listorg_description"><p>Neoway is a big data and analytics company that helps clients manage sales by aggregating and curating information about prospective customers and markets.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2002</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, USA (New York, NY)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/jaimedepaula/" target="_blank">Jaime Leonel de Paula Jr</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.accel.com" _blank"="">Accel Partners</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, Pointbreak, QMS Capital, <a href="https://www.temasek.com.sg/en/index.html" _blank"="">Temasek </a>, <a href="http://www.polluxcapital.com.br/" _blank"="">Pollux</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Neoway" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/neoway_nw" title="Twitter" target="_blank">@neoway_nw</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Mexico sector-ecommerce">
	<div>
		<h1><a href="https://www.neumarket.com/" title="Neumarket" target="_blank">Neumarket</a></h1>
		<div class="listorg_description"><p>Neumarket is an e-commerce that sells tires for Spanish speaking Latin America.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/juan-david-gutierrez-00005147/" target="_blank"> Juan David Gutierrez </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.variv.com/" _blank"="">Variv Capital</a>, <a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Neumarket" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/NeumarketCO" title="Twitter" target="_blank">@NeumarketCO</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech sector-saas">
	<div>
		<h1><a href="http://www.nibo.com.br/" title="Nibo" target="_blank">Nibo</a></h1>
		<div class="listorg_description"><p>Nibo is online software that enables companies and accountants to manage and control their finances.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/gabriel-gaspar-940319a/" target="_blank">Gabriel Gaspar</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.vostokemergingfinance.com" _blank"="">Vostok Emerging Finance (VEF)</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Nibo" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/NiboGestao" title="Twitter" target="_blank">@NiboGestao</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-fintech">
	<div>
		<h1><a href="https://www.noverde.com.br/" title="Noverde" target="_blank">Noverde</a></h1>
		<div class="listorg_description"><p>Noverde is a financial services startup that offers personal loans to underbanked consumers in Brazil.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/teixeiraeduardo1/" target="_blank">Eduardo Teixeira</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://ir.domo.com/phoenix.zhtml?c=254712&amp;p=irol-IRHome" _blank"="">Domo Invest</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Noverde" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-fintech">
	<div>
		<h1><a href="https://www.nubank.com.br/" title="Nubank" target="_blank">Nubank</a></h1>
		<div class="listorg_description"><p>Nubank is a mobile-based credit card and financial services business.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/david-vélez-1004875/" target="_blank">David Velez</a>: Co-Founder &amp; CEO     <a href="https://br.linkedin.com/in/eloidechery" target="_blank"> Eloi Dechery </a>: Founding Partner</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://foundersfund.com" _blank"="">Founders Fund</a>, <a href="https://www.sequoiacap.com" _blank"="">Sequoia Capital</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, Tiger Global Management, <a href="http://qedinvestors.com" _blank"="">QUED Investors</a>, <a href="http://ribbitcap.com" _blank"="">Ribbit Capital</a>, DST Global</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Nubank" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/nubankBrazil" title="Twitter" target="_blank">@nubankBrazil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early country-Argentina sector-cloud sector-saas">
	<div>
		<h1><a href="http://www.nubity.com/" title="Nubity" target="_blank">Nubity</a></h1>
		<div class="listorg_description"><p>Nubity manages and monitors cloud servers and apps.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Cloud Computing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/dariohpena/" target="_blank">Dario Peña</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a>, <a href="http://www.escalavc.co/" _blank"="">Escala.VC</a>, <a href="https://500.co" _blank"="">500 Startups</a>, <a href="http://livcapital.mx/" _blank"="">LIV Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Nubity" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/nubity" title="Twitter" target="_blank">@nubity</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-expansion sector-fintech sector-saas">
	<div>
		<h1><a href="http://www.nubox.com/" title="Nubox" target="_blank">Nubox</a></h1>
		<div class="listorg_description"><p>Nubox is a provider of a cloud-based accounting and payroll platform for small businesses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2001</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcos-mahave-cáceres-1716bb16/" target="_blank">Marcos Mahave Cáceres</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Nubox" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/NuboxChile" title="Twitter" target="_blank">@NuboxChile</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Austin, TX)  stage-early country-Mexico sector-hardware">
	<div>
		<h1><a href="http://www.nuve.us/" title="Nuve, INC" target="_blank">Nuve, INC</a></h1>
		<div class="listorg_description"><p>Nuve focuses on fraud prevention for the asset management industry.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Austin, TX)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico, USA (Austin, TX)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hardware</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/antonio-a-1666809/" target="_blank">Antonio Arocha </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a>, <a href="https://www.capitalfactory.com/" _blank"="">Capital Factory</a>, <a href="https://www.silvertonpartners.com/" _blank"="">Silverton Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Nuve, INC" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Nuve" title="Twitter" target="_blank">@Nuve</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-Brazil sector-ecommerce sector-saas">
	<div>
		<h1><a href="https://www.nuvemshop.com.br/" title="NuvemShop / Tienda Nube" target="_blank">NuvemShop / Tienda Nube</a></h1>
		<div class="listorg_description"><p>Nuvem Shop provides e-commerce tools for SMBs.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, e-Commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ssosa/" target="_blank">Santiago Sosa</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="http://www.ignia.mx/" _blank"="">IGNIA </a> , <a href="https://www.elevarequity.com/" _blank"="">Elevar Equity </a> , <a href="https://fjlabs.com/" _blank"="">FJ Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=NuvemShop / Tienda Nube" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/NuvemShop" title="Twitter" target="_blank">@NuvemShop</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-expansion country-USA sector-cloud sector-saas">
	<div>
		<h1><a href="http://o4it.com/" title="O4IT" target="_blank">O4IT</a></h1>
		<div class="listorg_description"><p>O4IT provides a wide range of cloud computing services to enterprises.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2005</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, USA (Miami, FL)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Cloud Computing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/efrainsoler/" target="_blank">Efrain Soler</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=O4IT" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/o4it" title="Twitter" target="_blank">@o4it</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="https://www.ocaanimation.com.br/" title="OCA Filmes" target="_blank">OCA Filmes</a></h1>
		<div class="listorg_description"><p>Oca Animation is a content creation and production studio specialising in 2D and 3D animation.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Media &amp; Entertainment</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/guilherme-alvernaz-12369931/" target="_blank">Guilherme Alvernaz</a>:Director &amp; Animation Director</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://investimage.com.br/en/team/" _blank"="">Investimage Asset Developement</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=OCA Filmes" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early country-LATAM sector-ecommerce">
	<div>
		<h1><a href="http://www.ofi.com.co/" title="OFI" target="_blank">OFI</a></h1>
		<div class="listorg_description"><p>Ofi.com.co is a business supply company that targets three customer profiles: large businesses/government, SMBs and B2C consumers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Latin America</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/peterostroske/?ppe=1" target="_blank">Peter Ostroske</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.velumventures.com" _blank"="">Velum Ventures</a>, <a href="https://sthventures.com/en" _blank"="">South Ventures</a>, <a href="http://risecapital.com" _blank"="">Rise Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=OFI" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/oficomco" title="Twitter" target="_blank">@oficomco</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="https://olist.com/" title="Olist" target="_blank">Olist</a></h1>
		<div class="listorg_description"><p>Olist is a marketplace that enables merchants to access and operate in other marketplaces while providing to those marketplaces with better solutions, quality of service and assortment.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/tiagodalvi/" target="_blank">Tiago Dalvi</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Olist" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/olistbr" title="Twitter" target="_blank">@olistbr</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-erp sector-saas">
	<div>
		<h1><a href="https://www.omie.com.br/" title="Omiexperience" target="_blank">Omiexperience</a></h1>
		<div class="listorg_description"><p>Omie is a cloud based ERP for SMBs.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, ERP</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcelolombardo/" target="_blank">Marcelo Lombardo</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://astellainvest.com/en/" _blank"="">Astella Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Omiexperience" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Boston, MA)  stage-expansion country-Argentina sector-erp sector-security">
	<div>
		<h1><a href="http://www.onapsis.com/" title="Onapsis" target="_blank">Onapsis</a></h1>
		<div class="listorg_description"><p>Onapsis is a cybersecurity and compliance solution for cloud and on-premise ERP and business applications. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Boston, MA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Germany, USA (Boston, MA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/mnunezdc/" target="_blank">Mariano Nunez</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="http://www.tpg.com" _blank"="">TPG</a>, Dragoneer Investment Group, <a href="http://www.406ventures.com" _blank"="">.406 Ventures</a>, <a href="http://www.evolutionequity.com" _blank"="">Evolution Equity Partners</a>, <a href="http://www.slb.com" _blank"="">Schlumberger Limited</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Onapsis" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/onapsis" title="Twitter" target="_blank">@onapsis</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early country-USA sector-bigdata sector-saas sector-social">
	<div>
		<h1><a href="http://web.ondore.com/products/" title="Ondore" target="_blank">Ondore</a></h1>
		<div class="listorg_description"><p>Ondore is a data analytics company that turns social media data into actionable insights for businesses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2005</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico, USA (San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Big Data, Social Networks</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/todd-stein-a0a308/" target="_blank">Todd Stein</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ondore" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Miami, FL)  stage-expansion country-Argentina country-Brazil country-Colombia sector-edtech">
	<div>
		<h1><a href="https://www.openenglish.com/" title="Open English" target="_blank">Open English</a></h1>
		<div class="listorg_description"><p>Open English is an edtech company focused on English language learning for the Latin American &amp; U.S. Hispanic markets.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Miami, FL)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Colombia, USA (Miami, FL), Venezuela</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/andresmoreno/" target="_blank">Andres Moreno</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.insightpartners.com" _blank"="">Insight Venture Partners</a>, <a href="https://www.tcv.com" _blank"="">Technology Crossover Ventures</a>, <a href="http://www.flybridge.com" _blank"="">Flybridge Capital Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Open English" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/OpenEnglish" title="Twitter" target="_blank">@OpenEnglish</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-ecommerce">
	<div>
		<h1><a href="https://www.oqvestir.com.br/" title="OQVestir" target="_blank">OQVestir</a></h1>
		<div class="listorg_description"><p>OQVestir is a luxury e-commerce store offering fashion products such as clothes, shoes and accessories.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/larsleber/" target="_blank">Lars Leber</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.tmg.com.br" _blank"="">TMG Capital</a>, Tiger Global Management</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=OQVestir" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/OQVestir" title="Twitter" target="_blank">@OQVestir</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-edtech">
	<div>
		<h1><a href="https://www.passeidireto.com/" title="Passei Direto" target="_blank">Passei Direto</a></h1>
		<div class="listorg_description"><p>Passei Direto is an online network for students to learn and share study materials.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/salvadorrodrigo/" target="_blank">Rodrigo Salvador</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://bozanoinvestimentos.com.br/" _blank"="">Bozano Investimentos</a>, <a href="www.grupoxango.com/" _blank"="">Grupo Xango</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Passei Direto" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PasseiDireto" title="Twitter" target="_blank">@PasseiDireto</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-ecommerce">
	<div>
		<h1><a href="http://www.petlove.com.br/" title="PetLove" target="_blank">PetLove</a></h1>
		<div class="listorg_description"><p>PetLove is an online store offering a wide range of pet products.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1999</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marciowaldman/" target="_blank">Marcio Waldman</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, Tiger Global Management, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=PetLove" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PetLoveBR" title="Twitter" target="_blank">@PetLoveBR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-ecommerce sector-pets">
	<div>
		<h1><a href="http://www.petsy.mx/" title="Petsy" target="_blank">Petsy</a></h1>
		<div class="listorg_description"><p>Petsy is an online retailer of pet products.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pablo-pedrejón-garc%C3%ADa-7b583427/" target="_blank">Pablo Pedrejón García</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a>, <a href="http://www.dilacapital.com/?lang=en" _blank"="">Dila Capital</a>, <a href="http://capitalimx.com/es" _blank"="">Capital Invent</a>, <a href="http://www.mountainnazca.com/index.html" _blank"="">Mountain Nazca</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Petsy" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/petsymx" title="Twitter" target="_blank">@petsymx</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-marketing">
	<div>
		<h1><a href="https://pig.gi/" title="Pig.gi" target="_blank">Pig.gi</a></h1>
		<div class="listorg_description"><p>Pig.gi is an adtech company. With it’s  app  users win Pig.gi Coins in exchange for engaging with brands and sharing their consumer data, which are then exchangeable for airtime from any mobile operator. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p>Joel Phillips: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://socialatomventures.com/" _blank"="">SocialAtom Ventures</a>, <a href="http://nomadicholdings.com/" _blank"="">Nomadic Investments</a>, <a href="http://www.cisae.com/" _blank"="">CISA Telecomunicaciones</a><br>
<a href="http://www.ideasycapital.com/" _blank"="">Ideas Y Capital</a>, <a href="http://bizrupt.biz/" _blank"="">Bizrupt</a>, <a href="http://www.valurecapital.co/" _blank"="">Valure Capital </a>, <a href="https://cambridgeanalytica.org/" _blank"="">Cambridge Analytica</a>, Ivernet Holdings</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Pig.gi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PiggiApp" title="Twitter" target="_blank">@PiggiApp</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (San Francisco, CA)  stage-early country-Brazil sector-saas">
	<div>
		<h1><a href="http://www.pipefy.com/" title="Pipefy" target="_blank">Pipefy</a></h1>
		<div class="listorg_description"><p>Pipefy offers a customizable platform for workflow management.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (San Francisco, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, USA (San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alessioalionco/" target="_blank">Alessio Alionco</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.trinityventures.com/" _blank"="">Trinity Ventures</a>, <a href="https://fundersclub.com" _blank"="">FundersClub</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Pipefy" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Pipefy" title="Twitter" target="_blank">@Pipefy</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="http://www.pitzi.com.br/" title="Pitzi" target="_blank">Pitzi</a></h1>
		<div class="listorg_description"><p>Pitzi provides insurance for mobile devices.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/dhatkoff/" target="_blank">Daniel Hatkoff</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p>Valiant Capital Partners, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="https://www.thrivecap.com" _blank"="">Thrive Capital</a>, <a href="http://www.flybridge.com" _blank"="">Flybridge Capital Partners</a>, <a href="http://qedinvestors.com" _blank"="">QUED Investors</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Pitzi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PitziBrasil" title="Twitter" target="_blank">@PitziBrasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-healthcare sector-saas">
	<div>
		<h1><a href="http://pixeon.com/" title="Pixeon Medical Systems" target="_blank">Pixeon Medical Systems</a></h1>
		<div class="listorg_description"><p>Pixeon addresses the imaging and workflow needs of diagnostic healthcare providers.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2003</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Healthcare</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rrdacruz/" target="_blank">Roberto Ribeiro da Cruz</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.intelcapital.com" _blank"="">Intel Capital</a>, <a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Pixeon Medical Systems" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/pixeon" title="Twitter" target="_blank">@pixeon</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-seed country-USA sector-edtech">
	<div>
		<h1><a href="https://courses.platzi.com/" title="Platzi" target="_blank">Platzi</a></h1>
		<div class="listorg_description"><p>Online education platform Platzi helps students from all over the world acquire new, in-demand skills and stay up-to-date with the tech industry.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, USA</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/johnfreddyvega/" target="_blank">John Freddy Vega</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.omidyar.com" _blank"="">Omidyar Network,</a><a href="https://500.co" _blank"="">500 Startups</a>,&nbsp;<a href="https://fundersclub.com" _blank"="">FundersClub</a>,<a href="http://www.mountainnazca.com/index.html" _blank"="">Mountain Nazca</a>, <a href="https://www.amasia.vc/" _blank"=""> Amasia Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Platzi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PlatziTeam" title="Twitter" target="_blank">@PlatziTeam</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-seed sector-saas">
	<div>
		<h1><a href="https://www.producteca.com/" title="Producteca" target="_blank">Producteca</a></h1>
		<div class="listorg_description"><p>Producteca is a SaaS company that gives companies the ability to manage their online sales across different marketplaces.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://ar.linkedin.com/in/andreskirschbaum" target="_blank">Andres Kirschbaum</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.jaguarvc.com/&quot;_blank&quot;">Juguar Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Producteca" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-agtech">
	<div>
		<h1><a href="http://promip.agr.br/" title="Promip" target="_blank">Promip</a></h1>
		<div class="listorg_description"><p>Promip is a biotech company that specializes in the production and distribution of stingless bees, predatory mite and parasite wasps. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Agtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcelo-poletti-27522823/" target="_blank"> Marcelo Poletti </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Promip" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Promip" title="Twitter" target="_blank">@Promip</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-healthcare sector-saas">
	<div>
		<h1><a href="http://www.prontmed.com/" title="Prontmed" target="_blank">Prontmed</a></h1>
		<div class="listorg_description"><p>Prontmed is a healthcare company focused in organizing and integrating health information in the Electronic Medical Record market.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1996</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Healthcare</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lasse-koivisto-1b07127/" target="_blank">Lasse Koivisto</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Prontmed" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/prontmed" title="Twitter" target="_blank">@prontmed</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-seed country-Brazil country-Chile country-Colombia country-Uruguay">
	<div>
		<h1><a href="http://www.Properati.com/" title="Properati" target="_blank">Properati</a></h1>
		<div class="listorg_description"><p>Properati.com is an online and mobile platform for the real estate market in Latam.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile, Colombia, México, Panamá, Perú, Uruguay</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/gabrielgruber/" target="_blank">Gabriel Gruber</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="http://www.patagoniaventures.com/#1" _blank"="">Patagonia Ventures</a>, <a href="http://www.neveq.com" _blank"="">Neveq</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Properati" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Properati" title="Twitter" target="_blank">@Properati</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Orange County, CA)  stage-early country-Chile">
	<div>
		<h1><a href="http://www.propertysimple.com/" title="PropertySimple" target="_blank">PropertySimple</a></h1>
		<div class="listorg_description"><p>PropertySimple provides artificial intelligence tools for real estate agents.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Orange County, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile, USA (Orange County, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adrianfisher/" target="_blank">Adrian Fisher</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://magmapartners.com" _blank"="">Magma Partners</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=PropertySimple" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PSimpleUSA" title="Twitter" target="_blank">@PSimpleUSA</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-healthcare">
	<div>
		<h1><a href="http://proradis.com.br/" title="ProRadis" target="_blank">ProRadis</a></h1>
		<div class="listorg_description"><p>ProRadis offers software solutions for health clinics and radiology labs.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/haissan-molaib-504b9322/" target="_blank">Haissan Molaib</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ProRadis" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ProRadis" title="Twitter" target="_blank">@ProRadis</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (San Francisco, CA)  stage-expansion country-Brazil sector-security">
	<div>
		<h1><a href="http://www.psafe.com/" title="Psafe" target="_blank">Psafe</a></h1>
		<div class="listorg_description"><p>Psafe develops security, privacy and performance solutions for mobile.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (San Francisco, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, USA (San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcodemello/" target="_blank">Marco DeMello</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://www.360totalsecurity.com/features/360-total-security-mac/" _blank"="">Qihoo 360 Technology</a>, <a href="http://www.pinnacleven.com/" _blank"="">Pinnacle Ventures</a>, <a href="http://www.redpoint.com/" _blank"="">Redpoint</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Psafe" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/PSafeTecnologia" title="Twitter" target="_blank">@PSafeTecnologia</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="https://www.quintoandar.com.br/" title="Quinto Andar" target="_blank">Quinto Andar</a></h1>
		<div class="listorg_description"><p>QuintoAndar is an apartment rental marketplace that enables a self-service, end-to-end online experience for both tenants and landlords.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/gabrielbraga/" target="_blank">Gabriel Braga</a>:<br>
Co-founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.acaciacp.com/index.php" _blank"="">Acacia Partners</a>, <a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Quinto Andar" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/quintoandar" title="Twitter" target="_blank">@quintoandar</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Mexico sector-delivery sector-logistics">
	<div>
		<h1><a href="http://www.rappi.com/" title="Rappi" target="_blank">Rappi</a></h1>
		<div class="listorg_description"><p>Rappi is a marketplace that connects users who want to purchase prepared foods, groceries, clothes, and more.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Logistics, Delivery</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/simonborrero/" target="_blank" rel="noopener">Simón Borrero</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br">Redpoint eventures</a>, <a href="https://www.onevc.vc/">ONEVC</a>,&nbsp;<a href="https://a16z.com/">Andreessen Horowitz</a>, <a href="https://www.monashees.com.br/">Monashees+</a>, <a href="https://foundationcapital.com/">Foundation Capital</a>, <a href="http://www.ycombinator.com/">Y Combinator</a>, <a href="https://fundersclub.com">FundersClub</a>, <a href="https://www.sequoiacap.com">Sequoia Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Rappi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/RappiColombia, RappiMexico" title="Twitter" target="_blank">@RappiColombia, RappiMexico</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://recargapay.com.br/" title="RecargaPay" target="_blank">RecargaPay</a></h1>
		<div class="listorg_description"><p>RecargaPay is a mobile payments &amp; wallet.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rodrigoteijeiro/" target="_blank">Rodrigo Teijeiro</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://theventure.city/" _blank"="">The Venture City</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=RecargaPay" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/recargapay" title="Twitter" target="_blank">@recargapay</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-seed country-Chile country-Colombia">
	<div>
		<h1><a href="http://www.renuevatucloset.com/" title="Renueva tu closet / Renová tu Vestidor" target="_blank">Renueva tu closet / Renová tu Vestidor</a></h1>
		<div class="listorg_description"><p>Renová tu Vestidor is a social fashion marketplace.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Chile, Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/cecilia-membrado-4033a88/" target="_blank">Cecilia Membrado</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.axonpartnersgroup.com" _blank"="">Axon Partners Group</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Renueva tu closet / Renová tu Vestidor" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/tuvestidor" title="Twitter" target="_blank">@tuvestidor</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-seed">
	<div>
		<h1><a href="https://www.rescata.co/" title="Rescata" target="_blank">Rescata</a></h1>
		<div class="listorg_description"><p>Rescata, Inc. offers repair, buy and sell, and insurance for smartphones.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/leonardo-miron-20328762/" target="_blank">Leonardo Miron</a>: Co-Founder &amp; Chief Growth Officer</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://capitalimx.com/es" _blank"="">Capital Invent</a>, E Capital</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Rescata" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/rescata_co" title="Twitter" target="_blank">@rescata_co</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-expansion country-Brazil country-Chile country-Colombia country-Mexico country-Peru country-Uruguay">
	<div>
		<h1><a href="https://www.restorando.com/" title="Restorando" target="_blank">Restorando</a></h1>
		<div class="listorg_description"><p>Restorando is an online reservation platform for restaurants.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile, Colombia, Mexico, Panama, Peru, Uruguay</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/franmartin/" target="_blank">Frank Martin</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.flybridge.com" _blank"="">Flybridge Capital Partners</a>, <a href="http://www.emcap.com" _blank"="">Emergence Capital Partners</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.atomico.com" _blank"="">Atomico</a>, <a href="http://www.stormventures.com" _blank"="">Storm Ventures</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="https://www.quasarbuilders.com/#" _blank"="">Quasar</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Restorando" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/restorandobra" title="Twitter" target="_blank">@restorandobra</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-marketing sector-saas">
	<div>
		<h1><a href="http://resultadosdigitais.com.br/" title="Resultados Digitais" target="_blank">Resultados Digitais</a></h1>
		<div class="listorg_description"><p>Resultados Digitais offers a digital marketing management platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ericnsantos/" target="_blank">Eric Santos</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a>, <a href="https://astellainvest.com/en/" _blank"="">Astella Investimentos</a>, <a href="https://www.tpg.com/platforms/tpggrowth" _blank"="">TPG Growth</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Resultados Digitais" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ResDigitais" title="Twitter" target="_blank">@ResDigitais</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="https://www.revelo.com.br/" title="Revelo" target="_blank">Revelo</a></h1>
		<div class="listorg_description"><p>Revelo is a jobs marketplace for positions such as developers, data scientists, digital marketing, etc.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/lachlan-de-crespigny-14969942/" target="_blank"> Lachlan de Crespigny</a>: Co-Founder , <a href="https://www.linkedin.com/in/lmzmendes/" target="_blank">Lucas Mendes</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://seek.com.au" _blank"="">Seek</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Revelo" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/revelojobs" title="Twitter" target="_blank">@revelojobs</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early">
	<div>
		<h1><a href="https://www.ripio.com/en/" title="Ripio" target="_blank">Ripio</a></h1>
		<div class="listorg_description"><p>Ripio is a bitcoin and digital payments company.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Blockchain/Cryptocurrency</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/sebastianserrano/" target="_blank">Sebastian Serrano</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://dcg.co" _blank"="">Digital Currency Group</a>, <a href="https://fundersclub.com" _blank"="">FundersClub</a>, <a href="http://venture.huiyin.com" _blank"="">Huiyin Blockchain Venture</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="http://boost.vc" _blank"="">Boost VC</a>, Medici Ventures, <a href="http://www.draper.vc" _blank"="">Draper Associates</a>, <a href="http://www.fenbushi.vc" _blank"="">Fenbushi Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ripio" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/RipioApp" title="Twitter" target="_blank">@RipioApp</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-marketing sector-content">
	<div>
		<h1><a href="http://rockcontent.com/" title="Rock Content" target="_blank">Rock Content</a></h1>
		<div class="listorg_description"><p>Rock Content is a content marketing company that provides marketing services.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/edmarferreira/" target="_blank">Edmar Ferreira</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a>, <a href="http://napkn.co" _blank"="">Napkn Ventures</a>, <a href="https://www.mdif.org/our-approach/digital-news-ventures" _blank"="">Digital News Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Rock Content" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/rockcontent" title="Twitter" target="_blank">@rockcontent</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-saas">
	<div>
		<h1><a href="http://runrun.it/" title="Runrun.it" target="_blank">Runrun.it</a></h1>
		<div class="listorg_description"><p>Runrun.it is a Team Management, workflow SaaS.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, HRtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/acsoares/" target="_blank">Antonio Carlos Soares</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="https://500.co" _blank"="">500 Startups</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Runrun.it" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/runrun_it" title="Twitter" target="_blank">@runrun_it</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-seed sector-agtech">
	<div>
		<h1><a href="http://www.s4agtech.com/" title="S4 Agtech" target="_blank">S4 Agtech</a></h1>
		<div class="listorg_description"><p>S4 provides risk management solutions to ensure food production.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Agtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/diego-steverlynck-166a045/" target="_blank">Diego Steverlynck</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="http://www.cygnusvc.com/" _blank"="">Cygnus Capital</a>, <a href="https://www.syngentaventures.com" _blank"="">Syngenta Ventures</a>, <a href="http://www.cultivationcapital.com" _blank"="">Cultivation Capital</a>, <a href="http://www.ebricksventures.com/" _blank"="">e.Bricks Ventures </a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=S4 Agtech" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/S4AGTech" title="Twitter" target="_blank">@S4AGTech</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-expansion country-Brazil country-Chile sector-saas sector-transportation">
	<div>
		<h1><a href="http://www.safertaxi.com/" title="SaferTaxi" target="_blank">SaferTaxi</a></h1>
		<div class="listorg_description"><p>Safer offers a mobile apps to locate and book taxis and private cars.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Transportation</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/clemensraemy/" target="_blank">Clemens Raemy</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a> (Prior), <a href="https://www.ottocapital.com" _blank"="">Otto Capital</a>, Draper Associates, Avalancha Ventures</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=SaferTaxi" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/SaferTaxiCL" title="Twitter" target="_blank">@SaferTaxiCL</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion sector-healthcare">
	<div>
		<h1><a href="https://www.salauno.com.mx/index" title="SalaUno" target="_blank">SalaUno</a></h1>
		<div class="listorg_description"><p>SalaUno offers eye care services to the bottom of Mexico’s pyramid (low and middle income population).</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/carlos-orellana-89b5181/" target="_blank">Carlos Orellana</a>: Co-CEO , <a href="https://www.linkedin.com/in/javierokhuysen" target="_blank">Javier Okhuysen</a>: Co-CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.adobecapital.org/">Adobe Capital</a>, <a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=SalaUno" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/SalaUno" title="Twitter" target="_blank">@SalaUno</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.salux.com.br/" title="Salux" target="_blank">Salux</a></h1>
		<div class="listorg_description"><p>Salux is a software provider in the healthcare IT market. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://br.linkedin.com/in/fabricio-avini-4412291" target="_blank"> Luiz Felipe Valter de Oliveira</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.cventures.com.br/" _blank"="">Cventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Salux" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (San Francisco, CA)  stage-early country-Argentina country-Uruguay">
	<div>
		<h1><a href="http://www.satellogic.com/" title="Satellogic" target="_blank">Satellogic</a></h1>
		<div class="listorg_description"><p>Satellogic is a spacetech company that provides imaging in a micro-satellite platform, with the ability to capture photo data of the Earth at 1 meter resolution.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (San Francisco, CA)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Israel, Uruguay, USA (San Francisco, CA)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Earth Observation. Spacetech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ekargieman/" target="_blank">Emiliano Kargieman</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, Tencent, <a href="http://www.cygnusvc.com/" _blank"="">Cygnus Capital</a>, <a href="http://www.pitangainvest.com.br/en/" _blank"="">Pitanga </a>, <a href="http://www.tuesday.vc/" _blank"="">CrunchFund </a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Satellogic" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Satellogic" title="Twitter" target="_blank">@Satellogic</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Uruguay  stage-early country-Argentina country-Brazil country-Chile country-Peru sector-hardware sector-saas">
	<div>
		<h1><a href="http://www.scanntech.com/" title="Scanntech" target="_blank">Scanntech</a></h1>
		<div class="listorg_description"><p>Scanntech provides point-of-sale software to retail stores.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1994</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Uruguay</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile, Paraguay, Peru, Uruguay</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hardware, SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/raúl-polakof-32b119/" target="_blank">Raúl Polakof</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.sequoiacap.com" _blank"="">Sequoia Capital</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Scanntech" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ScanntechUy" title="Twitter" target="_blank">@ScanntechUy</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-seed sector-fintech">
	<div>
		<h1><a href="https://www.sempli.co/" title="Sempli" target="_blank">Sempli</a></h1>
		<div class="listorg_description"><p>Sempli is an online lending platform for small and medium enterprises in Colombia.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/estebanvelascobarrera/" target="_blank"> Esteban Velasco  Vargas </a>: Co-Founder                                                          <a href="https://www.linkedin.com/in/felipe-llano-alvis-3b036026/?locale=en_US" target="_blank"> Felipe Llano Alvis </a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.fomin.org/" _blank"="">FOMIN</a>, <a href="https://www.velumventures.com" _blank"="">Velum Ventures</a>, <a href="https://www.velumventures.com" _blank"="">XTP1s</a>, Grupo Generaciones, <a href="https://english.dggf.nlm" _blank"="">DGGF</a>, XTP1</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Sempli" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/sempli_co" title="Twitter" target="_blank">@sempli_co</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early country-USA sector-bigdata sector-saas">
	<div>
		<h1><a href="http://www.shopperception.com/" title="Shopperception" target="_blank">Shopperception</a></h1>
		<div class="listorg_description"><p>Shopperception provides In-store analytics and activation for grocers, pharmacies and cosmetics.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, USA (Orlando, FL)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/arieldistefano/" target="_blank">Ariel Di Stefano</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com/" _blank"="">Alta Ventures</a>, <a href="http://www.cygnusvc.com/" _blank"="">Cygnus Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Shopperception" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/shopperception" title="Twitter" target="_blank">@shopperception</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-cloud">
	<div>
		<h1><a href="http://skyone.solutions/pb/" title="Sky.One Cloud Solutions" target="_blank">Sky.One Cloud Solutions</a></h1>
		<div class="listorg_description"><p>Sky.One offers cloud computing solutions for companies. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Cloud Computing</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ricardo-brand%C3%A3o-b773102b/" target="_blank">Ricardo Brandão</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.investtech.com.br/?lang=en/" _blank"="">Invest Tech</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Sky.One Cloud Solutions" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/SkyOneSolutions" title="Twitter" target="_blank">@SkyOneSolutions</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://www.smartmei.com.br/" title="SmartMEI" target="_blank">SmartMEI</a></h1>
		<div class="listorg_description"><p>SmartMEI provides banking and automated accounting for micro-entrepreneurs in Brazil.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcellopicchi/" target="_blank">Marcello Picchi</a>: Co-Founder , <a href="https://www.linkedin.com/in/carlosdejavite/" target="_blank">Carlos Dejavite</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.accion.org/venturelab" _blank"="">Accion Venture Lab</a>, <a href="http://canary.com.br/" _blank"="">Canary.vc</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=SmartMEI" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-cleantech">
	<div>
		<h1><a href="http://www.solis.ind.br/" title="Solis Aquecedor Solar" target="_blank">Solis Aquecedor Solar</a></h1>
		<div class="listorg_description"><p>Solis specializes in equipment for solar heating systems.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>CleanTech/Alternative/Renewable Energy</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/luiz-antonio-pinto-049bb46b/" target="_blank">Luiz Antonio Santos Pinto</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://inseedinvestimentos.com.br/en/" _blank"="">Inseed Investimientos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Solis Aquecedor Solar" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-agtech">
	<div>
		<h1><a href="http://_www.speclab.com.br/" title="SpecLab" target="_blank">SpecLab</a></h1>
		<div class="listorg_description"><p>SpecLab is a soil fertility analysis solution.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Agtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/thiago-camargo-719794163/" target="_blank">Thiago Camargo</a>: Director</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=SpecLab" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion sector-fintech">
	<div>
		<h1><a href="https://senorpago.com/" title="Sr. Pago" target="_blank">Sr. Pago</a></h1>
		<div class="listorg_description"><p>Sr.Pago is a mobile point of sale system that enables individuals and small businesses to receive card payments via smartphones and tablets.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pablogonzalezvargas/?ppe=1" target="_blank">Pablo Gonzalez Vargas </a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ignia.mx/" _blank"="">IGNIA </a>,&nbsp; <a href="https://bulltick.com/" _blank"="">Bulltick</a>,&nbsp; <a href="http://www.cen.vc/" _blank"="">Center Electric</a>,<a href="http://www.ebcapital.com.br/" _blank"="">EB Capital</a>&nbsp;</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Sr. Pago" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/senorpago" title="Twitter" target="_blank">@senorpago</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://www.starlinetecnologia.com.br /" title="Starline" target="_blank">Starline</a></h1>
		<div class="listorg_description"><p>Starline is an edtech company that automates the process of creating, applying and correcting exams for small, medium and large educational institutes.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1999</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adriano-rosa-guimarães-a33b0636/" target="_blank">Adriano Rosa Guimarães</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://confrapar.com.br/en/" _blank"="">Confrapar</a>, <a href="http://www.geraventure.com.br" _blank"="">Gera Venture</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Starline" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/_starline" title="Twitter" target="_blank">@_starline</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-security">
	<div>
		<h1><a href="http://site.svatech.com.br/" title="SVA Tech" target="_blank">SVA Tech</a></h1>
		<div class="listorg_description"><p>SVATech offers a programable smart vision system for security and operations.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/roberto-fernandino-b4372a8/" target="_blank">Roberto Fernandino</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://inseedinvestimentos.com.br/en/" _blank"="">Inseed Investimientos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=SVA Tech" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="https://www.tamboro.com.br/" title="Tamboro" target="_blank">Tamboro</a></h1>
		<div class="listorg_description"><p>Tamboro offer a personalized and adaptive learning platform for professionals, students and businesses.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/samara-werner-8302286/" target="_blank">Samara Werner</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Tamboro" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-early country-Peru sector-transportation">
	<div>
		<h1><a href="http://taximo.co/" title="Taximo" target="_blank">Taximo</a></h1>
		<div class="listorg_description"><p>Táximo offers taxi owners and investors to fully operate their taxis.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Peru</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Transportation</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/antoine-dumit-5532003/" target="_blank">Antoine Dumit</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://polymathv.com/" _blank"="">Polymath Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Taximo" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/TaximoColombia" title="Twitter" target="_blank">@TaximoColombia</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Miami, FL)  stage-expansion country-Argentina country-Brazil country-Chile country-Colombia country-CostaRica country-Mexico country-Uruguay sector-fintech sector-saas">
	<div>
		<h1><a href="http://www.technisys.com/" title="Technisys" target="_blank">Technisys</a></h1>
		<div class="listorg_description"><p>Technisys offers digital banking technologies for the financial services industry.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1995</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Miami, FL)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Colombia, Chile, Costa Rica, Luxembourg, Mexico, USA (Miami, FL), Uruguay</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/miguelsantosw/" target="_blank">Miguel Santos</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://altaventures.com" _blank"="">Alta Ventures</a>, <a href="http://endeavor.org/approach/catalyst/" _blank"="">Endeavor Catalyst</a>, <a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a>, <a href="http://www.intelcapital.com" _blank"="">Intel Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Technisys" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/technisys_com" title="Twitter" target="_blank">@technisys_com</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.tecsus.com.br/" title="TecSus" target="_blank">TecSus</a></h1>
		<div class="listorg_description"><p>TecSUS develops solutions for data transmission/reception, control of remote devices, and information management for the water/sanitation, electricity and gas industries.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Software, IoT</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/diogo-branquinho-ramos-a964766/" target="_blank">Diogo Branquinho Ramos</a>: System Engineer</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.primatec.tn/" _blank"="">Primatec</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=TecSus" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Honduras  stage-expansion country-USA sector-retail">
	<div>
		<h1><a href="https://www.tegu.com/" title="Tegu" target="_blank">Tegu</a></h1>
		<div class="listorg_description"><p>Tegu is a toy company that sells magnetic wooden toy blocks.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2006</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Honduras</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Honduras, USA</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Consumer/Retail</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/willhaughey/" target="_blank">Will Haughey</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.corecoholding.com//" _blank"="">CoreCo Private Equity</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Tegu" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-healthcare">
	<div>
		<h1><a href="https://www.meutem.com.br/" title="TEM Saúde" target="_blank">TEM Saúde</a></h1>
		<div class="listorg_description"><p>TEM is a fintech solution for healthcare designed to provide access to high quality medical care at affordable prices.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthcare/Life Sciences</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/fernanda-ferraz-a076225/" target="_blank">Fernanda Ferraza</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=TEM Saúde" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="http://www.tolife.com.br/" title="ToLife" target="_blank">ToLife</a></h1>
		<div class="listorg_description"><p>ToLife is a software provider for the healthcare market.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/leonardo-lima-de-carvalho-81b43925" target="_blank"> Leonardo Lima de Carvalho</a>: Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.voccp.com/english/" _blank"="">Vox Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ToLife" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ToLifeOficial" title="Twitter" target="_blank">@ToLifeOficial</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Colombia  stage-seed sector-fintech">
	<div>
		<h1><a href="https://tpaga.co/" title="Tpaga" target="_blank">Tpaga</a></h1>
		<div class="listorg_description"><p>Tpaga is a payment solution for unbanked consumers in Latin America. It enables consumers to receive and spend money through their cellphone.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2015</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Colombia</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/andres-gutierrez-8516588/" target="_blank">Andres Gutierre</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://greenvisorcapital.com/" _blank"="">Green Visor Capitall</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Tpaga" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/tpagaco" title="Twitter" target="_blank">@tpagaco</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early country-Argentina">
	<div>
		<h1><a href="https://www.trocafone.com/" title="Trocafone" target="_blank">Trocafone</a></h1>
		<div class="listorg_description"><p>Trocafone is an online marketplace to buy and sell used electronics, particularly smartphones.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/guillermo-freire-83915243/?locale=es_ES" target="_blank">Guillermo Freire</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a>, <a href="https://sthventures.com/en" _blank"="">South Ventures</a>, <a href="http://wayra.co/en/" _blank"="">Wayra</a>, <a href="https://500.co" _blank"="">500 Startups</a>, <a href="http://lumiacapital.com/" _blank"="">Lumia Capital</a>, <a href="https://www.quasarbuilders.com/#" _blank"="">Quasar</a>, <a href="http://www.barninvest.com.br/en/" _blank"="">Barn Investimentos</a>, <a href="https://fjlabs.com/" _blank"="">FJ Labs</a>, <a href="https://www.mercadolibre.com/fund" _blank"="">Mercado Libre Fund </a>, <a href="https://www.rocketship.vc/" _blank"="">Rocketship</a>, <a href="https://www.avgfunds.com/castor-ventures/" _blank"="">MIT Castor Ventures Fund</a>, <a href="https://sallfort.com/en/" _blank"="">Sallfort</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Trocafone" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/trocafone" title="Twitter" target="_blank">@trocafone</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early">
	<div>
		<h1><a href="http://tucanton.com/" title="Tu Canton" target="_blank">Tu Canton</a></h1>
		<div class="listorg_description"><p>Tu Canton is an online real estate marketplace.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Proptech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alejandro-pailles-b1858a55/" target="_blank">Alejandro Pailles</a>: Co-Founder &amp; Co-CEO, <a href="https://www.linkedin.com/in/juan-jose-solorzano-a15b6120/" target="_blank">Juan Jose Solorzano</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.dilacapital.com/?lang=en" _blank"="">Dila Capital</a>, <a href="http://angelventures.vc/" _blank"="">Angel Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Tu Canton" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Tucanton" title="Twitter" target="_blank">@Tucanton</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-ecommerce">
	<div>
		<h1><a href="http://www.ubook.com/welcome" title="Ubook" target="_blank">Ubook</a></h1>
		<div class="listorg_description"><p>Ubook is a streaming subscription service that distributes books, magazines, newspapers, interviews, courses, etc. in audio format.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>e-commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/flavioosso/" target="_blank">Flavio Osso</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.m3invest.com.br/" _blank"="">trivella m3 investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ubook" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-mobility">
	<div>
		<h1><a href="https://www.umov.me/" title="uMov.me" target="_blank">uMov.me</a></h1>
		<div class="listorg_description"><p>uMov.me is a mobile application platform for building enterprise mobile applications without programming.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Software, Mobility</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/alexandretrevisan/" target="_blank">Alexandre Trevisan</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.cventures.com.br/" _blank"="">Cventures</a>,  <a href="http://www.crp.com.br/en/" _blank"="">CRP Companhia de Participações</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=uMov.me" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/uMovme" title="Twitter" target="_blank">@uMovme</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-early sector-mobility">
	<div>
		<h1><a href="https://www.urbvan.com/" title="Urbvan" target="_blank">Urbvan</a></h1>
		<div class="listorg_description"><p><span style="font-weight: 400;">Urbvan is an app-based mass transit mobility network that smartly connects inter-city commuting by providing a safe, accessible, comfortable, and efficient experience.</span></p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2016</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Mobility</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/joao-matos-albino-63370030/">Joao Matos Albino</a>: Co-CEO, <a href="https://www.linkedin.com/in/renato-picard-46b35473/">Renato Picard</a>: Co-CEO,</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.dilacapital.com">Dila Capital</a>, <a href="http://mountainnazca.com/">Mountain Nazca</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Urbvan" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Urbvan_Mexico" title="Twitter" target="_blank">@Urbvan_Mexico</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-edtech">
	<div>
		<h1><a href="http://veduca.org/" title="Veduca" target="_blank">Veduca</a></h1>
		<div class="listorg_description"><p>Veduca is an online education platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Edtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/marcelomejla/" target="_blank">Marcelo Mejlachowicz</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://500.co" _blank"="">500 Startups</a>, <a href="http://www.boltventures.com.br" _blank"="">Bolt Ventures</a>, <a href="http://www.digital-education.com" _blank"="">Macmillan Digital Education</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Veduca" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/VeducaBrasil" title="Twitter" target="_blank">@VeducaBrasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early">
	<div>
		<h1><a href="https://www.ventrix.com.br/" title="Ventrix" target="_blank">Ventrix</a></h1>
		<div class="listorg_description"><p>Ventrix is a health tech company focused on telemedicine and cardio monitoring applications.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2005</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Healthtech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/roberto-castro-junior-0a30821/" target="_blank">Roberto Castro Junior</a>: Co-Founder</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://spventures.com.br/" _blank"="">SP Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ventrix" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ventrixhealth" title="Twitter" target="_blank">@ventrixhealth</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-hospitality sector-travel">
	<div>
		<h1><a href="http://www.viajanet.com.br/" title="Viajanet" target="_blank">Viajanet</a></h1>
		<div class="listorg_description"><p>ViajaNet is an online travel agency.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2009</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hospitality, Travel &amp; Tourism</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pnascimento/" target="_blank">Paulo Nascimento</a>: President &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://rpev.com.br" _blank"="">Redpoint eventures</a>, <a href="http://www.redpoint.com/" _blank"="">Redpoint</a>, <a href="http://generalcatalyst.com" _blank"="">General Catalyst</a>, <a href="http://www.pinnacleven.com/" _blank"="">Pinnacle Ventures</a>, <a href="http://www.fjlabs.com" _blank"="">FJ Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Viajanet" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ViajaNet" title="Twitter" target="_blank">@ViajaNet</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://www.vindi.com.br/" title="Vindi / Smartbill" target="_blank">Vindi / Smartbill</a></h1>
		<div class="listorg_description"><p>Vindi is a on-line payment platform focused in subscription and recurring billing. Vindi and Smartbill merged in 2017.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rodrigodantas/" target="_blank"> Rodrigo Dantas  Vargas </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://bozanoinvestimentos.com.br/" _blank"="">Bozano Investimentos</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Vindi / Smartbill" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-seed sector-transportation">
	<div>
		<h1><a href="http://www.virtualavionics.com.br/" title="Virtual Avionics" target="_blank">Virtual Avionics</a></h1>
		<div class="listorg_description"><p>Virtual Avionicsprovides realistic experiences for flight simulation.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Seed/Incubator</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Transportation</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/amauri-sousa-152194/" target="_blank">Amauri Sousa</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://portcapitalllc.com/" _blank"="">Portcapital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Virtual Avionics" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/VirtualAvionics" title="Twitter" target="_blank">@VirtualAvionics</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-hospitality sector-travel">
	<div>
		<h1><a href="http://www.voopter.com.br/" title="Voopter" target="_blank">Voopter</a></h1>
		<div class="listorg_description"><p>Voopter is a travel price comparison website and app.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Hospitality, Travel &amp; Tourism</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/pettersom/" target="_blank">Pettersom Paiva</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.travelcapitalist.com" _blank"="">Travel Capitalist Ventures</a>, <a href="http://www.globalfounders.vc" _blank"="">Global Founders Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Voopter" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/voopter_brasil" title="Twitter" target="_blank">@voopter_brasil</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion country-Argentina country-Chile country-Colombia country-Spain country-Peru country-USA sector-ecommerce sector-saas">
	<div>
		<h1><a href="http://www.vtex.com/" title="VTEX" target="_blank">VTEX</a></h1>
		<div class="listorg_description"><p>VTEX provides cloud based e-commerce platform and omnichannel solutions. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>1999</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina, Brazil, Chile, Colombia, Germany, Peru, Spain, UK, USA (Hollywood, FL)</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS, e-Commerce</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/mariano-gomide-de-faria-vtex/" target="_blank">Mariano Gomide de Faria</a>: Founder &amp; Co-CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.riverwoodcapital.com" _blank"="">Riverwood Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=VTEX" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/vtextruecloud" title="Twitter" target="_blank">@vtextruecloud</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-automotive sector-bigdata">
	<div>
		<h1><a href="http://www.webradar.com/" title="WebRadar" target="_blank">WebRadar</a></h1>
		<div class="listorg_description"><p>WebRadar is a big data analytics solution that operates in three segments: Telecom, Automotive and IoT. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2008</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Big Data</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/adriano-da-rocha-lima-a910b915/" target="_blank">Adriano da Rocha Lima </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.qualcommventures.com" _blank"="">Qualcomm Ventures</a>, <a href="https://www.dgf.com.br" _blank"="">DGF Investimentos</a>, <a href="http://www.intelcapital.com" _blank"="">Intel Capital</a>, Citrix</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=WebRadar" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-early sector-fintech">
	<div>
		<h1><a href="https://weel.com.br/" title="WEEL" target="_blank">WEEL</a></h1>
		<div class="listorg_description"><p>WEEL offers a cash flow management solution for businesses. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2014</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil, Israel</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/shneumark/?ppe=1" target="_blank">Simcha Neumark </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.monashees.com.br/" _blank"="">Monashees+</a>, <a href="http://www.fjlabs.com" _blank"="">FJ Labs</a>, <a href="http://www.mindset.ventures/ " _blank"="">Mindset Venturues</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=WEEL" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/WEEL_BR" title="Twitter" target="_blank">@WEEL_BR</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Mexico  stage-expansion">
	<div>
		<h1><a href="http://weex.mx/" title="Weex" target="_blank">Weex</a></h1>
		<div class="listorg_description"><p>Weex is a Mobile Virtual Operator that offers mobile telephony and data services in Mexico.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Mexico</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Mexico</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Telecommunications</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/risuarez/" target="_blank"> Ricardo Suarez  Vargas </a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.allvp.vc" _blank"="">ALLVP</a>, Coca-Cola</p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Weex" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-marketing sector-content">
	<div>
		<h1><a href="http://www.ycontent.com.br/" title="Ycontent" target="_blank">Ycontent</a></h1>
		<div class="listorg_description"><p>YContent is a video distribution platform.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Adtech &amp; Marketing</p>
	    
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://www.ebricksventures.com" _blank"="">e.Bricks Ventures</a>, <a href="http://www.initial.vc" _blank"="">Initial Capital</a>, <a href="https://500.co" _blank"="">500 Startups</a>, <a href="http://www.rhodium.co.il" _blank"="">Rhodium</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Ycontent" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ycontentco" title="Twitter" target="_blank">@ycontentco</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-USA (Miami, FL)  stage-expansion country-Colombia country-Ecuador country-Mexico sector-fintech">
	<div>
		<h1><a href="http://www.yellowpepper.com/" title="YellowPepper" target="_blank">YellowPepper</a></h1>
		<div class="listorg_description"><p>YellowPepper provides mobile banking and payment solutions to financial institutions in Latam.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2004</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>USA (Miami, FL)</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Colombia, Ecuador, Mexico, USA (Miami, FL), Vietnam</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Fintech</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/sergeelkiner/" target="_blank">Serge Elkiner</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://livcapital.mx/" _blank"="">LIV Capital</a>, <a href="http://www.fondodefondos.com.mx/" _blank"="">Fondo de Fondos</a>, <a href="http://www.ifc.org" _blank"="">International Finance Corporation (IFC)</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=YellowPepper" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion sector-security">
	<div>
		<h1><a href="http://www.zoemob.com/" title="ZoeMob" target="_blank">ZoeMob</a></h1>
		<div class="listorg_description"><p>ZoeMob provides a mobile family safety service that enables users to monitor their family’s whereabouts through smartphones and tablets.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2010</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Digital Security</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/danielavizu/" target="_blank">Daniel Avizú</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://valorcapitalgroup.com" _blank"="">Valor Capital</a>, <a href="http://www.altivia.net.br/" _blank"="">Altivia Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=ZoeMob" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Argentina  stage-early">
	<div>
		<h1><a href="http://www.zolvers.com/" title="Zolvers" target="_blank">Zolvers</a></h1>
		<div class="listorg_description"><p>Zolvers is an online marketplace that allows people to outsource errands such as cleaning, delivery, maintenance, and more. </p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2013</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Argentina</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Argentina</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Marketplace</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/ceciliaretegui/" target="_blank">Cecilia Retegui</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.jaguarvc.com" _blank"="">Jaguar Ventures</a>, <a href="http://www.nxtplabs.com" _blank"="">NXTP Labs</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Zolvers" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/Zolvers_latam" title="Twitter" target="_blank">@Zolvers_latam</a></h3></div>
		</div>
		
	</div>
</li>


<li class="member-directory country-Brazil  stage-expansion">
	<div>
		<h1><a href="http://www.zup.com.br/" title="Zup IT Innovation" target="_blank">Zup IT Innovation</a></h1>
		<div class="listorg_description"><p>Zup IT Innovation offers consulting services for businesses to come online.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2011</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Expansion Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Brazil</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Brazil</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>Business Services</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/bruno-pierobon-a3149536/" target="_blank">Bruno Pierobon</a>: Co-Founder &amp; CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="https://www.kaszek.com" _blank"="">KaszeK Ventures</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Zup IT Innovation" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		
		</div>
		
	</div>
</li>


<li class="member-directory country-Chile  stage-early sector-saas">
	<div>
		<h1><a href="http://www.zyght.com/" title="Zyght" target="_blank">Zyght</a></h1>
		<div class="listorg_description"><p>Zyght is a risk management solution for industrial operations.</p>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div></div>
		<p class="col-2 listorg_founded"><strong>Founded: </strong>2012</p>
		<p class="col-2 listorg_stages"><strong>Stage: </strong>Early Stage</p>
		<p class="col-2 listorg_country_hq"><strong>HQ: </strong>Chile</p>
		<p class="col-2 listorg_locations"><strong>Locations: </strong>Chile</p>
		<p class="listorg_portfolio"><strong>Sectors: </strong>SaaS</p>
	     <p class="listorg_portfolio"><strong>Executive Team: </strong> </p><p><a href="https://www.linkedin.com/in/rcalle/" target="_blank">Rodrigo Calle</a>: CEO</p>
 <p></p>
		<p class="listorg_portfolio"><strong>Investors: </strong></p><p><a href="http://scale.capital/" _blank"="">Scale Capital</a></p>
<p></p>
		
		<div class="directory_buttons">
		<div class="lavcabutton"><h3><a href="https://lavca.org/?s=Zyght" title="Media Coverage" target="_blank">Media Coverage</a></h3></div>
		<div class="lavcabutton lavcabutton-secondary"><h3><a href="https://twitter.com/ZYGHT_Tech" title="Twitter" target="_blank">@ZYGHT_Tech</a></h3></div>
		</div>
		
	</div>
</li>

</ul>


<p><em> This information has been gathered by LAVCA from sources believed to be reliable and from secondary sources that were checked whenever possible, but its accuracy and completeness are not guaranteed. LAVCA shall not be responsible for any inaccuracy unintentionally included in this publication. Readers should consult and rely solely on their own advisers regarding all pertinent information, legal, and accounting issues.</em></p>

<p><em>© 2017 by the Latin American Private Equity & Venture Capital Association. All rights reserved. </em></p>
</div></div>
<div class="hatom-extra" style="display:none !important;visibility:hidden;"><span class="entry-title">Latin American Startup Directory</span> was last modified: <span class="updated"> November 5th, 2018</span> by <span class="author vcard"><span class="fn">Anna Tulchinskaya</span></span></div>					</div>
	</div>

			<div class="tags-share-box hide-tags page-share has-line">
			<div class="post-share">
				<span class="share-title">Share</span>
				<div class="list-posts-share">
										<a target="_blank" href="https://www.facebook.com/sharer/sharer.php?u=https://lavca.org/vc/startup-directory/"><i class="fa fa-facebook"></i><span class="dt-share">Facebook</span></a>
					<a target="_blank" href="https://twitter.com/home?status=Check%20out%20this%20article:%20Latin%20American%20Startup%20Directory%20-%20https://lavca.org/vc/startup-directory/"><i class="fa fa-twitter"></i><span class="dt-share">Twitter</span></a>
					<a target="_blank" href="https://plus.google.com/share?url=https://lavca.org/vc/startup-directory/"><i class="fa fa-google-plus"></i><span class="dt-share">Google +</span></a>
					<a data-pin-do="none" target="_blank" href="https://pinterest.com/pin/create/button/?url=https://lavca.org/vc/startup-directory/&#038;media=https://lavca.org/wp-content/uploads/2017/06/Startup-Directory-LAVCA.png&#038;description=Latin%20American%20Startup%20Directory"><i class="fa fa-pinterest"></i><span class="dt-share">Pinterest</span></a>
				</div>
			</div>
		</div>
	
	
</article>                            </div>
        </div>

        
<div id="sidebar" class="penci-sidebar-content">
	<div class="theiaStickySidebar">
		<aside id="text-126" class="widget widget_text"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Startup Directory Methodology</span></h4>			<div class="textwidget"><p>Startups include companies with operations in <b>Argentina, Brazil, Chile, Colombia, Costa Rica, Ecuador, Mexico, Peru, and Uruguay.</b> Some companies also have operations in other markets, including the US and Europe. </p><br>

<p>Investors include <b>PE and VC firms, and also DFIs, family offices, and other professional investors</b> making private capital commitments to Latin American startups. Investors are based globally. </p><br>

<p><b>STAGES</b><br>
<b>Seed/Incubator:</b> Startup capital to incubate an idea into a company or product.<br>
<b>Early Stage:</b> Startup capital for companies including Series A and B.<br>
<b>Expansion Stage:</b> Capital for companies that have reached a sustainable level of development and may be shipping products including Series C, D, and beyond.</p><br>

<p>This list only includes transactions that were made public by secondary sources or in some instances were confirmed with primary sources by LAVCA as of Dec. 2017</p>

</div>
		</aside><aside id="media_image-8" class="widget widget_media_image"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Startups by Stage</span></h4><img width="575" height="285" src="https://lavca.org/wp-content/uploads/2018/08/stage-graph-2018-startup-directory-UPDATED-575x285.png" class="image wp-image-76163  attachment-medium size-medium" alt="2018 Startups by Stage" style="max-width: 100%; height: auto;" srcset="https://lavca.org/wp-content/uploads/2018/08/stage-graph-2018-startup-directory-UPDATED-575x285.png 575w, https://lavca.org/wp-content/uploads/2018/08/stage-graph-2018-startup-directory-UPDATED-768x381.png 768w, https://lavca.org/wp-content/uploads/2018/08/stage-graph-2018-startup-directory-UPDATED-585x290.png 585w, https://lavca.org/wp-content/uploads/2018/08/stage-graph-2018-startup-directory-UPDATED.png 800w" sizes="(max-width: 575px) 100vw, 575px" /></aside><aside id="text-137" class="widget widget_text"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Contact LAVCA About a Startup</span></h4>			<div class="textwidget"><p>Do you know a startup that qualifies to be on this list, but isn’t represented? Would you like to update information about a listed company for future editions? Please contact <a href="mailto:jruvolo@lavca.org"><strong>LAVCA</strong></a>.</p>
</div>
		</aside><aside id="media_image-7" class="widget widget_media_image"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">LAVCA Directories</span></h4><a href="https://lavca.org/vc/lists/"><img width="1000" height="563" src="https://lavca.org/wp-content/uploads/2018/08/LAVCA-investor-directories.jpg" class="image wp-image-76133  attachment-full size-full" alt="Directories" style="max-width: 100%; height: auto;" srcset="https://lavca.org/wp-content/uploads/2018/08/LAVCA-investor-directories.jpg 1000w, https://lavca.org/wp-content/uploads/2018/08/LAVCA-investor-directories-575x324.jpg 575w, https://lavca.org/wp-content/uploads/2018/08/LAVCA-investor-directories-768x432.jpg 768w, https://lavca.org/wp-content/uploads/2018/08/LAVCA-investor-directories-585x329.jpg 585w" sizes="(max-width: 1000px) 100vw, 1000px" /></a></aside>	</div>
</div>        <div style="clear:both"></div>
<!-- END CONTAINER -->
</div>
<div class="clear-footer"></div>


	<div id="widget-area">
	<div class="container">
	<div class="footer-widget-wrapper">
	<aside id="text-11" class="widget widget_text"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">About LAVCA</span></h4>			<div class="textwidget"><img src="https://lavca.org/wp-content/uploads/2018/09/LAVCA-Logo-Private-Capital-FINAL-08.14.18-small.png"><p>&nbsp; </p>LAVCA is the Association for Private Capital Investment in Latin America, a not-for-profit membership organization dedicated to supporting the growth of private capital in Latin America and the Caribbean through research, education, networking and advocacy. </div>
		</aside>	</div>
	<div class="footer-widget-wrapper">
<aside id="text-12" class="widget widget_text"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">LAVCA Offices</span></h4>			<div class="textwidget"><table><tr><td><img src="https://lavca.org/wp-content/uploads/2016/07/New-York-City.jpg" width="100" height="67"></td><td padding="5px"><strong>&nbsp;&nbsp;&nbsp;&nbsp;New York City:</strong><br />
&nbsp;&nbsp;&nbsp;&nbsp;589 8th Ave, 18th Floor<br />
&nbsp;&nbsp;&nbsp;&nbsp;New York, NY 10031</td></tr></table>
<br /> 
<table><tr><td><img src="https://lavca.org/wp-content/uploads/2016/07/Buenos-Aires-Footer.jpg" width="100" height="66"></td><td padding="5px">&nbsp;&nbsp;&nbsp;&nbsp;<strong>Buenos Aires: </strong><br />
&nbsp;&nbsp;&nbsp;&nbsp;Av. Figueroa Alcorta 3351<br />
&nbsp;&nbsp;&nbsp;&nbsp;Buenos Aires, Argentina, 1425</td></tr></table>
<br />
<em>LAVCA Also Has a Regional Presence in</em>:
<br><br>
 <table><tr><td><img src="https://lavca.org/wp-content/uploads/2016/07/San-Fran-Footer-1.jpg" width="100" height="67"></td><td padding="5px">&nbsp;&nbsp;&nbsp;&nbsp;<b>Silicon Valley:</b><br />
&nbsp;&nbsp;&nbsp;&nbsp;San Francisco, CA</td></tr></table>
<br />
<table><tr><td><img src="https://lavca.org/wp-content/uploads/2016/07/Sao-Paulo-Footer-1.jpg" width="100" height="67"></td><td padding="5px">&nbsp;&nbsp;&nbsp;&nbsp;<b>São Paulo:</b><br>&nbsp;&nbsp;&nbsp;&nbsp;São Paulo, Brazil</b></td></tr></table></div>
		</aside>	</div>
	<div class="footer-widget-wrapper last">
<aside id="nav_menu-3" class="widget widget_nav_menu"><h4 class="widget-title penci-border-arrow"><span class="inner-arrow">Explore LAVCA.org</span></h4><div class="menu-custom-sitemap-container"><ul id="menu-custom-sitemap" class="menu"><li id="menu-item-30375" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30375"><a href="https://lavca.org/about/">About LAVCA</a></li>
<li id="menu-item-30370" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30370"><a href="https://lavca.org/about/board-and-team/">Board, Team &#038; Councils</a></li>
<li id="menu-item-30371" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30371"><a href="https://lavca.org/media/">Media</a></li>
<li id="menu-item-30372" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30372"><a href="https://lavca.org/about/contact-lavca/">Contact Us</a></li>
<li id="menu-item-30373" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30373"><a href="https://lavca.org/membership-benefits/">Membership Benefits</a></li>
<li id="menu-item-30374" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30374"><a href="https://lavca.org/welcome/">Members Only Access</a></li>
<li id="menu-item-30380" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30380"><a href="https://lavca.org/education-and-programs/">Education and Programs</a></li>
<li id="menu-item-30377" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30377"><a href="https://lavca.org/research/">Research</a></li>
<li id="menu-item-30378" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-30378"><a href="https://lavca.org/public-policy-impact/">Policy</a></li>
<li id="menu-item-30379" class="addarrow menu-item menu-item-type-post_type menu-item-object-page current-page-ancestor menu-item-30379"><a href="https://lavca.org/vc/">LAVCA VC</a></li>
</ul></div></aside>		</div>
		</div>
		</div>


<footer id="footer-section">
	<div class="container">
							<div class="footer-logo-copyright footer-not-logo">
				
									<div class="footer-menu-wrap">
					<ul id="menu-footer-menu" class="footer-menu"><li id="menu-item-27324" class="menu-item menu-item-type-post_type menu-item-object-page menu-item-27324"><a href="https://lavca.org/about/contact-lavca/">Contact Us</a></li>
</ul>					</div>
				
									<div id="footer-copyright">
						<p>@2018 - LAVCA. All Rights Reserved. </p>
					</div>
													<div class="go-to-top-parent"><a href="#" class="go-to-top"><span><i class="fa fa-angle-up"></i><br>Back To Top</span></a></div>
							</div>
			</div>
</footer>

</div><!-- End .wrapper-boxed -->

<div id="fb-root"></div>

<div class="edd-free-downloads-modal-wrapper edd-free-downloads"><span class="edd-loading"></span><div id="edd-free-downloads-modal" style="display:none"></div></div>
<!-- DO NOT COPY THIS SNIPPET! &mdash; HubSpot Identification Code -->


</body>
</html>
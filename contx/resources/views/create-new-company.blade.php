@extends('layouts.web')
@section('content')
@include('elements.new-founder')
@include('elements.new-investor')

<style>
.chosen-search-input
{
  width:0px;
}
</style>
<div class="searchArea">
 <div class="container">
    <div class="row">
       <div class="col-12">
          <div class="seachbox">
             <input type="text" name="keyword" class="keyword" value="{{@$keyword}}" placeholder="Search Companies…">
             <button onClick="searchHome()"><img src="{{ asset('web/img/search.svg')}}"></button>
          </div>
       </div>
    </div>
    <div class="row">
       <div class="col-lg-4 col-md-5 order-md-2 col-12 text-center">
          @include('elements.add-company-btn')
       </div>
       @include('elements.main-navbar')
    </div>
 </div>
</div>
<section class="formMain">
  <div class="container">

           <div class="form-header">
              <h4>Company Personal Information</h4>
              <p>Add your new company information below.</p>

           </div>
           <form action="#" id="new-company" method="POST" enctype="multipart/form-data">
              {{csrf_field()}}
              <div class="uploadSection" >
                 <div class="inputFiles">
                  <img class="company-logo" src="#" />
                 </div>
                 <div class="inputFilesActions">
                    <p>Add Your Company Image/Logo</p>
                    <div class="row">
                       <div class="col-md-6 col-7">
                          <div class="uploadImg">
                             <input type='file' name="company-logo" id="company-logo-input"  accept="image/*" onchange="readURL(this);" />
                             <span>Upload Image</span>
                          </div>
                       </div>
                       <div class="col-md-6 col-5">
                          <a class="btn btn-remove remove-logo" id="remove-logo" style="display:none">Remove</a>
                       </div>
                    </div>
                 </div>
              </div>

              <div class="custom-form-main">
                 <div class="row">
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Your Name</label>
                          <input class="form-control" type="text" name="creator_name" placeholder="Enter your full name...">
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Your Email</label>
                          <input class="form-control" type="text" name="creator_email" placeholder="Enter your email...">
                       </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Company Name</label>
                          <input class="form-control" type="text" name="name" id="company_name" placeholder="Enter company name..." >
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Country</label>
                          <select class="form-control" name="country_id">
                            <option value="">Select Country</option>
                             @foreach($countries as $country)

                             <option value="{{$country->country_id}}">{{$country->name}}</option>
                             @endforeach
                          </select>
                       </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Founded In</label>
                          <select class="form-control" name="founded">
                            <option value="">Select Founded In</option>
                             @foreach($founded_years as $year)

                             <option>{{$year}}</option>
                             @endforeach
                          </select>
                       </div>
                    </div>
                    <!-- <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Founder Name</label>
                          <input class="form-control" type="text" name="" placeholder="Enter founder name...">
                       </div>
                    </div> -->

                 <!-- <div class="form-header mt-5 mb-3">
                    <h4>Founders</h4>
                 </div>
                 <ul class="addedFonderList">

                    <li>
                       <span>Jhon Due</span> <a href="javascript://"> <img src="{{asset('new/img/close.png')}}" alt="img"/></a></li>
                    </li>
                    <li>
                       <span>Jhon Due</span> <a href="javascript://"> <img src="{{asset('new/img/close.png')}}" alt="img"/></a></li>
                    </li>
                    <li>
                       <span>Jhon Due</span> <a href="javascript://"> <img src="{{asset('new/img/close.png')}}" alt="img"/></a></li>
                    </li>

                 </ul> -->
                 <div class="col-md-6 col-12">

                    <div class="form-group">
                       <input type="hidden" value="{{@$rand}}" id="rand">
                       <label>Founder Name</label><a id="founder" href="" data-toggle="modal" data-target="#createFounder" onclick="emptydata()"> <span class="addfounderlink">(Not in the list <span style="color:blue">CLICK HERE</span> to add)</span></a>

                       <!-- <select name="founder_name[]" multiple class="allfounders" id="founder-arr" style="visibility:hidden;position: absolute;">

                       </select> -->
                       <select data-placeholder="Choose founders ..." name="founder_name[]" multiple class="chosen-select form-control allfounders" id="founder-arr">
                          @foreach($founders as $founder)
                          <option value="{{$founder->founder_id}}">{{$founder->name}}</option>
                          @endforeach
                       </select>
                         <!-- <label style="display: none;" for="founder_name" id="error-founder" class="error">Choose Founders</label> -->

                       <!-- <div class="chosen-container chosen-container-multi" title="" id="industries_arr_chosen" style="width: 0px;">
                        <ul class="chosen-choices" id="founderss">

                       </ul>
                     </div> -->
                       <!-- <input class="form-control" type="text" name="founder_name" id="founder_name" placeholder="Enter founder name..."> -->



                    </div>
                 </div>
                  </div>


                 <div class="row">
                    <div class="col-12">
                       <div class="form-group">
                          <label>Description</label>
                          <textarea  class="form-control" placeholder="About Company information…" name="description"></textarea>
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Website</label>
                          <input class="form-control" type="text" name="website" id="website_name" placeholder="www.website.com">
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group">
                          <label>Industry</label>
                          <select data-placeholder="Choose industries ..." name="industries[]" multiple class="chosen-select form-control" id="industries-arr" style="width: 100%;">
                             @foreach($industries as $industry)
                             <option value="{{$industry->industry_id}}">{{$industry->name}}</option>
                             @endforeach
                          </select>
                          <label style="display: none;" for="industries" id="error-industry" class="error">Choose Industries</label>
                       </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-12">
                       <div class="form-header mt-5">
                          <h4>Social Profiles Information</h4>
                          <p>Add your social profiles information below.</p>
                       </div>
                    </div>
                 </div>
                 <div class="row">
                    <div class="col-md-6 col-12">
                       <div class="form-group formGrpIcn">
                          <label>Facebook</label>
                          <input class="form-control" type="text" name="facebook" >
                          <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group formGrpIcn">
                          <label>Twitter</label>
                          <input class="form-control" type="text" name="twitter" >
                          <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group formGrpIcn">
                          <label>Linked In</label>
                          <input class="form-control" type="text" name="linkedin" >
                          <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                       </div>
                    </div>
                    <div class="col-md-6 col-12">
                       <div class="form-group formGrpIcn">
                          <label>Instagram</label>
                          <input class="form-control" type="text" name="instagram" >
                          <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                       </div>
                    </div>
                 </div>
              </div>
              <div class="customBtn">
                 <button type="button" class="btn btn-cancel" data-dismiss="modal">Cancel</button>
                 <button type="submit" class="btn btn-primary ml-4">Submit</button>
              </div>
              </form>


  </div>
     </div>

</section>
<script>
$(document).ready(function(){
  $('#keyword').on('change keyup',function(e) {
      if(e.keyCode == 13){
        // var url = "{{route('web-home')}}";
        // var keyword="{{app('request')->input('keyword')}}";
        // var page = 1;
        // var keyword = $('#keyword').val();
        // url += "?page="+page+"&keyword="+keyword;
        // window.location = url;

        var keyword = $('.keyword').val();
        $.ajax({
          url: "/getslug",
          type: "get",
          data:{
            keyword:keyword,
          },
          datatype: "json"
        }).done(function(data){
          var key=data.slug.slug;
          var key2=data.slug;
          if(key2!='1'){
            window.location.href="{{route('web-company-details')}}/"+key;
          }else{
            $('#keyword').focus();
            alert('No Company found');
          }
        });
      }
   });
});

function searchHome(){
     // var url = "{{route('web-home')}}";
     // var keyword="app('request')->input('keyword')";
     // var page = 1;
     // var keyword = $('#keyword').val();
     // url += "?page="+page+"&keyword="+keyword;
     // window.location = url;

     var keyword = $('.keyword').val();
     $.ajax({
       url: "/getslug",
       type: "get",
       data:{
         keyword:keyword,
       },
       datatype: "json"
     }).done(function(data){
       var key=data.slug.slug;
       var key2=data.slug;
       if(key2!='1'){
         window.location.href="{{route('web-company-details')}}/"+key;
       }else{
         $('#keyword').focus();
           alert('No Company found');
       }
     });
 }
</script>
@endsection

<script>

function emptydata(){
  $("#founder_name").val('');
  $("#founder_email").val('');
  $("#faceb").val('');
  $("#twitt").val('');
  $("#linkd").val('');
  $("#insta").val('');
  $('.company-logo').attr('src', '#');

}



</script>

@extends('layouts.web')

@section('content')
   <div class="searchArea">
      <div class="container">
         <div class="row">
            <div class="col-md-8">
               <div class="seachbox">
                  <input type="text" name="keyword" id="keyword" placeholder="Search Companies">
                  <button onClick="searchHome()"><img src="{{ asset('WebAssets/img/search.png')}}"></button>
               </div>
            </div>
            <div class="col-md-4">
               <button type="button" class="create"> CREATE NEW COMPANY</button>
            </div>
            <div class="col-md-5 marg-t-5">
               <div class="tent">
                  @if($company->logo)
                  <img src="/resize/{{ $company->logo }}">
                  @else
                  <h3>{{ $company->name }}</h3>
                  @endif
               </div>
            </div>
            <div class="col-md-7 marg-t-5">
               <div class="location">
                  <h1>{{$company->name}}</h1>
                  <span>
                     <img src="{{ asset('WebAssets/img/location.png')}}">
                     <h6>japan</h6>
                     <img src="{{ asset('WebAssets/img/teg.png')}}">
                     <h6>lifestyle</h6>
                  </span>
                  <p>{{$company->description}}</p>
               </div>
            </div>
            <div class="col-md-5">
               <ul class="icons">
                  <li> <a href="{{ $company->website }}" target="_blank"><img src="{{ asset('WebAssets/img/link.png')}}"> </a> </li>
                   <li> <a href="{{ $company->linkedin }}" target="_blank"><img src="{{ asset('WebAssets/img/linkedin-in.png')}}"></a> </li>
                   <li> <a href="{{ $company->instagram }}" target="_blank"><img src="{{ asset('WebAssets/img/instagram.png')}}"></a> </li>
                   <li> <a href="{{ $company->twitter }}" target="_blank"><img src="{{ asset('WebAssets/img/twitter.png')}}"></a> </li>
               </ul>
            </div>
         </div>
      </div>
   </div>
   <!-- start disclosed funding section -->
   <section class="total-disclosed-funding">
      <div class="container">
         <div class="row">
            <div class="col-md-12">
               <div class="disclosed-funding">
                  <div class="rating">
                     <h1>Total disclosed funding</h1>
                     <h2>US$58M</h2>
                  </div>

                  <div class="series">
                     <h1>Series D <span>US$58M</span></h1>

                     <p>December 2019 <span>(news source)</span></p>
                  </div>
                  <div class="investors">
                     <h1>Investors</h1>
                     <p>
                       @foreach($investors as $investor)
                        <span onClick="goToUrl('{{ route('web-investor-details',['investor_name' => $investor->name ])}}')">{{$investor->name}}, </span>
                       @endforeach
                     </p>
                  </div>
               </div>
            </div>
         </div>

         <div class="row">
               <div class="col-md-12">
                  <div class="companies">
                     <div class="comapanies-heading">
                        <h1>Similar Companies</h1>
                     </div>
                 
                  <div class="about-company">
                     <div class="row">
                        <!-- <div class="col-md-2">
                           <div class="company-name">
                              <img src="img/logo.png">
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="company-description">
                              <h6>Grooves</h6>
                              <span>11 – 50 employees</span>
                           </div>
                        </div>
                        <div class="col-md-2">
                           <div class="company-name">
                              <img src="img/logo.png">
                           </div>
                        </div>
                        <div class="col-md-4">
                           <div class="company-description">
                              <h6>Rexit</h6>
                              <span>2 – 10 employees</span>
                           </div>
                        </div> -->
                        @foreach($similar_companies as $similar)
                        <div class="col-md-6">
                           @if($similar->logo)
                           <img onClick="goToUrl('{{ route('web-company-details',['company_name' => $similar->name ])}}')" src="/resize/{{ $similar->logo }}/64">
                           @else
                           <img onClick="goToUrl('{{ route('web-company-details',['company_name' => $similar->name ])}}')" src="https://via.placeholder.com/64">
                           @endif
                           <div class="company-description">
                              <h6 onClick="goToUrl('{{ route('web-company-details',['company_name' => $similar->name ])}}')">{{ $similar->name }}</h6>
                              <span>11 – 50 employees</span>
                           </div>
                        </div>
                        @endforeach

                        <!-- <div class="col-md-6">
                           <img src="img/logo.png">
                           <div class="company-description">
                              <h6>Grooves</h6>
                              <span>11 – 50 employees</span>
                           </div>
                        </div>
                        <div class="col-md-6">
                           <img src="img/logo.png">
                           <div class="company-description">
                              <h6>Grooves</h6>
                              <span>11 – 50 employees</span>
                           </div>
                        </div> -->

                     </div>
                  </div>
                  </div>
               </div>
   
            </div>



      </div>
   </section>
   <!-- end disclosed funding section -->


   <!--  CREATE NEW COMPANY popup start -->
   <div class="modal" id="createComp">
      <div class="modal-dialog modal-lg">
         <div class="modal-content">
            <div class="modal-header">
               <h4 class="modal-title">Create New Company</h4>
               <button type="button" class="close" data-dismiss="modal">&times;</button>
            </div>
            <div class="modal-body">
              <form action="{{route('web-company-save')}}" method="post">
               <div class="custom-form-main">
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Your Name</label>
                           <input class="form-control" type="text" name="creator_name" id="creator_name" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Your Email</label>
                           <input class="form-control" type="text" name="creator_email" id="creator_email" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Company Name</label>
                           <input class="form-control" type="text" name="name" id="name" required>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Country</label>
                           <select class="form-control" id="country_id" name="country_id">
                            @foreach($countries as $country)
                              <option value="{{$country->country_id}}" {{ (Request::old("country_id") == $country->country_id ? "selected":"") }} >{{$country->name}}</option>
                            @endforeach
                          </select>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Founded In</label>
                           <select class="form-control" id="founded" name="founded"">
                            @foreach($founded_years as $founded)
                              <option value="{{$founded}}" {{ (Request::old("founded") == $founded ? "selected":"") }} >{{$founded}}</option>
                            @endforeach
                          </select>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group">
                           <label>Founder Name</label>
                           <input class="form-control" type="text" name="founder_name" id="founder_name" required>
                        </div>
                     </div>
                  </div>
                  <div class="row">
                     <div class="col-12">
                        <div class="form-group">
                           <label>Description</label>
                           <textarea  class="form-control" id="description" name="description" required></textarea>  
                        </div>
                     </div>
                      <div class="col-12">
                        <div class="form-group">
                           <label>Website</label>
                           <input class="form-control" type="text" name="website" id="website" required>
                        </div>
                     </div>
                  </div>
                   <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Facebook</label>
                           <input class="form-control" type="text" name="facebook" id="facebook" required>
                           <div class="formIcon"> <i class="fa fa-facebook" aria-hidden="true"></i></div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Instagram</label>
                           <input class="form-control" type="text" name="instagram" id="instagram">
                            <div class="formIcon"> <i class="fa fa-instagram" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                      <div class="row">
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Twitter</label>
                           <input class="form-control" type="text" name="twitter" id="twitter">
                           <div class="formIcon"> <i class="fa fa-twitter" aria-hidden="true"></i></div>
                        </div>
                     </div>
                     <div class="col-md-6 col-12">
                        <div class="form-group formGrpIcn">
                           <label>Linked In</label>
                           <input class="form-control" type="text" name="linkedin" id="linkedin">
                           <div class="formIcon"> <i class="fa fa-linkedin" aria-hidden="true"></i></div>
                        </div>
                     </div>
                  </div>
                         <div class="row">
                     <div class="col-12">
                   <div class="form-group popupForm">
                        <label>Industry</label>   
         
                         <input type="text"  data-role="tagsinput" value="" id="testtag" name="industries" required>
                     </div>
                  </div>
               </div>

               </div>
            </div>
            {{csrf_field()}}
            <div class="modal-footer">
               <div class="customBtn">
                  <input type="submit" name="submit" value="Submit" class="btn btn-primary">
                  <!-- <button type="submit" class="btn btn-primary"  data-dismiss="modal">Submit</button> -->
               </div>
            </div>
            </form>
         </div>
      </div>
   </div>
   <!--  CREATE NEW COMPANY popup end -->

   <script type="text/javascript">

      $(document).ready(function(){
        $('#keyword').on('change keyup',function(e) {
            if(e.keyCode == 13){
              var url = "{{route('web-home')}}";
              var keyword="{{app('request')->input('keyword')}}";
              var page = 1;
              var keyword = $('#keyword').val();
              url += "?page="+page+"&keyword="+keyword;
              window.location = url;
            }
         });
      });

      function searchHome(){
           var url = "{{route('web-home')}}";
           var keyword="app('request')->input('keyword')";
           var page = 1;
           var keyword = $('#keyword').val();
           url += "?page="+page+"&keyword="+keyword;
           window.location = url;
       }


      $('#testtag').tagsinput({
         confirmKeys: [13, 44],
         maxTags: 5,
         typeahead: {
           source: {!! $industries_tags !!}
         }
       });

      function goToUrl(url){
         window.location = url;
       }

   </script>

   @stop
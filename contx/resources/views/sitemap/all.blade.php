<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>
<urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    @foreach ($countrys as $country)
        <url>
            <loc>{{ url('/') }}/companies/{{ $country->slug }}/all/all</loc>
            <lastmod>{{ $country->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @foreach ($countrys as $country)
    @foreach ($citys as $city)
      @if(@$city->country_id==@$country->country_id)
        <url>

            <loc>{{ url('/') }}/companies/{{ $country->slug }}/{{$city->slug}}/all</loc>

            <lastmod>{{ $country->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>

        </url>
          @endif
        @endforeach
    @endforeach
    @foreach ($industries as $industry)
        <url>
            <loc>{{ url('/') }}/companies/all/all/{{$industry->slug}}</loc>
            <lastmod>{{ $industry->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @foreach ($industries as $industry)
    @foreach ($countrys as $country)
    @foreach ($citys as $city)
      @if(@$city->country_id==@$country->country_id)
        <url>
            <loc>{{ url('/') }}/companies/{{$country->slug}}/{{$city->slug}}/{{$industry->slug}}</loc>
            <lastmod>{{ $industry->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
        @endif
        @endforeach
      @endforeach
    @endforeach
    @foreach ($countrys as $country)
        <url>
            <loc>{{ url('/') }}/investors/{{ $country->slug }}/all/all</loc>
            <lastmod>{{ $country->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @foreach ($countrys as $country)
    @foreach ($citys as $city)
      @if(@$city->country_id==@$country->country_id)
        <url>
            <loc>{{ url('/') }}/investors/{{ $country->slug }}/{{$city->slug}}/all</loc>
            <lastmod>{{ $country->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
        @endif
        @endforeach
    @endforeach
    @foreach ($industries as $industry)
        <url>
            <loc>{{ url('/') }}/investors/all/all/{{$industry->slug}}</loc>
            <lastmod>{{ $industry->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @foreach ($industries as $industry)
    @foreach ($countrys as $country)
    @foreach ($citys as $city)
      @if(@$city->country_id==@$country->country_id)
        <url>
            <loc>{{ url('/') }}/investors/{{$country->slug}}/{{$city->slug}}/{{$industry->slug}}</loc>
            <lastmod>{{ $industry->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
        @endif
        @endforeach
      @endforeach
    @endforeach
    @foreach ($countrys as $country)
        <url>
            <loc>{{ url('/') }}/founders/{{ $country->slug }}/all/all</loc>
            <lastmod>{{ $country->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach

    @foreach ($industries as $industry)
        <url>
            <loc>{{ url('/') }}/founders/all/all/{{$industry->slug}}</loc>
            <lastmod>{{ $industry->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>
    @endforeach
    @foreach ($industries as $industry)
    @foreach ($countrys as $country)

        <url>
            <loc>{{ url('/') }}/founders/{{$country->slug}}/all/{{$industry->slug}}</loc>
            <lastmod>{{ $industry->created_at->tz('UTC')->toAtomString() }}</lastmod>
            <changefreq>weekly</changefreq>
            <priority>0.9</priority>
        </url>

      @endforeach
    @endforeach
</urlset>

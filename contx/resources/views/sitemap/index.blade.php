<?php echo '<?xml version="1.0" encoding="UTF-8"?>'; ?>

<sitemapindex xmlns="http://www.sitemaps.org/schemas/sitemap/0.9">
    <sitemap>
        <loc>{{ url('/') }}/sitemap.xml/startups</loc>
    </sitemap>
    <sitemap>
        <loc>{{ url('/') }}/sitemap.xml/investors</loc>
    </sitemap>
    <sitemap>
        <loc>{{ url('/') }}/sitemap.xml/founders</loc>
    </sitemap>
    <sitemap>
          <loc>{{ url('/') }}/sitemap.xml/all</loc>
    </sitemap>
</sitemapindex>

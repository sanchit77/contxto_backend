<?php

namespace Tests;

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Modelizer\Selenium\SeleniumTestCase;

class SeleniumExampleTest extends SeleniumTestCase
{

    protected $screenCapturePath = __DIR__ . '/../log/screen-captures';
    // /**
    //  * A basic test example.
    //  *
    //  * @return void
    //  */
    // public function testExample()
    // {
    //     $this->visit('/');
    // }
    /**
     * A basic functional test example.
     *
     * @return void
     */
    public function testBasicExample()
    {
        // This is a sample code you can change as per your current scenario

        $this->visit('https://angel.co/mexico')
             //->see('Laravel')
             ->hold(3);
    }

    /**
     * A basic submission test example.
     *
     * @return void
     */
    public function testLoginFormExample()
    {
        // $loginInput = [
        //     'username' => 'dummy-name',
        //     'password' => 'dummy-password'
        // ];

        // // Login form test case scenario
        // $this->visit('/login')
        //      ->submitForm('#login-form', $loginInput)
        //      ->see('Welcome');  // Expected Result
    }

}

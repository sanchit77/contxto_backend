<?php

/*
|--------------------------------------------------------------------------
| Admin Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::post('test',['as'=>'test','uses'=>'Admin\StartupCont@test']);

Route::group([ 'namespace' => 'Admin'] , function()
{

    Route::get('/', ['as'=>'admin_login_get', 'uses'=>'AdminCont@admin_login_get']);
    Route::get('/login', ['as'=>'admin_login_get', 'uses'=>'AdminCont@admin_login_get']);

    Route::post('/', ['as'=>'admin_login_post', 'uses'=>'AdminCont@admin_login_post']);

    Route::post('index_data',['as'=>'startups_data','uses'=>'StartupCont@startups_data']);
    Route::post('index_data1',['as'=>'startups_data1','uses'=>'StartupCont@startups_data1']);
    Route::post('index_data_review',['as'=>'startups_data_review','uses'=>'StartupCont@startups_data_review']);


    Route::group(['middleware'=>'admin_middleware'] , function()
	{///////////		Logged In Admin 		////////////////

	Route::get('logout',['as'=>'admin_logout','uses'=>'AdminCont@admin_logout']);

    Route::get('dashboard',['as'=>'admin_dashboard','uses'=>'AdminCont@admin_dashboard']);
    Route::post('dashboard',['as'=>'admin_dash_data','uses'=>'AdminCont@admin_dash_data']);

    Route::get('profile/update', [ 'as' => 'aprofile_update_get', 'uses' => 'AdminCont@aprofile_update_get' ]);
	Route::post('profile/update', [ 'as' => 'aprofile_update_post', 'uses' => 'AdminCont@aprofile_update_post' ]);

	Route::get('password/update', [ 'as' => 'apassword_update_get', 'uses' => 'AdminCont@apassword_update_get' ]);
    Route::post('password/update', [ 'as' => 'apassword_update_post', 'uses' => 'AdminCont@apassword_update_post' ]);

    Route::group(['prefix'=>'startups'] , function()
    {///////////		Logged In Admin 		////////////////

        Route::any('countries',['as'=>'countries','uses'=>'StartupCont@countries']);
        Route::any('cities',['as'=>'cities','uses'=>'StartupCont@cities']);
        Route::any('industries',['as'=>'industries','uses'=>'StartupCont@industries']);

        Route::any('investors_ajax',['as'=>'investors_ajax','uses'=>'StartupCont@investors_ajax']);

        Route::any('target_countries',['as'=>'target_countries','uses'=>'StartupCont@target_countries']);

        Route::get('index',['as'=>'admin.startups.index','uses'=>'StartupCont@index']);

        Route::post('index',['as'=>'startups_data1','uses'=>'StartupCont@startups_data1']);

        Route::get('review',['as'=>'admin.startups.review','uses'=>'StartupCont@review']);

        Route::post('update/text',['as'=>'ajax_update_text','uses'=>'StartupCont@ajax_update_text']);
        Route::post('approve',['as'=>'ajax_approve_startup','uses'=>'StartupCont@ajax_approve_startup']);


        Route::post('add/has/many/relation',['as'=>'ajax_add_has_many_relation','uses'=>'StartupCont@ajax_add_has_many_relation']);
        Route::post('update/has/many/relation',['as'=>'ajax_update_has_many_relation','uses'=>'StartupCont@ajax_update_has_many_relation']);


        Route::post('update/target/countries',['as'=>'ajax_utarget_countries','uses'=>'StartupCont@ajax_utarget_countries']);

        Route::post('update/city',['as'=>'ajax_update_loc','uses'=>'StartupCont@ajax_update_loc']);
        Route::post('update/industry',['as'=>'ajax_update_industry','uses'=>'StartupCont@ajax_update_industry']);

        Route::post('create/investor',['as'=>'ajax_create_investor','uses'=>'StartupCont@ajax_create_investor']);
        Route::post('update/investor',['as'=>'ajax_update_investor','uses'=>'StartupCont@ajax_update_investor']);

        Route::post('data/investor/round',['as'=>'ajax_investor_round_data','uses'=>'StartupCont@ajax_investor_round_data']);
        Route::post('create/investor/round',['as'=>'ajax_cinvestor_round_data','uses'=>'StartupCont@ajax_cinvestor_round_data']);
        Route::post('update/investor/round',['as'=>'ajax_uinvestor_round_data','uses'=>'StartupCont@ajax_uinvestor_round_data']);

        Route::post('update/logo',['as'=>'ajax_update_logo','uses'=>'StartupCont@ajax_update_logo']);

        Route::post('create/news',['as'=>'ajax_cnews','uses'=>'StartupCont@ajax_cnews']);

        Route::post('data/founders',['as'=>'ajax_founder_data','uses'=>'StartupCont@ajax_founder_data']);
        Route::post('update/founder',['as'=>'ajax_founder_update','uses'=>'StartupCont@ajax_founder_update']);//Add As Well

    });

    Route::group(['prefix'=>'founders'] , function()
    {///////////		Founders 		////////////////

    Route::get('index',['as'=>'admin.founders.index','uses'=>'FounderCont@index']);
    Route::post('index',['as'=>'admin.founders.data','uses'=>'FounderCont@data_ajax']);
    Route::post('delete',['as'=>'admin.founders.delete','uses'=>'FounderCont@delete_ajax']);
    Route::get('startups',['as'=>'admin.founders.startups','uses'=>'FounderCont@startups']);

    Route::post('update',['as'=>'admin.founder.update','uses'=>'FounderCont@ajax_update']);

    });///////////		Founders 		////////////////

    Route::group(['prefix'=>'investors'] , function()
    {///////////		Investors 		////////////////

    Route::post('invupdate/logo',['as'=>'invajax_update_logo','uses'=>'InvestorCont@invajax_update_logo']);
    Route::get('index',['as'=>'admin.investors.index','uses'=>'InvestorCont@index']);

    Route::get('review',['as'=>'admin.investors.review','uses'=>'InvestorCont@review']);

    Route::post('index',['as'=>'admin.investors.data','uses'=>'InvestorCont@data_ajax']);

    Route::post('delete',['as'=>'admin.investors.delete','uses'=>'InvestorCont@delete_ajax']);

    Route::post('investors_data_review',['as'=>'investors_data_review','uses'=>'InvestorCont@investors_data_review']);

    Route::get('startups',['as'=>'admin.investors.startups','uses'=>'InvestorCont@startups']);

    Route::post('update',['as'=>'admin.investor.update','uses'=>'InvestorCont@ajax_update']);

    });///////////		Investors 		////////////////

    Route::group(['prefix'=>'industries'] , function()
    {///////////		Investors 		////////////////

    Route::get('index',['as'=>'admin.industries.index','uses'=>'IndustryCont@index']);
    Route::post('index',['as'=>'admin.industries.data','uses'=>'IndustryCont@data_ajax']);

    Route::post('delete',['as'=>'admin.industries.delete','uses'=>'IndustryCont@delete_ajax']);

    Route::get('startups',['as'=>'admin.industries.startups','uses'=>'IndustryCont@startups']);

    Route::post('update',['as'=>'admin.industry.update','uses'=>'IndustryCont@ajax_update']);

    });///////////		Investors 		////////////////

    Route::group(['prefix'=>'stage'] , function()
    {///////////		Investors 		////////////////

    Route::get('index',['as'=>'admin.stage.index','uses'=>'StageCont@index']);
    Route::get('add',['as'=>'admin.stage.add','uses'=>'StageCont@add']);
    Route::post('save',['as'=>'admin.stage.save','uses'=>'StageCont@save']);
    Route::get('delete/{id}',['as'=>'admin.stage.delete','uses'=>'StageCont@stageDelete']);

    Route::get('edit/{id}',['as'=>'admin.stage.edit','uses'=>'StageCont@StageEdit']);

    Route::post('update',['as'=>'admin.stage.update','uses'=>'StageCont@StageUpdate']);

    });///////////		Investors 		////////////////





    });///////////		Logged In Admin 		////////////////


});

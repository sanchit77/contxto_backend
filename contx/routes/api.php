<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:api')->get('/user', function (Request $request) {
//     return $request->user();
// });


Route::post('login', 'PassportController@login');
Route::post('register', 'PassportController@register');
 
Route::middleware('auth:api')->group(function () {
    Route::get('user', 'PassportController@details');
 
    Route::resource('products', 'ProductController');
});


Route::group([ 'namespace' => 'Api'] , function()
{
    Route::get('data', 'HomeCont@get_data');

    Route::post('startups/list', 'StartupCont@get_startups')->name('get_startups_list');

    Route::post('startups/add', 'StartupCont@add_startup');

    Route::get('countries/list', 'CountryCont@get_countries');

    Route::post('cities/list', 'CityCont@get_cities');

    Route::get('industries/list', 'IndustryCont@get_industries');

});


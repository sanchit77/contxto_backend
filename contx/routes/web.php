<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// return the home //

Route::get('/', function () {
    return redirect(route('web-home'));
});

Route::get('/companies/{country?}/{city?}/{industry?}', ['uses'=>'WebController@index','as'=>'web-home']);
Route::get('/companies/all/all/{name}', ['uses'=>'WebController@getIndustry','as'=>'web-home-industries']);
Route::get('/company/{company_name?}',['uses'=>'WebController@getCompanyDetails','as'=>'web-company-details']);
Route::get('/investor/{investor_name?}',['uses'=>'WebController@getInvestorDetails','as'=>'web-investor-details']);
Route::get('/founder/{founder_name?}',['uses'=>'WebController@getFounderDetails','as'=>'web-founder-details']);
Route::get('/checkemail/{email}',['uses'=>'WebController@investorCheck','as'=>'web-investor-check']);
Route::post('/company/new',['uses'=>'WebController@newCompany','as'=>'web-company-new']);
Route::get('/getslug',['uses'=>'WebController@getSlug','as'=>'web-get-slug']);
Route::get('/company/profile/{slug}',['uses'=>'WebController@profileCompany','as'=>'web-company-profile']);

Route::get('/company/create/new',['uses'=>'WebController@createCompany','as'=>'web-company-create-new']);
Route::get('/investor/create/new',['uses'=>'WebController@createInvestor','as'=>'web-investor-create-new']);

Route::post('/investor/new',['uses'=>'WebController@newInvestor','as'=>'web-investor-new']);

Route::get('/investors/{country?}/{industry?}/{sort?}', ['uses'=>'WebController@investors','as'=>'web-investors']);

Route::get('/founders/{country?}/{city?}/{industry?}/{sort?}', ['uses'=>'WebController@founders','as'=>'web-founders']);

Route::get('/company-news',['uses'=>'WebController@getCompanyNews','as'=>'web-company-news']);

Route::get('/newCompany',['uses'=>'WebController@companyCreate','as'=>'web-company-create']);

Route::get('/checkCompany',['uses'=>'WebController@companyCheck','as'=>'web-company-check']);

Route::post('/newCompany',['uses'=>'WebController@companySave','as'=>'web-company-save']);

Route::get('/investor/{investor_name}',['uses'=>'WebController@getInvestorDetails','as'=>'web-investor-details']);

Route::get('/cities',['uses'=>'WebController@getCities','as'=>'web-cities']);

Route::post('/newFounder',['uses'=>'WebController@founderSave','as'=>'web-founder-save']);

Route::post('/founder/new',['uses'=>'WebController@newFounder','as'=>'web-founder-new']);

Route::get('/founder/display',['uses'=>'WebController@founderDisplay','as'=>'web-founder-display']);

Route::get('/getid',['uses'=>'WebController@getId','as'=>'web-get-id']);

//////////////////		Image resizing 	//////////////////////////////////
Route::get('resize/{id}', function($id){

    $img = Image::make(config('app.IMAGE_URL').$id)->resize(function($constraint){	$constraint->aspectRatio();	});

    return $img->response();

});

Route::get('resize/{id}/{width?}/{height?}', function($id, $width=null, $height=null){

    $img = Image::make(config('app.IMAGE_URL').$id)->resize($width, $height, function($constraint){ $constraint->aspectRatio(); });

    return $img->response();

});
//////////////////		Image resizing 	//////////////////////////////////

//Route::get('web1',['as'=>'sync1','uses'=>'Admin\SyncCont@sync1']);

Route::get('test',['as'=>'test','uses'=>'Sync\SyncDbCont@test']);

Route::view('/web1', 'web/web1');
Route::get('sync1',['as'=>'sync1','uses'=>'Sync\SyncCont@sync1']);

Route::view('/web2', 'web/web2');
Route::get('sync2',['as'=>'sync2','uses'=>'Sync\SyncCont@sync2']);


Route::view('/web3', 'web/web3');
Route::view('/webAngel', ['as'=> 'webAngel', 'uses'=> 'Sync\SyncCont@webAngel']);
Route::get('sync/angel',['as'=>'sync_angel','uses'=>'Sync\SyncCont@sync_angel']);
Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');


/*
	Dynamic sitemap routes
*/

Route::get('/sitemap.xml', 'SitemapController@index');
Route::get('/sitemap.xml/startups', 'SitemapController@startups');
Route::get('/sitemap.xml/investors', 'SitemapController@investors');
Route::get('/sitemap.xml/founders', 'SitemapController@founders');
Route::get('/sitemap.xml/all', 'SitemapController@companies');
/*
	Slug generation for already saved data
*/

Route::get('/slug/startup/generate','SlugController@createStartupSlug');

Route::get('/slug/investor/generate','SlugController@createInvestorSlug');

Route::get('/slug/founder/generate','SlugController@createFounderSlug');
